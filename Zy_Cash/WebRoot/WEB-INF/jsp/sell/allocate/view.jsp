<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc" + "ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.size.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" id="ac_id" name="ac_id" value="${allocate.ac_id }"/>
<input type="hidden" id="ac_number" name="ac_number" value="${allocate.ac_number }"/>
<input type="hidden" id="ac_state" name="ac_state" value="${allocate.ac_state }"/>
<div class="border">
	<table width="100%" cellspacing="0" cellpadding="0">
	    <tr>
	      <td class="top-b border-b">
	        <input id="btn-send" class="t-btn btn-green" type="button" value="调出" style="display:none;"/>
	        <input id="btn-receive" class="t-btn btn-green" type="button" value="调入" style="display:none;"/>
	        <input id="btn-reject" class="t-btn btn-red" type="button" value="拒收" style="display:none;"/>
	        <input id="btn-reject-confirm" class="t-btn btn-red" type="button" value="确认" style="display:none;"/>
	        <input id="btn-print" class="t-btn btn-cblue" type="button" value="打印预览" onclick="javascript:handle.doPrint();"/>
	        <input id="btn_close" class="t-btn" type="button" value="返回"/>
	        <span style="float:right; padding-right:10px;">
	        	<input type="hidden" id="CurrentMode" value="0"/>
				<a id="sumBtn" name="mode-btn" class="t-btn btn-bl"  onclick="showMode.display(2);"><i></i>汇总</a>
			    <a id="listBtn" name="mode-btn" class="t-btn btn-blc on"  onclick="showMode.display(0);"><i></i>列表</a>
			    <a id="sizeBtn" name="mode-btn" class="t-btn btn-bc" onclick="showMode.display(1);"><i></i>尺码</a>
            </span>
	      </td>
	    </tr>
    </table>
</div>
<div class="grid-wrap">
    <div id="list-grid" style="display:'';width: 100%;">
        <table id="grid"></table>
        <div id="page"></div>
    </div>
    <div id="size-grid" style="display:none; width: 100%;">
        <table id="sizeGrid"></table>
        <div id="sizePage"></div>
    </div>
    <div id="sum-grid" style="display:none;width: 100%;">
       <table id="sumGrid"></table>
       <div id="sumPage"></div>
   </div>
</div>
<div>
	<table width="100%">
		<tr class="list first">
			<td align="right" width="60px">调出店铺：</td>
			<td width="130px">
				<input class="main_Input" type="text" readonly="readonly" name="outshop_name" id="outshop_name" value="${allocate.outshop_name }"/>
				<input type="hidden" name="ac_out_shop" id="ac_out_shop" value="${allocate.ac_out_shop }" />
			</td>
			<td align="right" width="60px">调出仓库：</td>
			<td width="130px">
				<input class="main_Input" type="text" readonly="readonly" name="outdepot_name" id="outdepot_name" value="${allocate.outdepot_name }"/>
				<input type="hidden" name="ac_out_dp" id="ac_out_dp" value="${allocate.ac_out_dp }" />
			</td>
			<td align="right" width="60px">调拨日期：</td>
			<td width="130px">
				<input readonly type="text" class="main_Input Wdate"  name="ac_date" id="ac_date" value="${allocate.ac_date }" style="width:198px;"/>
			</td>
			<td align="right" width="60px">单据编号：</td>
			<td>
				<input readonly type="text" class="main_Input"  value="${allocate.ac_number }"/>
			</td>
		</tr>
		<tr class="list">
			<td align="right">调入店铺：</td>
			<td>
				<input class="main_Input" type="text" readonly="readonly" name="inshop_name" id="inshop_name" value="${allocate.inshop_name }"/>
				<input type="hidden" name="ac_in_shop" id="ac_in_shop" value="${allocate.ac_in_shop }" />
			</td>
			<td align="right">调入仓库：</td>
			<td>
				<c:choose>
					<c:when test="${allocate.ac_state eq 1 and allocate.ac_in_shop eq sessionScope.cashier.ca_shop_code}">
						<input class="main_Input" type="text" readonly="readonly" name="indepot_name" id="indepot_name" value="${allocate.indepot_name }" style="width:170px;"/>
						<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryInDepot();"/>
						<input type="hidden" name="ac_in_dp" id="ac_in_dp" value="${allocate.ac_in_dp }" />
					</c:when>
					<c:otherwise>
						<input class="main_Input" type="text" readonly="readonly" name="indepot_name" id="indepot_name" value="${allocate.indepot_name }"/>
						<input type="hidden" name="ac_in_dp" id="ac_in_dp" value="${allocate.ac_in_dp }" />
					</c:otherwise>
				</c:choose>
			</td>
			<td align="right">总计数量：</td>
			<td>
				<input class="main_Input" type="text" name="ac_amount" id="ac_amount" value="${allocate.ac_amount }" readonly="readonly"/>
			</td>
			<td align="right">总计金额：</td>
			<td>
				<input class="main_Input" type="text" id="ac_sell_money" value="${allocate.ac_sell_money }" readonly="readonly"/>
			</td>
		</tr>
		<tr class="list last">
			<td align="right">经办人员：</td>
			<td>
				<input class="main_Input" type="text" value="${allocate.ac_man }" readonly="readonly"/>
			</td>
			<td align="right">备注：</td>
			<td colspan="3">
				<input class="main_Input" type="text" value="${allocate.ac_remark }" style="width:495px;" readonly="readonly"/>
			</td>
		</tr>
	</table>
</div>
</form>
<script src="<%=basePath%>data/sell/allocate/allocate_view.js"></script>
</body>
</html>