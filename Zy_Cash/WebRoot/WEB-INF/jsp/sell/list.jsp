<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title>零售查询</title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/search.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/date-util.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/DialogSelect.js\"></sc"+"ript>");
</script> 
</head>
<body>
<form id="form1" method="post" name="form1" action="">
<div class="wrapper">
	<div class="mod-search cf">
	   <div class="fl">
	    	<div id="filter-menu" class="ui-btn-menu fl">
	    		 <span style="float:left" class="ui-btn menu-btn">
		     		 <strong>日期：</strong>
	     		 	 <input type="text" class="main_Input Wdate-select" readonly="readonly" id="begindate" name="begindate" value="" />
					 -
                     <input type="text" class="main_Input Wdate-select" readonly="readonly" id="enddate" name="enddate" onclick="WdatePicker({minDate:'#F{$dp.$D(\'begindate\',{d:0})}'})"  value="" />
					 <b></b>
			  	 </span>
		 		<div class="con short" >
					<ul class="ul-inline">
						<li>
							<table class="table_add">
								<tr>
									<td align="right"><span id="t_code">单据号</span>：</td>
									<td><input type="text" id="code" name="code"
										class="main_Input w146" value="" /></td>
								</tr>
								<tr>
									<td align="right"><span id="t_emp">收银员</span>：</td>
									<td>
										<span class="ui-combo-wrap main_combo" id="spanEmp"></span>
										<input type="hidden" name="em_code"  id="em_code" value=""/>	
									</td>
								</tr>
								<tr id="tr_bd">
									  <td align="right">商品品牌：</td>
									  <td>
									  	<input class="main_Input" type="text" readonly="readonly" name="bd_name" id="bd_name" value="" style="width:122px;"/>
										<input type="button" value="" class="btn_select" onclick="javascript:DialogSelect.doQueryBrand(false,'bd_code','bd_name');"/>
										<input type="hidden" id="bd_code" name="bd_code" value=""/>
							  		</td>
								  </tr>
								  <tr id="tr_pd">
										<td align="right">商品名称：</td>
										<td>
											<input class="main_Input" type="text" readonly="readonly" name="pd_name" id="pd_name" value="" style="width:122px;"/>
											<input type="button" value="" class="btn_select" onclick="javascript:DialogSelect.doQueryProduct(false,'pd_code','pd_name');"/>
											<input type="hidden" id="pd_code" name="pd_code" value=""/>
										</td>
									</tr>
							</table>
						</li>
						<li style="float:right;margin-top: 5px;">
							<a class="ui-btn ui-btn-sp mrb" id="btn_ok" name="btn_ok">确认</a>
		       				<a class="ui-btn mrb" id="btn_reset" name="btn_reset">重置</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	    <div class="fl-m">
	    	<a class="ui-btn mrb" id="btn-search" name="btn-search">查询</a>
		</div>
	    <div class="fr">
	    	<a href="javascript:void(0);" class="ui-btn ui-btn-sp mrb" id="btn-type">明细查询</a>
	    	<input type="hidden" id="query_type" name="query_type" value="0"/>
	    </div> 
	 </div>
	<div class="grid-wrap">
	    <table id="grid">
	    </table>
    	<div id="page"></div>
	</div>
</div>
<script src="<%=basePath%>data/sell/sell_list.js?v=${time}"></script>
<script src="<%=printPath%>/CLodopfuncs.js"></script>
</form>
</body>
</html>