<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/css/default.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<style>
.left{background:#f0f0f0;width:100px;height:355px;overflow:hidden;top:0;left:0;position:absolute;border-bottom:#fff solid 1px;}
.left li{display:inline;float:left;width:100px;height:36px;font-size:14px;line-height:36px;}
.left li:hover{background:#e1e6eb;}
.left li a{color:#666;font-family: 'MicroSoft YaHei';text-decoration:none;width:80px;height:36px;display:block;padding-left:20px;}
.left li a:hover{color:#f60;text-decoration:none;}
.left li a.on{background:#fff;}

.right{background:#fff;width:566px;height:355px;overflow:auto;top:0;right:0;position:absolute;}
.right .div_con{float:left;width:520px;margin:10px 0 0 20px;}
.right .div_con h3{float:left;width:510px;height:30px;line-height:30px;font-size:14px;font-family:'Microsoft YaHei';margin:0;border-bottom:#ddd solid 1px;padding-left:10px;}
.right .div_con .info{float:left;width:100%;margin:10px 0 0;}
.right .div_con .info td{padding:5px;line-height:25px;}
.setok{width:656px;height:35px;overflow:hidden;border-top:#ddd solid 1px;bottom:0;left:0;position:absolute;padding:5px 10px 0 0;text-align:right;}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
</script> 
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<div class="left">
	<ul>
       	<li><a href="#vip_set" class="on">会员设置</a></li>
       	<li><a href="#cash_set">收银设置</a></li>
       	<li><a href="#other_set">其他设置</a></li>
	</ul>
</div>
<div class="right">
	<div id="vip_set" class="div_con">
		<h3>会员设置</h3>
		<table class="info">
			<tr>
				<td width="120" align="right">积分类别：</td>
				<td>
					<input type="radio" name="point_type" value="1" onclick="javascript:$('#key_point_type').val(1);"/> 四舍五入&nbsp;&nbsp;
		 			<input type="radio" name="point_type" value="2" onclick="javascript:$('#key_point_type').val(2);"/> 只入不舍&nbsp;&nbsp;
		 			<input type="radio" name="point_type" value="3" onclick="javascript:$('#key_point_type').val(3);"/> 只舍不入
		 			<input type="hidden" id="key_point_type" name="key_point_type" value="${model.KEY_POINT_TYPE}"/>
				</td>
			</tr>
			<tr>
				<td align="right">会员福利：</td>
				<td>
					<input type="radio" name="vip_birthday" value="1" onclick="javascript:$('#key_vip_birthday').val(1);$('.tr_vip_birthday').show();"/> 启用&nbsp;&nbsp;
			 		<input type="radio" name="vip_birthday" value="0" onclick="javascript:$('#key_vip_birthday').val(0);$('.tr_vip_birthday').hide();"/> 停用
	 				<input type="hidden" id="key_vip_birthday" name="key_vip_birthday" value="${model.KEY_VIP_BIRTHDAY}"/>
				</td>
			</tr>
			<tr class="tr_vip_birthday">
				<td align="right">生日折扣：
				</td>
				<td>
				<input type="text" class="main_Input" style="width:30px;height:12px;" id="key_vip_days" 
					onkeyup="javascript:vadiUtil.vadiNumber(this);" name="key_vip_days" value="${model.KEY_VIP_DAYS}"/> 天前后 
	 				折扣：<input type="text" class="main_Input" style="width:30px;height:12px;" 
	 						 onkeyup="javascript:vadiUtil.vadiDouble(this);" id="key_vip_rate" name="key_vip_rate" value="${model.KEY_VIP_RATE}"/>
				</td>
			</tr>
			<tr class="tr_vip_birthday">
				<td align="right">生日积分倍数：</td>
				<td>
	 				<input type="text" class="main_Input" style="width:30px;height:12px;" id="key_point_times" 
	 					onkeyup="javascript:vadiUtil.vadiNumber(this);" name="key_point_times" value="${model.KEY_POINT_TIMES}"/>
				</td>
			</tr>
			<tr>
				<td align="right">积分抵现不积分：</td>
				<td>
					<input type="radio" name="repoint" value="1" onclick="javascript:$('#key_repoint').val(1);"/> 启用&nbsp;&nbsp;
			 		<input type="radio" name="repoint" value="0" onclick="javascript:$('#key_repoint').val(0);"/> 停用
			 		<input type="hidden" id="key_repoint" name="key_repoint" value="${model.KEY_REPOINT}"/>
				</td>
			</tr>
			<tr>
				<td  align="right">会员折扣：</td>
				<td>
					<input type="radio" name="vip_state" value="1" onclick="javascript:$('#key_vip_state').val(1);"/> 启用&nbsp;&nbsp;
			 		<input type="radio" name="vip_state" value="0" onclick="javascript:$('#key_vip_state').val(0);"/> 停用
			 		<input type="hidden" id="key_vip_state" name="key_vip_state" value="${model.KEY_VIP_STATE}"/>
				</td>
			</tr>
			<tr class="tr_vip_rate">
				<td  align="right">会员折上折：</td>
				<td>
					<input type="radio" name="vip_double_rate" value="1" onclick="javascript:$('#key_vip_double_rate').val(1);"/> 启用&nbsp;&nbsp;
			 		<input type="radio" name="vip_double_rate" value="0" onclick="javascript:$('#key_vip_double_rate').val(0);"/> 停用
			 		<input type="hidden" id="key_vip_double_rate" name="key_vip_double_rate" value="${model.KEY_VIP_DOUBLE_RATE}"/>
				</td>
			</tr>
			<tr class="tr_vip_rate">
				<td align="right">会员折扣选择：</td>
				<td>
					<div>
		 				<div>
			 				<input type="radio" name="vip_rate_type" value="1" onclick="javascript:$('#key_vip_rate_type').val(1);"/> 会员类别&nbsp;&nbsp;&nbsp;
							<input type="radio" name="vip_rate_type" value="2" onclick="javascript:$('#key_vip_rate_type').val(2);"/> 会员商品类别<br/>
							<input type="radio" name="vip_rate_type" value="4" onclick="javascript:$('#key_vip_rate_type').val(4);"/> 商品会员价
			 				<input type="radio" name="vip_rate_type" value="3" onclick="javascript:$('#key_vip_rate_type').val(3);"/> 会员商品品牌
						</div>	
		 				<input type="hidden" id="key_vip_rate_type" name="key_vip_rate_type" value="${model.KEY_VIP_RATE_TYPE}"/>
		 			</div>
				</td>
			</tr>
		</table>
	</div>
	<div id="cash_set" class="div_con">
		<h3>收银设置</h3>
		<table class="info">
			<tr>
				<td width="120" align="right">金额精度：</td>
				<td>
			 		<input type="radio" name="money_decimal" value="1" onclick="javascript:$('#key_money_decimal').val(1);"/>&nbsp;整数
			 		<input type="radio" name="money_decimal" value="2" onclick="javascript:$('#key_money_decimal').val(2);"/>&nbsp;一位小数&nbsp;
					<input type="radio" name="money_decimal" value="3" onclick="javascript:$('#key_money_decimal').val(3);"/>&nbsp;两位小数&nbsp;
			 		<input type="hidden" id="key_money_decimal" name="key_money_decimal" value="${model.KEY_MONEY_DECIMAL}"/>
				</td>
			</tr>
			<tr>
				<td align="right">金额取舍：</td>
				<td>
					<input type="radio" name="money_type" value="1" onclick="javascript:$('#key_money_type').val(1);"/> 四舍五入&nbsp;
			 		<input type="radio" name="money_type" value="2" onclick="javascript:$('#key_money_type').val(2);"/> 只入不舍&nbsp;
			 		<input type="radio" name="money_type" value="3" onclick="javascript:$('#key_money_type').val(3);"/> 只舍不入
			 		<input type="hidden" id="key_money_type" name="key_money_type" value="${model.KEY_MONEY_TYPE}"/>
				</td>
			</tr>
			<tr>
				<td align="right">默认打折方式：</td>
				<td>
					<input type="radio" name="rate_type" value="1" onclick="javascript:$('#key_rate_type').val(1);"/> 折扣&nbsp;&nbsp;
			 		<input type="radio" name="rate_type" value="2" onclick="javascript:$('#key_rate_type').val(2);"/> 让利
			 		<input type="radio" name="rate_type" value="3" onclick="javascript:$('#key_rate_type').val(3);"/> 实际金额
			 		<input type="hidden" id="key_rate_type" name="key_rate_type" value="${model.KEY_RATE_TYPE}"/>
				</td>
			</tr>
			<tr>
				<td align="right">用优惠券再发：</td>
				<td>
			 		<input type="radio" name="reecoupon" value="1" onclick="javascript:$('#key_reecoupon').val(1);"/> 是
					<input type="radio" name="reecoupon" value="0" onclick="javascript:$('#key_reecoupon').val(0);"/> 否&nbsp;&nbsp;
			 		<input type="hidden" id="key_reecoupon" name="key_reecoupon" value="${model.KEY_REECOUPON}"/>
				</td>
			</tr>
			<tr>
				<td align="right">促销状态：</td>
				<td>
			 		<input type="radio" name="sale" value="1" onclick="javascript:$('#key_sale').val(1);"/> 启用
					<input type="radio" name="sale" value="0" onclick="javascript:$('#key_sale').val(0);"/> 停用&nbsp;&nbsp;
			 		<input type="hidden" id="key_sale" name="key_sale" value="${model.KEY_SALE}"/>
				</td>
			</tr>
			<tr>
				<td align="right">小票退货：</td>
				<td>
			 		<input type="radio" name="back_number" value="1" onclick="javascript:$('#key_back_number').val(1);"/> 启用
					<input type="radio" name="back_number" value="0" onclick="javascript:$('#key_back_number').val(0);"/> 停用&nbsp;&nbsp;
			 		<input type="hidden" id="key_back_number" name="key_back_number" value="${model.KEY_BACK_NUMBER}"/>
				</td>
			</tr>
		</table>
	</div>
	<div id="other_set" class="div_con">
		<h3>其他设置</h3>
		<table class="info">
			<tr>
				<td width="120" align="right">主导必选：</td>
				<td>
					<input type="radio" name="emp_main" value="1" onclick="javascript:$('#key_emp_main').val(1);"/> 是
					<input type="radio" name="emp_main" value="0" onclick="javascript:$('#key_emp_main').val(0);"/> 否&nbsp;&nbsp;
			 		<input type="hidden" id="key_emp_main" name="key_emp_main" value="${model.KEY_EMP_MAIN}"/>
				</td>
			</tr>
			<tr>
				<td align="right">副导必选：</td>
				<td>
					<input type="radio" name="emp_salve" value="1" onclick="javascript:$('#key_emp_salve').val(1);"/> 是
					<input type="radio" name="emp_salve" value="0" onclick="javascript:$('#key_emp_salve').val(0);"/> 否&nbsp;&nbsp;
			 		<input type="hidden" id="key_emp_salve" name="key_emp_salve" value="${model.KEY_EMP_SALVE}"/>
				</td>
			</tr>
			<tr>
				<td align="right">位置必选：</td>
				<td>
					<input type="radio" name="area" value="1" onclick="javascript:$('#key_area').val(1);"/> 是
					<input type="radio" name="area" value="0" onclick="javascript:$('#key_area').val(0);"/> 否&nbsp;&nbsp;
			 		<input type="hidden" id="key_area" name="key_area" value="${model.KEY_AREA}"/>
				</td>
			</tr>
			<tr>
				<td align="right">首行默认全选：</td>
				<td>
					<input type="radio" name="select_all" value="1" onclick="javascript:$('#key_select_all').val(1);"/> 是
					<input type="radio" name="select_all" value="0" onclick="javascript:$('#key_select_all').val(0);"/> 否&nbsp;&nbsp;
			 		<input type="hidden" id="key_select_all" name="key_select_all" value="${model.KEY_SELECT_ALL}"/>
				</td>
			</tr>
			<tr>
				<td align="right">线上交易：</td>
				<td>
					<input type="radio" name="line_sell" value="1" onclick="javascript:$('#key_line_sell').val(1);"/> 启用
					<input type="radio" name="line_sell" value="0" onclick="javascript:$('#key_line_sell').val(0);"/> 停用&nbsp;&nbsp;
			 		<input type="hidden" id="key_line_sell" name="key_line_sell" value="${model.KEY_LINE_SELL}"/>
				</td>
			</tr>
		</table>
	</div>
</div>
</form>
<div class="footdiv">
	<input class="btn_confirm" type="button" id="btn-save" name="btn-save" value="确定"/>
	<input class="btn_close" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
<script>
var config = parent.CONFIG,system=parent.SYSTEM;
var api = frameElement.api, W = api.opener;
$(function(){
	$("#btn-save").on('click',function(){
		var point_times = $("#key_point_times").val();
		var vip_birthday = $("#key_vip_birthday").val();
		var vip_days = $("#key_vip_days").val();
		var vip_rate = $("#key_vip_rate").val();
		if(null == point_times || '' == point_times){
			W.Public.tips({type: 1, content : "请输入会员生日积分倍数"});
			$("#key_point_times").select().focus();
			return;
		}
		if(null == vip_rate || '' == vip_rate){
			W.Public.tips({type: 1, content : "请输入会员生日积分倍数"});
			$("#key_vip_rate").select().focus();
			return;
		}
		if(null == vip_birthday || '' == vip_birthday){
			Public.tips({type: 1, content : "请选择生日福利"});
			$("#key_vip_birthday").focus();
			return;
		}else{
			if(vip_birthday == 1){
				if(null == vip_days || '' == vip_days){
					W.Public.tips({type: 1, content : "请输入会员生日范围"});
					$("#vip_days").select().focus();
					return;
				}
			}
		}
		var pdata = {};
		pdata.KEY_POINT_TIMES = point_times;
		pdata.KEY_VIP_DAYS = vip_days;
		pdata.KEY_VIP_RATE = vip_rate;
		$("input[type='hidden']").each(function(){
			var key = $(this).attr("id").toUpperCase();
			var value = $(this).val();
			pdata[key]=value;
		});
		$("#btn-save").attr("disabled",true);
		W.$.dialog.confirm('修改成功后会退出系统，确定要修改吗?', function(){
			$.ajax({
				type:"POST",
				url:config.BASEPATH+"cash/set/updateCash",
				data:{"data":JSON.stringify(pdata)},
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						W.Public.tips({type: 0, content : data.message});
						setTimeout(function(){
							window.parent.location.replace(config.BASEPATH+"logout");
						},300);
					}else{
						W.Public.tips({type: 1, content : data.message});
						setTimeout("api.zindex()",300);
					}
					$("#btn-save").attr("disabled",false);
				}
			});
		},function(){
			$("#btn-save").attr("disabled",false);
			setTimeout("api.zindex()",200);
		});
	});
	
	$(".right").scroll(function () {
        var _top = $(".right").scrollTop();
        if (_top < 200){
        	$(".left").find(".on").removeClass("on");
            $(".left").find("[href=\"#vip_set\"]").addClass("on");
        }else if((_top < 500) && (_top > 210)){
        	$(".left").find(".on").removeClass("on");
            $(".left").find("[href=\"#cash_set\"]").addClass("on");
        }else if((_top < 900) && (_top > 500)){
        	$(".left").find(".on").removeClass("on");
            $(".left").find("[href=\"#other_set\"]").addClass("on");
        }
    });
	$("input[type='hidden']").each(function(){
		var key = $(this).attr("id");
		key = key.replace('key_','');
		var _value = $(this).val();
		$("input[name='"+key+"']").each(function(){
			var value = $(this).val();
			if(value == _value){
				$(this).attr("checked",true);
			}
		});
	});
	$("#btn_close").click(function(){
		api.close();
	});
});
var vadiUtil={
		vadiDouble:function(obj) {
			var data = $(obj).val();
			if (isNaN(data)) {
				W.Public.tips({type: 1, content : "请输入数字"});
				$(obj).val("").focus();
				return;
			}
			if (data<0 || data>1) {
				W.Public.tips({type: 1, content : "数字必须0-1之间"});
				$(obj).val("").focus();
				return;
			}
		},
		vadiNumber:function(obj) {
			var data = $(obj).val();
			if (isNaN(data)) {
				W.Public.tips({type: 1, content : "请输入整数"});
				$(obj).val("").focus();
				return;
			}
		}
	};
</script>
</body>
</html>