<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.extend.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.messages_zh.js\"></sc"+"ript>");
</script> 
</head>
<body>
<form name="form1" method="post" action="" id="form1">
<div class="shop_box">
	<table class="table_add" width="100%">
		<tr>
			<td align="right" width="68px">日期：</td>
			<td align="left">
				<input class="main_Input w146"  type="text" name="sd_date" id="sd_date" value=""/>
			</td>
		</tr>
		<tr>
			<td align="right" width="68px">姓名：</td>
			<td align="left">
				<input class="main_Input w146"  type="text" name="sd_customer" id="sd_customer" value=""/>
			</td>
		</tr>
		<tr>
			<td align="right">手机：</td>
			<td align="left">
				<input class="main_Input w146"  type="text" name="sd_tel" id="sd_tel" value=""/>
			</td>
		</tr>
		<tr>
			<td align="right">订金：</td>
			<td align="left">
				<input class="main_Input w146"  type="text" name="sd_deposit" id="sd_deposit" value="0" 
					onkeyup="javascript:vadiUtil.vadiNumber(this);"/>
			</td>
		</tr>
		<tr>
			<td align="right">余款：</td>
			<td align="left">
				<input class="main_Input w146"  type="text" name="last_money" id="last_money" value="0" />
			</td>
		</tr>
		<tr>
			<td align="right">备注：</td>
			<td align="left">
				<input class="main_Input w146"  type="text" name="sd_remark" id="sd_remark" value="" maxlength=24/>
			</td>
		</tr>
	</table>
</div>
<div class="footdiv">
	<a id="ischeck" class="fl">
		<label class="chk">
			<input type="checkbox"/>打印
		</label>
	</a>
	<input class="ui-btn ui-btn-sp mrb" id="btn-save" name="btn-save" type="button" value="确定"/>
	<input class="ui-btn mrb" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
</form>
<script src="<%=printPath%>/CLodopfuncs.js"></script>
<script>
	$().ready(function() {
		$("#form1").validate({
			ignore: "",
			onfocusout : function(element) {
				$(element).valid();
			},
			errorPlacement: function(error, element) { //错误信息位置设置方法
				element.focus();
				$("#errorTip").empty();
				error.appendTo( $("#errorTip") );
			},
			rules : {
				sd_deposit : {
					required : true,
					digits:true,
					min:1
				},
				sd_tel:{
					required : true
				}
			},
			messages : {
				sd_deposit : {
					required : "请输入订金",
					digits : "订金必须是正整数"
				},
				sd_tel:{
					required : "请输入手机号"
				}
			}
		});
	});
</script>
<script src="<%=basePath%>data/deposit/pay_deposit.js?v=${time}"></script>
</body>
</html>