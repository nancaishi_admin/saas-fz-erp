<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.extend.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.messages_zh.js\"></sc"+"ript>");
</script> 
</head>
<body>
<form name="form1" method="post" action="" id="form1">
<input type="hidden" id="vm_id" name="vm_id" value=""/>
<input type="hidden" id="vm_cardcode" name="vm_cardcode" value=""/>
<input type="hidden" id="vm_mobile" name="vm_mobile" value=""/>
<div class="shop_box">
	<table class="table_add" width="100%">
		<tr>
			<td align="right" width="68px">卡号/手机：</td>
			<td align="left">
				<input class="main_Input w146"  type="text" name="cardcode" id="cardcode" value=""/>
			</td>
		</tr>
		<tr>
			<td align="right">姓名：</td>
			<td align="left">
				<input class="main_Input w146"  type="text" name="vm_name" id="vm_name" maxlength="10" value=""/>
			</td>
		</tr>
		<tr>
			<td align="right">剩余积分：</td>
			<td align="left">
				<input class="main_Input w146"  type="text" name="vm_points" id="vm_points" value="" />
			</td>
		</tr>
		<tr>
			<td align="right">操作：</td>
			<td align="left">
				<span class="ui-combo-wrap" id="spanOper"></span>
				<input type="hidden" name="option" id="option" value=""/>
			</td>
		</tr>
		<tr>
			<td align="right">积分：</td>
			<td align="left">
				<input class="main_Input w146"  type="text" name="point" id="point" value="0" />
			</td>
		</tr>
		<tr>
			<td align="right">备注：</td>
			<td align="left">
				<input class="main_Input w146"  type="text" name="vpl_remark" id="vpl_remark" value="" />
			</td>
		</tr>
		<tr>
			<td colspan="2" id="errorTip" style="text-align:center; color:red;">输入卡号/手机请回车查询</td>
		</tr>
	</table>
</div>
<div class="footdiv">
	<a id="ischeck" class="fl">
		<label class="chk">
			<input type="checkbox"/>打印
		</label>
	</a>
	<input class="ui-btn ui-btn-sp mrb" id="btn-save" name="btn-save" type="button" value="确定"/>
	<input class="ui-btn mrb" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
</form>
<script src="<%=printPath%>/CLodopfuncs.js"></script>
<script>
	$().ready(function() {
		$("#form1").validate({
			ignore: "",
			onfocusout : function(element) {
				$(element).valid();
			},
			errorPlacement: function(error, element) { //错误信息位置设置方法
				element.focus();
				$("#errorTip").empty();
				error.appendTo( $("#errorTip") );
			},
			rules : {
				point : {
					required : true,
					digits:true,
					min:1
				}
			},
			messages : {
				point : {
					required : "请输入积分",
					digits : "积分必须是正整数"
				}
			}
		});
	});
</script>
<script src="<%=basePath%>data/vip/member/member_point.js"></script>
</body>
</html>