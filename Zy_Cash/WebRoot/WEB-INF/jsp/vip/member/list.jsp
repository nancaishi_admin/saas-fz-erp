<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/search.js\"></sc"+"ript>");
</script> 
</head>
<body id="main_content">
<form name="form1" method="post" action="" id="form1">
<input type="hidden" name="flushFlag" id="flushFlag" value=""/>
<div class="wrapper">
	<div class="mod-search cf">
	   <div class="fl">
	    	<div id="filter-menu" class="ui-btn-menu fl">
	    		 <span style="float:left" class="ui-btn menu-btn">
		     		 <strong>卡号/手机</strong>
		     		 <input type="text" id="vm_cardcode" name="vm_cardcode" class="main_Input w146" value="" style="height:12px;margin-top:-3px;"/>
					 <b></b>
			  	 </span>
		 		<div class="con short" >
					<ul class="ul-inline">
						<li>
							<table class="table_add">
								<tr>
									<td align="right">发卡店铺：</td>
									<td><input class="main_Input w146" type="text" id="sp_name"
										name="sp_name" value="" readonly="readonly"
										style="width:121px;" /> <input type="button" id="btn_shop"
										name="btn_shop" value="" class="btn_select" /> <input
										type="hidden" name="vm_shop_code" id="vm_shop_code" value="" />
									</td>
								</tr>
								<tr>
									<td align="right">会员姓名：</td>
									<td><input type="text" id="vm_name" name="vm_name"
										class="main_Input w146" value="" /></td>
								</tr>
							</table>
						</li>
				<li style="float:right;margin-top: 5px;">
					<a class="ui-btn ui-btn-sp mrb" id="btn_ok">确认</a>
       				<a class="ui-btn mrb" id="btn_reset">重置</a>
				</li>
			</ul>
		</div>
			</div>
		</div>
	    <div class="fl-m">
	    	<a class="ui-btn mrb" id="btn-search" name="btn-search">查询</a>
	    	<a href="javascript:void(0);" class="ui-btn ui-btn-sp mrb" id="btn-add" >发卡</a>
	    	<a href="javascript:void(0);" class="ui-btn ui-btn-sp mrb" id="btn-rate" >打折</a>
	    	<a id="vip_other" style="margin-top:5px;">
				<label class="chk">
					<input type="checkbox"/>借卡
				</label>
			</a>
		</div>
	    <div class="fr">
	    	<a href="javascript:void(0);" class="ui-btn ui-btn-sp mrb" id="btn-report" >分析</a>
	    </div> 
	 </div>
	<div class="grid-wrap">
	    <table id="grid">
	    </table>
    	<div id="page"></div>
	</div>
</div>
</form>
<script src="<%=basePath%>data/vip/member/member_list.js"></script>
</body>
</html>