<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
	table tr td b {color:red;}
	.list td{line-height:35px; }
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.extend.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.messages_zh.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/date-util.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" id="cd_id" name="cd_id" value="${card.cd_id }"/>
<input type="hidden" id="cdl_type" name="cdl_type" value="${cdl_type }"/>
<div class="border">
			<table width="100%">
				<tr class="list first">
					<td align="right" width="80px">储值卡号：</td>
					<td width="130px">
						<input class="main_Input w146" type="text" name="cdl_cardcode" id="cdl_cardcode" value="${card.cd_cardcode }"/>
					</td>
					<td align="right" width="80px">姓名：</td>
					<td>
						<input readonly type="text" class="main_Input w146" value="${card.cd_name}" />
					</td>
				</tr>
				<tr class="list">
					<td align="right">状态：</td>
					<td>
						<input readonly type="text" id="cd_state" name="cd_state" class="main_Input w146" value="${card.cd_state}" />
					</td>
					<td align="right" width="80px">余额：</td>
					<td>
						<input readonly type="text" id="left_money" name="left_money" class="main_Input w146" value="${card.cd_money}" />
					</td>
				</tr>
				<tr class="list">
					<td align="right">金额：</td>
					<td>
						<input class="main_Input w146" type="text" name="cdl_money" id="cdl_money" value="0"/>
					</td>
					<td align="right">日期：</td>
					<td>
						<input readonly type="text" class="main_Input Wdate w146"  name="cdl_date" id="cdl_date" onclick="WdatePicker()" value=""/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">现金：</td>
					<td>
						<input class="main_Input w146" type="text" name="cdl_realcash" id="cdl_realcash" value="0"/>
					</td>
					<td align="right">刷卡：</td>
					<td>
						<input class="main_Input w146" type="text" name="cdl_bankmoney" id="cdl_bankmoney" value="0"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right"><b>*</b>现金账户：</td>
					<td>
						<span class="ui-combo-wrap" id="spanBank" ></span>
						<input type="hidden" name="cdl_ba_code" id="cdl_ba_code" value=""/>
					</td>
					<td align="right"><b>*</b>刷卡账户：</td>
					<td>
						<span class="ui-combo-wrap" id="spanBanks" ></span>
						<input type="hidden" name="cdl_bank_code" id="cdl_bank_code" value=""/>
					</td>
				</tr>
				<tr class="list last">
					<td align="right"></td>
					<td id="errorTip" class="errorTip" colspan="3"></td>
				</tr>
			</table>
</div>
<div class="footdiv">
	<a id="ischeck" class="fl">
		<label class="chk">
			<input type="checkbox"/>打印
		</label>
	</a>
	<input class="ui-btn ui-btn-sp mrb" id="btn-save" name="btn-save" type="button" value="确定"/>
	<input class="ui-btn mrb" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
</form>
<script>
	$().ready(function() {
		$("#form1").validate({
			ignore: "",
			onfocusout : function(element) {
				$(element).valid();
			},
			errorPlacement: function(error, element) { //错误信息位置设置方法
				element.focus();
				$("#errorTip").empty();
				error.appendTo( $("#errorTip") );
			},
			rules : {
				cdl_money : {
					required : true,
					digits:true,
					min:1
				},
				cdl_realcash : {
					required : true,
					digits:true
				},
				cdl_bankmoney : {
					required : true,
					digits:true
				},
				cdl_ba_code : {
					required : true
				},
				cdl_bank_code : {
					required : true
				}
			},
			messages : {
				cdl_money : {
					required : "请输入充值金额",
					digits : "请输入正整数"
				},
				cdl_realcash : {
					required : "请输入实收现金",
					digits : "请输入正整数"
				},
				cdl_bankmoney : {
					required : "请输入实收刷卡",
					digits : "请输入正整数"
				},
				cdl_ba_code : "请选择现金账户",
				cdl_bank_code : "请选择刷卡账户"
			}
		});
	});
</script>
<script src="<%=basePath%>data/card/card_recharge.js"></script>
</body>
</html>