<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
	table tr td b {color:red;}
	.list td{line-height:35px; }
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/date-util.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/lundarDate.js\"></sc"+"ript>");
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<div class="border">
			<table width="100%">
				<tr class="list first">
					<td align="right" width="80px"><b>*</b>代金券号：</td>
					<td width="130px">
						<input class="main_Input w146" type="text" name="vc_cardcode" id="vc_cardcode" value="" maxlength="15"/>
					</td>
					<td align="right" width="80px"><b>*</b>发放日期：</td>
					<td>
						<input readonly type="text" class="main_Input Wdate w146"  name="vc_grantdate" id="vc_grantdate" onclick="WdatePicker()" value=""/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">有效日期：</td>
					<td>
						<input readonly type="text" class="main_Input Wdate w146"  name="vc_enddate" id="vc_enddate" onclick="WdatePicker()" value=""/>
					</td>
					<td align="right">发卡店铺：</td>
					<td>
						<input class="main_Input w146" type="text" readonly="readonly" name="shop_name" id="shop_name" value=""/>
						<input type="hidden" name="vc_shop_code" id="vc_shop_code" value=""/>
					</td>
				</tr>
				<tr class="list">
					<td align="right"><b>*</b>面&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;值：</td>
					<td>
						<input class="main_Input w146" type="text" name="vc_money" id="vc_money" value="0"/>
					</td>
					<td align="right"><b>*</b>实收现金：</td>
					<td>
						<input class="main_Input w146" type="text" name="vc_realcash" id="vc_realcash" value="0"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right"><b>*</b>收款账户：</td>
					<td>
						<span class="ui-combo-wrap" id="spanBank" ></span>
						<input type="hidden" name="vcl_ba_code" id="vcl_ba_code" value=""/>
					</td>
					<td align="right"><b>*</b>经办人员：</td>
					<td>
						<input class="main_Input w146" type="text" readonly="readonly" name="vc_manager" id="vc_manager" value=""/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">用户姓名：</td>
					<td>
						<input class="main_Input w146" type="text" name="vc_name" id="vc_name" value=""/>
					</td>
					<td align="right">手机号码：</td>
					<td>
						<input class="main_Input w146" type="text" name="vc_mobile" id="vc_mobile" value=""/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">备注信息：</td>
					<td colspan="3">
						<input class="main_Input" type="text" name="vc_remark" id="vc_remark" value="" style="width:405px;"/>
					</td>
				</tr>
				<tr class="list last">
					<td align="right"></td>
					<td id="errorTip" class="errorTip" colspan="3"></td>
				</tr>
			</table>
</div>
<div class="footdiv">
	<a id="ischeck" class="fl">
		<label class="chk">
			<input type="checkbox"/>打印
		</label>
	</a>
	<input class="ui-btn ui-btn-sp mrb" id="btn-save" name="btn-save" type="button" value="确定"/>
	<input class="ui-btn mrb" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
</form>
<script src="<%=printPath%>/CLodopfuncs.js"></script>
<script src="<%=basePath%>data/voucher/voucher_add.js"></script>
<script language="javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.extend.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.messages_zh.js\"></sc"+"ript>");
</script>
<script>
	$().ready(function() {
		$("#form1").validate({
			ignore: "",
			onfocusout : function(element) {
				$(element).valid();
			},
			errorPlacement: function(error, element) { //错误信息位置设置方法
				element.focus();
				$("#errorTip").empty();
				error.appendTo( $("#errorTip") );
			},
			rules : {
				start_no : {
					required : true,
					digits:true
				},
				add_count : {
					required : true,
					digits:true,
					min:1
				},
				vc_shop_code : {
					required : true
				},
				vc_manager : {
					required : true
				},
				vc_money : {
					required : true
				},
				vc_realcash : {
					required : true
				},
				vcl_ba_code : {
					required : true
				},
				vc_mobile : {
					isMobile : true
				}
			},
			messages : {
				start_no : {
					required : "请输入起始编号",
					digits : "请输入整数"
				},
				add_count : {
					required : "请输入新增数量",
					digits : "请输入正整数"
				},
				vc_shop_code : "请选择发放店铺",
				vc_manager : "请选择经办人",
				vc_money : "请输入面值",
				vc_realcash : "请输入实收现金",
				vcl_ba_code : "请选择银行账户",
				vc_mobile : "请输入正确的手机号码"
			}
		});
	});
</script>
</body>
</html>