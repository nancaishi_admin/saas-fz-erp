<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
</script>
<style>
</style>
</head>
<body>
<form name="form1" method="post" action="" id="form1" >
<div class="wrapper">
	<div class="grid-wrap" >
		<table id="grid">
		</table>
		<div id="page"></div>
	</div>
	<div class="mod-search cf" style="margin-left:5px;margin-top:10px; float: left;">
	    <div class="fl" style="width:100%">
	      	<ul class="ul-inline">
		        <li>卡号/手机：
					<input type="text" class="main_Input" id="vm_cardcode" name="vm_cardcode" value=""/>
					<input type="text" style="display:none;"/>
					<input type="hidden" id="vm_id" name="vm_id" value=""/>
					<input type="hidden" id="vm_code" name="vm_code" value=""/>
		        </li>
	      		<li>姓名：
					<input type="text" class="main_Input" style="width:78px;" id="vm_name" name="vm_name" value="" readonly="readonly"/>
		        </li>
		        <li >积分：
					<input type="text" class="main_Input" style="width:78px;" id="vm_point" name="vm_point" value="" readonly="readonly"/>
		        </li>
		        <li style="margin-left: 40px;">
			    	<a class="ui-btn mrb ui-btn-sp" id="btn-change" name="btn-change">兑换</a>
		        </li>
	     	</ul>
		</div>
	</div>
</div>
</form>
<script type="text/javascript" src="<%=basePath%>data/gift/gift_list.js"></script>
</body>
</html>