<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/css/default.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
</script> 
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<table class="t_add" width="100%" style="padding:10px;">
	<tr style="line-height:45px; ">
		<td align="right" >天气：</td>
		<td align="left">
		 	<img id="btn_weather" src="http://api.k780.com:88/upload/weather/d/0.gif"/>
		</td>
	</tr>
	<tr style="line-height:45px; ">
		<td align="right" >温度：</td>
		<td align="left">
		 	<span id="btn_tmp">12℃~20℃</span>
		</td>
	</tr>
	<tr style="line-height:45px; ">
		<td align="right" >风级：</td>
		<td align="left">
		 	<span id="btn_wind">无</span>
		</td>
	</tr>
	<tr style="line-height:45px; ">
		<td align="right" >城市：</td>
		<td align="left">
		 	<span id="btn_city"></span>
		</td>
	</tr>
	<tr >
		<td align="center" colspan="2">
		 	<span style="color:red;">当前店铺所在地的天气</span>
		</td>
	</tr>
</table>
</form>
<div class="footdiv">
	<input class="btn_close" type="button" id="btn_close" name="btn_close" value="关闭"/>
</div>
<script>
var config = parent.CONFIG,system=parent.SYSTEM;
var api = frameElement.api, W = api.opener;
$(function(){
	function init(){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"day/weather",
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					if(null != data.data && undefined != data.data){
						var tmpHtml = data.data.we_min_tmp+"℃~"+data.data.we_max_tmp+"℃";
						var windHtml = data.data.we_wind;
						var we_id = data.data.we_we_id;
						$("#btn_tmp").text(tmpHtml);
						$("#btn_wind").text(windHtml);
						$("#btn_city").text(data.data.we_city);
						$("#btn_weather").attr("src","http://api.k780.com:88/upload/weather/d/"+(parseInt(we_id)-1)+".gif");
					}
				}
			}
		});
	}
	init();
	$("#btn_close").click(function(){
		api.close();
	});
});
</script>
</body>
</html>