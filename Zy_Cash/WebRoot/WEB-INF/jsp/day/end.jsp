<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/css/default.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/css/iconfont/iconfont.css" rel="stylesheet" type="text/css"/>
<style>
.mutab {
	    height: 32px;
	    margin: 10px 0 0 11px;
	}
	ul {
	    list-style: none;
	}
	.mutab li {
	    cursor: pointer;
	    display: inline;
	    float: left;
	    margin-right: 5px;
	}
	.select {
	    background: #fff;
	    border-style: solid;
	    border-width: 2px 1px 0;
	    border-color: #08c #ddd #ddd;
	    height: 30px;
	    text-align: center;
	    line-height: 30px;
	    width: 120px;
	    color: #08c;
	    font-weight: 700;
	    cursor: hand;
	}
	.menu {
	    background: #f1f1f1;
	    border-style: solid;
	    border-width: 1px 1px 0;
	    border-color: #ddd;
	    height: 30px;
	    text-align: center;
	    line-height: 30px;
	    width: 120px;
	    cursor: hand;
	}
	.content {
	    border: #ddd 1px solid;
	    height: 360px;
	    width: 96%;
	    background-color: #FFFFFF;
	    margin: 0 0 0 10px;
	}
	.i-hand{cursor: pointer;}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=false\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
</script>
</head>
<body>
<form name="form1" method="post" action="" id="form1">
<input type="hidden" id="de_id" name="de_id" value="${model.de_id}"/>
<input type="hidden" id="de_state" name="de_state" value="1"/>
<div class="border">
	<div class="mutab">
		<ul id="menu_tab">
			<li id="menu1" class="select" onclick="THISPAGE.initDom(1)">交班信息</li>
			<li id="menu2" class="menu" onclick="THISPAGE.initDom(2)" >日结记录</li>
		</ul>
		<span style="color:red;margin-left: 20px;height:30px;font: 12px Microsoft Yahei;" id="errorTip"></span>
	</div>
	<div class="content" id="content1">
		<div style="margin:0 5px 0 5px;">
			<table class="t_add" width="100%">
				<tr>
					<td style="margin-top: 8px;">收银员：${sessionScope.cashier.em_name}</td>
					<td>班&nbsp;&nbsp;次：${sessionScope.cashier.st_name}</td>
					<td>单据数：
						<c:choose>
							<c:when test="${model.de_bills eq  null }">0</c:when>
							<c:otherwise>${model.de_bills}</c:otherwise>
						</c:choose>
						<input type="hidden" id="de_bills" name="de_bills" value="${model.de_bills}"/>
					</td>
					<td>零售单：
						<c:choose>
							<c:when test="${model.de_sell_bills eq  null }">0</c:when>
							<c:otherwise>${model.de_sell_bills}</c:otherwise>
						</c:choose>
						<input type="hidden" id="de_sell_bills" name="de_sell_bills" value="${model.de_sell_bills}"/>
					</td>
					<td>退货单：
						<c:choose>
							<c:when test="${model.de_back_bills eq  null }">0</c:when>
							<c:otherwise>${model.de_back_bills}</c:otherwise>
						</c:choose>
						<input type="hidden" id="de_back_bills" name="de_back_bills" value="${model.de_back_bills}"/>
					</td>
					<td>换货单：
						<c:choose>
							<c:when test="${model.de_change_bills eq  null }">0</c:when>
							<c:otherwise>${model.de_change_bills}</c:otherwise>
						</c:choose>
						<input type="hidden" id="de_change_bills" name="de_change_bills" value="${model.de_change_bills}"/>
					</td>
				</tr>
				<tr>
					<td colspan="6" >
						<hr/>
					</td>
				</tr>
				<tr>
					<td>销售数量：
						<c:choose>
							<c:when test="${model.de_amount eq null}">0</c:when>
							<c:otherwise>${model.de_amount}</c:otherwise>
						</c:choose>
						<input type="hidden" id="de_amount" name="de_amount" value="${model.de_amount}"/>
						<input type="hidden" id="de_sell_money" name="de_sell_money" value="${model.de_sell_money}"/>
					</td>
					<td>调&nbsp;&nbsp;出：
						<c:choose>
							<c:when test="${model.de_out_amount eq null}">0</c:when>
							<c:otherwise>${model.de_out_amount}</c:otherwise>
						</c:choose>
						<input type="hidden" id="de_out_amount" name="de_out_amount" value="${model.de_out_amount}"/>
					</td>
					<%-- <td>上班库存：
						<c:choose>
							<c:when test="${model.de_last_stock eq null}">0</c:when>
							<c:otherwise>${model.de_last_stock}</c:otherwise>
						</c:choose>
					</td> --%>
					<td colspan="4">
						开始时间：${model.de_begindate}
					</td>
				</tr>
				<tr>
					<td>销售金额：
						<c:choose>
							<c:when test="${model.de_money eq null}">0</c:when>
							<c:otherwise>${model.de_money}</c:otherwise>
						</c:choose>
						<input type="hidden" id="de_money" name="de_money" value="${model.de_money}"/>
					</td>
					<td>调&nbsp;&nbsp;入：
						<c:choose>
							<c:when test="${model.de_in_amount eq null}">0</c:when>
							<c:otherwise>${model.de_in_amount}</c:otherwise>
						</c:choose>
						<input type="hidden" id="de_in_amount" name="de_in_amount" value="${model.de_in_amount}"/>
					</td>
					<td colspan="4">
						交班时间：${model.de_enddate}
						<input type="hidden" id="de_enddate" name="de_enddate" value="${model.de_enddate}"/>
					</td>
				</tr>
				<tr>
					<td colspan="6">
						<hr/>
					</td>
				</tr>
			</table>
		</div>
		<div>
			<ul class="ul-inline" style="width:530px;margin-left:5px;">
				<li>
					<span class="span_title">现金：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_cash eq null}">0</c:when>
							<c:otherwise>${model.de_cash}</c:otherwise>
						</c:choose>
						<input type="hidden" id="de_cash" name="de_cash" value="${model.de_cash}"/>
					</span>
				</li>
				<li>
					<span class="span_title">用微支付：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_wx_money eq null}">0</c:when>
							<c:otherwise>${model.de_wx_money}</c:otherwise>
						</c:choose>
						<input type="hidden" id="de_wx_money" name="de_wx_money" value="${model.de_wx_money}"/>
					</span>
				</li>
				<li>
					<span class="span_title">用支付宝：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_ali_money eq null}">0</c:when>
							<c:otherwise>${model.de_ali_money}</c:otherwise>
						</c:choose>
						<input type="hidden" id="de_ali_money" name="de_ali_money" value="${model.de_ali_money}"/>
					</span>
				</li>
				<li>
					<span class="span_title">用储值卡：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_cd_money eq null}">0</c:when>
							<c:otherwise>${model.de_cd_money}</c:otherwise>
						</c:choose>
						<input type="hidden" id="de_cd_money" name="de_cd_money" value="${model.de_cd_money}"/>
					</span>
				</li>
				<li>
					<span class="span_title">用银行卡：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_bank_money eq null}">0</c:when>
							<c:otherwise>${model.de_bank_money}</c:otherwise>
						</c:choose>
						<input type="hidden" id="de_bank_money" name="de_bank_money" value="${model.de_bank_money}"/>
					</span>
				</li>
				<li>
					<span class="span_title">用商场卡：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_mall_money eq null}">0</c:when>
							<c:otherwise>${model.de_mall_money}</c:otherwise>
						</c:choose>
						<input type="hidden" id="de_mall_money" name="de_mall_money" value="${model.de_mall_money}"/>
					</span>
				</li>
				<li>
					<span class="span_title">用优惠券：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_ec_money eq null}">0</c:when>
							<c:otherwise>${model.de_ec_money}</c:otherwise>
						</c:choose>
						<input type="hidden" id="de_ec_money" name="de_ec_money" value="${model.de_ec_money}"/>
					</span>
				</li>
				<li>
					<span class="span_title">用代金券：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_vc_money eq null}">0</c:when>
							<c:otherwise>${model.de_vc_money}</c:otherwise>
						</c:choose>
						<input type="hidden" id="de_vc_money" name="de_vc_money" value="${model.de_vc_money}"/>
					</span>
				</li>
				<li>
					<span class="span_title">用订金：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_sd_money eq null}">0</c:when>
							<c:otherwise>${model.de_sd_money}</c:otherwise>
						</c:choose>
						<input type="hidden" id="de_sd_money" name="de_sd_money" value="${model.de_sd_money}"/>
					</span>
				</li>
				<li>
					<span class="span_title">积分抵现：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_point_money eq null}">0</c:when>
							<c:otherwise>${model.de_point_money}</c:otherwise>
						</c:choose>
						<input type="hidden" id="de_point_money" name="de_point_money" value="${model.de_point_money}"/>
					</span>
				</li>
			</ul>
		</div>
		<div style="float: left;width:523px;margin-left:5px;">
			<hr/>
		</div>
		<div>
			<ul class="ul-inline" style="width:530px;margin-left:5px;">
				<li>
					<span class="span_title">会员办卡：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_vips eq null}">0</c:when>
							<c:otherwise>${model.de_vips}</c:otherwise>
						</c:choose>
						<input type="hidden" id="de_vips" name="de_vips" value="${model.de_vips}"/>
					</span>
				</li>
				<li>
					<span class="span_title">办储值卡：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_cd_sends eq null}">0</c:when>
							<c:otherwise>${model.de_cd_sends}</c:otherwise>
						</c:choose>
						<input type="hidden" id="de_cd_sends" name="de_cd_sends" value="${model.de_cd_sends}"/>
					</span>
				</li>
				<li>
					<span class="span_title">发储值现金：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_cd_send_cash eq null}">0</c:when>
							<c:otherwise>${model.de_cd_send_cash}</c:otherwise>
						</c:choose>
						<input type="hidden" id="de_cd_send_cash" name="de_cd_send_cash" value="${model.de_cd_send_cash}"/>
					</span>
				</li>
				<li>
					<span class="span_title">发储值刷卡：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_cd_send_bank eq null}">0</c:when>
							<c:otherwise>${model.de_cd_send_bank}</c:otherwise>
						</c:choose>
						<input type="hidden" id="de_cd_send_bank" name="de_cd_send_bank" value="${model.de_cd_send_bank}"/>
					</span>
				</li>
				<li>
					<span class="span_title">会员积分：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_vip_point eq null}">0</c:when>
							<c:otherwise>${model.de_vip_point}</c:otherwise>
						</c:choose>
						<input type="hidden" id="de_vip_point" name="de_vip_point" value="${model.de_vip_point}"/>
					</span>
				</li>
				<li>
					<span class="span_title">兑礼品数：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_gifts eq null}">0</c:when>
							<c:otherwise>${model.de_gifts}</c:otherwise>
						</c:choose>
						<input type="hidden" id="de_gifts" name="de_gifts" value="${model.de_gifts}"/>
					</span>
				</li>
				<li>
					<span class="span_title">充储值现金：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_cd_fill_cash eq null}">0</c:when>
							<c:otherwise>${model.de_cd_fill_cash}</c:otherwise>
						</c:choose>
						<input type="hidden" id="de_cd_fill_cash" name="de_cd_fill_cash" value="${model.de_cd_fill_cash}"/>
					</span>
				</li>
				<li>
					<span class="span_title">充储值刷卡：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_cd_fill_bank eq null}">0</c:when>
							<c:otherwise>${model.de_cd_fill_bank}</c:otherwise>
						</c:choose>
						<input type="hidden" id="de_cd_fill_bank" name="de_cd_fill_bank" value="${model.de_cd_fill_bank}"/>
					</span>
				</li>
				<li>
					<span class="span_title">发代金券：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_vc_sends eq null}">0</c:when>
							<c:otherwise>${model.de_vc_sends}</c:otherwise>
						</c:choose>
						<input type="hidden" id="de_vc_sends" name="de_vc_sends" value="${model.de_vc_sends}"/>
					</span>
				</li>
				<li>
					<span class="span_title">发券金额：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_vc_send_money eq null}">0</c:when>
							<c:otherwise>${model.de_vc_send_money}</c:otherwise>
						</c:choose>
						<input type="hidden" id="de_vc_send_money" name="de_vc_send_money" value="${model.de_vc_send_money}"/>
					</span>
				</li>
				<li>
					<span class="span_title">收订金金额：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_sd_send_money eq null}">0</c:when>
							<c:otherwise>${model.de_sd_send_money}</c:otherwise>
						</c:choose>
						<input type="hidden" id="de_sd_send_money" name="de_sd_send_money" value="${model.de_sd_send_money}"/>
					</span>
				</li>
				<li>
					<span class="span_title">退订金金额：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_sd_back_money eq null}">0</c:when>
							<c:otherwise>${model.de_sd_back_money}</c:otherwise>
						</c:choose>
						<input type="hidden" id="de_sd_back_money" name="de_sd_back_money" value="${model.de_sd_back_money}"/>
					</span>
				</li>
			</ul>
		</div>
		<div style="float: left;width:523px;margin-left:5px;">
			<hr/>
		</div>
		<div>
			<ul class="ul-inline" style="width:530px;margin-left:5px;">
				<li>
					<span class="span_title">上班结存：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_last_money eq null}">0</c:when>
							<c:otherwise>${model.de_last_money}</c:otherwise>
						</c:choose>
					</span>
				</li>
				<li>
					<span class="span_title">上班备用：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_petty_cash eq null}">0</c:when>
							<c:otherwise>${model.de_petty_cash}</c:otherwise>
						</c:choose>
					</span>
				</li>
				<li>
					<span class="span_title">抹零金额：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_lost_money eq null}">0</c:when>
							<c:otherwise>${model.de_lost_money}</c:otherwise>
						</c:choose>
						<input type="hidden" id="de_lost_money" name="de_lost_money" value="${model.de_lost_money}"/>
					</span>
				</li>
			</ul>
		</div>
		<div style="float: left;margin:7px 5px 0 5px;">
			<table class="t_add" >
				<tr class="list">
					<td>
						<span class="span_title">合计现金：</span>
						<span class="span_value">
							<c:choose>
								<c:when test="${model.total_cash eq null}">0</c:when>
								<c:otherwise>${model.total_cash}</c:otherwise>
							</c:choose>
							<input type="hidden" id="total_cash" name="total_cash" value="${model.total_cash}"/>
						</span>
					</td>
					<td>
						<span class="span_title">合计刷卡：</span>
						<span class="span_value">
							<c:choose>
								<c:when test="${model.total_bank eq null}">0</c:when>
								<c:otherwise>${model.total_bank}</c:otherwise>
							</c:choose>
							<input type="hidden" id="total_bank" name="total_bank" value="${model.total_bank}"/>
						</span>
					</td>
				</tr>
				<tr class="list">
					<td>
						<span class="span_title">现金帐户：</span>
						<span class="ui-combo-wrap" id="spanCash"></span>
						<input type="hidden" name="de_in_cash" id="de_in_cash"/>
					</td>
					<td>
						<span class="span_title">刷卡帐户：</span>
						<span class="ui-combo-wrap" id="spanBank"></span>
						<input type="hidden" name="de_in_bank" id="de_in_bank"/>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<div class="content grid-wrap" id="content2">
		<table id="grid"></table>
    	<div id="page"></div>
	</div>
</div>
</form>
<div id="div1" style="display:none;">
		<div>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td> 店铺：${sessionScope.cashier.shop_name}</td>
				</tr>
				<tr>
					<td >收银员：${sessionScope.cashier.em_name}&nbsp;&nbsp;班次：${sessionScope.cashier.st_name}</td>
				</tr>
				<tr>
					<td>
						<hr/>
					</td>
				</tr>
				<tr><td>单据数：
						<c:choose>
							<c:when test="${model.de_bills eq  null }">0</c:when>
							<c:otherwise>${model.de_bills}</c:otherwise>
						</c:choose>
					</td>
				</tr>
				<tr>
					<td>零售单：
						<c:choose>
							<c:when test="${model.de_sell_bills eq  null }">0</c:when>
							<c:otherwise>${model.de_sell_bills}</c:otherwise>
						</c:choose>
					</td>
				</tr>
				<tr>
					<td>退货单：
						<c:choose>
							<c:when test="${model.de_back_bills eq  null }">0</c:when>
							<c:otherwise>${model.de_back_bills}</c:otherwise>
						</c:choose>
					</td>
				</tr>
				<tr><td>
						换货单：
						<c:choose>
							<c:when test="${model.de_change_bills eq  null }">0</c:when>
							<c:otherwise>${model.de_change_bills}</c:otherwise>
						</c:choose>
					</td>
				</tr>
				<tr>
					<td>
						<hr/>
					</td>
				</tr>
				<tr><td>销售数量：
						<c:choose>
							<c:when test="${model.de_amount eq null}">0</c:when>
							<c:otherwise>${model.de_amount}</c:otherwise>
						</c:choose>
					</td>
				</tr>
				<tr><td>销售金额：
						<c:choose>
							<c:when test="${model.de_money eq null}">0</c:when>
							<c:otherwise>${model.de_money}</c:otherwise>
						</c:choose>
					</td>
				</tr>
				<tr>
					<td>调&nbsp;&nbsp;出：
						<c:choose>
							<c:when test="${model.de_out_amount eq null}">0</c:when>
							<c:otherwise>${model.de_out_amount}</c:otherwise>
						</c:choose>
					</td>
				</tr>
				<tr>
					<td>调&nbsp;&nbsp;入：
						<c:choose>
							<c:when test="${model.de_in_amount eq null}">0</c:when>
							<c:otherwise>${model.de_in_amount}</c:otherwise>
						</c:choose>
					</td>
				</tr>
				<tr>
					<td>
						<hr/>
					</td>
				</tr>
				<tr>
					<td><span class="span_title">现金：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_cash eq null}">0</c:when>
							<c:otherwise>${model.de_cash}</c:otherwise>
						</c:choose>
					</span>
					</td>
				</tr>
				<tr>
					<td><span class="span_title">用微支付：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_wx_money eq null}">0</c:when>
							<c:otherwise>${model.de_wx_money}</c:otherwise>
						</c:choose>
					</span>
					</td>
				</tr>
				<tr>
					<td><span class="span_title">用支付宝：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_ali_money eq null}">0</c:when>
							<c:otherwise>${model.de_ali_money}</c:otherwise>
						</c:choose>
					</span>
					</td>
				</tr>
				<tr>
					<td><span class="span_title">用储值卡：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_cd_money eq null}">0</c:when>
							<c:otherwise>${model.de_cd_money}</c:otherwise>
						</c:choose>
					</span>
					</td>
				</tr>
				<tr>
					<td><span class="span_title">用银行卡：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_bank_money eq null}">0</c:when>
							<c:otherwise>${model.de_bank_money}</c:otherwise>
						</c:choose>
					</span>
					</td>
				</tr>
				<tr>
					<td><span class="span_title">用商场卡：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_mall_money eq null}">0</c:when>
							<c:otherwise>${model.de_mall_money}</c:otherwise>
						</c:choose>
					</span>
					</td>
				</tr>
				<tr>
					<td><span class="span_title">用优惠券：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_ec_money eq null}">0</c:when>
							<c:otherwise>${model.de_ec_money}</c:otherwise>
						</c:choose>
					</span>
					</td>
				</tr>
				<tr>
					<td><span class="span_title">用代金券：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_vc_money eq null}">0</c:when>
							<c:otherwise>${model.de_vc_money}</c:otherwise>
						</c:choose>
					</span>
					</td>
				</tr>
				<tr>
					<td><span class="span_title">用订金：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_sd_money eq null}">0</c:when>
							<c:otherwise>${model.de_sd_money}</c:otherwise>
						</c:choose>
					</span>
					</td>
				</tr>
				<tr>
					<td><span class="span_title">积分抵现：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_point_money eq null}">0</c:when>
							<c:otherwise>${model.de_point_money}</c:otherwise>
						</c:choose>
					</span>
					</td>
				</tr>
				<tr>
					<td><hr />
					</td>
				</tr>
				<tr>
					<td><span class="span_title">会员办卡：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_vips eq null}">0</c:when>
							<c:otherwise>${model.de_vips}</c:otherwise>
						</c:choose>
					</span>
					</td>
				</tr>
				<tr>
					<td><span class="span_title">办储值卡：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_cd_sends eq null}">0</c:when>
							<c:otherwise>${model.de_cd_sends}</c:otherwise>
						</c:choose>
					</span>
					</td>
				</tr>
				<tr>
					<td><span class="span_title">发储值现金：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_cd_send_cash eq null}">0</c:when>
							<c:otherwise>${model.de_cd_send_cash}</c:otherwise>
						</c:choose>
					</span>
					</td>
				</tr>
				<tr>
					<td><span class="span_title">发储值刷卡：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_cd_send_bank eq null}">0</c:when>
							<c:otherwise>${model.de_cd_send_bank}</c:otherwise>
						</c:choose>
					</span>
					</td>
				</tr>
				<tr>
					<td><span class="span_title">会员积分：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_vip_point eq null}">0</c:when>
							<c:otherwise>${model.de_vip_point}</c:otherwise>
						</c:choose>
					</span>
					</td>
				</tr>
				<tr>
					<td><span class="span_title">兑礼品数：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_gifts eq null}">0</c:when>
							<c:otherwise>${model.de_gifts}</c:otherwise>
						</c:choose>
					</span>
					</td>
				</tr>
				<tr>
					<td><span class="span_title">充储值现金：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_cd_fill_cash eq null}">0</c:when>
							<c:otherwise>${model.de_cd_fill_cash}</c:otherwise>
						</c:choose>
					</span>
					</td>
				</tr>
				<tr>
					<td><span class="span_title">充储值刷卡：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_cd_fill_bank eq null}">0</c:when>
							<c:otherwise>${model.de_cd_fill_bank}</c:otherwise>
						</c:choose>
					</span>
					</td>
				</tr>
				<tr>
					<td><span class="span_title">发代金券：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_vc_sends eq null}">0</c:when>
							<c:otherwise>${model.de_vc_sends}</c:otherwise>
						</c:choose>
					</span>
					</td>
				</tr>
				<tr>
					<td><span class="span_title">发券金额：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_vc_send_money eq null}">0</c:when>
							<c:otherwise>${model.de_vc_send_money}</c:otherwise>
						</c:choose>
					</span>
					</td>
				</tr>
				<tr>
					<td><span class="span_title">收订金金额：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_sd_send_money eq null}">0</c:when>
							<c:otherwise>${model.de_sd_send_money}</c:otherwise>
						</c:choose>
					</span>
					</td>
				</tr>
				<tr>
					<td><span class="span_title">退订金金额：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_sd_back_money eq null}">0</c:when>
							<c:otherwise>${model.de_sd_back_money}</c:otherwise>
						</c:choose>
					</span>
					</td>
				</tr>
				<tr>
					<td><hr />
					</td>
				</tr>
				<tr>
					<td><span class="span_title">上班结存：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_last_money eq null}">0</c:when>
							<c:otherwise>${model.de_last_money}</c:otherwise>
						</c:choose>
					</span>
					</td>
				</tr>
				<tr>
					<td><span class="span_title">上班备用：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_petty_cash eq null}">0</c:when>
							<c:otherwise>${model.de_petty_cash}</c:otherwise>
						</c:choose>
					</span>
					</td>
				</tr>
				<tr>
					<td><span class="span_title">抹零金额：</span>
					<span class="span_value">
						<c:choose>
							<c:when test="${model.de_lost_money eq null}">0</c:when>
							<c:otherwise>${model.de_lost_money}</c:otherwise>
						</c:choose>
					</span>
					</td>
				</tr>
				<tr>
					<td><hr />
					</td>
				</tr>
				<tr>
					<td><span class="span_title">合计现金：</span>
						<span class="span_value">
							<c:choose>
								<c:when test="${model.total_cash eq null}">0</c:when>
								<c:otherwise>${model.total_cash}</c:otherwise>
							</c:choose>
						</span>
					</td>
				</tr>
				<tr>
					<td><span class="span_title">合计刷卡：</span>
						<span class="span_value">
							<c:choose>
								<c:when test="${model.total_bank eq null}">0</c:when>
								<c:otherwise>${model.total_bank}</c:otherwise>
							</c:choose>
						</span>
					</td>
				</tr>
				<tr><td>
						交班时间：${model.de_enddate}
					</td>
				</tr>
			</table>
		</div>
	</div>


<div class="footdiv">
	<input class="btn_confirm" type="button" id="btn-save" name="btn-save" value="交班"/>
	<input class="btn_close" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
<script src="<%=printPath%>/CLodopfuncs.js"></script> 
<script src="<%=basePath%>data/day/end.js?v=${time}"></script>
</body>
</html>