<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
</script> 
</head>
<body>
<table class="table_add" width="100%" style="padding:10px;">
	<tr >
		<td align="right" >卡号：</td>
		<td align="left">
		 	<input type="text" class="main_Input w146" id="vm_cardcode" name="vm_cardcode" value="${model.vm_cardcode}"/>
		</td>
	</tr>
	<tr >
		<td align="right" >姓名：</td>
		<td align="left">
		 	<input type="text" class="main_Input w146" id="vm_name" name="vm_name" value="${model.vm_name}"/>
		</td>
	</tr>
	<tr >
		<td align="right" >积分：</td>
		<td align="left">
		 	<input type="text" class="main_Input w146" id="vm_points" name="vm_points" value="${model.vm_points}"/>
		</td>
	</tr>
	<tr >
		<td align="right" >规则：</td>
		<td align="left">
		 	<b>${model.sh_sell_money}分抵一元</b>
		 	<input type="hidden" id="point_money" name="point_money" value="${model.sh_sell_money}"/>
		</td>
	</tr>
	<tr >
		<td align="right" >使用：</td>
		<td align="left">
		 	<input type="text" class="main_Input w146" id="vip_point" name="vip_point" value=""/>
		</td>
	</tr>
	<tr >
		<td align="right" >抵现：</td>
		<td align="left">
		 	<input type="text" class="main_Input w146" id="sh_point_money" name="sh_point_money" value=""/>
		</td>
	</tr>
	<tr >
		<td align="right" >剩余：</td>
		<td align="left">
		 	<input type="text" class="main_Input w146" id="vm_last_point" name="vm_last_point" value=""/>
		</td>
	</tr>
</table>
<div class="footdiv">
	<a id="ischeck" class="fl">
		<label class="chk">
			<input type="checkbox"/>打印
		</label>
	</a>
	<input class="btn_confirm" type="button" id="btn-save" name="btn-save" value="确定"/>
	<input class="btn_close" type="button" id="btn_close" name="btn_close" value="关闭"/>
</div>
<script src="<%=basePath%>data/cash/vip_point.js?v=${time}"></script>
</body>
</html>