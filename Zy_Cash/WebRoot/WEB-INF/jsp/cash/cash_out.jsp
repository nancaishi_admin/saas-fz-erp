<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/css/iconfont/iconfont.css" type="text/css" rel="stylesheet" />
<style>
.span_title{width: 70px;padding: 11px 16px;}
.text_icon{width:117px;background:#f2f2f2;}
.ul-inline li{margin-top:7px;}
.more_height{height:30px;line-height: 30px;}
.more_width{width:158px;}
.a_select{
	height:42px;
	width:40px;
	font-size:24px;
	vertical-align: middle;
	text-align: center;
	line-height:42px;
    border: 1px solid #DDD;
    border-right-width:0px;
    background-color:#f2f2f2;
    color: #555;
    cursor: pointer;
    display: inline-block;
    float: left;
}
.bg_red {color: #eb4f38;}
.bg_green {color: #1b96a9;}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
</script> 
</head>
<body>
<form name="form1" id="form1" method="post" action="">
<input type="hidden" id="sell_type" name="sell_type" value=""/>
<div class="con" >
	<ul class="ul-inline" style="width:560px;">
		<li>
       		<span class="span_title"><i class="iconfont bg_red">&#xe62a;</i>金额：</span>
       		<input type="text" class="main_Input more_width " style="border:0;font-weight:700;line-height:40px;height:40px;" id="sell_money" name="sell_money" value="0.0"/>
       	</li>
       	<li>
       		<span class="span_title"><i class="iconfont bg_green">&#xe64f;</i>找零：</span>
	       	<input type="text" class="main_Input more_width " style="border:0;font-weight:700;line-height:40px;height:40px;" id="change_money" name="change_money" value="0.0"/>
       	</li>
  		<li>
  			<span class="span_title">抹&nbsp;&nbsp;零：</span>
  			<input type="text" maxlength="8" class="main_Input more_height more_width" id="sh_lost_money" name="sh_lost_money" value="0" 
  				onmouseup="this.select();" onkeyup="javascript:THISPAGE.buildCash();if(event.keyCode==13){$('#sh_cash').select().focus();}"/>
  		</li>
  		<li>
        	<span class="span_title">现&nbsp;&nbsp;金：</span>
        	<input type="text" id="sh_cash" class="main_Input more_height more_width" name="sh_cash" value="" 
        		onkeyup="javascript:THISPAGE.changeMoney();if(event.keyCode==13){$('#sh_bank_money').select().focus();};" 
        		onmouseup="this.select();"/>
       	</li>
       	<li>
  			<span class="span_title">支付宝：</span>
  			<input type="text" class="main_Input w146 more_height more_width" id="sh_ali_money" name="sh_ali_money" value="0"
  			onkeyup="javascript:if(handle.vadiDouble(this)){THISPAGE.buildCash();}if(event.keyCode==13){$('#sh_mall_money').select().focus();}" 
    			onmouseup="this.select();" onblur="if(this.value=='')this.value=0"/>
  		</li>
  		<li>
  			<span class="span_title">微支付：</span>
  			<input type="text" class="main_Input w146 more_height more_width" id="sh_wx_money" name="sh_wx_money" value="0"
  			onkeyup="javascript:if(handle.vadiDouble(this)){THISPAGE.buildCash();}if(event.keyCode==13){$('#sh_mall_money').select().focus();}" 
    			onmouseup="this.select();" onblur="if(this.value=='')this.value=0"/>
  		</li>
   		<li>
    		<span class="span_title">银行卡：</span>
    		<input type="text" maxlength="8" class="main_Input more_height more_width" id="sh_bank_money" name="sh_bank_money" value="0" 
    			onkeyup="javascript:if(handle.vadiDouble(this)){THISPAGE.buildCash();}if(event.keyCode==13){$('#sh_mall_money').select().focus();}" 
    			onmouseup="this.select();" onblur="if(this.value=='')this.value=0"/>
   		</li>
  		<li>
  			<span class="span_title">商场卡：</span>
  			<input type="text" maxlength="8" class="main_Input more_height more_width" id="sh_mall_money" name="sh_mall_money" value="0" 
				onmouseup="this.select();" onblur="if(this.value=='')this.value=0" onkeyup="javascript:if(handle.vadiDouble(this)){THISPAGE.buildCash();}"/>
  		</li>
  		<li>
  			<span class="span_title">收钱吧：</span>
  			<a class="iconfont a_select" id="use_third">&#xe64f;</a>
   			<input type="text" class="main_Input text_icon more_height" id="sh_third_money" name="sh_third_money" value="0" readonly="readonly"/>
  		</li>
   		<li>
   			<span class="span_title">代金券：</span>
   			<a class="iconfont a_select" id="use_voucher">&#xe600;</a>
   			<input type="text" class="main_Input text_icon more_height" id="sh_vc_money" name="sh_vc_money" value="0" readonly="readonly"/>
   			<input type="hidden" name="vc_money" id="vc_money" value=""/>
			<input type="hidden" name="vc_ids" id="vc_ids" value=""/>
			<input type="hidden" name="vc_rate" id="vc_rate" value=""/>
   		</li>
   		<%-- <li>
    		<span class="span_title">电子券：</span>
    		<input type="text" class="main_Input text_icon more_height" id="sh_ec_money" name="sh_ec_money" value="0" readonly="readonly"/>
    		<a class="iconfont a_select" id="use_ecoupon">&#xe61d;</a>
    		<input type="hidden" id="ecu_flag" name="ecu_flag" value="${data.ecu_flag}"/>
    		<input type="hidden" name="ec_money" id="ec_money" value=""/>
			<input type="hidden" name="ec_codes" id="ec_codes" value=""/>
			<input type="hidden" name="ecu_name" id="ecu_name" value=""/>
			<input type="hidden" name="ecu_tel" id="ecu_tel" value=""/>
   		</li> --%>
   		<li>
	    	<span class="span_title">储值卡：</span>
	    	<input type="text" class="main_Input text_icon more_height" id="sh_cd_money" name="sh_cd_money" value="0" readonly="readonly"/>
	    	<a class="iconfont a_select" id="use_card">&#xe605;</a>
	    	<input type="hidden" name="cd_money" id="cd_money" value=""/>
			<input type="hidden" name="cd_ids" id="cd_ids" value=""/>
			<input type="hidden" name="cd_rate" id="cd_rate" value=""/>
	    </li>
	    <li>
   			<span class="span_title">优惠券：</span>
   			<input type="text" class="main_Input text_icon more_height" id="sh_ec_money" name="sh_ec_money" value="0" readonly="readonly"/>
   			<a class="iconfont a_select" id="use_ecoupon">&#xe624;</a>
   			<input type="hidden" id="ecu_flag" name="ecu_flag" value="${data.ecu_flag}"/>
			<input type="hidden" name="ecu_id" id="ecu_id" value=""/>
			<input type="hidden" name="ecu_name" id="ecu_name" value=""/>
			<input type="hidden" name="ecu_tel" id="ecu_tel" value=""/>
   		</li>
  		<li>
  			<span class="span_title">积&nbsp;&nbsp;分：</span>
  			<input type="text" class="main_Input text_icon more_height " id="sh_point_money" name="sh_point_money" value="0" readonly="readonly"/>
  			<a class="iconfont a_select" id="use_vip">&#xe6b0;</a>
  			<input type="hidden" name="vip_point" id="vip_point" value=""/>
  		</li>
  		<li>
			<span class="span_title">订&nbsp;&nbsp;金：</span>
			<input type="text" class="main_Input more_height more_width" style="background:#f2f2f2;" id="sh_sd_money" name="sh_sd_money" value="0" readonly="readonly"/>
			<input type="hidden" name="sd_number" id="sd_number" value=""/>
        </li>
  		<li>
			<span class="span_title">备&nbsp;&nbsp;注：</span>
			<input type="text" id="sh_remark" name="sh_remark" class="main_Input more_height more_width"/>
  		</li>
	</ul>
</div>
</form>
<div class="footdiv">
	<input class="btn_confirm" type="button" id="btn-save" name="btn-save" value="确定"/>
	<input class="btn_close" type="button" id="btn_close" name="btn_close" value="关闭"/>
</div>
<script src="<%=printPath%>/CLodopfuncs.js"></script>
<script src="<%=basePath%>data/cash/cash_out.js?v=${time}"></script>
</body>
</html>