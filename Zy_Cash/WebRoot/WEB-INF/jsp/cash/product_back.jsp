<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=false&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
</script> 
</head>
<body id="main_content">
<div class="wrapper">
	<div class="mod-search cf">
	   <div class="fl">
			<ul class="ul-inline">
				<li>货号：
					<input type="text" id="pd_no" name="pd_no" class="main_Input w146" value=""/>
					<input type="text" style="display:none;"/>
				</li>
			</ul>
		</div>
	    <div class="fl-m">
	    	<a class="ui-btn mrb" id="btn-search" name="btn-search">查询</a>
	    	<a href="javascript:void(0);" class="ui-btn ui-btn-sp mrb" id="btn-add" >图片</a>
	    </div> 
    	<div class="fl-m">
    		<span style="float:left;line-height:30px">
				<input type="checkbox" name="querys" value="" checked="checked" onclick="javascript:if(this.checked){$('#isquery').val(1);}else{$('#isquery').val(0);};"/><b>模糊查询</b>&nbsp;&nbsp;
				<input type="hidden" id="isquery" name="isquery" value="1" />
			</span>
			<span style="float:left;line-height:30px">
				<input type="checkbox" id="all_chk"  name="all_chk" onclick="javascript:if(this.checked){$('#isall').val(1);}else{$('#isall').val(0);};;"/><b>全部显示</b>
				<input type="hidden" id="isall"  name="isall" value="0" />
			</span>
    	</div>
	 </div>
	<div class="grid-wrap">
	    <table id="grid">
	    </table>
    	<div id="page"></div>
	</div>
</div>
<div class="footdiv">
	<input class="ui-btn ui-btn-sp mrb" id="sumbit_ok" name="sumbit_ok" type="button" value="确定"/>
	<input class="ui-btn mrb" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
<script src="<%=basePath%>data/cash/product_back.js?v=${time}"></script>
</body>
</html>