<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=false&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
</script> 
</head>
<body id="main_content">
<form action="" method="post">
<div class="wrapper">
	<div class="mod-search cf ml10">
		<div class="fl" style="width:auto;">
			<ul class="ul-inline">
				<li>
					<table class="searchbar">
						<tr>
							<td>货号:
								<input type="text" id="pd_no" name="pd_no" class="main_Input w146" value=""/>
							</td>
							<td width="120">尺码:
								<span class="ui-combo-wrap main_combo" id="spanSize"></span>
								<input type="hidden" name="sz_code"  id="sz_code" value=""/>
							</td>
							<td width="120">杯型:
								<span class="ui-combo-wrap main_combo" id="spanBra"></span>
								<input type="hidden" name="br_code"  id="br_code" value=""/>
							</td>
							<td width="200">仓库:
								<input type="text" id="dp_names" name="dp_names" class="main_Input atfer_select" style="width:120px" readonly="readonly" value=""/>
								<input type="button" value="" name="btn_depot" id="btn_depot" class="btn_select" />
							</td>
						</tr>
					</table>
				</li>
			</ul>
		</div>
		<div class="fl-m">
			<a class="ui-btn mrb" id="btn_search">查询</a>
			<a class="ui-btn mrb" id="btn_reset">重置</a>
		</div>
	</div>
	<div class="grid-wrap">
	    <table id="grid">
	    </table>
	    <div id="page"></div>
	</div> 	
</div>
</form>
<script src="<%=basePath%>data/stock/other.js"></script>
</body>
</html>