<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<link href="<%=basePath%>resources/css/iconfont/iconfont.css" type="text/css" rel="stylesheet" />
<link href="<%=basePath%>resources/grid/css/common.css?v=${time}" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css?v=${time}" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/css/main.css?v=${time}" type="text/css" rel="stylesheet" />
<link href="<%=basePath%>resources/css/menu.css?v=${time}" type="text/css" rel="stylesheet" />
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/cash.grid.celledit.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
</script> 
<script type="text/javascript">
function doLogout(){
	$.dialog.confirm('确定退出吗?', function(){
		window.location.replace("<%=basePath%>logout");
	});
}
/**系统配置*/
var CONFIG={
	BASEPATH:'<%=basePath%>',						//项目路径
	SERVERPATH:'<%=serverPath%>',					//项目路径
	TODAY:'${requestScope.today}',
	PRINT_NAME:'\u53a6\u95e8\u667a\u6167\u6570\u7f51\u7edc\u79d1\u6280\u6709\u9650\u516c\u53f8',
	PRINT_KEY:'2CCC74198DEF8B1D8D08C1CF3F7E04F2',
	REG_DOUBLE:/^(-)?[0-9]+\.{0,1}[0-9]{0,2}$/,/*/^[-\+]?\d+(\.\d+)?$/,*/
	BASEROWNUM:15,									//基本资料分页条数
	BASEROWLIST:[15,30,50],							//基本资料分页条数
	DATAROWNUM:100,									//单据分页条数
	DATAROWLIST:[100,200,300],						//单据分页条数
	OPTIONLIMIT:{PRINT:'P',INSERT:'I',DELETE:'D',UPDATE:'U',SELECT:'S',EXPORT:'E',APPROVE:'A',FINISH:'F',IMPORT:'R',BACK:'B'}
};
/**系统参数*/
var SYSTEM={
	SHOP_CODE:'${sessionScope.cashier.ca_shop_code}',	
	SHOP_NAME:'${sessionScope.cashier.shop_name}',
	SHOP_END:'${sessionScope.cashier.shop_end}',
	ST_CODE:'${sessionScope.cashier.st_code}',
	DP_CODE:'${sessionScope.cashier.dp_code}',
	DP_NAME:'${sessionScope.cashier.dp_name}',
	EM_CODE:'${sessionScope.cashier.ca_em_code}',
	EM_NAME:'${sessionScope.cashier.em_name}',
	MAXMONEY:'${sessionScope.cashier.ca_maxmoney}',//最大让利金额
	MIN_RATE:'${sessionScope.cashier.ca_minrate}',//最小折扣
	ERASE:'${sessionScope.cashier.ca_erase}',//最大抹零
	DAYS:'${sessionScope.cashier.ca_days}',//零售查询天数
//	SHOWSALVE:'${sessionScope.cashier_set.KEY_SHOW_SALVE}',
//	SHOWAREA:'${sessionScope.cashier_set.KEY_SHOW_AREA}',
	CASHOUT:'${sessionScope.cashier_set.KEY_CASH}',
	HAND_RATE:'${sessionScope.cashier_set.KEY_HAND_RATE}',
	DAYEND:'${sessionScope.cashier_set.KEY_DAYEND}',
	ISBACK:'${sessionScope.cashier_set.KEY_BACK}',
	ISCHANGE:'${sessionScope.cashier_set.KEY_CHANGE}',
	BACKNUMBER:'${sessionScope.cashier_set.KEY_BACK_NUMBER}',
	VIP_QUERY:'${sessionScope.cashier_set.KEY_VIP_MANAGE}',
	VIP_ADD:'${sessionScope.cashier_set.KEY_VIP_ADD}',
	VIP_EDIT:'${sessionScope.cashier_set.KEY_VIP_EDIT}',
	EDIT_PASS:'${sessionScope.cashier_set.KEY_EDIT_PASS}',
	VOUCHER_QUERY:'${sessionScope.cashier_set.KEY_VOUCHER_QUERY}',
	VOUCHER_ADD:'${sessionScope.cashier_set.KEY_VOUCHER_ADD}',
	VIP_POINT:'${sessionScope.cashier_set.KEY_VIP_POINT}',
	CARD_QUERY:'${sessionScope.cashier_set.KEY_CARD_QUERY}',
	CARD_ADD:'${sessionScope.cashier_set.KEY_CARD_ADD}',
	CARD_CHARGE:'${sessionScope.cashier_set.KEY_CARD_CHARGE}',
	SELECT_ALL:'${sessionScope.cashier_set.KEY_SELECT_ALL}',
	RATE_TYPE:'${sessionScope.cashier_set.KEY_RATE_TYPE}',
	EMP_MAIN:'${sessionScope.cashier_set.KEY_EMP_MAIN}',
	EMP_SALVE:'${sessionScope.cashier_set.KEY_EMP_SALVE}',
	AREA:'${sessionScope.cashier_set.KEY_AREA}',
	AUTO_PRINT:'${sessionScope.cashier_set.print.sp_auto}',
	PRINT:'${sessionScope.cashier_set.print.sp_print}',
	REECOUPON:'${sessionScope.cashier_set.KEY_REECOUPON}'
};
</script>
<title>智慧数收银平台</title>
</head>
<body>
<input type="hidden" id="da_id" name="da_id" value=""/>
<input type="hidden" id="sd_number" name="sd_number" value=""/>
<input type="hidden" id="sd_deposit" name="sd_deposit" value=""/>
<input type="hidden" id="vm_name" name="vm_name" value="${sessionScope.vip_info.vm_name}"/>
<input type="hidden" id="vm_cardcode" name="vm_cardcode" value="${sessionScope.vip_info.vm_cardcode} "/>
<input type="hidden" id="vm_points" name="vm_points" value="${sessionScope.vip_info.vm_points}"/>
<input type="hidden" id="vm_mobile" name="vm_mobile" value="${sessionScope.vip_info.vm_mobile}"/>
<input type="hidden" id="vip_info" name="vip_info" value=""/>
<input type="hidden" id="vip_id" name="vip_id" value="${sessionScope.vip_info.vm_id}"/>
<input type="hidden" id="sh_id" name="sh_id" value=""/>
<input type="hidden" id="change_money" name="change_money" value="0"/>
<div id="header">
  	<div class="logo fl">
  		<span><i></i><b></b>${sessionScope.cashier.shop_name}(剩余<i id='end_days'></i>天)</span>
  	</div>
  	<ul class="head_icon fr">
  		<li>
	    	<a class="icon_menu" id="dayend">
				<i class="iconfont">&#xe6b0;</i>
				<span >交班</span>
			</a>
	    </li>
	    <li>
	    	<a class="icon_menu" id="btn_allocate">
				<i class="iconfont">&#xe650;</i>
				<span >调拨</span>
			</a>
	    </li>
  		<li>
	    	<a class="icon_menu" id="btn_weather">
				<i class="iconfont">&#xe64e;</i>
				<span >天气</span>
			</a>
	    </li>
	    <li>
	    	<a class="icon_menu" id="btn_come">
				<i class="iconfont">&#xe709;</i>
				<span >进店<b id="da_come">0</b></span>
			</a>
	    </li>
	    <li>
	    	<a href="javascript:void(0);" id="btn_receive">
	    		<i class="iconfont">&#xe657;</i>
	    		<span>接待<b id="da_receive">0</b></span>
	    	</a>
	    </li> 
	    <li>
			<a href="javascript:void(0);" id="btn_try">
	    		<i class="iconfont">&#xe651;</i>
	    		<span>试穿<b id="da_try">0</b></span>
	    	</a>
		</li> 
		<li>
	    	<ul id="jsddm" class="menu_set">
				<li>
					<a href="javascript:void(0);" >
			    		<i class="iconfont">&#xe918;</i>
			    		<span style="padding-top:8px;" id="allMsgCount">工作台<b></b></span>
			    	</a>
					<ul>
						<li><a href="javascript:void(0);" id="btn_want_receive">收货管理(<em></em>)</a></li>
						<li><a href="javascript:void(0);">线上销售</a></li>
						<li><a href="javascript:void(0);">会员生日</a></li>
						<li><a href="javascript:void(0);" id="shop_service">维修管理</a></li>
						<li><a href="javascript:void(0);" id="shop_program">搭配方案</a></li>
					</ul>
				</li>	
			</ul>
	    </li> 
	    <li>
	    	<ul id="jsddm" class="menu_set">
				<li>
					<a href="javascript:void(0);" >
			    		<i class="iconfont">&#xe60a;</i>
			    		<span style="padding-top:8px;">设置</span>
			    	</a>
					<ul>
						<li><a href="javascript:void(0);" id="btn_print_set">打印设置</a></li>
						<li><a href="javascript:void(0);" id="btn_cash_set">收银设置</a></li>
						<li><a href="javascript:void(0);" id="btn_sms_set">短信设置</a></li>
						<li><a href="javascript:void(0);" id="btn_allocate_set">调拨设置</a></li>
					</ul>
				</li>	
			</ul>
	    </li>
	    <li>
	    	<ul id="jsddm" class="menu_set">
				<li>
					<a href="javascript:void(0);" >
			    		<i class="iconfont">&#xe631;</i>
			    		<span style="padding-top:8px;">工具</span>
			    	</a>
					<ul>
						<li><a href="javascript:void(0);" id="btn_shop_plan">智能计划</a></li>
						<li><a href="javascript:void(0);" id="btn_shop_target">目标管理</a></li>
						<li><a href="javascript:void(0);" id="btn_shop_kpiassess">KPI考核</a></li>
						<li><a href="javascript:void(0);" id="btn_shop_kpipk">PK工具</a></li>
					</ul>
				</li>	
			</ul>
	    </li>  
	    <li>
	    	<a href="javascript:void(0);" id="edit_pass">
	    		<i class="iconfont">&#xe60c;</i>
	    		<span>改密码</span>
	    	</a>
	    </li>
	    <li>
	    	<a href="javascript:void(0);" onclick="javascript:doLogout();">
	    		<i class="iconfont">&#xe63e;</i>
	    		<span>退出</span>
	    	</a>
	    </li>
  	</ul>
</div>
<div class="grid-wrap fl">
    <table id="grid">
    </table>
   	<div id="page"></div>
</div>
<div class="fr clearfix">
	<div class="sell_icon">
		<ul class="">
			<li>
				<a class="icon_menu" id="vip_rate">
					<i class="iconfont bg_green">&#xe605;</i>
					<span >会员折扣</span>
				</a>
			</li>
			<li>
				<a class="icon_menu" id="vip_manage">
					<i class="iconfont bg_green">&#xe636;</i>
					<span >会员管理</span>
				</a>
			</li>
			<li>
				<a class="icon_menu" id="gift_point">
					<i class="iconfont bg_green">&#xe6e1;</i>
					<span >积分兑换</span>
				</a>
			</li>
			<li>
				<a class="icon_menu" id="hand_rate">
					<i class="iconfont bg_green">&#xe651;</i>
					<span >手动打折</span>
				</a>
			</li>
			<li>
				<a class="icon_menu" id="sell_list">
					<i class="iconfont bg_green">&#xe62d;</i>
					<span >零售记录</span>
				</a>
			</li>
			<li>
				<a class="icon_menu">
					<i class="iconfont bg_green">&#xe637;</i>
					<span >礼品发放</span>
				</a>
			</li>
			<li>
				<a class="icon_menu" id="all_rate">
					<i class="iconfont bg_green">&#xe911;</i>
					<span >整单折扣</span>
				</a>
			</li>
			<li>
				<a class="icon_menu" id="other_stock">
					<i class="iconfont bg_green">&#xe61b;</i>
					<span >商品库存</span><!-- 各店 -->
				</a>
			</li>
			<!--
			有个查询库存的地方就行了 
			<li>
				<a class="icon_menu" id="product_stock">
					<i class="iconfont bg_green">&#xe716;</i>
					<span >商品库存</span>
				</a>
			</li> -->
			<li>
				<a class="icon_menu" id="money_access">
					<i class="iconfont bg_green">&#xe64f;</i>
					<span >银行收支</span>
				</a>
			</li>
			<li>
				<a class="icon_menu" id="voucher_manage">
					<i class="iconfont bg_green">&#xe600;</i>
					<span >代金券管理</span>
				</a>
			</li>
			<li>
				<a class="icon_menu" id="card_manage">
					<i class="iconfont bg_green">&#xe61d;</i>
					<span >储值卡管理</span>
				</a>
			</li>
			<li>
				<a class="icon_menu" id="vip_point">
					<i class="iconfont bg_green">&#xe6b0;</i>
					<span >积分管理</span>
				</a>
			</li>
			<!-- <li>
				<a class="icon_menu" id="shop_service">
					<i class="iconfont bg_green">&#xe643;</i>
					<span >维修服务</span>
				</a>
			</li> -->
		</ul>
	</div>
	<div id="sell_cash">
		<span class="span_cash">金额：<b id="total_money">0.00</b></span>
		<span><a id="btn-cash" class="ui-btn ui-btn-green" name="btn-cash">结&nbsp;&nbsp;算</a></span>
	</div>
</div>
<div id="bottom">
	<div class="btn_type" style="width: 160px;height: 40px;">
		<span id="sell_btn" style="float:right;margin-right: 18px;">
			<a id="btn_sell" name="btn_sell" class="sell btn-sell in"><i class="iconfont">&#xe619;</i>售</a>
		    <a id="btn-back" name="btn-back" class="sell btn-back"><i class="iconfont">&#xe623;</i>退</a>
		    <a id="btn-change" name="btn-change" class="sell btn-change"><i class="iconfont">&#xe6b0;</i>换</a>
        </span>
		<input type="hidden" name="sell_type"  id="sell_type" value="0"/>
	</div>
	<div class="c_input">
		<input class="main_Input w146" type="text" name="pd_no" id="pd_no" value="" onmouseup="this.select();"/>
		<input type="text" style="display:none;"/>
	</div>
	<div class="c_input">
		<a id="btn-search" style="height: 29px;" class="ui-btn mrb" name="btn-search">查询</a>
	</div>
	<div class="btn_type">
		<a id="open_type">
			<label class="chk">
				<input type="checkbox"/>尺码横排
			</label>
		</a>
	</div>
	<div class="c_option">
		<a style="height:29px;" class="ui-btn mrb ui-btn-green" id="btn-clear" name="btn-clear">清空</a>
		<a style="height:29px;" class="ui-btn ui-btn-red" id="btn-mobile" name="btn-mobile">手机下单</a>
		<a style="height:29px;" class="ui-btn ui-btn-green" id="btn-pay" name="btn-pay">付订金</a>
		<a style="height:29px;" class="ui-btn ui-btn-red" id="btn-get" name="btn-get">取订金</a>
	</div>
	<div class="c_putup">
		<a class="icon_menu">
			<i class="iconfont bg_white putup" code="" id="putup_1">&#xe716;</i>
		</a>
		<a class="icon_menu">
			<i class="iconfont bg_white putup" code="" id="putup_2">&#xe716;</i>
		</a>
		<a class="icon_menu">
			<i class="iconfont bg_white putup" code="" id="putup_3">&#xe716;</i>
		</a>
		<a class="icon_menu">
			<i class="iconfont bg_white putup" code="" id="putup_4">&#xe716;</i>
		</a>
		<a class="icon_menu">
			<i class="iconfont bg_white putup" code="" id="putup_5">&#xe716;</i>
		</a>
	</div>
	<div class="c_input" style="margin-top:10px;">
		<a class="color-fff">VIP：</a>
		<input class="main_Input w146" type="text" name="mobile" id="mobile" value="" onmouseup="this.select();"/>
		<input type="text" style="display:none;"/>
	</div>
	<span id="vip_detail" style="margin-top:13px;float: left;margin-left:3px; "><a class="color-fff" id="vip_data"></a>  </br><a  class="color-fff" id="vip_card"></a></span>
</div>
<script type="text/javascript">
var timeout         = 300;
var closetimer		= 0;
var ddmenuitem      = 0;
function jsddm_open() {
	jsddm_canceltimer();
	jsddm_close();
	ddmenuitem = $(this).find('ul').eq(0).css('visibility', 'visible');
}
function jsddm_close() {
	if (ddmenuitem)
		ddmenuitem.css('visibility', 'hidden');
}
function jsddm_timer() {
	closetimer = window.setTimeout(jsddm_close, timeout);
}
function jsddm_canceltimer() {
	if (closetimer) {
		window.clearTimeout(closetimer);
		closetimer = null;
	}
}
$(document).ready(function() {
	$('.menu_set > li').bind('mouseover', jsddm_open);
	$('.menu_set > li').bind('mouseout', jsddm_timer);
	
	var sdate = new Date(SYSTEM.SHOP_END.replace(/-/g, "/")); 
	var now = new Date(); 
	var days = sdate.getTime() - now.getTime(); 
	var day = parseInt(days / (1000 * 60 * 60 * 24));
	if(day > 0){
		$("#end_days").text(day);
	}else{
		$("#end_days").text(0);
	}
});
document.onclick = jsddm_close;
</script>
<script type="text/javascript" src="<%=basePath%>data/index/main.js?v=${time}"></script>
<script src="<%=printPath%>/CLodopfuncs.js"></script> 
</body>
</html>