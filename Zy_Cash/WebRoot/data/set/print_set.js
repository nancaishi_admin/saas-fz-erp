var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var api = frameElement.api, W = api.opener;
var pt_id = 17;
var handle = {
	formatPosition:function(val, opt, row){
		return row.spf_position=="0"?"表头":"表尾";
	},
	formatFieldShow:function(val, opt, row){
		var html = '';
		html += '<input type="checkbox" '+(row.spf_show=="1"?"checked":"")+' onclick="javascript:$(this).next().val(this.checked?1:0);"/>';
		html += '<input type="hidden" id="fieldShow_'+row.spf_id+'" value="'+row.spf_show+'"/>';
		return html;
	},
	formatDataShow:function(val, opt, row){
		var html = '';
		html += '<input type="checkbox" '+(row.spd_show=="1"?"checked":"")+' onclick="javascript:$(this).next().val(this.checked?1:0);"/>';
		html += '<input type="hidden" id="dataShow_'+row.spd_id+'" value="'+row.spd_show+'"/>';
		return html;
	},
	formatDataAlign:function(val, opt, row){
		if ($.trim(row.spd_align) == '') {
			return '';
		}
		var html = '';
		html += "<input onclick='javascript:$(this).siblings(\"input:last\").val($(this).val());' type='radio' name='radio"+row.spd_id+"' "+(row.spd_align=="0"?"checked":"")+" value='0'/>左";
		html += "<input onclick='javascript:$(this).siblings(\"input:last\").val($(this).val());' type='radio' name='radio"+row.spd_id+"' "+(row.spd_align=="1"?"checked":"")+" value='1'/>中";
		html += "<input onclick='javascript:$(this).siblings(\"input:last\").val($(this).val());' type='radio' name='radio"+row.spd_id+"' "+(row.spd_align=="2"?"checked":"")+" value='2'/>右";
		html += '<input type="hidden" id="dataAlign_'+row.spd_id+'" value="'+row.spd_align+'"/>';
		return html;
	},
	buildPrint:function(){
		var print = {};
		print.sp_id = $("#sp_id").val();
		print.sp_code = $("#sp_code").val();
		print.sp_remark = $("#sp_remark").val();
		print.sp_title = $("#sp_title").val();
		print.sp_qrcode = $("#sp_qrcode").val();
		print.sp_print = THISPAGE.$_isprint.chkVal().join()? 1 : 0;
		print.sp_auto = THISPAGE.$_autoprint.chkVal().join()? 1 : 0;
		return print;
	},
	buildPrintSet:function(){
		var printSets = [];
		printSets.push({sps_key:'page_width',sps_value:$('#page_width').val()});
		printSets.push({sps_key:'page_height',sps_value:$('#page_height').val()});
		printSets.push({sps_key:'table_subtotal',sps_value:$('#table_subtotal').is(':checked')?"1":"0"});
		printSets.push({sps_key:'table_total',sps_value:$('#table_total').is(':checked')?"1":"0"});
		printSets.push({sps_key:'print_times',sps_value:$('#print_times').val()});
		printSets.push({sps_key:'show_logo',sps_value:$('#show_logo').val()});
		return printSets;
	},
	buildPrintField:function(){
		var printFields = [];
		var ids = $("#grid").jqGrid('getDataIDs');
		if(ids == "" || ids.length == 0){
			return printFields;
		}
		var field = null;
		for(var i=0;i < ids.length;i++){
			var rowData = $("#grid").jqGrid("getRowData", ids[i]);
			field = {};
			field.spf_id = rowData.spf_id;
			field.spf_code = rowData.spf_code;
			field.spf_name = rowData.spf_name;
			field.spf_namecustom = rowData.spf_namecustom;
			field.spf_line = rowData.spf_line;
			field.spf_row = rowData.spf_row;
			field.spf_colspan = rowData.spf_colspan;
			field.spf_show = $("#fieldShow_"+rowData.spf_id).val();
			field.spf_position = rowData.spf_position;
			printFields.push(field);
		}
		return printFields;
	},
	buildPrintData:function(){
		var printDatas = [];
		var ids = $("#gridData").jqGrid('getDataIDs');
		if(ids == "" || ids.length == 0){
			return printDatas;
		}
		var data = null;
		for(var i=0;i < ids.length;i++){
			var rowData = $("#gridData").jqGrid("getRowData", ids[i]);
			data = {};
			data.spd_id = rowData.spd_id;
			data.spd_code = rowData.spd_code;
			data.spd_name = rowData.spd_name;
			data.spd_namecustom = rowData.spd_namecustom;
			data.spd_width = rowData.spd_width;
			data.spd_order = rowData.spd_order;
			data.spd_show = $("#dataShow_"+rowData.spd_id).val();
			data.spd_align = $("#dataAlign_"+rowData.spd_id).val();
			printDatas.push(data);
		}
		return printDatas;
	},
	save:function(){
		var sp_id = $("#sp_id").val();
		var postData = {};
		postData.print = this.buildPrint();
		postData.printSets = this.buildPrintSet();
		postData.printFields = this.buildPrintField();
		postData.printDatas = this.buildPrintData();
		if(postData.printFields.length == 0){
			Public.tips({type: 2, content : "系统未配置表头设置，请联系系统管理员！"});
			return;
		}
		if(postData.printDatas.length == 0){
			Public.tips({type: 2, content : "系统未配置表格设置，请联系系统管理员！"});
			return;
		}
		var isUpdate = sp_id != "" && sp_id > 0;
		var saveUrl = config.BASEPATH+"cash/set/savePrint";
		if(isUpdate){
			saveUrl = config.BASEPATH+"cash/set/updatePrint";
		}
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:saveUrl,
			data:{postData:JSON.stringify(postData)},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : '保存成功'});
					setTimeout("api.close()",400);
				}else{
					Public.tips({type: 1, content : data.message});
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	}
};
var THISPAGE = {
	init:function(){
		this.initParam();
		this.initDom();
		this.initCombo();
		this.initEvent();
		this.loadData();
	},
	initParam:function(){
		
	},
	initDom:function(){
		this.$_isprint = $("#isprint").cssCheckbox();
		this.$_autoprint = $("#autoprint").cssCheckbox();
		this.$_show_logo = $("#show_logo").cssCheckbox();
	},
	initCombo:function(){
		var pdata = [{id:"0",name:"纵向"},
		             {id:"1",name:"橫向"}];
		layCombo = $('#spanLay').combo({
	        value: 'id',
	        text: 'name',
	        data:pdata,
	        width : 55,
			height : 30,
			listId : '',
			editable : false,
			defaultSelected:0,
			callback : {
				onChange : function(data) {
					$("#de_in_cash").val(data.ba_code);
				}
			}
		}).getCombo();
	},
	loadData:function(){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'cash/set/print',
			data:{pt_id:pt_id},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					if(undefined != data.data.print){
						THISPAGE.initPrint(data.data.print);
					}
					THISPAGE.initFieldGrid(data.data.fields);
					THISPAGE.initDataGrid(data.data.datas);
					THISPAGE.initPrintSet(data.data.sets);
				}
			}
		});
	},
	initPrint:function(data){
		$("#sp_id").val(data.sp_id);
		$("#sp_code").val(data.sp_code);
		$("#sp_remark").val(data.sp_remark);
		$("#sp_title").val(data.sp_title);
		$("#sp_qrcode").val(data.sp_qrcode);
		if(data.sp_print == 1){
			this.$_isprint.chkAll();
		}
		if(data.sp_auto == 1){
			this.$_autoprint.chkAll();
		}
	},
	initPrintSet:function(data){
		if(data.show_logo == 1){
			this.$_show_logo.chkAll();
		}
		$("#print_times").val(data.print_times);
		$("#page_width").val(data.page_width);
		$("#page_height").val(data.page_height);
	},
	initFieldGrid:function(data){
		var colModel = [
        	{name: 'spf_code',label:'字段',index: 'spf_code',hidden:true},            
	    	{name: 'spf_name',label:'名称',index: 'spf_name', width:100, title: false},
	    	{name: 'spf_namecustom',label:'显示名称',index: 'spf_namecustom', width: 100,editable:true,title: false},
	    	{name: 'spf_line',label:'行',index: 'spf_line', width: 50,editable:true,title: false},
	    	{name: 'spf_row',label:'列',index: 'spf_row', width: 50,editable:true,title: false},
	    	{name: 'spf_colspan',label:'单元格',index: 'spf_colspan', width: 50, title: false,editable:true},
	    	{name: '',label:'显示',index: 'spf_show', width: 50, align:'center',sortable:false,formatter:handle.formatFieldShow},
	    	{name: '',label:'位置',index: 'spf_position', width: 50,align:'center',sortable:false, formatter:handle.formatPosition},
	    	{name: 'spf_show',label:'',index: 'spf_show', hidden:true},
	    	{name: 'spf_position',label:'',index: 'spf_position',hidden:true},
	    	{name: 'spf_id',hidden:true}
	    ];
		$('#grid').GridUnload();
		$('#grid').jqGrid({
			datatype: 'local',
			data:data,
			width: 540,
			height: 359,
			altRows:true,
			gridview: true,
			onselectrow: false,
			cellEdit: true,
			colModel: colModel,
			rowNum:99,
			rownumbers: true,//行号
			multiselect:false,//多选
			viewrecords: true,
			cellsubmit: 'clientArray',
			pgbuttons:true,
			shrinkToFit:false,//表格是否自动填充
			autoScroll: true,
			jsonReader: {
	            repeatitems: false,
	            id: "spf_id"
            },
			gridComplete: function(data){
				
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	initDataGrid:function(data){
		var colModel = [
        	{name: 'spd_code',label:'字段称',index: 'spd_code',hidden:true},
	    	{name: 'spd_name',label:'名称',index: 'spd_name', width:100, title: false},
	    	{name: 'spd_namecustom',label:'显示名称',index: 'spd_namecustom', width: 80,editable:true,title: false},
	    	{name: '',label:'显示',index: 'spd_show', width: 60,sortable:false,formatter:handle.formatDataShow},
	    	{name: 'spd_width',label:'宽度',index: 'spd_width', width: 60, editable:true},
	    	{name: 'align',label:'对齐',index: 'align', width: 90,sortable:false,formatter:handle.formatDataAlign},
	    	{name: 'spd_order',label:'顺序',index: 'spd_order', width: 60, editable:true},
	    	{name:'spd_show',hidden:true},
	    	{name:'spd_align',hidden:true},
	    	{name:'spd_id',hidden:true}
	    ];
		$('#gridData').GridUnload();
		$('#gridData').jqGrid({
			datatype: 'local',
			data:data,
			width: 540,
			height: 359,
			altRows:true,
			gridview: true,
			onselectrow: false,
			cellEdit: true,
			colModel: colModel,
			rowNum:99,
			rownumbers: true,//行号
			viewrecords: true,
			cellsubmit: 'clientArray',
			pgbuttons:true,
			shrinkToFit:false,//表格是否自动填充
			autoScroll: true,
			jsonReader: {
	            repeatitems: false,
	            id: "spd_id"
            },
            gridComplete: function(data){
				
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	changeType:function(id){
		if(undefined != id && "" != id){
			$("#menu1").removeClass().addClass("menu");
			$("#menu2").removeClass().addClass("menu");
			$("#menu3").removeClass().addClass("menu");
			$("#menu"+id).removeClass().addClass("select");
			$(".content").each(function(){
				$(this).hide();
			});
			$("#content"+id).show();
		}else{
			$("#menu2").removeClass().addClass("menu");
			$("#menu3").removeClass().addClass("menu");
			$("#menu1").removeClass().addClass("select");
			$(".content").each(function(){
				$(this).hide();
			});
			$("#content1").show();
		}
	},
	initEvent:function(){
		$("#btn_close").on('click',function(){
			api.close();
		});
		$("#btn-save").on('click',function(){
			handle.save();
		});
	}
};
THISPAGE.init();