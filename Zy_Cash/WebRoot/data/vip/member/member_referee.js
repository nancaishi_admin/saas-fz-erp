var config = parent.CONFIG,system=parent.SYSTEM;
var isRefresh = false;
var queryurl = config.BASEPATH+'vip/member/page';
var api = frameElement.api, W = api.opener;
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
	},
	initGrid:function(){
		var colModel = [
	    	{name: 'vm_code',label:'编号',index: 'vm_code',width:0,hidden:true},
	    	{name: 'vm_cardcode',label:'卡号',index: 'vm_cardcode',width:100},
	    	{name: 'vm_name',label:'名称',index: 'vm_name',width:75},
	    	{name: 'vm_mobile',label:'手机',index: 'vm_mobile',width:96},
	    	{name: 'vm_sex',label:'性别',index: 'vm_sex',align:'center',width:40},
	    ];
		$('#grid').jqGrid({
			datatype: 'json',
			width: 380,
			height: 140,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:99,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'vm_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += '&vm_cardcode='+Public.encodeURI($("#SearchContent").val());
		return params;
	},
	reset:function(){
		$("#vm_cardcode").val("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			Public.hideConstion();
			THISPAGE.reloadData();
		});
	}
};
function doSelect(){
	var selectedId = $("#grid").jqGrid('getGridParam', 'selrow');
	if(selectedId == null || selectedId == ""){
		Public.tips({type: 2, content : "请选择!"});
		return false;
	}
	var rowData = $("#grid").jqGrid("getRowData", selectedId);
	return rowData;
}
THISPAGE.init();