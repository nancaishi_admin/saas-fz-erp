var config = parent.CONFIG,system=parent.SYSTEM;
var isRefresh = false;
var queryurl = config.BASEPATH+'voucher/list';
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var handle = {
	operate: function(oper, id){//修改、新增
		var height = 300;
		var width = 538;
		if(oper == 'add'){
			title = '新增';
			url = config.BASEPATH+"voucher/to_add";
			data = {oper: oper, callback:this.callback};
		}
		W.$.dialog({
			id:"twoPageID",
			title : title,
			content : 'url:'+url,
			data: data,
			width : width,
			height : height,
			max : false,
			min : false,
			drag: false,
			lock: false,
			close:function(){
				try{setTimeout("api.zindex()",300);}catch(e){}
			}
		});
	},
    callback: function(data, oper, dialogWin){
		if(oper == "add"){
			$("#grid").jqGrid('addRowData', data.id, data, 'first');
		}
	},
	formatLeftMoney : function(val, opt, row){
		return row.vc_money - row.vc_used_money;
	},
	formatState:function(val, opt, row){//0:正常 1:停用
		if(val == "0"){
			return "正常";
		}else if(val == "1"){
			return "停用";
		}else{
			return val;
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
	},
	initGrid:function(){
		var colModel = [
	    	{name: 'vc_code',label:'编号',index: 'vc_code',hidden: true},
	    	{name: 'vc_cardcode',label:'券号',index: 'vc_cardcode',width:120},
	    	{name: 'vc_name',label:'姓名',index: 'vc_name',width:80},
	    	{name: 'vc_mobile',label:'手机号码',index: 'vc_mobile',width:100},
	    	{name: 'shop_name',label:'发放门店',index: 'shop_name',width:100},
	    	{name: 'vc_grantdate',label:'发放日期',index: 'vc_grantdate',width:90},
	    	{name: 'vc_enddate',label:'有效日期',index: 'vc_enddate',width:90},
	    	{name: 'vc_money',label:'发放金额',index: 'vc_money',width:60},
	    	{name: 'vc_realcash',label:'实收现金',index: 'vc_realcash',width:60},
	    	{name: 'left_money',label:'剩余金额',index: 'vc_money',width:60,formatter: handle.formatLeftMoney},
	    	{name: 'vc_state',label:'状态',index: 'vc_state',width:60,formatter:handle.formatState}
	    ];
		$('#grid').jqGrid({
			datatype: 'json',
			width: 933,
			height: 418,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:' 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:-1,//每页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id: 'vc_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		var code = $.trim($("#SearchContent").val());
		params += '&vc_cardcode='+Public.encodeURI(code);
		return params;
	},
	reset:function(){
		$("#vc_cardcode").val("");
	},
	reloadData:function(){
		var code = $.trim($("#SearchContent").val());
		if(null == code || '' == code){
			W.Public.tips({type: 2, content : "请输入卡号!"});
			return;
		}
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
		//新增
		$('#btn-add').on('click', function(e){
			e.preventDefault();
			handle.operate('add');
		});
	}
};
THISPAGE.init();