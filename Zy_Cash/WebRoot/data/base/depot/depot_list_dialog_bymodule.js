var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;

var api = frameElement.api;
var multiselect = false;
if(undefined != api.data.multiselect){
	multiselect = api.data.multiselect;
}
var _height = api.config.height-132,_width = api.config.width-34;//获取弹出框弹出宽、高
var queryurl = '';
if(api.data.module == 'sell_allocate'){//前台调拨单
	queryurl = config.BASEPATH+'base/depot/list4sellallocate';
}else if(api.data.module == 'want'){//店铺发货单
	queryurl = config.BASEPATH+'base/depot/list4want';
}

var dblClick = false;
var THISPAGE = {
	init:function (){
		this.initGrid();
		this.initEvent();	
	},
	initGrid:function(){
		var colModel = [
			{name: 'dp_code', label:'仓库编号',index: 'dp_code', width: 70},
	    	{name: 'dp_name', label:'仓库名称',index: 'dp_name', width: 100},
	    	{name: 'shop_name', label:'店铺名称',index: 'shop_name', width: 100},
	    	{name: 'ty_name', label:'类型',index: 'ty_name', width: 70,align:'center'}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
			multiselect:multiselect,//多选
			viewrecords: true,
			rowNum:-1,//每页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:true,//表格是否自动填充
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id: 'dp_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow: function (rowid, iRow, iCol, e) {
				if(!multiselect){
					dblClick = true;
					api.close();
				}
            }
	    });
	},
	buildParams:function(){
		var params = '';
		params += "searchContent="+Public.encodeURI($.trim($('#SearchContent').val()));
		if(api.data.sp_code != undefined){
			params += "&sp_code="+api.data.sp_code;
		}
		return params;
	},
	reloadData:function(){
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+THISPAGE.buildParams()}).trigger("reloadGrid");
	},
	initEvent:function(){
		$('#SearchContent').on('keyup',function(event){
			if (event.keyCode==13){
				$('#btn-search').click();
			}
		});
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
	}
};
function doSelect(){
	if(multiselect){//多选
		var ids = $("#grid").jqGrid('getGridParam', 'selarrrow');
		if(ids == null || ids.length == 0){
			Public.tips({type: 2, content : "请选择仓库！"});
			return false;
		}
		var result = [];
		for (var i = 0; i < ids.length; i++) {
			result.push($("#grid").jqGrid("getRowData", ids[i]));
		}
		return result;
		
	}else{//单选
		var selectedId = $("#grid").jqGrid('getGridParam', 'selrow');
		if(selectedId == null || selectedId == ""){
			Public.tips({type: 2, content : "请选择仓库！"});
			return false;
		}
		var rowData = $("#grid").jqGrid("getRowData", selectedId);
		return rowData;
	}
}
THISPAGE.init();