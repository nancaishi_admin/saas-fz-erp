var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var api = frameElement.api;
var multiselect = false;
if(undefined != api.data.multiselect){
	multiselect = api.data.multiselect;
}
var _height = api.config.height-132,_width = api.config.width-34;//获取弹出框弹出宽、高
var queryurl = config.BASEPATH+'sys/reward/page';

var handle = {
	formatIcon : function(val, opt, row){
		var html_con = '';
		html_con += '<div class="icons">';
		html_con += '<i class="iconfont '+row.rw_style+'">'+row.rw_icon+'</i>';
		html_con += '</div>';
		return html_con;
	}
};

var dblClick = false;
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
			{label:'图标',name: 'icon',width: 100, formatter: handle.formatIcon,align:'center',sortable:false},
			{label:'编号',name: 'rw_code', index: 'rw_code', width:80, fixed:true},
			{label:'名称',name: 'rw_name', index: 'rw_name', width: 140},
			{label:'分数',name: 'rw_score', index: 'rw_score', width: 100,align:'right',hidden:true},
			{label:'备注',name: 'rw_remark', index: 'rw_remark', width: 180,hidden:true},
			{label:'',name: 'rw_style', index: 'rw_style', width: 180,hidden:true},
			{label:'',name: 'rw_icon', index: 'rw_icon', width: 180,hidden:true}
	    ];
		$('#grid').jqGrid({
			url:queryurl,			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条', 
			multiselect:multiselect,//多选
			viewrecords: true,
			rowNum:config.BASEROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'rw_id'
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow: function (rowid, iRow, iCol, e) {
				if(!multiselect){
					dblClick = true;
					api.close();
				}
            }
	    });
	},
	buildParams:function(){
		var params = '';
		params += "searchContent="+Public.encodeURI($.trim($('#SearchContent').val()));
		return params;
	},
	reloadData:function(){
		var param=this.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		$('#SearchContent').on('keyup',function(){
			if (event.keyCode==13){
				$('#btn-search').click();
			}
		});
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
	}
};

function doSelect(){
	if(multiselect){//多选
		var ids = $("#grid").jqGrid('getGridParam', 'selarrrow');
		if(ids == null || ids.length == 0){
			Public.tips({type: 2, content : "请选择奖励！"});
			return false;
		}
		var result = [];
		for (var i = 0; i < ids.length; i++) {
			result.push($("#grid").jqGrid("getRowData", ids[i]));
		}
		return result;
		
	}else{//单选
		var selectedId = $("#grid").jqGrid('getGridParam', 'selrow');
		if(selectedId == null || selectedId == ""){
			Public.tips({type: 2, content : "请选择奖励！"});
			return false;
		}
		var rowData = $("#grid").jqGrid("getRowData", selectedId);
		return rowData;
	}
}

THISPAGE.init();