var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var queryurl = config.BASEPATH+'stock/otherList';
var api = frameElement.api, W = api.opener;
var dp_code = system.DP_CODE,dp_name=system.DP_NAME,depots = [];
function getDepotList(){
	$.ajax({
		type:"POST",
		url:config.BASEPATH+'stock/ownDepot?'+THISPAGE.buildUrlParams(),
		async:false,
		dataType:"json",
		success:function(data){
			if(data != null){
				$_listDepot=data.items;
				return  $_listDepot;
			}
		}
	});
}
var handle = {
	doDepot:function(){
		depot = W.$.dialog({ 
		   	id:'TwoPageID',
		   	title:'仓库选择',
		   	data : {multiselect:true},
		   	max: false,
		   	min: false,
		   	lock:false,
		   	width : 520,
			height : 320,
		   	fixed:false,
		   	drag: true,
		   	resize:false,
		   	content:'url:'+config.BASEPATH+'stock/to_depot',
		   	ok:function(){
				var selected = depot.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = depot.content.doSelect();
				if(selected){
					depots = selected;
					var names = "";
					for(i in depots){
						names += depots[i].dp_name;
					}
					$("#dp_names").val(names);
					setTimeout("api.zindex()",200);
				}
			},
			cancel:true
		});
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initCombo();
		this.initGrid('');
		this.initEvent();
	},
	initDom:function(){
		depots = [{"dp_code":dp_code,"dp_name":dp_name}];
	},
	initCombo:function(){
		sizeCombo = $('#spanSize').combo({
	        value: 'sz_code',
	        text: 'sz_name',
	        width : 60,
			height : 30,
			listId : '',
			editable : true,
			defaultSelected:-1,
			callback : {
				onChange : function(data) {
					if(null != data){
						$("#sz_code").val(data.sz_code);
					}
				}
			}
		}).getCombo();
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"stock/size",
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					var pdata = data.data;
					sizeCombo.loadData(pdata,-1);
				}
			}
		});
		braCombo = $('#spanBra').combo({
	        value: 'br_code',
	        text: 'br_name',
	        width : 60,
			height : 30,
			listId : '',
			editable : true,
			defaultSelected:-1,
			callback : {
				onChange : function(data) {
					if(null != data){
						$("#br_code").val(data.br_code);
					}
				}
			}
		}).getCombo();
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"stock/braList",
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					var pdata = data.data;
					braCombo.loadData(pdata,-1);
				}
			}
		});
	},
	buildUrlParams:function(){
		var pd_no=$.trim($("#pd_no").val());
		var params="";
		params += "pd_no=" +Public.encodeURI(pd_no);
		params += "&sz_code="+$("#sz_code").val();
		params += "&br_code="+$("#br_code").val();
		params += "&data="+Public.encodeURI(JSON.stringify(depots));
		return params;
	},
	initGrid:function(url){
		var colModel = [
	    	{name: 'pd_no',label:'货号',index:'pd_no', width: 80,title: false,frozen : true},
	    	{name: 'pd_name',label:'名称',index:'pd_name',width: 110,align:'center', title: false,frozen: true},
	    	{name: 'cr_name',label:'颜色',index:'sd_cr_code',width: 70, align:'center',title: true,frozen: true},
	    	{name: 'sz_name',label:'尺码',index:'sd_sz_code',width: 70, align:'center',title: true,frozen:true},
	    	{name: 'br_name',label:'杯型',index:'sd_br_code',width: 45, align:'center',title: true,frozen:true},
	    	{name: 'sd_cr_code',index:'',title: false,hidden:true},
	    	{name: 'sd_sz_code',index:'',title: false,hidden:true},
	    	{name: 'sd_br_code',index:'',title: false,hidden:true}
	    ];
		if(null != depots && depots.length > 0){
			for(i in depots){
				colModel.push({name:"sd_"+depots[i].dp_code,label:depots[i].dp_name,index:'',width:80,title:true,sortable:false});
			}
		}else{
			colModel.push({name:"sd_"+dp_code,label:dp_name,index:'',width:80,title:true,sortable:false});
		}
		colModel.push({name: "sd_amount",label:'小计',width: 80, title: false,sortable:false,align:'center'});
		colModel.push({name: "sd_id",label:'id',index:'',width: 0,hidden:true});
		$('#grid').jqGrid({
			url:url,
			datatype: 'json',
			width:930,
			height:325,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',
			viewrecords: true,
			rowNum:config.BASEROWNUM,//每页条数
			multiSort: true,
			shrinkToFit:false,//表格是否自动填充
			autoScroll: true,
			scroll: 1,//是否滚动翻页
			sortable:true,
			footerrow :true,
			userDataOnFooter : true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',  
				userdata:'data.data',
				repeatitems : false
			},
			loadComplete: function(data){
			},
			gridComplete:function(){
				$("#grid").jqGrid('setFrozenColumns');
			},
			loadError: function(xhr, status, error){
			}
	    });
	},
	initEvent:function(){
		//回车查询
		$('#pd_no').keyup(function(e){if (e.keyCode==13){$('#btn_search').click();}});
		//查询
		$('#btn_search').on('click', function(e){
			var  pd_no = $("#pd_no").val();
			if(pd_no != null && "" != pd_no){
				var url = queryurl+'?'+THISPAGE.buildUrlParams();
				$("#grid").jqGrid('GridUnload');
				THISPAGE.initGrid(url);
			}else{
				W.Public.tips({type: 1, content : '请输入货号!'});
			}
		});
		
		//查询
		$('#btn_ok').on('click', function(e){
			$('#btn_search').click();
		});
		
		//查询
		$('#btn_depot').on('click', function(e){
			handle.doDepot();
		});
		
		//重置
		$('#btn_reset').on('click',function(){
			$('#pd_no').val('');	
			$('#sz_code').val('');
			sizeCombo.selectByIndex(-1,false);
			$('#br_code').val('');
			braCombo.selectByIndex(-1,false);
			$('#dp_names').val('');
		});
	}
};
THISPAGE.init();