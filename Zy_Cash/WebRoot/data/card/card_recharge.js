var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;

var handle = {
	save:function(){
		if (!$("#form1").valid()) {
			return;
		}
		var cd_realcash = $("#cdl_realcash").val();
		var cd_bankmoney = $("#cdl_bankmoney").val();
		var cd_money = $("#cdl_money").val();
		if(null == cd_realcash || '' == cd_realcash){
			cd_realcash = 0;
		}
		if(null == cd_bankmoney || '' == cd_bankmoney){
			cd_realcash = 0;
		}
		if(null == cd_money || '' == cd_money){
			W.Public.tips({type: 3, content : "请输入发卡金额"});
			return;
		}else{
			if(parseFloat(cd_money) < (parseFloat(cd_bankmoney)+parseFloat(cd_realcash))){
				W.Public.tips({type: 3, content : "实收金额不能大于发卡金额"});
				return;
			}
		}
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"cash/card/recharge",
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : data.message});
					handle.loadData(data.data);
					setTimeout("api.zindex()",500);
					THISPAGE.initDom();
				}else{
					Public.tips({type: 1, content : data.message});
					setTimeout("api.zindex()",500);
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.cd_id;
		pdata.cd_money=data.cd_money;
		pdata.cd_used_money=data.cd_used_money;
		pdata.cd_realcash=data.cd_realcash;
		pdata.cd_bankmoney=data.cd_bankmoney;
		pdata.cd_cashrate=data.cd_cashrate;
		pdata.total_money=data.cd_money+data.cd_used_money;
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	}
};

var THISPAGE = {
	init:function (){
		this.initParam();
		this.initDom();
		this.initCombo();
		this.initEvent();
	},
	initParam:function(){
		var cd_state = $("#cd_state").val();
		if(1 != cd_state){
			$("#cd_state").val("正常");
		}else{
			$("#cd_state").val("停用");
		}
		this.$_cdl_type = $("#span_cdl_type").cssRadio({ callback: function($_obj){
			var val = $_obj.find("input").val();
			$("#cdl_type").val(val);
			if(val == 1){
				$("span[name='label1']").text("充");
				$("span[name='label2']").text("收");
			}else{
				$("span[name='label1']").text("减");
				$("span[name='label2']").text("减");
			}
		}});
		this.$_ischeck = $("#ischeck").cssCheckbox();
		_self = this;
	},
	initDom:function(){
		$("#cdl_date").val(DateToFullDateTimeString(new Date()));
	},
	initCombo:function(){
		bankCombo = $('#spanBank').combo({
	        value: 'ba_code',
	        text: 'ba_name',
	        width : 158,
			height : 30,
			listId : '',
			editable : false,
			defaultSelected:-1,
			callback : {
				onChange : function(data) {
					$("#cdl_ba_code").val(data.ba_code);
				}
			}
		}).getCombo();
		banksCombo = $('#spanBanks').combo({
	        value: 'ba_code',
	        text: 'ba_name',
	        width : 158,
			height : 30,
			listId : '',
			editable : false,
			defaultSelected:-1,
			callback : {
				onChange : function(data) {
					$("#cdl_bank_code").val(data.ba_code);
				}
			}
		}).getCombo();
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'money/bank/list',
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					bankCombo.loadData(data.data);
					banksCombo.loadData(data.data);
				}
			}
		});
	},
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save();
		});
		$("#btn_close").click(function(){
			api.close();
		});
	}
};
THISPAGE.init();