var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var handle = {
	save:function(){
		if (!$("#form1").valid()) {
			return;
		}
		var cd_realcash = $("#cd_realcash").val();
		var cd_bankmoney = $("#cd_bankmoney").val();
		var cd_money = $("#cd_money").val();
		if(null == cd_realcash || '' == cd_realcash){
			cd_realcash = 0;
		}
		if(null == cd_bankmoney || '' == cd_bankmoney){
			cd_realcash = 0;
		}
		if(null == cd_money || '' == cd_money){
			W.Public.tips({type: 3, content : "请输入发卡金额"});
			return;
		}else{
			if(parseFloat(cd_money) < (parseFloat(cd_bankmoney)+parseFloat(cd_realcash))){
				W.Public.tips({type: 3, content : "实收金额不能大于发卡金额"});
				return;
			}
		}
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"cash/card/save",
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					W.Public.tips({type: 0, content : data.message});
					handle.loadData(data.data);
					setTimeout("api.close()",200);
				}else{
					W.Public.tips({type: 1, content : data.message});
					setTimeout("api.zindex()",200);
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.cd_id;
		pdata.cd_code=data.cd_code;
		pdata.cd_cardcode=data.cd_cardcode;
		pdata.cd_name=data.cd_name;
		pdata.cd_mobile=data.cd_mobile;
		pdata.shop_name= $('#shop_name').val();
		pdata.cd_grantdate=data.cd_grantdate;
		pdata.cd_enddate=data.cd_enddate;
		pdata.cd_money=data.cd_money;
		pdata.cd_used_money=data.cd_used_money;
		pdata.left_money=0;
		pdata.cd_state=data.cd_state;
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	}
};

var THISPAGE = {
	init:function (){
		this.initParam();
		this.initDom();
		this.initCombo();
		this.initEvent();
	},
	initParam:function(){
		$("#cd_cardcode").focus();
		$("#cd_manager").val(system.EM_NAME);
		$("#cd_shop_code").val(system.SHOP_CODE);
		$("#shop_name").val(system.SHOP_NAME);
		this.$_ischeck = $("#ischeck").cssCheckbox();
		_self = this;
	},
	initCombo:function(){
		bankCombo = $('#spanBank').combo({
	        value: 'ba_code',
	        text: 'ba_name',
	        width : 158,
			height : 30,
			listId : '',
			editable : false,
			defaultSelected:-1,
			callback : {
				onChange : function(data) {
					$("#cdl_ba_code").val(data.ba_code);
				}
			}
		}).getCombo();
		banksCombo = $('#spanBanks').combo({
	        value: 'ba_code',
	        text: 'ba_name',
	        width : 158,
			height : 30,
			listId : '',
			editable : false,
			defaultSelected:-1,
			callback : {
				onChange : function(data) {
					$("#cdl_bank_code").val(data.ba_code);
				}
			}
		}).getCombo();
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'money/bank/list',
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					bankCombo.loadData(data.data);
					banksCombo.loadData(data.data);
				}
			}
		});
	},
	initDom:function(){
		var currentDate = new Date();
		$("#cd_grantdate").val(DateToFullDateTimeString(currentDate));
		currentDate.setYear(currentDate.getFullYear() + 2); 
		$("#cd_enddate").val(DateToFullDateTimeString(currentDate));
	},
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save();
		});
		$("#btn_close").click(function(){
			api.close();
		});
	}
};
THISPAGE.init();
