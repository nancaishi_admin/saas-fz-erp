var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var isRefresh = false;
var queryurl = config.BASEPATH+'cash/card/list';
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var handle = {
	operate: function(oper, id){//修改、新增
		var width = 545,height = 380;
		if(oper == 'add'){
			title = '新增';
			url = config.BASEPATH+"cash/card/to_add";
			data = {oper: oper, callback:this.callback};
		}else if(oper == 'recharge'){
			height = 280;
			title = '充值';
			url = config.BASEPATH+"cash/card/to_recharge/1?cd_id="+id;
			data = {oper: oper,callback: this.callback};
		}else if(oper == 'recharge-minus'){
			height = 280;
			title = '减值';
			url = config.BASEPATH+"cash/card/to_recharge/2?cd_id="+id;
			data = {oper: oper,callback: this.callback};
		}else{
			return false;
		}
		W.$.dialog({
			title : title,
			content : 'url:'+url,
			data: data,
			width : width,
			height : height,
			max : false,
			min : false,
			cache : false,
			lock: false,
			close:function(){
				try{setTimeout("api.zindex()",200);}catch(e){}
			}
			
		});
	},
	editPass: function(id){
		if(id == null || id == ""){
			W.Public.tips({type: 2, content : "请选择储值卡!"});
			return;
		}
		W.$.dialog({
			title : '修改密码',
			content : 'url:'+config.BASEPATH+"cash/card/to_pass?cd_id="+id,
			width : 265,
			height : 232,
			max : false,
			min : false,
			cache : false,
			lock: false,
			close:function(){
				try{setTimeout("api.zindex()",200);}catch(e){}
			}
		});
	},
    callback: function(data, oper, dialogWin){
		var gridData = $("#grid").data('gridData');
		if(!gridData){
			gridData = {};
			$("#grid").data('gridData', gridData);
		}
		gridData[data.id] = data;
		if(oper == "recharge" || oper == "recharge-minus") {
			$("#grid").jqGrid('setRowData', data.id, data);
			dialogWin && dialogWin.api.close();
		}
		if(oper == "add"){
			$("#grid").jqGrid('addRowData', data.id, data, 'first');
		}
	},
	operFmatter : function(val, opt, row){
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-recharge" title="充值">&#xe645;</i>';
		html_con += '<i class="iconfont i-hand ui-icon-recharge-minus" title="减值">&#xe646;</i>';
		html_con += '</div>';
		return html_con;
	},
	formatTotalMoney : function(val, opt, row){
		return row.cd_money + row.cd_used_money;
	},
	formatState:function(val, opt, row){//0:正常 1:停用
		if(val == "0"){
			return "正常";
		}else if(val == "1"){
			return "停用";
		}else{
			return val;
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
	},
	initGrid:function(){
		var colModel = [
	    	{name: 'operate',label:'操作',width: 60, formatter: handle.operFmatter,align:'center', sortable:false},
	    	{name: 'cd_cardcode',label:'储值卡号',index: 'cd_cardcode',width:100},
	    	{name: 'cd_name',label:'姓名',index: 'cd_name',width:80},
	    	{name: 'cd_mobile',label:'手机号码',index: 'cd_mobile',width:100},
//	    	{name: 'cd_idcard',label:'身份证号',index: 'cd_idcard',width:120},
	    	{name: 'shop_name',label:'发放门店',index: 'shop_name',width:100},
	    	{name: 'cd_grantdate',label:'发放日期',index: 'cd_grantdate',width:80},
	    	{name: 'cd_enddate',label:'有效日期',index: 'cd_enddate',width:80},
	    	{name: 'total_money',label:'总计',index: 'total_money',width:60,align:'right',formatter: handle.formatTotalMoney},
	    	{name: 'cd_used_money',label:'已用',index: 'cd_used_money',width:60,align:'right'},
	    	{name: 'cd_money',label:'剩余',index: 'cd_money',width:60,align:'right'},
	    	{name: 'cd_state',label:'状态',index: 'cd_state',width:50,formatter:handle.formatState},
	    ];
		$('#grid').jqGrid({
//			url:queryurl,
			datatype: 'json',
			width: 933,
			height: 418,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:-1,//每页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id: 'cd_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	reset:function(){
		$("#cd_cardcode").val("");
	},
	reloadData:function(){
		var code = $.trim($("#cd_cardcode").val());
		if(null == code || "" == code){
			W.Public.tips({type: 2, content : "请输入卡号!"});
			return;
		}
		var params = 'cd_cardcode='+Public.encodeURI(code);
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+params}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
		$("#cd_cardcode").keyup(function(e){
			e.preventDefault();
			if(e.keyCode ==13){
				$('#btn-search').click();
			}
		});
		$('#btn-pass').on('click', function(e){
			e.preventDefault();
			var id = $("#grid").jqGrid('getGridParam', 'selrow');
			handle.editPass(id);
		});
		
		//新增
		$('#btn-add').on('click', function(e){
			e.preventDefault();
			if(1 != system.CARD_ADD){
				W.Public.tips({type: 1, content : "无发卡权限!"});
				return;
			}
			handle.operate('add');
		});
		$('#grid').on('click', '.operating .ui-icon-recharge', function(e){
			e.preventDefault();
			if(1 != system.CARD_CHARGE){
				W.Public.tips({type: 1, content : "无充值权限!"});
				return;
			}
			var id = $(this).parent().data('id');
			handle.operate('recharge', id);
		});
		$('#grid').on('click', '.operating .ui-icon-recharge-minus', function(e){
			e.preventDefault();
			if(1 != system.CARD_CHARGE){
				W.Public.tips({type: 1, content : "无减值权限!"});
				return;
			}
			var id = $(this).parent().data('id');
			handle.operate('recharge-minus', id);
		});
	}
};
THISPAGE.init();