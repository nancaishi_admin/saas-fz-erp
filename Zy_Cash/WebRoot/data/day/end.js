var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var url = config.BASEPATH+"dayend/list";
var api = frameElement.api, W = api.opener;
var THISPAGE = {
	init:function(){
		this.addEvent();
		this.initCombo();
		this.initDom();
		this.initGrid();
	},
	initDom:function(id){
		if(undefined != id && "" != id){
			$("#menu1").removeClass().addClass("menu");
			$("#menu2").removeClass().addClass("menu");
			$("#menu"+id).removeClass().addClass("select");
			$(".content").each(function(){
				$(this).hide();
			});
			$("#content"+id).show();
			if(id == 2){
				THISPAGE.reloadData();
			}
		}else{
			$("#menu2").removeClass().addClass("menu");
			$("#menu1").removeClass().addClass("select");
			$(".content").each(function(){
				$(this).hide();
			});
			$("#content1").show();
		}
	},
	FormatPrint:function(val, opt, row){
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-print" title="补票">&#xe615;</i>';
		html_con += '</div>';
		return html_con;
	},
	reloadData:function(){
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:url}).trigger("reloadGrid");
	},
	initGrid:function(){
		var colModel = [
        	{name: 'oper',index:'oper',label:'操作',formatter:THISPAGE.FormatPrint,width:30,align:'center',title: false,sortable:false},
        	{name: 'de_id',label:'ID',index: 'de_id',hidden:true},
        	{name: 'de_enddate',label:'交班日期',index: 'de_enddate',width:150},
	    	{name: 'em_name',label:'交班人',index: 'em_name',width:50},
	    	{name: 'st_name',label:'班次',index: 'st_name',width:60},
	    	{name: 'de_sell_money',label:'零售金额',index: 'de_sell_money',width:85},
	    	{name: 'de_money',label:'实现金额',index: 'de_money',width:85}	    
	    	];
		$('#grid').jqGrid({
			datatype: 'local',
			width: 540,
			height: 322,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:-1,//每页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id: 'de_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	initCombo:function(){
		cashCombo = $('#spanCash').combo({
	        value: 'ba_code',
	        text: 'ba_name',
	        width : 108,
			height : 30,
			listId : '',
			editable : false,
			defaultSelected:-1,
			callback : {
				onChange : function(data) {
					$("#de_in_cash").val(data.ba_code);
				}
			}
		}).getCombo();
		bankCombo = $('#spanBank').combo({
	        value: 'ba_code',
	        text: 'ba_name',
	        width : 108,
			height : 30,
			listId : '',
			editable : false,
			defaultSelected:-1,
			callback : {
				onChange : function(data) {
					$("#de_in_bank").val(data.ba_code);
				}
			}
		}).getCombo();
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'money/bank/list',
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					bankCombo.loadData(data.data,-1);
					cashCombo.loadData(data.data,-1);
				}
			}
		});
	},
	buildMoney:function(){
	},
	addEvent:function(){
		$("#btn-save").click(function(){
			THISPAGE.save();
		});
		$("#btn_close").click(function(){
			api.close();
		});
		$('#grid').on('click', '.operating .ui-icon-print', function(e){
			e.preventDefault();
			var id = $(this).parent().data('id');
			THISPAGE.print(id);
		});
	},
	print:function(id){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'dayend/endByID',
			data:{"de_id":id},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					LODOP.SET_PRINT_PAGESIZE(1,"74mm",'', '');
					LODOP.ADD_PRINT_HTM("0","1mm","98%","100%",data.data);// 对象2
					LODOP.SET_PRINT_MODE("PRINT_PAGE_PERCENT", "Auto-Width");//整宽不变形
					LODOP.SET_PRINT_STYLE("FontSize",11);
					LODOP.PREVIEW();
				}
			}
		});
	},
	save:function(){
		var de_in_cash = $("#de_in_cash").val();
		var de_in_bank = $("#de_in_bank").val();
		if(null == de_in_cash || "" == de_in_cash){
			W.Public.tips({type: 1, content : "请选择现金帐户"});
			return;
		}
		if(null == de_in_bank || "" == de_in_bank){
			W.Public.tips({type: 1, content : "请选择刷卡帐户"});
			return;
		}
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'dayend/update',
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					W.Public.tips({type: 1, content : "日结成功"});
					printDayEnd();

					setTimeout(function(){
						window.parent.location.replace(config.BASEPATH+"logout");
					},1000);
				}
			}
		});
	}
};
THISPAGE.init();
function printDayEnd(){
	LODOP.SET_PRINT_PAGESIZE(1, 720, '', '');
	LODOP.SET_PRINT_STYLE("FontSize", 12);
    LODOP.SET_PRINT_STYLE("FontName", "微软雅黑");
	LODOP.ADD_PRINT_HTM(3,0,"7cm","20cm",document.getElementById("div1").innerHTML);// 对象2
	LODOP.PREVIEW();
}
