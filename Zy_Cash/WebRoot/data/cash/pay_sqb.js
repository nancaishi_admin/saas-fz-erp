var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var api = frameElement.api, W = api.opener;
var sell_money = 200;
var callback = api.data.callback;
var w_pay_money = api.data.total_money;
var queryurl = config.BASEPATH+"cash/payList?1=1";
var UTIL = {
	formatPay:function(val, opt, row){
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<input type="button" class="btn-green" state="'+val+'" onclick="UTIL.doConfirm(this,'+opt.rowId+')"  value="确认"/>';
		html_con += '</div>';
		return html_con; 
	},
	formatState:function(val, opt, row){
		if(val != null ){
			if(val == 0)
				return "未支付";
			else if(val == 1)
				return "已支付";
		}
	},
	formatCode:function(val, opt, row){
		var code = row.pt_number;
		if(code != null && "" != code){
			if(code.length > 6){
				code = code.substring(code.length-6,code.length);
			}
		}
		return "*"+code;
	},
	doConfirm:function(obj,rowid){
		var grid = $("#grid").jqGrid("getRowData", rowid);
		var pt_number = grid.pt_number;
		if(null != pt_number && "" != pt_number){
			THISPAGE.queryPay(pt_number);
		}
	},
	tab:function(obj){
		$(".pay-tab").each(function(){
			$(this).removeClass("select");
		});
		$(obj).addClass("select");
		
		$(".contbox").each(function(){
			$(this).css("display","none");
		});
		var obj_id = $(obj).attr("id");
		$("#"+obj_id+"-div").css("display","block");
		if(obj_id == "pay-query"){
			THISPAGE.reloadData();
		}
	},
	doPay:function(){
		var code = $("#pt_paycode").val();
		if(null != code && "" != code){
			$.ajax({
				type:"POST",
				url:config.BASEPATH+"cash/doPay",
				data:encodeURI(encodeURI($('#form1').serialize())),
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data){
						var result = data.data;
						if(data.stat == 200){
							if(result.result_code == "PAY_SUCCESS"){
								var pdata ={flag:1,third_money:$("#pt_money").val()};
								if(callback && typeof callback == 'function'){
									callback(pdata,W,window);
									setTimeout("api.close()",100);
								}
							}else{
								W.Public.tips({type: 2, content :result.error_message});
								setTimeout("api.zindex()",200);
								return;
							}
						}else{
							W.Public.tips({type: 2, content :data.message});
							setTimeout("api.zindex()",200);
							return;
						}
					}
				}
			});
		}else{
			W.Public.tips({type: 2, content : "请输入付款码!"});
			setTimeout("api.zindex()",300);
		}
	}
};
var THISPAGE = {
	init:function(){
		this.initStyle();
//		this.initComob();
		this.initGrid();
		this.addEvent();
	},
	initStyle:function(){
		$("#pt_paycode").focus();
		$("#pt_money").val(w_pay_money);
	},
	initComob:function(){
		var payInfo = {data:{items:[
                            {pay_code:'',pay_name:'全部'},
                          	{pay_code:'0',pay_name:'未支付'},
	                        {pay_code:'1',pay_name:'已支付'}
	                  	]}};
		$('#span_pay_result').combo({
			data:payInfo.data.items,
			value: 'pay_code',
			text: 'pay_name',
			width :120,
			height:80,
			listId:'',
			defaultSelected:0,
			editable: true,
			callback:{
				onChange: function(data){
					$("#pay_state").val(data.pay_code);
				}
			}
		}).getCombo();
	},
	reloadData:function(){
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl}).trigger("reloadGrid");
	},
	initGrid:function(){
		var colModel = [
        	{name: 'pt_opt', index:'pt_opt',label:'操作', width:40,align:'center',title: false,formatter:UTIL.formatPay},
            {name: 'pt_state', index:'pt_state',label:'状态', width:55,align:'center',title: false,formatter:UTIL.formatState},
            {name: '_pt_state', index:'_pt_state',label:'状态', title: false,hidden:true},
	    	{name: '_pt_number', index: '_pt_number',label:'订单号(后6位)', width:150,align:'center',title: false,formatter:UTIL.formatCode},
	    	{name: 'pt_number', index:'pt_number',label:'订单号', title: false,hidden:true},
	    	{name: 'pt_money', index: 'pt_money',label:'金额', width:60,align:'right',title: false}
	    ];
		$('#grid').jqGrid({
			datatype: 'json',
			width:375,
			height:78,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.DATAROWNUM,//每页条数
			shrinkToFit:false,//表格是否自动填充
			autoScroll: true,
			pager: '#page',//分页
			pgtext: false,
			pgbuttons:false,
			recordtext:'{0} - {1} 共 {2} 条',
			jsonReader: {
				root:"data",
				repeatitems : false,
				id:'pt_id'
			},
			loadComplete: function(data){
			}
	    });
	},
	addEvent:function(){
		$("#pt_paycode").keyup(function(e){
			if(e.keyCode == 13){//回车事件
				UTIL.doPay();
			}
		});
		$("#btn_ok").click(function(e){
			UTIL.doPay();
		});
		$("#btn_close").click(function(e){
			api.close();
		});
		$("#pt_money").keyup(function(e){//如果金额改变，回车则更新二维码
			var pay_money = $(this).val();
			if (!config.REG_DOUBLE.test(pay_money)){
				$(this).val(w_pay_money).focus().select();
				return;
			}
		});
		$("#btn-query").on("click",function(e){
			/*var pt_number = $("#pt_number").val();
			if(null != pt_number && "" != pt_number){
				THISPAGE.queryPay(pt_number);
			}*/
		});
		$("#btn_clear").on("click",function(e){
			UTIL.delPayTemp(0,"many");
		});
		$("#btn_del").on("click",function(e){
			var rowId = $('#grid').jqGrid('getGridParam','selrow');
			UTIL.delPayTemp(rowId,"one");
		});
	},
	queryPay:function(number){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"cash/payTemp",
			data:{"pt_number":number},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data){
					var result = data.data;
					if(data.stat == 200){
						if(result.result_code == "PAY_SUCCESS"){
							var pdata ={flag:1,third_money:$("#pt_money").val()};
							if(callback && typeof callback == 'function'){
								callback(pdata,W,window);
								setTimeout("api.close()",100);
							}
						}else{
							W.Public.tips({type: 2, content :result.error_message});
							setTimeout("api.zindex()",200);
							return;
						}
					}else{
						W.Public.tips({type: 2, content :data.message});
						setTimeout("api.zindex()",200);
						return;
					}
				}
			}
		});
	}
};
THISPAGE.init();