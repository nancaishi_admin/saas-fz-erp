var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var api = frameElement.api, W = api.opener;
$(function(){
	$("#cd_cardcode").focus();
	THISPAGE.init();
});
var UTIL = {
	operFormater:function(val, opt, row){
    	var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-dele" title="删除">&#xe60e;</i>';
		html_con += '</div>';
    	return html_con;
	}
};
var THISPAGE = {
	init:function(){
		this.initGrid();
		this.addEvent();
	},
	initGrid:function(){
		var colModel = [
            {name: 'opera', index: '',label:'操作',width:50, title: false,formatter: UTIL.operFormater},
	    	{name: 'cd_cardcode', index: 'cd_cardcode',label:'卡号', width:185, title: false},	    
	    	{name: 'cd_money', index: 'cd_money',label:'余额', width:80,align:'right',title: false},
	    	{name: 'cd_used_money', index:'cd_used_money',label:'使用金额', width:80,align:'right', title: false,editable:true},
	    	{name: 'cd_id', index:'cd_id',label:'ID',hidden:true, title: false},
	    	{name: 'cd_cashrate', index:'cd_cashrate',label:'现金率',hidden:true, title: false}
	    ];
		$('#grid').jqGrid({
			datatype: 'json',
			width: 465,
			height:200,
			gridview: true,
			colModel: colModel,
			rownumbers: true,
			multiselect:false,
			viewrecords: true,
			rowNum:999,
			shrinkToFit:false,
			autoScroll: true,
			footerrow :true,
			userDataOnFooter : true,
			cellEdit: true,
			cellsubmit: 'clientArray',
			jsonReader: {
				repeatitems : false,
				userData:"userData",
				id:'cd_id'
			},
			loadComplete: function(data){
			},
			gridComplete:function(data){
				THISPAGE.buildMoney();
			},
			beforeSaveCell:function(rowid, cellname, value, iRow, iCol){		
				var cd_money = $("#grid").jqGrid("getRowData", rowid).cd_money;
				if(parseFloat(value)<=parseFloat(cd_money)){
					return value;
				}else{
					W.RXDialog.commonTip('manyMoney','使用金额超过余额!','320','60','1');
					setTimeout("api.zindex()",500);
					return cd_money;
				}
			},
			afterSaveCell:function(rowid, cellname, value, iRow, iCol){
				THISPAGE.buildMoney();
			}
	    });
	},
	buildMoney:function(){
		var grid=$('#grid'),data=grid.getRowData();
		var totalMoney = 0;
		for(var i=0;i<data.length;i++){
			totalMoney += parseFloat(data[i].cd_used_money);
		}
		var userData = {};
		userData.cd_cardcode = "合计";
		userData.cd_used_money=totalMoney;
		$("#grid").footerData("set", userData);
	},
	query:function(){
		var card_code = $("#cd_cardcode").val();
		if(null != card_code && "" != card_code){
			$.ajax({
				type:"POST",
				url:config.BASEPATH+"cash/card/queryByCode",
				data:"cd_cardcode="+card_code+"&cd_pass="+$("#cd_pass").val(),
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						var pdata = data.data;
						if(pdata.cd_money != undefined && pdata.cd_money != null){
							$("#cd_money").val(pdata.cd_money);
							$("#cd_cashrate").val(pdata.cd_cashrate);
							$("#cd_usemoney").focus().select();
							$("#used_code").val(card_code);
							$("#cd_id").val(pdata.cd_id);
						}else{
							W.Public.tips({type: 2, content : "卡号与密码不正确!"});
							setTimeout("api.zindex()",500);
							$("#cd_cardcode").focus().select();
							$("#used_code").val("");
							$("#cd_id").val("");
							return;
						}
					}else{
						W.Public.tips({type: 2, content : "卡号与密码不正确!"});
						setTimeout("api.zindex()",500);
						$("#cd_cardcode").focus().select();
						$("#used_code").val("");
						$("#cd_id").val("");
						return;
					}
					
				}
			});
		}else{
			W.Public.tips({type: 2, content : "卡号与密码不正确!"});
			setTimeout("api.zindex()",500);
			return;
		}
	},
	addEvent:function(){
		$("#cd_pass").keyup(function(event){
			if(event.keyCode== 13){
				THISPAGE.query();
			}
		});
		$("#cd_usemoney").keyup(function(event){
			if(event.keyCode== 13){
				THISPAGE.add();
			}
		});
		$('#grid').on('click', '.operating .ui-icon-dele', function(e){
			e.preventDefault();
			var id = $(this).parent().data('id');
			THISPAGE.del(id);
		});
		$("#btn-save").click(function(){
			THISPAGE.submit();
		});
	},
	flush:function(){
		$("#cd_money").val("");
		$("#cd_usemoney").val("");
		$("#cd_pass").val("");
		$("#cd_id").val("");
		$("#cd_cardcode").val("").focus();
	},
	del:function(rowId){
		$('#grid').jqGrid('delRowData', rowId);
	},
	add:function(){
		var cardData = $("#grid").getRowData();
		if(cardData.length == 5){
			W.Public.tips({type: 2, content : "同时只能用5张卡!"});
			setTimeout("api.zindex()",500);
			return;
		}
		var used_code = $("#used_code").val();
		var cd_id = $("#cd_id").val();
		var cd_cardcode = $.trim($("#cd_cardcode").val()); 
		if(used_code != null && used_code != cd_cardcode){
			W.Public.tips({type: 2, content : "请重输入密码!"});
			setTimeout("api.zindex()",500);
			$("#cd_pass").focus().select();
			return;
		}
		var cd_usemoney = $("#cd_usemoney").val();
		var cd_money = $.trim($("#cd_money").val());
		var cd_cashrate = $("#cd_cashrate").val();
		if (cd_cardcode.length == 0 ){
			W.Public.tips({type: 2, content : "卡号不能为空!"});
			setTimeout("api.zindex()",500);
			$("#cd_cardcode").focus().select();
	        return;
		}
		if(cd_money == null || cd_money == ""){
			W.Public.tips({type: 2, content : "回车查询余额!"});
			setTimeout("api.zindex()",500);
			$("#cd_cardcode").focus().select();
			return;
		}
		if (!config.REG_DOUBLE.test(cd_usemoney)){
			W.Public.tips({type: 2, content : "余额输入错误!"});
			setTimeout("api.zindex()",500);
		    $("#cd_usemoney").focus().select();
	        return;
		}
		if(isNaN(cd_usemoney)){
			W.Public.tips({type: 2, content : "请输入余额!"});
			setTimeout("api.zindex()",500);
	        $("#cd_usemoney").focus().select();
	        return;
	    }
		if (parseFloat(cd_money) < parseFloat(cd_usemoney)){
			W.Public.tips({type: 2, content : "卡内余额不足!"});
			setTimeout("api.zindex()",500);
		    $("#cd_usemoney").focus().select();
	        return;
		}
		for(var i=0;i<cardData.length;i++){
			var _cd_cardcode = cardData[i].cd_cardcode;
			if(_cd_cardcode == cd_cardcode){
				W.Public.tips({type: 2, content : "同一张卡不能重复使用!"});
				setTimeout("api.zindex()",500);
	      		$("#cd_cardcode").focus().select();
				return;
			}
		}
		var cardjson = {cd_id:cd_id,cd_cardcode:cd_cardcode,cd_money:cd_money,cd_used_money:cd_usemoney,cd_cashrate:cd_cashrate};
		$("#grid").jqGrid('addRowData', cd_id, cardjson, 'first');
		THISPAGE.flush();
		$("#used_code").val("");
	}
};
function submit(){
	var grid=$('#grid'),rows=grid.getRowData();
	if(null != rows && rows.length > 0){
		var totalMoney = 0,cd_ids="",cd_money="",cashrate = 0,total_cash = 0;
		for(var i=0;i<rows.length;i++){
			totalMoney = parseFloat(parseFloat(totalMoney)+parseFloat(rows[i].cd_used_money)).toFixed(2);
			total_cash = parseFloat(parseFloat(total_cash)+parseFloat(rows[i].cd_used_money)*parseFloat(rows[i].cd_cashrate)).toFixed(2);
			cd_ids += rows[i].cd_id+",";
			cd_money += rows[i].cd_used_money+",";
		}
		cashrate = parseFloat(total_cash/totalMoney).toFixed(2);
		var pdata={};
		pdata.cd_ids=cd_ids;
		pdata.cd_money=cd_money;
		pdata.cd_rate=cashrate;
		pdata.sh_cd_money=totalMoney;
		return pdata;
	}else{
		W.Public.tips({type: 2, content : "储值卡未加入使用列表，请加入!"});
		return false;
	}
}