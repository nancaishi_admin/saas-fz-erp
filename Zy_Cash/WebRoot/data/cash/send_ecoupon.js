var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var api = frameElement.api, W = api.opener;
var pdata = api.data || {};
var THISPAGE = {
	init:function(){
		this.initDom();
		this.initEvent();
	},
	initDom:function(){
		$("#ecu_name").val(pdata.ecu_name);
		$("#ecu_tel").val(pdata.ecu_tel).select().focus();
	},
	save:function(){
		var ecu_name = $("#ecu_name").val();
		var ecu_tel = $("#ecu_tel").val();
		pdata.ecu_name = ecu_name;
		pdata.ecu_tel = ecu_tel;
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'cash/sendCoupon',
			data:pdata,
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					api.close();
				}else{
					W.Public.tips({type: 1, content : "收银失败"});
				}
			}
		});
	},
	initEvent:function(){
		$("#btn_close").on('click',function(){
			api.close();
		});
		$("#btn-save").on('click',function(){
			THISPAGE.save();
		});
	}
};
THISPAGE.init();