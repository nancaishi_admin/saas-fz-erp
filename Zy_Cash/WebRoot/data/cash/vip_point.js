var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var api = frameElement.api, W = api.opener;
$(function(){
	$("#vip_point").focus();
	THISPAGE.init();
});
var THISPAGE = {
	init:function(){
		this.initDom();
		this.addEvent();
	},
	initDom:function(){
		this.$_ischeck = $("#ischeck").cssCheckbox();
		_self = this;
	},
	addEvent:function(){
		$("#vip_point").keyup(function(event){
			if(event.keyCode== 13){
				THISPAGE.add();
			}
		});
		$("#btn-save").on('click',function(e){
			e.preventDefault();
			var vip_point = $("#vip_point").val();
			var sh_point_money = $("#sh_point_money").val();
			if(null != sh_point_money && "" != sh_point_money && parseFloat(sh_point_money) > 0
					&& null != vip_point && parseFloat(vip_point) > 0){
				api.close();
			}else{
				W.Public.tips({type: 2, content : "未使用积分，请使用!"});
				return false;
			}
		});
		$("#btn_close").on('click',function(){
			api.close();
		});
	},
	add:function(){
		var vip_point = $("#vip_point").val();
		var vm_points = $("#vm_points").val();
		var point_money = $("#point_money").val();
		if(isNaN(vip_point)){
			W.Public.tips({type: 2, content : "请输入数字!"});
			$("#vip_point").focus().select();
			setTimeout("api.zindex()",300);
			return;
		}
		if(vip_point.match(/^[-\+]?\d+$/) == null){
			W.Public.tips({type: 2, content : "请输入整数!"});
			setTimeout("api.zindex()",300);
			return;
		}
		if (parseInt(vip_point) > parseInt(vm_points)){
			W.Public.tips({type: 2, content : "积分不足!"});
			setTimeout("api.zindex()",300);
			return;
		}
		if(parseInt(vip_point) < parseInt(point_money)){
			W.Public.tips({type: 2, content : "积分不足抵现!"});
			$("#vip_point").val("");
			$("#sh_point_money").val("");
			$("#vm_last_point").val("");
			setTimeout("api.zindex()",300);
			return;
		}
		$("#sh_point_money").val(parseInt(parseInt(vip_point)/parseInt(point_money)));
		if (vip_point != "" && vip_point != "0"){
			$("#vm_last_point").val(parseInt(vm_points)-parseInt(vip_point));
		}else{
			$("#vm_last_point").val(parseInt(vm_points));
		}
	}
};
function submit(){
	var vip_point = $("#vip_point").val();
	var sh_point_money = $("#sh_point_money").val();
	var print = _self.$_ischeck.chkVal().join()?1:0;
	var pdata={};
	pdata.vm_cardcode=$("#vm_cardcode").val();
	pdata.vm_name=$("#vm_name").val();
	pdata.vm_points=$("#vm_points").val();
	pdata.vm_last_point=$("#vm_last_point").val();
	pdata.vip_point=vip_point;
	pdata.sh_point_money=sh_point_money;
	pdata.isprint=print;
	return pdata;
}