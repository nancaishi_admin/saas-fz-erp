var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var api = frameElement.api, W = api.opener;
var vipInfo = api.data.vipInfo;
var handle = {
	doVipRate:function(){
		var vip_code = $.trim($("#vip_code").val());
		if(null == vip_code || '' == vip_code){
			W.Public.tips({type: 2, content : "请输入卡号或手机号!"});
			return;
		}
		var vip_other = _self.$_vip_other.chkVal().join()?1:0;
		var sell_type = W.$("#sell_type").val();
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"cash/vipRate",
			data:{"vip_code":vip_code,"vip_other":vip_other,"sell_type":sell_type},
			cache:false,
			dataType:"json",
			success:function(data){
				if (data.stat == 200){
					W.flag = true;
					if(vipInfo && typeof vipInfo == 'function'){
						vipInfo(data.data);
					}
					setTimeout("api.close()",200);
				}else if(data.stat == 404){
					W.flag = false;
					W.Public.tips({type:1,content:'会员不存在!'});
					setTimeout(function(){
						$("#vip_code").select().focus();
					},200);
				}else{
					W.flag = false;
					W.Public.tips({type:1,content:'打折失败!'});
					setTimeout(function(){
						$("#vip_code").select().focus();
					},200);
				}
			}
		});
	}
};
var THISPAGE = {
	init:function(){
		this.initDom();
		this.initEvent();
	},
	initDom:function(){
		_self = this;
		_self.$_vip_other = $("#vip_other").cssCheckbox();
		$("#vip_code").focus();
	},
	initEvent:function(){
		$("#vip_code").on('keyup',function(event){
			if(event.keyCode == 13){
				$("#btn-save").click();
			}
		});
		$("#btn-save").on('click',function(){
			handle.doVipRate();
		});
		$("#btn_close").on('click',function(){
			api.close();
		});
	}
};
THISPAGE.init();
