var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var api = frameElement.api, W = api.opener;
var queryurl = config.BASEPATH+"ecoupon/queryByCode";
var UTIL = {
	operFormater:function(val, opt, row){
    	var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-use" title="使用">&#xe618;</i>';
		html_con += '</div>';
    	return html_con;
	},
	checkFormat:function(val, opt, row){
		var check = row.ecu_check_no;
		if(undefined != check && null != check && "" != check){
			if(check.length > 2){
				check = check.substr(0,2)+"**";
				return check;
			}
			return "";
		}
		return "";
	},
	useFormat:function(val, opt, row){
		var use_state = row.use_state;
		if(use_state == 1){
			return "可用";
		}else{
			return "不可用";
		}
	}
};
var THISPAGE = {
	init:function(){
		this.initGrid();
		this.initCom();
		this.addEvent();
	},
	initCom:function(){
		$("#ecu_tel").focus();
	},
	initGrid:function(){
		var colModel = [
	    	{name: 'ecu_code', index: 'ecu_code',label:'券号', width:90, title: false},
	    	{name: 'ecu_id', index: 'ecu_id',label:'id', hidden:true},
	    	{name: 'use_model', index: 'use_model',label:'使用范围类型', hidden:true},
	    	{name: 'use_state', index: 'use_state',label:'使用状态', hidden:true},
	    	{name: 'ecu_money', index: 'ecu_money',label:'金额', width:50,align:'right',title: false},
	    	{name: '_state', index: '_state',label:'状态', width:60,formatter: UTIL.useFormat,title: false},
	    	{name: 'ecu_limitmoney', index:'ecu_limitmoney',label:'满用', width:50,align:'right', title: false},
	    	{name: 'ecu_enddate', index:'ecu_enddate',label:'到期时间',width:80, title: false},
	    	{name: 'ecu_check_no', index: 'ecu_check_no',label:'验证码', hidden:true},	 
	    	{name: 'check_no', index: 'check_no',label:'验证码', width:55, title: false,formatter: UTIL.checkFormat},	 
	    	{name: 'input_check', index: 'input_check',label:'输入验证', width:60,editable:true},
	    	{name: 'opera', index: '',label:'使用',width:30, title: false,formatter: UTIL.operFormater}
	    ];
		$('#grid').jqGrid({
			datatype: 'json',
			width: 565,
			height:200,
			gridview: true,
			colModel: colModel,
			rownumbers: true,
			multiselect:false,
			viewrecords: true,
			rowNum:999,
			shrinkToFit:false,
			autoScroll: true,
			footerrow :true,
			userDataOnFooter : true,
			cellEdit: true,
			cellsubmit: 'clientArray',
			jsonReader: {
				root:'data',
				repeatitems : false,
				id:'ecu_id'
			},
			loadComplete: function(data){
			}
	    });
	},
	loadData:function(tel){
		var params = 'ecu_tel='+tel;
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+params}).trigger("reloadGrid");
	},
	addEvent:function(){
		$("#ecu_tel").keyup(function(event){
			if(event.keyCode== 13){
				THISPAGE.query();
			}
		});
		$("#btn-search").on('click',function(){
			THISPAGE.query();
		});
		$('#grid').on('click', '.operating .ui-icon-use', function(e){
			e.preventDefault();
			var id = $(this).parent().data('id');
			THISPAGE.useEcoupon(id);
		});
	},
	query:function(){
		var ecu_tel = $("#ecu_tel").val();
		if(null != ecu_tel && "" != ecu_tel){
			THISPAGE.loadData(ecu_tel);
		}else{
			W.Public.tips({type: 2, content : "请输入手机号!"});
			setTimeout("api.zindex()",500);
			return;
		}
	},
	useEcoupon:function(id){
		var data = $("#grid").jqGrid("getRowData", id);
		var check_no = data.ecu_check_no,input_check = data.input_check;
		var use_state = data.use_state;
		if(use_state != 1){
			W.Public.tips({type: 2, content : "条件不满足，不可用!"});
			return;
		}
		if(null != check_no && "" != check_no){
			if(check_no == input_check){
				//验证使用范围和使用金额限制
			}else{
				W.Public.tips({type: 2, content : "验证码不正确!"});
				return;
			}
		}else{
			W.Public.tips({type: 2, content : "请输入验证码!"});
			return;
		}
		W.cashData.content.$("#ecu_id").val(data.ecu_id);
		W.cashData.content.$("#sh_ec_money").val(data.ecu_money);
		setTimeout("api.close()",200);
	}
};
/*function submit(){
	var selectedId = $("#grid").jqGrid('getGridParam', 'selrow');
	if(selectedId == null || selectedId == ""){
		Public.tips({type: 2, content : "请选择优惠券！"});
		return false;
	}
	var rowData = $("#grid").jqGrid("getRowData", selectedId);
	return rowData;
}*/
$(function(){
	THISPAGE.init();
});