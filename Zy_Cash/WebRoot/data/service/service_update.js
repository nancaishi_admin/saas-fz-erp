var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;;
var handle = {
	update:function(state,ss_state,ss_id){
		var ask="";
		if(state=="1"){
			ask="确认取货?";
		}
		$.dialog.confirm(ask, function(){
			var url="";
			if(ss_state=="1"){
				var ss_satis= $("#ss_satis").val();
				url="?ss_satis="+ss_satis+"&ss_id="+ss_id+"&ss_state="+ss_state;
			}else{
				W.Public.tips({type: 2, content : "请确认已经维修！"});
				return;
			}
			url = encodeURI(encodeURI(url)); 
			$("#btn-save").attr("disabled",true);
			$.ajax({
				type:"POST",
				url:config.BASEPATH+"service/update"+url,
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						W.Public.tips({type: 3, content : data.message});
						setTimeout("api.zindex()",500);
						W.isRefresh = true;
						api.close();
					}else{
						W.Public.tips({type: 1, content : data.message});
					}
					$("#btn-save").attr("disabled",false);
				}
			});
		}, function(){
		   return;
		});
	},
	satisCombo:function(){
		var satisInfo = {
				data:{items:[
					{satis_Code:'非常满意',satis_Name:'非常满意'},
					{satis_Code:'满意',satis_Name:'满意'},
					{satis_Code:'一般',satis_Name:'一般'},
					{satis_Code:'不满意',satis_Name:'不满意'},
					{satis_Code:'非常不满意',satis_Name:'非常不满意'},
					]}
		}
		satisCombo = $('#span_satis').combo({
			data:satisInfo.data.items,
			value: 'satis_Code',
			text: 'satis_Name',
			width : 142,
			height : 80,
			listId:'',
			defaultSelected: 0,
			editable: true,
			callback:{
				onChange: function(data){
					$("#ss_satis").val(data.satis_Name);
				}
			}
		}).getCombo();
	},
};
var THISPAGE = {
	init:function (){
		this.initParam();
		this.initDom();
		this.initEvent();
	},
	initParam:function(){
	},
	initDom:function(){
		handle.satisCombo();
	},
	initEvent:function(){
		$("#btn_close").click(function(){
			W.isRefresh = false;
			api.close();
		});
	}
};
THISPAGE.init();