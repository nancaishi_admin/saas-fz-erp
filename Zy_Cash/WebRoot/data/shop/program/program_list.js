var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var isRefresh = false;
var api = frameElement.api, W = api.opener;
var queryurl = config.BASEPATH+'shop/program/list';
var _height = api.config.height-132,_width = api.config.width-34;//获取弹出框弹出宽、高

var handle = {
	toView:function(id,sp_id){
		var url = config.BASEPATH+"shop/program/to_view?sps_id="+id+"&sp_id="+sp_id;
		var data = {callback: this.callback};
		$.dialog({
			title : false,
			content : 'url:'+url,
			data: data,
			max : false,
			min : false,
			cache : false,
			lock: false,
			close:function(){
		   		if(isRefresh){
		   			THISPAGE.reloadData();
			   	}
		   	}
		}).max();
	},
	operFmatter : function(val, opt, row){
		var btnHtml = '';
        btnHtml += '<input type="button" value="查看" class="btn_sp" onclick="javascript:handle.toView(\'' + row.sps_id + '\',\'' + row.sp_id + '\');" />';
		return btnHtml;
	},
	sp_state : function(val, opt, row){
		if(val == '0'){
			return '未阅读';
		} else if(val == '1'){
			return '已阅读';
		} else {
			return '';
		}
	}
}
 


var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom: function(){
		this.$_date = $('#date');
	 	this.$_date.cssRadio(
		 	{callback:
		 		function($_obj){  //$_obj是radio外面的lable
		 			dateRedioClick($_obj.find("input").attr("id"));
		 		}
		 	}
	 	);
	 	this.$_satae = $('#satae');
	 	this.$_satae.cssRadio(
		 	{callback:
		 		function($_obj){  //$_obj是radio外面的lable
		 			$("#sp_state").val($($_obj).find('input').val());
		 		}
		 	}
	 	);
	},
	initGrid:function (){
		var gridWH = Public.setGrid();
		var colModel = [
		    {name: 'sp_id', label:'',index: 'sp_id', hidden:true},
		    {name: 'sps_id', label:'',index: 'sps_id', hidden:true},
		    {name: 'sp_imgpath', label:'',index: 'sp_imgpath', hidden:true},
	    	{name: 'operate', label:'操作', width: 60, fixed:true,title: false,formatter: handle.operFmatter, sortable:false},
	    	{name: 'sp_shop_name', label:'门店名称', index: 'sp_shop_name', width: 140, title: false,fixed:true},
	    	{name: 'sp_title', label:'方案标题', index: 'sp_title', width: 140, title: false},
	    	{name: 'sps_state', label:'状态', index: 'sps_state', width: 80, title: false,align:'center',formatter:handle.sp_state},
	    	{name: 'sp_us_name', label:'发布人', index: 'sp_us_name', width: 100, title: false,align:'left'},
	    	{name: 'sp_sysdate', label:'发布时间', index: 'sp_sysdate', title: false,align:'center'},
	    ];
	    $("#grid").jqGrid({
	    	url:queryurl,
			//loadonce:true, //只从后台加载一次数据，datatype被修改为local,如果弹出框想要查询则再开启为json
			datatype: 'json',
			width: _width,
			height: _height,
			altRows:true,
			gridview: true,
			onselectrow: false,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			multiselect:false,//多选
			viewrecords: true,
			pgbuttons:true,
			rowNum:30,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			shrinkToFit:false,//表格是否自动填充
			autoScroll: true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id:'sps_id'
			}
	    });
	},
	buildParams:function(){
		var begindate = $("#begindate").val();//开始日期		
		var enddate = $("#enddate").val();//结束日期		
		var sp_state = $("#sp_state").val();
		var sp_shop_code = $("#sp_shop_code").val();
		var params = '';
		params += 'begindate='+begindate;
		params += '&enddate='+enddate;
		params += '&sp_state='+sp_state;
		params += '&sp_shop_code='+sp_shop_code;
		return params;
	},
	reset:function(){
		$("#shop_name").val("");
		$("#sp_shop_code").val("");
		$("#sp_stateAll").click();
		$("#sp_state").val("");
		$("#theDate").click();
		$("#begindate").val("");
		$("#enddate").val("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
		//重置
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
};
THISPAGE.init();