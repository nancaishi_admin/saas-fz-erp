var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var queryurl = config.BASEPATH+'shop/kpiassess/list';
var api = frameElement.api;
var _height = api.config.height-142,_width = api.config.width-34;//获取弹出框弹出宽、高

var handle = {
	operate: function(oper, id){//修改、新增
		if(oper == 'view'){
			url = config.BASEPATH+"shop/kpiassess/to_view?ka_id="+id;
			data = {oper: oper,callback: this.callback};
		}else if(oper == 'complete'){
			url = config.BASEPATH+"shop/kpiassess/to_view?ka_id="+id;
			data = {oper: oper,callback: this.callback};
		}else{
			return false;
		}
		$.dialog({
			title : false,
			content : 'url:'+url,
			data: data,
			max : false,
			min : false,
			cache : false,
			lock: false
		}).max();
	},
	del: function(rowId){//删除
	 	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
			$.ajax({
				type:"POST",
				url:config.BASEPATH+'shop/kpiassess/del',
				data:{"ka_id":rowId},
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : '删除成功!'});
						$('#grid').jqGrid('delRowData', rowId);
					}else{
						Public.tips({type: 1, content : data.message});
					}
				}
			});
		});	
    },
    callback: function(data, oper, dialogWin){
		if(oper == "complete") {
			$("#grid").jqGrid('setRowData', data.id, data);
			dialogWin && dialogWin.api.close();
		}
		if(oper == "view") {
			$("#grid").jqGrid('setRowData', data.id, data);
			dialogWin && dialogWin.api.close();
		}
		if(oper == "add"){
			THISPAGE.reloadData();
			dialogWin && dialogWin.api.close();
		}
	},
	operFmatter : function(val, opt, row){
		var btnHtml = '';
		btnHtml += '<input type="button" value="查看" class="btn_ck" onclick="javascript:handle.operate(\'view\',' + opt.rowId + ');" />';
		return btnHtml;
	},
	formatState:function(val, opt, row){
		if(val == 0){
			return '未总结';
		}else if(val == 1){
			return '已总结';
		}
		return val;
	},
	formatType:function(val, opt, row){
		if(val == 0){
			return '店铺';
		}else if(val == 1){
			return '员工组';
		}else if(val == 2){
			return '员工';
		}
		return val;
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_ka_type = $("#td_ka_type").cssRadio({ callback: function($_obj){
			$("#ka_type").val($_obj.find("input").val());
			THISPAGE.reloadData();
		}});
	},
	initGrid:function(){
		var colModel = [
	    	{label:'操作',name: 'operate',width: 60, formatter: handle.operFmatter,align:'center', sortable:false},
	    	{label:'',name: 'ka_us_id',index: 'ka_us_id',width:100,hidden:true},
	    	{label:'单据编号',name: 'ka_number',index: 'ka_number',width:150},
	    	{label:'考核范围',name: 'ka_type',index: 'ka_type',width:100,formatter: handle.formatType,align:'center'},
	    	{label:'考核对象',name: 'kal_name',index: '',width:180,title:true},
	    	{label:'开始时间',name: 'ka_begin',index: 'ka_begin',width:80},
	    	{label:'结束时间',name: 'ka_end',index: 'ka_end',width:80}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.BASEROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:false,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id: 'ka_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'ka_type='+$("#ka_type").val();
		return params;
	},
	reset:function(){
		THISPAGE.$_ka_type.setValue(0);
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
	}
}
THISPAGE.init();