var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var isRefresh = false;
var ac_number = $("#ac_number").val();
var ac_out_shop = $("#ac_out_shop").val();
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var queryurl = config.BASEPATH+'sell/allocate/detail_list/'+ac_number+'/'+ac_out_shop;
var querysumurl = config.BASEPATH+'sell/allocate/detail_sum/'+ac_number+'/'+ac_out_shop;
var querysizeurl = config.BASEPATH+'sell/allocate/detail_size/'+ac_number+'/'+ac_out_shop;

var needListRefresh = false;//列表模式
var needSizeRefresh = false;//尺码模式
var needSumRefresh = false;//汇总模式
var _height = $(parent).height()-280,_width = $(parent).width()-2;
var showMode = {
	display:function(mode){
		$("#CurrentMode").val(mode);
		$("a[name='mode-btn']").removeClass("on");
		$("#list-grid").hide();
		$("#size-grid").hide();
		$("#sum-grid").hide();
		if(mode == 0){
			$("#listBtn").addClass("on");
			$("#list-grid").show();
			showMode.refreshList();
		}else if(mode == 1){
			$("#sizeBtn").addClass("on");
			$("#size-grid").show();
			showMode.refreshSize();
		}else if(mode == 2){
			$("#sumBtn").addClass("on");
			$("#sum-grid").show();
			showMode.refreshSum();
		}
	},
	refreshList:function(){//列表模式刷新数据
		if(!needListRefresh){
    		return;
    	}
    	THISPAGE.reloadGridData();
    	needListRefresh = false;
	},
	refreshSum:function(){
		if(!needSumRefresh){
			return;
		}
		$('#sumGrid').GridUnload();
		THISPAGE.addEvent();
		THISPAGE.initSumGrid();
		needSumRefresh = false;
	},
	refreshSize:function(){
		if(!needSizeRefresh){
			return;
		}
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'sell/allocate/detail_size_title/'+ac_number,
			cache:false,
			dataType:"json",
            success: function (data) {
            	if(undefined != data && data.stat == 200){
                    var titles = data.data.titles;
                    var sizeCodeMap = data.data.sizeCodeMap;
                    $('#sizeGrid').GridUnload();
                    THISPAGE.initSizeGroupGrid(titles, sizeCodeMap);
                    THISPAGE.addEvent();
                    needSizeRefresh = false;
				}else{
					Public.tips({type: 1, content : data.message});
				}
            }
        });
	},
	refresh:function(){
		var currentMode = $("#CurrentMode").val();
    	if(currentMode == '0'){//列表模式
    		needListRefresh = true;
    		showMode.refreshList();
        	needSizeRefresh = true;
        	needSumRefresh = true;
    	}else if(currentMode == '1'){//尺码模式
    		needSizeRefresh = true;
    		showMode.refreshSize();
    		needListRefresh = true;
        	needSumRefresh = true;
    	}else if(currentMode == '2'){//汇总模式
    		needSumRefresh = true;
    		showMode.refreshSum();
    		needListRefresh = true;
        	needSizeRefresh = true;
    	}
	}
};

var handle = {
	doSend:function(){
		$.dialog.confirm('调出单据将减少店铺相应库存，确定要调出吗？', function(){
			$("#btn-send").attr("disabled",true);
			var params = "";
			params += "number="+ac_number;
			params += "&ac_out_dp="+$("#ac_out_dp").val();
			$.ajax({
	            type: "POST",
	            url: config.BASEPATH+"sell/allocate/send",
	            data: params,
	            cache:false,
				dataType:"json",
	            success: function (data) {
	            	if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : "调出成功"});
						handle.loadData(data.data);
					}else{
						Public.tips({type: 1, content : data.message});
					}
					$("#btn-send").attr("disabled",false);
	            }
	        });
		});
	},
	doReceive:function(){
		$.dialog.confirm('调入单据将增加店铺相应库存，确定要调入吗？', function(){
			$("#btn-receive").attr("disabled",true);
			var params = "";
			params += "number="+ac_number;
			params += "&ac_in_dp="+$("#ac_in_dp").val();
			$.ajax({
	            type: "POST",
	            url: config.BASEPATH+"sell/allocate/receive",
	            data: params,
	            cache:false,
				dataType:"json",
	            success: function (data) {
	            	if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : "调入成功"});
						handle.loadReceiveData(data.data);
					}else{
						Public.tips({type: 1, content : data.message});
					}
					$("#btn-receive").attr("disabled",false);
	            }
	        });
		});
	},
	doReject:function(){
		$.dialog.confirm('确定要拒收吗？', function(){
			$("#btn-reject").attr("disabled",true);
			var params = "";
			params += "number="+ac_number;
			$.ajax({
	            type: "POST",
	            url: config.BASEPATH+"sell/allocate/reject",
	            data: params,
	            cache:false,
				dataType:"json",
	            success: function (data) {
	            	if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : "拒收成功"});
						handle.loadData(data.data);
					}else{
						Public.tips({type: 1, content : data.message});
					}
					$("#btn-reject").attr("disabled",false);
	            }
	        });
		});
	},
	doRejectConfirm:function(){
		$.dialog.confirm('确认单据将恢复店铺相应库存，确定要确认单据吗？', function(){
			$("#btn-reject-confirm").attr("disabled",true);
			var params = "";
			params += "number="+ac_number;
			$.ajax({
	            type: "POST",
	            url: config.BASEPATH+"sell/allocate/rejectconfirm",
	            data: params,
	            cache:false,
				dataType:"json",
	            success: function (data) {
	            	if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : "确认成功"});
						handle.loadData(data.data);
					}else{
						Public.tips({type: 1, content : data.message});
					}
					$("#btn-reject-confirm").attr("disabled",false);
	            }
	        });
		});
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.ac_id;
		pdata.ac_state=data.ac_state;
		pdata.ac_out_shop=data.ac_out_shop;
		pdata.ac_in_shop=data.ac_in_shop;
		pdata.operate='';
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	},
	loadReceiveData:function(data){
		var pdata = {};
		pdata.id=data.ac_id;
		pdata.ac_state=data.ac_state;
		pdata.ac_receiver=data.ac_receiver;
		pdata.ac_recedate=data.ac_recedate;
		pdata.operate='';
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	},
	doPrint:function(){
		var paramData = {};
		paramData.title = "单据打印模板";
		paramData.width = "565";
		paramData.height = "500";
		paramData.data = {from:"print",displayMode:$("#CurrentMode").val(),'number':ac_number};
		paramData.url = config.BASEPATH+"set/print/to_list/19";
	    Public.openDialog(paramData);
	},
	operFmatter :function(val, opt, row){
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-image" title="图片">&#xe654;</i>';
		html_con += '</div>';
		return html_con;
	},
	formatSellMoney :function(val, opt, row){
		if($.trim(val) != ""){
			return val;
		}
		return Public.formatMoney(row.acl_amount*row.acl_sell_price);
	}
};

var Utils = {
	doQueryInDepot : function(){
		commonDia = $.dialog({
			title : '选择调入仓库',
			content : 'url:'+config.BASEPATH+'base/depot/to_list_dialog_bymodule',
			data : {multiselect:false,module:'sell_allocate',sp_code:$("#ac_in_shop").val()},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#ac_in_dp").val(selected.dp_code);
				$("#indepot_name").val(selected.dp_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#ac_in_dp").val(selected.dp_code);
					$("#indepot_name").val(selected.dp_name);
				}
			},
			cancel:true
		});
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.addEvent();
		this.initGrid();
		this.initEvent();
	},
	initDom:function(){
		needListRefresh = false;//列表模式
        needSizeRefresh = true;//尺码模式
        needSumRefresh = true;//汇总模式
        var ac_state = $("#ac_state").val();
        if('out' == api.data.oper){
			$("#btn-send").show();
		}
        if('in' == api.data.oper){
        	$("#btn-receive").show();
        	$("#btn-reject").show();
        }
        if('confirm' == api.data.oper){
			$("#btn-reject-confirm").show();
		}
	},
	gridTotal:function(){
    	var grid=$('#grid');
		var acl_amount=grid.getCol('acl_amount',false,'sum');
    	var acl_sell_money=grid.getCol('acl_sell_money',false,'sum');
    	grid.footerData('set',{pd_no:'合计：',acl_amount:acl_amount,acl_sell_money:Public.formatMoney(acl_sell_money)});
    	$("#ac_amount").val(acl_amount);
    	$("#ac_sell_money").val(Public.formatMoney(acl_sell_money));
    },
    sumGridTotal:function(){
    	var grid=$('#sumGrid');
    	var acl_amount=grid.getCol('acl_amount',false,'sum');
    	var acl_sell_money=grid.getCol('acl_sell_money',false,'sum');
    	grid.footerData('set',{pd_no:'合计：',acl_amount:acl_amount,acl_sell_money:Public.formatMoney(acl_sell_money)});
    	$("#ac_amount").val(acl_amount);
    	$("#ac_sell_money").val(Public.formatMoney(acl_sell_money));
    },
	initGrid:function(){
		var colModel = [
		    {label:'操作',name:'operate', width: 40, fixed:true, formatter: handle.operFmatter, sortable:false,align:'center'},
		    {label:'',name: 'acl_pd_code', index: 'acl_pd_code', hidden:true},
	    	{label:'商品货号',name: 'pd_no', index: 'pd_no', width: 100},
	    	{label:'商品名称',name: 'pd_name', index: 'pd_name', width: 100},
	    	{label:'颜色',name: 'cr_name', index: 'cr_name', width: 60},
	    	{label:'尺码',name: 'sz_name', index: 'sz_name', width: 60},
	    	{label:'杯型',name: 'br_name', index: 'br_name', width: 60},
	    	{label:'单位',name: 'pd_unit', index: 'pd_unit', width: 60},
	    	{label:'数量',name: 'acl_amount', index: 'acl_amount', width: 70,align:'right',sorttype: 'int'},
	    	{label:'零售价',name: 'acl_sell_price', index: 'acl_sell_price', width: 80,align:'right',sorttype: 'float',formatter: Public.formatMoney},
	    	{label:'零售额',name: 'acl_sell_money', index: 'acl_sell_money', width: 80,align:'right',sorttype: 'float',formatter: handle.formatSellMoney},
	    	{label:'备注',name: 'acl_remark', index: 'acl_remark', width: 180}
	    ];
		$('#grid').jqGrid({
			url:queryurl,
			loadonce:true,
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:9999,//每页条数
			cmTemplate: {sortable:true,title:true},
			shrinkToFit:false,//表格是否自动填充
            footerrow: true,
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'acl_id'  //图标ID
			},
			loadComplete: function(data){
				var ids = $("#grid").getDataIDs();
				THISPAGE.gridTotal();
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
				
			}
	    });
	},
	initSumGrid:function(){
		var colModel = [
		    {label:'操作',name:'operate', width: 40, fixed:true, formatter: handle.operFmatter, sortable:false,align:'center'},
		    {label:'',name: 'acl_pd_code', index: 'acl_pd_code', hidden:true},
	    	{label:'商品货号',name: 'pd_no', index: 'pd_no', width: 100},
	    	{label:'商品名称',name: 'pd_name', index: 'pd_name', width: 100},
	    	{label:'品牌',name: 'bd_name', index: 'pd_bd_code', width: 80},
	    	{label:'类别',name: 'tp_name', index: 'pd_tp_code', width: 80},
	    	{label:'单位',name: 'pd_unit', index: 'pd_unit', width: 60},
	    	{label:'数量',name: 'acl_amount', index: 'acl_amount', width: 70,align:'right',sorttype: 'int'},
	    	{label:'单价',name: 'acl_sell_price', index: 'acl_sell_price', width: 80,align:'right',sorttype: 'float',formatter: Public.formatMoney},
	    	{label:'金额',name: 'acl_sell_money', index: 'acl_sell_money', width: 80,align:'right',sorttype: 'float',formatter: handle.formatSellMoney},
	    	{label:'备注',name: 'acl_remark', index: 'acl_remark', width: 180}
	    ];
		$('#sumGrid').jqGrid({
			url:querysumurl,
			loadonce:true,
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#sumPage',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:9999,//每页条数
			cmTemplate: {sortable:true,title:true},
			shrinkToFit:false,//表格是否自动填充
            footerrow: true,
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'acl_id'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.sumGridTotal();
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
			}
	    });
	},
	initSizeGroupGrid:function(dyColNames, sizeCodeMap) {
		var gridWH = Public.setGrid();//操作
        var headers = [];
        var dyns = dyColNames;
        var dySize = dyns.length;
        var dms = [];
        for (var i = 0; i < dyns.length; i++) {
            var dys = dyns[i];
            for (var j = 0; j < dys.length; j++) {
                var dyss = null;
                dyss = dys[j].split('_AND_');
                if (i == 0 && j == 0) {
                    dyns[i][j] = dyss[0];
                }
                headers.push(dyss[1]);
                dms.push({label:dyss[1],name: dyss[0], index: dyss[0], width: 45, title: false, align: 'right', sorttype: 'int', sortable: false});
            }
            break;
        }
		var colModel = [
            {label:'',name: 'sizeGroupCode', hidden: true},
            {label:'',name: 'pd_code', index: 'pd_code', width: 100,hidden:true},
            {label:'',name: 'cr_code', index: 'cr_code', width: 100,hidden:true},
            {label:'',name: 'br_code', index: 'br_code', width: 100,hidden:true},
            {label:'操作',name:'operate', width: 40, fixed:true, formatter: handle.operFmatter, sortable:false,align:'center'},
            {label:'商品货号',name: 'pd_no', index: 'pd_no', width: 100},
	    	{label:'商品名称',name: 'pd_name', index: 'pd_name', width: 100},
	    	{label:'颜色',name: 'cr_name', index: 'acl_cr_code', width: 60},
	    	{label:'杯型',name: 'br_name', index: 'acl_br_code', width: 60},
        ];
        colModel = colModel.concat(dms);
        colModel = colModel.concat([
            {label:'数量',name: 'Amount_Total',	index:'Amount_Total',	width: 70, align: 'right', sorttype: 'int', sortable: false},
            {label:'零售价',name: 'sell_price', index: 'sell_price', width: 80,align:'right',sorttype: 'float',formatter: Public.formatMoney},
	    	{label:'零售金额',name: 'sell_money', index: 'sell_money', width: 80,align:'right',sorttype: 'float',formatter: Public.formatMoney}
	    ]);
        $('#sizeGrid').jqGrid({
			url: querysizeurl,
			loadonce:true,
            datatype: 'json',
            autowidth: true,
            height: gridWH.h-100,
            gridview: true,
            colModel: colModel,
            rownumbers: true,//行号
            pager: '#sizePage',//分页
            pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
            viewrecords: true,
            rowNum: 9999,//每页条数
            shrinkToFit: false,//表格是否自动填充
            footerrow: true,
            userDataOnFooter: true,
			jsonReader: {
				root: 'data.data',
				userdata: 'data.userData',
				repeatitems : false,
				id:'id'
			},
			loadComplete: function(data) {
				
			},
			gridComplete:function(){
				Public.resizeSpecifyGrid('sizeGrid', 215 + (dyns.length * 32), 12);
			},
			loadError: function(xhr, status, error) {
                Public.tips({type: 1, content : '操作失败了哦，服务可能已过期，请按F5刷新！'});
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
			},
			onPaging: function(pgButton) {
				
			},
            onSelectRow: function (rowId, status) {
                var sizeG = $("#sizeGrid").jqGrid("getRowData", rowId).sizeGroupCode;
                var bg = $('[sizeGCode=' + sizeG + ']').css("background");
                if (bg == 'none') {
                    return;
                }
                $("[sizeGCode]").css("background-color", '#eee');
                $('[sizeGCode=' + sizeG + ']').css({ "background": "none", "background-color": "#F8FF94"});
            }
	    });
        if (dySize >= 2) {
        	$('#sizeGrid').jqGrid('setGroupHeaders', {
                useColSpanStyle: true,
                groupHeaders:[]
            });
            $('#sizeGrid').jqGrid("setComplexGroupHeaders", {
                complexGroupHeaders: {startColumnName: dyns[0][0], numberOfColumns: dyns[0].length},
                rowspan: dySize,
                topTitle:true
            });
            for (var key in sizeCodeMap) {
                $('#sizeGrid').jqGrid("setComplexGroupHeaders",{
                    complexGroupHeaders: {startColumnName: dyns[0][0], numberOfColumns: dyns[0].length},
                    rowspan: dySize,
                    sizeGroupCode: key,
                    titleTextGroup:sizeCodeMap[key],
                    topTitle:false
                });
            }
        }else{
        	$('#sizeGrid').jqGrid('setGroupHeaders', {
                useColSpanStyle: true,
                groupHeaders: [{startColumnName: dyns[0][0], numberOfColumns: dyns[0].length, titleText: '尺码'}]
            });
        }
    },
	reloadGridData: function () {
        $("#grid").jqGrid('setGridParam', {datatype: 'json', page: 1, url: queryurl}).trigger("reloadGrid");
    },
	initEvent:function(){
		$('#btn-send').on('click', function(e){
			e.preventDefault();
			handle.doSend();
		});
		$('#btn-receive').on('click', function(e){
			e.preventDefault();
			handle.doReceive();
		});
		$('#btn-reject').on('click', function(e){
			e.preventDefault();
			handle.doReject();
		});
		$('#btn-reject-confirm').on('click', function(e){
			e.preventDefault();
			handle.doRejectConfirm();
		});
		$("#btn_close").click(function(e){
			e.preventDefault();
			api.close();
		});
        $('#grid').on('click', '.operating .ui-icon-image', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
        	Public.openImg($("#grid").jqGrid("getRowData", id).acl_pd_code);
        });
	},
	addEvent:function(){
		//移除事件，防止重复绑定
		$('#sizeGrid').off('click','.operating .ui-icon-image');
		$('#sumGrid').off('click','.operating .ui-icon-image');
        $('#sizeGrid').on('click', '.operating .ui-icon-image', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
        	Public.openImg($("#sizeGrid").jqGrid("getRowData", id).pd_code);
        });
        $('#sumGrid').on('click', '.operating .ui-icon-image', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
        	Public.openImg($("#sumGrid").jqGrid("getRowData", id).acl_pd_code);
        });
	}
};

THISPAGE.init();