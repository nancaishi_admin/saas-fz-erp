var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var queryurl = config.BASEPATH+'sell/pageShop';
var api = frameElement.api, W = api.opener;
var height = $(parent).height()-222,width = $(parent).width()-34;
var cashierData = {},empData = api.data;
var UTIL = {
	FormatPrint:function(val, opt, row){
		if(val == '合计'){
			return val;
		}
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-print" title="补票">&#xe615;</i>';
		html_con += '</div>';
		return html_con;
	},
	FormatState:function(val, opt, row){
		var title = "";
		if(val == 0){
			title = "零";
		}
		if(val == 1){
			title = "退";
		}
		if(val == 2){
			title = "换 ";
		}
		return title;
	},
	FormatMoney:function(val, opt, row){
		if($.trim(row.shl_amount) == '' || $.trim(row.shl_price) == ''){
			return val;
		}
		var amount = row.shl_amount;
		var price = row.shl_price;
		var money = parseFloat(amount*price).toFixed(2);
		return money;
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initCombo();
		this.initGrid();
		this.initEvent();
	},
	initDom:function(){
		$('#begindate').val(config.TODAY);
		$('#enddate').val(config.TODAY);
	},
	initCombo:function(){
		empCombo = $('#spanEmp').combo({
	        value: 'code',
	        text: 'name',
	        width : 158,
			height : 30,
			listId : '',
			editable : false,
			defaultSelected:0,
			callback : {
				onChange : function(data) {
					$("#em_code").val(data.code);
				}
			}
		}).getCombo();
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"cash/emp/listCash?"+Math.random(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					cashierData = data.data;
					empCombo.loadData(cashierData,-1);
				}
			}
		});
	},
	gridTotal:function(){
    	var grid=$('#grid');
		var sh_amount=grid.getCol('sh_amount',false,'sum');
    	var sh_money=grid.getCol('sh_money',false,'sum');
    	var sh_cash=grid.getCol('sh_cash',false,'sum');
    	var sh_lost_money=grid.getCol('sh_lost_money',false,'sum');
    	var sh_ec_money=grid.getCol('sh_ec_money',false,'sum');
    	var sh_vc_money=grid.getCol('sh_vc_money',false,'sum');
    	var sh_cd_money=grid.getCol('sh_cd_money',false,'sum');
    	var sh_bank_money=grid.getCol('sh_bank_money',false,'sum');
    	var sh_mall_money=grid.getCol('sh_mall_money',false,'sum');
    	var sh_point_money=grid.getCol('sh_point_money',false,'sum');
    	var sh_ali_money=grid.getCol('sh_ali_money',false,'sum');
    	var sh_wx_money=grid.getCol('sh_wx_money',false,'sum');
    	var sh_sd_money=grid.getCol('sh_sd_money',false,'sum');
    	grid.footerData('set',{oper:'合计',
    		sh_amount:sh_amount,
    		sh_money:sh_money,
    		sh_cash:sh_cash,
    		sh_lost_money:sh_lost_money,
    		sh_ec_money:sh_ec_money,
    		sh_vc_money:sh_vc_money,
    		sh_cd_money:sh_cd_money,
    		sh_bank_money:sh_bank_money,
    		sh_mall_money:sh_mall_money,
    		sh_point_money:sh_point_money,
    		sh_ali_money:sh_ali_money,
    		sh_wx_money:sh_wx_money,
    		sh_sd_money:sh_sd_money
    		});
    },
	initGrid:function(){
		var colModel = [
        	{name: 'oper',index:'oper',label:'操作',formatter:UTIL.FormatPrint,width:60,align:'center',title: false,sortable:false},
        	{name: 'sh_state',index:'sh_state',label:'类型',formatter:UTIL.FormatState,width:60,align:'center',title: false,sortable:false},
        	{name: 'em_name',index:'em_name',label:'收银员',width:70, title: false},	
	    	{name: 'sh_number',index:'sh_number',label:'单号',width:140,align:'left', title:false},
	    	{name: 'sh_date',index:'sh_date',label:'日期',width:90, align:'left',title:false},
	    	{name: 'sh_amount',index:'sh_amount',label:'数量',width:70, align:'right',title:false},
	    	{name: 'sh_money',index:'sh_money',label:'金额',width:70, align:'right',title:false},
	    	{name: 'sh_cash',index:'sh_cash',label:'现金',width:70, align:'right',title:false},
	    	{name: 'sh_lost_money',index:'sh_lost_money',label:'抹零',width:70,align:'right',title:false},
	    	{name: 'sh_ec_money',index:'sh_ec_money',label:'优惠券',width:70, title: false,align:'right'},
	    	{name: 'sh_vc_money',index:'sh_vc_money',label:'代金券',width:70, title: false,align:'right'},   	
	    	{name: 'sh_cd_money',index:'sh_cd_money',label:'储值卡',width:70,title: false,align:'right'},
	    	{name: 'sh_bank_money',index:'sh_bank_money',label:'银行卡',width:70,align:'right', title:false},
	    	{name: 'sh_mall_money',index:'sh_mall_money',label:'商场卡',width:70,align:'right',title:false},
	    	{name: 'sh_point_money',index:'sh_point_money',label:'积分抵现',width:60,align:'right', title:false},
	    	/*{name: 'lineMoney',index:'SH_LineMoney',label:'红包',width:60,align:'right',title:false},*/
	    	{name: 'sh_ali_money',index:'sh_ali_money',label:'支付宝',width:60, align:'center',title:false},
	    	{name: 'sh_wx_money',index:'sh_wx_money',label:'微支付',width:60, align:'center',title:false},
	    	{name: 'sh_sd_money',index:'sh_sd_money',label:'订金',width:70,align:'right',title:false},
	    	{name: 'sh_remark',index:'sh_remark',label:'备注',width:100,align:'left',title:false}
	    ];
		$('#grid').jqGrid({
			url:'',
			datatype: 'json',
			width: width,
			height: height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.DATAROWNUM,//每页条数
			rowList:config.DATAROWLIST,//分页条数
			shrinkToFit:false,//表格是否自动填充
			footerrow :true,
			userDataOnFooter : true,
			jsonReader: {
				root: 'data.list',
				userdata:'data.data',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id:'sh_id'
			},
			loadComplete: function(data){
				if(undefined != data && undefined != data.data){
					var _data = data.data.list;
					for(key in _data){
						var state = _data[key].sh_state;
						if(state == 1){
							$("#grid").jqGrid('setRowData',_data[key].sh_id,false,{color:'#eb3f5f'});
						}
						if(state == 2){
							$("#grid").jqGrid('setRowData',_data[key].sh_id,false,{color:'#3286b4'});
						}
					}
				}
				THISPAGE.gridTotal();
				
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
				handle.operate('',rowid);
			},
			onPaging: function(pgButton) {
			}
	    });
	},
	initCash:function(){
		$("#grid").jqGrid('GridUnload');
		THISPAGE.initGrid();
	},
	gridListTotal:function(){
    	var grid=$('#grid');
		var shl_amount=grid.getCol('shl_amount',false,'sum');
    	var shl_sell_money=grid.getCol('shl_sell_money',false,'sum');
    	var shl_money=grid.getCol('shl_money',false,'sum');
    	grid.footerData('set',{shl_number:'合计',
    		shl_amount:shl_amount,
    		shl_sell_money:shl_sell_money,
    		shl_money:shl_money
    		});
    },
	initList:function(){
		$("#grid").jqGrid('GridUnload');
		var colModel = [
	    	{name: 'shl_number',label:'单号',index:'shl_number',align:'center',width:140,title: false},
	    	{name: 'shl_date',label:'时间',index:'shl_date',width:110, title: false},   
	    	{name: 'em_name',label:'收银员',index:'em_name',width:90, title: false},
	    	{name: 'main_name',label:'主导购',index:'main_name',width:70, title: false},
	    	{name: 'slave_name',label:'副导购',index:'slave_name',width:70, title: false},
	    	{name: 'vm_name',label:'会员',index:'vm_name',width:100, title: false},
	    	{name: 'pd_no',label:'货号',index:'pd_no',width:110,align:'left',title:false},
	    	{name: 'pd_name',label:'名称',index:'pd_name',width:100, align:'left',title:true},
	    	{name: 'bd_name',label:'品牌',index:'bd_name',width:60, align:'left',title:true},
	    	{name: 'pd_unit',label:'单位',index:'pd_unit',width:60,align:'center', title: false},		    	
	    	{name: 'cr_name',label:'颜色',index:'cr_name',width:50, align:'center',title:true},
	    	{name: 'sz_name',label:'尺码',index:'sz_name',width:60,align:'center', title: false},
	    	{name: 'bs_name',label:'怀型',index:'bs_name',width:50, align:'center',title: false},
	    	{name: 'shl_amount',label:'数量',index:'shl_amount',width:50,align:'right',title: false},
	    	{name: 'shl_sell_price',label:'零售价',index:'shl_sell_price',width:70,align:'right', title:false},
	    	{name: 'shl_sell_money',label:'零售金额',index:'shl_sell_money',width:70,align:'right',formatter:UTIL.FormatMoney,title:false},
	    	{name: 'shl_price',label:'折后价',index:'shl_price',width:70,align:'right',title:true},
	    	{name: 'shl_money',label:'折后金额',index:'shl_money',width:70,align:'right',title:true}
	    ];
		$('#grid').jqGrid({
			datatype: 'json',
			width: width,
			height: height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.DATAROWNUM,//每页条数
			rowList:config.DATAROWLIST,//分页条数
			shrinkToFit:true,//表格是否自动填充
			autoScroll: true,
			footerrow :true,
			userDataOnFooter : true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id:'shl_id'
			},
			loadComplete: function(data){
				if(undefined != data && undefined != data.data){
					var _data = data.data.list;
					for(key in _data){
						var state = _data[key].shl_state;
						if(state == 1){
							$("#grid").jqGrid('setRowData',_data[key].shl_id,false,{color:'#eb3f5f'});
						}
						if(state == 2){
							$("#grid").jqGrid('setRowData',_data[key].shl_id,false,{color:'#3286b4'});
						}
					}
				}
				THISPAGE.gridListTotal();
			}
	    });		
	},
	initParam:function(){		
		var begindate = $('#begindate').val();
		var enddate = $('#enddate').val();
		var em_code = $("#em_code").val();
		var code = $('#code').val();
		var type = $("#query_type").val();
		var param = "";
		param = "begindate=" +begindate
		+"&enddate="+enddate
		+"&type="+type
		+"&em_code="+em_code
		+"&code="+Public.encodeURI(code);
		return param;
	},
	reloadData:function(){
		var params = THISPAGE.initParam();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+"?"+params}).trigger("reloadGrid");
	},
	reloadList:function(){
		var params = THISPAGE.initParam();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+"?"+params}).trigger("reloadGrid");
	},
	print:function(id){
		$.ajax({
            type: "POST",
            url: config.BASEPATH+"sell/print",
            data: {sh_id:id},
            cache:false,
			dataType:"json",
            success: function (data) {
            	if(undefined != data && data.stat == 200){
            		var unit = "mm";
					var html = data.data.html;
					var page_height = data.data.page_height+unit;
					var page_width = data.data.page_width+unit;
					var page_title = data.data.page_title;
					var sh_number = data.data.number;
					LODOP.SET_LICENSES(config.PRINT_NAME,config.PRINT_KEY,"","");
					LODOP.PRINT_INIT(page_title);
					LODOP.SET_PRINT_PAGESIZE(0,page_width,page_height,"");
					LODOP.ADD_PRINT_BARCODE("5mm","5mm","60mm","10mm","128Auto",sh_number);
					LODOP.SET_PRINT_STYLEA(0,"ShowBarText",1);
					LODOP.ADD_PRINT_HTM("22mm","0mm","100%","100%",html);
					LODOP.SET_PRINT_STYLEA(0,"ItemType",1);// ItemType的值：数字型，0--普通项 1--页眉页脚 2--页号项 3--页数项 4--多页项
					LODOP.SET_PRINT_MODE("PRINT_PAGE_PERCENT","Auto-Width");//整宽不变形
					LODOP.SET_SHOW_MODE("LANDSCAPE_DEFROTATED", 1);
					LODOP.PREVIEW();
				}else{
					W.Public.tips({type: 1, content : data.message});
				}
            }
        });
	},
	reset:function(){
		THISPAGE.initDom();
		$("#code").val("");
		$("#em_code").val("");
		$("#bd_name").val("");
		$("#bd_code").val("");
		$("#pd_name").val("");
		$("#pd_code").val("");
		empCombo.selectByIndex(-1,false);
	},
	initEvent:function(){
		$("#btn_ok").on('click',function(){
			$("#btn-search").click();
			Public.hideConstion();
		});
		$("#btn-search").on('click',function(){
			var type = $("#query_type").val();
			if(type == 0){
				THISPAGE.reloadList();
			}else{
				THISPAGE.reloadData();
			}
		});
		$('#grid').on('click', '.operating .ui-icon-print', function(e){
			e.preventDefault();
			var id = $(this).parent().data('id');
			THISPAGE.print(id);
		});
		//重置
		$('#btn_reset').on('click',function(e){
			THISPAGE.reset();
		});
		$("#btn-type").on('click',function(){
			var type = $("#query_type").val();
			$("#em_code").val("");
			if(type == 0){
				$("#query_type").val(1);
				$(this).text("收银查询");
				$("#t_code").text("货号");
				$("#t_emp").text("导购");
				THISPAGE.initList();
				empCombo.loadData(empData,-1);
			}else{
				$("#query_type").val(0);
				$(this).text("明细查询");
				$("#t_code").text("单据号");
				$("#t_emp").text("收银员");
				THISPAGE.initCash();
				empCombo.loadData(cashierData,-1);
			}
		});
		$("#begindate").on('click',function(){
			var today = config.TODAY;
			var days = system.DAYS;
			if(undefined != days && null != days && parseInt(days) > -1){
				var mindate = addDay(today,-days);
				WdatePicker({minDate:mindate,maxDate:today});
			}else{
				WdatePicker({maxDate:today});
			}
		});
	}
};
$(function (){
	THISPAGE.init();
}); 
