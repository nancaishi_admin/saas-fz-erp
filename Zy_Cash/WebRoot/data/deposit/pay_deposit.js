var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var total_money = 0;
var vadiUtil={
	vadiNumber:function(obj) {
		var data = $(obj).val();
		if (isNaN(data)) {
			W.Public.tips({type: 1, content : "请输入数字"});
			$(obj).val(0).focus().select();
			return;
		}
		if(parseFloat(data) < 0){
			W.Public.tips({type: 1, content : "请输入正数"});
			$(obj).val(0).focus().select();
			return;
		}
		$("#last_money").val(parseFloat(total_money-data).toFixed(2));
	}
};
var handle = {
	save:function(id){
		if (!$("#form1").valid()) {
			return;
		}
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"deposit/payDeposit",
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					W.flag = true;
					W.Public.tips({type: 0, content : data.message});
					setTimeout("api.close()",300);
					handle.print();
					if(callback && typeof callback == 'function'){
						callback();
					}
				}else{
					W.flag = false;
					W.Public.tips({type: 1, content : data.message});
					setTimeout("api.zindex()",300);
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	print:function(){
		var print = _self.$_ischeck.chkVal().join()?1:0;
		if(print == 1){//选中打印
			var temp = "<table><tr><td align='center'>.</td></tr></table>";
			LODOP.SET_PRINT_STYLE("FontSize","10");
			LODOP.ADD_PRINT_TEXT("3mm",'2mm','50mm','15mm','单据类型：预付订金');
			LODOP.ADD_PRINT_TEXT("8mm",'2mm','50mm','15mm','姓名：'+$("#sd_customer").val());
			LODOP.ADD_PRINT_TEXT("13mm",'2mm','50mm','15mm','手机：'+$("#sd_tel").val());
			LODOP.ADD_PRINT_TEXT("18mm",'2mm','50mm','15mm','日期：'+$("#sd_date").val());
			var sd_deposit = $("#sd_deposit").val();
			LODOP.ADD_PRINT_TEXT("23mm",'2mm','50mm','15mm','订金：'+sd_deposit);
			var last_money = $("#last_money").val();
			LODOP.ADD_PRINT_TEXT("28mm",'2mm','50mm','15mm','余款：'+last_money);
			LODOP.ADD_PRINT_TEXT("33mm",'2mm','50mm','15mm','店铺：'+system.SHOP_NAME);
			LODOP.ADD_PRINT_HTML("38mm", "2mm",'100%',"100%", temp);
			LODOP.SET_PRINT_MODE("PRINT_PAGE_PERCENT", "Auto-Width");//整宽不变形
			LODOP.PREVIEW();
		}
	}
};
var THISPAGE = {
	init:function(){
		this.initParam();
		this.initEvent();
	},
	initParam:function(){
		total_money =  W.$("#total_money").text();
		$("#sd_customer").focus();
		this.$_ischeck = $("#ischeck").cssCheckbox();
		_self = this;
		$("#last_money").val(total_money);
		$("#sd_date").val(config.TODAY);
	},
	initEvent:function(){
		$("#btn-save").on('click',function(){
			handle.save();
		});
		$("#btn_close").on('click',function(){
			api.close();
		});
	}
};
THISPAGE.init();