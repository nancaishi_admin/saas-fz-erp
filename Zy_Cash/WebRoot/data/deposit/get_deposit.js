var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var isRefresh = false;
var queryurl = config.BASEPATH+'deposit/getDeposit';
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var handle = {
	takeDeposit:function(id){
		if(undefined != id && "" != id){
			W.$.dialog.confirm('确定提取订金?', function(){
				var rowData = $("#grid").jqGrid("getRowData", id);
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'deposit/take',
					data:{"sd_number":rowData.sd_number},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){
							W.flag = true;
							W.$("#sd_deposit").val(rowData.sd_deposit);
							W.$("#sd_number").val(rowData.sd_number);
							if(callback && typeof callback == 'function'){
								callback();
							}
							setTimeout("api.close()",200);
						}else{
							W.flag = false;
							Public.tips({type: 1, content : data.message});
						}
					}
				});
			});
		}else{
			W.Public.tips({type: 2, content : "请选择订单!"});
		}
	},
	back:function(id){
		if(undefined != id && "" != id){
			W.$.dialog.confirm('确定取消订金?', function(){
				var rowData = $("#grid").jqGrid("getRowData", id);
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'deposit/back',
					data:{"sd_number":rowData.sd_number},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){
							W.Public.tips({type: 2, content : "退订成功!"});
//							setTimeout("api.close()",1000);
							THISPAGE.reloadData();
						}else{
							Public.tips({type: 1, content : data.message});
						}
					}
				});
			});
		}else{
			W.Public.tips({type: 2, content : "请选择订单!"});
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
	},
	initGrid:function(){
		var colModel = [
	    	{name: 'sd_number',label:'编号',index: 'sd_number',width:115},
	    	{name: 'sd_customer',label:'姓名',index: 'sd_customer',width:70},
	    	{name: 'sd_tel',label:'手机',index: 'sd_tel',width:100},
	    	{name: 'sd_deposit',label:'订金',index: 'sd_deposit',width:60},
	    	{name: 'sd_date',label:'预订日期',index: 'sd_date',width:100},
	    	{name: 'emp_name',label:'收银员',index: 'emp_name',width:70}
	    ];
		$('#grid').jqGrid({
			url:queryurl,
			datatype: 'json',
			width: 605,
			height: 303,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:' 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:-1,//每页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id: 'sd_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		var code = $.trim($("#SearchContent").val());
		params += '&code='+Public.encodeURI(code);
		return params;
	},
	reset:function(){
		$("#SearchContent").val("");
	},
	reloadData:function(){
		var code = $.trim($("#SearchContent").val());
	/*	if(null == code || '' == code){
			W.Public.tips({type: 2, content : "请输入手机号!"});
			return;
		}*/
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
		$("#btn-get").on('click',function(e){
			e.preventDefault();
			var id = $("#grid").jqGrid('getGridParam', 'selrow');
			handle.takeDeposit(id);
		});
		$("#btn-back").on('click',function(e){
			e.preventDefault();
			var id = $("#grid").jqGrid('getGridParam', 'selrow');
			handle.back(id);
		});
	}
};
THISPAGE.init();