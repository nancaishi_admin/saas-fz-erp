package zy.dao.index.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.index.IndexDAO;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.entity.sell.cashier.T_Sell_Cashier_Set;
import zy.entity.sell.dayend.T_Sell_DayEnd;
import zy.entity.sell.set.T_Sell_Print;
import zy.entity.sell.set.T_Sell_Set;
import zy.entity.sys.sqb.T_Sys_Sqb;
import zy.util.CommonUtil;
@Repository
public class IndexDAOImpl extends BaseDaoImpl implements IndexDAO{

	@Override
	public void login(Map<String, Object> param) {
		try{
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT ca_id,ca_em_code,ca_days,ca_shop_code,ca_erase,ca_maxmoney,ca_minrate");
		sql.append(" ,em_name,co_code as companycode,c.companyid");
		sql.append(" ,(select sp_shop_type FROM t_base_shop s1 WHERE s1.sp_code=s.sp_upcode AND s1.companyid=s.companyid LIMIT 1) AS shop_uptype");
		sql.append(" ,dp_code,dp_name");
		sql.append(" ,sp_shop_type as shop_type,sp_upcode as shop_upcode,sp_name as shop_name,sp_end as shop_end");
		sql.append(" FROM t_sell_cashier c");
		sql.append(" JOIN t_base_emp e");
		sql.append(" ON e.em_code=c.ca_em_code");
		sql.append(" AND e.companyid=c.companyid");
		sql.append(" JOIN t_sys_company ");
		sql.append(" ON c.companyid=co_id");
		sql.append(" JOIN t_base_shop s");
		sql.append(" ON s.sp_code=c.ca_shop_code");
		sql.append(" AND s.companyid=c.companyid");
		sql.append(" JOIN t_base_depot d");
		sql.append(" ON d.dp_shop_code=c.ca_shop_code");
		sql.append(" AND d.companyid=c.companyid");
		sql.append(" AND dp_default=1");
		sql.append(" WHERE 1=1");
		sql.append(" AND sp_state=0");
		sql.append(" AND ca_state=0");
		sql.append(" AND ca_em_code=:ca_code");
		sql.append(" AND ca_pass=:ca_pass");
		sql.append(" AND co_code=:ca_co_code");
		sql.append(" AND sp_end>SYSDATE()");
		sql.append(" LIMIT 1");
		T_Sell_Cashier cashier = namedParameterJdbcTemplate.queryForObject(sql.toString(), 
				param, 
				new BeanPropertyRowMapper<>(T_Sell_Cashier.class));
		sql.setLength(0);
		sql.append(" DELETE FROM t_sell_shop_temp");
		sql.append(" WHERE 1=1");
		sql.append(" AND sht_em_code=:ca_em_code");
		sql.append(" AND sht_shop_code=:ca_shop_code");
		sql.append(" AND companyid=:companyid");
		//TODO 为了测试,登录时暂未删除数据 
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(cashier));
		sql.setLength(0);
		sql.append(" update t_sell_cashier");
		sql.append(" set ca_last_login=sysdate()");
		sql.append(" where ca_id=:ca_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(cashier));
		
		sql.setLength(0);
		sql.append(" SELECT de_id,de_st_code,st_name");
		sql.append(" FROM t_sell_dayend t");
		sql.append(" JOIN t_sell_shift s");
		sql.append(" ON st_code=de_st_code");
		sql.append(" AND s.companyid=t.companyid");
		sql.append(" WHERE de_em_code=:ca_em_code");
		sql.append(" AND de_shop_code=:ca_shop_code");
		sql.append(" AND de_state=0");
		sql.append(" and t.companyid=:companyid");
		T_Sell_DayEnd dayEnd = null;
		try {
			dayEnd = namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new BeanPropertySqlParameterSource(cashier), 
					new BeanPropertyRowMapper<>(T_Sell_DayEnd.class));
		} catch (Exception e) {
		}
		if(null != dayEnd){
			cashier.setSt_code(dayEnd.getDe_st_code());
			cashier.setSt_name(dayEnd.getSt_name());
		}
		
		sql.setLength(0);
		sql.append("SELECT sp_print,sp_auto");
		sql.append(" FROM t_sell_print");
		sql.append(" WHERE sp_shop_code=:ca_shop_code");
		sql.append(" AND companyid=:companyid");
		T_Sell_Print print = null;
		try {
			print = namedParameterJdbcTemplate.queryForObject(sql.toString(), 
				new BeanPropertySqlParameterSource(cashier),
				new BeanPropertyRowMapper<>(T_Sell_Print.class)
				);
		} catch (Exception e) {
		}
		param.clear();
		if(null != print){
			param.put(CommonUtil.KEY_PRINT, print);
		}
		param.put(CommonUtil.KEY_CASHIER, cashier);
		//收钱吧设置
		sql.setLength(0);
		sql.append(" SELECT sqb_id,sqb_device_id,sqb_device_name,sqb_terminal_sn,sqb_terminal_key,sqb_activate_time,sqb_activate_time,");
		sql.append(" sqb_checkin_time,sqb_shop_code,sqb_state,companyid");
		sql.append(" FROM t_sys_sqb");
		sql.append(" WHERE 1=1");
		sql.append(" AND sqb_state = 0");
		sql.append(" AND sqb_shop_code = :shop_code");
		sql.append(" AND companyid = :companyid");
		sql.append(" LIMIT 1");
		T_Sys_Sqb sqb = null;
		try {
			sqb = namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new MapSqlParameterSource().addValue("shop_code", cashier.getCa_shop_code()).addValue("companyid", cashier.getCompanyid()),
					new BeanPropertyRowMapper<>(T_Sys_Sqb.class));
		} catch (Exception e) {
		}
		cashier.setSqb(sqb);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public List<T_Sell_Cashier_Set> cashierSet(T_Sell_Cashier cashier) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT cs_key,cs_value");
		sql.append(" FROM t_sell_cashier_set");
		sql.append(" WHERE cs_em_code=:ca_em_code");
		sql.append(" AND companyid=:companyid");
		List<T_Sell_Cashier_Set> setlist = namedParameterJdbcTemplate.query(sql.toString(), new BeanPropertySqlParameterSource(cashier), new BeanPropertyRowMapper<>(T_Sell_Cashier_Set.class));
		return setlist;
	}

	@Override
	public List<T_Sell_Set> shopSet(T_Sell_Cashier cashier) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT st_key,st_value");
		sql.append(" FROM t_sell_set");
		sql.append(" WHERE st_shop_code=:ca_shop_code");
		sql.append(" AND companyid=:companyid");
		List<T_Sell_Set> setlist = namedParameterJdbcTemplate.query(sql.toString(), new BeanPropertySqlParameterSource(cashier), new BeanPropertyRowMapper<>(T_Sell_Set.class));
		return setlist;
	}

	@Override
	public void editPass(Map<String, Object> paramMap) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT CA_ID ");
		sql.append(" FROM t_sell_cashier");
		sql.append(" WHERE ca_pass=:old_pass");
		sql.append(" AND ca_id=:ca_id");
		Integer id = null;
		try {
			id = namedParameterJdbcTemplate.queryForObject(sql.toString(), paramMap, Integer.class);
		} catch (Exception e) {
			throw new IllegalArgumentException("旧密码不正确");
		}
		if(null != id){
			sql.setLength(0);
			sql.append(" UPDATE t_sell_cashier");
			sql.append(" SET ca_pass=:new_pass");
			sql.append(" WHERE ca_id=:ca_id");
			namedParameterJdbcTemplate.update(sql.toString(),paramMap);
		}else{
			throw new IllegalArgumentException("旧密码不正确");
		}
	}

}
