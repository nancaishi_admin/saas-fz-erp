package zy.controller.base;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.base.shop.T_Base_Shop;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.service.base.shop.ShopService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("base/shop")
public class ShopController extends BaseController{
	
	@Resource
	private ShopService shopService;
	
	/**
	 * 店铺查询弹出框，按照功能模块
	 */
	@RequestMapping("to_list_dialog_bymodule")
	public String to_list_dialog_bymodule() {
		return "base/shop/list_dialog_bymodule";
	}
	/**
	 * 查询下级店铺弹出框页面，支持单选和多选
	 */
	@RequestMapping("to_list_sub_dialog")
	public String to_list_sub_dialog() {
		return "base/shop/list_sub_dialog";
	}
	/**
	 * 前台调拨查询店铺(查询上级店铺的所有自营及合伙子店铺)
	 */
	@RequestMapping(value = "list_for_allocate", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object list_for_allocate(String searchContent, HttpSession session) {
		T_Sell_Cashier cashier = getCashier(session);
		Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SHOP_TYPE, cashier.getShop_uptype().toString());
        param.put(CommonUtil.SHOP_CODE, cashier.getShop_upcode());
        param.put("searchContent", StringUtil.decodeString(searchContent));
        List<T_Base_Shop> shops = shopService.sub_list(param);
        T_Base_Shop currentShop = null;
		for (T_Base_Shop shop : shops) {
			if(cashier.getCa_shop_code().equals(shop.getSp_code())){
				currentShop = shop;
				break;
			}
		}
		shops.remove(currentShop);
		return ajaxSuccess(shops);
	}
	
	/**
	 * 总公司和分公司查询下级店铺(自营店、合伙店)，否则查询自己店铺
	 */
	@RequestMapping(value = "sub_list", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object sub_list(String searchContent, HttpSession session) {
		Map<String,Object> param = new HashMap<String, Object>();
		T_Sell_Cashier cashier = getCashier(session);
		param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SHOP_TYPE, cashier.getShop_uptype().toString());
        param.put(CommonUtil.SHOP_CODE, cashier.getShop_upcode());
        param.put("searchContent", StringUtil.decodeString(searchContent));
		return ajaxSuccess(shopService.sub_list(param));
	}
}
