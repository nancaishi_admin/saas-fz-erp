package zy.controller.base;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.form.PageForm;
import zy.service.base.product.ProductService;
import zy.util.CommonUtil;

@Controller
@RequestMapping("base/product")
public class ProductController extends BaseController{
	@Resource
	private ProductService productService;
	
	@RequestMapping("to_list_dialog")
	public String to_list_dialog() {
		return "base/product/list_dialog";
	}
	
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object list(PageForm pageForm,HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put("searchContent", pageForm.getSearchContent());
		return ajaxSuccess(productService.page(param));
	}
	
}
