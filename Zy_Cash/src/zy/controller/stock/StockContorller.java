package zy.controller.stock;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.base.bra.T_Base_Bra;
import zy.entity.base.size.T_Base_Size;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.form.PageForm;
import zy.service.base.bra.BraService;
import zy.service.base.depot.DepotService;
import zy.service.base.size.SizeService;
import zy.service.stock.data.DataService;
import zy.util.CommonUtil;
import zy.util.StringUtil;
@Controller
@RequestMapping("stock")
public class StockContorller extends BaseController{
	@Resource
	private DataService dataService;
	@Resource
	private DepotService depotService;
	@Resource
	private SizeService sizeService;
	@Resource
	private BraService braService;
	
	@RequestMapping(value = "to_other", method = RequestMethod.GET)
	public String to_ohter() {
		return "stock/other";
	}
	
	@RequestMapping(value = "to_stock", method = RequestMethod.GET)
	public String to_stock() {
		return "stock/stock";
	}
	
	@RequestMapping(value = "to_depot", method = RequestMethod.GET)
	public String to_depot() {
		return "stock/depot";
	}
	
	@RequestMapping(value = "size", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object size(HttpServletRequest request,HttpSession session){
		String name = request.getParameter("sz_name");
		T_Sell_Cashier user = getCashier(session);
		Map<String,Object> param = new HashMap<String, Object>(2);
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put("name", name);
		List<T_Base_Size> sizeList = sizeService.list(param);
		return ajaxSuccess(sizeList, "查询成功, 共" + sizeList.size() + "条数据");
	}
	
	@RequestMapping(value = "braList", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object braList(HttpServletRequest request,HttpSession session){
		String br_name = request.getParameter("br_name");
		T_Sell_Cashier user = getCashier(session);
		Map<String,Object> param = new HashMap<String, Object>(2);
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put("br_name", br_name);
		List<T_Base_Bra> braList = braService.braList(param);
		return ajaxSuccess(braList, "查询成功, 共" + braList.size() + "条数据");
	}
	
	@RequestMapping(value = "ownDepot", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object ownDepot(PageForm pageForm, HttpSession session) {
        T_Sell_Cashier cashier = getCashier(session);
        Map<String,Object> param = new HashMap<String, Object>();
        bulidParam(param, cashier);
        param.put("dp_spell", StringUtil.getSpell(pageForm.getSearchContent()));
		return ajaxSuccess(depotService.ownDepot(param));
	}
	
	
	@RequestMapping(value = "otherList", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object otherList(PageForm pageForm,HttpServletRequest request, HttpSession session) {
		String sz_code = request.getParameter("sz_code");
		String br_code = request.getParameter("br_code");
		String pd_no = request.getParameter("pd_no");
		String data = request.getParameter("data");
        T_Sell_Cashier cashier = getCashier(session);
        Map<String,Object> param = new HashMap<String, Object>();
        bulidParam(param, cashier);
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("sz_code", sz_code);
        param.put("br_code", br_code);
        param.put("pd_no", StringUtil.decodeString(pd_no));
        param.put("data", StringUtil.decodeString(data));
		return ajaxSuccess(dataService.otherList(param));
	}
	
}
