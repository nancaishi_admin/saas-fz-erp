package zy.controller.cash;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.PageData;
import zy.entity.sell.cash.T_Sell_Pay_Temp;
import zy.entity.sell.cash.T_Sell_Shop_Temp;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.entity.sell.ecoupon.T_Sell_Ecoupon_User;
import zy.entity.vip.member.T_Vip_Member;
import zy.form.PageForm;
import zy.service.base.product.ProductService;
import zy.service.sell.cash.CashService;
import zy.service.sell.ecoupon.ECouponService;
import zy.util.CommonUtil;
import zy.util.NumberUtil;
import zy.util.StringUtil;
@Controller
@RequestMapping("cash")
public class CashController extends BaseController{
	@Resource
	private CashService cashService;
	@Resource
	private ECouponService couponService;
	@Resource
	private ProductService productService;
	@RequestMapping("to_product_sell")
	public String to_product_sell() {
		return "cash/product_sell";
	}
	@RequestMapping("to_product_change")
	public String to_product_change() {
		return "cash/product_change";
	}
	@RequestMapping("to_vip_rate")
	public String to_vip_rate() {
		return "cash/vip_rate";
	}
	@RequestMapping("to_hand_rate")
	public String to_hand_rate() {
		return "cash/hand_rate";
	}
	@RequestMapping("to_all_rate")
	public String to_all_rate() {
		return "cash/all_rate";
	}
	@RequestMapping("to_voucher")
	public String to_voucher() {
		return "cash/voucher";
	}
	@RequestMapping("to_product_back")
	public String to_product_back() {
		return "cash/product_back";
	}
	@RequestMapping("to_card")
	public String to_card() {
		return "cash/card";
	}
	@RequestMapping("to_ecoupon")
	public String to_ecoupon() {
		return "cash/ecoupon";
	}
	@RequestMapping("to_third")
	public String to_third() {
		return "cash/pay_sqb";
	}
	
	@RequestMapping("to_send_ecoupon")
	public String to_send_ecoupon() {
		return "cash/send_ecoupon";
	}
	@RequestMapping("to_size")
	public String to_size() {
		return "cash/product_size";
	}
	@RequestMapping("to_cash")
	public String to_cash(HttpServletRequest request,HttpSession session,Model model) {
		Map<String,Object> param = new HashMap<String, Object>();
		bulidParam(param, getCashier(session));
		param.put(CommonUtil.KEY_VIP, session.getAttribute(CommonUtil.KEY_VIP));
		cashService.to_cash(param);
		model.addAttribute("data",param);
		return "cash/cash_out";
	}
	
	@RequestMapping("to_img_view")
	public String to_img_view(HttpServletRequest request,HttpSession session,Model model) {
		String pd_code = request.getParameter("pd_code");
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(CommonUtil.COMPANYID, getCompanyid(session));
		params.put("pd_code", pd_code);
		model.addAttribute("imgs", productService.productimglist(params));
		return "cash/product_img";
	}
	
	
	/**
	 * 查询临时表，不包含挂单的数据
	 * 
	 * */
	@RequestMapping(value = "listTemp", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listTemp(HttpServletRequest request,HttpSession session) {
//		Object sell_type = request.getParameter("sell_type");
		T_Sell_Cashier cashier = getCashier(session);
        Map<String,Object> param = new HashMap<String, Object>();
        bulidParam(param, cashier);
//        param.put(CommonUtil.SELL_TYPE, sell_type);
		return ajaxSuccess(cashService.listTemp(param));
	}
	@RequestMapping(value = "updateEmp", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object updateEmp(HttpServletRequest request,HttpSession session) {
		Object line = request.getParameter("line");
		Object type = request.getParameter("type");
		Object sht_id = request.getParameter("sht_id");
		Object em_code = request.getParameter("em_code");
		Object em_name = request.getParameter("em_name");
		T_Sell_Cashier cashier = getCashier(session);
		Map<String,Object> param = new HashMap<String, Object>();
        bulidParam(param, cashier);
        param.put("line", line);
        param.put("type", type);
        param.put("sht_id", sht_id);
        param.put("em_code", em_code);
        param.put("em_name", em_name);
        cashService.updateEmp(param);
		return ajaxSuccess();
	}
	@RequestMapping(value = "updateArea", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object updateArea(HttpServletRequest request,HttpSession session) {
		Object line = request.getParameter("line");
		Object sht_id = request.getParameter("sht_id");
		Object da_code = request.getParameter("da_code");
		Map<String,Object> param = new HashMap<String, Object>();
        bulidParam(param, getCashier(session));
        param.put("line", line);
        param.put("sht_id", sht_id);
        param.put("da_code", da_code);
        cashService.updateArea(param);
		return ajaxSuccess();
	}
	
	
	@RequestMapping(value = "product", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object product(HttpServletRequest request,HttpSession session) {
		Object pd_no = request.getParameter("pd_no");
        Map<String,Object> param = new HashMap<String, Object>();
        bulidParam(param, getCashier(session));
        param.put(CommonUtil.KEY_VIP, session.getAttribute(CommonUtil.KEY_VIP));
		param.put(CommonUtil.KEY_CASHIER_SET, session.getAttribute(CommonUtil.KEY_CASHIER_SET));
        param.put("pd_no", pd_no);
        T_Sell_Shop_Temp product = cashService.product(param);
		if(null != product){
			return ajaxSuccess(param);
		}else{
			return result(NOTFOUND);
		}
	}
	/**
	 * 条码查询功能
	 * 查到则直接插入临时表，并计算折扣
	 * */
	@RequestMapping(value = "productSize", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object productSize(HttpServletRequest request,HttpSession session) {
		String pd_code = request.getParameter("sht_pd_code");
		String pd_szg_code = request.getParameter("pd_szg_code");
        Map<String,Object> param = new HashMap<String, Object>();
        bulidParam(param, getCashier(session));
        param.put(CommonUtil.KEY_VIP, session.getAttribute(CommonUtil.KEY_VIP));
		param.put(CommonUtil.KEY_CASHIER_SET, session.getAttribute(CommonUtil.KEY_CASHIER_SET));
        param.put("pd_szg_code", pd_szg_code);
        param.put("pd_code", pd_code);
        Map<String,Object> map = cashService.productSize(param);
		if(map.size() > 0){
			return ajaxSuccess(map);
		}else{
			return result(NOTFOUND);
		}
	}
	/**
	 * 货号查询功能
	 * */
	@RequestMapping(value = "pageSell", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object pageSell(PageForm pageForm,HttpServletRequest request,HttpSession session) {
		Object code = request.getParameter("code");
		Object isall = request.getParameter("isall");//0全部显示1显示有库存
		Object isquery = request.getParameter("isquery");//1模糊查询0精确查询
        Map<String,Object> param = new HashMap<String, Object>();
        bulidParam(param, getCashier(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put("code", StringUtil.getSpell(""+StringUtil.decodeString(code)));
        param.put("isall", isall);
        param.put("isquery", isquery);
		PageData<T_Sell_Shop_Temp> pageData = cashService.pageSell(param);
		return ajaxSuccess(pageData, "查询成功, 共" + pageData.getList().size() + "条数据");
	}
	@RequestMapping(value = "pageBack", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object pageBack(PageForm pageForm,HttpServletRequest request,HttpSession session) {
		Object sell_type = request.getParameter("sell_type");
		Object code = request.getParameter("code");
		Object isnumber = request.getParameter("isnumber");
		Object isall = request.getParameter("isall");//0全部显示1显示有库存
		Object isquery = request.getParameter("isquery");//1模糊查询0精确查询
        Map<String,Object> param = new HashMap<String, Object>();
        bulidParam(param, getCashier(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put("code", StringUtil.getSpell(""+StringUtil.decodeString(code)));
        param.put("isnumber", isnumber);
        param.put("isall", isall);
        param.put("isquery", isquery);
        param.put(CommonUtil.SELL_TYPE, sell_type);
		PageData<T_Sell_Shop_Temp> pageData = cashService.pageBack(param);
		return ajaxSuccess(pageData, "查询成功, 共" + pageData.getList().size() + "条数据");
	}
	/**
	 * 条码查询功能
	 * 查到则直接插入临时表，并计算折扣
	 * */
	@RequestMapping(value = "subCode", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object subCode(HttpServletRequest request,HttpSession session) {
		Object sell_type = request.getParameter("sell_type");
		Object barcode = request.getParameter("barcode");
        Map<String,Object> param = new HashMap<String, Object>();
        bulidParam(param, getCashier(session));
        param.put(CommonUtil.SELL_TYPE, sell_type);
        param.put(CommonUtil.KEY_VIP, session.getAttribute(CommonUtil.KEY_VIP));
		param.put(CommonUtil.KEY_CASHIER_SET, session.getAttribute(CommonUtil.KEY_CASHIER_SET));
        param.put("code", barcode);
		int flag = cashService.subCode(param);
		if(flag > 0){
			return ajaxSuccess();
		}else{
			return result(NOTFOUND);
		}
	}
	
	/**
	 * 查询是否是零售单据
	 * */
	
	@RequestMapping(value = "queryNumber", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object queryNumber(HttpServletRequest request,HttpSession session) {
		Object sell_type = request.getParameter("sell_type");
		Object code = request.getParameter("code");
        Map<String,Object> param = new HashMap<String, Object>();
        bulidParam(param, getCashier(session));
        param.put(CommonUtil.SELL_TYPE, sell_type);
        param.put(CommonUtil.KEY_VIP, session.getAttribute(CommonUtil.KEY_VIP));
		param.put(CommonUtil.KEY_CASHIER_SET, session.getAttribute(CommonUtil.KEY_CASHIER_SET));
        param.put("code", code);
		int flag = cashService.queryNumber(param);
		if(flag > 0){
			return ajaxSuccess(flag);
		}else{
			return result(NOTFOUND);
		}
	}
	
	@RequestMapping(value = "saveBackTemp", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object saveBackTemp(HttpServletRequest request,HttpSession session) {
		Object sell_type = request.getParameter("sell_type");
		Object data = request.getParameter("data");
		Map<String,Object> param = new HashMap<String, Object>();
		bulidParam(param, getCashier(session));
		param.put(CommonUtil.SELL_TYPE, sell_type);
		param.put(CommonUtil.KEY_CASHIER_SET, session.getAttribute(CommonUtil.KEY_CASHIER_SET));
		param.put(CommonUtil.KEY_VIP, session.getAttribute(CommonUtil.KEY_VIP));
		param.put("data", data);
		cashService.saveBackTemp(param);
		//为什么要取会员数据，因为根据单据退货。可能会有会员信息在里面
		T_Vip_Member vip = (T_Vip_Member)param.get(CommonUtil.KEY_VIP);
		if(null != vip){
			param.put(CommonUtil.KEY_VIP, vip);
			session.setAttribute(CommonUtil.KEY_VIP, vip);
		}else{
			session.removeAttribute(CommonUtil.KEY_VIP);
		}
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "saveTemp", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object saveTemp(HttpServletRequest request,HttpSession session) {
		Object sell_type = request.getParameter("sell_type");
		Object data = request.getParameter("data");
		Map<String,Object> param = new HashMap<String, Object>();
		bulidParam(param, getCashier(session));
		param.put(CommonUtil.SELL_TYPE, sell_type);
		param.put(CommonUtil.KEY_CASHIER_SET, session.getAttribute(CommonUtil.KEY_CASHIER_SET));
		param.put(CommonUtil.KEY_VIP, session.getAttribute(CommonUtil.KEY_VIP));
		param.put("data", data);
		cashService.saveTemp(param);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "delTemp", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object delTemp(HttpServletRequest request,HttpSession session) {
		Object sell_type = request.getParameter("sell_type");
		Object sht_id = request.getParameter("sht_id");
		Object isgift = request.getParameter("isgift");
		Object temp_size = request.getParameter("temp_size");
		Map<String,Object> param = new HashMap<String, Object>();
		param.put(CommonUtil.SELL_TYPE, sell_type);
		param.put(CommonUtil.KEY_VIP, session.getAttribute(CommonUtil.KEY_VIP));
		param.put(CommonUtil.KEY_CASHIER_SET, session.getAttribute(CommonUtil.KEY_CASHIER_SET));
		bulidParam(param, getCashier(session));
		param.put("sht_id", sht_id);
		param.put("isgift", isgift);
		cashService.delTemp(param);
		if(null != temp_size && Integer.parseInt(StringUtil.trimString(temp_size)) == 1){
			removeVip(session);
		}
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "clearTemp", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object clearTemp(HttpServletRequest request,HttpSession session) {
		Map<String,Object> param = new HashMap<String, Object>();
		bulidParam(param, getCashier(session));
		cashService.clearTemp(param);
		removeVip(session);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "updateTemp", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object updateTemp(HttpServletRequest request,HttpSession session) {
		Object sell_type = request.getParameter("sell_type");
		Object sht_id = request.getParameter("sht_id");
		Object sht_amount = request.getParameter("sht_amount");
		Map<String,Object> param = new HashMap<String, Object>();
		param.put(CommonUtil.SELL_TYPE, sell_type);
		param.put(CommonUtil.KEY_VIP, session.getAttribute(CommonUtil.KEY_VIP));
		param.put(CommonUtil.KEY_CASHIER_SET, session.getAttribute(CommonUtil.KEY_CASHIER_SET));
		bulidParam(param, getCashier(session));
		param.put("sht_id", sht_id);
		param.put("sht_amount", sht_amount);
		cashService.updateTemp(param);
		return ajaxSuccess();
	}
	
	/***
	 * 会员打折
	 * 
	 * */
	@RequestMapping(value = "vipRate", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object vipRate(HttpServletRequest request,HttpSession session) {
		Object vip_code = request.getParameter("vip_code");
		String vip_other = request.getParameter("vip_other");
		Object sell_type = request.getParameter("sell_type");
		Map<String,Object> param = new HashMap<String, Object>();
		param.put(CommonUtil.SELL_TYPE, sell_type);
		bulidParam(param, getCashier(session));
		param.put("vip_code", vip_code);
		param.put(CommonUtil.KEY_CASHIER_SET, session.getAttribute(CommonUtil.KEY_CASHIER_SET));
		T_Vip_Member vip = cashService.queryVip(param);
		if(null != vip){
			param.put(CommonUtil.KEY_VIP, vip);
			vip.setVip_other(StringUtil.trimString(vip_other).equals("")?0:NumberUtil.toInteger(vip_other));
			cashService.vipRate(param);//会员打折
			session.setAttribute(CommonUtil.KEY_VIP, vip);
			return ajaxSuccess(vip);
		}else{
			session.removeAttribute(CommonUtil.KEY_VIP);
			return result(NOTFOUND);
		}
	}
	
	/***
	 * 手动打折
	 * 
	 * */
	@RequestMapping(value = "handRate", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object handRate(HttpServletRequest request,HttpSession session) {
		Object sell_type = request.getParameter("sell_type");
		Object many_rate = request.getParameter("many_rate");
		Object sht_id = request.getParameter("sht_id");
		Object no_point = request.getParameter("no_point");
		Object hand_type = request.getParameter("hand_type");
		Object hand_value = request.getParameter("hand_value");
		Map<String,Object> param = new HashMap<String, Object>();
		bulidParam(param, getCashier(session));
		param.put(CommonUtil.SELL_TYPE, sell_type);
		param.put(CommonUtil.KEY_CASHIER_SET, session.getAttribute(CommonUtil.KEY_CASHIER_SET));
		param.put(CommonUtil.KEY_VIP, session.getAttribute(CommonUtil.KEY_VIP));
		param.put("many_rate", many_rate);
		param.put("sht_id", sht_id);
		param.put("no_point", no_point);
		param.put("hand_type", hand_type);
		param.put("hand_value", hand_value);
		cashService.handRate(param);
		return ajaxSuccess();
	}
	
	/***
	 * 整单打折
	 * 
	 * */
	@RequestMapping(value = "allRate", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object allRate(HttpServletRequest request,HttpSession session) {
		Object sell_type = request.getParameter("sell_type");
		Object hand_type = request.getParameter("hand_type");
		Object hand_value = request.getParameter("hand_value");
		Object total_money = request.getParameter("total_money");
		Map<String,Object> param = new HashMap<String, Object>();
		bulidParam(param, getCashier(session));
		param.put(CommonUtil.SELL_TYPE, sell_type);
		param.put(CommonUtil.KEY_CASHIER_SET, session.getAttribute(CommonUtil.KEY_CASHIER_SET));
		param.put(CommonUtil.KEY_VIP, session.getAttribute(CommonUtil.KEY_VIP));
		param.put("hand_type", hand_type);
		param.put("hand_value", hand_value);
		param.put("total_money", total_money);
		cashService.allRate(param);
		return ajaxSuccess();
	}
	/**
	 * 提取挂单
	 * 清空现有零售数据，把挂单数据加入进来
	 * */
	@RequestMapping(value = "putDown", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object putDown(HttpServletRequest request,HttpSession session) {
		Object put_code = request.getParameter("put_code");
		Map<String,Object> param = new HashMap<String, Object>();
		bulidParam(param, getCashier(session));
		param.put(CommonUtil.KEY_CASHIER_SET, session.getAttribute(CommonUtil.KEY_CASHIER_SET));
		param.put(CommonUtil.KEY_VIP, session.getAttribute(CommonUtil.KEY_VIP));
		param.put("put_code", put_code);
		T_Vip_Member vip = cashService.putDown(param);
		if(null != vip){
			param.put(CommonUtil.KEY_VIP, vip);
			session.setAttribute(CommonUtil.KEY_VIP, vip);
		}else{
			session.removeAttribute(CommonUtil.KEY_VIP);
		}
		return ajaxSuccess(param);
	}
	
	/**
	 * 挂单
	 * */
	@RequestMapping(value = "putUp", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object putUp(HttpServletRequest request,HttpSession session) {
		Object put_code = request.getParameter("put_code");
		Map<String,Object> param = new HashMap<String, Object>();
		bulidParam(param, getCashier(session));
		param.put("put_code", put_code);
		cashService.putUp(param);
		session.removeAttribute(CommonUtil.KEY_VIP);
		return ajaxSuccess();
	}
	@RequestMapping(value = "sendCoupon", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object sendCoupon(final T_Sell_Ecoupon_User user, HttpServletRequest request,HttpSession session) {
		final Map<String,Object> param = new HashMap<String, Object>();
		bulidParam(param, getCashier(session));
		new Thread(new Runnable() {
			@Override
			public void run() {
				couponService.sendCoupon(param,user);
			}
		}).start();
		return ajaxSuccess();
	}
	
	/**
	 * 零售收银
	 * */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "sellCashOut", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object sellCashOut(HttpServletRequest request,HttpSession session) {
		Map<String,Object> param = new HashMap<String, Object>();
		Object sell_type = request.getParameter("sell_type");
		String[] cd_ids = null,cd_money = null,vc_ids = null,vc_money = null;
		double cd_rate = 0d,vc_rate = 0d;
		//电子券使用
		String ecu_id = StringUtil.decodeString(request.getParameter("ecu_id"));
		//储值卡使用
		String _cd_ids = StringUtil.decodeString(request.getParameter("cd_ids"));
		if(!StringUtil.isEmpty(_cd_ids)){
			String _cd_money = StringUtil.decodeString(request.getParameter("cd_money"));
			if(!StringUtil.isEmpty(_cd_money)){
				cd_ids = StringUtil.decodeString(_cd_ids).split(",");
				cd_money = StringUtil.decodeString(_cd_money).split(",");
			}
			String _cd_rate = StringUtil.decodeString(request.getParameter("cd_rate"));
			if(!StringUtil.isEmpty(_cd_rate)){
				cd_rate = NumberUtil.toDouble(StringUtil.trimString(_cd_rate));
			}else{
				cd_rate = 1d;
			}
		}
		
		//代金券使用
		String _vc_ids = StringUtil.decodeString(request.getParameter("vc_ids"));
		if(!StringUtil.isEmpty(_vc_ids)){
			String _vc_money = StringUtil.decodeString(request.getParameter("vc_money"));
			if(!StringUtil.isEmpty(_vc_money)){
				vc_ids = StringUtil.decodeString(_vc_ids).split(",");
				vc_money = StringUtil.decodeString(_vc_money).split(",");
			}
			String _vc_rate = StringUtil.decodeString(request.getParameter("vc_rate"));
			if(!StringUtil.isEmpty(_vc_rate)){
				vc_rate = NumberUtil.toDouble(StringUtil.trimString(_vc_rate));
			}else{
				vc_rate = 1d;
			}
		}
		String change_money = StringUtil.decodeString(request.getParameter("change_money"));
		String sh_cash = StringUtil.decodeString(request.getParameter("sh_cash"));
		String sh_ali_money = StringUtil.decodeString(request.getParameter("sh_ali_money"));
		String sh_wx_money = StringUtil.decodeString(request.getParameter("sh_wx_money"));
		String sh_sd_money = StringUtil.decodeString(request.getParameter("sh_sd_money"));//订金
		String sd_number = StringUtil.decodeString(request.getParameter("sd_number"));//订金编号
		String sh_point_money = StringUtil.decodeString(request.getParameter("sh_point_money"));//积分抵用金额
		String vip_point = StringUtil.decodeString(request.getParameter("vip_point"));//会员使用积分
		String sell_money = StringUtil.decodeString(request.getParameter("sell_money"));//商品总额
		String sh_lost_money = StringUtil.decodeString(request.getParameter("sh_lost_money"));//找零金额
		String sh_bank_money = StringUtil.decodeString(request.getParameter("sh_bank_money"));//银行卡使用金额
		String sh_mall_money = StringUtil.decodeString(request.getParameter("sh_mall_money"));//商场卡使用金额
		String sh_vc_money = StringUtil.decodeString(request.getParameter("sh_vc_money"));//代金券使用金额
		String sh_ec_money = StringUtil.decodeString(request.getParameter("sh_ec_money"));//电子券使用金额
//		String sh_sc_money = request.getParameter("sh_sc_money");//优惠券使用金额
		String sh_cd_money = StringUtil.decodeString(request.getParameter("sh_cd_money"));//储值卡使用金额
		String sh_remark = StringUtil.decodeString(request.getParameter("sh_remark"));
		String ecu_name = StringUtil.decodeString(request.getParameter("ecu_name"));//发送电子券客户姓名
		String ecu_tel = StringUtil.decodeString(request.getParameter("ecu_tel"));//发送电子券客户手机号
		bulidParam(param, getCashier(session));
		Map<String,Object> setMap = (Map<String,Object>)param.get(CommonUtil.KEY_CASHIER_SET);
		param.put(CommonUtil.SELL_TYPE, sell_type);
		param.put(CommonUtil.KEY_CASHIER_SET, setMap);
		param.put(CommonUtil.KEY_VIP, session.getAttribute(CommonUtil.KEY_VIP));
		param.put("ecu_id", ecu_id);
//		param.put("ec_money", ec_money);
		param.put("cd_ids", cd_ids);
		param.put("cd_money", cd_money);
		param.put("vc_ids", vc_ids);
		param.put("vc_money", vc_money);
		param.put("cd_rate", cd_rate);
		param.put("vc_rate", vc_rate);
		param.put("sh_cash",NumberUtil.toDouble(sh_cash)-NumberUtil.toDouble(change_money));//实付现金可能大实收，实收要去掉找零的金额
		param.put("sh_ali_money", sh_ali_money);
		param.put("sh_wx_money", sh_wx_money);
		param.put("sh_lost_money", sh_lost_money);
		param.put("sh_bank_money", sh_bank_money);
		param.put("sh_mall_money", sh_mall_money);
		param.put("sh_vc_money", sh_vc_money);
		param.put("sh_ec_money", sh_ec_money);
//		param.put("sh_sc_money", sh_sc_money);
		param.put("sh_cd_money", sh_cd_money);
		param.put("sh_sd_money", sh_sd_money);
		param.put("sd_number", sd_number);
		param.put("sh_point_money", sh_point_money);
		param.put("vip_point", vip_point);
		param.put("sell_money", sell_money);
		param.put("sh_remark", StringUtil.decodeString(sh_remark));
		
		param.put("ecu_name", StringUtil.decodeString(ecu_name));
		param.put("ecu_tel", StringUtil.decodeString(ecu_tel));
		Map<String,Object> map = cashService.sellCashOut(param);
		T_Sell_Ecoupon_User user = null;
		//判断是否查询优惠券发放
		if("0".equals(StringUtil.trimString(sell_type))){//零售情况下才会有优惠券
			if(null != sh_ec_money && !"".equals(sh_ec_money) && !"0".equals(sh_ec_money)){//有使用优惠券
				String reecoupon = (String)setMap.get("KEY_REECOUPON");
				if("1".equals(StringUtil.trimString(reecoupon))){
					user = couponService.checkEcoupon(param);
				}
			}else{
				user = couponService.checkEcoupon(param);
			}
		}
		map.put("user", user);
		removeVip(session);
		return ajaxSuccess(map);
	}
	
	@RequestMapping(value = "doPay", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object doPay(
			T_Sell_Pay_Temp pay,
			HttpServletRequest request,HttpSession session) {
		Map<String,Object> param = new HashMap<String, Object>();
		T_Sell_Cashier cashier = getCashier(session);
		bulidParam(param, cashier);
		param.put("sqb", cashier.getSqb());
		param.put("pay", pay);
		param.put("companycode", cashier.getCompanycode());
		return ajaxSuccess(cashService.doPay(param));
	}
	@RequestMapping(value = "payTemp", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object payTemp(
			String pt_number,
			HttpServletRequest request,HttpSession session) {
		Map<String,Object> param = new HashMap<String, Object>();
		T_Sell_Cashier cashier = getCashier(session);
		bulidParam(param, cashier);
		param.put("sqb", cashier.getSqb());
		param.put("pt_number", pt_number);
		return ajaxSuccess(cashService.payTemp(param));
	}
	@RequestMapping(value = "payList", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object payList(
			HttpServletRequest request,HttpSession session) {
		Map<String,Object> param = new HashMap<String, Object>();
		T_Sell_Cashier cashier = getCashier(session);
		bulidParam(param, cashier);
		return ajaxSuccess(cashService.payList(param));
	}
	
}
