package zy.controller.money;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.service.money.bank.BankService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("money/bank")
public class BankController extends BaseController{
	
	@Resource
	private BankService bankService;
	
	@RequestMapping(value = "/to_list", method = RequestMethod.GET)
	public String to_list() {
		return "money/bank/list";
	}
	/**
	 * 查询银行账户弹出框页面，支持单选和多选
	 */
	@RequestMapping("to_list_dialog")
	public String to_list_sub_dialog() {
		return "money/bank/list_dialog";
	}
	
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object list(String searchContent, HttpSession session) {
		T_Sell_Cashier cashier = getCashier(session);
		Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, cashier.getCompanyid());
        param.put(CommonUtil.SHOP_UPCODE, cashier.getShop_upcode());
        param.put("searchContent", StringUtil.getSpell(StringUtil.decodeString(searchContent)));
		return ajaxSuccess(bankService.listShop(param));
	}
}
