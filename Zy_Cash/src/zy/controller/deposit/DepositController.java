package zy.controller.deposit;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.entity.sell.deposit.T_Sell_Deposit;
import zy.service.shop.deposit.DepositService;
@Controller
@RequestMapping("deposit")
public class DepositController extends BaseController{
	@Resource
	private DepositService depositService;
	
	@RequestMapping("to_pay")
	public String to_pay() {
		return "deposit/pay_deposit";
	}
	@RequestMapping("to_get")
	public String to_get() {
		return "deposit/get_deposit";
	}
	
	@RequestMapping(value = "payDeposit", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object payDeposit(T_Sell_Deposit deposit, HttpServletRequest request,HttpSession session) {
		T_Sell_Cashier cashier = getCashier(session);
        Map<String,Object> param = new HashMap<String, Object>();
        bulidParam(param, cashier);
        param.put("sd_customer", deposit.getSd_customer());
        param.put("sd_tel", deposit.getSd_tel());
        param.put("sd_deposit", deposit.getSd_deposit());
        param.put("sd_date", deposit.getSd_date());
        param.put("sd_remark", deposit.getSd_remark());
        depositService.payDeposit(param);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "getDeposit", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object getDeposit(HttpServletRequest request,HttpSession session) {
		String code = request.getParameter("code");
		T_Sell_Cashier cashier = getCashier(session);
        Map<String,Object> param = new HashMap<String, Object>();
        bulidParam(param, cashier);
        param.put("code", code);
		return ajaxSuccess(depositService.getDeposit(param));
	}
	@RequestMapping(value = "take", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object take(HttpServletRequest request,HttpSession session) {
		T_Sell_Cashier cashier = getCashier(session);
		String number = request.getParameter("sd_number");
        Map<String,Object> param = new HashMap<String, Object>();
        bulidParam(param, cashier);
        param.put("number", number);
        depositService.take(param);
		return ajaxSuccess();
	}
	@RequestMapping(value = "back", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object back(HttpServletRequest request,HttpSession session) {
		T_Sell_Cashier cashier = getCashier(session);
		String number = request.getParameter("sd_number");
        Map<String,Object> param = new HashMap<String, Object>();
        bulidParam(param, cashier);
        param.put("number", number);
        depositService.back(param);
		return ajaxSuccess();
	}
}
