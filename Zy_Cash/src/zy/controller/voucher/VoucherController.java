package zy.controller.voucher;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.entity.sell.voucher.T_Sell_Voucher;
import zy.service.sell.voucher.VoucherService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("voucher")
public class VoucherController extends BaseController{
	
	@Resource
	private VoucherService voucherService;
	
	@RequestMapping(value = "/to_list", method = RequestMethod.GET)
	public String to_list() {
		return "voucher/list";
	}
	@RequestMapping(value = "/to_add", method = RequestMethod.GET)
	public String to_add() {
		return "voucher/add";
	}
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object list(
			@RequestParam(required = false) String vc_cardcode,
			HttpServletRequest request,HttpSession session) {
		T_Sell_Cashier user = getCashier(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SHOP_TYPE, user.getShop_type());
        param.put(CommonUtil.SHOP_CODE, user.getCa_shop_code());
        param.put(CommonUtil.SHOP_UPTYPE, user.getShop_uptype());
        param.put(CommonUtil.SHOP_UPCODE, user.getShop_upcode());
        param.put("vc_cardcode", StringUtil.decodeString(vc_cardcode));
		return ajaxSuccess(voucherService.listShop(param));
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Sell_Voucher voucher,String start_no,Integer add_count,String mark,Integer prefix,String vcl_ba_code,HttpSession session) {
		Map<String, Object> param = new HashMap<String,Object>();
		T_Sell_Cashier cashier = getCashier(session);
		voucher.setVc_state(0);
		voucher.setVc_used_money(0d);
		voucher.setVc_cashrate(StringUtil.double2Two(voucher.getVc_realcash()/voucher.getVc_money()));
		voucher.setCompanyid(cashier.getCompanyid());
		param.put("voucher", voucher);
		param.put(CommonUtil.SHOP_TYPE, cashier.getShop_type());
        param.put(CommonUtil.SHOP_CODE, cashier.getCa_shop_code());
        param.put(CommonUtil.SHOP_UPTYPE, cashier.getShop_uptype());
        param.put(CommonUtil.SHOP_UPCODE, cashier.getShop_upcode());
        param.put(CommonUtil.COMPANYID, cashier.getCompanyid());
		voucherService.saveShop(param);
		return ajaxSuccess(voucher);
	}
	@RequestMapping(value = "queryByCode", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object queryByCode(HttpServletRequest request,HttpSession session) {
		String vc_cardcode = request.getParameter("vc_cardcode");
		T_Sell_Cashier cashier = getCashier(session);
		Map<String,Object> param = new HashMap<String, Object>();
		param.put("vc_cardcode", vc_cardcode);
		param.put(CommonUtil.COMPANYID, cashier.getCompanyid());
		param.put(CommonUtil.SHOP_CODE, cashier.getCa_shop_code());
		param.put(CommonUtil.SHOP_UPTYPE, cashier.getShop_uptype());
        param.put(CommonUtil.SHOP_UPCODE, cashier.getShop_upcode());
        T_Sell_Voucher voucher = voucherService.queryByCode(param);
        if(null != voucher){
        	return ajaxSuccess(voucher);
        }else{
        	return ajaxFail();
        }
	}
}
