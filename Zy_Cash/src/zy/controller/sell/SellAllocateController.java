package zy.controller.sell;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sell.allocate.T_Sell_Allocate;
import zy.entity.sell.allocate.T_Sell_AllocateList;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.form.PageForm;
import zy.service.sell.allocate.SellAllocateService;
import zy.util.CommonUtil;
import zy.util.StringUtil;
import zy.vo.sell.SellAllocateVO;

@Controller
@RequestMapping("sell/allocate")
public class SellAllocateController extends BaseController{
	@Resource
	private SellAllocateService sellAllocateService;
	
	@RequestMapping(value = "to_list", method = RequestMethod.GET)
	public String to_list() {
		return "sell/allocate/list";
	}
	@RequestMapping(value = "to_list_copy_dialog", method = RequestMethod.GET)
	public String to_list_copy_dialog() {
		return "sell/allocate/list_copy_dialog";
	}
	@RequestMapping(value = "to_add", method = RequestMethod.GET)
	public String to_add() {
		return "sell/allocate/add";
	}
	@RequestMapping(value = "to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer ac_id,Model model,HttpSession session) {
		T_Sell_Cashier cashier = getCashier(session);
		T_Sell_Allocate allocate = sellAllocateService.load(ac_id);
		if(null != allocate){
			sellAllocateService.initUpdate(allocate.getAc_number(), cashier.getCa_id(), cashier.getCompanyid());
			model.addAttribute("allocate",allocate);
		}
		return "sell/allocate/update";
	}
	@RequestMapping(value = "to_view", method = RequestMethod.GET)
	public String to_view(@RequestParam Integer ac_id,Model model) {
		T_Sell_Allocate allocate = sellAllocateService.load(ac_id);
		if(null != allocate){
			model.addAttribute("allocate",allocate);
		}
		return "sell/allocate/view";
	}
	@RequestMapping(value = "to_select_product", method = RequestMethod.GET)
	public String to_select_product() {
		return "sell/allocate/select_product";
	}
	@RequestMapping(value = "to_temp_update", method = RequestMethod.GET)
	public String to_temp_update() {
		return "sell/allocate/temp_update";
	}
	@RequestMapping(value = "to_report", method = RequestMethod.GET)
	public String to_report() {
		return "sell/allocate/report";
	}
	
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = false) Integer ac_state,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String ac_out_shop,
			@RequestParam(required = false) String ac_in_shop,
			@RequestParam(required = false) String ac_man,
			@RequestParam(required = false) String ac_number,
			HttpServletRequest request,HttpSession session) {
		T_Sell_Cashier cashier = getCashier(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.SHOP_CODE, cashier.getCa_shop_code());
        param.put("ac_state", ac_state);
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("ac_out_shop", ac_out_shop);
        param.put("ac_in_shop", ac_in_shop);
        param.put("ac_man", StringUtil.decodeString(ac_man));
        param.put("ac_number", StringUtil.decodeString(ac_number));
		return ajaxSuccess(sellAllocateService.page(param));
	}
	
	@RequestMapping(value = "detail_list/{ac_number}/{ac_out_shop}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_list(@PathVariable String ac_number,@PathVariable String ac_out_shop,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("acl_number", ac_number);
		params.put("companyid", getCompanyid(session));
		List<T_Sell_AllocateList> list = sellAllocateService.detail_list(params);
		if(!ac_out_shop.equals(getCashier(session).getCa_shop_code())){
			for (T_Sell_AllocateList item : list) {
				item.setAcl_sell_price(item.getAcl_in_sell_price());
			}
		}
		return ajaxSuccess(list);
	}
	
	@RequestMapping(value = "detail_sum/{ac_number}/{ac_out_shop}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_sum(@PathVariable String ac_number,@PathVariable String ac_out_shop,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("acl_number", ac_number);
		params.put("companyid", getCompanyid(session));
		List<T_Sell_AllocateList> list = sellAllocateService.detail_sum(params);
		if(!ac_out_shop.equals(getCashier(session).getCa_shop_code())){
			for (T_Sell_AllocateList item : list) {
				item.setAcl_sell_price(item.getAcl_in_sell_price());
			}
		}
		return ajaxSuccess(list);
	}
	
	@RequestMapping(value = "detail_size_title/{ac_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_size_title(@PathVariable String ac_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("acl_number", ac_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(sellAllocateService.detail_size_title(params));
	}
	
	@RequestMapping(value = "detail_size/{ac_number}/{ac_out_shop}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_size(@PathVariable String ac_number,@PathVariable String ac_out_shop,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("acl_number", ac_number);
		params.put("companyid", getCompanyid(session));
		params.put("ac_out_shop", ac_out_shop);
		params.put("cashier", getCashier(session));
		return ajaxSuccess(sellAllocateService.detail_size(params));
	}
	
	@RequestMapping(value = "temp_list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_list(HttpSession session) {
		T_Sell_Cashier cashier = getCashier(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("acl_us_id", cashier.getCa_id());
		params.put("companyid", cashier.getCompanyid());
		return ajaxSuccess(sellAllocateService.temp_list(params));
	}
	
	@RequestMapping(value = "temp_sum", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_sum(HttpSession session) {
		T_Sell_Cashier cashier = getCashier(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("acl_us_id", cashier.getCa_id());
		params.put("companyid", cashier.getCompanyid());
		return ajaxSuccess(sellAllocateService.temp_sum(params));
	}
	
	@RequestMapping(value = "temp_size_title", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_size_title(HttpSession session) {
		T_Sell_Cashier cashier = getCashier(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("acl_us_id", cashier.getCa_id());
		params.put("companyid", cashier.getCompanyid());
		return ajaxSuccess(sellAllocateService.temp_size_title(params));
	}
	
	@RequestMapping(value = "temp_size", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_size(HttpSession session) {
		T_Sell_Cashier cashier = getCashier(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("acl_us_id", cashier.getCa_id());
		params.put("companyid", cashier.getCompanyid());
		return ajaxSuccess(sellAllocateService.temp_size(params));
	}
	
	@RequestMapping(value = "temp_save_bybarcode", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_save_bybarcode(HttpSession session,
			@RequestParam(required = true) String barcode,
			@RequestParam(required = true) Integer amount,
			@RequestParam(required = false) String sp_code) {
		T_Sell_Cashier cashier = getCashier(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("barcode", barcode);
		params.put("amount", amount);
		params.put("sp_code", sp_code);
		params.put("cashier", cashier);
		Map<String, Object> resultMap = sellAllocateService.temp_save_bybarcode(params);
		return ajaxSuccess(resultMap);
	}
	
	@RequestMapping(value = "page_product", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page_product(PageForm pageForm,@RequestParam(required = false) String alreadyExist,
			@RequestParam(required = false) String exactQuery,
			HttpSession session) {
		T_Sell_Cashier cashier = getCashier(session);
		Map<String,Object> param = new HashMap<String, Object>();
        param.put("searchContent", pageForm.getSearchContent());
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.COMPANYID, cashier.getCompanyid());
        param.put("us_id", cashier.getCa_id());
        param.put("alreadyExist", alreadyExist);
        param.put("exactQuery", exactQuery);
		return ajaxSuccess(sellAllocateService.page_product(param));
	}
	
	@RequestMapping(value = "temp_loadproduct", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_loadproduct(HttpSession session,
			@RequestParam(required = true) String pd_code,
			@RequestParam(required = true) String sp_code,
			@RequestParam(required = false) String exist,
			@RequestParam(required = false) String dp_code) {
		T_Sell_Cashier cashier = getCashier(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put(CommonUtil.COMPANYID, cashier.getCompanyid());
		params.put("pd_code", pd_code);
		params.put("sp_code", sp_code);
		params.put("exist", exist);
		params.put("dp_code", dp_code);
		params.put("us_id", cashier.getCa_id());
		return ajaxSuccess(sellAllocateService.temp_loadproduct(params));
	}
	
	@RequestMapping(value = "temp_save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_save(@RequestBody Map<String, Object> data,HttpSession session) {
		sellAllocateService.temp_save(SellAllocateVO.convertMap2Model(data, getCashier(session)));
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_del(@RequestParam Integer acl_id) {
		sellAllocateService.temp_del(acl_id);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateAmount", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateAmount(T_Sell_AllocateList temp) {
		sellAllocateService.temp_updateAmount(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateRemarkById", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateRemarkById(T_Sell_AllocateList temp) {
		temp.setAcl_remark(StringUtil.decodeString(temp.getAcl_remark()));
		sellAllocateService.temp_updateRemarkById(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateRemarkByPdCode", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateRemarkByPdCode(T_Sell_AllocateList temp,HttpSession session) {
		T_Sell_Cashier cashier = getCashier(session);
		temp.setAcl_remark(StringUtil.decodeString(temp.getAcl_remark()));
		temp.setAcl_us_id(cashier.getCa_id());
		temp.setCompanyid(cashier.getCompanyid());
		sellAllocateService.temp_updateRemarkByPdCode(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_delByPiCode", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_delByPiCode(@RequestParam String pd_code,
			@RequestParam(required = false) String cr_code,
			@RequestParam(required = false) String br_code,
			HttpSession session) {
		T_Sell_Cashier cashier = getCashier(session);
		T_Sell_AllocateList temp = new T_Sell_AllocateList();
		temp.setAcl_pd_code(pd_code);
		temp.setAcl_cr_code(cr_code);
		temp.setAcl_br_code(br_code);
		temp.setAcl_us_id(cashier.getCa_id());
		temp.setCompanyid(cashier.getCompanyid());
		sellAllocateService.temp_delByPiCode(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_clear", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_clear(HttpSession session) {
		T_Sell_Cashier cashier = getCashier(session);
		sellAllocateService.temp_clear(cashier.getCa_id(), cashier.getCompanyid());
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_import", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_import(@RequestBody Map<String, Object> data,HttpSession session) {
		T_Sell_Cashier cashier = getCashier(session);
		List<String[]> datas = SellAllocateVO.convertMap2Model_import(data);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("datas", datas);
		params.put("cashier", cashier);
		params.put("sp_code", data.get("sp_code"));
		sellAllocateService.temp_import(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_copy", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_copy(@RequestParam String sp_code,@RequestParam String ids,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("cashier", getCashier(session));
		params.put("sp_code", sp_code);
		params.put("ids", ids);
		sellAllocateService.temp_copy(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Sell_Allocate allocate,HttpSession session) {
		sellAllocateService.save(allocate, getCashier(session));
		return ajaxSuccess(allocate);
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Sell_Allocate allocate,HttpSession session) {
		sellAllocateService.update(allocate, getCashier(session));
		return ajaxSuccess(allocate);
	}
	
	@RequestMapping(value = "send", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object send(String number,String ac_out_dp,HttpSession session) {
		return ajaxSuccess(sellAllocateService.send(number, ac_out_dp, getCashier(session)));
	}
	
	@RequestMapping(value = "receive", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object receive(String number,String ac_in_dp,HttpSession session) {
		return ajaxSuccess(sellAllocateService.receive(number, ac_in_dp, getCashier(session)));
	}
	
	@RequestMapping(value = "reject", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object reject(String number,HttpSession session) {
		return ajaxSuccess(sellAllocateService.reject(number, getCashier(session)));
	}
	
	@RequestMapping(value = "rejectconfirm", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object rejectconfirm(String number,HttpSession session) {
		return ajaxSuccess(sellAllocateService.rejectconfirm(number, getCashier(session)));
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(String number,HttpSession session) {
		sellAllocateService.del(number, getCompanyid(session));
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "print", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object print(String number,Integer sp_id,Integer displayMode,HttpSession session) {
		Map<String, Object> resultMap = sellAllocateService.loadPrintData(number, sp_id, displayMode, getCashier(session));
		return ajaxSuccess(SellAllocateVO.buildPrintJson(resultMap, displayMode));
	}
	
	@RequestMapping(value = "pageReport", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object pageReport(PageForm pageForm,
			@RequestParam(required = false) Integer ac_state,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String ac_out_shop,
			@RequestParam(required = false) String ac_in_shop,
			@RequestParam(required = false) String ac_man,
			@RequestParam(required = false) String ac_number,
			HttpServletRequest request,HttpSession session) {
		T_Sell_Cashier cashier = getCashier(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.SHOP_CODE, cashier.getCa_shop_code());
        param.put("ac_state", ac_state);
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("ac_out_shop", ac_out_shop);
        param.put("ac_in_shop", ac_in_shop);
        param.put("ac_man", StringUtil.decodeString(ac_man));
        param.put("ac_number", StringUtil.decodeString(ac_number));
		return ajaxSuccess(sellAllocateService.pageReport(param));
	}
	
}
