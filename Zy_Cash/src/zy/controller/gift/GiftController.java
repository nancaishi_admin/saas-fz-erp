package zy.controller.gift;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.form.PageForm;
import zy.service.shop.gift.GiftService;
import zy.util.CommonUtil;
import zy.util.NumberUtil;
import zy.util.StringUtil;
@Controller
@RequestMapping("sell/gift")
public class GiftController extends BaseController {
	@Resource
	private GiftService giftService;
	
	@RequestMapping(value = "/to_list", method = RequestMethod.GET)
	public String to_list() {
		return "gift/list";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,String vm_mobile,
			HttpServletRequest request,HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        bulidParam(param, getCashier(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("vm_mobile", StringUtil.decodeString(vm_mobile));
		return ajaxSuccess(giftService.sendList(param));
	}
	
	@RequestMapping(value = "send", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object send(String vm_id,String point,
			String json,String vm_code,
			HttpServletRequest request,HttpSession session) {
		Map<String,Object> param = new HashMap<String, Object>();
		bulidParam(param, getCashier(session));
		param.put("vm_id", vm_id);
		param.put("vm_code", vm_code);
		param.put("point", -NumberUtil.toInteger(point));
		param.put("json", json);
		giftService.send(param);
		return ajaxSuccess();
	}

}
