package zy.controller.shop;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.service.shop.plan.PlanService;
import zy.util.CommonUtil;
import zy.util.DateUtil;

@Controller
@RequestMapping("shop/plan")
public class PlanController extends BaseController{
	@Resource
	private PlanService planService;
	
	@RequestMapping(value = "to_progress_chart", method = RequestMethod.GET)
	public String to_progress_chart() {
		return "shop/plan/progress_chart";
	}
	
	@RequestMapping(value = "statByYearMonth", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object statByYearMonth(HttpSession session) {
		T_Sell_Cashier cashier = getCashier(session);
		Map<String,Object> param = new HashMap<String, Object>();
		param.put(CommonUtil.COMPANYID, getCompanyid(session));
		param.put(CommonUtil.SHOP_TYPE, cashier.getShop_type());
		param.put(CommonUtil.SHOP_CODE, cashier.getCa_shop_code());
		param.put("year", DateUtil.getYear());
		param.put("month", DateUtil.getMonth(DateUtil.getCurrentDate()));
		return ajaxSuccess(planService.statByYearMonth(param));
	}
	
}
