package zy.controller.service;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.PageData;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.entity.shop.service.T_Shop_Service;
import zy.entity.shop.service.T_Shop_Service_Item;
import zy.service.shop.service.ServiceService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("service")
public class ServiceController  extends BaseController{
	@Resource
	private ServiceService serviceService;
	
	@RequestMapping(value = "/to_list", method = RequestMethod.GET)
	public String to_list() {
		return "service/list";
	}
	
	@RequestMapping("to_add")
	public String to_add() {
		return "service/add";
	}
	
	@RequestMapping(value = "/to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer ss_id,Model model,HttpSession session) {
		T_Shop_Service service = serviceService.load(ss_id);
		if(null != service){
			model.addAttribute("service",service);
		}
		return "service/update";
	}
	
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object list(HttpServletRequest request,HttpSession session) {
		String page = request.getParameter("page"); // 页码
        String rows = request.getParameter("rows"); // 每页条数
        String sidx = request.getParameter("sidx");
        String sord = request.getParameter("sord");
        String begindate = request.getParameter("begindate");
        String enddate = request.getParameter("enddate");
        String ss_state = request.getParameter("ss_state");
        String ss_name = request.getParameter("ss_name");
        String ss_tel = request.getParameter("ss_tel");
        
        int pageIndex = 1;
        int pageSize = super.pagesize;
        if (org.apache.commons.lang.StringUtils.isNotEmpty(page)) {
        	pageIndex = Integer.parseInt(page);
        	pageIndex = pageIndex == 0 ? 1 : pageIndex;
        }
        if (org.apache.commons.lang.StringUtils.isNotEmpty(rows)) {
        	pageSize = Integer.parseInt(rows);
        }
        T_Sell_Cashier cashier = getCashier(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, cashier.getCompanyid());
        param.put(CommonUtil.SHOP_CODE, cashier.getCa_shop_code());
        param.put(CommonUtil.SHOP_TYPE, cashier.getShop_type());
        param.put(CommonUtil.SHOP_UPCODE, cashier.getShop_upcode());
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("ss_state", ss_state);
        param.put("ss_name", StringUtil.decodeString(ss_name));
        param.put("ss_tel", ss_tel);
        param.put(CommonUtil.PAGESIZE, pageSize);
        param.put(CommonUtil.PAGEINDEX, pageIndex);
        param.put(CommonUtil.SIDX, sidx);
        param.put(CommonUtil.SORD, sord);
		PageData<T_Shop_Service> pageData = serviceService.page(param);
		return ajaxSuccess(pageData, "查询成功, 共" + pageData.getList().size() + "条数据");
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Shop_Service service,HttpServletRequest request,HttpSession session) {
		 T_Sell_Cashier cashier = getCashier(session);
		 serviceService.save(service,cashier);
		 return ajaxSuccess();
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Shop_Service service,HttpServletRequest request,HttpSession session) {
		T_Sell_Cashier cashier = getCashier(session);
		serviceService.update(service,cashier);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "list_serviceitem_combo", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public void list_serviceitem_combo(HttpServletRequest request,HttpSession session,HttpServletResponse response) {
		try {
			PrintWriter out=null;
			 T_Sell_Cashier cashier = getCashier(session);
			Map<String,Object> param = new HashMap<String, Object>();
			param.put(CommonUtil.COMPANYID, cashier.getCompanyid());
			String ssi_list="{\"data\":{\"items\":[";
			List<T_Shop_Service_Item> list = serviceService.list_serviceitem_combo(param);
			for (int i=0;i<list.size();i++){
				ssi_list+="{\"ssi_id\":\""+list.get(i).getSsi_id()+"\",\"ssi_name\":\""+list.get(i).getSsi_name()+"\"},";
			}
			ssi_list=ssi_list.substring(0,ssi_list.length()-1);
			ssi_list+="]}}";
			out = response.getWriter();
			out.print(ssi_list);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
