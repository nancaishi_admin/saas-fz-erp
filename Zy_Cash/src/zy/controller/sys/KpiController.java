package zy.controller.sys;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.service.sys.kpi.KpiService;
import zy.util.CommonUtil;

@Controller
@RequestMapping("sys/kpi")
public class KpiController extends BaseController{
	@Resource
	private KpiService kpiService;
	
	@RequestMapping("to_view")
	public String to_view() {
		return "sys/kpi/view";
	}
	
	@RequestMapping(value = "listScores", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listScores(@RequestParam(required = true) String ki_code,HttpSession session) {
		T_Sell_Cashier cashier = getCashier(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        if(CommonUtil.FOUR.equals(cashier.getShop_type().toString())){//加盟
        	params.put(CommonUtil.SHOP_CODE, cashier.getCa_shop_code());
		}else{//自营、合伙
			params.put(CommonUtil.SHOP_CODE, cashier.getShop_upcode());
		}
        params.put("ki_code", ki_code);
		return ajaxSuccess(kpiService.listScores(params));
	}
	
}
