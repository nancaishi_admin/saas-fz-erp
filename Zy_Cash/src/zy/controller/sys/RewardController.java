package zy.controller.sys;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.form.PageForm;
import zy.service.sys.reward.RewardService;
import zy.util.CommonUtil;

@Controller
@RequestMapping("sys/reward")
public class RewardController extends BaseController{
	@Resource
	private RewardService rewardService;
	
	@RequestMapping("to_list_dialog")
	public String to_list_dialog() {
		return "sys/reward/list_dialog";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,HttpSession session) {
		T_Sell_Cashier cashier = getCashier(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put(CommonUtil.PAGESIZE, pageForm.getRows());
        params.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        params.put(CommonUtil.SHOP_CODE, cashier.getShop_upcode());
        params.put("searchContent", pageForm.getSearchContent());
		return ajaxSuccess(rewardService.page(params));
	}
	
}
