package zy.controller.index;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.entity.sell.day.T_Sell_Day;
import zy.entity.sell.shift.T_Sell_Shift;
import zy.form.StringForm;
import zy.interceptor.CookieHandler;
import zy.interceptor.SessionHandler;
import zy.service.base.emp.EmpService;
import zy.service.index.IndexService;
import zy.service.sell.cash.CashService;
import zy.service.sell.day.DayService;
import zy.service.sell.dayend.DayEndService;
import zy.service.sell.displayarea.DisplayAreaService;
import zy.service.sell.shift.ShiftService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.MD5;

@Controller
public class IndexController extends BaseController{
	@Autowired
	private IndexService indexService;
	@Resource
	private DayService dayService;
	@Resource
	private EmpService empService;
	@Resource
	private DisplayAreaService displayAreaService;
	@Resource
	private CashService cashService;
	@Resource
	private ShiftService shiftService;
	@Resource
	private DayEndService dayEndService;
	
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String index() {
		return "index";
	}
	@RequestMapping(value = "/toIndex", method = RequestMethod.GET)
	public String toIndex() {
		return "to_index";
	}
	@RequestMapping(value = "/to_shift", method = RequestMethod.GET)
	public String to_shift() {
		return "index/shift";
	}
	@RequestMapping(value = "shift", method = {RequestMethod.GET,RequestMethod.POST})
	public @ResponseBody Object shift(HttpServletRequest request,HttpSession session) {
		Map<String,Object> param = new HashMap<String, Object>();
		bulidParam(param, getCashier(session));
		List<T_Sell_Shift> shiftList = shiftService.listShop(param);
		return ajaxSuccess(shiftList);
	}
	/**
	 * 管理员主页面
	 * */
	@RequestMapping(value = "/main", method = RequestMethod.GET)
	public String main(HttpServletRequest request,HttpSession session) {
		request.setAttribute("today", DateUtil.getYearMonthDate());
		return "index/main";
	}
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public @ResponseBody Object login(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
		String us_account = request.getParameter("us_account");
		String us_pass = request.getParameter("us_pass");
		String co_code = request.getParameter("co_code");
		String remember = request.getParameter("remember");
		Map<String,Object> paramMap = CookieHandler.getCookies(request);
		session.removeAttribute(CommonUtil.KEY_VIP);//删除会员的信息
		paramMap.put("ca_pass", MD5.encryptMd5(us_pass));
		paramMap.put("ca_co_code",co_code);
		paramMap.put("ca_code",us_account);	
		indexService.login(paramMap);
		T_Sell_Cashier user = (T_Sell_Cashier)paramMap.get(CommonUtil.KEY_CASHIER);
		if(null != user){
			session.setAttribute(CommonUtil.KEY_CASHIER, user);
			session.setAttribute(CommonUtil.KEY_CASHIER_SET, paramMap);
			SessionHandler.kickUser(user.getCompanyid()+""+user.getCa_shop_code()+user.getCa_em_code(), session);
		}
		Map<String,Object> cookie = CookieHandler.getCookies(request);
		if(null == cookie || null == cookie.get("ca_remember")){
			cookie = new HashMap<String,Object>(7);
			cookie.put("ca_co_code",co_code);
			cookie.put("ca_code",us_account);
			cookie.put("ca_remember",remember);
			CookieHandler.createCookie(response,cookie,10);
		}else{
			cookie.put("ca_code", us_account);
			cookie.put("ca_co_code", co_code);
			cookie.put("ca_remember",remember);
		}
		return returnResult(user);
	}
	@RequestMapping(value = "/logout", method = {RequestMethod.GET,RequestMethod.POST})
	public String logout(HttpSession session) {
		T_Sell_Cashier user = (T_Sell_Cashier)session.getAttribute(CommonUtil.KEY_CASHIER);
		if(user != null){
			SessionHandler.delUserFromSession(user.getCompanyid()+user.getCa_shop_code()+user.getCa_em_code());
		}
		Enumeration<?> keys = session.getAttributeNames();
		String key;
		for (;keys.hasMoreElements();){
			key = (String)keys.nextElement();
			session.removeAttribute(key);
		}
		if(session != null)session.invalidate();
		return "redirect:/index";
	}
	
	
	@RequestMapping(value = "index/to_pass", method = RequestMethod.GET)
	public String to_pass() {
		return "index/pass";
	}
	@RequestMapping(value = "index/editPass", method = {RequestMethod.GET,RequestMethod.POST})
	public @ResponseBody Object editPass(HttpServletRequest request,HttpSession session) {
		T_Sell_Cashier user = (T_Sell_Cashier)session.getAttribute(CommonUtil.KEY_CASHIER);
		String old_pass = request.getParameter("old_pass");
		String new_pass = request.getParameter("new_pass");
		Map<String,Object> param = new HashMap<String, Object>(3);
		param.put("old_pass", MD5.encryptMd5(old_pass));
		param.put("new_pass", MD5.encryptMd5(new_pass));
		param.put("ca_id", user.getCa_id());
		indexService.editPass(param);
		return ajaxSuccess();
	}
	@RequestMapping(value = "init", method = {RequestMethod.GET,RequestMethod.POST})
	public @ResponseBody Object init(HttpServletRequest request,HttpSession session) {
		T_Sell_Cashier user = (T_Sell_Cashier)session.getAttribute(CommonUtil.KEY_CASHIER);
		final Map<String,Object> param = new HashMap<String, Object>(3);
		param.put(CommonUtil.COMPANYID, user.getCompanyid());
		param.put(CommonUtil.SHOP_CODE, user.getCa_shop_code());
		param.put("today", DateUtil.getYearMonthDate());
		List<StringForm> empList = empService.listShop(param);	
		List<StringForm> areaList = displayAreaService.listShop(param);
		Thread weather = new Thread(new Runnable() {
			public void run() {
				dayService.weather(param);
			}
		});
		weather.start();  //上线时则功能要开启
		T_Sell_Day day = dayService.queryDay(param);
		param.put(CommonUtil.PUTUP, CommonUtil.ONE);
		param.put(CommonUtil.EMP_CODE, user.getCa_em_code());
		List<StringForm> putup = cashService.putUpList(param);
		Map<String,Object> _param = new HashMap<String, Object>(4);
		_param.put("emp", empList);
		_param.put("area", areaList);
		_param.put("day", day);
		_param.put("putup", putup);
		return ajaxSuccess(_param);
	}
	
}
