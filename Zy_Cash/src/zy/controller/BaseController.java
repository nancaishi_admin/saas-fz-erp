package zy.controller;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;

import zy.entity.JsonBean;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.util.CommonUtil;
import zy.util.StringUtil;

public class BaseController {
	protected final int SUCCESS = 200;
	protected final int EXISTED = 304;
	protected final int FAIL = 500;
	protected final int NOTFOUND = 404;
	protected int pagesize = 10;

	protected JsonBean returnResult(Object data) {
		JsonBean jsonBean = new JsonBean();
		if (null != data) {
			jsonBean.setStat(SUCCESS);
			jsonBean.setData(data);
		} else {
			jsonBean.setStat(FAIL);
			jsonBean.setData("");
		}
		return jsonBean;
	}

	protected JsonBean success(int flag) {
		JsonBean jsonBean = new JsonBean();
		jsonBean.setStat(SUCCESS);
		jsonBean.setData(flag);
		return jsonBean;
	}

	protected JsonBean success(String flag) {
		JsonBean jsonBean = new JsonBean();
		jsonBean.setStat(SUCCESS);
		jsonBean.setData(flag);
		return jsonBean;
	}

	protected JsonBean fail(int flag) {
		JsonBean jsonBean = new JsonBean();
		jsonBean.setStat(FAIL);
		jsonBean.setData(flag);
		return jsonBean;
	}

	protected JsonBean fail(String flag) {
		JsonBean jsonBean = new JsonBean();
		jsonBean.setStat(FAIL);
		jsonBean.setData(flag);
		return jsonBean;
	}

	protected JsonBean result(int flag) {
		JsonBean jsonBean = new JsonBean();
		if (SUCCESS == flag) {
			jsonBean.setStat(SUCCESS);
			jsonBean.setMessage("保存成功!");
		} else if (EXISTED == flag) {
			jsonBean.setStat(EXISTED);
			jsonBean.setMessage("数据已存在!");
		} else if(NOTFOUND == flag){
			jsonBean.setStat(NOTFOUND);
			jsonBean.setMessage("查询数据不存在!");
		}else{
			jsonBean.setStat(FAIL);
			jsonBean.setMessage("保存失败!");
		}
		jsonBean.setData(flag);
		return jsonBean;
	}

	protected JsonBean result(String flag) {
		JsonBean jsonBean = new JsonBean();
		if (null != flag && "1".equals(flag)) {
			jsonBean.setStat(SUCCESS);
			jsonBean.setMessage("保存成功!");
		} else if ("3".equals(flag)) {
			jsonBean.setStat(EXISTED);
			jsonBean.setMessage("数据已存在!");
		} else {
			jsonBean.setStat(FAIL);
			jsonBean.setMessage("保存失败!");
		}
		jsonBean.setData(flag);
		return jsonBean;
	}

	public Object ajaxSuccess() {
		return ajaxSuccessMessage("");
	}

	public Object ajaxSuccess(Object data) {
		JsonBean jsonBean = new JsonBean();
		jsonBean.setStat(SUCCESS);
		jsonBean.setData(data);
		jsonBean.setMessage("操作成功");
		return jsonBean;
	}

	public Object ajaxSuccess(Object data, String message) {
		JsonBean jsonBean = new JsonBean();
		jsonBean.setStat(SUCCESS);
		jsonBean.setData(data);
		if (message != null && !message.equals("")) {
			jsonBean.setMessage(message);
		} else {
			jsonBean.setMessage("操作成功");
		}
		return jsonBean;
	}

	public Object ajaxSuccessMessage(String message) {
		JsonBean jsonBean = new JsonBean();
		jsonBean.setStat(SUCCESS);
		if (message != null && !message.equals("")) {
			jsonBean.setMessage(message);
		} else {
			jsonBean.setMessage("操作成功");
		}
		return jsonBean;
	}

	public Object ajaxFail() {
		JsonBean jsonBean = new JsonBean();
		jsonBean.setStat(FAIL);
		jsonBean.setMessage("");
		return jsonBean;
	}

	public Object ajaxFail(String message) {
		JsonBean jsonBean = new JsonBean();
		jsonBean.setStat(FAIL);
		if (message != null && !message.equals("")) {
			jsonBean.setMessage(message);
		} else {
			jsonBean.setMessage("操作失败");
		}
		return jsonBean;
	}

	public Object ajaxFail(Object data, String message) {
		JsonBean jsonBean = new JsonBean();
		jsonBean.setStat(FAIL);
		jsonBean.setData(data);
		if (message != null && !message.equals("")) {
			jsonBean.setMessage(message);
		} else {
			jsonBean.setMessage("操作失败,请联系管理员!");
		}
		return jsonBean;
	}

	@ExceptionHandler(IllegalArgumentException.class)
	@ResponseBody
	public Object illegalArgumentException(WebRequest request, IllegalArgumentException exception) {
		if (exception.getMessage() != null && !exception.getMessage().equals("")) {
			return ajaxFail(exception.getMessage());
		} else {
			return ajaxFail();
		}
	}

	@ExceptionHandler(EmptyResultDataAccessException.class)
	@ResponseBody
	public Object emptyResultDataAccessException(WebRequest request, EmptyResultDataAccessException exception) {
		if (exception.getMessage() != null && !exception.getMessage().equals("")) {
			return ajaxFail("没有查询到数据");
		} else {
			return ajaxFail();
		}
	}

	@ExceptionHandler(RuntimeException.class)
	@ResponseBody
	public Object runtimeException(WebRequest request, RuntimeException exception) {
		if (exception.getMessage() != null && !exception.getMessage().equals("")) {
			return ajaxFail(exception.getMessage());
		} else {
			return ajaxFail("");
		}
	}
	

	@ExceptionHandler(Exception.class)
	@ResponseBody
	public Object exception(WebRequest request, Exception exception) {
		return ajaxFail("操作失败!");
	}

	public T_Sell_Cashier getCashier(HttpSession session) {
		T_Sell_Cashier user = (T_Sell_Cashier) session.getAttribute(CommonUtil.KEY_CASHIER);
		return user;
	}
	public int getCompanyid(HttpSession session){
		T_Sell_Cashier user = (T_Sell_Cashier) session.getAttribute(CommonUtil.KEY_CASHIER);
		if(user != null){
			return user.getCompanyid();
		}
		return 0;
	}
	public void bulidParam(Map<String,Object> param,T_Sell_Cashier cashier){
		param.put(CommonUtil.COMPANYID, cashier.getCompanyid());
        param.put(CommonUtil.SHOP_TYPE, StringUtil.trimString(cashier.getShop_type()));
        param.put(CommonUtil.SHOP_CODE, cashier.getCa_shop_code());
        param.put(CommonUtil.SHOP_NAME, cashier.getShop_name());
        param.put(CommonUtil.EMP_CODE, cashier.getCa_em_code());
        param.put(CommonUtil.EMP_NAME, cashier.getEm_name());
        param.put(CommonUtil.DP_CODE, cashier.getDp_code());
        param.put(CommonUtil.SHOP_UPTYPE, StringUtil.trimString(cashier.getShop_uptype()));
        param.put(CommonUtil.SHOP_UPCODE, cashier.getShop_upcode());
        param.put(CommonUtil.PUTUP, CommonUtil.ZERO);//不是挂单的数据
        param.put("st_code", cashier.getSt_code());
	}
	
	public void removeVip(HttpSession session){
		session.removeAttribute(CommonUtil.KEY_VIP);
	}
}
