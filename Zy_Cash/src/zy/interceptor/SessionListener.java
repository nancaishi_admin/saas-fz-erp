package zy.interceptor;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.util.CommonUtil;

public class SessionListener implements HttpSessionListener {
	public void sessionDestroyed(HttpSessionEvent sessionEvent) {
		HttpSession session = sessionEvent.getSession();
		if(session != null){
			T_Sell_Cashier login = (T_Sell_Cashier)session.getAttribute(CommonUtil.KEY_CASHIER);
			if(login != null){
				SessionHandler.delUserFromSession(login.getCompanyid()+login.getCa_shop_code()+login.getCa_em_code());
			}
		}
	}

	@Override
	public void sessionCreated(HttpSessionEvent se) {
	}
}
