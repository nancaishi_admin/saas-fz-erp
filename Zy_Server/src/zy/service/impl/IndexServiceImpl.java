package zy.service.impl;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.IndexDAO;
import zy.dao.sys.sqb.SqbDAO;
import zy.service.IndexService;
@Service
public class IndexServiceImpl implements IndexService{
	@Resource
	private IndexDAO indexDAO;
	@Resource
	private SqbDAO sqbDAO;
	@Override
	public void login(Map<String, Object> paramMap) {
		//1.登录内容
		indexDAO.login(paramMap);
	}
	
	@Override
	public void editPass(Map<String, Object> paramMap) {
		indexDAO.editPass(paramMap);
	}

}
