package zy.service.base;

import java.util.Map;

import zy.dao.base.pojo.ShopDO;
import zy.entity.PageData;

public interface CompanyShopService {
	PageData<ShopDO> listShop(Map<String, Object> paramMap);
	
	void delayShop(Map<String, Object> paramMap);
}
