package zy.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.IndexDAO;
import zy.entity.sys.menu.T_Sys_Menu;
import zy.entity.sys.user.T_Sys_User;
import zy.util.CommonUtil;
import zy.vo.sys.MainVO;
@Repository
public class IndexDAOImpl extends BaseDaoImpl implements IndexDAO{

	@Override
	public void login(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer();
		sql.append(" select us_id,us_code,us_name,us_ro_code");
		sql.append(" ,us_end,us_limit");
		sql.append(" from t_zhjr_user u");
		sql.append(" where 1=1");
		sql.append(" and us_state=0");
		sql.append(" and us_pass=:us_pass");
		sql.append(" and us_account=:us_code");
		long a = System.currentTimeMillis();
		T_Sys_User user = namedParameterJdbcTemplate.queryForObject(sql.toString(), 
				param, 
				new BeanPropertyRowMapper<>(T_Sys_User.class));
		long b = System.currentTimeMillis();
		System.out.println("time:"+(b-a));
		sql.setLength(0);
		sql.append(" SELECT mn_id,mn_code,mn_name,rm_limit as mn_limit,mn_upcode,mn_url,mn_icon,mn_style,mn_child");
		sql.append(" FROM t_zhjr_rolemenu r");
		sql.append(" JOIN t_zhjr_menu m");
		sql.append(" ON mn_code=rm_mn_code");
		sql.append(" WHERE rm_ro_code=:rolecode");
		sql.append(" AND mn_state=0");
		sql.append(" AND rm_state=0");
		sql.append(" ORDER BY mn_order");
		param.put("rolecode", user.getUs_ro_code());
		param.put(CommonUtil.COMPANYID,user.getCompanyid());
		a = System.currentTimeMillis();
		List<T_Sys_Menu> menuList = namedParameterJdbcTemplate.query(sql.toString(), 
				param, 
				new BeanPropertyRowMapper<>(T_Sys_Menu.class));
		b = System.currentTimeMillis();
		System.out.println("time:"+(b-a));
		param.put(CommonUtil.KEY_USER, user);
		param.put(CommonUtil.KEY_MENU, menuList);
		param.put(CommonUtil.KEY_MENULIMIT, MainVO.buildMenuLimit(menuList));
		
		sql.setLength(0);
		sql.append(" update t_zhjr_user");
		sql.append(" set us_last=sysdate()");
		sql.append(" where us_id=:us_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(user));
	}

	@Override
	public void editPass(Map<String, Object> paramMap) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT us_id ");
		sql.append(" FROM t_zhjr_user");
		sql.append(" WHERE us_pass=:old_pass");
		sql.append(" AND us_id=:us_id");
		Integer id = null;
		try {
			id = namedParameterJdbcTemplate.queryForObject(sql.toString(), paramMap, Integer.class);
		} catch (Exception e) {
			throw new IllegalArgumentException("旧密码不正确");
		}
		if(null != id){
			sql.setLength(0);
			sql.append(" UPDATE t_zhjr_user");
			sql.append(" SET us_pass=:new_pass");
			sql.append(" WHERE us_id=:us_id");
			namedParameterJdbcTemplate.update(sql.toString(),paramMap);
		}else{
			throw new IllegalArgumentException("旧密码不正确");
		}
	}

}
