package zy.dao.base.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.base.CompanyShopDAO;
import zy.dao.base.pojo.ShopDO;
import zy.util.StringUtil;
@Repository
public class CompanyShopDAOImpl extends BaseDaoImpl implements CompanyShopDAO{

	@Override
	public List<ShopDO> listShop(Map<String, Object> param) {
		Object co_code = param.get("co_code");
		Object search = param.get("searchContent");
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT s.sp_id,s.sp_code,s.sp_name,s.sp_state,s.sp_end,c.co_code,c.co_name,c.co_man");
		sql.append(" FROM t_base_shop s ");
		sql.append(" JOIN t_sys_company c");
		sql.append(" ON c.co_id=s.companyid");
		sql.append(" WHERE 1=1");
		if(StringUtil.isNotEmpty(search)){
			sql.append(" AND INSTR(sp_name,:searchContent)>0");
		}
		if(StringUtil.isNotEmpty(co_code)){
			sql.append(" AND c.co_code=:co_code");
		}
		sql.append(" ORDER BY c.co_code DESC");
		sql.append(" LIMIT :start,:end");
		List<ShopDO> list = namedParameterJdbcTemplate.query(sql.toString(), param, 
				new BeanPropertyRowMapper<>(ShopDO.class));
		return list;
	}

	@Override
	public Integer countShop(Map<String, Object> param) {
		Object co_code = param.get("co_code");
		Object search = param.get("searchContent");
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT COUNT(1)");
		sql.append(" FROM t_base_shop s ");
		sql.append(" JOIN t_sys_company c");
		sql.append(" ON c.co_id=s.companyid");
		sql.append(" WHERE 1=1");
		if(StringUtil.isNotEmpty(search)){
			sql.append(" AND INSTR(sp_name,:searchContent)>0");
		}
		if(StringUtil.isNotEmpty(co_code)){
			sql.append(" AND c.co_code=:co_code");
		}
		Integer count = namedParameterJdbcTemplate.queryForObject(sql.toString(), param, Integer.class);
		return count;
	}

	@Override
	public void delayShop(Map<String, Object> paramMap) {
		StringBuffer sql = new StringBuffer();
		sql.append(" UPDATE t_base_shop SET ");
		sql.append(" sp_end=ADDDATE(sp_end ,INTERVAL :months MONTH)");
		sql.append(" WHERE sp_id=:sp_id");
		namedParameterJdbcTemplate.update(sql.toString(), paramMap);
	}

}
