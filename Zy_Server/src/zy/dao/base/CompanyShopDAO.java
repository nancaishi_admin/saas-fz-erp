package zy.dao.base;

import java.util.List;
import java.util.Map;

import zy.dao.base.pojo.ShopDO;

public interface CompanyShopDAO {
	public List<ShopDO> listShop(Map<String, Object> param);
	public Integer countShop(Map<String, Object> param);
	public void delayShop(Map<String, Object> paramMap);
}
