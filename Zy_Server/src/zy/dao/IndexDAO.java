package zy.dao;

import java.util.List;
import java.util.Map;

import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.entity.sell.cashier.T_Sell_Cashier_Set;
import zy.entity.sell.set.T_Sell_Set;

public interface IndexDAO {
	public void login(Map<String,Object> param);
	void editPass(Map<String,Object> paramMap);
}
