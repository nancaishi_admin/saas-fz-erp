package zy.controller.stock;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.stock.data.T_Stock_Data;
import zy.entity.stock.data.T_Stock_DataView;
import zy.service.stock.data.DataService;
@Controller
@RequestMapping("api/stock")
public class StockController extends BaseController{
	@Resource
	private DataService dataService;
	
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object list(@RequestBody Map<String, Object> params) {
		List<T_Stock_Data> list = dataService.listAPI(params);
		return ajaxSuccess(list);
	}
	
	@RequestMapping(value = "loadByBarcode", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object loadByBarcode(@RequestBody Map<String, Object> params) {
		return ajaxSuccess(dataService.loadByBarcodeAPI(params));
	}
	
	@RequestMapping(value = "query", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object query(@RequestBody Map<String, Object> params) {
		Map<String,Object> data = dataService.queryAPI(params);
		return ajaxSuccess(data);
	}
	@RequestMapping(value = "other", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object other(@RequestBody Map<String, Object> params) {
		List<T_Stock_DataView> data = dataService.otherAPI(params);
		return ajaxSuccess(data);
	}
}
