package zy.controller.vip;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSON;

import zy.controller.BaseController;
import zy.dto.base.emp.EmpLoginDto;
import zy.entity.vip.member.T_Vip_Member;
import zy.entity.vip.member.T_Vip_Member_Info;
import zy.service.vip.member.MemberService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("api/vip/member")
public class MemberController extends BaseController{
	@Resource
	private MemberService memberService;
	
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object list(@RequestBody Map<String, Object> params) {
		return ajaxSuccess(memberService.list_server(params));
	}
	
	@RequestMapping(value = "save", method = RequestMethod.POST)
	@ResponseBody
	public Object save(@RequestParam(value = "img", required = false) MultipartFile img,
			@RequestParam(value = "member", required = true) String member,
			@RequestParam(value = "memberInfo", required = true) String memberInfo,
			@RequestParam(value = "empLoginDto", required = true) String empLoginDto) throws IllegalStateException, IOException {
		T_Vip_Member memberModel = JSON.parseObject(member, T_Vip_Member.class);
		T_Vip_Member_Info memberInfoModel = JSON.parseObject(memberInfo, T_Vip_Member_Info.class);
		String newFilePrefix = new Date().getTime() + "" + new Random().nextInt(9999);
		String suffix = null;
		if (img != null) {
			String uploadfileName = img.getOriginalFilename();
			suffix = uploadfileName.substring(uploadfileName.lastIndexOf("."));
			String img_path = CommonUtil.VIP_IMG_PATH + "/" + newFilePrefix + suffix;
			File savefile = new File(CommonUtil.CHECK_BASE, img_path);
			if (!savefile.getParentFile().exists()){
				savefile.getParentFile().mkdirs();
			}
			img.transferTo(savefile);
			memberModel.setVm_img_path(img_path);
		}
		memberService.save(memberModel, memberInfoModel,JSON.parseObject(empLoginDto,EmpLoginDto.class));
		return ajaxSuccess(memberModel);
	}
	
	@RequestMapping(value = "update", method = RequestMethod.POST)
	@ResponseBody
	public Object update(@RequestBody Map<String, Object> params) {
		T_Vip_Member member = JSON.parseObject(StringUtil.trimString(params.get("member")), T_Vip_Member.class);
		T_Vip_Member_Info memberInfo = JSON.parseObject(StringUtil.trimString(params.get("memberInfo")), T_Vip_Member_Info.class);
		EmpLoginDto empLoginDto = JSON.parseObject(StringUtil.trimString(params.get("empLoginDto")), EmpLoginDto.class);
		memberService.update(member, memberInfo, empLoginDto);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "updateImg", method = RequestMethod.POST)
	@ResponseBody
	public Object updateDoorImg(@RequestParam(required = true) Integer vm_id,
			@RequestParam(value = "img", required = true) MultipartFile img,HttpSession session)throws IOException {
		if (img == null || img.getSize() <= 0) {
			return ajaxFail("���ϴ�ͼƬ");
		}
		String uploadfileName = img.getOriginalFilename();
		String suffix = uploadfileName.substring(uploadfileName.lastIndexOf("."));
		String img_path = CommonUtil.VIP_IMG_PATH + "/" + new Date().getTime() + "" + new Random().nextInt(9999) + suffix;
		File savefile = new File(CommonUtil.CHECK_BASE, img_path);
		if (!savefile.getParentFile().exists()){
			savefile.getParentFile().mkdirs();
		}
		img.transferTo(savefile);
		T_Vip_Member member = new T_Vip_Member();
		member.setVm_id(vm_id);
		member.setVm_img_path(img_path);
		memberService.updateImg(member);
		return ajaxSuccess(member);
	}
	
	@RequestMapping(value = "load/{vm_id}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object load(@PathVariable Integer vm_id) {
		Map<String, Object> resultMap = new HashMap<String,Object>();
		T_Vip_Member member = memberService.queryByID(vm_id);
		resultMap.put("member", member);
		if(null != member){
			T_Vip_Member_Info memberInfo = memberService.queryInfoByCode(member.getVm_code(), member.getCompanyid());
			resultMap.put("memberInfo", memberInfo);
		}
		return ajaxSuccess(resultMap);
	}
	
	@RequestMapping(value = "search", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object search(@RequestBody Map<String, Object> params) {
		return ajaxSuccess(memberService.search(params));
	}
	
}
