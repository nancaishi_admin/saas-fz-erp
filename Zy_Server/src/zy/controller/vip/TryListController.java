package zy.controller.vip;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;

import zy.controller.BaseController;
import zy.entity.vip.trylist.T_Vip_TryList;
import zy.service.vip.trylist.TryListService;
import zy.util.StringUtil;

@Controller
@RequestMapping("api/vip/trylist")
public class TryListController extends BaseController{
	
	@Resource
	private TryListService tryListService;
	
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object list(@RequestBody Map<String, Object> params) {
		return ajaxSuccess(tryListService.list(params));
	}

	@RequestMapping(value = "save", method = RequestMethod.POST)
	@ResponseBody
	public Object save(@RequestBody Map<String, Object> params) {
		T_Vip_TryList tryList = JSON.parseObject(StringUtil.trimString(params.get("tryList")), T_Vip_TryList.class);
		tryListService.save(tryList);
		return ajaxSuccess();
	}
}
