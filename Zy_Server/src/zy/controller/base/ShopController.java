package zy.controller.base;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.base.CompanyShopService;
import zy.util.CommonUtil;
@Controller
@RequestMapping("shop")
public class ShopController extends BaseController{
	@Autowired
	private CompanyShopService companyShopService;
	@RequestMapping("to_list")
	public String to_list() {
		return "shop/list";
	}
	@RequestMapping("to_delay")
	public String to_delay() {
		return "shop/delay";
	}
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			HttpServletRequest request,HttpSession session) {
		String co_code = request.getParameter("co_code");
		String searchContent = request.getParameter("searchContent");
		T_Sys_User user = getUser(session);
		
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.KEY_USER, user);
        param.put("co_code", co_code);
        param.put("searchContent", searchContent);
		return ajaxSuccess(companyShopService.listShop(param));
	}
	
	
	@RequestMapping(value = "delayShop", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object delayShop(HttpServletRequest request){
		String sp_id = request.getParameter("sp_id");
		String months = request.getParameter("months");
		Map<String,Object> param = new HashMap<String, Object>();
		param.put("sp_id", sp_id);
		param.put("months", months);
		companyShopService.delayShop(param);
		return ajaxSuccess();
	}
}
