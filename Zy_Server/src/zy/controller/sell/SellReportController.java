package zy.controller.sell;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.service.sell.report.SellReportService;
import zy.util.StringUtil;

@Controller
@RequestMapping("api/sell/report")
public class SellReportController extends BaseController{
	@Resource
	private SellReportService sellReportService;
	
	@RequestMapping(value = "listShopByVip", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listShopByVip(@RequestBody Map<String, Object> params) {
		return ajaxSuccess(sellReportService.listShopByVip(params));
	}
	
	@RequestMapping(value = "listShopList", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listShopList(@RequestBody Map<String, Object> params) {
		String sh_number = StringUtil.trimString(params.get("sh_number"));
		Integer companyid = Integer.parseInt(StringUtil.trimString(params.get("companyid")));
		return ajaxSuccess(sellReportService.listShopList(sh_number, companyid));
	}
	
	@RequestMapping(value = "loadConsumeSnapshot", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object loadConsumeSnapshot(@RequestBody Map<String, Object> params) {
		String vm_code = StringUtil.trimString(params.get("vm_code"));
		Integer companyid = Integer.parseInt(StringUtil.trimString(params.get("companyid")));
		return ajaxSuccess(sellReportService.loadConsumeSnapshot(vm_code, companyid));
	}
	
}
