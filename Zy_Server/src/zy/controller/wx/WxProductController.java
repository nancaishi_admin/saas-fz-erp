package zy.controller.wx;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.wx.product.T_Wx_Product;
import zy.service.wx.product.WxProductService;
import zy.util.StringUtil;

@Controller
@RequestMapping("api/wx/product")
public class WxProductController extends BaseController{
	
	@Resource
	private WxProductService wxProductService;
	
	@RequestMapping(value = "hot_sell", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object hot_sell(@RequestBody Map<String, Object> params) {
		List<T_Wx_Product> list = wxProductService.hot_sell(params);
		for (T_Wx_Product item : list) {
			if(StringUtil.isNotEmpty(item.getPdm_img_path())){
				item.setPdm_img_path("http://ftp.zhjr100.com/"+item.getPdm_img_path());
			}
		}
		return ajaxSuccess(list);
	}
}
