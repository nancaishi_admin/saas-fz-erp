package zy.interceptor;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 */
public class CookieHandler {
	
	public static void createCookie(HttpServletResponse response,
			Map<String, Object> nameValues, int days) {
		Set<String> set = nameValues.keySet();
		Iterator<String> it = set.iterator();
		for (; it.hasNext();) {
			String name = (String) it.next();
			String value = nameValues.get(name).toString();
			Cookie cookie = new Cookie(name, value);
			cookie.setMaxAge(days * 24 * 60 * 60);
			cookie.setPath("/");
			response.addCookie(cookie);
		}
	}
	public static HashMap<String, String> getCookies(
		 HttpServletRequest request) {
		 Cookie[] cookies = request.getCookies();
		 HashMap<String, String> cookieHt = new HashMap<String, String>();
		 if (cookies != null && cookies.length > 0) {
			 for (int i = 0; i < cookies.length; i++) {
				 Cookie cookie = cookies[i];
				 cookieHt.put(cookie.getName(), cookie.getValue());
			 }
		 }
		 return cookieHt;
	 }
	 public static void setCookieValueByName(HttpServletRequest request,
		 String name, String value) {
		 Cookie[] cookies = request.getCookies();
		 if (cookies != null && cookies.length > 0) {
			 for (int i = 0; i < cookies.length; i++) {
				 if (name.equalsIgnoreCase(cookies[i].getName())) {
					 cookies[i].setValue(value);
				 }
			 }
		 }
	 }
	 public static String getCookieValueByName(HttpServletRequest request,
		 String name) {
		 Cookie[] cookies = request.getCookies();
		 String resValue = "";
		 if (cookies != null && cookies.length > 0) {
			 for (int i = 0; i < cookies.length; i++) {
				 if (name.equalsIgnoreCase(cookies[i].getName())) {
					 resValue = cookies[i].getValue();
				 }
			 }
		 }
		 return resValue;
	 }
	 public static void deletesCookie(HttpServletRequest request,
		 HttpServletResponse response) {
		 Cookie[] cookies = request.getCookies();
		 if (cookies != null) {
			 for (int i = 0; i < cookies.length; i++) {
				 Cookie cookie = cookies[i];
				 cookie.setMaxAge(0);
				 response.addCookie(cookie);
			 }
		 }
	 }
}
