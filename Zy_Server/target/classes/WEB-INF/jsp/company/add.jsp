<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
</script> 
</head>
<body>
<form method="post" action="" id="form1" name="form1">
	<table class="table_add">
		<tr>
			<td width="60px" align="right"><font color="red">*</font>编号：
			</td>
			<td width="180px" >
				<input class="main_Input w146" type="text" name="co_code" id="co_code" value="${co_code}" readonly/>
			</td>
			<td width="60px" align="right"><font color="red">*</font>名称：
			</td>
			<td width="180px" >
				<input class="main_Input w146" type="text" name="co_name" id="co_name" value="" maxlength="20"/>
			</td> 		 
		</tr>
	    <tr>
	    	<td align="right" width="60px" ><b style="color:red;">*</b>类型：</td>
			<td width="180px">
				<span class="ui-combo-wrap" id="spanType" ></span>
				<input type="hidden" name="co_type"  id="co_type" value=""/>
			</td> 
		  	<td align="right"><b style="color:red;">*</b>门店数：
		  	</td>
			<td>
		  	 	<input type="text" class="main_Input w146" name="co_shops" id="co_shops" value="" maxlength=4 onkeyup="javascript:vadiUtil.vadiNumber(this);" />
		  	</td>			
	     </tr>
		<tr>
			<td align="right" width="60px" ><b style="color:red;">*</b>版本：</td>
			<td width="180px">
				<span class="ui-combo-wrap" id="spanVer" ></span>
				<input type="hidden" name="co_ver"  id="co_ver" value=""/>
			</td> 
			<td align="right" >用户数： 
			</td>
			<td>
				<input type="text" class="main_Input w146" name="co_users"  id="co_users" value="8"/>
			</td>
		</tr>
		<tr>
			<td align="right" width="60px"><font color="red">*</font>服务商：
			</td>
			<td width="180px">
				<span class="ui-combo-wrap" id="spanUnit" ></span>
				<input type="hidden" name="co_unit"  id="co_unit" value=""/>
			</td>
			 <td align="right"><font color="red">*</font>推广人： 
		    </td>
			<td>
		    	<input class="main_Input w146" type="text" name="co_salesman" id="co_salesman" title="" maxlength="15" value=""/>
		    </td>
		</tr>
	    <tr>
		    <td align="right"><font color="red">*</font>电话： 
		    </td>
			<td>
		    	<input class="main_Input w146" type="text" name="co_tel" id="co_tel" title="" maxlength="15" value=""/>
		    </td>
			<td align="right"><font color="red">*</font>联系人：
			</td>
			<td>
				<input class="main_Input w146" type="text" name="co_man" id="co_man" maxlength="16" value=""/>
			</td>
		</tr>
		<tr>
		    <td align="right">QQ： 
		    </td>
			<td>
		    	<input class="main_Input w146" type="text" name="co_qq" id="co_qq" title="" maxlength="15" value=""/>
		    </td>
			<td align="right">微信：
			</td>
			<td>
				<input class="main_Input w146" type="text" name="co_wechat" id="co_wechat" maxlength="16" value=""/>
			</td>
		</tr>
		<tr>
		  	<td align="right">地区：
		  	</td>
		  	<td colspan="3">
  				<span class="ui-combo-wrap" id="spanProvince"></span>&nbsp;
				<input type="hidden" class="main_Input w146" name="co_province"  id="co_province" value=""/>
  				<span class="ui-combo-wrap" id="spanCity"></span>&nbsp;
				<input type="hidden" class="main_Input w146" name="co_city"  id="co_city" value=""/>
  				<span class="ui-combo-wrap" id="spanTown"></span>&nbsp;
				<input type="hidden" class="main_Input w146" name="co_town"  id="co_town" value=""/>
		  	</td>
		</tr>
		<tr>
			<td align="right">地址：
			</td>
			<td colspan="3">
				<input class="main_Input" type="text" name="co_addr" id="co_addr" style="width:395px"/>
			</td>
		</tr>
	</table>
</form>
<div class="footdiv">
	<input class="btn_confirm" type="button" id="btn-save" name="btn-save" value="确定"/>
	<input class="btn_close" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
<script src="<%=basePath%>data/company/company_add.js"></script>
</body>
</html>
