﻿(function($){
	$.fn.preview = function(){
		var xOffset = 10;
		var yOffset = 20;
		var w = $(window).width();
		$(this).each(function(){
			$(this).hover(function(e){
				var preDivtop = $(this).offset().top;
				var preDivleft = $(this).offset().left;
				
				if(/nophoto.png$/.test($(this).attr("src"))){
					
				}else if(/.png$|.gif$|.jpg$|.bmp$/.test($(this).attr("src"))){
					$("body").append("<div id='preview'><div><img src='"+$(this).attr('src')+"' width='260' /></div></div>");
				}else{
					$("body").append("<div id='preview'><div><p>"+$(this).attr('title')+"</p></div></div>");
				}
				
				$("#preview").css({
					position:"absolute",
					padding:"4px",
					border:"1px solid #f3f3f3",
					backgroundColor:"#eeeeee",
					top:preDivtop + "px",
					left:(preDivleft + 120) + "px",
					zIndex:1000
				});
				$("#preview > div").css({
					padding:"5px",
					backgroundColor:"white",
					border:"1px solid #cccccc"
				});
				$("#preview > div > p").css({
					textAlign:"center",
					fontSize:"12px",
					padding:"8px 0 3px",
					margin:"0"
				});
				
			},function(){
				$("#preview").remove();
			})				  
		});
	};
})(jQuery);