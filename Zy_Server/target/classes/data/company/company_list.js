var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var isRefresh = false;
var _limitMenu = system.MENULIMIT.BASE13;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'company/page';
var handle = {
	operate: function(oper, id){//修改、新增
		var height = 350;
		var width = 550;
		if(oper == 'add'){
			title = '新增商家';
			url = config.BASEPATH+"company/to_add";
			data = {oper: oper, callback: this.callback};
		}else if(oper == 'edit'){
			title = '修改用户';
			url = config.BASEPATH+"base/color/to_update?co_id="+id;
			data = {oper: oper,callback: this.callback};
		}else{
			return false;
		}
		$.dialog({
			title : title,
			content : 'url:'+url,
			data: data,
			width : width,
			height : height,
			max : false,
			min : false,
			cache : false,
			lock: true
		});
	},
	del: function(rowIds){//删除
		if(rowIds != null && rowIds != ''){
		 	$.dialog.confirm('删除后无法恢复，确定删除吗?', function(){
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'base/color/del',
					data:{"co_id":rowIds},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){		 		
							$('#grid').jqGrid('delRowData', rowIds);
						}else{
							Public.tips({type: 1, content : "删除失败"});
						}
					}
				});
			});
	   }else{
	      $.dialog.tips("请选择数据!!!",1,"32X32/fail.png");
	   }	
    },
	callback: function(data, oper, dialogWin){
		if(isRefresh){
			THISPAGE.reloadData();
		}
	},
	formatVer:function(val,opt, row){
		if(val == 1){
			return "总代版";
		}
		if(val == 2){
			return "连锁版";
		}
		if(val == 3){
			return "基础版";
		}
		if(val == 4){
			return "大众版";
		}
	},
	formatType:function(val,opt, row){
		if(val == 0){
			return "体验商户";
		}
		if(val == 1){
			return "正式商户";
		}
	},
	formatState:function(val,opt,row){
		if(row.co_state == 0){
			return "正常";
		}else{
			return "停用";	
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initGrid();
		this.initEvent();	
	},
	initParam:function(){
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
	    	{name: 'operate',label:'操作',width: 70, fixed:true, formatter: Public.operFmatter,align:'center', title: false,sortable:false},
	    	{name: '_co_state',label:'状态',index: '_co_state',width:80,formatter:handle.formatState, title: false},
	    	{name: 'co_state',label:'状态',index: 'co_state',hidden:true, title: false},
	    	{name: 'co_code',label:'编号',index: 'co_code',width:80, title: false},
	    	{name: 'co_name',label:'名称',index: 'co_name',width:120, title: false},
	    	{name: 'co_man',label:'联系人',index: 'co_man',width:80, title: false},
	    	{name: 'co_tel',label:'电话',index: 'co_tel',width:110, title: false},
	    	{name: 'co_shops',label:'门店数',index: 'co_shops',width:80, align:'center',title: false},
	    	{name: 'co_type',label:'类型',index: 'co_type',width:80, formatter:handle.formatType,title: false},
	    	{name: 'co_ver',label:'版本',index: 'co_ver',width:80,formatter:handle.formatVer, title: false}
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
			datatype: 'json',
			width: gridWH.w-12,
			height: gridWH.h-20,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			page:1,
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:30,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'co_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	reloadData:function(){
		var param = "?searchContent="+Public.encodeURI($("#SearchContent").val());
		$("#grid").jqGrid('setGridParam',{datatype:"json",url:queryurl+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		$('#SearchContent').on('keyup',function(){
			if (event.keyCode==13){
				$('#search').click();
			}
		});
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
		//新增
		$('#btn-add').on('click', function(e){
			e.preventDefault();
			handle.operate('add');
		});
		$('#btn-import').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.IMPORT)) {
				return ;
			};
			handle.operate('import');
		});
		//修改
		$('#grid').on('click', '.operating .ui-icon-pencil', function(e){
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.operate('edit', id);
		});
		//删除
		$('#grid').on('click', '.operating .ui-icon-trash', function(e){
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.del(id);
		});
	}
};
THISPAGE.init();