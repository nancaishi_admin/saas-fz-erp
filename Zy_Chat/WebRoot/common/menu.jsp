<%@ page contentType="text/html;charset=utf-8" language="java" %>
<div class="footernav border-t">
    <ul class="flex-wrap">
        <li class="flex-con">
            <a href="${base_path}to_home" id="home">
                <span><i class="iconfont icon-qiye"></i></span>
                <label>首页</label>
            </a>
        </li>
        <li class="flex-con">
            <a href="${base_path}to_product" id="product">
                <span><i class="iconfont icon-leibie1"></i></span>
                <label>商品</label>
            </a>
        </li>       
        <li class="flex-con">
            <a href="${base_path}to_cart" id="cart">
                <span data-tip="9"><i class="iconfont icon-cart"></i></span>
                <label>购物车</label>
            </a>            
        </li>
        <li class="flex-con">
            <a href="${base_path}to_msg" id="message">
                <span><i class="iconfont icon-gonggao"></i></span>
                <label>消息</label>
            </a>
        </li>
        <li class="flex-con">
            <a href="${base_path}to_my" id="my">
                <span><i class="iconfont icon-wode"></i></span>
                <label>我的</label>
            </a>            
        </li>         
    </ul>
</div>
<input type="hidden" id="class_name" name="class_name" value="${param.class}"/>
<script>
$(function(){
	var _class = $("#class_name").val();
	$(".flex-con").children("a").each(function(){
		$(this).removeClass("active");
		if(_class == $(this).prop("id")){
			$(this).addClass("active");	
		}
	});
});
</script>