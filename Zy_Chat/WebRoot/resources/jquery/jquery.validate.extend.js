//验证手机号码
jQuery.validator.addMethod("isMobile", function(value, element) {
	var length = value.length;
	return this.optional(element) || (length == 11 && /^1\d{10}$/.test(value));
}, "请填写正确的手机号码");
//验证电话号码
jQuery.validator.addMethod("isPhone", function(value, element) {
	var length = value.length;
	return this.optional(element) || (/^0\d{2,3}-?\d{7,8}$/.test(value));
}, "请填写正确的电话号码");