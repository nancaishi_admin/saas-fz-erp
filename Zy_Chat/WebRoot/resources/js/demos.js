$(function () {
  'use strict'; 
  //picker
  $(document).on("pageInit", "#page-picker", function(e, id, page) {
    $("#picker-name").picker({
      toolbarTemplate: '<header class="border-t">\
      <a class="close-picker">确定</a>\
      <h4 class="title">请选择收货时间</h4>\
      </header>',
      cols: [
        // {
        //   textAlign: 'center',
        //   values: ['今天', '明天', '04月26日 星期一', '04月27日 星期二', '04月28日 星期三', '04月29日 星期四', '04月30日 星期五', '05月01日 星期六']
        // },
        {
          textAlign: 'center',
          values: ['14:00 - 14:30', '14:30 - 15:00', '15:00 - 15:30', '15:30 - 16:00', '16:00 - 16:30']
        }
      ]
    });
  });

  $.init();
});
