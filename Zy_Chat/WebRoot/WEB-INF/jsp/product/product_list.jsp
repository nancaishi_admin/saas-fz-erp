<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
	<title>全部商品</title>
    <link rel="stylesheet" href="${base_path}resources/css/reset.css"/>
    <link rel="stylesheet" href="${base_path}resources/css/common.css"/>
    <link rel="stylesheet" href="${base_path}resources/css/style.css"/>
    <link rel="stylesheet" href="${base_path}resources/css/iconfont/iconfont.css"/>
    <script src="${base_path}resources/js/common.js"></script>
    <script src="${base_path}resources/js/zepto.min.js"></script>
    <script src="${base_path}resources/js/touch.js"></script>
    <script src="${base_path}resources/js/touchslide.1.1.js"></script>
    <script src="${base_path}resources/js/gototop.js"></script>
</head>
<body>
    <div class="main flex-wrap">
        <a class="circle box-s">
            <i class="iconfont icon-people"></i>
        </a>

        <div class="search-wrap" id="search">
            <div class="searchbar" >
                <i class="iconfont icon-sousuo"></i>
                <div class="search-text">输入商品货号、商品名称</div>
                <div class="search-input">
                    <form action="javascript:search();">
                    <input type="text" placeholder="输入商品货号、商品名称" id="search-input">
                    </form>
                </div>
                <i class="iconfont icon-guanbi"></i>
            </div>
            <div class="search-searbtn text-info">搜索</div>
        </div>
        <div class="filtrate tbox box-s">
            <div>
                <ul class="flex-wrap">
                    <li class="flex-con"><span>价格</span></li>
                    <li class="flex-con"><span>销量</span></li>
                    <li class="flex-con"><span>人气</span></li>
                    <li class="flex-con border-l border-r choose"><i class="iconfont icon-shop"></i><span>筛选</span></li>
                </ul>
            </div>
            <div class="cstyle"><i class="iconfont icon-shop"></i></div>
        </div>          
        <div id="actionSheet_wrap" class="filtrate-x">
            <div class="mask-transition" id="mask"></div>
            <div class="actionsheet" id="actionsheet">
                <ul class="filtrate-list">
                    <li class="tbox">
                        <div><label>属性：</label></div>
                        <div><a class="border on">全部</a><a class="border">新品</a><a class="border">折扣</a></div>
                    </li>
                    <li class="tbox">
                        <div><label>分类：</label></div>
                        <div><a class="border on">全部</a><a class="border">衬衫</a><a class="border">休闲服</a><a class="border">西服</a><a class="border">运动服</a><a class="border">家居服</a><a class="border">其他</a></div>
                    </li>
                    <li class="tbox">
                        <div><label>价格：</label></div>
                        <div><span class="border"><input type="tel"></span> <b>—</b> <span class="border"><input type="tel"></span></div>
                    </li>
                </ul>
                <ul class="filtrate-btn flex-wrap">
                    <li class="flex-con"><a class="btn" id="actionsheet-cancel">取消</a></li>
                    <li class="flex-con"><a class="btn ok-btn" id="surebtn">确定筛选</a></li>
                </ul>
            </div>
        </div>        
        <div class="filtrate-info box-s" style="display: none;">
            <span>属性：<b>新品</b></span>
            <span>类型：<b>休闲服</b></span>
            <span>价格：<b>500-1000</b></span>
        </div> 
        <div class="content flex-con">
            <!--
			<div class="nonetop">
                <i class="iconfont icon-cart"></i>
                <p>当前暂无商品</p>
            </div>
			-->
            <ul class="shoplist" style="margin-top: 0.08rem;">
                <li class="col-xs-12 oneline">
                    <a href="">
                        <div class="listimg"><img src="${base_path}resources/images/daogou.jpg"></div>
                        <div>
                            <h5 class="ellipsis-2">高端tpu底倒三角高帮男鞋厚底发型师明星同款大舌头牛皮情侣潮靴</h5>
                            <p><b>￥399</b>.00<span>￥669.00</span></p>
                        </div>
                        <i class="iconfont icon-cart"></i>
                    </a>
                </li>
                <li class="col-xs-12 oneline">
                    <a href="">
                        <div class="listimg"><img src="${base_path}resources/images/daogou.jpg"></div>
                        <div>
                            <h5 class="ellipsis-2">高端tpu底倒三角高帮男鞋厚底发型师明星同款大舌头牛皮情侣潮靴</h5>
                            <p><b>￥399</b>.00<span>￥669.00</span></p>
                        </div>
                        <i class="iconfont icon-cart"></i>
                    </a>
                </li>
                <li class="col-xs-12 oneline">
                    <a href="">
                        <div class="listimg"><img src="${base_path}resources/images/daogou.jpg"></div>
                        <div>
                            <h5 class="ellipsis-2">高端tpu底倒三角高帮男鞋厚底发型师明星同款大舌头牛皮情侣潮靴</h5>
                            <p><b>￥399</b>.00<span>￥669.00</span></p>
                        </div>
                        <i class="iconfont icon-cart"></i>
                    </a>
                </li>
                <li class="col-xs-12 oneline">
                    <a href="">
                        <div class="listimg"><img src="${base_path}resources/images/daogou.jpg"></div>
                        <div>
                            <h5 class="ellipsis-2">高端tpu底倒三角高帮男鞋厚底发型师明星同款大舌头牛皮情侣潮靴</h5>
                            <p><b>￥399</b>.00<span>￥669.00</span></p>
                        </div>
                        <i class="iconfont icon-cart"></i>
                    </a>
                </li>
                <li class="col-xs-12 oneline">
                    <a href="">
                        <div class="listimg"><img src="${base_path}resources/images/daogou.jpg"></div>
                        <div>
                            <h5 class="ellipsis-2">高端tpu底倒三角高帮男鞋厚底发型师明星同款大舌头牛皮情侣潮靴</h5>
                            <p><b>￥399</b>.00<span>￥669.00</span></p>
                        </div>
                        <i class="iconfont icon-cart"></i>
                    </a>
                </li>
                <li class="col-xs-12 oneline">
                    <a href="">
                        <div class="listimg"><img src="${base_path}resources/images/daogou.jpg"></div>
                        <div>
                            <h5 class="ellipsis-2">高端tpu底倒三角高帮男鞋厚底发型师明星同款大舌头牛皮情侣潮靴</h5>
                            <p><b>￥399</b>.00<span>￥669.00</span></p>
                        </div>
                        <i class="iconfont icon-cart"></i>
                    </a>
                </li>
                <div class="clearfix"></div>
            </ul>
        </div>
		<jsp:include page="/common/menu.jsp">
			<jsp:param value="product" name="class"/>
		</jsp:include>
    </div>

</body>
<script>
    $(function(){
        $('.searchbar').on('click', function(){
            $(this).parent().addClass("focusin");
            $('#search-input').focus();        
        });
        $('.icon-guanbi').on('click', function(){
            $('#search-input').val('');
        });
        $('.filtrate').on('click', '.choose',function () {
            $(this).addClass("on");
            var mask = $('#mask');
            var Actionsheet = $('#actionsheet');
            Actionsheet.addClass('actionsheet-toggle');
            mask.show().addClass('fade-toggle').one('click', function () {
                hideActionSheet(Actionsheet, mask);
                $(".choose").removeClass("on");
            });
            $('#actionsheet-cancel').one('click', function () {
                hideActionSheet(Actionsheet, mask);
                $(".choose").removeClass("on");
            });    
            $('#surebtn').one('click', function () {
                hideActionSheet(Actionsheet, mask);
                $(".filtrate-info").show();
                $(".choose").addClass("on");
            });        
            $('.actionsheet-menu').one('click', '.border-t', function () {
                hideActionSheet(Actionsheet, mask);
            });    
            Actionsheet.unbind('transitionend').unbind('webkitTransitionEnd');
            function hideActionSheet(Actionsheet, mask) {
                Actionsheet.removeClass('actionsheet-toggle');
                mask.removeClass('fade-toggle');
                Actionsheet.on('transitionend', function () {
                    mask.hide();
                }).on('webkitTransitionEnd', function () {
                    mask.hide();
                })
            }        
        });  
    })

    $(".content").swipeUp(function(){
        $(".circle").show();
        $(".circle").removeClass("out");
    })
    $(".content").swipeDown(function(){
        $(".circle").addClass("out");
    })
    $(".content").scroll(function(){
         if($(".content").scrollTop()<50){
            $(".circle").addClass("out");
          }
    });
    $(".circle").on('click', function () {  
        $(".content").scrollTo({toT: 0});  
    });
</script>
</html>