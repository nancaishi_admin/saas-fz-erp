<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
	<title>微商城</title>
    <link rel="stylesheet" href="${base_path}resources/css/reset.css"/>
    <link rel="stylesheet" href="${base_path}resources/css/common.css"/>
    <link rel="stylesheet" href="${base_path}resources/css/style.css"/>
    <link rel="stylesheet" href="${base_path}resources/css/iconfont/iconfont.css"/>
    <script src="${base_path}resources/js/common.js"></script>
    <script src="${base_path}resources/js/zepto.min.js"></script>
    <script src="${base_path}resources/js/touch.js"></script>
    <script src="${base_path}resources/js/touchslide.1.1.js"></script>
</head>
<body>
    <div class="main flex-wrap">
        <div class="search-wrap topsear" id="search">
            <div class="searchbar" >
                <i class="iconfont icon-sousuo"></i>
                <div class="search-text">输入商品货号、商品名称</div>
                <div class="search-input">
                    <form action="javascript:search();">
                    <input type="text" placeholder="输入商品货号、商品名称" id="search-input">
                    </form>
                </div>
                <i class="iconfont icon-guanbi"></i>
            </div>
            <div class="search-searbtn text-info">搜索</div>
        </div>

        <div class="content flex-con"> 
                
            <div id="focus" class="focus">
                <div class="hd">
                    <ul></ul>
                </div>
                <div class="bd">
                    <ul>
                            <li><a href="#"><img _src="${base_path}resources/images/1.jpg" src="${base_path}resources/images/blank.png"/></a></li>
                            <li><a href="#"><img _src="${base_path}resources/images/2.jpg" src="${base_path}resources/images/blank.png"/></a></li>
                    </ul>
                </div>
            </div>
            <script type="text/javascript">
                TouchSlide({ 
                    slideCell:"#focus",
                    titCell:".hd ul", //开启自动分页 autoPage:true ，此时设置 titCell 为导航元素包裹层
                    mainCell:".bd ul", 
                    effect:"leftLoop", 
                    interTime:5000,
                    autoPlay:true,//自动播放
                    autoPage:true, //自动分页
                    switchLoad:"_src" //切换加载，真实图片路径为"_src" 
                });
            </script>

            <ul class="navlist box-s text-center">
                <li class="col-xs-3"><span><i class="iconfont icon-cart"></i></span><p>开始购物</p></li>
                <li class="col-xs-3"><span data-tip="5"><i class="iconfont icon-daijinquan"></i></span><p>优惠券</p></li>
                <li class="col-xs-3"><span><i class="iconfont icon-mendian1"></i></span><p>线下店铺</p></li>
                <li class="col-xs-3"><span><i class="iconfont icon-shengriliwu"></i></span><p>积分礼品</p></li>
                <div class="clearfix"></div>
            </ul>            

            <ul class="imgnav">
                <li class="col-xs-6"><a class="box-s" href=""><img src="${base_path}resources/images/imgnav1.jpg"></a></li>
                <li class="col-xs-6">
                    <a href="" class="box-s"><img src="${base_path}resources/images/imgnav2.jpg"></a>
                </li>
                <li class="col-xs-6">
                    <a href="" class="box-s"><img src="${base_path}resources/images/imgnav3.jpg"></a>
                </li>
                <div class="clearfix"></div>                
            </ul>

            <div class="imgba mb-10">
                <a><img src="${base_path}resources/images/9.jpg"></a></a>
            </div>

            <div class="inventory mb-10">
                <div class="inventory-top mb-10">
                    <p class="border-b">我的专属购物单</p>
                    <div class="box-s inventory-nav flex-wrap">
                        <ul>
                            <li class="on">新鲜蔬菜</li>
                            <li>水产冻货</li>
                            <li>肉禽蛋类</li>
                            <li>米面粮油</li>
                            <li>干货调味</li>
                            <li>水产冻货</li>
                        </ul>
                        <p class="flex-con"><i class="iconfont icon-unfold"></i></p>
                    </div>
                </div>
                <ul class="shop-list box-s mb-10">
                    <li class="border-b">
                        <div class="fl"><img src="${base_path}resources/images/shop1.jpg"></div>
                        <div>
                            <h5 class="border-b">金针菇<p class="tbox fr"><span>更多规格</span><span><i class="iconfont icon-unfold"></i></span></p></h5>
                            <div class="tbox shop-info">
                                <div>
                                    <h6>¥21.60 /袋（3斤)</h6>
                                    <div class="money tbox">
                                        <div><label class="text-center">降</label></div>
                                        <div><b>￥7.20</b>/斤</div>
                                        <div><i class="often">经常买</i></div>
                                    </div>
                                    <p class="oldmoney">&nbsp;原价￥8.20/斤&nbsp;</p>
                                </div>
                                <div>
                                    <a class="btn ok-btn">+现在抢购</a>                       
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </li>
                    <li class="border-b">
                        <div class="fl"><img src="${base_path}resources/images/shop1.jpg"></div>
                        <div>
                            <h5 class="border-b">金针菇</h5>
                            <div class="tbox shop-info">
                                <div>
                                    <h6>¥21.60 /袋（3斤)</h6>
                                    <div class="money tbox">
                                        <div><label class="text-center">降</label></div>
                                        <div><b>￥7.20</b>/斤</div>
                                        <div><i class="low">最低价</i></div>
                                    </div>
                                    <p class="oldmoney">&nbsp;原价￥8.20/斤&nbsp;</p>
                                </div>
                                <div>                                   
                                    <a class="add border"><i class="iconfont icon-add"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </li>
                </ul>
                <ul class="shop-list box-s mb-10">
                    <li class="border-b">
                        <div class="fl"><img src="${base_path}resources/images/shop2.jpg"></div>
                        <div>
                            <h5 class="border-b">金针菇<p class="tbox fr"><span>更多规格</span><span><i class="iconfont icon-unfold"></i></span></p></h5>
                            <div class="tbox shop-info">
                                <div>
                                    <h6>¥21.60 /袋（3斤)</h6>
                                    <div class="money tbox">
                                        <div><label class="text-center">降</label></div>
                                        <div><b>￥7.20</b>/斤</div>
                                        <div><i class="low">最低价</i></div>
                                    </div>
                                    <p class="oldmoney">&nbsp;原价￥8.20/斤&nbsp;</p>
                                </div>
                                <div>                                   
                                    <span class="border tbox text-center">
                                        <a class="border-r"><i class="iconfont icon-move"></i></a>
                                        <div><input type="tel" value="1"></div>
                                        <a class="border-l"><i class="iconfont icon-add"></i></a>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </li>
                </ul>
            </div>
        </div>
        <jsp:include page="/common/menu.jsp">
			<jsp:param value="home" name="class"/>
		</jsp:include>
    </div>
</body>

<script>
    $(function(){
        $('.searchbar').on('click', function(){
            $(this).parent().addClass("focusin");
            $('#search-input').focus();        
        });
        $('.icon-guanbi').on('click', function(){
            $('#search-input').val('');
        });
    })
    $(".content").scroll(function(){
        if($(".content").scrollTop()>20&&$(".content").scrollTop()<150){
            $(".topsear").hide();
          }else{
            $(".topsear").show();
            $(".topsear").addClass("cbg");
          }
         if($(".content").scrollTop()<150){
            $(".topsear").removeClass("cbg");
          }
    });
    $('.inventory-nav').on('click', '.icon-unfold', function () {        
        $('.inventory-nav').addClass("down"); 
        $(this).removeClass("icon-unfold").addClass("icon-fold");        
        $('.inventory-nav').one('click', '.icon-fold', function () {      
            $('.inventory-nav').removeClass("down"); 
            $(this).removeClass("icon-fold").addClass("icon-unfold");        
        });      
    });
</script>
</html>
