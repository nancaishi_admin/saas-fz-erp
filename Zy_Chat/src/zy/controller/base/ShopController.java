package zy.controller.base;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.base.shop.T_Base_Shop_Line;
import zy.service.base.shop.ShopLineService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("api/base/shop")
public class ShopController extends BaseController{
	@Autowired
	private ShopLineService shopLineService;
	
	@RequestMapping(value = "listByMobile/{mobile}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object login(@PathVariable String mobile) {
		List<T_Base_Shop_Line> list = shopLineService.listByMobile(mobile);
		for (T_Base_Shop_Line item : list) {
			if(StringUtil.isNotEmpty(item.getSpl_logo())){
				item.setSpl_logo(CommonUtil.FTP_SERVER_PATH+item.getSpl_logo());
			}
		}
		return ajaxSuccess(list);
	}
	
}
