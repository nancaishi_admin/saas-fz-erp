package zy.controller.wx;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import zy.controller.BaseController;
import zy.entity.PageData;
import zy.entity.wx.my.T_Wx_Comment;
import zy.entity.wx.product.T_Wx_Product;
import zy.service.wx.product.WxCommentService;
import zy.service.wx.product.WxProductService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("api/wx/product")
public class WxProductController extends BaseController{
	@Autowired
	private WxProductService wxProductService;
	
	@Autowired
	private WxCommentService wxCommentService;
	
	@RequestMapping(value = "hot_sell", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object hot_sell(@RequestBody Map<String, Object> params) {
		List<T_Wx_Product> list = wxProductService.hot_sell(params);
		for (T_Wx_Product item : list) {
			if(StringUtil.isNotEmpty(item.getPdm_img_path())){
				item.setPdm_img_path(CommonUtil.FTP_SERVER_PATH+item.getPdm_img_path());
			}else {
				item.setPdm_img_path(CommonUtil.DEFAULT_PICTURE);
			}
		}
		return ajaxSuccess(list);
	}
	
	@RequestMapping(value = "listByType", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listByType(@RequestBody Map<String, Object> params) {
		List<T_Wx_Product> list = wxProductService.listByType(params);
		for (T_Wx_Product item : list) {
			if(StringUtil.isNotEmpty(item.getPdm_img_path())){
				item.setPdm_img_path(CommonUtil.FTP_SERVER_PATH+item.getPdm_img_path());
			}else {
				item.setPdm_img_path(CommonUtil.DEFAULT_PICTURE);
			}
		}
		return ajaxSuccess(list);
	}
	
	@RequestMapping(value = "load", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object load(@RequestBody Map<String, Object> params) {
		String shop_code = StringUtil.trimString(params.get("shop_code"));
		String pd_code = StringUtil.trimString(params.get("pd_code"));
		Integer companyid = Integer.parseInt(params.get("companyid").toString());
		T_Wx_Product product = wxProductService.load(pd_code,shop_code,companyid);
		if (product == null) {
			Map<String, Object> resultMap = new HashMap<String, Object>();
			resultMap.put("product", product);
			return ajaxSuccess(resultMap);
		}
		Map<String, Object> resultMap = wxProductService.loadStock_Wx(params);
		resultMap.put("product", product);
		return ajaxSuccess(resultMap);
	}
	
	@RequestMapping(value = "getComments", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object getComments(@RequestBody Map<String, Object> params) {
		Map<String, Object> data = new HashMap<String, Object>();
		PageData<T_Wx_Comment> pageData= wxCommentService.page(params);
		List<T_Wx_Comment> list = pageData.getList();
		List<Map<String, Object>> newlist = new ArrayList<Map<String, Object>>();
		SimpleDateFormat formatter = new SimpleDateFormat("MM月dd日 HH:mm:ss");

		for(T_Wx_Comment comment : list){
			Map<String, Object> comdic= new HashMap<String, Object>();
			comdic.put("content", comment.getContent());
			String dateString = formatter.format(comment.getCreatedate());
			comdic.put("createdate", dateString);
			comdic.put("nickname", comment.getNikname());
			comdic.put("avatarurl", comment.getAvatarurl());
			comdic.put("star", comment.getStar());
			
			List<String> pics = new ArrayList<String>();
			if (StringUtil.isNotEmpty(comment.getPic1())){
				pics.add(comment.getPic1());	
			}
			if (StringUtil.isNotEmpty(comment.getPic2())){
				pics.add(comment.getPic2());	
			}
			if (StringUtil.isNotEmpty(comment.getPic3())){
				pics.add(comment.getPic3());	
			}
			if (StringUtil.isNotEmpty(comment.getPic4())){
				pics.add(comment.getPic4());	
			}
			comdic.put("pics", pics);
			newlist.add(comdic);
			
		}
		data.put("data", newlist);
		data.put("totalCount", pageData.getPageInfo().getTotalRow());
		return ajaxSuccess(data);
	}
	
	/**
	 * 评价图片保存
	 * @param session
	 * @param response
	 */
	@RequestMapping(value = "saveCommentImg", method = { RequestMethod.GET,RequestMethod.POST })
	@ResponseBody
	public Object saveCommentImg(MultipartFile uploadify, String serverPath, HttpSession session) throws Exception{
		if (uploadify == null || uploadify.getSize() <= 0) {
			throw new IllegalArgumentException("请选择上传照片");
		}
		//获取图片大小
		long picSize = (uploadify.getSize()/1024) + 1;
		//图片不能大于1M
		if(picSize > 1024){
			throw new IllegalArgumentException("上传的图片不能大于1M!");
		}
		
		String realpath = CommonUtil.CHECK_BASE;
		String uploadFileName = uploadify.getOriginalFilename();
		String suffix = uploadFileName.substring(uploadFileName
				.lastIndexOf("."));
		String img_name = new Date().getTime() + "" + new Random().nextInt(9999) + suffix;
		String img_path = CommonUtil.WXCOMMENT_IMG_PATH + "/" + img_name;
		String all_path = serverPath + img_path;//页面展示路径

		File savefile = new File(realpath, img_path);
		if (!savefile.getParentFile().exists()) {
			savefile.getParentFile().mkdirs();
		}
		uploadify.transferTo(savefile);
		return ajaxSuccess(all_path);
	}
	
	/**
	 * 保存商品评价
	 * @param t_Wx_Comment
	 * @return
	 */
	@RequestMapping(value = "saveComment", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object saveComment(@RequestBody T_Wx_Comment t_Wx_Comment) {
		return ajaxSuccess(wxCommentService.saveComment(t_Wx_Comment));
	}
	
	/**
	 * 获取商品分类数据
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "getProductType", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object getProductType(@RequestBody Map<String, Object> params) {
		List<T_Wx_Product> list = wxProductService.hot_sell(params);
		for (T_Wx_Product item : list) {
			if(StringUtil.isNotEmpty(item.getPdm_img_path())){
				item.setPdm_img_path(CommonUtil.FTP_SERVER_PATH+item.getPdm_img_path());
			}else {
				item.setPdm_img_path(CommonUtil.DEFAULT_PICTURE);
			}
		}
		return ajaxSuccess(list);
	}
	
	/**
	 * 根据商品分类获取对应的商品数据
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "getProductsByType", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object getProductsByType(@RequestBody Map<String, Object> params) {
		List<T_Wx_Product> list = wxProductService.hot_sell(params);
		for (T_Wx_Product item : list) {
			if(StringUtil.isNotEmpty(item.getPdm_img_path())){
				item.setPdm_img_path(CommonUtil.FTP_SERVER_PATH+item.getPdm_img_path());
			}else {
				item.setPdm_img_path(CommonUtil.DEFAULT_PICTURE);
			}
		}
		return ajaxSuccess(list);
	}
}
