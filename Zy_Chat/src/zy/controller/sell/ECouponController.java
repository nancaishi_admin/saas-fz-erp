package zy.controller.sell;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.service.sell.ecoupon.ECouponService;

@Controller
@RequestMapping("api/sell/ecoupon")
public class ECouponController extends BaseController{

	@Autowired
	private ECouponService eCouponService;
	
	@RequestMapping(value = "listEcoupon", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listEcoupon(@RequestBody Map<String, Object> params) {
		return ajaxSuccess(eCouponService.listEcoupon4Push(params));
	}
	
	@RequestMapping(value = "receiveEcoupon", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object receiveEcoupon(@RequestBody Map<String, Object> params) {
		return ajaxSuccess(eCouponService.listEcoupon4Push(params));
	}
	/**
	 * 查询可领的优惠券数据
	 * @param params 参数集
	 * @return json 
	 * */
	@RequestMapping(value = "shopCoupons", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object shopCoupons(@RequestBody Map<String, Object> params) {
		
		return ajaxSuccess(eCouponService.listEcoupon4Push(params));
	}
}
