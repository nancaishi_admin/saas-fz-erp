package zy.controller.sell;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;

import zy.controller.BaseController;
import zy.entity.vip.member.T_Vip_Member;
import zy.entity.wx.cart.T_Wx_Cartlist;
import zy.service.cart.WxCartService;
@Controller
@RequestMapping("api/wx/cart")
public class CartController extends BaseController{
	@Autowired
	private WxCartService cartService;
	
	
	@RequestMapping(value = "getMemberAccountInfo", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object getMemberAccountInfo(@RequestBody Map<String, Object> params) {
		T_Vip_Member list = cartService.getMemberAccountInfo(params);
		return ajaxSuccess(list);
	}
	@RequestMapping(value = "getCartInfo", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object getCartInfo(@RequestBody Map<String, Object> params) {
		List<T_Wx_Cartlist> list = cartService.listCart(params);
		return ajaxSuccess(list);
	}
	@RequestMapping(value = "modifyCartNum", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object modifyCartNum(@RequestBody Map<String, Object> params) {
		int flag = cartService.modifyCartNum(params);
		return result(flag);
	}
	@RequestMapping(value = "deleteCart", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object deleteCart(@RequestBody String data,HttpServletRequest request) {
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("ids", JSON.parseObject(data).getJSONArray("ids"));
		int flag = cartService.deleteCart(params);
		return result(flag);
	}
	@RequestMapping(value = "addCart", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object addCart(@RequestBody Map<String, Object> params) {
		int flag = cartService.addCart(params);
		return result(flag);
	}
	
}
