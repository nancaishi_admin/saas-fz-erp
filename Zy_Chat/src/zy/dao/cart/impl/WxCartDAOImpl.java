package zy.dao.cart.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.cart.WxCartDAO;
import zy.entity.wx.cart.T_Wx_Cartlist;

import com.alibaba.fastjson.JSONArray;
@Repository
public class WxCartDAOImpl extends BaseDaoImpl implements WxCartDAO {

	@Override
	public List<T_Wx_Cartlist> listCart(Map<String, Object> paramMap) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT t.id,sysdate,t.pd_code,t.pd_name,");
		sql.append(" cr_name,sz_name,br_name,sub_name,pd_pic,");
//		sql.append(" CONCAT(cr_name,sz_name,br_name) skuName,");
		sql.append(" (SELECT pdm_img_path FROM t_base_product_img c where c.pdm_pd_code=t.pd_code AND c.companyid=t.companyid LIMIT 1) pd_pic,");
		sql.append(" sub_code,cr_code,sz_code,br_code,amount,price,t.companyid,wu_code");
		sql.append(" FROM t_wx_cartlist t");
		sql.append(" JOIN t_base_product p");
		sql.append(" ON p.pd_code=t.pd_code");
		sql.append(" AND p.companyid=t.companyid");
		sql.append(" where 1=1");
		sql.append(" AND wu_code=:wu_code");
		sql.append(" AND t.companyid=:companyid");
		return namedParameterJdbcTemplate.query(sql.toString(),paramMap,
				new BeanPropertyRowMapper<>(T_Wx_Cartlist.class));
	}

	@Override
	public void modifyCartNum(Map<String, Object> paramMap) {
		StringBuffer sql = new StringBuffer();
		sql.append(" UPDATE t_wx_cartlist");
		sql.append(" SET amount=:amount");
		sql.append(" WHERE id=:id");
		namedParameterJdbcTemplate.update(sql.toString(), paramMap);
	}

	@Override
	public void deleteCart(List<T_Wx_Cartlist> list) {
		StringBuffer sql = new StringBuffer();
		sql.append(" DELETE FROM t_wx_cartlist");
		sql.append(" WHERE id=:id");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(list.toArray()));
	}

	@Override
	public void addCart(Map<String, Object> paramMap) {
		StringBuffer sql = new StringBuffer();
		sql.append(" INSERT INTO t_wx_cartlist(");
		sql.append(" sysdate,pd_code,pd_name,pd_pic,cr_name,sz_name,br_name,sub_name,");
		sql.append(" sub_code,cr_code,sz_code,br_code,amount,price,sell_price,companyid,wu_code");
		sql.append(" )VALUES( ");
		sql.append(" :sysdate,:pd_code,:pd_name,:pd_pic,:cr_name,:sz_name,:br_name,:sub_name,");
		sql.append(" :sub_code,:cr_code,:sz_code,:br_code,:amount,:price,:sell_price,:companyid,:wu_code");
		sql.append(")");
		namedParameterJdbcTemplate.update(sql.toString(), paramMap);
	}

}
