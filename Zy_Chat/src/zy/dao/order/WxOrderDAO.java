package zy.dao.order;

import java.util.List;
import java.util.Map;

import zy.entity.wx.order.T_Wx_Order;
import zy.entity.wx.order.T_Wx_Order_Detail;

public interface WxOrderDAO {
	List<T_Wx_Order> listOrder(Map<String,Object> paramMap);
	T_Wx_Order getOrder(Map<String,Object> paramMap);
	
	int ModifyOrder(Map<String, Object> paramMap);
	List<T_Wx_Order_Detail> getOrderDetail(Map<String,Object> paramMap);
	int confirmOrder(Map<String,Object> paramMap);
	int cancleOrder(Map<String,Object> paramMap);
	/**
	 * 保存订单数据
	 * 1.构造明细数据从购物车中取数据，构造订单明细数据
	 * 2.构造订单编号
	 * 3.验证库存
	 * 4.保存订单主表
	 * 5.保存订单明细表
	 * 6.返回状态值
	 * @param paramMap
	 * */
	Map<String, Object> saveOrder(Map<String,Object> paramMap);
}
