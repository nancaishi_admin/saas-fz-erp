package zy.service.order.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.order.WxOrderDAO;
import zy.entity.wx.order.T_Wx_Order;
import zy.entity.wx.order.T_Wx_Order_Detail;
import zy.service.order.WxOrderService;
import zy.util.CommonUtil;
import zy.util.NumberUtil;
import zy.util.StringUtil;

@Service
public class WxOrderServiceImpl implements WxOrderService{
	@Autowired
	private WxOrderDAO wxOrderDAO;
	
	private void vadiParam(Map<String,Object> paramMap){
		Object companyId = paramMap.get(CommonUtil.COMPANYID);
		Object wu_code = paramMap.get("wu_code");
		if(StringUtil.isEmpty(companyId)){
			throw new IllegalArgumentException("商家ID参数不有为空!");
		}
		if(StringUtil.isEmpty(wu_code)){
			throw new IllegalArgumentException("用户编号不能为空!");
		}
	}
	
	@Override
	public Object listOrder(Map<String, Object> paramMap) {
		vadiParam(paramMap);
		Integer pageSize = (Integer)paramMap.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)paramMap.get(CommonUtil.PAGEINDEX);
		paramMap.put(CommonUtil.START, (pageIndex-1)*pageSize);
		paramMap.put(CommonUtil.END, pageSize);
		List<Map<String, Object>> orderDataList = new ArrayList<Map<String,Object>>();
		 List<T_Wx_Order> list = wxOrderDAO.listOrder(paramMap);
		 for(T_Wx_Order order : list){
			 
			 Map<String, Object> ordermap = new HashMap<String,Object>();
			 Map<String, Object> detailparamMap =  new HashMap<String,Object>();
			 detailparamMap.put("number", order.getNumber());
			 detailparamMap.put("companyid", order.getCompanyid());
			 detailparamMap.put("wu_code", order.getWu_code());
			 List<T_Wx_Order_Detail> details = getOrderDetail(detailparamMap);
			 List<String> piclist = new ArrayList<String>();
			 for(T_Wx_Order_Detail detail : details){
				 detail.setPd_pic(CommonUtil.FTP_SERVER_PATH+detail.getPd_pic());				
				 piclist.add(detail.getPd_pic());

			 }
			 Map<String, Object> orderdic = new HashMap<String,Object>();
			 orderdic.put("number", order.getNumber());
			 orderdic.put("remark", order.getRemark());
			 orderdic.put("sysdate", order.getSysdate().toString());
			 orderdic.put("money", order.getMoney());
			 orderdic.put("wx_money", order.getWx_money());
			 orderdic.put("sell_money", order.getSell_money());
			 orderdic.put("remark", order.getRemark());
			 orderdic.put("order_status", order.getOrder_status());
			 orderdic.put("pay_status", order.getPay_status());
			 orderdic.put("ec_money", order.getEc_money());
			 orderdic.put("acc_money", order.getAcc_money());
			 orderdic.put("address", order.getAddress());
			 orderdic.put("linkman", order.getLinkman());
			 orderdic.put("mobile", order.getMobile());
			 
			 ordermap.put("order", orderdic);
			 ordermap.put("pics", piclist);
			 ordermap.put("details", details);
			 orderDataList.add(ordermap);
		 }
		 
		 return orderDataList;
	}

	@Override
	public T_Wx_Order getOrder(Map<String, Object> paramMap) {
		Object number = paramMap.get("number");
		if(StringUtil.isEmpty(number)){
			throw new IllegalArgumentException("订单编号不能为空!");
		}
		return wxOrderDAO.getOrder(paramMap);
	}
	
	@Override
	public int ModifyOrder(Map<String, Object> paramMap) {
	
		return wxOrderDAO.ModifyOrder(paramMap);
	}
	
	@Override
	public List<T_Wx_Order_Detail> getOrderDetail(Map<String, Object> paramMap) {
		vadiParam(paramMap);
		Object number = paramMap.get("number");
		if(StringUtil.isEmpty(number)){
			throw new IllegalArgumentException("订单编号不有为空!");
		}
		return wxOrderDAO.getOrderDetail(paramMap);
	}

	@Override
	public int confirmOrder(Map<String, Object> paramMap) {
		return wxOrderDAO.confirmOrder(paramMap);
	}

	@Override
	public int cancleOrder(Map<String, Object> paramMap) {
		return wxOrderDAO.cancleOrder(paramMap);
	}
	@Transactional
	@Override
	public Object saveOrder(Map<String, Object> paramMap) {
		vadiParam(paramMap);
		String ids = (String)paramMap.get("cartids");
		if(null != ids && ids.length() > 0){
			List<Integer> idList = new ArrayList<>();
			for(String id:ids.split(",")){
				idList.add(NumberUtil.toInteger(id));
			}
			paramMap.put("ids", idList);
		}else{
			throw new IllegalArgumentException("购物车无数据");
		}
		return wxOrderDAO.saveOrder(paramMap);
	}
	public static void main(String[] args) {
	}
}
