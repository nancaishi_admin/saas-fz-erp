package zy.service.cart.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONArray;

import zy.dao.cart.WxCartDAO;
import zy.entity.vip.member.T_Vip_Member;
import zy.entity.wx.cart.T_Wx_Cartlist;
import zy.service.cart.WxCartService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.NumberUtil;
@Service
public class WxCartServiceImpl implements WxCartService{
	@Autowired
	private WxCartDAO wxCartDAO;
	@Override
	public List<T_Wx_Cartlist> listCart(Map<String, Object> paramMap) {
		List<T_Wx_Cartlist> list = wxCartDAO.listCart(paramMap);
		return list;
	}
	@Transactional
	@Override
	public int modifyCartNum(Map<String, Object> paramMap) {
		try {
			wxCartDAO.modifyCartNum(paramMap);
			return CommonUtil.SUCCESS;
		} catch (Exception e) {
			return CommonUtil.FAIL;
		}
	}
	@Transactional
	@Override
	public int deleteCart(Map<String, Object> paramMap) {
			try {
				JSONArray ids = (JSONArray)paramMap.get("ids");
				if(null != ids && ids.size() > 0){
					List<T_Wx_Cartlist> list = new ArrayList<T_Wx_Cartlist>(ids.size());
					for(int i = 0;i<ids.size();i++){
						T_Wx_Cartlist item = new T_Wx_Cartlist();
						item.setId(ids.getIntValue(i));
						list.add(item);
					}
					wxCartDAO.deleteCart(list);
					return CommonUtil.SUCCESS;
				}else{
					return CommonUtil.FAIL;
				}
			} catch (Exception e) {
				e.printStackTrace();
				return CommonUtil.FAIL;
			}
	}
	@Transactional
	@Override
	public int addCart(Map<String, Object> paramMap) {
		try {
			Integer id = 0,amount = 0;
			List<T_Wx_Cartlist> list = wxCartDAO.listCart(paramMap);
			if(null != list && list.size() > 0){
				for(T_Wx_Cartlist item:list){
					if(item.getSub_code().equals(""+paramMap.get("sub_code"))){
						id = item.getId();
						amount = item.getAmount() + NumberUtil.toInteger(paramMap.get("amount"));
						break;
					}
				}
			}
			if(id > 0){
				paramMap.clear();
				paramMap.put("id", id);
				paramMap.put("amount", amount);
				wxCartDAO.modifyCartNum(paramMap);
			}else{
				paramMap.put("sysdate", DateUtil.getCurrentTime());
				wxCartDAO.addCart(paramMap);
			}
			return CommonUtil.SUCCESS;
		} catch (Exception e) {
			return CommonUtil.FAIL;
		}
	}
	@Override
	public T_Vip_Member getMemberAccountInfo(Map<String, Object> paramMap) {
		return null;
	}
	
	
}
