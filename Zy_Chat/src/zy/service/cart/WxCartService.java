package zy.service.cart;

import java.util.List;
import java.util.Map;

import zy.entity.vip.member.T_Vip_Member;
import zy.entity.wx.cart.T_Wx_Cartlist;

public interface WxCartService {
	List<T_Wx_Cartlist> listCart(Map<String,Object> paramMap);
	int modifyCartNum(Map<String,Object> paramMap);
	int deleteCart(Map<String,Object> paramMap);
	int addCart(Map<String,Object> paramMap);
	T_Vip_Member getMemberAccountInfo(Map<String,Object> paramMap);
}
