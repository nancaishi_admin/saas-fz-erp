package zy.dto.batch.fee;

import java.io.Serializable;

public class BatchFeeReportDto implements Serializable{
	private static final long serialVersionUID = -8526572349564443203L;
	private String code;
	private String name;
	private Double money;
	private String date;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getMoney() {
		return money;
	}
	public void setMoney(Double money) {
		this.money = money;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
}
