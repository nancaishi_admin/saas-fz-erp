package zy.dto.batch.sell;

import java.util.HashMap;
import java.util.Map;

import zy.entity.batch.sell.T_Batch_SellList;

public class SellDetailSizeReportDto extends T_Batch_SellList{
	private static final long serialVersionUID = -9073587164802863213L;
	private String size_amount;
	private Integer totalamount;
	private Double unit_money;
	private Double sell_money;
	private Double cost_money;
	private Map<String, Integer> amountMap = new HashMap<String, Integer>();
	public String getSize_amount() {
		return size_amount;
	}
	public void setSize_amount(String size_amount) {
		this.size_amount = size_amount;
	}
	public Integer getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(Integer totalamount) {
		this.totalamount = totalamount;
	}
	public Map<String, Integer> getAmountMap() {
		return amountMap;
	}
	public void setAmountMap(Map<String, Integer> amountMap) {
		this.amountMap = amountMap;
	}
	public Double getUnit_money() {
		return unit_money;
	}
	public void setUnit_money(Double unit_money) {
		this.unit_money = unit_money;
	}
	public Double getSell_money() {
		return sell_money;
	}
	public void setSell_money(Double sell_money) {
		this.sell_money = sell_money;
	}
	public Double getCost_money() {
		return cost_money;
	}
	public void setCost_money(Double cost_money) {
		this.cost_money = cost_money;
	}
}
