package zy.dto.batch.sell;

import zy.entity.batch.sell.T_Batch_SellList;

public class SellDetailReportDto extends T_Batch_SellList{
	private static final long serialVersionUID = -3822489311538222327L;
	private String se_make_date;
	private String se_ar_state;
	private String se_manager;
	private String client_name;
	private String depot_name;
	public String getSe_make_date() {
		return se_make_date;
	}
	public void setSe_make_date(String se_make_date) {
		this.se_make_date = se_make_date;
	}
	public String getSe_ar_state() {
		return se_ar_state;
	}
	public void setSe_ar_state(String se_ar_state) {
		this.se_ar_state = se_ar_state;
	}
	public String getSe_manager() {
		return se_manager;
	}
	public void setSe_manager(String se_manager) {
		this.se_manager = se_manager;
	}
	public String getClient_name() {
		return client_name;
	}
	public void setClient_name(String client_name) {
		this.client_name = client_name;
	}
	public String getDepot_name() {
		return depot_name;
	}
	public void setDepot_name(String depot_name) {
		this.depot_name = depot_name;
	}
}
