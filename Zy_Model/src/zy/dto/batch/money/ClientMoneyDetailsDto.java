package zy.dto.batch.money;

import java.io.Serializable;

public class ClientMoneyDetailsDto implements Serializable{
	private static final long serialVersionUID = 5998858141660510906L;
	private Integer id;
	private String make_date;
	private String number;
	private Integer type;
	private String client_code;
	private String client_name;
	private Integer pay_state;
	private Double money;
	private Double discount_money;
	private Double prepay;
	private Double received;
	private Double receivable;
	private String manager;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMake_date() {
		return make_date;
	}
	public void setMake_date(String make_date) {
		this.make_date = make_date;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getClient_code() {
		return client_code;
	}
	public void setClient_code(String client_code) {
		this.client_code = client_code;
	}
	public String getClient_name() {
		return client_name;
	}
	public void setClient_name(String client_name) {
		this.client_name = client_name;
	}
	public Integer getPay_state() {
		return pay_state;
	}
	public void setPay_state(Integer pay_state) {
		this.pay_state = pay_state;
	}
	public Double getMoney() {
		return money;
	}
	public void setMoney(Double money) {
		this.money = money;
	}
	public Double getDiscount_money() {
		return discount_money;
	}
	public void setDiscount_money(Double discount_money) {
		this.discount_money = discount_money;
	}
	public Double getPrepay() {
		return prepay;
	}
	public void setPrepay(Double prepay) {
		this.prepay = prepay;
	}
	public Double getReceived() {
		return received;
	}
	public void setReceived(Double received) {
		this.received = received;
	}
	public Double getReceivable() {
		return receivable;
	}
	public void setReceivable(Double receivable) {
		this.receivable = receivable;
	}
	public String getManager() {
		return manager;
	}
	public void setManager(String manager) {
		this.manager = manager;
	}
}
