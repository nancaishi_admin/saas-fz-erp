package zy.dto.common;

import zy.entity.base.product.T_Base_Barcode;

public class BarcodeImportDto extends T_Base_Barcode{
	private static final long serialVersionUID = 5451021970604425576L;
	private String pd_szg_code;
	private Double unit_price;
	private Double sell_price;
	private Double cost_price;
	private Double sort_price;
	private Double vip_price;
	public String getPd_szg_code() {
		return pd_szg_code;
	}
	public void setPd_szg_code(String pd_szg_code) {
		this.pd_szg_code = pd_szg_code;
	}
	public Double getUnit_price() {
		return unit_price;
	}
	public void setUnit_price(Double unit_price) {
		this.unit_price = unit_price;
	}
	public Double getSell_price() {
		return sell_price;
	}
	public void setSell_price(Double sell_price) {
		this.sell_price = sell_price;
	}
	public Double getCost_price() {
		return cost_price;
	}
	public void setCost_price(Double cost_price) {
		this.cost_price = cost_price;
	}
	public Double getSort_price() {
		return sort_price;
	}
	public void setSort_price(Double sort_price) {
		this.sort_price = sort_price;
	}
	public Double getVip_price() {
		return vip_price;
	}
	public void setVip_price(Double vip_price) {
		this.vip_price = vip_price;
	}
}
