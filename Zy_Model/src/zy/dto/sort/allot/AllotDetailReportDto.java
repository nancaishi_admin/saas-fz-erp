package zy.dto.sort.allot;

import zy.entity.sort.allot.T_Sort_AllotList;

public class AllotDetailReportDto extends T_Sort_AllotList{
	private static final long serialVersionUID = 5702248866953060641L;
	private String at_date;
	private String at_ar_state;
	private String at_manager;
	private String shop_name;
	private String outdp_name;
	private String indp_name;
	public String getAt_date() {
		return at_date;
	}
	public void setAt_date(String at_date) {
		this.at_date = at_date;
	}
	public String getAt_ar_state() {
		return at_ar_state;
	}
	public void setAt_ar_state(String at_ar_state) {
		this.at_ar_state = at_ar_state;
	}
	public String getAt_manager() {
		return at_manager;
	}
	public void setAt_manager(String at_manager) {
		this.at_manager = at_manager;
	}
	public String getShop_name() {
		return shop_name;
	}
	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}
	public String getOutdp_name() {
		return outdp_name;
	}
	public void setOutdp_name(String outdp_name) {
		this.outdp_name = outdp_name;
	}
	public String getIndp_name() {
		return indp_name;
	}
	public void setIndp_name(String indp_name) {
		this.indp_name = indp_name;
	}
}
