package zy.dto.shop.want;

public class ProductStorePriceDto {
	private Integer pdp_id;
	private Double pdp_cost_price;
	private Double pdp_vip_price;
	private Double pdp_sell_price;
	private Double pdp_sort_price;
	private String pd_code;
	private Double pd_sell_price;
	private Double pd_vip_price;
	private Double pd_sort_price;
	private Double pd_cost_price;
	private Double unitprice;
	private Integer amount;
	public Integer getPdp_id() {
		return pdp_id;
	}
	public void setPdp_id(Integer pdp_id) {
		this.pdp_id = pdp_id;
	}
	public Double getPdp_cost_price() {
		return pdp_cost_price;
	}
	public void setPdp_cost_price(Double pdp_cost_price) {
		this.pdp_cost_price = pdp_cost_price;
	}
	public Double getPdp_vip_price() {
		return pdp_vip_price;
	}
	public void setPdp_vip_price(Double pdp_vip_price) {
		this.pdp_vip_price = pdp_vip_price;
	}
	public Double getPdp_sell_price() {
		return pdp_sell_price;
	}
	public void setPdp_sell_price(Double pdp_sell_price) {
		this.pdp_sell_price = pdp_sell_price;
	}
	public Double getPdp_sort_price() {
		return pdp_sort_price;
	}
	public void setPdp_sort_price(Double pdp_sort_price) {
		this.pdp_sort_price = pdp_sort_price;
	}
	public String getPd_code() {
		return pd_code;
	}
	public void setPd_code(String pd_code) {
		this.pd_code = pd_code;
	}
	public Double getPd_sell_price() {
		return pd_sell_price;
	}
	public void setPd_sell_price(Double pd_sell_price) {
		this.pd_sell_price = pd_sell_price;
	}
	public Double getPd_vip_price() {
		return pd_vip_price;
	}
	public void setPd_vip_price(Double pd_vip_price) {
		this.pd_vip_price = pd_vip_price;
	}
	public Double getPd_sort_price() {
		return pd_sort_price;
	}
	public void setPd_sort_price(Double pd_sort_price) {
		this.pd_sort_price = pd_sort_price;
	}
	public Double getPd_cost_price() {
		return pd_cost_price;
	}
	public void setPd_cost_price(Double pd_cost_price) {
		this.pd_cost_price = pd_cost_price;
	}
	public Double getUnitprice() {
		return unitprice;
	}
	public void setUnitprice(Double unitprice) {
		this.unitprice = unitprice;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
}
