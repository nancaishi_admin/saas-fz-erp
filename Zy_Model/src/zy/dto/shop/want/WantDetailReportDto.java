package zy.dto.shop.want;

import zy.entity.shop.want.T_Shop_WantList;

public class WantDetailReportDto extends T_Shop_WantList{
	private static final long serialVersionUID = 8937163731079335279L;
	private String wt_date;
	private String wt_ar_state;
	private String wt_manager;
	private String shop_name;
	private String outdp_name;
	private String indp_name;
	public String getWt_date() {
		return wt_date;
	}
	public void setWt_date(String wt_date) {
		this.wt_date = wt_date;
	}
	public String getWt_ar_state() {
		return wt_ar_state;
	}
	public void setWt_ar_state(String wt_ar_state) {
		this.wt_ar_state = wt_ar_state;
	}
	public String getWt_manager() {
		return wt_manager;
	}
	public void setWt_manager(String wt_manager) {
		this.wt_manager = wt_manager;
	}
	public String getShop_name() {
		return shop_name;
	}
	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}
	public String getOutdp_name() {
		return outdp_name;
	}
	public void setOutdp_name(String outdp_name) {
		this.outdp_name = outdp_name;
	}
	public String getIndp_name() {
		return indp_name;
	}
	public void setIndp_name(String indp_name) {
		this.indp_name = indp_name;
	}
}
