package zy.dto.money.expense;

import java.io.Serializable;

public class ExpenseReportMonthDto implements Serializable{
	private static final long serialVersionUID = 6297544440204146775L;
	private String id;
	private String month;
	private double business_income;//业务收入
	private double business_cost;//业务成本
	private double gross_profit;//毛利润
	private double expense;//费用
	private double net_profit;//净利润
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public double getBusiness_income() {
		return business_income;
	}
	public void setBusiness_income(double business_income) {
		this.business_income = business_income;
	}
	public double getBusiness_cost() {
		return business_cost;
	}
	public void setBusiness_cost(double business_cost) {
		this.business_cost = business_cost;
	}
	public double getGross_profit() {
		return gross_profit;
	}
	public void setGross_profit(double gross_profit) {
		this.gross_profit = gross_profit;
	}
	public double getExpense() {
		return expense;
	}
	public void setExpense(double expense) {
		this.expense = expense;
	}
	public double getNet_profit() {
		return net_profit;
	}
	public void setNet_profit(double net_profit) {
		this.net_profit = net_profit;
	}
}
