package zy.dto.money.expense;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class ExpenseReportDepartmentDto implements Serializable{
	private static final long serialVersionUID = 5464279935805416896L;
	private String mp_code;
	private String mp_name;
	private String depart_money;
	private Map<String, Double> moneyMap = new HashMap<String, Double>();
	private Double total;
	public String getMp_code() {
		return mp_code;
	}
	public void setMp_code(String mp_code) {
		this.mp_code = mp_code;
	}
	public String getMp_name() {
		return mp_name;
	}
	public void setMp_name(String mp_name) {
		this.mp_name = mp_name;
	}
	public String getDepart_money() {
		return depart_money;
	}
	public void setDepart_money(String depart_money) {
		this.depart_money = depart_money;
	}
	public Map<String, Double> getMoneyMap() {
		return moneyMap;
	}
	public void setMoneyMap(Map<String, Double> moneyMap) {
		this.moneyMap = moneyMap;
	}
	public Double getTotal() {
		if (moneyMap != null) {
			double totalMoney = 0d;
			for (String key : moneyMap.keySet()) {
				totalMoney += moneyMap.get(key);
			}
			return totalMoney;
		}
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
}
