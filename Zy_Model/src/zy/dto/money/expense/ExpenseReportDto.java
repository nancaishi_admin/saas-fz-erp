package zy.dto.money.expense;

import java.io.Serializable;

public class ExpenseReportDto implements Serializable{
	private static final long serialVersionUID = 3995092957033301082L;
	private String code;
	private String name;
	private Double money;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getMoney() {
		return money;
	}
	public void setMoney(Double money) {
		this.money = money;
	}
}
