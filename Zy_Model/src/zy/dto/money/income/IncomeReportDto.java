package zy.dto.money.income;

import java.io.Serializable;

public class IncomeReportDto implements Serializable{
	private static final long serialVersionUID = -7954965142878491992L;
	private String code;
	private String name;
	private Double money;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getMoney() {
		return money;
	}
	public void setMoney(Double money) {
		this.money = money;
	}
}
