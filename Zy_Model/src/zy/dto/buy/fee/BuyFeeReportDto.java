package zy.dto.buy.fee;

import java.io.Serializable;

public class BuyFeeReportDto implements Serializable{
	private static final long serialVersionUID = -230980497949939754L;
	private String code;
	private String name;
	private Double money;
	private String date;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getMoney() {
		return money;
	}
	public void setMoney(Double money) {
		this.money = money;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
}
