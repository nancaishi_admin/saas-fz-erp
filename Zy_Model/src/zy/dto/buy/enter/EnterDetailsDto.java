package zy.dto.buy.enter;

import zy.entity.buy.enter.T_Buy_EnterList;

public class EnterDetailsDto extends T_Buy_EnterList{
	private static final long serialVersionUID = -6688363156981960160L;
	private String et_make_date;
	private String supply_name;
	private String depot_name;
	public String getEt_make_date() {
		return et_make_date;
	}
	public void setEt_make_date(String et_make_date) {
		this.et_make_date = et_make_date;
	}
	public String getSupply_name() {
		return supply_name;
	}
	public void setSupply_name(String supply_name) {
		this.supply_name = supply_name;
	}
	public String getDepot_name() {
		return depot_name;
	}
	public void setDepot_name(String depot_name) {
		this.depot_name = depot_name;
	}
}
