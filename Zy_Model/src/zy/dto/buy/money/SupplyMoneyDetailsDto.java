package zy.dto.buy.money;

import java.io.Serializable;

public class SupplyMoneyDetailsDto implements Serializable{
	private static final long serialVersionUID = 8913003978010952296L;
	private Integer id;
	private String make_date;
	private String number;
	private Integer type;
	private String supply_code;
	private String supply_name;
	private Integer pay_state;
	private Double money;
	private Double discount_money;
	private Double prepay;
	private Double payabled;
	private Double payable;
	private String manager;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMake_date() {
		return make_date;
	}
	public void setMake_date(String make_date) {
		this.make_date = make_date;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getSupply_code() {
		return supply_code;
	}
	public void setSupply_code(String supply_code) {
		this.supply_code = supply_code;
	}
	public String getSupply_name() {
		return supply_name;
	}
	public void setSupply_name(String supply_name) {
		this.supply_name = supply_name;
	}
	public Integer getPay_state() {
		return pay_state;
	}
	public void setPay_state(Integer pay_state) {
		this.pay_state = pay_state;
	}
	public Double getMoney() {
		return money;
	}
	public void setMoney(Double money) {
		this.money = money;
	}
	public Double getDiscount_money() {
		return discount_money;
	}
	public void setDiscount_money(Double discount_money) {
		this.discount_money = discount_money;
	}
	public Double getPrepay() {
		return prepay;
	}
	public void setPrepay(Double prepay) {
		this.prepay = prepay;
	}
	public Double getPayabled() {
		return payabled;
	}
	public void setPayabled(Double payabled) {
		this.payabled = payabled;
	}
	public Double getPayable() {
		return payable;
	}
	public void setPayable(Double payable) {
		this.payable = payable;
	}
	public String getManager() {
		return manager;
	}
	public void setManager(String manager) {
		this.manager = manager;
	}
}
