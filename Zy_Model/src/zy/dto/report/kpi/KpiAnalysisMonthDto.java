package zy.dto.report.kpi;

import java.io.Serializable;

public class KpiAnalysisMonthDto implements Serializable{
	private static final long serialVersionUID = -6276097341138437670L;
	private String id;
	private String month;
	private int comeamount;//进店量
	private int receiveamount;//接待人数
	private int dealcount;//成交单数
	private int vipcount;//开卡人数
	private int cardcount;//新增储值卡个数
	private int ecouponcount;//新增优惠券个数
	private int sellamount;//销售数量
	private double sellmoney;//销售金额
	private double retailmoney;//零售金额
	private double costmoney;//成本
	private double profits;//利润
	private double profits_rate;//利润率
	private double planmoney;//计划金额
	private double complete_rate;//完成率
	private double lost_money;//抹零金额
	private double joint_rate;//连带率
	private double deal_rate;//成交率
	private double sellmoney_vip;//会员消费
	private double sellmoney_vip_proportion;//会员消费占比
	private double avg_price;//平均件单价
	private double avg_sellprice;//平均客单价
	private double expense_money;//费用支出
	private double net_profits;//净利润
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public int getComeamount() {
		return comeamount;
	}
	public void setComeamount(int comeamount) {
		this.comeamount = comeamount;
	}
	public int getReceiveamount() {
		return receiveamount;
	}
	public void setReceiveamount(int receiveamount) {
		this.receiveamount = receiveamount;
	}
	public int getDealcount() {
		return dealcount;
	}
	public void setDealcount(int dealcount) {
		this.dealcount = dealcount;
	}
	public int getVipcount() {
		return vipcount;
	}
	public void setVipcount(int vipcount) {
		this.vipcount = vipcount;
	}
	public int getCardcount() {
		return cardcount;
	}
	public void setCardcount(int cardcount) {
		this.cardcount = cardcount;
	}
	public int getEcouponcount() {
		return ecouponcount;
	}
	public void setEcouponcount(int ecouponcount) {
		this.ecouponcount = ecouponcount;
	}
	public int getSellamount() {
		return sellamount;
	}
	public void setSellamount(int sellamount) {
		this.sellamount = sellamount;
	}
	public double getSellmoney() {
		return sellmoney;
	}
	public void setSellmoney(double sellmoney) {
		this.sellmoney = sellmoney;
	}
	public double getRetailmoney() {
		return retailmoney;
	}
	public void setRetailmoney(double retailmoney) {
		this.retailmoney = retailmoney;
	}
	public double getCostmoney() {
		return costmoney;
	}
	public void setCostmoney(double costmoney) {
		this.costmoney = costmoney;
	}
	public double getProfits() {
		return profits;
	}
	public void setProfits(double profits) {
		this.profits = profits;
	}
	public double getProfits_rate() {
		return profits_rate;
	}
	public void setProfits_rate(double profits_rate) {
		this.profits_rate = profits_rate;
	}
	public double getPlanmoney() {
		return planmoney;
	}
	public void setPlanmoney(double planmoney) {
		this.planmoney = planmoney;
	}
	public double getComplete_rate() {
		return complete_rate;
	}
	public void setComplete_rate(double complete_rate) {
		this.complete_rate = complete_rate;
	}
	public double getLost_money() {
		return lost_money;
	}
	public void setLost_money(double lost_money) {
		this.lost_money = lost_money;
	}
	public double getJoint_rate() {
		return joint_rate;
	}
	public void setJoint_rate(double joint_rate) {
		this.joint_rate = joint_rate;
	}
	public double getDeal_rate() {
		return deal_rate;
	}
	public void setDeal_rate(double deal_rate) {
		this.deal_rate = deal_rate;
	}
	public double getSellmoney_vip() {
		return sellmoney_vip;
	}
	public void setSellmoney_vip(double sellmoney_vip) {
		this.sellmoney_vip = sellmoney_vip;
	}
	public double getSellmoney_vip_proportion() {
		return sellmoney_vip_proportion;
	}
	public void setSellmoney_vip_proportion(double sellmoney_vip_proportion) {
		this.sellmoney_vip_proportion = sellmoney_vip_proportion;
	}
	public double getAvg_price() {
		return avg_price;
	}
	public void setAvg_price(double avg_price) {
		this.avg_price = avg_price;
	}
	public double getAvg_sellprice() {
		return avg_sellprice;
	}
	public void setAvg_sellprice(double avg_sellprice) {
		this.avg_sellprice = avg_sellprice;
	}
	public double getExpense_money() {
		return expense_money;
	}
	public void setExpense_money(double expense_money) {
		this.expense_money = expense_money;
	}
	public double getNet_profits() {
		return net_profits;
	}
	public void setNet_profits(double net_profits) {
		this.net_profits = net_profits;
	}
}
