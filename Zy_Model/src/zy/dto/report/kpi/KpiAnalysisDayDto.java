package zy.dto.report.kpi;

import java.io.Serializable;

public class KpiAnalysisDayDto implements Serializable{
	private static final long serialVersionUID = 9030476342437250487L;
	private String id;
	private String day;
	private String we_name;
	private String we_max_tmp;
	private String we_min_tmp;
	private String weekday;
	private int comeamount;//进店量
	private int receiveamount;//接待人数
	private int dealcount;//成交单数
	private int vipcount;//开卡人数
	private int cardcount;//新增储值卡个数
	private int ecouponcount;//新增优惠券个数
	private int sellamount;//销售数量
	private double sellmoney;//销售金额
	private double retailmoney;//零售金额
	private double costmoney;//成本
	private double profits;//利润
	private double profits_rate;//利润率
	private double lost_money;//抹零金额
	private double joint_rate;//连带率
	private double deal_rate;//成交率
	private double sellmoney_vip;//会员消费
	private double sellmoney_vip_proportion;//会员消费占比
	private double avg_price;//平均件单价
	private double avg_sellprice;//平均客单价
	private double planmoney;//计划金额
	private double complete_rate;//完成率
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public int getComeamount() {
		return comeamount;
	}
	public void setComeamount(int comeamount) {
		this.comeamount = comeamount;
	}
	public int getReceiveamount() {
		return receiveamount;
	}
	public void setReceiveamount(int receiveamount) {
		this.receiveamount = receiveamount;
	}
	public int getDealcount() {
		return dealcount;
	}
	public void setDealcount(int dealcount) {
		this.dealcount = dealcount;
	}
	public int getVipcount() {
		return vipcount;
	}
	public void setVipcount(int vipcount) {
		this.vipcount = vipcount;
	}
	public int getCardcount() {
		return cardcount;
	}
	public void setCardcount(int cardcount) {
		this.cardcount = cardcount;
	}
	public int getEcouponcount() {
		return ecouponcount;
	}
	public void setEcouponcount(int ecouponcount) {
		this.ecouponcount = ecouponcount;
	}
	public int getSellamount() {
		return sellamount;
	}
	public void setSellamount(int sellamount) {
		this.sellamount = sellamount;
	}
	public double getSellmoney() {
		return sellmoney;
	}
	public void setSellmoney(double sellmoney) {
		this.sellmoney = sellmoney;
	}
	public double getRetailmoney() {
		return retailmoney;
	}
	public void setRetailmoney(double retailmoney) {
		this.retailmoney = retailmoney;
	}
	public double getCostmoney() {
		return costmoney;
	}
	public void setCostmoney(double costmoney) {
		this.costmoney = costmoney;
	}
	public double getProfits() {
		return profits;
	}
	public void setProfits(double profits) {
		this.profits = profits;
	}
	public double getProfits_rate() {
		return profits_rate;
	}
	public void setProfits_rate(double profits_rate) {
		this.profits_rate = profits_rate;
	}
	public double getLost_money() {
		return lost_money;
	}
	public void setLost_money(double lost_money) {
		this.lost_money = lost_money;
	}
	public double getJoint_rate() {
		return joint_rate;
	}
	public void setJoint_rate(double joint_rate) {
		this.joint_rate = joint_rate;
	}
	public double getDeal_rate() {
		return deal_rate;
	}
	public void setDeal_rate(double deal_rate) {
		this.deal_rate = deal_rate;
	}
	public double getSellmoney_vip() {
		return sellmoney_vip;
	}
	public void setSellmoney_vip(double sellmoney_vip) {
		this.sellmoney_vip = sellmoney_vip;
	}
	public double getSellmoney_vip_proportion() {
		return sellmoney_vip_proportion;
	}
	public void setSellmoney_vip_proportion(double sellmoney_vip_proportion) {
		this.sellmoney_vip_proportion = sellmoney_vip_proportion;
	}
	public double getAvg_price() {
		return avg_price;
	}
	public void setAvg_price(double avg_price) {
		this.avg_price = avg_price;
	}
	public double getAvg_sellprice() {
		return avg_sellprice;
	}
	public void setAvg_sellprice(double avg_sellprice) {
		this.avg_sellprice = avg_sellprice;
	}
	public String getWe_name() {
		return we_name;
	}
	public void setWe_name(String we_name) {
		this.we_name = we_name;
	}
	public String getWe_max_tmp() {
		return we_max_tmp;
	}
	public void setWe_max_tmp(String we_max_tmp) {
		this.we_max_tmp = we_max_tmp;
	}
	public String getWe_min_tmp() {
		return we_min_tmp;
	}
	public void setWe_min_tmp(String we_min_tmp) {
		this.we_min_tmp = we_min_tmp;
	}
	public String getWeekday() {
		return weekday;
	}
	public void setWeekday(String weekday) {
		this.weekday = weekday;
	}
	public double getPlanmoney() {
		return planmoney;
	}
	public void setPlanmoney(double planmoney) {
		this.planmoney = planmoney;
	}
	public double getComplete_rate() {
		return complete_rate;
	}
	public void setComplete_rate(double complete_rate) {
		this.complete_rate = complete_rate;
	}
}
