package zy.dto.vip.member;

import java.io.Serializable;

public class ConsumeSnapshotDto implements Serializable{
	private static final long serialVersionUID = 6865531621476094542L;
	private Double customer_unit_price;//客单价
	private Double unit_price;//物单价
	private Double joint_rate;//连带率
	private Double consume_money;//消费金额
	private Integer consume_times;//消费次数
	private Integer consume_amount;//消费件数
	private Integer consume_rate;//消费频率
	private Integer recommend_amount;//推荐人数
	private String lastbuy_date;//上次消费日期
	private Double lastbuy_money;//上次消费金额
	public Double getCustomer_unit_price() {
		return customer_unit_price;
	}
	public void setCustomer_unit_price(Double customer_unit_price) {
		this.customer_unit_price = customer_unit_price;
	}
	public Double getUnit_price() {
		return unit_price;
	}
	public void setUnit_price(Double unit_price) {
		this.unit_price = unit_price;
	}
	public Double getJoint_rate() {
		return joint_rate;
	}
	public void setJoint_rate(Double joint_rate) {
		this.joint_rate = joint_rate;
	}
	public Double getConsume_money() {
		return consume_money;
	}
	public void setConsume_money(Double consume_money) {
		this.consume_money = consume_money;
	}
	public Integer getConsume_times() {
		return consume_times;
	}
	public void setConsume_times(Integer consume_times) {
		this.consume_times = consume_times;
	}
	public Integer getConsume_amount() {
		return consume_amount;
	}
	public void setConsume_amount(Integer consume_amount) {
		this.consume_amount = consume_amount;
	}
	public Integer getConsume_rate() {
		return consume_rate;
	}
	public void setConsume_rate(Integer consume_rate) {
		this.consume_rate = consume_rate;
	}
	public Integer getRecommend_amount() {
		return recommend_amount;
	}
	public void setRecommend_amount(Integer recommend_amount) {
		this.recommend_amount = recommend_amount;
	}
	public String getLastbuy_date() {
		return lastbuy_date;
	}
	public void setLastbuy_date(String lastbuy_date) {
		this.lastbuy_date = lastbuy_date;
	}
	public Double getLastbuy_money() {
		return lastbuy_money;
	}
	public void setLastbuy_money(Double lastbuy_money) {
		this.lastbuy_money = lastbuy_money;
	}
}
