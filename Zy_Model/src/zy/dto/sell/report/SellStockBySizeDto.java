package zy.dto.sell.report;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class SellStockBySizeDto implements Serializable{
	private static final long serialVersionUID = -2493158842815366393L;
	private String cr_code;
	private String cr_name;
	private String br_code;
	private String br_name;
	private Map<String, Integer> sellAmountMap = new HashMap<String, Integer>();
	private Map<String, Integer> stockAmountMap = new HashMap<String, Integer>();
	private Map<String, String> amountMap = new HashMap<String, String>();
	public String getCr_code() {
		return cr_code;
	}
	public void setCr_code(String cr_code) {
		this.cr_code = cr_code;
	}
	public String getCr_name() {
		return cr_name;
	}
	public void setCr_name(String cr_name) {
		this.cr_name = cr_name;
	}
	public String getBr_code() {
		return br_code;
	}
	public void setBr_code(String br_code) {
		this.br_code = br_code;
	}
	public String getBr_name() {
		return br_name;
	}
	public void setBr_name(String br_name) {
		this.br_name = br_name;
	}
	public Map<String, Integer> getSellAmountMap() {
		return sellAmountMap;
	}
	public void setSellAmountMap(Map<String, Integer> sellAmountMap) {
		this.sellAmountMap = sellAmountMap;
	}
	public Map<String, Integer> getStockAmountMap() {
		return stockAmountMap;
	}
	public void setStockAmountMap(Map<String, Integer> stockAmountMap) {
		this.stockAmountMap = stockAmountMap;
	}
	public Map<String, String> getAmountMap() {
		return amountMap;
	}
	public void setAmountMap(Map<String, String> amountMap) {
		this.amountMap = amountMap;
	}
	public String getTotalAmount() {
		if (stockAmountMap.isEmpty() && sellAmountMap.isEmpty()) {
			return "";
		}
		int stockAmount = 0,sellAmount = 0;
		for (String key : sellAmountMap.keySet()) {
			sellAmount += sellAmountMap.get(key);
		}
		for (String key : stockAmountMap.keySet()) {
			stockAmount += stockAmountMap.get(key);
		}
		return sellAmount+"/"+stockAmount;
	}
}
