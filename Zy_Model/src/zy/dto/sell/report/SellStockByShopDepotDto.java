package zy.dto.sell.report;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class SellStockByShopDepotDto implements Serializable{
	private static final long serialVersionUID = -1222234400374760850L;
	private String cr_code;
	private String cr_name;
	private String sz_code;
	private String sz_name;
	private String br_code;
	private String br_name;
	private String sub_code;
	private Integer totalAmount;
	private Map<String, Integer> shop_SellAmountMap = new HashMap<String, Integer>();
	private Map<String, Integer> shop_StockAmountMap = new HashMap<String, Integer>();
	private Map<String, Integer> shop_OnWayAmountMap = new HashMap<String, Integer>();
	private Map<String, String> shop_AmountMap = new HashMap<String, String>();
	private Map<String, Integer> depot_StockAmountMap = new HashMap<String, Integer>();
	public String getCr_code() {
		return cr_code;
	}
	public void setCr_code(String cr_code) {
		this.cr_code = cr_code;
	}
	public String getCr_name() {
		return cr_name;
	}
	public void setCr_name(String cr_name) {
		this.cr_name = cr_name;
	}
	public String getSz_code() {
		return sz_code;
	}
	public void setSz_code(String sz_code) {
		this.sz_code = sz_code;
	}
	public String getSz_name() {
		return sz_name;
	}
	public void setSz_name(String sz_name) {
		this.sz_name = sz_name;
	}
	public String getBr_code() {
		return br_code;
	}
	public void setBr_code(String br_code) {
		this.br_code = br_code;
	}
	public String getBr_name() {
		return br_name;
	}
	public void setBr_name(String br_name) {
		this.br_name = br_name;
	}
	public String getSub_code() {
		return sub_code;
	}
	public void setSub_code(String sub_code) {
		this.sub_code = sub_code;
	}
	public Map<String, Integer> getShop_SellAmountMap() {
		return shop_SellAmountMap;
	}
	public void setShop_SellAmountMap(Map<String, Integer> shop_SellAmountMap) {
		this.shop_SellAmountMap = shop_SellAmountMap;
	}
	public Map<String, Integer> getShop_StockAmountMap() {
		return shop_StockAmountMap;
	}
	public void setShop_StockAmountMap(Map<String, Integer> shop_StockAmountMap) {
		this.shop_StockAmountMap = shop_StockAmountMap;
	}
	public Map<String, Integer> getShop_OnWayAmountMap() {
		return shop_OnWayAmountMap;
	}
	public void setShop_OnWayAmountMap(Map<String, Integer> shop_OnWayAmountMap) {
		this.shop_OnWayAmountMap = shop_OnWayAmountMap;
	}
	public Map<String, Integer> getDepot_StockAmountMap() {
		return depot_StockAmountMap;
	}
	public void setDepot_StockAmountMap(Map<String, Integer> depot_StockAmountMap) {
		this.depot_StockAmountMap = depot_StockAmountMap;
	}
	public Map<String, String> getShop_AmountMap() {
		return shop_AmountMap;
	}
	public void setShop_AmountMap(Map<String, String> shop_AmountMap) {
		this.shop_AmountMap = shop_AmountMap;
	}
	public Integer getTotalAmount() {
		totalAmount = 0;
		for (String key : shop_StockAmountMap.keySet()) {
			totalAmount += shop_StockAmountMap.get(key);
		}
		for (String key : depot_StockAmountMap.keySet()) {
			totalAmount += depot_StockAmountMap.get(key);
		}
		return totalAmount;
	}
}
