package zy.dto.base.emp;

import java.io.Serializable;

public class EmpPerformanceDto implements Serializable{
	private static final long serialVersionUID = 6463757737024217881L;
	private String pf_name;
	private String pf_value;
	public EmpPerformanceDto(){
	}
	public EmpPerformanceDto(String pf_name, String pf_value) {
		this.pf_name = pf_name;
		this.pf_value = pf_value;
	}
	public String getPf_name() {
		return pf_name;
	}
	public void setPf_name(String pf_name) {
		this.pf_name = pf_name;
	}
	public String getPf_value() {
		return pf_value;
	}
	public void setPf_value(String pf_value) {
		this.pf_value = pf_value;
	}
}
