package zy.dto.base.product;

import java.util.Map;

import zy.entity.base.product.T_Base_Product;

public class ProductCartDto extends T_Base_Product{
	private static final long serialVersionUID = -5802947929564026261L;
	private boolean selected;
	private Map<String,Object> stockMap;
	private int amount = 1;
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public Map<String, Object> getStockMap() {
		return stockMap;
	}
	public void setStockMap(Map<String, Object> stockMap) {
		this.stockMap = stockMap;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
}
