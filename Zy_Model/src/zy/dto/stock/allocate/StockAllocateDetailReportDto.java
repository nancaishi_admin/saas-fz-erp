package zy.dto.stock.allocate;

import zy.entity.stock.allocate.T_Stock_AllocateList;

public class StockAllocateDetailReportDto extends T_Stock_AllocateList{
	private static final long serialVersionUID = 9176809601482317504L;
	private String ac_date;
	private String ac_manager;
	private String outdepot_name;
	private String indepot_name;
	public String getAc_date() {
		return ac_date;
	}
	public void setAc_date(String ac_date) {
		this.ac_date = ac_date;
	}
	public String getAc_manager() {
		return ac_manager;
	}
	public void setAc_manager(String ac_manager) {
		this.ac_manager = ac_manager;
	}
	public String getOutdepot_name() {
		return outdepot_name;
	}
	public void setOutdepot_name(String outdepot_name) {
		this.outdepot_name = outdepot_name;
	}
	public String getIndepot_name() {
		return indepot_name;
	}
	public void setIndepot_name(String indepot_name) {
		this.indepot_name = indepot_name;
	}
}
