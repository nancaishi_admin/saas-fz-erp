package zy.dto.stock.data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class StockReportByShopDto implements Serializable{
	private static final long serialVersionUID = -232358483439473765L;
	private String code;
	private String name;
	private String amountmoney;
	private Map<String, Integer> amountMap = new HashMap<String, Integer>();
	private Map<String, Double> moneyMap = new HashMap<String, Double>();
	private Integer totalamount;
	private Double totalmoney;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAmountmoney() {
		return amountmoney;
	}
	public void setAmountmoney(String amountmoney) {
		this.amountmoney = amountmoney;
	}
	public Map<String, Integer> getAmountMap() {
		return amountMap;
	}
	public void setAmountMap(Map<String, Integer> amountMap) {
		this.amountMap = amountMap;
	}
	public Map<String, Double> getMoneyMap() {
		return moneyMap;
	}
	public void setMoneyMap(Map<String, Double> moneyMap) {
		this.moneyMap = moneyMap;
	}
	public Integer getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(Integer totalamount) {
		this.totalamount = totalamount;
	}
	public Double getTotalmoney() {
		return totalmoney;
	}
	public void setTotalmoney(Double totalmoney) {
		this.totalmoney = totalmoney;
	}
}
