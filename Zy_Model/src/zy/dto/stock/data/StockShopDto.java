package zy.dto.stock.data;

import java.io.Serializable;

public class StockShopDto implements Serializable{
	private static final long serialVersionUID = -5003579490881325818L;
	private String shop_code;
	private String shop_name;
	private Integer amount;
	private Double cost_money;
	private Double sell_money;
	private Double sort_money;
	public String getShop_code() {
		return shop_code;
	}
	public void setShop_code(String shop_code) {
		this.shop_code = shop_code;
	}
	public String getShop_name() {
		return shop_name;
	}
	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	public Double getCost_money() {
		return cost_money;
	}
	public void setCost_money(Double cost_money) {
		this.cost_money = cost_money;
	}
	public Double getSell_money() {
		return sell_money;
	}
	public void setSell_money(Double sell_money) {
		this.sell_money = sell_money;
	}
	public Double getSort_money() {
		return sort_money;
	}
	public void setSort_money(Double sort_money) {
		this.sort_money = sort_money;
	}
}
