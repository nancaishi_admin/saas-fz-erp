package zy.dto.stock.check;

import zy.entity.stock.check.T_Stock_CheckList;

public class CheckDiffDto extends T_Stock_CheckList{
	private static final long serialVersionUID = 3816402370410667727L;
	private String ba_number;
	private String ba_dp_code;
	private String depot_name;
	public String getBa_number() {
		return ba_number;
	}
	public void setBa_number(String ba_number) {
		this.ba_number = ba_number;
	}
	public String getBa_dp_code() {
		return ba_dp_code;
	}
	public void setBa_dp_code(String ba_dp_code) {
		this.ba_dp_code = ba_dp_code;
	}
	public String getDepot_name() {
		return depot_name;
	}
	public void setDepot_name(String depot_name) {
		this.depot_name = depot_name;
	}
}
