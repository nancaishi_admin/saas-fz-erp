package zy.dto.sqb;

import java.io.Serializable;

public class QueryReq implements Serializable{
	private static final long serialVersionUID = -6086574311021244689L;
	private String terminal_sn;//终端号
	private String terminal_key;//终端密钥
	private String client_sn;//商户系统订单号
	private String sn;//收钱吧系统内部唯一订单号
	public String getTerminal_sn() {
		return terminal_sn;
	}
	public void setTerminal_sn(String terminal_sn) {
		this.terminal_sn = terminal_sn;
	}
	public String getTerminal_key() {
		return terminal_key;
	}
	public void setTerminal_key(String terminal_key) {
		this.terminal_key = terminal_key;
	}
	public String getClient_sn() {
		return client_sn;
	}
	public void setClient_sn(String client_sn) {
		this.client_sn = client_sn;
	}
	public String getSn() {
		return sn;
	}
	public void setSn(String sn) {
		this.sn = sn;
	}
}
