package zy.entity.common.todo;

import java.io.Serializable;

public class Common_ToDo implements Serializable{
	private static final long serialVersionUID = -8339450206662259406L;
	private Integer td_id;
	private String td_name;
	private String td_url;
	private Integer td_state;
	private String td_version;
	private String td_shop_type;
	private String td_style;
	private String td_icon;
	private String td_identity;
	public Integer getTd_id() {
		return td_id;
	}
	public void setTd_id(Integer td_id) {
		this.td_id = td_id;
	}
	public String getTd_name() {
		return td_name;
	}
	public void setTd_name(String td_name) {
		this.td_name = td_name;
	}
	public String getTd_url() {
		return td_url;
	}
	public void setTd_url(String td_url) {
		this.td_url = td_url;
	}
	public Integer getTd_state() {
		return td_state;
	}
	public void setTd_state(Integer td_state) {
		this.td_state = td_state;
	}
	public String getTd_version() {
		return td_version;
	}
	public void setTd_version(String td_version) {
		this.td_version = td_version;
	}
	public String getTd_shop_type() {
		return td_shop_type;
	}
	public void setTd_shop_type(String td_shop_type) {
		this.td_shop_type = td_shop_type;
	}
	public String getTd_style() {
		return td_style;
	}
	public void setTd_style(String td_style) {
		this.td_style = td_style;
	}
	public String getTd_icon() {
		return td_icon;
	}
	public void setTd_icon(String td_icon) {
		this.td_icon = td_icon;
	}
	public String getTd_identity() {
		return td_identity;
	}
	public void setTd_identity(String td_identity) {
		this.td_identity = td_identity;
	}
}
