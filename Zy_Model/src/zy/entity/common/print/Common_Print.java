package zy.entity.common.print;

import java.io.Serializable;

public class Common_Print implements Serializable{
	private static final long serialVersionUID = 3269568098346885676L;
	private Integer pt_id;
	private String pt_name;
	private Integer pt_type;
	public Integer getPt_id() {
		return pt_id;
	}
	public void setPt_id(Integer pt_id) {
		this.pt_id = pt_id;
	}
	public String getPt_name() {
		return pt_name;
	}
	public void setPt_name(String pt_name) {
		this.pt_name = pt_name;
	}
	public Integer getPt_type() {
		return pt_type;
	}
	public void setPt_type(Integer pt_type) {
		this.pt_type = pt_type;
	}
}
