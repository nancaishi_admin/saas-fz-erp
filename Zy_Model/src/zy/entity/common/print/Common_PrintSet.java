package zy.entity.common.print;

import java.io.Serializable;

public class Common_PrintSet implements Serializable{
	private static final long serialVersionUID = -6442697353389438722L;
	private Integer pts_id;
	private Integer pts_pt_id;
	private String pts_code;
	private String pts_name;
	public Integer getPts_id() {
		return pts_id;
	}
	public void setPts_id(Integer pts_id) {
		this.pts_id = pts_id;
	}
	public Integer getPts_pt_id() {
		return pts_pt_id;
	}
	public void setPts_pt_id(Integer pts_pt_id) {
		this.pts_pt_id = pts_pt_id;
	}
	public String getPts_code() {
		return pts_code;
	}
	public void setPts_code(String pts_code) {
		this.pts_code = pts_code;
	}
	public String getPts_name() {
		return pts_name;
	}
	public void setPts_name(String pts_name) {
		this.pts_name = pts_name;
	}
}
