package zy.entity.common.print;

import java.io.Serializable;

public class Common_PrintData implements Serializable{
	private static final long serialVersionUID = 1890149197243572980L;
	private Integer ptd_id;
	private Integer ptd_pt_id;
	private String ptd_code;
	private String ptd_name;
	private Integer ptd_order;
	private Integer ptd_show;
	private Integer ptd_width;
	private Integer ptd_align;
	public Integer getPtd_id() {
		return ptd_id;
	}
	public void setPtd_id(Integer ptd_id) {
		this.ptd_id = ptd_id;
	}
	public Integer getPtd_pt_id() {
		return ptd_pt_id;
	}
	public void setPtd_pt_id(Integer ptd_pt_id) {
		this.ptd_pt_id = ptd_pt_id;
	}
	public String getPtd_code() {
		return ptd_code;
	}
	public void setPtd_code(String ptd_code) {
		this.ptd_code = ptd_code;
	}
	public String getPtd_name() {
		return ptd_name;
	}
	public void setPtd_name(String ptd_name) {
		this.ptd_name = ptd_name;
	}
	public Integer getPtd_order() {
		return ptd_order;
	}
	public void setPtd_order(Integer ptd_order) {
		this.ptd_order = ptd_order;
	}
	public Integer getPtd_show() {
		return ptd_show;
	}
	public void setPtd_show(Integer ptd_show) {
		this.ptd_show = ptd_show;
	}
	public Integer getPtd_width() {
		return ptd_width;
	}
	public void setPtd_width(Integer ptd_width) {
		this.ptd_width = ptd_width;
	}
	public Integer getPtd_align() {
		return ptd_align;
	}
	public void setPtd_align(Integer ptd_align) {
		this.ptd_align = ptd_align;
	}
}
