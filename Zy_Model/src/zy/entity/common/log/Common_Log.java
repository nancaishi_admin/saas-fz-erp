package zy.entity.common.log;

import java.io.Serializable;

public class Common_Log implements Serializable{
	private static final long serialVersionUID = 6681362161900210846L;
	private Integer lg_id;
	private String lg_title;
	private String lg_content;
	private String lg_user;
	private String lg_sysdate;
	public Integer getLg_id() {
		return lg_id;
	}
	public void setLg_id(Integer lg_id) {
		this.lg_id = lg_id;
	}
	public String getLg_title() {
		return lg_title;
	}
	public void setLg_title(String lg_title) {
		this.lg_title = lg_title;
	}
	public String getLg_content() {
		return lg_content;
	}
	public void setLg_content(String lg_content) {
		this.lg_content = lg_content;
	}
	public String getLg_user() {
		return lg_user;
	}
	public void setLg_user(String lg_user) {
		this.lg_user = lg_user;
	}
	public String getLg_sysdate() {
		return lg_sysdate;
	}
	public void setLg_sysdate(String lg_sysdate) {
		this.lg_sysdate = lg_sysdate;
	}
}
