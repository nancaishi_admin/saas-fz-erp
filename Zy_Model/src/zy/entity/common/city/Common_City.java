package zy.entity.common.city;

import java.io.Serializable;

public class Common_City implements Serializable{
	private static final long serialVersionUID = 1L;
	private Integer cy_id;
	private String cy_name;
	private Integer cy_upid;
	private Integer cy_level;
	private String cy_area_code;
	private String cy_post_code;
	private Integer cy_state;
	public Integer getCy_id() {
		return cy_id;
	}
	public void setCy_id(Integer cy_id) {
		this.cy_id = cy_id;
	}
	public String getCy_name() {
		return cy_name;
	}
	public void setCy_name(String cy_name) {
		this.cy_name = cy_name;
	}
	public Integer getCy_upid() {
		return cy_upid;
	}
	public void setCy_upid(Integer cy_upid) {
		this.cy_upid = cy_upid;
	}
	public Integer getCy_level() {
		return cy_level;
	}
	public void setCy_level(Integer cy_level) {
		this.cy_level = cy_level;
	}
	public String getCy_area_code() {
		return cy_area_code;
	}
	public void setCy_area_code(String cy_area_code) {
		this.cy_area_code = cy_area_code;
	}
	public String getCy_post_code() {
		return cy_post_code;
	}
	public void setCy_post_code(String cy_post_code) {
		this.cy_post_code = cy_post_code;
	}
	public Integer getCy_state() {
		return cy_state;
	}
	public void setCy_state(Integer cy_state) {
		this.cy_state = cy_state;
	}
	
}
