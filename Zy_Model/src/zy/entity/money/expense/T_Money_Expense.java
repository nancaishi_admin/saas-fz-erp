package zy.entity.money.expense;

import java.io.Serializable;

public class T_Money_Expense implements Serializable{
	private static final long serialVersionUID = -4441124332390616672L;
	private Integer ep_id;
	private String ep_number;
	private String ep_shop_code;
	private String shop_name;
	private String ep_maker;
	private String ep_manager;
	private String ep_date;
	private Integer ep_ar_state;
	private String ep_ar_date;
	private Double ep_money;
	private String ep_ba_code;
	private String ba_name;
	private String ep_dm_code;
	private String dm_name;
	private String ep_remark;
	private Integer ep_share;
	private String ep_sysdate;
	private Integer ep_us_id;
	private Integer companyid;
	private String ar_describe;
	public Integer getEp_id() {
		return ep_id;
	}
	public void setEp_id(Integer ep_id) {
		this.ep_id = ep_id;
	}
	public String getEp_number() {
		return ep_number;
	}
	public void setEp_number(String ep_number) {
		this.ep_number = ep_number;
	}
	public String getEp_shop_code() {
		return ep_shop_code;
	}
	public void setEp_shop_code(String ep_shop_code) {
		this.ep_shop_code = ep_shop_code;
	}
	public String getShop_name() {
		return shop_name;
	}
	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}
	public String getEp_maker() {
		return ep_maker;
	}
	public void setEp_maker(String ep_maker) {
		this.ep_maker = ep_maker;
	}
	public String getEp_manager() {
		return ep_manager;
	}
	public void setEp_manager(String ep_manager) {
		this.ep_manager = ep_manager;
	}
	public String getEp_date() {
		return ep_date;
	}
	public void setEp_date(String ep_date) {
		this.ep_date = ep_date;
	}
	public Integer getEp_ar_state() {
		return ep_ar_state;
	}
	public void setEp_ar_state(Integer ep_ar_state) {
		this.ep_ar_state = ep_ar_state;
	}
	public String getEp_ar_date() {
		return ep_ar_date;
	}
	public void setEp_ar_date(String ep_ar_date) {
		this.ep_ar_date = ep_ar_date;
	}
	public Double getEp_money() {
		return ep_money;
	}
	public void setEp_money(Double ep_money) {
		this.ep_money = ep_money;
	}
	public String getEp_ba_code() {
		return ep_ba_code;
	}
	public void setEp_ba_code(String ep_ba_code) {
		this.ep_ba_code = ep_ba_code;
	}
	public String getBa_name() {
		return ba_name;
	}
	public void setBa_name(String ba_name) {
		this.ba_name = ba_name;
	}
	public String getEp_remark() {
		return ep_remark;
	}
	public void setEp_remark(String ep_remark) {
		this.ep_remark = ep_remark;
	}
	public Integer getEp_share() {
		return ep_share;
	}
	public void setEp_share(Integer ep_share) {
		this.ep_share = ep_share;
	}
	public String getEp_sysdate() {
		return ep_sysdate;
	}
	public void setEp_sysdate(String ep_sysdate) {
		this.ep_sysdate = ep_sysdate;
	}
	public Integer getEp_us_id() {
		return ep_us_id;
	}
	public void setEp_us_id(Integer ep_us_id) {
		this.ep_us_id = ep_us_id;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getAr_describe() {
		return ar_describe;
	}
	public void setAr_describe(String ar_describe) {
		this.ar_describe = ar_describe;
	}
	public String getEp_dm_code() {
		return ep_dm_code;
	}
	public void setEp_dm_code(String ep_dm_code) {
		this.ep_dm_code = ep_dm_code;
	}
	public String getDm_name() {
		return dm_name;
	}
	public void setDm_name(String dm_name) {
		this.dm_name = dm_name;
	}
}
