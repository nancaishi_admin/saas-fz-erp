package zy.entity.money.expense;

import java.io.Serializable;

public class T_Money_ExpenseList implements Serializable{
	private static final long serialVersionUID = 8189824096242405103L;
	private Integer epl_id;
	private String epl_number;
	private String epl_mp_code;
	private String mp_name;
	private Double epl_money;
	private String epl_remark;
	private String epl_sharedate;
	private Integer epl_sharemonth;
	private Integer epl_us_id;
	private Integer companyid;
	public Integer getEpl_id() {
		return epl_id;
	}
	public void setEpl_id(Integer epl_id) {
		this.epl_id = epl_id;
	}
	public String getEpl_number() {
		return epl_number;
	}
	public void setEpl_number(String epl_number) {
		this.epl_number = epl_number;
	}
	public String getEpl_mp_code() {
		return epl_mp_code;
	}
	public void setEpl_mp_code(String epl_mp_code) {
		this.epl_mp_code = epl_mp_code;
	}
	public String getMp_name() {
		return mp_name;
	}
	public void setMp_name(String mp_name) {
		this.mp_name = mp_name;
	}
	public Double getEpl_money() {
		return epl_money;
	}
	public void setEpl_money(Double epl_money) {
		this.epl_money = epl_money;
	}
	public String getEpl_remark() {
		return epl_remark;
	}
	public void setEpl_remark(String epl_remark) {
		this.epl_remark = epl_remark;
	}
	public String getEpl_sharedate() {
		return epl_sharedate;
	}
	public void setEpl_sharedate(String epl_sharedate) {
		this.epl_sharedate = epl_sharedate;
	}
	public Integer getEpl_sharemonth() {
		return epl_sharemonth;
	}
	public void setEpl_sharemonth(Integer epl_sharemonth) {
		this.epl_sharemonth = epl_sharemonth;
	}
	public Integer getEpl_us_id() {
		return epl_us_id;
	}
	public void setEpl_us_id(Integer epl_us_id) {
		this.epl_us_id = epl_us_id;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
