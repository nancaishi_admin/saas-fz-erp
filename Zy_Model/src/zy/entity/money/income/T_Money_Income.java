package zy.entity.money.income;

import java.io.Serializable;

public class T_Money_Income implements Serializable{
	private static final long serialVersionUID = 7998162212287251654L;
	private Integer ic_id;
	private String ic_number;
	private String ic_shop_code;
	private String shop_name;
	private String ic_maker;
	private String ic_manager;
	private String ic_date;
	private Integer ic_ar_state;
	private String ic_ar_date;
	private Double ic_money;
	private String ic_ba_code;
	private String ba_name;
	private String ic_remark;
	private String ic_sysdate;
	private Integer ic_us_id;
	private Integer companyid;
	private String ar_describe;
	public Integer getIc_id() {
		return ic_id;
	}
	public void setIc_id(Integer ic_id) {
		this.ic_id = ic_id;
	}
	public String getIc_number() {
		return ic_number;
	}
	public void setIc_number(String ic_number) {
		this.ic_number = ic_number;
	}
	public String getIc_shop_code() {
		return ic_shop_code;
	}
	public void setIc_shop_code(String ic_shop_code) {
		this.ic_shop_code = ic_shop_code;
	}
	public String getShop_name() {
		return shop_name;
	}
	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}
	public String getIc_maker() {
		return ic_maker;
	}
	public void setIc_maker(String ic_maker) {
		this.ic_maker = ic_maker;
	}
	public String getIc_manager() {
		return ic_manager;
	}
	public void setIc_manager(String ic_manager) {
		this.ic_manager = ic_manager;
	}
	public String getIc_date() {
		return ic_date;
	}
	public void setIc_date(String ic_date) {
		this.ic_date = ic_date;
	}
	public Integer getIc_ar_state() {
		return ic_ar_state;
	}
	public void setIc_ar_state(Integer ic_ar_state) {
		this.ic_ar_state = ic_ar_state;
	}
	public String getIc_ar_date() {
		return ic_ar_date;
	}
	public void setIc_ar_date(String ic_ar_date) {
		this.ic_ar_date = ic_ar_date;
	}
	public Double getIc_money() {
		return ic_money;
	}
	public void setIc_money(Double ic_money) {
		this.ic_money = ic_money;
	}
	public String getIc_ba_code() {
		return ic_ba_code;
	}
	public void setIc_ba_code(String ic_ba_code) {
		this.ic_ba_code = ic_ba_code;
	}
	public String getIc_remark() {
		return ic_remark;
	}
	public void setIc_remark(String ic_remark) {
		this.ic_remark = ic_remark;
	}
	public String getIc_sysdate() {
		return ic_sysdate;
	}
	public void setIc_sysdate(String ic_sysdate) {
		this.ic_sysdate = ic_sysdate;
	}
	public Integer getIc_us_id() {
		return ic_us_id;
	}
	public void setIc_us_id(Integer ic_us_id) {
		this.ic_us_id = ic_us_id;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getBa_name() {
		return ba_name;
	}
	public void setBa_name(String ba_name) {
		this.ba_name = ba_name;
	}
	public String getAr_describe() {
		return ar_describe;
	}
	public void setAr_describe(String ar_describe) {
		this.ar_describe = ar_describe;
	}
}
