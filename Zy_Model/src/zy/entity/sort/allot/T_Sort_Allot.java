package zy.entity.sort.allot;

import java.io.Serializable;

public class T_Sort_Allot implements Serializable{
	private static final long serialVersionUID = 8850467154019742690L;
	private Integer at_id;
	private String at_number;
	private String at_date;
	private String at_shop_code;
	private String shop_name;
	private String at_outdp_code;
	private String at_indp_code;
	private String outdepot_name;
	private String indepot_name;
	private String at_maker;
	private String at_manager;
	private String at_handnumber;
	private Integer at_applyamount;
	private Integer at_sendamount;
	private Double at_applymoney;
	private Double at_sendmoney;
	private Double at_sendcostmoney;
	private Double at_sendsellmoney;
	private Double at_discount_money;
	private String at_ba_code;
	private String at_property;
	private String at_remark;
	private Integer at_ar_state;
	private String at_ar_date;
	private Integer at_pay_state;
	private Double at_receivable;
	private Double at_received;
	private Double at_prepay;
	private Double at_lastdebt;
	private Integer at_type;
	private String at_sysdate;
	private Integer at_us_id;
	private Integer at_isdraft;
	private Integer companyid;
	private String ar_describe;
	public Integer getAt_id() {
		return at_id;
	}
	public void setAt_id(Integer at_id) {
		this.at_id = at_id;
	}
	public String getAt_number() {
		return at_number;
	}
	public void setAt_number(String at_number) {
		this.at_number = at_number;
	}
	public String getAt_date() {
		return at_date;
	}
	public void setAt_date(String at_date) {
		this.at_date = at_date;
	}
	public String getAt_shop_code() {
		return at_shop_code;
	}
	public void setAt_shop_code(String at_shop_code) {
		this.at_shop_code = at_shop_code;
	}
	public String getShop_name() {
		return shop_name;
	}
	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}
	public String getAt_outdp_code() {
		return at_outdp_code;
	}
	public void setAt_outdp_code(String at_outdp_code) {
		this.at_outdp_code = at_outdp_code;
	}
	public String getAt_indp_code() {
		return at_indp_code;
	}
	public void setAt_indp_code(String at_indp_code) {
		this.at_indp_code = at_indp_code;
	}
	public String getOutdepot_name() {
		return outdepot_name;
	}
	public void setOutdepot_name(String outdepot_name) {
		this.outdepot_name = outdepot_name;
	}
	public String getIndepot_name() {
		return indepot_name;
	}
	public void setIndepot_name(String indepot_name) {
		this.indepot_name = indepot_name;
	}
	public String getAt_maker() {
		return at_maker;
	}
	public void setAt_maker(String at_maker) {
		this.at_maker = at_maker;
	}
	public String getAt_manager() {
		return at_manager;
	}
	public void setAt_manager(String at_manager) {
		this.at_manager = at_manager;
	}
	public String getAt_handnumber() {
		return at_handnumber;
	}
	public void setAt_handnumber(String at_handnumber) {
		this.at_handnumber = at_handnumber;
	}
	public Integer getAt_applyamount() {
		return at_applyamount;
	}
	public void setAt_applyamount(Integer at_applyamount) {
		this.at_applyamount = at_applyamount;
	}
	public Integer getAt_sendamount() {
		return at_sendamount;
	}
	public void setAt_sendamount(Integer at_sendamount) {
		this.at_sendamount = at_sendamount;
	}
	public Double getAt_applymoney() {
		return at_applymoney;
	}
	public void setAt_applymoney(Double at_applymoney) {
		this.at_applymoney = at_applymoney;
	}
	public Double getAt_sendmoney() {
		return at_sendmoney;
	}
	public void setAt_sendmoney(Double at_sendmoney) {
		this.at_sendmoney = at_sendmoney;
	}
	public Double getAt_sendcostmoney() {
		return at_sendcostmoney;
	}
	public void setAt_sendcostmoney(Double at_sendcostmoney) {
		this.at_sendcostmoney = at_sendcostmoney;
	}
	public Double getAt_sendsellmoney() {
		return at_sendsellmoney;
	}
	public void setAt_sendsellmoney(Double at_sendsellmoney) {
		this.at_sendsellmoney = at_sendsellmoney;
	}
	public Double getAt_discount_money() {
		return at_discount_money;
	}
	public void setAt_discount_money(Double at_discount_money) {
		this.at_discount_money = at_discount_money;
	}
	public String getAt_ba_code() {
		return at_ba_code;
	}
	public void setAt_ba_code(String at_ba_code) {
		this.at_ba_code = at_ba_code;
	}
	public String getAt_property() {
		return at_property;
	}
	public void setAt_property(String at_property) {
		this.at_property = at_property;
	}
	public String getAt_remark() {
		return at_remark;
	}
	public void setAt_remark(String at_remark) {
		this.at_remark = at_remark;
	}
	public Integer getAt_ar_state() {
		return at_ar_state;
	}
	public void setAt_ar_state(Integer at_ar_state) {
		this.at_ar_state = at_ar_state;
	}
	public String getAt_ar_date() {
		return at_ar_date;
	}
	public void setAt_ar_date(String at_ar_date) {
		this.at_ar_date = at_ar_date;
	}
	public Integer getAt_pay_state() {
		return at_pay_state;
	}
	public void setAt_pay_state(Integer at_pay_state) {
		this.at_pay_state = at_pay_state;
	}
	public Double getAt_receivable() {
		return at_receivable;
	}
	public void setAt_receivable(Double at_receivable) {
		this.at_receivable = at_receivable;
	}
	public Double getAt_received() {
		return at_received;
	}
	public void setAt_received(Double at_received) {
		this.at_received = at_received;
	}
	public Double getAt_prepay() {
		return at_prepay;
	}
	public void setAt_prepay(Double at_prepay) {
		this.at_prepay = at_prepay;
	}
	public Double getAt_lastdebt() {
		return at_lastdebt;
	}
	public void setAt_lastdebt(Double at_lastdebt) {
		this.at_lastdebt = at_lastdebt;
	}
	public Integer getAt_type() {
		return at_type;
	}
	public void setAt_type(Integer at_type) {
		this.at_type = at_type;
	}
	public String getAt_sysdate() {
		return at_sysdate;
	}
	public void setAt_sysdate(String at_sysdate) {
		this.at_sysdate = at_sysdate;
	}
	public Integer getAt_us_id() {
		return at_us_id;
	}
	public void setAt_us_id(Integer at_us_id) {
		this.at_us_id = at_us_id;
	}
	public Integer getAt_isdraft() {
		return at_isdraft;
	}
	public void setAt_isdraft(Integer at_isdraft) {
		this.at_isdraft = at_isdraft;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getAr_describe() {
		return ar_describe;
	}
	public void setAr_describe(String ar_describe) {
		this.ar_describe = ar_describe;
	}
}
