package zy.entity.batch.order;

import java.io.Serializable;

public class T_Batch_Order implements Serializable{
	private static final long serialVersionUID = -1205571357589658853L;
	private Integer od_id;
	private String od_number;
	private String od_make_date;
	private String od_delivery_date;
	private String od_client_code;
	private String client_name;
	private String od_client_shop_code;
	private String client_shop_name;
	private String od_depot_code;
	private String depot_name;
	private String od_maker;
	private String od_manager;
	private Integer od_state;
	private String od_handnumber;
	private Integer od_amount;
	private Integer od_realamount;
	private Double od_money;
	private Double od_retailmoney;
	private Double od_costmoney;
	private String od_remark;
	private Integer od_ar_state;
	private String od_ar_date;
	private Integer od_isdraft;
	private String od_sysdate;
	private Integer od_us_id;
	private Integer od_type;
	private Integer od_isprint;
	private Integer companyid;
	private String ar_describe;
	public Integer getOd_id() {
		return od_id;
	}
	public void setOd_id(Integer od_id) {
		this.od_id = od_id;
	}
	public String getOd_number() {
		return od_number;
	}
	public void setOd_number(String od_number) {
		this.od_number = od_number;
	}
	public String getOd_make_date() {
		return od_make_date;
	}
	public void setOd_make_date(String od_make_date) {
		this.od_make_date = od_make_date;
	}
	public String getOd_delivery_date() {
		return od_delivery_date;
	}
	public void setOd_delivery_date(String od_delivery_date) {
		this.od_delivery_date = od_delivery_date;
	}
	public String getOd_client_code() {
		return od_client_code;
	}
	public void setOd_client_code(String od_client_code) {
		this.od_client_code = od_client_code;
	}
	public String getClient_name() {
		return client_name;
	}
	public void setClient_name(String client_name) {
		this.client_name = client_name;
	}
	public String getOd_depot_code() {
		return od_depot_code;
	}
	public void setOd_depot_code(String od_depot_code) {
		this.od_depot_code = od_depot_code;
	}
	public String getDepot_name() {
		return depot_name;
	}
	public void setDepot_name(String depot_name) {
		this.depot_name = depot_name;
	}
	public String getOd_maker() {
		return od_maker;
	}
	public void setOd_maker(String od_maker) {
		this.od_maker = od_maker;
	}
	public String getOd_manager() {
		return od_manager;
	}
	public void setOd_manager(String od_manager) {
		this.od_manager = od_manager;
	}
	public Integer getOd_state() {
		return od_state;
	}
	public void setOd_state(Integer od_state) {
		this.od_state = od_state;
	}
	public String getOd_handnumber() {
		return od_handnumber;
	}
	public void setOd_handnumber(String od_handnumber) {
		this.od_handnumber = od_handnumber;
	}
	public Integer getOd_amount() {
		return od_amount;
	}
	public void setOd_amount(Integer od_amount) {
		this.od_amount = od_amount;
	}
	public Integer getOd_realamount() {
		return od_realamount;
	}
	public void setOd_realamount(Integer od_realamount) {
		this.od_realamount = od_realamount;
	}
	public Double getOd_money() {
		return od_money;
	}
	public void setOd_money(Double od_money) {
		this.od_money = od_money;
	}
	public Double getOd_retailmoney() {
		return od_retailmoney;
	}
	public void setOd_retailmoney(Double od_retailmoney) {
		this.od_retailmoney = od_retailmoney;
	}
	public Double getOd_costmoney() {
		return od_costmoney;
	}
	public void setOd_costmoney(Double od_costmoney) {
		this.od_costmoney = od_costmoney;
	}
	public String getOd_remark() {
		return od_remark;
	}
	public void setOd_remark(String od_remark) {
		this.od_remark = od_remark;
	}
	public Integer getOd_ar_state() {
		return od_ar_state;
	}
	public void setOd_ar_state(Integer od_ar_state) {
		this.od_ar_state = od_ar_state;
	}
	public String getOd_ar_date() {
		return od_ar_date;
	}
	public void setOd_ar_date(String od_ar_date) {
		this.od_ar_date = od_ar_date;
	}
	public Integer getOd_isdraft() {
		return od_isdraft;
	}
	public void setOd_isdraft(Integer od_isdraft) {
		this.od_isdraft = od_isdraft;
	}
	public String getOd_sysdate() {
		return od_sysdate;
	}
	public void setOd_sysdate(String od_sysdate) {
		this.od_sysdate = od_sysdate;
	}
	public Integer getOd_us_id() {
		return od_us_id;
	}
	public void setOd_us_id(Integer od_us_id) {
		this.od_us_id = od_us_id;
	}
	public Integer getOd_type() {
		return od_type;
	}
	public void setOd_type(Integer od_type) {
		this.od_type = od_type;
	}
	public Integer getOd_isprint() {
		return od_isprint;
	}
	public void setOd_isprint(Integer od_isprint) {
		this.od_isprint = od_isprint;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getAr_describe() {
		return ar_describe;
	}
	public void setAr_describe(String ar_describe) {
		this.ar_describe = ar_describe;
	}
	public String getOd_client_shop_code() {
		return od_client_shop_code;
	}
	public void setOd_client_shop_code(String od_client_shop_code) {
		this.od_client_shop_code = od_client_shop_code;
	}
	public String getClient_shop_name() {
		return client_shop_name;
	}
	public void setClient_shop_name(String client_shop_name) {
		this.client_shop_name = client_shop_name;
	}
}
