package zy.entity.batch.sell;

import java.io.Serializable;

public class T_Batch_Sell implements Serializable{
	private static final long serialVersionUID = 8543312927114249726L;
	private Integer se_id;
	private String se_number;
	private String se_make_date;
	private String se_client_code;
	private String client_name;
	private String se_client_shop_code;
	private String client_shop_name;
	private String se_depot_code;
	private String depot_name;
	private String se_maker;
	private String se_manager;
	private String se_handnumber;
	private Integer se_amount;
	private Double se_money;
	private Double se_retailmoney;
	private Double se_costmoney;
	private Double se_rebatemoney;
	private Double se_discount_money;
	private String se_ba_code;
	private String se_property;
	private String se_remark;
	private Integer se_ar_state;
	private String se_ar_date;
	private Integer se_isdraft;
	private Integer se_pay_state;
	private Double se_receivable;
	private Double se_received;
	private Double se_prepay;
	private String se_order_number;
	private Integer se_type;
	private String se_stream_code;
	private Double se_stream_money;
	private String stream_name;
	private Double se_lastdebt;
	private String se_sysdate;
	private Integer se_us_id;
	private Integer se_isprint;
	private Integer companyid;
	private String ar_describe;
	public Integer getSe_id() {
		return se_id;
	}
	public void setSe_id(Integer se_id) {
		this.se_id = se_id;
	}
	public String getSe_number() {
		return se_number;
	}
	public void setSe_number(String se_number) {
		this.se_number = se_number;
	}
	public String getSe_make_date() {
		return se_make_date;
	}
	public void setSe_make_date(String se_make_date) {
		this.se_make_date = se_make_date;
	}
	public String getSe_client_code() {
		return se_client_code;
	}
	public void setSe_client_code(String se_client_code) {
		this.se_client_code = se_client_code;
	}
	public String getClient_name() {
		return client_name;
	}
	public void setClient_name(String client_name) {
		this.client_name = client_name;
	}
	public String getSe_depot_code() {
		return se_depot_code;
	}
	public void setSe_depot_code(String se_depot_code) {
		this.se_depot_code = se_depot_code;
	}
	public String getDepot_name() {
		return depot_name;
	}
	public void setDepot_name(String depot_name) {
		this.depot_name = depot_name;
	}
	public String getSe_maker() {
		return se_maker;
	}
	public void setSe_maker(String se_maker) {
		this.se_maker = se_maker;
	}
	public String getSe_manager() {
		return se_manager;
	}
	public void setSe_manager(String se_manager) {
		this.se_manager = se_manager;
	}
	public String getSe_handnumber() {
		return se_handnumber;
	}
	public void setSe_handnumber(String se_handnumber) {
		this.se_handnumber = se_handnumber;
	}
	public Integer getSe_amount() {
		return se_amount;
	}
	public void setSe_amount(Integer se_amount) {
		this.se_amount = se_amount;
	}
	public Double getSe_money() {
		return se_money;
	}
	public void setSe_money(Double se_money) {
		this.se_money = se_money;
	}
	public Double getSe_retailmoney() {
		return se_retailmoney;
	}
	public void setSe_retailmoney(Double se_retailmoney) {
		this.se_retailmoney = se_retailmoney;
	}
	public Double getSe_costmoney() {
		return se_costmoney;
	}
	public void setSe_costmoney(Double se_costmoney) {
		this.se_costmoney = se_costmoney;
	}
	public Double getSe_rebatemoney() {
		return se_rebatemoney;
	}
	public void setSe_rebatemoney(Double se_rebatemoney) {
		this.se_rebatemoney = se_rebatemoney;
	}
	public Double getSe_discount_money() {
		return se_discount_money;
	}
	public void setSe_discount_money(Double se_discount_money) {
		this.se_discount_money = se_discount_money;
	}
	public String getSe_ba_code() {
		return se_ba_code;
	}
	public void setSe_ba_code(String se_ba_code) {
		this.se_ba_code = se_ba_code;
	}
	public String getSe_property() {
		return se_property;
	}
	public void setSe_property(String se_property) {
		this.se_property = se_property;
	}
	public String getSe_remark() {
		return se_remark;
	}
	public void setSe_remark(String se_remark) {
		this.se_remark = se_remark;
	}
	public Integer getSe_ar_state() {
		return se_ar_state;
	}
	public void setSe_ar_state(Integer se_ar_state) {
		this.se_ar_state = se_ar_state;
	}
	public String getSe_ar_date() {
		return se_ar_date;
	}
	public void setSe_ar_date(String se_ar_date) {
		this.se_ar_date = se_ar_date;
	}
	public Integer getSe_isdraft() {
		return se_isdraft;
	}
	public void setSe_isdraft(Integer se_isdraft) {
		this.se_isdraft = se_isdraft;
	}
	public Integer getSe_pay_state() {
		return se_pay_state;
	}
	public void setSe_pay_state(Integer se_pay_state) {
		this.se_pay_state = se_pay_state;
	}
	public Double getSe_receivable() {
		return se_receivable;
	}
	public void setSe_receivable(Double se_receivable) {
		this.se_receivable = se_receivable;
	}
	public Double getSe_received() {
		return se_received;
	}
	public void setSe_received(Double se_received) {
		this.se_received = se_received;
	}
	public Double getSe_prepay() {
		return se_prepay;
	}
	public void setSe_prepay(Double se_prepay) {
		this.se_prepay = se_prepay;
	}
	public String getSe_order_number() {
		return se_order_number;
	}
	public void setSe_order_number(String se_order_number) {
		this.se_order_number = se_order_number;
	}
	public Integer getSe_type() {
		return se_type;
	}
	public void setSe_type(Integer se_type) {
		this.se_type = se_type;
	}
	public String getSe_stream_code() {
		return se_stream_code;
	}
	public void setSe_stream_code(String se_stream_code) {
		this.se_stream_code = se_stream_code;
	}
	public Double getSe_stream_money() {
		return se_stream_money;
	}
	public void setSe_stream_money(Double se_stream_money) {
		this.se_stream_money = se_stream_money;
	}
	public String getStream_name() {
		return stream_name;
	}
	public void setStream_name(String stream_name) {
		this.stream_name = stream_name;
	}
	public Double getSe_lastdebt() {
		return se_lastdebt;
	}
	public void setSe_lastdebt(Double se_lastdebt) {
		this.se_lastdebt = se_lastdebt;
	}
	public String getSe_sysdate() {
		return se_sysdate;
	}
	public void setSe_sysdate(String se_sysdate) {
		this.se_sysdate = se_sysdate;
	}
	public Integer getSe_us_id() {
		return se_us_id;
	}
	public void setSe_us_id(Integer se_us_id) {
		this.se_us_id = se_us_id;
	}
	public Integer getSe_isprint() {
		return se_isprint;
	}
	public void setSe_isprint(Integer se_isprint) {
		this.se_isprint = se_isprint;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getAr_describe() {
		return ar_describe;
	}
	public void setAr_describe(String ar_describe) {
		this.ar_describe = ar_describe;
	}
	public String getSe_client_shop_code() {
		return se_client_shop_code;
	}
	public void setSe_client_shop_code(String se_client_shop_code) {
		this.se_client_shop_code = se_client_shop_code;
	}
	public String getClient_shop_name() {
		return client_shop_name;
	}
	public void setClient_shop_name(String client_shop_name) {
		this.client_shop_name = client_shop_name;
	}
}
