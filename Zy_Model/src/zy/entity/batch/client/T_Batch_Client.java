package zy.entity.batch.client;

import java.io.Serializable;

public class T_Batch_Client implements Serializable{
	private static final long serialVersionUID = 8747271642540284845L;
	private Integer ci_id;
	private String ci_code;
	private String ci_name;
	private String ci_spell;
	private String ci_area;
	private String ar_name;
	private Double ci_rate;
	private Double ci_init_debt;
	private Double ci_receivable;
	private Double ci_received;
	private Double ci_prepay;
	private String ci_sp_code;
	private Integer ci_default;
	private String ci_lastdate;
	private Integer ci_batch_cycle;
	private Integer ci_settle_cycle;
	private Integer companyid;
	private String ci_addr;
	private String ci_tel;
	private String ci_man;
	private String ci_mobile;
	private String ci_bank_open;
	private String ci_bank_code;
	private String ci_remark;
	private Double ci_earnest;
	private Double ci_deposit;
	private Integer ci_use_credit;
	private Double ci_credit_limit;
	public Integer getCi_id() {
		return ci_id;
	}
	public void setCi_id(Integer ci_id) {
		this.ci_id = ci_id;
	}
	public String getCi_code() {
		return ci_code;
	}
	public void setCi_code(String ci_code) {
		this.ci_code = ci_code;
	}
	public String getCi_name() {
		return ci_name;
	}
	public void setCi_name(String ci_name) {
		this.ci_name = ci_name;
	}
	public String getCi_spell() {
		return ci_spell;
	}
	public void setCi_spell(String ci_spell) {
		this.ci_spell = ci_spell;
	}
	public String getCi_area() {
		return ci_area;
	}
	public void setCi_area(String ci_area) {
		this.ci_area = ci_area;
	}
	public String getAr_name() {
		return ar_name;
	}
	public void setAr_name(String ar_name) {
		this.ar_name = ar_name;
	}
	public Double getCi_rate() {
		return ci_rate;
	}
	public void setCi_rate(Double ci_rate) {
		this.ci_rate = ci_rate;
	}
	public Double getCi_init_debt() {
		return ci_init_debt;
	}
	public void setCi_init_debt(Double ci_init_debt) {
		this.ci_init_debt = ci_init_debt;
	}
	public Double getCi_receivable() {
		return ci_receivable;
	}
	public void setCi_receivable(Double ci_receivable) {
		this.ci_receivable = ci_receivable;
	}
	public Double getCi_received() {
		return ci_received;
	}
	public void setCi_received(Double ci_received) {
		this.ci_received = ci_received;
	}
	public Double getCi_prepay() {
		return ci_prepay;
	}
	public void setCi_prepay(Double ci_prepay) {
		this.ci_prepay = ci_prepay;
	}
	public String getCi_sp_code() {
		return ci_sp_code;
	}
	public void setCi_sp_code(String ci_sp_code) {
		this.ci_sp_code = ci_sp_code;
	}
	public Integer getCi_default() {
		return ci_default;
	}
	public void setCi_default(Integer ci_default) {
		this.ci_default = ci_default;
	}
	public String getCi_lastdate() {
		return ci_lastdate;
	}
	public void setCi_lastdate(String ci_lastdate) {
		this.ci_lastdate = ci_lastdate;
	}
	public Integer getCi_batch_cycle() {
		return ci_batch_cycle;
	}
	public void setCi_batch_cycle(Integer ci_batch_cycle) {
		this.ci_batch_cycle = ci_batch_cycle;
	}
	public Integer getCi_settle_cycle() {
		return ci_settle_cycle;
	}
	public void setCi_settle_cycle(Integer ci_settle_cycle) {
		this.ci_settle_cycle = ci_settle_cycle;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getCi_addr() {
		return ci_addr;
	}
	public void setCi_addr(String ci_addr) {
		this.ci_addr = ci_addr;
	}
	public String getCi_tel() {
		return ci_tel;
	}
	public void setCi_tel(String ci_tel) {
		this.ci_tel = ci_tel;
	}
	public String getCi_man() {
		return ci_man;
	}
	public void setCi_man(String ci_man) {
		this.ci_man = ci_man;
	}
	public String getCi_mobile() {
		return ci_mobile;
	}
	public void setCi_mobile(String ci_mobile) {
		this.ci_mobile = ci_mobile;
	}
	public String getCi_bank_open() {
		return ci_bank_open;
	}
	public void setCi_bank_open(String ci_bank_open) {
		this.ci_bank_open = ci_bank_open;
	}
	public String getCi_bank_code() {
		return ci_bank_code;
	}
	public void setCi_bank_code(String ci_bank_code) {
		this.ci_bank_code = ci_bank_code;
	}
	public String getCi_remark() {
		return ci_remark;
	}
	public void setCi_remark(String ci_remark) {
		this.ci_remark = ci_remark;
	}
	public Double getCi_earnest() {
		return ci_earnest;
	}
	public void setCi_earnest(Double ci_earnest) {
		this.ci_earnest = ci_earnest;
	}
	public Double getCi_deposit() {
		return ci_deposit;
	}
	public void setCi_deposit(Double ci_deposit) {
		this.ci_deposit = ci_deposit;
	}
	public Integer getCi_use_credit() {
		return ci_use_credit;
	}
	public void setCi_use_credit(Integer ci_use_credit) {
		this.ci_use_credit = ci_use_credit;
	}
	public Double getCi_credit_limit() {
		return ci_credit_limit;
	}
	public void setCi_credit_limit(Double ci_credit_limit) {
		this.ci_credit_limit = ci_credit_limit;
	}
}
