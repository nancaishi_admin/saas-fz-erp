package zy.entity.sys.workbench;

import java.io.Serializable;

public class T_Sys_WorkBench implements Serializable{
	private static final long serialVersionUID = -7269006587415945920L;
	private Integer wb_id;
	private Integer wb_us_id;
	private Integer wb_mn_id;
	private String mn_name;
	private String mn_url;
	private String mn_style;
	private String mn_icon;
	private Integer wb_oper_usid;
	private Integer wb_weight;
	private Integer wb_type;
	private String wb_sysdate;
	private Integer companyid;
	public Integer getWb_id() {
		return wb_id;
	}
	public void setWb_id(Integer wb_id) {
		this.wb_id = wb_id;
	}
	public Integer getWb_us_id() {
		return wb_us_id;
	}
	public void setWb_us_id(Integer wb_us_id) {
		this.wb_us_id = wb_us_id;
	}
	public Integer getWb_mn_id() {
		return wb_mn_id;
	}
	public void setWb_mn_id(Integer wb_mn_id) {
		this.wb_mn_id = wb_mn_id;
	}
	public String getMn_name() {
		return mn_name;
	}
	public void setMn_name(String mn_name) {
		this.mn_name = mn_name;
	}
	public String getMn_url() {
		return mn_url;
	}
	public void setMn_url(String mn_url) {
		this.mn_url = mn_url;
	}
	public String getMn_style() {
		return mn_style;
	}
	public void setMn_style(String mn_style) {
		this.mn_style = mn_style;
	}
	public String getMn_icon() {
		return mn_icon;
	}
	public void setMn_icon(String mn_icon) {
		this.mn_icon = mn_icon;
	}
	public Integer getWb_oper_usid() {
		return wb_oper_usid;
	}
	public void setWb_oper_usid(Integer wb_oper_usid) {
		this.wb_oper_usid = wb_oper_usid;
	}
	public Integer getWb_weight() {
		return wb_weight;
	}
	public void setWb_weight(Integer wb_weight) {
		this.wb_weight = wb_weight;
	}
	public Integer getWb_type() {
		return wb_type;
	}
	public void setWb_type(Integer wb_type) {
		this.wb_type = wb_type;
	}
	public String getWb_sysdate() {
		return wb_sysdate;
	}
	public void setWb_sysdate(String wb_sysdate) {
		this.wb_sysdate = wb_sysdate;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
