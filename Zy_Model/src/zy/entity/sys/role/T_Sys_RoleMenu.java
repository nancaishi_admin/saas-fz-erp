package zy.entity.sys.role;

import java.io.Serializable;

public class T_Sys_RoleMenu implements Serializable{
	private static final long serialVersionUID = 1L;
	private Integer rm_id;
	private String rm_ro_code;
	private String rm_mn_code;
	private String rm_limit;
	private Integer rm_state;
	private Integer companyid;
	public Integer getRm_id() {
		return rm_id;
	}
	public void setRm_id(Integer rm_id) {
		this.rm_id = rm_id;
	}
	public String getRm_ro_code() {
		return rm_ro_code;
	}
	public void setRm_ro_code(String rm_ro_code) {
		this.rm_ro_code = rm_ro_code;
	}
	public String getRm_mn_code() {
		return rm_mn_code;
	}
	public void setRm_mn_code(String rm_mn_code) {
		this.rm_mn_code = rm_mn_code;
	}
	public String getRm_limit() {
		return rm_limit;
	}
	public void setRm_limit(String rm_limit) {
		this.rm_limit = rm_limit;
	}
	public Integer getRm_state() {
		return rm_state;
	}
	public void setRm_state(Integer rm_state) {
		this.rm_state = rm_state;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	
}
