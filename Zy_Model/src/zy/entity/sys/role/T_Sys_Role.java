package zy.entity.sys.role;

import java.io.Serializable;

public class T_Sys_Role implements Serializable{
	private static final long serialVersionUID = 1L;
	private Integer ro_id;
	private String ro_code;
	private String ro_name;
	private Integer ro_shop_type;
	private String ty_name;
	private String ro_shop_code;
	private Integer ro_default;
	private String ro_date;
	private Integer companyid;
	public Integer getRo_id() {
		return ro_id;
	}
	public void setRo_id(Integer ro_id) {
		this.ro_id = ro_id;
	}
	public String getRo_code() {
		return ro_code;
	}
	public void setRo_code(String ro_code) {
		this.ro_code = ro_code;
	}
	public String getRo_name() {
		return ro_name;
	}
	public void setRo_name(String ro_name) {
		this.ro_name = ro_name;
	}
	public String getTy_name() {
		return ty_name;
	}
	public void setTy_name(String ty_name) {
		this.ty_name = ty_name;
	}
	public Integer getRo_shop_type() {
		return ro_shop_type;
	}
	public void setRo_shop_type(Integer ro_shop_type) {
		this.ro_shop_type = ro_shop_type;
	}
	
	public String getRo_shop_code() {
		return ro_shop_code;
	}
	public void setRo_shop_code(String ro_shop_code) {
		this.ro_shop_code = ro_shop_code;
	}
	public Integer getRo_default() {
		return ro_default;
	}
	public void setRo_default(Integer ro_default) {
		this.ro_default = ro_default;
	}
	public String getRo_date() {
		return ro_date;
	}
	public void setRo_date(String ro_date) {
		this.ro_date = ro_date;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	
}
