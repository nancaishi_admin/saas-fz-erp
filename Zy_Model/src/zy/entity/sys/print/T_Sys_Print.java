package zy.entity.sys.print;

import java.io.Serializable;

public class T_Sys_Print implements Serializable{
	private static final long serialVersionUID = 1772527246597545756L;
	private Integer sp_id;
	private String sp_name;
	private Integer sp_type;
	private String sp_remark;
	private String sp_shop_code;
	private String sp_sysdate;
	private Integer sp_us_id;
	private Integer companyid;
	public Integer getSp_id() {
		return sp_id;
	}
	public void setSp_id(Integer sp_id) {
		this.sp_id = sp_id;
	}
	public String getSp_name() {
		return sp_name;
	}
	public void setSp_name(String sp_name) {
		this.sp_name = sp_name;
	}
	public Integer getSp_type() {
		return sp_type;
	}
	public void setSp_type(Integer sp_type) {
		this.sp_type = sp_type;
	}
	public String getSp_remark() {
		return sp_remark;
	}
	public void setSp_remark(String sp_remark) {
		this.sp_remark = sp_remark;
	}
	public String getSp_shop_code() {
		return sp_shop_code;
	}
	public void setSp_shop_code(String sp_shop_code) {
		this.sp_shop_code = sp_shop_code;
	}
	public String getSp_sysdate() {
		return sp_sysdate;
	}
	public void setSp_sysdate(String sp_sysdate) {
		this.sp_sysdate = sp_sysdate;
	}
	public Integer getSp_us_id() {
		return sp_us_id;
	}
	public void setSp_us_id(Integer sp_us_id) {
		this.sp_us_id = sp_us_id;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
