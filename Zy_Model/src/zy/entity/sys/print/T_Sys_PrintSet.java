package zy.entity.sys.print;

import java.io.Serializable;

public class T_Sys_PrintSet implements Serializable{
	private static final long serialVersionUID = 1309729664479855204L;
	private Integer sps_id;
	private Integer sps_sp_id;
	private String sps_code;
	private String sps_name;
	private Integer companyid;
	public Integer getSps_id() {
		return sps_id;
	}
	public void setSps_id(Integer sps_id) {
		this.sps_id = sps_id;
	}
	public Integer getSps_sp_id() {
		return sps_sp_id;
	}
	public void setSps_sp_id(Integer sps_sp_id) {
		this.sps_sp_id = sps_sp_id;
	}
	public String getSps_code() {
		return sps_code;
	}
	public void setSps_code(String sps_code) {
		this.sps_code = sps_code;
	}
	public String getSps_name() {
		return sps_name;
	}
	public void setSps_name(String sps_name) {
		this.sps_name = sps_name;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
