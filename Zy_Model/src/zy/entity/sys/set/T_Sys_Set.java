package zy.entity.sys.set;

import java.io.Serializable;

public class T_Sys_Set implements Serializable{
	private static final long serialVersionUID = 925531035196205361L;
	private Integer st_id;
	private Integer companyid;
	private Integer st_buy_calc_costprice;
	private Integer st_buy_os;
	private Integer st_batch_os;
	private Integer st_batch_showrebates;
	private Integer st_ar_print;
	private Integer st_blank;
	private Integer st_subcode_isrule;
	private String st_subcode_rule;
	private Integer st_allocate_unitprice;
	private Integer st_check_showstock;
	private Integer st_check_showdiffer;
	private Integer st_force_checkstock;
	private Integer st_useable;
	private Integer st_use_upmoney;
	public Integer getSt_id() {
		return st_id;
	}
	public void setSt_id(Integer st_id) {
		this.st_id = st_id;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public Integer getSt_buy_calc_costprice() {
		return st_buy_calc_costprice;
	}
	public void setSt_buy_calc_costprice(Integer st_buy_calc_costprice) {
		this.st_buy_calc_costprice = st_buy_calc_costprice;
	}
	public Integer getSt_buy_os() {
		return st_buy_os;
	}
	public void setSt_buy_os(Integer st_buy_os) {
		this.st_buy_os = st_buy_os;
	}
	public Integer getSt_batch_os() {
		return st_batch_os;
	}
	public void setSt_batch_os(Integer st_batch_os) {
		this.st_batch_os = st_batch_os;
	}
	public Integer getSt_batch_showrebates() {
		return st_batch_showrebates;
	}
	public void setSt_batch_showrebates(Integer st_batch_showrebates) {
		this.st_batch_showrebates = st_batch_showrebates;
	}
	public Integer getSt_ar_print() {
		return st_ar_print;
	}
	public void setSt_ar_print(Integer st_ar_print) {
		this.st_ar_print = st_ar_print;
	}
	public Integer getSt_blank() {
		return st_blank;
	}
	public void setSt_blank(Integer st_blank) {
		this.st_blank = st_blank;
	}
	public Integer getSt_subcode_isrule() {
		return st_subcode_isrule;
	}
	public void setSt_subcode_isrule(Integer st_subcode_isrule) {
		this.st_subcode_isrule = st_subcode_isrule;
	}
	public String getSt_subcode_rule() {
		return st_subcode_rule;
	}
	public void setSt_subcode_rule(String st_subcode_rule) {
		this.st_subcode_rule = st_subcode_rule;
	}
	public Integer getSt_allocate_unitprice() {
		return st_allocate_unitprice;
	}
	public void setSt_allocate_unitprice(Integer st_allocate_unitprice) {
		this.st_allocate_unitprice = st_allocate_unitprice;
	}
	public Integer getSt_check_showstock() {
		return st_check_showstock;
	}
	public void setSt_check_showstock(Integer st_check_showstock) {
		this.st_check_showstock = st_check_showstock;
	}
	public Integer getSt_check_showdiffer() {
		return st_check_showdiffer;
	}
	public void setSt_check_showdiffer(Integer st_check_showdiffer) {
		this.st_check_showdiffer = st_check_showdiffer;
	}
	public Integer getSt_force_checkstock() {
		return st_force_checkstock;
	}
	public void setSt_force_checkstock(Integer st_force_checkstock) {
		this.st_force_checkstock = st_force_checkstock;
	}
	public Integer getSt_useable() {
		return st_useable;
	}
	public void setSt_useable(Integer st_useable) {
		this.st_useable = st_useable;
	}
	public Integer getSt_use_upmoney() {
		return st_use_upmoney;
	}
	public void setSt_use_upmoney(Integer st_use_upmoney) {
		this.st_use_upmoney = st_use_upmoney;
	}
}
