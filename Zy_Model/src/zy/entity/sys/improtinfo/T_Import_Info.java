package zy.entity.sys.improtinfo;

import java.io.Serializable;

public class T_Import_Info implements Serializable{
	private static final long serialVersionUID = -3063155428478877709L;
	private Integer ii_id;
	private String ii_filename;
	private Integer ii_total;
	private Integer ii_success;
	private Integer ii_fail;
	private String ii_error_filename;
	private Integer ii_us_id;
	private String ii_sysdate;
	private String ii_type;
	private Integer companyid;
	public Integer getIi_id() {
		return ii_id;
	}
	public void setIi_id(Integer ii_id) {
		this.ii_id = ii_id;
	}
	public String getIi_filename() {
		return ii_filename;
	}
	public void setIi_filename(String ii_filename) {
		this.ii_filename = ii_filename;
	}
	public Integer getIi_total() {
		return ii_total;
	}
	public void setIi_total(Integer ii_total) {
		this.ii_total = ii_total;
	}
	public Integer getIi_success() {
		return ii_success;
	}
	public void setIi_success(Integer ii_success) {
		this.ii_success = ii_success;
	}
	public Integer getIi_fail() {
		return ii_fail;
	}
	public void setIi_fail(Integer ii_fail) {
		this.ii_fail = ii_fail;
	}
	public String getIi_error_filename() {
		return ii_error_filename;
	}
	public void setIi_error_filename(String ii_error_filename) {
		this.ii_error_filename = ii_error_filename;
	}
	public Integer getIi_us_id() {
		return ii_us_id;
	}
	public void setIi_us_id(Integer ii_us_id) {
		this.ii_us_id = ii_us_id;
	}
	public String getIi_sysdate() {
		return ii_sysdate;
	}
	public void setIi_sysdate(String ii_sysdate) {
		this.ii_sysdate = ii_sysdate;
	}
	public String getIi_type() {
		return ii_type;
	}
	public void setIi_type(String ii_type) {
		this.ii_type = ii_type;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
