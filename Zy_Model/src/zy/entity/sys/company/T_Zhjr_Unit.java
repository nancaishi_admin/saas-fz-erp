package zy.entity.sys.company;

import java.io.Serializable;

public class T_Zhjr_Unit implements Serializable{
	private static final long serialVersionUID = 1L;
	private Integer un_id;
	private String un_name;
	private Integer un_state;
	private String un_man;
	private String un_tel;
	private String un_addr;
	public Integer getUn_id() {
		return un_id;
	}
	public void setUn_id(Integer un_id) {
		this.un_id = un_id;
	}
	public String getUn_name() {
		return un_name;
	}
	public void setUn_name(String un_name) {
		this.un_name = un_name;
	}
	public Integer getUn_state() {
		return un_state;
	}
	public void setUn_state(Integer un_state) {
		this.un_state = un_state;
	}
	public String getUn_man() {
		return un_man;
	}
	public void setUn_man(String un_man) {
		this.un_man = un_man;
	}
	public String getUn_tel() {
		return un_tel;
	}
	public void setUn_tel(String un_tel) {
		this.un_tel = un_tel;
	}
	public String getUn_addr() {
		return un_addr;
	}
	public void setUn_addr(String un_addr) {
		this.un_addr = un_addr;
	}

}
