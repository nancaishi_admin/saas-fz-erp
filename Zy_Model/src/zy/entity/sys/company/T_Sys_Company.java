package zy.entity.sys.company;

import java.io.Serializable;

public class T_Sys_Company implements Serializable{
	private static final long serialVersionUID = 1L;
	private Integer co_id;
	private String co_code;
	private String co_name;
	private Integer co_state;
	private String co_sysdate;
	private String co_man;
	private String co_tel;
	private Integer co_shops;
	private String co_province;
	private String co_city;
	private String co_town;
	private String co_addr;
	private String co_qq;
	private Integer co_type;
	private Integer co_ver;
	private Integer co_users;
	private String co_wechat;
	private String co_salesman;
	private String co_unit;
	public Integer getCo_id() {
		return co_id;
	}
	public void setCo_id(Integer co_id) {
		this.co_id = co_id;
	}
	public String getCo_code() {
		return co_code;
	}
	public void setCo_code(String co_code) {
		this.co_code = co_code;
	}
	public String getCo_name() {
		return co_name;
	}
	public void setCo_name(String co_name) {
		this.co_name = co_name;
	}
	public Integer getCo_state() {
		return co_state;
	}
	public void setCo_state(Integer co_state) {
		this.co_state = co_state;
	}
	public String getCo_sysdate() {
		return co_sysdate;
	}
	public void setCo_sysdate(String co_sysdate) {
		this.co_sysdate = co_sysdate;
	}
	public String getCo_man() {
		return co_man;
	}
	public void setCo_man(String co_man) {
		this.co_man = co_man;
	}
	public String getCo_tel() {
		return co_tel;
	}
	public void setCo_tel(String co_tel) {
		this.co_tel = co_tel;
	}
	public Integer getCo_shops() {
		return co_shops;
	}
	public void setCo_shops(Integer co_shops) {
		this.co_shops = co_shops;
	}
	public String getCo_province() {
		return co_province;
	}
	public void setCo_province(String co_province) {
		this.co_province = co_province;
	}
	public String getCo_city() {
		return co_city;
	}
	public void setCo_city(String co_city) {
		this.co_city = co_city;
	}
	public String getCo_town() {
		return co_town;
	}
	public void setCo_town(String co_town) {
		this.co_town = co_town;
	}
	public String getCo_addr() {
		return co_addr;
	}
	public void setCo_addr(String co_addr) {
		this.co_addr = co_addr;
	}
	public String getCo_qq() {
		return co_qq;
	}
	public void setCo_qq(String co_qq) {
		this.co_qq = co_qq;
	}
	public Integer getCo_type() {
		return co_type;
	}
	public void setCo_type(Integer co_type) {
		this.co_type = co_type;
	}
	public Integer getCo_ver() {
		return co_ver;
	}
	public void setCo_ver(Integer co_ver) {
		this.co_ver = co_ver;
	}
	public Integer getCo_users() {
		return co_users;
	}
	public void setCo_users(Integer co_users) {
		this.co_users = co_users;
	}
	public String getCo_wechat() {
		return co_wechat;
	}
	public void setCo_wechat(String co_wechat) {
		this.co_wechat = co_wechat;
	}
	public String getCo_salesman() {
		return co_salesman;
	}
	public void setCo_salesman(String co_salesman) {
		this.co_salesman = co_salesman;
	}
	public String getCo_unit() {
		return co_unit;
	}
	public void setCo_unit(String co_unit) {
		this.co_unit = co_unit;
	}
	
}
