package zy.entity.vip.membertype;

import java.io.Serializable;

public class T_Vip_MemberType implements Serializable{
	private static final long serialVersionUID = -1897350441353675045L;
	private Integer mt_id;
	private String mt_code;
	private String mt_name;
	private Double mt_discount;
	private Integer mt_isauto_up;
	private Integer mt_min_point;
	private Integer mt_money;
	private Integer mt_point;
	private Integer mt_point_to_money;
	private String mt_shop_upcode;
	private String mt_mark;
	private Integer last_point;
	private Integer total_point;
	private Integer companyid;
	public Integer getMt_id() {
		return mt_id;
	}
	public void setMt_id(Integer mt_id) {
		this.mt_id = mt_id;
	}
	public String getMt_code() {
		return mt_code;
	}
	public void setMt_code(String mt_code) {
		this.mt_code = mt_code;
	}
	public String getMt_name() {
		return mt_name;
	}
	public void setMt_name(String mt_name) {
		this.mt_name = mt_name;
	}
	public Double getMt_discount() {
		return mt_discount;
	}
	public void setMt_discount(Double mt_discount) {
		this.mt_discount = mt_discount;
	}
	public Integer getMt_isauto_up() {
		return mt_isauto_up;
	}
	public void setMt_isauto_up(Integer mt_isauto_up) {
		this.mt_isauto_up = mt_isauto_up;
	}
	public Integer getMt_min_point() {
		return mt_min_point;
	}
	public void setMt_min_point(Integer mt_min_point) {
		this.mt_min_point = mt_min_point;
	}
	public Integer getMt_money() {
		return mt_money;
	}
	public void setMt_money(Integer mt_money) {
		this.mt_money = mt_money;
	}
	public Integer getMt_point() {
		return mt_point;
	}
	public void setMt_point(Integer mt_point) {
		this.mt_point = mt_point;
	}
	public Integer getMt_point_to_money() {
		return mt_point_to_money;
	}
	public void setMt_point_to_money(Integer mt_point_to_money) {
		this.mt_point_to_money = mt_point_to_money;
	}
	public String getMt_shop_upcode() {
		return mt_shop_upcode;
	}
	public void setMt_shop_upcode(String mt_shop_upcode) {
		this.mt_shop_upcode = mt_shop_upcode;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public Integer getLast_point() {
		return last_point;
	}
	public void setLast_point(Integer last_point) {
		this.last_point = last_point;
	}
	public Integer getTotal_point() {
		return total_point;
	}
	public void setTotal_point(Integer total_point) {
		this.total_point = total_point;
	}
	public String getMt_mark() {
		return mt_mark;
	}
	public void setMt_mark(String mt_mark) {
		this.mt_mark = mt_mark;
	}
}
