package zy.entity.vip.member;

import java.io.Serializable;

/**
 * 会员类别报表实体类
 *
 */
public class T_Vip_TypeAnalysis implements Serializable{
	private static final long serialVersionUID = 7984068804508490205L;
	private Integer id;
	private String mt_code;
	private String mt_name;
	private Integer vip_count;//会员人数
	private Integer consumer_count;//消费人数
	private Double vip_count_rate;//人数占比
	private Double consumer_vip_rate;//消费人数占比
	private Double rebate_monery_sum;//消费金额
	private Double monery_rate;//金额占比
	private Double cost_monery_sum;//成本
	private Double profits_sum;//利润
	private Double profits_rate;//利润占比
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMt_code() {
		return mt_code;
	}
	public void setMt_code(String mt_code) {
		this.mt_code = mt_code;
	}
	public String getMt_name() {
		return mt_name;
	}
	public void setMt_name(String mt_name) {
		this.mt_name = mt_name;
	}
	public Integer getVip_count() {
		return vip_count;
	}
	public void setVip_count(Integer vip_count) {
		this.vip_count = vip_count;
	}
	public Integer getConsumer_count() {
		return consumer_count;
	}
	public void setConsumer_count(Integer consumer_count) {
		this.consumer_count = consumer_count;
	}
	public Double getVip_count_rate() {
		return vip_count_rate;
	}
	public void setVip_count_rate(Double vip_count_rate) {
		this.vip_count_rate = vip_count_rate;
	}
	public Double getConsumer_vip_rate() {
		return consumer_vip_rate;
	}
	public void setConsumer_vip_rate(Double consumer_vip_rate) {
		this.consumer_vip_rate = consumer_vip_rate;
	}
	public Double getRebate_monery_sum() {
		return rebate_monery_sum;
	}
	public void setRebate_monery_sum(Double rebate_monery_sum) {
		this.rebate_monery_sum = rebate_monery_sum;
	}
	public Double getMonery_rate() {
		return monery_rate;
	}
	public void setMonery_rate(Double monery_rate) {
		this.monery_rate = monery_rate;
	}
	public Double getCost_monery_sum() {
		return cost_monery_sum;
	}
	public void setCost_monery_sum(Double cost_monery_sum) {
		this.cost_monery_sum = cost_monery_sum;
	}
	public Double getProfits_sum() {
		return profits_sum;
	}
	public void setProfits_sum(Double profits_sum) {
		this.profits_sum = profits_sum;
	}
	public Double getProfits_rate() {
		return profits_rate;
	}
	public void setProfits_rate(Double profits_rate) {
		this.profits_rate = profits_rate;
	}
}
