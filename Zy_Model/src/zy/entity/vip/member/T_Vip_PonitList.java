package zy.entity.vip.member;

import java.io.Serializable;

public class T_Vip_PonitList implements Serializable{
	private static final long serialVersionUID = 1L;
	private Integer vpl_id;
	private String vpl_vm_code;
	private Double vpl_point;
	private Integer vpl_type;
	private String dt_name;
	private String vpl_remark;
	private String vpl_date;
	private String vpl_shop_name;
	private String vpl_manager;
	private Integer companyid;
	public Integer getVpl_id() {
		return vpl_id;
	}
	public void setVpl_id(Integer vpl_id) {
		this.vpl_id = vpl_id;
	}
	public Double getVpl_point() {
		return vpl_point;
	}
	public void setVpl_point(Double vpl_point) {
		this.vpl_point = vpl_point;
	}
	public Integer getVpl_type() {
		return vpl_type;
	}
	public void setVpl_type(Integer vpl_type) {
		this.vpl_type = vpl_type;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getVpl_vm_code() {
		return vpl_vm_code;
	}
	public void setVpl_vm_code(String vpl_vm_code) {
		this.vpl_vm_code = vpl_vm_code;
	}
	public String getVpl_remark() {
		return vpl_remark;
	}
	public void setVpl_remark(String vpl_remark) {
		this.vpl_remark = vpl_remark;
	}
	public String getVpl_date() {
		return vpl_date;
	}
	public void setVpl_date(String vpl_date) {
		this.vpl_date = vpl_date;
	}
	public String getVpl_shop_name() {
		return vpl_shop_name;
	}
	public void setVpl_shop_name(String vpl_shop_name) {
		this.vpl_shop_name = vpl_shop_name;
	}
	public String getVpl_manager() {
		return vpl_manager;
	}
	public void setVpl_manager(String vpl_manager) {
		this.vpl_manager = vpl_manager;
	}
	public String getDt_name() {
		return dt_name;
	}
	public void setDt_name(String dt_name) {
		this.dt_name = dt_name;
	}

}
