package zy.entity.vip.member;

import java.io.Serializable;

/**
 * 品牌消费报表
 *
 */
public class T_Vip_BrandAnalysis implements Serializable{
	private static final long serialVersionUID = 7984068804508490205L;
	private Integer id;
	private String bd_code;
	private String bd_name;
	private Integer count_sum;
	private Double count_sum_rate;
	private Double money_sum;
	private Double money_sum_rate;
	private Double cost_monery_sum;
	private Double profits_sum;
	private Double profits_sum_rate;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getBd_code() {
		return bd_code;
	}
	public void setBd_code(String bd_code) {
		this.bd_code = bd_code;
	}
	public String getBd_name() {
		return bd_name;
	}
	public void setBd_name(String bd_name) {
		this.bd_name = bd_name;
	}
	public Integer getCount_sum() {
		return count_sum;
	}
	public void setCount_sum(Integer count_sum) {
		this.count_sum = count_sum;
	}
	public Double getCount_sum_rate() {
		return count_sum_rate;
	}
	public void setCount_sum_rate(Double count_sum_rate) {
		this.count_sum_rate = count_sum_rate;
	}
	public Double getMoney_sum() {
		return money_sum;
	}
	public void setMoney_sum(Double money_sum) {
		this.money_sum = money_sum;
	}
	public Double getMoney_sum_rate() {
		return money_sum_rate;
	}
	public void setMoney_sum_rate(Double money_sum_rate) {
		this.money_sum_rate = money_sum_rate;
	}
	public Double getCost_monery_sum() {
		return cost_monery_sum;
	}
	public void setCost_monery_sum(Double cost_monery_sum) {
		this.cost_monery_sum = cost_monery_sum;
	}
	public Double getProfits_sum() {
		return profits_sum;
	}
	public void setProfits_sum(Double profits_sum) {
		this.profits_sum = profits_sum;
	}
	public Double getProfits_sum_rate() {
		return profits_sum_rate;
	}
	public void setProfits_sum_rate(Double profits_sum_rate) {
		this.profits_sum_rate = profits_sum_rate;
	}
}
