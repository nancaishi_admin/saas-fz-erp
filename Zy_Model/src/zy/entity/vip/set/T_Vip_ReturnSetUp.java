package zy.entity.vip.set;

import java.io.Serializable;

public class T_Vip_ReturnSetUp implements Serializable{
	private static final long serialVersionUID = -5925841660661293379L;
	private Integer rts_id;
	private Integer rts_day;
	private Integer rts_second;
	private String rts_name;
	private String rts_shop_code;
	private Integer companyid;
	public Integer getRts_id() {
		return rts_id;
	}
	public void setRts_id(Integer rts_id) {
		this.rts_id = rts_id;
	}
	public Integer getRts_day() {
		return rts_day;
	}
	public void setRts_day(Integer rts_day) {
		this.rts_day = rts_day;
	}
	public Integer getRts_second() {
		return rts_second;
	}
	public void setRts_second(Integer rts_second) {
		this.rts_second = rts_second;
	}
	public String getRts_name() {
		return rts_name;
	}
	public void setRts_name(String rts_name) {
		this.rts_name = rts_name;
	}
	public String getRts_shop_code() {
		return rts_shop_code;
	}
	public void setRts_shop_code(String rts_shop_code) {
		this.rts_shop_code = rts_shop_code;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
