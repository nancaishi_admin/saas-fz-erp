package zy.entity;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *  on 2016/5/27.
 */
public class TableToModel {
    private String packageOutPath = "zy";//指定实体生成所在包的路径
    private String authorName = "YanXue";//作者名字
    private String tableName = "user";//表名
    private String[] colNames; // 列名数组
    private String[] colTypes; //列名类型数组
    private int[] colSizes; //列名大小数组
    private String changeTableNameStr = "";//驼峰转换后的类名
    private boolean f_util = false; // 是否需要导入包java.util.*
    private boolean f_sql = false; // 是否需要导入包java.sql.*
    private boolean f_decimal = false;//是否需要导入java.math.BigDecimal

    //数据库连接
    private static final String URL ="jdbc:mysql://116.62.144.91:3306/zhs_data";
    private static final String NAME = "developer";
    private static final String PASS = "Zhs@1230";
    private static final String DRIVER ="com.mysql.jdbc.Driver";

    /*
	 * 构造函数
	 */
    public TableToModel(){

    }

    public void genTableToCode(){
        //创建连接
        Connection con = null;
        //查要生成实体类的表
        String sql = "SELECT * FROM " + tableName;
        PreparedStatement pStemt = null;
        try {
            Class.forName(DRIVER);
            con = DriverManager.getConnection(URL,NAME,PASS);
            pStemt = con.prepareStatement(sql);
            ResultSetMetaData rsmd = pStemt.getMetaData();
            int size = rsmd.getColumnCount();	//统计列
            colNames = new String[size];
            colTypes = new String[size];
            colSizes = new int[size];
            for (int i = 0; i < size; i++) {
                String columnName = rsmd.getColumnName(i+1);
                if(!"CREATED_BY".equals(columnName) && !"CREATION_DATE".equals(columnName)
                        && !"LAST_UPDATED_BY".equals(columnName) && !"LAST_UPDATE_DATE".equals(columnName)){
                    //将字符串转换为驼峰样式
                   /* String [] columnNameStr =columnName.toLowerCase().split("_");
                    columnName = "";
                    for(String column : columnNameStr){
                        if(columnName.length() == 0){
                            columnName += column;
                        }else {
                            columnName += initCap(column);
                        }
                    }*/
                	System.out.println(i+":"+columnName);
                    colNames[i] = columnName;
                    colTypes[i] = rsmd.getColumnTypeName(i + 1);

                    if(colTypes[i].equalsIgnoreCase("datetime")){
                        f_util = true;
                    }
                    if(colTypes[i].equalsIgnoreCase("image") || colTypes[i].equalsIgnoreCase("text")){
                        f_sql = true;
                    }
                    if(colTypes[i].equalsIgnoreCase("decimal")){
                        f_decimal = true;
                    }
                    colSizes[i] = rsmd.getColumnDisplaySize(i + 1);
                }
            }

            String content = parse(colNames,colTypes,colSizes);
            File directory = new File("");
            //System.out.println("绝对路径："+directory.getAbsolutePath());
            //System.out.println("相对路径："+directory.getCanonicalPath());

            String outputPath = directory.getAbsolutePath()+ "\\src\\"+this.packageOutPath.replace(".", "\\")+"\\"+changeTableNameStr  + ".java";
            File file = new File(outputPath);
            if(!file.exists()){
                file = new File(directory.getAbsolutePath()+ "\\src\\"+this.packageOutPath.replace(".", "\\"),changeTableNameStr  + ".java");
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(outputPath);
            PrintWriter pw = new PrintWriter(fw);
            pw.println(content);
            pw.flush();
            pw.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e1) {
            e1.printStackTrace();
        } catch (SQLException e1) {
            e1.printStackTrace();
        }finally{
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 传参的方式生成表对应的实体类
     * @param tableName
     */
    public void genTableToCode(String tableName){
        this.tableName = tableName;
        genTableToCode();
    }
    private String buildTableName(String tableName){
    	String table_name = "";
    	 String [] tableNameStr =tableName.toLowerCase().split("_");
    	 int i = 0;
         for(String table : tableNameStr){
        	 if(i == 0){
        		 table_name = initCap(table);
        	 }else{
        		 table_name = table_name+"_"+initCap(table);
        	 }
        	 i++;
         } 
         return table_name;
    }
    /**
     * 功能：生成实体类主体代码
     * @param colNames
     * @param colTypes
     * @param colSizes
     * @return
     */
    private String parse(String[] colNames, String[] colTypes, int[] colSizes) {
        StringBuffer sb = new StringBuffer();
        //将表名字符串转换为驼峰样式
//        String [] tableNameStr =tableName;
        changeTableNameStr = buildTableName(tableName);
//        for(String table : tableNameStr){
//           changeTableNameStr += initCap(table);
//        }  
        sb.append("package " + this.packageOutPath + ";\r\n");
        sb.append("\r\n");

        //判断是否导入工具包
        if(f_util){
            sb.append("import java.util.Date;\r\n");
        }
        if(f_sql){
            sb.append("import java.sql.*;\r\n");
        }
        if(f_decimal){
            sb.append("import java.math.BigDecimal;\r\n");
        }
        sb.append("import java.io.Serializable;\r\n");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        sb.append("\r\n");
        //注释部分
        sb.append("/**\r\n");
        sb.append("* Created by "+this.authorName+" on "+df.format(new java.util.Date())+"\r\n");
        sb.append("*@Description "+tableName+" 实体类\r\n");
        sb.append("*/ \r\n");

        //实体部分
        sb.append("\r\n\r\npublic class " + buildTableName(tableName)+ " implements Serializable" + "{\r\n");
        processAllAttrs(sb);//属性
        processAllMethod(sb);//get set方法
        sb.append("}\r\n");

        return sb.toString();
    }

    /**
     * 功能：生成所有属性
     * @param sb
     */
    private void processAllAttrs(StringBuffer sb) {
        for (int i = 0; i < colNames.length; i++) {
            sb.append("\tprivate " + sqlType2JavaType(colTypes[i]) + " " + colNames[i] + ";\r\n");
        }

    }

    /**
     * 功能：生成所有方法
     * @param sb
     */
    private void processAllMethod(StringBuffer sb) {
        for (int i = 0; i < colNames.length; i++) {
            sb.append("\tpublic void set" + initCap(colNames[i]) + "(" + sqlType2JavaType(colTypes[i]) + " " +
                    colNames[i] + "){\r\n");
            sb.append("\t\tthis." + colNames[i] + "=" + colNames[i] + ";\r\n");
            sb.append("\t}\r\n");
            sb.append("\tpublic " + sqlType2JavaType(colTypes[i]) + " get" + initCap(colNames[i]) + "(){\r\n");
            sb.append("\t\treturn " + colNames[i] + ";\r\n");
            sb.append("\t}\r\n");
        }

    }

    /**
     * 功能：将输入字符串的首字母改成大写
     * @param str
     * @return
     */
    private String initCap(String str) {
        char[] ch = str.toCharArray();
        if(ch[0] >= 'a' && ch[0] <= 'z'){
            ch[0] = (char)(ch[0] - 32);
        }
        return new String(ch);
    }

    /**
     * 功能：获得列的数据类型
     * @param sqlType
     * @return
     */
    private String sqlType2JavaType(String sqlType) {
    	sqlType = sqlType.trim().toLowerCase();
    	if(sqlType.contains("unsigned")){
    		sqlType = sqlType.replace("unsigned","");
    	}
    	sqlType = sqlType.trim();
        if(sqlType.equalsIgnoreCase("bit")){
            return "boolean";
        }else if(sqlType.equalsIgnoreCase("tinyint")){
            return "Integer";
        }else if(sqlType.equalsIgnoreCase("smallint")){
            return "Integer";
        }else if(sqlType.equalsIgnoreCase("int")){
            return "Integer";
        }else if(sqlType.equalsIgnoreCase("bigint")){
            return "Long";
        }else if(sqlType.equalsIgnoreCase("float") || sqlType.equalsIgnoreCase("double") || sqlType.equalsIgnoreCase("decimal")){
            return "Double";
        }else if(sqlType.equalsIgnoreCase("numeric")
                || sqlType.equalsIgnoreCase("real") || sqlType.equalsIgnoreCase("money")
                || sqlType.equalsIgnoreCase("smallmoney")){
            return "Double";
        }else if(sqlType.equalsIgnoreCase("varchar") || sqlType.equalsIgnoreCase("char")
                || sqlType.equalsIgnoreCase("nvarchar") || sqlType.equalsIgnoreCase("nchar")
                || sqlType.equalsIgnoreCase("text") || sqlType.equalsIgnoreCase("json")){
            return "String";
        }else if(sqlType.equalsIgnoreCase("datetime") || sqlType.equalsIgnoreCase("date")){
            return "Date";
        }else if(sqlType.equalsIgnoreCase("image")){
            return "Blod";
        }

        return null;
    }


    public static void main(String[] args) {
        TableToModel genTableToCode =  new TableToModel();
        List<String> tableNames = new ArrayList<String>();
        tableNames.add("t_wx_order");
//        tableNames.add("t_wx_order_detail");
//        tableNames.add("t_wx_comment");
        for(String tableName : tableNames){
            genTableToCode.genTableToCode(tableName);
        }
    }

}
