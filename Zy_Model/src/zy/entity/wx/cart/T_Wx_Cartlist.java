package zy.entity.wx.cart;

import java.util.Date;
import java.math.BigDecimal;
import java.io.Serializable;

/**
* Created by YanXue on 2018-05-21
*@Description t_wx_cartlist 瀹炰綋绫�
*/ 


public class T_Wx_Cartlist implements Serializable{
	private Integer id;
	private Date sysdate;
	private String pd_code;
	private String pd_pic;
	private String pd_name;
	private String sub_code;
	private String cr_code;
	private String sz_code;
	private String br_code;
	private String sub_name;
	private String cr_name;
	private String sz_name;
	private String br_name;
	private Integer amount;
	private Double price;
	private Integer companyid;
	private String wu_code;
	public void setId(Integer id){
		this.id=id;
	}
	public Integer getId(){
		return id;
	}
	public void setSysdate(Date sysdate){
		this.sysdate=sysdate;
	}
	public Date getSysdate(){
		return sysdate;
	}
	public void setPd_code(String pd_code){
		this.pd_code=pd_code;
	}
	public String getPd_code(){
		return pd_code;
	}
	public void setSub_code(String sub_code){
		this.sub_code=sub_code;
	}
	public String getSub_code(){
		return sub_code;
	}
	public void setCr_code(String cr_code){
		this.cr_code=cr_code;
	}
	public String getCr_code(){
		return cr_code;
	}
	public void setSz_code(String sz_code){
		this.sz_code=sz_code;
	}
	public String getSz_code(){
		return sz_code;
	}
	public void setBr_code(String br_code){
		this.br_code=br_code;
	}
	public String getBr_code(){
		return br_code;
	}
	public void setPrice(Double price){
		this.price=price;
	}
	public Double getPrice(){
		return price;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	public void setCompanyid(Integer companyid){
		this.companyid=companyid;
	}
	public Integer getCompanyid(){
		return companyid;
	}
	public void setWu_code(String wu_code){
		this.wu_code=wu_code;
	}
	public String getWu_code(){
		return wu_code;
	}
	public String getPd_name() {
		return pd_name;
	}
	public void setPd_name(String pd_name) {
		this.pd_name = pd_name;
	}
//	public String getSkuName() {
//		return skuName;
//	}
//	public void setSkuName(String skuName) {
//		this.skuName = skuName;
//	}
	public String getCr_name() {
		return cr_name;
	}
	public String getSub_name() {
		return sub_name;
	}
	public void setSub_name(String sub_name) {
		this.sub_name = sub_name;
	}
	public void setCr_name(String cr_name) {
		this.cr_name = cr_name;
	}
	public String getSz_name() {
		return sz_name;
	}
	public void setSz_name(String sz_name) {
		this.sz_name = sz_name;
	}
	public String getBr_name() {
		return br_name;
	}
	public void setBr_name(String br_name) {
		this.br_name = br_name;
	}
	public String getPd_pic() {
		return pd_pic;
	}
	public void setPd_pic(String pd_pic) {
		this.pd_pic = pd_pic;
	}
	
}

