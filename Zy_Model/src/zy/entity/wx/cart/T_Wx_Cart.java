package zy.entity.wx.cart;

import java.util.Date;
import java.math.BigDecimal;
import java.io.Serializable;

/**
* Created by YanXue on 2018-05-11
*@Description t_wx_cart 实体类
*/ 


public class T_Wx_Cart implements Serializable{
	private Integer id;
	private String wx_user_code;
	private String shop_code;
	private Date sysdate;
	private Integer mount;
	private Double money;
	private Integer companyid;
	public void setId(Integer id){
		this.id=id;
	}
	public Integer getId(){
		return id;
	}
	public void setWx_user_code(String wx_user_code){
		this.wx_user_code=wx_user_code;
	}
	public String getWx_user_code(){
		return wx_user_code;
	}
	public void setShop_code(String shop_code){
		this.shop_code=shop_code;
	}
	public String getShop_code(){
		return shop_code;
	}
	public void setSysdate(Date sysdate){
		this.sysdate=sysdate;
	}
	public Date getSysdate(){
		return sysdate;
	}
	public void setMount(Integer mount){
		this.mount=mount;
	}
	public Integer getMount(){
		return mount;
	}
	public void setMoney(Double money){
		this.money=money;
	}
	public Double getMoney(){
		return money;
	}
	public void setCompanyid(Integer companyid){
		this.companyid=companyid;
	}
	public Integer getCompanyid(){
		return companyid;
	}
}

