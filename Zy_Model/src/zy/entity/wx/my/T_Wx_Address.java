package zy.entity.wx.my;

import java.io.Serializable;

public class T_Wx_Address implements Serializable{
	private static final long serialVersionUID = -545992459994481942L;
	private Integer wd_id;
	private String wd_code;
	private String wd_user_code;
	private String wd_name;
	private String wd_mobile;
	private String wd_province;
	private String wd_city;
	private String wd_area;
	private String wd_town;
	private String wd_addr;
	private String wd_zipcode;
	private Integer wd_state;
	private String wd_sysdate;
	private String wd_x;
	private String wd_y;
	public Integer getWd_id() {
		return wd_id;
	}
	public void setWd_id(Integer wd_id) {
		this.wd_id = wd_id;
	}
	public String getWd_code() {
		return wd_code;
	}
	public void setWd_code(String wd_code) {
		this.wd_code = wd_code;
	}
	public String getWd_user_code() {
		return wd_user_code;
	}
	public void setWd_user_code(String wd_user_code) {
		this.wd_user_code = wd_user_code;
	}
	public String getWd_name() {
		return wd_name;
	}
	public void setWd_name(String wd_name) {
		this.wd_name = wd_name;
	}
	public String getWd_mobile() {
		return wd_mobile;
	}
	public void setWd_mobile(String wd_mobile) {
		this.wd_mobile = wd_mobile;
	}
	public String getWd_province() {
		return wd_province;
	}
	public void setWd_province(String wd_province) {
		this.wd_province = wd_province;
	}
	public String getWd_city() {
		return wd_city;
	}
	public void setWd_city(String wd_city) {
		this.wd_city = wd_city;
	}
	public String getWd_area() {
		return wd_area;
	}
	public void setWd_area(String wd_area) {
		this.wd_area = wd_area;
	}
	public String getWd_town() {
		return wd_town;
	}
	public void setWd_town(String wd_town) {
		this.wd_town = wd_town;
	}
	public String getWd_addr() {
		return wd_addr;
	}
	public void setWd_addr(String wd_addr) {
		this.wd_addr = wd_addr;
	}
	public String getWd_zipcode() {
		return wd_zipcode;
	}
	public void setWd_zipcode(String wd_zipcode) {
		this.wd_zipcode = wd_zipcode;
	}
	public Integer getWd_state() {
		return wd_state;
	}
	public void setWd_state(Integer wd_state) {
		this.wd_state = wd_state;
	}
	public String getWd_sysdate() {
		return wd_sysdate;
	}
	public void setWd_sysdate(String wd_sysdate) {
		this.wd_sysdate = wd_sysdate;
	}
	public String getWd_x() {
		return wd_x;
	}
	public void setWd_x(String wd_x) {
		this.wd_x = wd_x;
	}
	public String getWd_y() {
		return wd_y;
	}
	public void setWd_y(String wd_y) {
		this.wd_y = wd_y;
	}
}
