package zy.entity.wx.my;

import java.util.Date;
import java.io.Serializable;

/**
* Created by YanXue on 2018-06-27
*@Description t_wx_comment 实体类
*/ 


public class T_Wx_Comment implements Serializable{
	private Integer id;
	private String content;
	private Date createdate;
	private Integer star;
	private String pic1;
	private String pic2;
	private String pic3;
	private String pic4;
	private String nikname;
	private String avatarurl;
	private String wu_code;
	private String pd_code;
	private String shop_code;
	private Integer companyid;
	private String type;
	public void setId(Integer id){
		this.id=id;
	}
	public Integer getId(){
		return id;
	}
	public void setContent(String content){
		this.content=content;
	}
	public String getContent(){
		return content;
	}
	public void setCreatedate(Date createdate){
		this.createdate=createdate;
	}
	public Date getCreatedate(){
		return createdate;
	}
	public void setStar(Integer star){
		this.star=star;
	}
	public Integer getStar(){
		return star;
	}
	public void setPic1(String pic1){
		this.pic1=pic1;
	}
	public String getPic1(){
		return pic1;
	}
	public void setPic2(String pic2){
		this.pic2=pic2;
	}
	public String getPic2(){
		return pic2;
	}
	public void setPic3(String pic3){
		this.pic3=pic3;
	}
	public String getPic3(){
		return pic3;
	}
	public void setPic4(String pic4){
		this.pic4=pic4;
	}
	public String getPic4(){
		return pic4;
	}
	public void setNikname(String nikname){
		this.nikname=nikname;
	}
	public String getNikname(){
		return nikname;
	}
	public void setAvatarurl(String avatarurl){
		this.avatarurl=avatarurl;
	}
	public String getAvatarurl(){
		return avatarurl;
	}
	public void setWu_code(String wu_code){
		this.wu_code=wu_code;
	}
	public String getWu_code(){
		return wu_code;
	}
	public void setPd_code(String pd_code){
		this.pd_code=pd_code;
	}
	public String getPd_code(){
		return pd_code;
	}
	public void setShop_code(String shop_code){
		this.shop_code=shop_code;
	}
	public String getShop_code(){
		return shop_code;
	}
	public void setCompanyid(Integer companyid){
		this.companyid=companyid;
	}
	public Integer getCompanyid(){
		return companyid;
	}
	public void setType(String type){
		this.type=type;
	}
	public String getType(){
		return type;
	}
}

