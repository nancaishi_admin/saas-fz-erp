package zy.entity.wx.product;

import zy.entity.base.product.T_Base_Product;

public class T_Wx_Product extends T_Base_Product{
	private static final long serialVersionUID = -8291613398382444018L;
	private Integer wp_id;
	private String wp_number;
	private String wp_shop_code;
	private String wp_pd_code;
	private String wp_pd_name;
	private Double wp_sell_price;
	private Double wp_rate_price;
	private Integer wp_type;
	private Integer wp_state;
	private String wp_pt_code;
	private String wp_pt_name;
	private Integer wp_istry;
	private String wp_try_way;
	private Integer wp_isbuy;
	private String wp_buy_way;
	private Integer wp_postfree;
	private String wp_subtitle;
	private Integer wp_weight;
	private Integer wp_sell_amount;
	private Integer wp_virtual_amount;
	private Integer wp_buy_limit;
	private Integer wp_browse_count;
	private Integer wp_likecount;
	private String wp_sysdate;
	private String wp_desc;
	private Integer companyid;
	private String wp_property_code;
	private String wp_property_name;
	private String wp_promise_code;
	private String wp_promise_name;
	private String shop_codes;
	private String shop_names;
	public Integer getWp_id() {
		return wp_id;
	}
	public void setWp_id(Integer wp_id) {
		this.wp_id = wp_id;
	}
	public String getWp_number() {
		return wp_number;
	}
	public void setWp_number(String wp_number) {
		this.wp_number = wp_number;
	}
	public String getWp_shop_code() {
		return wp_shop_code;
	}
	public void setWp_shop_code(String wp_shop_code) {
		this.wp_shop_code = wp_shop_code;
	}
	public String getWp_pd_code() {
		return wp_pd_code;
	}
	public void setWp_pd_code(String wp_pd_code) {
		this.wp_pd_code = wp_pd_code;
	}
	public String getWp_pd_name() {
		return wp_pd_name;
	}
	public void setWp_pd_name(String wp_pd_name) {
		this.wp_pd_name = wp_pd_name;
	}
	public Double getWp_sell_price() {
		return wp_sell_price;
	}
	public void setWp_sell_price(Double wp_sell_price) {
		this.wp_sell_price = wp_sell_price;
	}
	public Double getWp_rate_price() {
		return wp_rate_price;
	}
	public void setWp_rate_price(Double wp_rate_price) {
		this.wp_rate_price = wp_rate_price;
	}
	public Integer getWp_type() {
		return wp_type;
	}
	public void setWp_type(Integer wp_type) {
		this.wp_type = wp_type;
	}
	public Integer getWp_state() {
		return wp_state;
	}
	public void setWp_state(Integer wp_state) {
		this.wp_state = wp_state;
	}
	public String getWp_pt_code() {
		return wp_pt_code;
	}
	public void setWp_pt_code(String wp_pt_code) {
		this.wp_pt_code = wp_pt_code;
	}
	public String getWp_pt_name() {
		return wp_pt_name;
	}
	public void setWp_pt_name(String wp_pt_name) {
		this.wp_pt_name = wp_pt_name;
	}
	public Integer getWp_istry() {
		return wp_istry;
	}
	public void setWp_istry(Integer wp_istry) {
		this.wp_istry = wp_istry;
	}
	public String getWp_try_way() {
		return wp_try_way;
	}
	public void setWp_try_way(String wp_try_way) {
		this.wp_try_way = wp_try_way;
	}
	public Integer getWp_isbuy() {
		return wp_isbuy;
	}
	public void setWp_isbuy(Integer wp_isbuy) {
		this.wp_isbuy = wp_isbuy;
	}
	public String getWp_buy_way() {
		return wp_buy_way;
	}
	public void setWp_buy_way(String wp_buy_way) {
		this.wp_buy_way = wp_buy_way;
	}
	public Integer getWp_postfree() {
		return wp_postfree;
	}
	public void setWp_postfree(Integer wp_postfree) {
		this.wp_postfree = wp_postfree;
	}
	public String getWp_subtitle() {
		return wp_subtitle;
	}
	public void setWp_subtitle(String wp_subtitle) {
		this.wp_subtitle = wp_subtitle;
	}
	public Integer getWp_weight() {
		return wp_weight;
	}
	public void setWp_weight(Integer wp_weight) {
		this.wp_weight = wp_weight;
	}
	public Integer getWp_sell_amount() {
		return wp_sell_amount;
	}
	public void setWp_sell_amount(Integer wp_sell_amount) {
		this.wp_sell_amount = wp_sell_amount;
	}
	public Integer getWp_virtual_amount() {
		return wp_virtual_amount;
	}
	public void setWp_virtual_amount(Integer wp_virtual_amount) {
		this.wp_virtual_amount = wp_virtual_amount;
	}
	public Integer getWp_buy_limit() {
		return wp_buy_limit;
	}
	public void setWp_buy_limit(Integer wp_buy_limit) {
		this.wp_buy_limit = wp_buy_limit;
	}
	public Integer getWp_browse_count() {
		return wp_browse_count;
	}
	public void setWp_browse_count(Integer wp_browse_count) {
		this.wp_browse_count = wp_browse_count;
	}
	public Integer getWp_likecount() {
		return wp_likecount;
	}
	public void setWp_likecount(Integer wp_likecount) {
		this.wp_likecount = wp_likecount;
	}
	public String getWp_sysdate() {
		return wp_sysdate;
	}
	public void setWp_sysdate(String wp_sysdate) {
		this.wp_sysdate = wp_sysdate;
	}
	public String getWp_desc() {
		return wp_desc;
	}
	public void setWp_desc(String wp_desc) {
		this.wp_desc = wp_desc;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getWp_promise_code() {
		return wp_promise_code;
	}
	public void setWp_promise_code(String wp_promise_code) {
		this.wp_promise_code = wp_promise_code;
	}
	public String getWp_promise_name() {
		return wp_promise_name;
	}
	public void setWp_promise_name(String wp_promise_name) {
		this.wp_promise_name = wp_promise_name;
	}
	public String getWp_property_code() {
		return wp_property_code;
	}
	public void setWp_property_code(String wp_property_code) {
		this.wp_property_code = wp_property_code;
	}
	public String getWp_property_name() {
		return wp_property_name;
	}
	public void setWp_property_name(String wp_property_name) {
		this.wp_property_name = wp_property_name;
	}
	public String getShop_codes() {
		return shop_codes;
	}
	public void setShop_codes(String shop_codes) {
		this.shop_codes = shop_codes;
	}
	public String getShop_names() {
		return shop_names;
	}
	public void setShop_names(String shop_names) {
		this.shop_names = shop_names;
	}
}
