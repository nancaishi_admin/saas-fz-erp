package zy.entity.wx.product;

import java.io.Serializable;

public class T_Wx_ProductList implements Serializable{
	private static final long serialVersionUID = -5994522173689433056L;
	private Integer pl_id;
	private String pl_number;
	private String pl_code;
	private String pl_name;
	private Integer pl_type;
	private Integer companyid;
	public Integer getPl_id() {
		return pl_id;
	}
	public void setPl_id(Integer pl_id) {
		this.pl_id = pl_id;
	}
	public String getPl_number() {
		return pl_number;
	}
	public void setPl_number(String pl_number) {
		this.pl_number = pl_number;
	}
	public String getPl_code() {
		return pl_code;
	}
	public void setPl_code(String pl_code) {
		this.pl_code = pl_code;
	}
	public String getPl_name() {
		return pl_name;
	}
	public void setPl_name(String pl_name) {
		this.pl_name = pl_name;
	}
	public Integer getPl_type() {
		return pl_type;
	}
	public void setPl_type(Integer pl_type) {
		this.pl_type = pl_type;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
