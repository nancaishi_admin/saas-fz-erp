package zy.entity.wx.product;

import java.io.Serializable;

public class T_Wx_ProductShop implements Serializable{
	private static final long serialVersionUID = -6968449352336248478L;
	private Integer ps_id;
	private String ps_number;
	private String ps_shop_code;
	private Integer companyid;
	public Integer getPs_id() {
		return ps_id;
	}
	public void setPs_id(Integer ps_id) {
		this.ps_id = ps_id;
	}
	public String getPs_number() {
		return ps_number;
	}
	public void setPs_number(String ps_number) {
		this.ps_number = ps_number;
	}
	public String getPs_shop_code() {
		return ps_shop_code;
	}
	public void setPs_shop_code(String ps_shop_code) {
		this.ps_shop_code = ps_shop_code;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
