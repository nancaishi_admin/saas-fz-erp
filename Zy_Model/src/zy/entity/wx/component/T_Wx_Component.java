package zy.entity.wx.component;

import java.io.Serializable;

public class T_Wx_Component implements Serializable{
	private static final long serialVersionUID = -5319667994107209200L;
	private Integer wc_id;
	private String wc_appid;
	private String wc_verify_ticket;
	private String wc_createtime;
	private String wc_access_token;
	private String wc_expire_time;
	private String wc_pre_auth_code;
	private String wc_expire_time_pre_auth;
	public Integer getWc_id() {
		return wc_id;
	}
	public void setWc_id(Integer wc_id) {
		this.wc_id = wc_id;
	}
	public String getWc_appid() {
		return wc_appid;
	}
	public void setWc_appid(String wc_appid) {
		this.wc_appid = wc_appid;
	}
	public String getWc_verify_ticket() {
		return wc_verify_ticket;
	}
	public void setWc_verify_ticket(String wc_verify_ticket) {
		this.wc_verify_ticket = wc_verify_ticket;
	}
	public String getWc_createtime() {
		return wc_createtime;
	}
	public void setWc_createtime(String wc_createtime) {
		this.wc_createtime = wc_createtime;
	}
	public String getWc_access_token() {
		return wc_access_token;
	}
	public void setWc_access_token(String wc_access_token) {
		this.wc_access_token = wc_access_token;
	}
	public String getWc_expire_time() {
		return wc_expire_time;
	}
	public void setWc_expire_time(String wc_expire_time) {
		this.wc_expire_time = wc_expire_time;
	}
	public String getWc_pre_auth_code() {
		return wc_pre_auth_code;
	}
	public void setWc_pre_auth_code(String wc_pre_auth_code) {
		this.wc_pre_auth_code = wc_pre_auth_code;
	}
	public String getWc_expire_time_pre_auth() {
		return wc_expire_time_pre_auth;
	}
	public void setWc_expire_time_pre_auth(String wc_expire_time_pre_auth) {
		this.wc_expire_time_pre_auth = wc_expire_time_pre_auth;
	}
}
