package zy.entity.wx.user;

import java.util.Date;
import java.io.Serializable;
public class T_Wx_User_Account_Detail implements Serializable{
	private static final long serialVersionUID = -8264914016516778606L;
	private Integer id;
	private String type;
	private Double money;
	private String wu_code;
	private Date createdate;
	private String remark;
	private String order_number;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Double getMoney() {
		return money;
	}
	public void setMoney(Double money) {
		this.money = money;
	}
	public String getWu_code() {
		return wu_code;
	}
	public void setWu_code(String wu_code) {
		this.wu_code = wu_code;
	}
	public Date getCreatedate() {
		return createdate;
	}
	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getOrder_number() {
		return order_number;
	}
	public void setOrder_number(String order_number) {
		this.order_number = order_number;
	}

}

