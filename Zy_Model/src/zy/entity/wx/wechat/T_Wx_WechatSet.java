package zy.entity.wx.wechat;

import java.io.Serializable;

public class T_Wx_WechatSet implements Serializable{
	private static final long serialVersionUID = -3594456447093564301L;
	private Integer ws_id;
	private String ws_title;
	private String ws_bgimg_path;
	private Double ws_useable_balance;
	private String ws_sysdate;
	private String ws_shop_code;
	private Integer companyid;
	public Integer getWs_id() {
		return ws_id;
	}
	public void setWs_id(Integer ws_id) {
		this.ws_id = ws_id;
	}
	public String getWs_title() {
		return ws_title;
	}
	public void setWs_title(String ws_title) {
		this.ws_title = ws_title;
	}
	public String getWs_bgimg_path() {
		return ws_bgimg_path;
	}
	public void setWs_bgimg_path(String ws_bgimg_path) {
		this.ws_bgimg_path = ws_bgimg_path;
	}
	public Double getWs_useable_balance() {
		return ws_useable_balance;
	}
	public void setWs_useable_balance(Double ws_useable_balance) {
		this.ws_useable_balance = ws_useable_balance;
	}
	public String getWs_sysdate() {
		return ws_sysdate;
	}
	public void setWs_sysdate(String ws_sysdate) {
		this.ws_sysdate = ws_sysdate;
	}
	public String getWs_shop_code() {
		return ws_shop_code;
	}
	public void setWs_shop_code(String ws_shop_code) {
		this.ws_shop_code = ws_shop_code;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
