package zy.entity.wx.wechat;

import java.io.Serializable;

public class T_Wx_WechatInfo implements Serializable{
	private static final long serialVersionUID = -275618818300129562L;
	private Integer wx_id;
	private String wx_name;
	private String wx_originalid;
	private String wx_accountchat;
	private Integer wx_accounttype;
	private String wx_appid;
	private String wx_appsecret;
	private String wx_sysdate;
	private String wx_mt_code;
	private String wx_shop_code;
	private String wx_enddate;
	private Integer companyid;
	private String wx_qrc_ticket;
	private String wx_mch_id;
	private String wx_mch_key;
	private String wx_mch_cer_path;
	private String wx_mch_pwd;
	public Integer getWx_id() {
		return wx_id;
	}
	public void setWx_id(Integer wx_id) {
		this.wx_id = wx_id;
	}
	public String getWx_name() {
		return wx_name;
	}
	public void setWx_name(String wx_name) {
		this.wx_name = wx_name;
	}
	public String getWx_originalid() {
		return wx_originalid;
	}
	public void setWx_originalid(String wx_originalid) {
		this.wx_originalid = wx_originalid;
	}
	public String getWx_accountchat() {
		return wx_accountchat;
	}
	public void setWx_accountchat(String wx_accountchat) {
		this.wx_accountchat = wx_accountchat;
	}
	public Integer getWx_accounttype() {
		return wx_accounttype;
	}
	public void setWx_accounttype(Integer wx_accounttype) {
		this.wx_accounttype = wx_accounttype;
	}
	public String getWx_appid() {
		return wx_appid;
	}
	public void setWx_appid(String wx_appid) {
		this.wx_appid = wx_appid;
	}
	public String getWx_appsecret() {
		return wx_appsecret;
	}
	public void setWx_appsecret(String wx_appsecret) {
		this.wx_appsecret = wx_appsecret;
	}
	public String getWx_sysdate() {
		return wx_sysdate;
	}
	public void setWx_sysdate(String wx_sysdate) {
		this.wx_sysdate = wx_sysdate;
	}
	public String getWx_mt_code() {
		return wx_mt_code;
	}
	public void setWx_mt_code(String wx_mt_code) {
		this.wx_mt_code = wx_mt_code;
	}
	public String getWx_shop_code() {
		return wx_shop_code;
	}
	public void setWx_shop_code(String wx_shop_code) {
		this.wx_shop_code = wx_shop_code;
	}
	public String getWx_enddate() {
		return wx_enddate;
	}
	public void setWx_enddate(String wx_enddate) {
		this.wx_enddate = wx_enddate;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getWx_qrc_ticket() {
		return wx_qrc_ticket;
	}
	public void setWx_qrc_ticket(String wx_qrc_ticket) {
		this.wx_qrc_ticket = wx_qrc_ticket;
	}
	public String getWx_mch_id() {
		return wx_mch_id;
	}
	public void setWx_mch_id(String wx_mch_id) {
		this.wx_mch_id = wx_mch_id;
	}
	public String getWx_mch_key() {
		return wx_mch_key;
	}
	public void setWx_mch_key(String wx_mch_key) {
		this.wx_mch_key = wx_mch_key;
	}
	public String getWx_mch_cer_path() {
		return wx_mch_cer_path;
	}
	public void setWx_mch_cer_path(String wx_mch_cer_path) {
		this.wx_mch_cer_path = wx_mch_cer_path;
	}
	public String getWx_mch_pwd() {
		return wx_mch_pwd;
	}
	public void setWx_mch_pwd(String wx_mch_pwd) {
		this.wx_mch_pwd = wx_mch_pwd;
	}
}
