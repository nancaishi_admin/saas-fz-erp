package zy.entity.wx.order;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
* Created by YanXue on 2018-06-16
*@Description t_wx_order 实体类
*/ 


public class T_Wx_Order implements Serializable{
	private Long id;
	private String number;
	private Date sysdate;
	private String address;
	private String linkman;
	private String mobile;
	
	
	private String em_code;
	private String wu_code;

	private String shop_code;
	private Integer amount;
	private Double money;
	private Double sell_money;
	private Double cd_money;
	private Integer state;
	private Integer companyid;
	private Double cd_rate;
	private Double vc_money;
	private Double vc_rate;
	private Double point_money;
	private Double ec_money;
	private Double wx_money;
	private Double ali_money;
	private Double third_money;
	private Double red_money;
	private String remark;
	private Double acc_money;
	private String order_status;
	private String trade_no;
	private Date pay_time;
	private String pay_status;
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getLinkman() {
		return linkman;
	}
	public void setLinkman(String linkman) {
		this.linkman = linkman;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getWu_code() {
		return wu_code;
	}
	public void setWu_code(String wu_code) {
		this.wu_code = wu_code;
	}
	public String getOrder_status() {
		return order_status;
	}
	public void setOrder_status(String order_status) {
		this.order_status = order_status;
	}
	public Double getAcc_money() {
		return acc_money;
	}
	public void setAcc_money(Double acc_money) {
		this.acc_money = acc_money;
	}
	private List<T_Wx_Order_Detail> orderDetails;
	public void setId(Long id){
		this.id=id;
	}
	public Long getId(){
		return id;
	}
	public void setNumber(String number){
		this.number=number;
	}
	public String getNumber(){
		return number;
	}
	public void setSysdate(Date sysdate){
		this.sysdate=sysdate;
	}
	public Date getSysdate(){
		return sysdate;
	}
	public void setEm_code(String em_code){
		this.em_code=em_code;
	}
	public String getEm_code(){
		return em_code;
	}
	public void setShop_code(String shop_code){
		this.shop_code=shop_code;
	}
	public String getShop_code(){
		return shop_code;
	}
	public void setAmount(Integer amount){
		this.amount=amount;
	}
	public Integer getAmount(){
		return amount;
	}
	public void setMoney(Double money){
		this.money=money;
	}
	public Double getMoney(){
		return money;
	}
	public void setSell_money(Double sell_money){
		this.sell_money=sell_money;
	}
	public Double getSell_money(){
		return sell_money;
	}
	public void setCd_money(Double cd_money){
		this.cd_money=cd_money;
	}
	public Double getCd_money(){
		return cd_money;
	}
	public void setState(Integer state){
		this.state=state;
	}
	public Integer getState(){
		return state;
	}
	public void setCompanyid(Integer companyid){
		this.companyid=companyid;
	}
	public Integer getCompanyid(){
		return companyid;
	}
	public void setCd_rate(Double cd_rate){
		this.cd_rate=cd_rate;
	}
	public Double getCd_rate(){
		return cd_rate;
	}
	public void setVc_money(Double vc_money){
		this.vc_money=vc_money;
	}
	public Double getVc_money(){
		return vc_money;
	}
	public void setVc_rate(Double vc_rate){
		this.vc_rate=vc_rate;
	}
	public Double getVc_rate(){
		return vc_rate;
	}
	public void setPoint_money(Double point_money){
		this.point_money=point_money;
	}
	public Double getPoint_money(){
		return point_money;
	}
	public void setEc_money(Double ec_money){
		this.ec_money=ec_money;
	}
	public Double getEc_money(){
		return ec_money;
	}
	public void setWx_money(Double wx_money){
		this.wx_money=wx_money;
	}
	public Double getWx_money(){
		return wx_money;
	}
	public void setAli_money(Double ali_money){
		this.ali_money=ali_money;
	}
	public Double getAli_money(){
		return ali_money;
	}
	public void setThird_money(Double third_money){
		this.third_money=third_money;
	}
	public Double getThird_money(){
		return third_money;
	}
	public void setRed_money(Double red_money){
		this.red_money=red_money;
	}
	public Double getRed_money(){
		return red_money;
	}
	public void setRemark(String remark){
		this.remark=remark;
	}
	public String getRemark(){
		return remark;
	}
	public List<T_Wx_Order_Detail> getOrderDetails() {
		return orderDetails;
	}
	public void setOrderDetails(List<T_Wx_Order_Detail> orderDetails) {
		this.orderDetails = orderDetails;
	}
	public String getTrade_no() {
		return trade_no;
	}
	public void setTrade_no(String trade_no) {
		this.trade_no = trade_no;
	}
	public Date getPay_time() {
		return pay_time;
	}
	public void setPay_time(Date pay_time) {
		this.pay_time = pay_time;
	}
	public String getPay_status() {
		return pay_status;
	}
	public void setPay_status(String pay_status) {
		this.pay_status = pay_status;
	}
	
}

