package zy.entity.wx.order;

import java.util.Date;
import java.math.BigDecimal;
import java.io.Serializable;

/**
* Created by YanXue on 2018-06-16
*@Description t_wx_order_detail 实体类
*/ 


public class T_Wx_Order_Detail implements Serializable{
	private Long id;
	private String number;
	private Date sysdate;
	private String pd_code;
	private String pd_name;
	private String sub_code;
	private String sub_name;
	private String cr_code;
	private String cr_name;
	private String sz_code;
	private String sz_name;
	private String br_code;
	private String br_name;
	private String em_code;
	private String em_name;
	private String shop_code;
	private Integer amount;
	private Double sell_price;
	private Double price;
	private Double money;
	private Integer state;
	private Integer companyid;
	private String sale_code;
	private String wu_code;
	private String pd_pic;
	
	public String getSub_name() {
		return sub_name;
	}
	public void setSub_name(String sub_name) {
		this.sub_name = sub_name;
	}
	public String getPd_pic() {
		return pd_pic;
	}
	public void setPd_pic(String pd_pic) {
		this.pd_pic = pd_pic;
	}
	public void setId(Long id){
		this.id=id;
	}
	public Long getId(){
		return id;
	}
	public void setNumber(String number){
		this.number=number;
	}
	public String getNumber(){
		return number;
	}
	public void setSysdate(Date sysdate){
		this.sysdate=sysdate;
	}
	public Date getSysdate(){
		return sysdate;
	}
	public void setPd_code(String pd_code){
		this.pd_code=pd_code;
	}
	public String getPd_code(){
		return pd_code;
	}
	public void setPd_name(String pd_name){
		this.pd_name=pd_name;
	}
	public String getPd_name(){
		return pd_name;
	}
	public void setSub_code(String sub_code){
		this.sub_code=sub_code;
	}
	public String getSub_code(){
		return sub_code;
	}
	public void setCr_code(String cr_code){
		this.cr_code=cr_code;
	}
	public String getCr_code(){
		return cr_code;
	}
	public void setCr_name(String cr_name){
		this.cr_name=cr_name;
	}
	public String getCr_name(){
		return cr_name;
	}
	public void setSz_code(String sz_code){
		this.sz_code=sz_code;
	}
	public String getSz_code(){
		return sz_code;
	}
	public void setSz_name(String sz_name){
		this.sz_name=sz_name;
	}
	public String getSz_name(){
		return sz_name;
	}
	public void setBr_code(String br_code){
		this.br_code=br_code;
	}
	public String getBr_code(){
		return br_code;
	}
	public void setBr_name(String br_name){
		this.br_name=br_name;
	}
	public String getBr_name(){
		return br_name;
	}
	public void setEm_code(String em_code){
		this.em_code=em_code;
	}
	public String getEm_code(){
		return em_code;
	}
	public void setEm_name(String em_name){
		this.em_name=em_name;
	}
	public String getEm_name(){
		return em_name;
	}
	public void setShop_code(String shop_code){
		this.shop_code=shop_code;
	}
	public String getShop_code(){
		return shop_code;
	}
	public void setAmount(Integer amount){
		this.amount=amount;
	}
	public Integer getAmount(){
		return amount;
	}
	public void setSell_price(Double sell_price){
		this.sell_price=sell_price;
	}
	public Double getSell_price(){
		return sell_price;
	}
	public void setPrice(Double price){
		this.price=price;
	}
	public Double getPrice(){
		return price;
	}
	public void setMoney(Double money){
		this.money=money;
	}
	public Double getMoney(){
		return money;
	}
	public void setState(Integer state){
		this.state=state;
	}
	public Integer getState(){
		return state;
	}
	public void setCompanyid(Integer companyid){
		this.companyid=companyid;
	}
	public Integer getCompanyid(){
		return companyid;
	}
	public void setSale_code(String sale_code){
		this.sale_code=sale_code;
	}
	public String getSale_code(){
		return sale_code;
	}
	public String getWu_code() {
		return wu_code;
	}
	public void setWu_code(String wu_code) {
		this.wu_code = wu_code;
	}
	
}

