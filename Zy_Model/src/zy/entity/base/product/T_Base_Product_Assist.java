package zy.entity.base.product;

import java.io.Serializable;

/**
 * 商品资料辅助属性模板
 *
 */
public class T_Base_Product_Assist implements Serializable{
	private static final long serialVersionUID = -3063155428478877709L;
	private Integer pda_id;
	private String pda_name;
	private String pda_fill;
	private String pda_color;
	private String pda_size;
	private String pda_place;
	private String pda_model;
	private String pda_safe;
	private String pda_execute;
	private String pda_grade;
	private String pda_price_name;//价格特性
	private Double pda_salesman_comm;//业务提成金额
	private Double pda_salesman_commrate;//业务员提成率
	private String pda_gbcode;//国标码
	private String pda_wash_explain;//洗涤说明
	private Integer pda_sale;
	private Integer pda_change;
	private Integer pda_gift;
	private Integer pda_vip_sale;
	private Integer pda_present;
	private Integer pda_buy;
	private Integer pda_point;
	private Double pda_batch_price1;
	private Double pda_batch_price2;
	private Double pda_batch_price3;
	private Integer companyid;
	public Integer getPda_id() {
		return pda_id;
	}
	public void setPda_id(Integer pda_id) {
		this.pda_id = pda_id;
	}
	public String getPda_name() {
		return pda_name;
	}
	public void setPda_name(String pda_name) {
		this.pda_name = pda_name;
	}
	public String getPda_fill() {
		return pda_fill;
	}
	public void setPda_fill(String pda_fill) {
		this.pda_fill = pda_fill;
	}
	public String getPda_color() {
		return pda_color;
	}
	public void setPda_color(String pda_color) {
		this.pda_color = pda_color;
	}
	public String getPda_size() {
		return pda_size;
	}
	public void setPda_size(String pda_size) {
		this.pda_size = pda_size;
	}
	public String getPda_place() {
		return pda_place;
	}
	public void setPda_place(String pda_place) {
		this.pda_place = pda_place;
	}
	public String getPda_model() {
		return pda_model;
	}
	public void setPda_model(String pda_model) {
		this.pda_model = pda_model;
	}
	public String getPda_safe() {
		return pda_safe;
	}
	public void setPda_safe(String pda_safe) {
		this.pda_safe = pda_safe;
	}
	public String getPda_execute() {
		return pda_execute;
	}
	public void setPda_execute(String pda_execute) {
		this.pda_execute = pda_execute;
	}
	public String getPda_grade() {
		return pda_grade;
	}
	public void setPda_grade(String pda_grade) {
		this.pda_grade = pda_grade;
	}
	public String getPda_price_name() {
		return pda_price_name;
	}
	public void setPda_price_name(String pda_price_name) {
		this.pda_price_name = pda_price_name;
	}
	public Double getPda_salesman_comm() {
		return pda_salesman_comm;
	}
	public void setPda_salesman_comm(Double pda_salesman_comm) {
		this.pda_salesman_comm = pda_salesman_comm;
	}
	public Double getPda_salesman_commrate() {
		return pda_salesman_commrate;
	}
	public void setPda_salesman_commrate(Double pda_salesman_commrate) {
		this.pda_salesman_commrate = pda_salesman_commrate;
	}
	public String getPda_gbcode() {
		return pda_gbcode;
	}
	public void setPda_gbcode(String pda_gbcode) {
		this.pda_gbcode = pda_gbcode;
	}
	public String getPda_wash_explain() {
		return pda_wash_explain;
	}
	public void setPda_wash_explain(String pda_wash_explain) {
		this.pda_wash_explain = pda_wash_explain;
	}
	public Integer getPda_sale() {
		return pda_sale;
	}
	public void setPda_sale(Integer pda_sale) {
		this.pda_sale = pda_sale;
	}
	public Integer getPda_change() {
		return pda_change;
	}
	public void setPda_change(Integer pda_change) {
		this.pda_change = pda_change;
	}
	public Integer getPda_gift() {
		return pda_gift;
	}
	public void setPda_gift(Integer pda_gift) {
		this.pda_gift = pda_gift;
	}
	public Integer getPda_vip_sale() {
		return pda_vip_sale;
	}
	public void setPda_vip_sale(Integer pda_vip_sale) {
		this.pda_vip_sale = pda_vip_sale;
	}
	public Integer getPda_present() {
		return pda_present;
	}
	public void setPda_present(Integer pda_present) {
		this.pda_present = pda_present;
	}
	public Integer getPda_buy() {
		return pda_buy;
	}
	public void setPda_buy(Integer pda_buy) {
		this.pda_buy = pda_buy;
	}
	public Integer getPda_point() {
		return pda_point;
	}
	public void setPda_point(Integer pda_point) {
		this.pda_point = pda_point;
	}
	public Double getPda_batch_price1() {
		return pda_batch_price1;
	}
	public void setPda_batch_price1(Double pda_batch_price1) {
		this.pda_batch_price1 = pda_batch_price1;
	}
	public Double getPda_batch_price2() {
		return pda_batch_price2;
	}
	public void setPda_batch_price2(Double pda_batch_price2) {
		this.pda_batch_price2 = pda_batch_price2;
	}
	public Double getPda_batch_price3() {
		return pda_batch_price3;
	}
	public void setPda_batch_price3(Double pda_batch_price3) {
		this.pda_batch_price3 = pda_batch_price3;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	
}
