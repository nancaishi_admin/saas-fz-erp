package zy.entity.base.product;

import java.io.Serializable;

public class T_Base_Product_Shop_Price implements Serializable{
	private static final long serialVersionUID = -3063155428478877709L;
	private Integer pdp_id;
	private String pdp_pd_code;
	private Integer pdp_pd_point;
	private String pdp_shop_code;
	private Double pdp_cost_price;
	private Double pdp_sell_price;
	private Double pdp_vip_price;
	private Double pdp_sort_price;
	private Integer companyid;
	private String pd_code;
	private String pd_no;
	private String shop_name;
	private String shop_type;
	public Integer getPdp_id() {
		return pdp_id;
	}
	public void setPdp_id(Integer pdp_id) {
		this.pdp_id = pdp_id;
	}
	public String getPdp_pd_code() {
		return pdp_pd_code;
	}
	public void setPdp_pd_code(String pdp_pd_code) {
		this.pdp_pd_code = pdp_pd_code;
	}
	public String getPdp_shop_code() {
		return pdp_shop_code;
	}
	public void setPdp_shop_code(String pdp_shop_code) {
		this.pdp_shop_code = pdp_shop_code;
	}
	public Double getPdp_cost_price() {
		return pdp_cost_price;
	}
	public void setPdp_cost_price(Double pdp_cost_price) {
		this.pdp_cost_price = pdp_cost_price;
	}
	public Double getPdp_sell_price() {
		return pdp_sell_price;
	}
	public void setPdp_sell_price(Double pdp_sell_price) {
		this.pdp_sell_price = pdp_sell_price;
	}
	public Double getPdp_vip_price() {
		return pdp_vip_price;
	}
	public void setPdp_vip_price(Double pdp_vip_price) {
		this.pdp_vip_price = pdp_vip_price;
	}
	public Double getPdp_sort_price() {
		return pdp_sort_price;
	}
	public void setPdp_sort_price(Double pdp_sort_price) {
		this.pdp_sort_price = pdp_sort_price;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getPd_code() {
		return pd_code;
	}
	public void setPd_code(String pd_code) {
		this.pd_code = pd_code;
	}
	public String getPd_no() {
		return pd_no;
	}
	public void setPd_no(String pd_no) {
		this.pd_no = pd_no;
	}
	public Integer getPdp_pd_point() {
		return pdp_pd_point;
	}
	public void setPdp_pd_point(Integer pdp_pd_point) {
		this.pdp_pd_point = pdp_pd_point;
	}
	public String getShop_name() {
		return shop_name;
	}
	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}
	public String getShop_type() {
		return shop_type;
	}
	public void setShop_type(String shop_type) {
		this.shop_type = shop_type;
	}
}
