package zy.entity.base.product;

import java.io.Serializable;

public class T_Base_Product_Img implements Serializable{
	private static final long serialVersionUID = -3063155428478877709L;
	private Integer pdm_id;
	private String pdm_pd_code;
	private String pdm_cr_code;
	private String pdm_img_name;
	private String pdm_all_path;
	private String pdm_img_path;
	private String pdm_state;
	private Integer companyid;
	private String cr_name;
	public Integer getPdm_id() {
		return pdm_id;
	}
	public void setPdm_id(Integer pdm_id) {
		this.pdm_id = pdm_id;
	}
	public String getPdm_pd_code() {
		return pdm_pd_code;
	}
	public void setPdm_pd_code(String pdm_pd_code) {
		this.pdm_pd_code = pdm_pd_code;
	}
	public String getPdm_cr_code() {
		return pdm_cr_code;
	}
	public void setPdm_cr_code(String pdm_cr_code) {
		this.pdm_cr_code = pdm_cr_code;
	}
	public String getPdm_img_name() {
		return pdm_img_name;
	}
	public void setPdm_img_name(String pdm_img_name) {
		this.pdm_img_name = pdm_img_name;
	}
	public String getPdm_all_path() {
		return pdm_all_path;
	}
	public void setPdm_all_path(String pdm_all_path) {
		this.pdm_all_path = pdm_all_path;
	}
	public String getPdm_img_path() {
		return pdm_img_path;
	}
	public void setPdm_img_path(String pdm_img_path) {
		this.pdm_img_path = pdm_img_path;
	}
	public String getPdm_state() {
		return pdm_state;
	}
	public void setPdm_state(String pdm_state) {
		this.pdm_state = pdm_state;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getCr_name() {
		return cr_name;
	}
	public void setCr_name(String cr_name) {
		this.cr_name = cr_name;
	}
}
