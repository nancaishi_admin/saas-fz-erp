package zy.entity.base.product;

import java.io.Serializable;

public class T_Base_Product implements Serializable{
	private static final long serialVersionUID = -3063155428478877709L;
	//T_Base_Product
	private Integer pd_id;
	private String pd_code;
	private String pd_no;
	private String pd_number;
	private String pd_name;
	private String pd_spell;
	private String pd_szg_code;
	private String pd_szg_name;
	private String pd_tp_code;
	private String pd_tp_name;
	private String pd_tp_upcode;
	private String pd_unit;
	private String pd_bd_code;
	private String pd_bd_name;//品牌名称
	private String pd_season;
	private String pd_style;
	private Integer pd_year;
	private Double pd_cost_price;
	private Integer pd_sell_cycle;
	private Double pd_sell_price;
	private Integer pd_state;
	private String pd_fabric;
	private Integer pd_bra;
	private Integer pd_change;
	private String pd_date;
	private String pd_returndate;
	private Double pd_commission;
	private Double pd_commission_rate;
	private Integer pd_gift;
	private Integer pd_score;
	private Integer companyid;
	//t_base_product_info
	private String pd_sysdate;
	private String pd_fill;
	private String pd_color;
	private String pd_size;
	private String pd_place;
	private String pd_in_fabric;
	private String pd_model;
	private String pd_safe;
	private String pd_execute;
	private String pd_grade;
	private String pd_add_manager;
	private String pd_add_date;
	private String pd_mod_manager;
	private String pd_mod_date;
	private String pd_sp_code;
	private String pd_sp_name;
	private Integer pd_buy_cycle;
	private Double pd_buy_price;
	private String pd_buy_date;
	private Integer pd_buy;
	private Double pd_sort_price;
	private Double pd_batch_price;
	private Double pd_batch_price1;
	private Double pd_batch_price2;
	private Double pd_batch_price3;
	private Double pd_sign_price;
	private Double pd_vip_price;
	private Integer pd_vip_sale;
	private Integer pd_present;
	private Integer pd_point;
	private Integer pd_sale;
	private String pd_price_name;//价格特性
	private Double pd_salesman_comm;//业务提成金额
	private Double pd_salesman_commrate;//业务员提成率
	private String pd_gbcode;//国标码
	private String pd_wash_explain;//洗涤说明
	
	private String pdc_cr_codes;//颜色编号
	private String pdc_cr_codes_bak;
	private String pdb_br_codes;//杯型编号
	private String pdb_br_codes_bak;
	private String pdb_br_name;
	
	private String exist; //商品是否已经录入
	private Integer sd_amount;//该商品库存数
	private Integer img_count;//图片数量
	private String pdm_img_path;//图片路径
	private Integer pdm_id;//图片ID
	public Integer getPd_id() {
		return pd_id;
	}
	public void setPd_id(Integer pd_id) {
		this.pd_id = pd_id;
	}
	public String getPd_code() {
		return pd_code;
	}
	public void setPd_code(String pd_code) {
		this.pd_code = pd_code;
	}
	public String getPd_no() {
		return pd_no;
	}
	public void setPd_no(String pd_no) {
		this.pd_no = pd_no;
	}
	public String getPd_number() {
		return pd_number;
	}
	public void setPd_number(String pd_number) {
		this.pd_number = pd_number;
	}
	public String getPd_name() {
		return pd_name;
	}
	public void setPd_name(String pd_name) {
		this.pd_name = pd_name;
	}
	public String getPd_spell() {
		return pd_spell;
	}
	public void setPd_spell(String pd_spell) {
		this.pd_spell = pd_spell;
	}
	public String getPd_szg_code() {
		return pd_szg_code;
	}
	public void setPd_szg_code(String pd_szg_code) {
		this.pd_szg_code = pd_szg_code;
	}
	public String getPd_szg_name() {
		return pd_szg_name;
	}
	public void setPd_szg_name(String pd_szg_name) {
		this.pd_szg_name = pd_szg_name;
	}
	public String getPd_tp_code() {
		return pd_tp_code;
	}
	public void setPd_tp_code(String pd_tp_code) {
		this.pd_tp_code = pd_tp_code;
	}
	public String getPd_tp_name() {
		return pd_tp_name;
	}
	public void setPd_tp_name(String pd_tp_name) {
		this.pd_tp_name = pd_tp_name;
	}
	public String getPd_tp_upcode() {
		return pd_tp_upcode;
	}
	public void setPd_tp_upcode(String pd_tp_upcode) {
		this.pd_tp_upcode = pd_tp_upcode;
	}
	public String getPd_unit() {
		return pd_unit;
	}
	public void setPd_unit(String pd_unit) {
		this.pd_unit = pd_unit;
	}
	public String getPd_bd_code() {
		return pd_bd_code;
	}
	public void setPd_bd_code(String pd_bd_code) {
		this.pd_bd_code = pd_bd_code;
	}
	public String getPd_bd_name() {
		return pd_bd_name;
	}
	public void setPd_bd_name(String pd_bd_name) {
		this.pd_bd_name = pd_bd_name;
	}
	public String getPd_season() {
		return pd_season;
	}
	public void setPd_season(String pd_season) {
		this.pd_season = pd_season;
	}
	public String getPd_style() {
		return pd_style;
	}
	public void setPd_style(String pd_style) {
		this.pd_style = pd_style;
	}
	public Integer getPd_year() {
		return pd_year;
	}
	public void setPd_year(Integer pd_year) {
		this.pd_year = pd_year;
	}
	public Double getPd_cost_price() {
		return pd_cost_price;
	}
	public void setPd_cost_price(Double pd_cost_price) {
		this.pd_cost_price = pd_cost_price;
	}
	public Integer getPd_sell_cycle() {
		return pd_sell_cycle;
	}
	public void setPd_sell_cycle(Integer pd_sell_cycle) {
		this.pd_sell_cycle = pd_sell_cycle;
	}
	public Double getPd_sell_price() {
		return pd_sell_price;
	}
	public void setPd_sell_price(Double pd_sell_price) {
		this.pd_sell_price = pd_sell_price;
	}
	public Integer getPd_state() {
		return pd_state;
	}
	public void setPd_state(Integer pd_state) {
		this.pd_state = pd_state;
	}
	public String getPd_fabric() {
		return pd_fabric;
	}
	public void setPd_fabric(String pd_fabric) {
		this.pd_fabric = pd_fabric;
	}
	public Integer getPd_bra() {
		return pd_bra;
	}
	public void setPd_bra(Integer pd_bra) {
		this.pd_bra = pd_bra;
	}
	public Integer getPd_change() {
		return pd_change;
	}
	public void setPd_change(Integer pd_change) {
		this.pd_change = pd_change;
	}
	public String getPd_date() {
		return pd_date;
	}
	public void setPd_date(String pd_date) {
		this.pd_date = pd_date;
	}
	public String getPd_returndate() {
		return pd_returndate;
	}
	public void setPd_returndate(String pd_returndate) {
		this.pd_returndate = pd_returndate;
	}
	public Double getPd_commission() {
		return pd_commission;
	}
	public void setPd_commission(Double pd_commission) {
		this.pd_commission = pd_commission;
	}
	public Double getPd_commission_rate() {
		return pd_commission_rate;
	}
	public void setPd_commission_rate(Double pd_commission_rate) {
		this.pd_commission_rate = pd_commission_rate;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public Integer getPd_gift() {
		return pd_gift;
	}
	public void setPd_gift(Integer pd_gift) {
		this.pd_gift = pd_gift;
	}
	public Integer getPd_score() {
		return pd_score;
	}
	public void setPd_score(Integer pd_score) {
		this.pd_score = pd_score;
	}
	public String getPd_sysdate() {
		return pd_sysdate;
	}
	public void setPd_sysdate(String pd_sysdate) {
		this.pd_sysdate = pd_sysdate;
	}
	public String getPd_fill() {
		return pd_fill;
	}
	public void setPd_fill(String pd_fill) {
		this.pd_fill = pd_fill;
	}
	public String getPd_color() {
		return pd_color;
	}
	public void setPd_color(String pd_color) {
		this.pd_color = pd_color;
	}
	public String getPd_size() {
		return pd_size;
	}
	public void setPd_size(String pd_size) {
		this.pd_size = pd_size;
	}
	public String getPd_place() {
		return pd_place;
	}
	public void setPd_place(String pd_place) {
		this.pd_place = pd_place;
	}
	public String getPd_in_fabric() {
		return pd_in_fabric;
	}
	public void setPd_in_fabric(String pd_in_fabric) {
		this.pd_in_fabric = pd_in_fabric;
	}
	public String getPd_model() {
		return pd_model;
	}
	public void setPd_model(String pd_model) {
		this.pd_model = pd_model;
	}
	public String getPd_safe() {
		return pd_safe;
	}
	public void setPd_safe(String pd_safe) {
		this.pd_safe = pd_safe;
	}
	public String getPd_execute() {
		return pd_execute;
	}
	public void setPd_execute(String pd_execute) {
		this.pd_execute = pd_execute;
	}
	public String getPd_grade() {
		return pd_grade;
	}
	public void setPd_grade(String pd_grade) {
		this.pd_grade = pd_grade;
	}
	public String getPd_sp_code() {
		return pd_sp_code;
	}
	public void setPd_sp_code(String pd_sp_code) {
		this.pd_sp_code = pd_sp_code;
	}
	public String getPd_sp_name() {
		return pd_sp_name;
	}
	public void setPd_sp_name(String pd_sp_name) {
		this.pd_sp_name = pd_sp_name;
	}
	public Integer getPd_buy_cycle() {
		return pd_buy_cycle;
	}
	public void setPd_buy_cycle(Integer pd_buy_cycle) {
		this.pd_buy_cycle = pd_buy_cycle;
	}
	public Double getPd_buy_price() {
		return pd_buy_price;
	}
	public void setPd_buy_price(Double pd_buy_price) {
		this.pd_buy_price = pd_buy_price;
	}
	public String getPd_buy_date() {
		return pd_buy_date;
	}
	public void setPd_buy_date(String pd_buy_date) {
		this.pd_buy_date = pd_buy_date;
	}
	public Integer getPd_buy() {
		return pd_buy;
	}
	public void setPd_buy(Integer pd_buy) {
		this.pd_buy = pd_buy;
	}
	public Double getPd_sort_price() {
		return pd_sort_price;
	}
	public void setPd_sort_price(Double pd_sort_price) {
		this.pd_sort_price = pd_sort_price;
	}
	public Double getPd_batch_price() {
		return pd_batch_price;
	}
	public void setPd_batch_price(Double pd_batch_price) {
		this.pd_batch_price = pd_batch_price;
	}
	public Double getPd_batch_price1() {
		return pd_batch_price1;
	}
	public void setPd_batch_price1(Double pd_batch_price1) {
		this.pd_batch_price1 = pd_batch_price1;
	}
	public Double getPd_batch_price2() {
		return pd_batch_price2;
	}
	public void setPd_batch_price2(Double pd_batch_price2) {
		this.pd_batch_price2 = pd_batch_price2;
	}
	public Double getPd_batch_price3() {
		return pd_batch_price3;
	}
	public void setPd_batch_price3(Double pd_batch_price3) {
		this.pd_batch_price3 = pd_batch_price3;
	}
	public Double getPd_sign_price() {
		return pd_sign_price;
	}
	public void setPd_sign_price(Double pd_sign_price) {
		this.pd_sign_price = pd_sign_price;
	}
	public Double getPd_vip_price() {
		return pd_vip_price;
	}
	public void setPd_vip_price(Double pd_vip_price) {
		this.pd_vip_price = pd_vip_price;
	}
	public Integer getPd_vip_sale() {
		return pd_vip_sale;
	}
	public void setPd_vip_sale(Integer pd_vip_sale) {
		this.pd_vip_sale = pd_vip_sale;
	}
	public Integer getPd_present() {
		return pd_present;
	}
	public void setPd_present(Integer pd_present) {
		this.pd_present = pd_present;
	}
	public Integer getPd_point() {
		return pd_point;
	}
	public void setPd_point(Integer pd_point) {
		this.pd_point = pd_point;
	}
	public Integer getPd_sale() {
		return pd_sale;
	}
	public void setPd_sale(Integer pd_sale) {
		this.pd_sale = pd_sale;
	}
	public String getPdc_cr_codes() {
		return pdc_cr_codes;
	}
	public void setPdc_cr_codes(String pdc_cr_codes) {
		this.pdc_cr_codes = pdc_cr_codes;
	}
	public String getPdb_br_codes() {
		return pdb_br_codes;
	}
	public void setPdb_br_codes(String pdb_br_codes) {
		this.pdb_br_codes = pdb_br_codes;
	}
	public String getExist() {
		return exist;
	}
	public void setExist(String exist) {
		this.exist = exist;
	}
	public Integer getSd_amount() {
		return sd_amount;
	}
	public void setSd_amount(Integer sd_amount) {
		this.sd_amount = sd_amount;
	}
	public Integer getImg_count() {
		return img_count;
	}
	public void setImg_count(Integer img_count) {
		this.img_count = img_count;
	}
	public String getPdb_br_name() {
		return pdb_br_name;
	}
	public void setPdb_br_name(String pdb_br_name) {
		this.pdb_br_name = pdb_br_name;
	}
	public String getPdc_cr_codes_bak() {
		return pdc_cr_codes_bak;
	}
	public void setPdc_cr_codes_bak(String pdc_cr_codes_bak) {
		this.pdc_cr_codes_bak = pdc_cr_codes_bak;
	}
	public String getPdb_br_codes_bak() {
		return pdb_br_codes_bak;
	}
	public void setPdb_br_codes_bak(String pdb_br_codes_bak) {
		this.pdb_br_codes_bak = pdb_br_codes_bak;
	}
	public String getPdm_img_path() {
		return pdm_img_path;
	}
	public void setPdm_img_path(String pdm_img_path) {
		this.pdm_img_path = pdm_img_path;
	}
	public String getPd_add_manager() {
		return pd_add_manager;
	}
	public void setPd_add_manager(String pd_add_manager) {
		this.pd_add_manager = pd_add_manager;
	}
	public String getPd_add_date() {
		return pd_add_date;
	}
	public void setPd_add_date(String pd_add_date) {
		this.pd_add_date = pd_add_date;
	}
	public String getPd_mod_manager() {
		return pd_mod_manager;
	}
	public void setPd_mod_manager(String pd_mod_manager) {
		this.pd_mod_manager = pd_mod_manager;
	}
	public String getPd_mod_date() {
		return pd_mod_date;
	}
	public void setPd_mod_date(String pd_mod_date) {
		this.pd_mod_date = pd_mod_date;
	}
	public String getPd_price_name() {
		return pd_price_name;
	}
	public void setPd_price_name(String pd_price_name) {
		this.pd_price_name = pd_price_name;
	}
	public Double getPd_salesman_comm() {
		return pd_salesman_comm;
	}
	public void setPd_salesman_comm(Double pd_salesman_comm) {
		this.pd_salesman_comm = pd_salesman_comm;
	}
	public Double getPd_salesman_commrate() {
		return pd_salesman_commrate;
	}
	public void setPd_salesman_commrate(Double pd_salesman_commrate) {
		this.pd_salesman_commrate = pd_salesman_commrate;
	}
	public String getPd_gbcode() {
		return pd_gbcode;
	}
	public void setPd_gbcode(String pd_gbcode) {
		this.pd_gbcode = pd_gbcode;
	}
	public Integer getPdm_id() {
		return pdm_id;
	}
	public void setPdm_id(Integer pdm_id) {
		this.pdm_id = pdm_id;
	}
	public String getPd_wash_explain() {
		return pd_wash_explain;
	}
	public void setPd_wash_explain(String pd_wash_explain) {
		this.pd_wash_explain = pd_wash_explain;
	}
}
