package zy.entity.base.product;

import java.io.Serializable;

public class T_Base_Barcode implements Serializable{
	private static final long serialVersionUID = -3063155428478877709L;
	private Integer bc_id;
	private String bc_pd_code;
	private String bc_pd_name;//商品名称；
	private String bc_pd_no;
	private String pd_bd_name;//品牌名称
	private String pd_tp_name;
	private Double pd_sell_price;
	private String bc_size;
	private String bc_sizename;//尺码名称
	private String bc_bra;
	private String bc_braname;//杯型名称
	private String bc_color;
	private String bc_colorname;//颜色名称
	private String bc_subcode;
	private String bc_sys_barcode;//系统条码
	private String bc_barcode;
	private Integer companyid;
	private Double pd_vip_price;
	private Double pd_sign_price;
	private String pd_grade;
	private String pd_safe;
	private String pd_fill;
	private String pd_execute;
	private String pd_fabric;
	private String pd_in_fabric;
	private String pd_place;
	private String pd_wash_explain;
	private Integer bc_printnumber;//打印数量
	private Integer id;
	public Integer getBc_id() {
		return bc_id;
	}
	public void setBc_id(Integer bc_id) {
		this.bc_id = bc_id;
	}
	public String getBc_pd_code() {
		return bc_pd_code;
	}
	public void setBc_pd_code(String bc_pd_code) {
		this.bc_pd_code = bc_pd_code;
	}
	public String getBc_pd_name() {
		return bc_pd_name;
	}
	public void setBc_pd_name(String bc_pd_name) {
		this.bc_pd_name = bc_pd_name;
	}
	public String getBc_pd_no() {
		return bc_pd_no;
	}
	public void setBc_pd_no(String bc_pd_no) {
		this.bc_pd_no = bc_pd_no;
	}
	public String getBc_size() {
		return bc_size;
	}
	public void setBc_size(String bc_size) {
		this.bc_size = bc_size;
	}
	public String getBc_sizename() {
		return bc_sizename;
	}
	public void setBc_sizename(String bc_sizename) {
		this.bc_sizename = bc_sizename;
	}
	public String getBc_bra() {
		return bc_bra;
	}
	public void setBc_bra(String bc_bra) {
		this.bc_bra = bc_bra;
	}
	public String getBc_braname() {
		return bc_braname;
	}
	public void setBc_braname(String bc_braname) {
		this.bc_braname = bc_braname;
	}
	public String getBc_color() {
		return bc_color;
	}
	public void setBc_color(String bc_color) {
		this.bc_color = bc_color;
	}
	public String getBc_colorname() {
		return bc_colorname;
	}
	public void setBc_colorname(String bc_colorname) {
		this.bc_colorname = bc_colorname;
	}
	public String getBc_subcode() {
		return bc_subcode;
	}
	public void setBc_subcode(String bc_subcode) {
		this.bc_subcode = bc_subcode;
	}
	public String getBc_sys_barcode() {
		return bc_sys_barcode;
	}
	public void setBc_sys_barcode(String bc_sys_barcode) {
		this.bc_sys_barcode = bc_sys_barcode;
	}
	public String getBc_barcode() {
		return bc_barcode;
	}
	public void setBc_barcode(String bc_barcode) {
		this.bc_barcode = bc_barcode;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getPd_bd_name() {
		return pd_bd_name;
	}
	public void setPd_bd_name(String pd_bd_name) {
		this.pd_bd_name = pd_bd_name;
	}
	public String getPd_tp_name() {
		return pd_tp_name;
	}
	public void setPd_tp_name(String pd_tp_name) {
		this.pd_tp_name = pd_tp_name;
	}
	public Double getPd_sell_price() {
		return pd_sell_price;
	}
	public void setPd_sell_price(Double pd_sell_price) {
		this.pd_sell_price = pd_sell_price;
	}
	public Double getPd_vip_price() {
		return pd_vip_price;
	}
	public void setPd_vip_price(Double pd_vip_price) {
		this.pd_vip_price = pd_vip_price;
	}
	public Double getPd_sign_price() {
		return pd_sign_price;
	}
	public void setPd_sign_price(Double pd_sign_price) {
		this.pd_sign_price = pd_sign_price;
	}
	public String getPd_grade() {
		return pd_grade;
	}
	public void setPd_grade(String pd_grade) {
		this.pd_grade = pd_grade;
	}
	public String getPd_safe() {
		return pd_safe;
	}
	public void setPd_safe(String pd_safe) {
		this.pd_safe = pd_safe;
	}
	public String getPd_fill() {
		return pd_fill;
	}
	public void setPd_fill(String pd_fill) {
		this.pd_fill = pd_fill;
	}
	public String getPd_execute() {
		return pd_execute;
	}
	public void setPd_execute(String pd_execute) {
		this.pd_execute = pd_execute;
	}
	public Integer getBc_printnumber() {
		return bc_printnumber;
	}
	public void setBc_printnumber(Integer bc_printnumber) {
		this.bc_printnumber = bc_printnumber;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getPd_fabric() {
		return pd_fabric;
	}
	public void setPd_fabric(String pd_fabric) {
		this.pd_fabric = pd_fabric;
	}
	public String getPd_in_fabric() {
		return pd_in_fabric;
	}
	public void setPd_in_fabric(String pd_in_fabric) {
		this.pd_in_fabric = pd_in_fabric;
	}
	public String getPd_place() {
		return pd_place;
	}
	public void setPd_place(String pd_place) {
		this.pd_place = pd_place;
	}
	public String getPd_wash_explain() {
		return pd_wash_explain;
	}
	public void setPd_wash_explain(String pd_wash_explain) {
		this.pd_wash_explain = pd_wash_explain;
	}
}
