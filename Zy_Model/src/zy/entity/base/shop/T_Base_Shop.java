package zy.entity.base.shop;

import java.io.Serializable;

public class T_Base_Shop implements Serializable{
	private static final long serialVersionUID = -6237839209608014416L;
	private Integer sp_id;
	private String sp_code;
	private String sp_name;
	private String sp_spell;
	private String sp_upcode;
	private String sp_shop_type;
	private String ty_name;
	private Integer sp_state;
	private String sp_end;
	private Integer sp_main;
	private Integer sp_lock;
	private Integer sp_init;
	private Double sp_rate;
	private Double sp_init_debt;
	private Double sp_receivable;
	private Double sp_received;
	private Double sp_sms_money;
	private Integer sp_sort_cycle;
	private Integer sp_settle_cycle;
	private Double sp_prepay;
	
	private Integer version;
	
	private Integer spi_id;
	private String spi_man;
	private String spi_tel;
	private String spi_mobile;
	private String spi_addr;
	private String spi_province;
	private String spi_city;
	private String spi_town;
	private String spi_remark;
	private Integer companyid;
	private Integer reward_count;
	public Integer getSp_id() {
		return sp_id;
	}
	public void setSp_id(Integer sp_id) {
		this.sp_id = sp_id;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public String getSp_name() {
		return sp_name;
	}
	public void setSp_name(String sp_name) {
		this.sp_name = sp_name;
	}
	public String getSp_spell() {
		return sp_spell;
	}
	public void setSp_spell(String sp_spell) {
		this.sp_spell = sp_spell;
	}
	public String getSp_upcode() {
		return sp_upcode;
	}
	public void setSp_upcode(String sp_upcode) {
		this.sp_upcode = sp_upcode;
	}
	public String getSp_shop_type() {
		return sp_shop_type;
	}
	public void setSp_shop_type(String sp_shop_type) {
		this.sp_shop_type = sp_shop_type;
	}
	public String getTy_name() {
		return ty_name;
	}
	public void setTy_name(String ty_name) {
		this.ty_name = ty_name;
	}
	public Integer getSp_state() {
		return sp_state;
	}
	public void setSp_state(Integer sp_state) {
		this.sp_state = sp_state;
	}
	public Integer getSp_lock() {
		return sp_lock;
	}
	public void setSp_lock(Integer sp_lock) {
		this.sp_lock = sp_lock;
	}
	public String getSp_end() {
		return sp_end;
	}
	public void setSp_end(String sp_end) {
		this.sp_end = sp_end;
	}
	public Integer getSpi_id() {
		return spi_id;
	}
	public void setSpi_id(Integer spi_id) {
		this.spi_id = spi_id;
	}
	public String getSpi_man() {
		return spi_man;
	}
	public void setSpi_man(String spi_man) {
		this.spi_man = spi_man;
	}
	public String getSpi_tel() {
		return spi_tel;
	}
	public void setSpi_tel(String spi_tel) {
		this.spi_tel = spi_tel;
	}
	public String getSpi_mobile() {
		return spi_mobile;
	}
	public void setSpi_mobile(String spi_mobile) {
		this.spi_mobile = spi_mobile;
	}
	public String getSpi_addr() {
		return spi_addr;
	}
	public void setSpi_addr(String spi_addr) {
		this.spi_addr = spi_addr;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getSpi_province() {
		return spi_province;
	}
	public void setSpi_province(String spi_province) {
		this.spi_province = spi_province;
	}
	public String getSpi_city() {
		return spi_city;
	}
	public void setSpi_city(String spi_city) {
		this.spi_city = spi_city;
	}
	public String getSpi_town() {
		return spi_town;
	}
	public void setSpi_town(String spi_town) {
		this.spi_town = spi_town;
	}
	public Integer getSp_init() {
		return sp_init;
	}
	public void setSp_init(Integer sp_init) {
		this.sp_init = sp_init;
	}
	public Double getSp_rate() {
		return sp_rate;
	}
	public void setSp_rate(Double sp_rate) {
		this.sp_rate = sp_rate;
	}
	public Integer getSp_main() {
		return sp_main;
	}
	public void setSp_main(Integer sp_main) {
		this.sp_main = sp_main;
	}
	public Double getSp_init_debt() {
		return sp_init_debt;
	}
	public void setSp_init_debt(Double sp_init_debt) {
		this.sp_init_debt = sp_init_debt;
	}
	public Double getSp_receivable() {
		return sp_receivable;
	}
	public void setSp_receivable(Double sp_receivable) {
		this.sp_receivable = sp_receivable;
	}
	public Double getSp_received() {
		return sp_received;
	}
	public void setSp_received(Double sp_received) {
		this.sp_received = sp_received;
	}
	public Double getSp_sms_money() {
		return sp_sms_money;
	}
	public void setSp_sms_money(Double sp_sms_money) {
		this.sp_sms_money = sp_sms_money;
	}
	public Integer getSp_sort_cycle() {
		return sp_sort_cycle;
	}
	public void setSp_sort_cycle(Integer sp_sort_cycle) {
		this.sp_sort_cycle = sp_sort_cycle;
	}
	public Integer getSp_settle_cycle() {
		return sp_settle_cycle;
	}
	public void setSp_settle_cycle(Integer sp_settle_cycle) {
		this.sp_settle_cycle = sp_settle_cycle;
	}
	public Double getSp_prepay() {
		return sp_prepay;
	}
	public void setSp_prepay(Double sp_prepay) {
		this.sp_prepay = sp_prepay;
	}
	public String getSpi_remark() {
		return spi_remark;
	}
	public void setSpi_remark(String spi_remark) {
		this.spi_remark = spi_remark;
	}
	public Integer getReward_count() {
		return reward_count;
	}
	public void setReward_count(Integer reward_count) {
		this.reward_count = reward_count;
	}
	public Integer getVersion() {
		return version;
	}
	public void setVersion(Integer version) {
		this.version = version;
	}
	
}
