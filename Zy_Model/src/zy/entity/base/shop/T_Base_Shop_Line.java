package zy.entity.base.shop;

public class T_Base_Shop_Line extends T_Base_Shop{
	private static final long serialVersionUID = -8148067630396919067L;
	private Integer spl_id;
	private String spl_shop_code;
	private String spl_ordershop_code;
	private String spl_project;
	private String spl_logo;
	private String spl_office_hour;
	private String spl_longitude;
	private String spl_latitude;
	private Integer spl_state;
	private String spl_sysdate;
	private String ordershop_name;
	public Integer getSpl_id() {
		return spl_id;
	}
	public void setSpl_id(Integer spl_id) {
		this.spl_id = spl_id;
	}
	public String getSpl_shop_code() {
		return spl_shop_code;
	}
	public void setSpl_shop_code(String spl_shop_code) {
		this.spl_shop_code = spl_shop_code;
	}
	public String getSpl_ordershop_code() {
		return spl_ordershop_code;
	}
	public void setSpl_ordershop_code(String spl_ordershop_code) {
		this.spl_ordershop_code = spl_ordershop_code;
	}
	public String getSpl_project() {
		return spl_project;
	}
	public void setSpl_project(String spl_project) {
		this.spl_project = spl_project;
	}
	public String getSpl_logo() {
		return spl_logo;
	}
	public void setSpl_logo(String spl_logo) {
		this.spl_logo = spl_logo;
	}
	public String getSpl_office_hour() {
		return spl_office_hour;
	}
	public void setSpl_office_hour(String spl_office_hour) {
		this.spl_office_hour = spl_office_hour;
	}
	public String getSpl_longitude() {
		return spl_longitude;
	}
	public void setSpl_longitude(String spl_longitude) {
		this.spl_longitude = spl_longitude;
	}
	public String getSpl_latitude() {
		return spl_latitude;
	}
	public void setSpl_latitude(String spl_latitude) {
		this.spl_latitude = spl_latitude;
	}
	public Integer getSpl_state() {
		return spl_state;
	}
	public void setSpl_state(Integer spl_state) {
		this.spl_state = spl_state;
	}
	public String getSpl_sysdate() {
		return spl_sysdate;
	}
	public void setSpl_sysdate(String spl_sysdate) {
		this.spl_sysdate = spl_sysdate;
	}
	public String getOrdershop_name() {
		return ordershop_name;
	}
	public void setOrdershop_name(String ordershop_name) {
		this.ordershop_name = ordershop_name;
	}
}
