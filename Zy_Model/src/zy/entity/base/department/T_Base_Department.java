package zy.entity.base.department;

import java.io.Serializable;

public class T_Base_Department implements Serializable{
	private static final long serialVersionUID = -3063155428478877709L;
	private Integer dm_id;
	private String dm_code;
	private String dm_name;
	private String dm_spell;
	private String dm_shop_code;
	private Integer companyid;

	public Integer getDm_id() {
		return dm_id;
	}

	public void setDm_id(Integer dm_id) {
		this.dm_id = dm_id;
	}

	public String getDm_code() {
		return dm_code;
	}

	public void setDm_code(String dm_code) {
		this.dm_code = dm_code;
	}

	public String getDm_name() {
		return dm_name;
	}

	public void setDm_name(String dm_name) {
		this.dm_name = dm_name;
	}

	public String getDm_spell() {
		return dm_spell;
	}

	public void setDm_spell(String dm_spell) {
		this.dm_spell = dm_spell;
	}

	public String getDm_shop_code() {
		return dm_shop_code;
	}

	public void setDm_shop_code(String dm_shop_code) {
		this.dm_shop_code = dm_shop_code;
	}

	public Integer getCompanyid() {
		return companyid;
	}

	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}

}
