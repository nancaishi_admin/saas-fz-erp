package zy.entity.base.size;

import java.io.Serializable;

public class T_Base_Size implements Serializable{
	private static final long serialVersionUID = 1L;
	private Integer sz_id;
	private String sz_code;
	private String sz_name;
	private Integer companyid;
	public Integer getSz_id() {
		return sz_id;
	}
	public void setSz_id(Integer sz_id) {
		this.sz_id = sz_id;
	}
	public String getSz_code() {
		return sz_code;
	}
	public void setSz_code(String sz_code) {
		this.sz_code = sz_code;
	}
	public String getSz_name() {
		return sz_name;
	}
	public void setSz_name(String sz_name) {
		this.sz_name = sz_name;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	
}
