package zy.entity.base.size;

import java.io.Serializable;
import java.util.List;

public class T_Base_Size_Group implements Serializable{
	private static final long serialVersionUID = -3063155428478877709L;
	private Integer szg_id;
	private String szg_code;
	private String szg_name;
	private Integer szg_number;
	private Integer companyid;
	
	private List<T_Base_SizeList> sizelist;

	public Integer getSzg_id() {
		return szg_id;
	}

	public void setSzg_id(Integer szg_id) {
		this.szg_id = szg_id;
	}

	public String getSzg_code() {
		return szg_code;
	}

	public void setSzg_code(String szg_code) {
		this.szg_code = szg_code;
	}

	public String getSzg_name() {
		return szg_name;
	}

	public void setSzg_name(String szg_name) {
		this.szg_name = szg_name;
	}

	public Integer getSzg_number() {
		return szg_number;
	}

	public void setSzg_number(Integer szg_number) {
		this.szg_number = szg_number;
	}

	public Integer getCompanyid() {
		return companyid;
	}

	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}

	public List<T_Base_SizeList> getSizelist() {
		return sizelist;
	}

	public void setSizelist(List<T_Base_SizeList> sizelist) {
		this.sizelist = sizelist;
	}

}
