package zy.entity.base.brand;

import java.io.Serializable;

public class T_Base_Brand implements Serializable{
	private static final long serialVersionUID = -8715084882551183559L;
	private Integer bd_id;
	private String bd_code;
	private String bd_name;
	private String bd_spell;
	private String bd_state;
	private Integer companyid;
	public Integer getBd_id() {
		return bd_id;
	}
	public void setBd_id(Integer bd_id) {
		this.bd_id = bd_id;
	}
	public String getBd_code() {
		return bd_code;
	}
	public void setBd_code(String bd_code) {
		this.bd_code = bd_code;
	}
	public String getBd_name() {
		return bd_name;
	}
	public void setBd_name(String bd_name) {
		this.bd_name = bd_name;
	}
	public String getBd_spell() {
		return bd_spell;
	}
	public void setBd_spell(String bd_spell) {
		this.bd_spell = bd_spell;
	}
	public String getBd_state() {
		return bd_state;
	}
	public void setBd_state(String bd_state) {
		this.bd_state = bd_state;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
