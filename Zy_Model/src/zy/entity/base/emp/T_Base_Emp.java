package zy.entity.base.emp;

import java.io.Serializable;

public class T_Base_Emp implements Serializable{
	private static final long serialVersionUID = -3063155428478877709L;
	private Integer em_id;
	private String em_code;
	private String em_name;
	private String em_sex;
	private String em_marry;
	private String em_cardid;
	private String em_mobile;
	private String em_dm_code;
	private String em_dm_name;
	private String em_addr;
	private String em_birthday;
	private Integer em_state;
	private String em_state_name;
	private String em_workdate;
	private String em_indate;
	private String em_outdate;
	private String em_shop_code;
	private String em_shop_name;
	private String em_login_shop;
	private Integer em_type;
	private String em_type_name;
	private String em_img;
	private String em_pass;
	private Double em_royaltyrate;
	private Integer companyid;
	private Integer reward_count;
	public Integer getEm_id() {
		return em_id;
	}
	public void setEm_id(Integer em_id) {
		this.em_id = em_id;
	}
	public String getEm_code() {
		return em_code;
	}
	public void setEm_code(String em_code) {
		this.em_code = em_code;
	}
	public String getEm_name() {
		return em_name;
	}
	public void setEm_name(String em_name) {
		this.em_name = em_name;
	}
	public String getEm_sex() {
		return em_sex;
	}
	public void setEm_sex(String em_sex) {
		this.em_sex = em_sex;
	}
	public String getEm_marry() {
		return em_marry;
	}
	public void setEm_marry(String em_marry) {
		this.em_marry = em_marry;
	}
	public String getEm_cardid() {
		return em_cardid;
	}
	public void setEm_cardid(String em_cardid) {
		this.em_cardid = em_cardid;
	}
	public String getEm_mobile() {
		return em_mobile;
	}
	public void setEm_mobile(String em_mobile) {
		this.em_mobile = em_mobile;
	}
	public String getEm_dm_code() {
		return em_dm_code;
	}
	public void setEm_dm_code(String em_dm_code) {
		this.em_dm_code = em_dm_code;
	}
	public String getEm_addr() {
		return em_addr;
	}
	public void setEm_addr(String em_addr) {
		this.em_addr = em_addr;
	}
	public String getEm_birthday() {
		return em_birthday;
	}
	public void setEm_birthday(String em_birthday) {
		this.em_birthday = em_birthday;
	}
	public Integer getEm_state() {
		return em_state;
	}
	public void setEm_state(Integer em_state) {
		this.em_state = em_state;
	}
	public String getEm_workdate() {
		return em_workdate;
	}
	public void setEm_workdate(String em_workdate) {
		this.em_workdate = em_workdate;
	}
	public String getEm_indate() {
		return em_indate;
	}
	public void setEm_indate(String em_indate) {
		this.em_indate = em_indate;
	}
	public String getEm_outdate() {
		return em_outdate;
	}
	public void setEm_outdate(String em_outdate) {
		this.em_outdate = em_outdate;
	}
	public String getEm_shop_code() {
		return em_shop_code;
	}
	public void setEm_shop_code(String em_shop_code) {
		this.em_shop_code = em_shop_code;
	}
	public Integer getEm_type() {
		return em_type;
	}
	public void setEm_type(Integer em_type) {
		this.em_type = em_type;
	}
	public String getEm_img() {
		return em_img;
	}
	public void setEm_img(String em_img) {
		this.em_img = em_img;
	}
	public String getEm_pass() {
		return em_pass;
	}
	public void setEm_pass(String em_pass) {
		this.em_pass = em_pass;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public Double getEm_royaltyrate() {
		return em_royaltyrate;
	}
	public void setEm_royaltyrate(Double em_royaltyrate) {
		this.em_royaltyrate = em_royaltyrate;
	}
	public String getEm_dm_name() {
		return em_dm_name;
	}
	public void setEm_dm_name(String em_dm_name) {
		this.em_dm_name = em_dm_name;
	}
	public String getEm_state_name() {
		return em_state_name;
	}
	public void setEm_state_name(String em_state_name) {
		this.em_state_name = em_state_name;
	}
	public String getEm_shop_name() {
		return em_shop_name;
	}
	public void setEm_shop_name(String em_shop_name) {
		this.em_shop_name = em_shop_name;
	}
	public String getEm_type_name() {
		return em_type_name;
	}
	public void setEm_type_name(String em_type_name) {
		this.em_type_name = em_type_name;
	}
	public String getEm_login_shop() {
		return em_login_shop;
	}
	public void setEm_login_shop(String em_login_shop) {
		this.em_login_shop = em_login_shop;
	}
	public Integer getReward_count() {
		return reward_count;
	}
	public void setReward_count(Integer reward_count) {
		this.reward_count = reward_count;
	}
}
