package zy.entity.stock.data;

import java.io.Serializable;

public class T_Stock_Data implements Serializable{
	private static final long serialVersionUID = 3809953709442417118L;
	private Long sd_id;
	private String sd_pd_code;
	private String pd_no;
	private String pd_name;
	private String pd_unit;
	private String cr_name;
	private String sz_name;
	private String br_name;
	private Double pd_price;
	private String sd_code;
	private String sd_cr_code;
	private String sd_sz_code;
	private String sd_br_code;
	private String sd_dp_code;
	private String shop_code; 
	private String dp_name;
	Integer sd_amount;
	private Double sd_money;
	private Double sd_rate;
	private Integer sd_init;
	private String sd_date;
	private Integer companyid;
	public Long getSd_id() {
		return sd_id;
	}
	public void setSd_id(Long sd_id) {
		this.sd_id = sd_id;
	}
	public String getSd_pd_code() {
		return sd_pd_code;
	}
	public void setSd_pd_code(String sd_pd_code) {
		this.sd_pd_code = sd_pd_code;
	}
	public String getSd_code() {
		return sd_code;
	}
	public void setSd_code(String sd_code) {
		this.sd_code = sd_code;
	}
	public String getSd_cr_code() {
		return sd_cr_code;
	}
	public void setSd_cr_code(String sd_cr_code) {
		this.sd_cr_code = sd_cr_code;
	}
	public String getSd_sz_code() {
		return sd_sz_code;
	}
	public void setSd_sz_code(String sd_sz_code) {
		this.sd_sz_code = sd_sz_code;
	}
	public String getSd_br_code() {
		return sd_br_code;
	}
	public void setSd_br_code(String sd_br_code) {
		this.sd_br_code = sd_br_code;
	}
	public String getSd_dp_code() {
		return sd_dp_code;
	}
	public void setSd_dp_code(String sd_dp_code) {
		this.sd_dp_code = sd_dp_code;
	}
	public Integer getSd_amount() {
		return sd_amount;
	}
	public void setSd_amount(Integer sd_amount) {
		this.sd_amount = sd_amount;
	}
	public Integer getSd_init() {
		return sd_init;
	}
	public void setSd_init(Integer sd_init) {
		this.sd_init = sd_init;
	}
	public String getSd_date() {
		return sd_date;
	}
	public void setSd_date(String sd_date) {
		this.sd_date = sd_date;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getPd_no() {
		return pd_no;
	}
	public void setPd_no(String pd_no) {
		this.pd_no = pd_no;
	}
	public String getPd_name() {
		return pd_name;
	}
	public void setPd_name(String pd_name) {
		this.pd_name = pd_name;
	}
	public String getPd_unit() {
		return pd_unit;
	}
	public void setPd_unit(String pd_unit) {
		this.pd_unit = pd_unit;
	}
	public String getCr_name() {
		return cr_name;
	}
	public void setCr_name(String cr_name) {
		this.cr_name = cr_name;
	}
	public String getSz_name() {
		return sz_name;
	}
	public void setSz_name(String sz_name) {
		this.sz_name = sz_name;
	}
	public String getBr_name() {
		return br_name;
	}
	public void setBr_name(String br_name) {
		this.br_name = br_name;
	}
	public Double getPd_price() {
		return pd_price;
	}
	public void setPd_price(Double pd_price) {
		this.pd_price = pd_price;
	}
	public String getDp_name() {
		return dp_name;
	}
	public void setDp_name(String dp_name) {
		this.dp_name = dp_name;
	}
	public String getShop_code() {
		return shop_code;
	}
	public void setShop_code(String shop_code) {
		this.shop_code = shop_code;
	}
	public Double getSd_money() {
		return sd_money;
	}
	public void setSd_money(Double sd_money) {
		this.sd_money = sd_money;
	}
	public Double getSd_rate() {
		return sd_rate;
	}
	public void setSd_rate(Double sd_rate) {
		this.sd_rate = sd_rate;
	}
	
}
