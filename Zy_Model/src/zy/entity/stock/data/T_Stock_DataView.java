package zy.entity.stock.data;

public class T_Stock_DataView extends T_Stock_Data{
	private static final long serialVersionUID = -8831429274773071915L;
	private String pd_bd_code;
	private String pd_tp_code;
	private String pd_date;
	private Integer pd_year;
	private String pd_season;
	private String pd_style;
	private String pd_fabric;
	private Double pd_cost_price;
	private Double pd_sell_price;
	private String bd_name;
	private String tp_name;
	private Double cost_money;
	private Double sell_money;
	public String getPd_bd_code() {
		return pd_bd_code;
	}
	public void setPd_bd_code(String pd_bd_code) {
		this.pd_bd_code = pd_bd_code;
	}
	public String getPd_tp_code() {
		return pd_tp_code;
	}
	public void setPd_tp_code(String pd_tp_code) {
		this.pd_tp_code = pd_tp_code;
	}
	public String getPd_date() {
		return pd_date;
	}
	public void setPd_date(String pd_date) {
		this.pd_date = pd_date;
	}
	public Integer getPd_year() {
		return pd_year;
	}
	public void setPd_year(Integer pd_year) {
		this.pd_year = pd_year;
	}
	public String getPd_season() {
		return pd_season;
	}
	public void setPd_season(String pd_season) {
		this.pd_season = pd_season;
	}
	public String getPd_style() {
		return pd_style;
	}
	public void setPd_style(String pd_style) {
		this.pd_style = pd_style;
	}
	public String getPd_fabric() {
		return pd_fabric;
	}
	public void setPd_fabric(String pd_fabric) {
		this.pd_fabric = pd_fabric;
	}
	public Double getPd_cost_price() {
		return pd_cost_price;
	}
	public void setPd_cost_price(Double pd_cost_price) {
		this.pd_cost_price = pd_cost_price;
	}
	public Double getPd_sell_price() {
		return pd_sell_price;
	}
	public void setPd_sell_price(Double pd_sell_price) {
		this.pd_sell_price = pd_sell_price;
	}
	public String getBd_name() {
		return bd_name;
	}
	public void setBd_name(String bd_name) {
		this.bd_name = bd_name;
	}
	public String getTp_name() {
		return tp_name;
	}
	public void setTp_name(String tp_name) {
		this.tp_name = tp_name;
	}
	public Double getCost_money() {
		if (sd_amount != null && pd_cost_price != null) {
			return sd_amount * pd_cost_price;
		}
		return cost_money;
	}
	public void setCost_money(Double cost_money) {
		this.cost_money = cost_money;
	}
	public Double getSell_money() {
		if (sd_amount != null && pd_sell_price != null) {
			return sd_amount * pd_sell_price;
		}
		return sell_money;
	}
	public void setSell_money(Double sell_money) {
		this.sell_money = sell_money;
	}
}
