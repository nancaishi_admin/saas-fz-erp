package zy.entity.stock.price;

import java.io.Serializable;

public class T_Stock_PriceList implements Serializable{
	private static final long serialVersionUID = -6955163654356058291L;
	private Integer pcl_id;
	private String pcl_number;
	private String pcl_pd_code;
	private Double pcl_oldprice;
	private Double pcl_newprice;
	private Integer pcl_us_id;
	private Integer companyid;
	
	private String pd_no;
	private String pd_name;
	private String pd_unit;
	public Integer getPcl_id() {
		return pcl_id;
	}
	public void setPcl_id(Integer pcl_id) {
		this.pcl_id = pcl_id;
	}
	public String getPcl_number() {
		return pcl_number;
	}
	public void setPcl_number(String pcl_number) {
		this.pcl_number = pcl_number;
	}
	public String getPcl_pd_code() {
		return pcl_pd_code;
	}
	public void setPcl_pd_code(String pcl_pd_code) {
		this.pcl_pd_code = pcl_pd_code;
	}
	public Double getPcl_oldprice() {
		return pcl_oldprice;
	}
	public void setPcl_oldprice(Double pcl_oldprice) {
		this.pcl_oldprice = pcl_oldprice;
	}
	public Double getPcl_newprice() {
		return pcl_newprice;
	}
	public void setPcl_newprice(Double pcl_newprice) {
		this.pcl_newprice = pcl_newprice;
	}
	public Integer getPcl_us_id() {
		return pcl_us_id;
	}
	public void setPcl_us_id(Integer pcl_us_id) {
		this.pcl_us_id = pcl_us_id;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getPd_no() {
		return pd_no;
	}
	public void setPd_no(String pd_no) {
		this.pd_no = pd_no;
	}
	public String getPd_name() {
		return pd_name;
	}
	public void setPd_name(String pd_name) {
		this.pd_name = pd_name;
	}
	public String getPd_unit() {
		return pd_unit;
	}
	public void setPd_unit(String pd_unit) {
		this.pd_unit = pd_unit;
	}
}
