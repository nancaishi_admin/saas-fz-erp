package zy.entity.stock.check;

import java.io.Serializable;

public class T_Stock_BatchScope implements Serializable{
	private static final long serialVersionUID = 6175474256804947351L;
	private Integer bs_id;
	private String bs_ba_number;
	private Integer bs_scope;
	private String bs_scope_code;
	private String bs_scope_name;
	private Integer companyid;
	public Integer getBs_id() {
		return bs_id;
	}
	public void setBs_id(Integer bs_id) {
		this.bs_id = bs_id;
	}
	public String getBs_ba_number() {
		return bs_ba_number;
	}
	public void setBs_ba_number(String bs_ba_number) {
		this.bs_ba_number = bs_ba_number;
	}
	public Integer getBs_scope() {
		return bs_scope;
	}
	public void setBs_scope(Integer bs_scope) {
		this.bs_scope = bs_scope;
	}
	public String getBs_scope_code() {
		return bs_scope_code;
	}
	public void setBs_scope_code(String bs_scope_code) {
		this.bs_scope_code = bs_scope_code;
	}
	public String getBs_scope_name() {
		return bs_scope_name;
	}
	public void setBs_scope_name(String bs_scope_name) {
		this.bs_scope_name = bs_scope_name;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
