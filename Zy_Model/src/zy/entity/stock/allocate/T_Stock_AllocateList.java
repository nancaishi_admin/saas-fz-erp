package zy.entity.stock.allocate;

import java.io.Serializable;

public class T_Stock_AllocateList implements Serializable{
	private static final long serialVersionUID = 2388178449735991152L;
	private Integer acl_id;
	private String acl_number;
	private String acl_pd_code;
	private String acl_sub_code;
	private String acl_sz_code;
	private String acl_szg_code;
	private String acl_cr_code;
	private String acl_br_code;
	private Integer acl_amount;
	private Double acl_unitprice;
	private Double acl_unitmoney;
	private String acl_remark;
	private Integer acl_us_id;
	private Integer companyid;
	
	private String pd_no;
	private String pd_name;
	private String pd_unit;
	private String pd_season;
	private Integer pd_year;
	private String cr_name;
	private String sz_name;
	private String br_name;
	private String bd_code;
	private String tp_code;
	private String bd_name;
	private String tp_name;
	/**
	 * 操作类型 update,add
	 */
	private String operate_type;
	public Integer getAcl_id() {
		return acl_id;
	}
	public void setAcl_id(Integer acl_id) {
		this.acl_id = acl_id;
	}
	public String getAcl_number() {
		return acl_number;
	}
	public void setAcl_number(String acl_number) {
		this.acl_number = acl_number;
	}
	public String getAcl_pd_code() {
		return acl_pd_code;
	}
	public void setAcl_pd_code(String acl_pd_code) {
		this.acl_pd_code = acl_pd_code;
	}
	public String getAcl_sub_code() {
		return acl_sub_code;
	}
	public void setAcl_sub_code(String acl_sub_code) {
		this.acl_sub_code = acl_sub_code;
	}
	public String getAcl_sz_code() {
		return acl_sz_code;
	}
	public void setAcl_sz_code(String acl_sz_code) {
		this.acl_sz_code = acl_sz_code;
	}
	public String getAcl_szg_code() {
		return acl_szg_code;
	}
	public void setAcl_szg_code(String acl_szg_code) {
		this.acl_szg_code = acl_szg_code;
	}
	public String getAcl_cr_code() {
		return acl_cr_code;
	}
	public void setAcl_cr_code(String acl_cr_code) {
		this.acl_cr_code = acl_cr_code;
	}
	public String getAcl_br_code() {
		return acl_br_code;
	}
	public void setAcl_br_code(String acl_br_code) {
		this.acl_br_code = acl_br_code;
	}
	public Integer getAcl_amount() {
		return acl_amount;
	}
	public void setAcl_amount(Integer acl_amount) {
		this.acl_amount = acl_amount;
	}
	public Double getAcl_unitprice() {
		return acl_unitprice;
	}
	public void setAcl_unitprice(Double acl_unitprice) {
		this.acl_unitprice = acl_unitprice;
	}
	public Double getAcl_unitmoney() {
		if (acl_amount != null && acl_unitprice != null) {
			return acl_amount * acl_unitprice;
		}
		return acl_unitmoney;
	}
	public String getAcl_remark() {
		return acl_remark;
	}
	public void setAcl_remark(String acl_remark) {
		this.acl_remark = acl_remark;
	}
	public Integer getAcl_us_id() {
		return acl_us_id;
	}
	public void setAcl_us_id(Integer acl_us_id) {
		this.acl_us_id = acl_us_id;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getPd_no() {
		return pd_no;
	}
	public void setPd_no(String pd_no) {
		this.pd_no = pd_no;
	}
	public String getPd_name() {
		return pd_name;
	}
	public void setPd_name(String pd_name) {
		this.pd_name = pd_name;
	}
	public String getPd_unit() {
		return pd_unit;
	}
	public void setPd_unit(String pd_unit) {
		this.pd_unit = pd_unit;
	}
	public String getPd_season() {
		return pd_season;
	}
	public void setPd_season(String pd_season) {
		this.pd_season = pd_season;
	}
	public Integer getPd_year() {
		return pd_year;
	}
	public void setPd_year(Integer pd_year) {
		this.pd_year = pd_year;
	}
	public String getCr_name() {
		return cr_name;
	}
	public void setCr_name(String cr_name) {
		this.cr_name = cr_name;
	}
	public String getSz_name() {
		return sz_name;
	}
	public void setSz_name(String sz_name) {
		this.sz_name = sz_name;
	}
	public String getBr_name() {
		return br_name;
	}
	public void setBr_name(String br_name) {
		this.br_name = br_name;
	}
	public String getBd_name() {
		return bd_name;
	}
	public void setBd_name(String bd_name) {
		this.bd_name = bd_name;
	}
	public String getTp_name() {
		return tp_name;
	}
	public void setTp_name(String tp_name) {
		this.tp_name = tp_name;
	}
	public String getOperate_type() {
		return operate_type;
	}
	public void setOperate_type(String operate_type) {
		this.operate_type = operate_type;
	}
	public String getBd_code() {
		return bd_code;
	}
	public void setBd_code(String bd_code) {
		this.bd_code = bd_code;
	}
	public String getTp_code() {
		return tp_code;
	}
	public void setTp_code(String tp_code) {
		this.tp_code = tp_code;
	}
	public void setAcl_unitmoney(Double acl_unitmoney) {
		this.acl_unitmoney = acl_unitmoney;
	}
	
}
