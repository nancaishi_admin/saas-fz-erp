package zy.entity.stock;

import zy.entity.base.product.T_Base_Barcode;

public class T_Stock_Import extends T_Base_Barcode{
	private static final long serialVersionUID = -2887660030941620374L;
	private String pd_szg_code;
	private Double unit_price;
	public String getPd_szg_code() {
		return pd_szg_code;
	}
	public void setPd_szg_code(String pd_szg_code) {
		this.pd_szg_code = pd_szg_code;
	}
	public Double getUnit_price() {
		return unit_price;
	}
	public void setUnit_price(Double unit_price) {
		this.unit_price = unit_price;
	}
}
