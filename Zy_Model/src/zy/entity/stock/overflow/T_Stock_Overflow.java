package zy.entity.stock.overflow;

import java.io.Serializable;

public class T_Stock_Overflow implements Serializable{
	private static final long serialVersionUID = 7279904725257617250L;
	private Integer of_id;
	private String of_number;
	private String of_date;
	private String of_dp_code;
	private String depot_name;
	private String of_manager;
	private Integer of_amount;
	private Double of_money;
	private String of_remark;
	private Integer of_ar_state;
	private String of_ar_date;
	private Integer of_isdraft;
	private String of_sysdate;
	private Integer of_us_id;
	private String of_maker;
	private Integer companyid;
	private String ar_describe;
	public Integer getOf_id() {
		return of_id;
	}
	public void setOf_id(Integer of_id) {
		this.of_id = of_id;
	}
	public String getOf_number() {
		return of_number;
	}
	public void setOf_number(String of_number) {
		this.of_number = of_number;
	}
	public String getOf_date() {
		return of_date;
	}
	public void setOf_date(String of_date) {
		this.of_date = of_date;
	}
	public String getOf_dp_code() {
		return of_dp_code;
	}
	public void setOf_dp_code(String of_dp_code) {
		this.of_dp_code = of_dp_code;
	}
	public String getDepot_name() {
		return depot_name;
	}
	public void setDepot_name(String depot_name) {
		this.depot_name = depot_name;
	}
	public String getOf_manager() {
		return of_manager;
	}
	public void setOf_manager(String of_manager) {
		this.of_manager = of_manager;
	}
	public Integer getOf_amount() {
		return of_amount;
	}
	public void setOf_amount(Integer of_amount) {
		this.of_amount = of_amount;
	}
	public Double getOf_money() {
		return of_money;
	}
	public void setOf_money(Double of_money) {
		this.of_money = of_money;
	}
	public String getOf_remark() {
		return of_remark;
	}
	public void setOf_remark(String of_remark) {
		this.of_remark = of_remark;
	}
	public Integer getOf_ar_state() {
		return of_ar_state;
	}
	public void setOf_ar_state(Integer of_ar_state) {
		this.of_ar_state = of_ar_state;
	}
	public String getOf_ar_date() {
		return of_ar_date;
	}
	public void setOf_ar_date(String of_ar_date) {
		this.of_ar_date = of_ar_date;
	}
	public Integer getOf_isdraft() {
		return of_isdraft;
	}
	public void setOf_isdraft(Integer of_isdraft) {
		this.of_isdraft = of_isdraft;
	}
	public String getOf_sysdate() {
		return of_sysdate;
	}
	public void setOf_sysdate(String of_sysdate) {
		this.of_sysdate = of_sysdate;
	}
	public Integer getOf_us_id() {
		return of_us_id;
	}
	public void setOf_us_id(Integer of_us_id) {
		this.of_us_id = of_us_id;
	}
	public String getOf_maker() {
		return of_maker;
	}
	public void setOf_maker(String of_maker) {
		this.of_maker = of_maker;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getAr_describe() {
		return ar_describe;
	}
	public void setAr_describe(String ar_describe) {
		this.ar_describe = ar_describe;
	}
}
