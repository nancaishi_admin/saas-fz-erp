package zy.entity.stock.useable;

import java.io.Serializable;

public class T_Stock_Useable implements Serializable{
	private static final long serialVersionUID = -613733799971554621L;
	private Integer ua_id;
	private String ua_code;
	private String ua_name;
	private String ua_remark;
	private Integer ua_type;
	private Integer ua_join;
	private Integer companyid;
	public Integer getUa_id() {
		return ua_id;
	}
	public void setUa_id(Integer ua_id) {
		this.ua_id = ua_id;
	}
	public String getUa_code() {
		return ua_code;
	}
	public void setUa_code(String ua_code) {
		this.ua_code = ua_code;
	}
	public String getUa_name() {
		return ua_name;
	}
	public void setUa_name(String ua_name) {
		this.ua_name = ua_name;
	}
	public String getUa_remark() {
		return ua_remark;
	}
	public void setUa_remark(String ua_remark) {
		this.ua_remark = ua_remark;
	}
	public Integer getUa_type() {
		return ua_type;
	}
	public void setUa_type(Integer ua_type) {
		this.ua_type = ua_type;
	}
	public Integer getUa_join() {
		return ua_join;
	}
	public void setUa_join(Integer ua_join) {
		this.ua_join = ua_join;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
