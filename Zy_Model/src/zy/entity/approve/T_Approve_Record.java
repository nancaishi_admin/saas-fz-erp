package zy.entity.approve;

import java.io.Serializable;

public class T_Approve_Record implements Serializable{
	private static final long serialVersionUID = 2090131398185297172L;
	private Integer ar_id;
	private String ar_number;
	private Integer ar_state;
	private String ar_describe;
	private String ar_sysdate;
	private String ar_type;
	private String ar_us_name;
	private Integer companyid;
	public Integer getAr_id() {
		return ar_id;
	}
	public void setAr_id(Integer ar_id) {
		this.ar_id = ar_id;
	}
	public String getAr_number() {
		return ar_number;
	}
	public void setAr_number(String ar_number) {
		this.ar_number = ar_number;
	}
	public Integer getAr_state() {
		return ar_state;
	}
	public void setAr_state(Integer ar_state) {
		this.ar_state = ar_state;
	}
	public String getAr_describe() {
		return ar_describe;
	}
	public void setAr_describe(String ar_describe) {
		this.ar_describe = ar_describe;
	}
	public String getAr_sysdate() {
		return ar_sysdate;
	}
	public void setAr_sysdate(String ar_sysdate) {
		this.ar_sysdate = ar_sysdate;
	}
	public String getAr_type() {
		return ar_type;
	}
	public void setAr_type(String ar_type) {
		this.ar_type = ar_type;
	}
	public String getAr_us_name() {
		return ar_us_name;
	}
	public void setAr_us_name(String ar_us_name) {
		this.ar_us_name = ar_us_name;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
