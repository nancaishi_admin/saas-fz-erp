package zy.entity.sell.day;

import java.io.Serializable;

public class T_Sell_Weather implements Serializable{
	private static final long serialVersionUID = 1L;
	private Integer we_id;
	private String we_shop_code;
	private String we_city;
	private String we_wind;
	private Integer we_max_tmp;
	private Integer we_min_tmp;
	private Integer we_we_id;
	private String we_name;
	private String we_icon;
	private Integer companyid;
	private String we_date;
	private Integer we_tomorrow;
	public Integer getWe_id() {
		return we_id;
	}
	public void setWe_id(Integer we_id) {
		this.we_id = we_id;
	}
	public String getWe_shop_code() {
		return we_shop_code;
	}
	public void setWe_shop_code(String we_shop_code) {
		this.we_shop_code = we_shop_code;
	}
	public String getWe_city() {
		return we_city;
	}
	public void setWe_city(String we_city) {
		this.we_city = we_city;
	}
	public String getWe_wind() {
		return we_wind;
	}
	public void setWe_wind(String we_wind) {
		this.we_wind = we_wind;
	}
	public Integer getWe_max_tmp() {
		return we_max_tmp;
	}
	public void setWe_max_tmp(Integer we_max_tmp) {
		this.we_max_tmp = we_max_tmp;
	}
	public Integer getWe_min_tmp() {
		return we_min_tmp;
	}
	public void setWe_min_tmp(Integer we_min_tmp) {
		this.we_min_tmp = we_min_tmp;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getWe_date() {
		return we_date;
	}
	public void setWe_date(String we_date) {
		this.we_date = we_date;
	}
	public Integer getWe_we_id() {
		return we_we_id;
	}
	public void setWe_we_id(Integer we_we_id) {
		this.we_we_id = we_we_id;
	}
	public String getWe_name() {
		return we_name;
	}
	public void setWe_name(String we_name) {
		this.we_name = we_name;
	}
	public String getWe_icon() {
		return we_icon;
	}
	public void setWe_icon(String we_icon) {
		this.we_icon = we_icon;
	}
	public Integer getWe_tomorrow() {
		return we_tomorrow;
	}
	public void setWe_tomorrow(Integer we_tomorrow) {
		this.we_tomorrow = we_tomorrow;
	}
}
