package zy.entity.sell.set;

import java.io.Serializable;

public class T_Sell_Print implements Serializable{
	private static final long serialVersionUID = 1L;
	private Integer sp_id;
	private String sp_code;
	private String sp_title;
	private String sp_qrcode;
	private String sp_remark;
	private Integer sp_print;
	private Integer sp_auto;
	private String sp_shop_code;
	private Integer companyid;
	public Integer getSp_id() {
		return sp_id;
	}
	public void setSp_id(Integer sp_id) {
		this.sp_id = sp_id;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public String getSp_title() {
		return sp_title;
	}
	public void setSp_title(String sp_title) {
		this.sp_title = sp_title;
	}
	public String getSp_qrcode() {
		return sp_qrcode;
	}
	public void setSp_qrcode(String sp_qrcode) {
		this.sp_qrcode = sp_qrcode;
	}
	public Integer getSp_print() {
		return sp_print;
	}
	public void setSp_print(Integer sp_print) {
		this.sp_print = sp_print;
	}
	public Integer getSp_auto() {
		return sp_auto;
	}
	public void setSp_auto(Integer sp_auto) {
		this.sp_auto = sp_auto;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getSp_remark() {
		return sp_remark;
	}
	public void setSp_remark(String sp_remark) {
		this.sp_remark = sp_remark;
	}
	public String getSp_shop_code() {
		return sp_shop_code;
	}
	public void setSp_shop_code(String sp_shop_code) {
		this.sp_shop_code = sp_shop_code;
	}

}
