package zy.entity.sell.set;

import java.io.Serializable;

public class T_Sell_PrintSet implements Serializable{
	private static final long serialVersionUID = 1L;
	private Integer sps_id;
	private String sps_key;
	private String sps_value;
	private String sps_sp_code;
	private Integer companyid;
	
	public Integer getSps_id() {
		return sps_id;
	}
	public void setSps_id(Integer sps_id) {
		this.sps_id = sps_id;
	}
	public String getSps_key() {
		return sps_key;
	}
	public void setSps_key(String sps_key) {
		this.sps_key = sps_key;
	}
	public String getSps_value() {
		return sps_value;
	}
	public void setSps_value(String sps_value) {
		this.sps_value = sps_value;
	}
	public String getSps_sp_code() {
		return sps_sp_code;
	}
	public void setSps_sp_code(String sps_sp_code) {
		this.sps_sp_code = sps_sp_code;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
