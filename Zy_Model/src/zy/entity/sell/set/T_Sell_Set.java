package zy.entity.sell.set;

import java.io.Serializable;

public class T_Sell_Set implements Serializable{
	private static final long serialVersionUID = 1L;
	private Integer st_id;
	private String st_key;
	private String st_value;
	private String st_shop_code;
	private Integer companyid;
	public Integer getSt_id() {
		return st_id;
	}
	public void setSt_id(Integer st_id) {
		this.st_id = st_id;
	}
	public String getSt_key() {
		return st_key;
	}
	public void setSt_key(String st_key) {
		this.st_key = st_key;
	}
	public String getSt_value() {
		return st_value;
	}
	public void setSt_value(String st_value) {
		this.st_value = st_value;
	}
	public String getSt_shop_code() {
		return st_shop_code;
	}
	public void setSt_shop_code(String st_shop_code) {
		this.st_shop_code = st_shop_code;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	
}
