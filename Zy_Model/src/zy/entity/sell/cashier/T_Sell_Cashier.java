package zy.entity.sell.cashier;

import java.io.Serializable;

import zy.entity.sys.sqb.T_Sys_Sqb;

public class T_Sell_Cashier implements Serializable{
	private static final long serialVersionUID = 1L;
	private Integer ca_id;
	private String ca_em_code;
	private String em_name;
	private String ca_pass;
	private String ca_shop_code;
	private String shop_upcode;
	private String dp_code;
	private String dp_name;
	private String st_code;
	private String st_name;
	private String shop_type;
	private String shop_uptype;
	private String shop_name;
	private String shop_end;
	private Integer ca_state;
	private Double ca_erase;
	private Double ca_maxmoney;
	private Double ca_minrate;
	private Integer ca_days;
	private Integer companyid;
	private String companycode;
	private String ca_last_login;
	private T_Sys_Sqb sqb;
	public Integer getCa_id() {
		return ca_id;
	}
	public void setCa_id(Integer ca_id) {
		this.ca_id = ca_id;
	}
	public String getCa_em_code() {
		return ca_em_code;
	}
	public void setCa_em_code(String ca_em_code) {
		this.ca_em_code = ca_em_code;
	}
	public String getEm_name() {
		return em_name;
	}
	public void setEm_name(String em_name) {
		this.em_name = em_name;
	}
	public String getCa_pass() {
		return ca_pass;
	}
	public void setCa_pass(String ca_pass) {
		this.ca_pass = ca_pass;
	}
	public String getDp_code() {
		return dp_code;
	}
	public void setDp_code(String dp_code) {
		this.dp_code = dp_code;
	}
	public String getDp_name() {
		return dp_name;
	}
	public void setDp_name(String dp_name) {
		this.dp_name = dp_name;
	}
	public String getCa_shop_code() {
		return ca_shop_code;
	}
	public void setCa_shop_code(String ca_shop_code) {
		this.ca_shop_code = ca_shop_code;
	}
	public String getShop_upcode() {
		return shop_upcode;
	}
	public void setShop_upcode(String shop_upcode) {
		this.shop_upcode = shop_upcode;
	}
	public String getShop_type() {
		return shop_type;
	}
	public void setShop_type(String shop_type) {
		this.shop_type = shop_type;
	}
	public String getShop_uptype() {
		return shop_uptype;
	}
	public void setShop_uptype(String shop_uptype) {
		this.shop_uptype = shop_uptype;
	}
	public String getShop_name() {
		return shop_name;
	}
	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}
	public Integer getCa_state() {
		return ca_state;
	}
	public void setCa_state(Integer ca_state) {
		this.ca_state = ca_state;
	}
	public Double getCa_erase() {
		return ca_erase;
	}
	public void setCa_erase(Double ca_erase) {
		this.ca_erase = ca_erase;
	}
	public Double getCa_maxmoney() {
		return ca_maxmoney;
	}
	public void setCa_maxmoney(Double ca_maxmoney) {
		this.ca_maxmoney = ca_maxmoney;
	}
	public Double getCa_minrate() {
		return ca_minrate;
	}
	public void setCa_minrate(Double ca_minrate) {
		this.ca_minrate = ca_minrate;
	}
	public Integer getCa_days() {
		return ca_days;
	}
	public void setCa_days(Integer ca_days) {
		this.ca_days = ca_days;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getCompanycode() {
		return companycode;
	}
	public void setCompanycode(String companycode) {
		this.companycode = companycode;
	}
	public String getCa_last_login() {
		return ca_last_login;
	}
	public void setCa_last_login(String ca_last_login) {
		this.ca_last_login = ca_last_login;
	}
	public String getShop_end() {
		return shop_end;
	}
	public void setShop_end(String shop_end) {
		this.shop_end = shop_end;
	}
	public String getSt_code() {
		return st_code;
	}
	public void setSt_code(String st_code) {
		this.st_code = st_code;
	}
	public String getSt_name() {
		return st_name;
	}
	public void setSt_name(String st_name) {
		this.st_name = st_name;
	}
	public T_Sys_Sqb getSqb() {
		return sqb;
	}
	public void setSqb(T_Sys_Sqb sqb) {
		this.sqb = sqb;
	}
}
