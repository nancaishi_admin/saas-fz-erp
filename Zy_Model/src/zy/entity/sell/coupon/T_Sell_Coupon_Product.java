package zy.entity.sell.coupon;

import java.io.Serializable;

public class T_Sell_Coupon_Product implements Serializable{
	private static final long serialVersionUID = 1L;
	private Integer scp_id;
	private String scp_sc_number;
	private String scp_pd_code;
	private Integer companyid;
	public Integer getScp_id() {
		return scp_id;
	}
	public void setScp_id(Integer scp_id) {
		this.scp_id = scp_id;
	}
	public String getScp_sc_number() {
		return scp_sc_number;
	}
	public void setScp_sc_number(String scp_sc_number) {
		this.scp_sc_number = scp_sc_number;
	}
	public String getScp_pd_code() {
		return scp_pd_code;
	}
	public void setScp_pd_code(String scp_pd_code) {
		this.scp_pd_code = scp_pd_code;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
