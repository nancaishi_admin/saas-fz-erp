package zy.entity.sell.dayend;

import java.io.Serializable;

public class T_Sell_DayEnd implements Serializable{
	private static final long serialVersionUID = 1L;
	private Integer de_id;
	private String de_em_code;
	private String em_name;
	private String de_shop_code;
	private String shop_name;
	private String de_st_code;
	private String st_name;
	private Integer de_state;
	private Integer de_bills;
	private Integer de_sell_bills;
	private Integer de_back_bills;
	private Integer de_change_bills;
	private Double total_cash;
	private Double total_bank;
	private Integer de_amount;
	private Double de_sell_money;
	private Double de_money;
	private Integer companyid;
	private Double de_last_money;
	private Double de_petty_cash;
	private Integer de_gifts;
	private Integer de_vips;
	private Double de_cash;
	private Double de_ali_money;
	private Double de_wx_money;
	private Double de_cd_money;
	private Double de_vc_money;
	private Double de_oc_money;
	private Double de_sd_money;
	private Double de_point_money;
	private Double de_ec_money;
	private Double de_bank_money;
	private Double de_mall_money;
	private Double de_lost_money;
	private Integer de_cd_sends;
	private Double de_cd_send_cash;
	private Double de_cd_send_bank;
	private Double de_cd_fill_cash;
	private Double de_cd_fill_bank;
	
	private Integer de_vc_sends;
	private Double de_vc_send_money;
	private Double de_sd_send_money;
	private Double de_sd_back_money;
	private Integer de_vip_point;
	private String de_begindate;
	private String de_enddate;
	private String de_man;
	private Integer de_in_amount;
	private Integer de_out_amount;
	private Integer de_stock_amount;
	private Integer de_last_stock;
	private String de_in_cash;
	private String de_in_bank;
	private String bank_name;
	public Integer getDe_id() {
		return de_id;
	}
	public void setDe_id(Integer de_id) {
		this.de_id = de_id;
	}
	public String getDe_em_code() {
		return de_em_code;
	}
	public void setDe_em_code(String de_em_code) {
		this.de_em_code = de_em_code;
	}
	public String getDe_shop_code() {
		return de_shop_code;
	}
	public void setDe_shop_code(String de_shop_code) {
		this.de_shop_code = de_shop_code;
	}
	public String getDe_st_code() {
		return de_st_code;
	}
	public void setDe_st_code(String de_st_code) {
		this.de_st_code = de_st_code;
	}
	public String getSt_name() {
		return st_name;
	}
	public void setSt_name(String st_name) {
		this.st_name = st_name;
	}
	public Integer getDe_state() {
		return de_state;
	}
	public void setDe_state(Integer de_state) {
		this.de_state = de_state;
	}
	public Integer getDe_bills() {
		return de_bills;
	}
	public void setDe_bills(Integer de_bills) {
		this.de_bills = de_bills;
	}
	public Double getDe_sell_money() {
		return de_sell_money;
	}
	public void setDe_sell_money(Double de_sell_money) {
		this.de_sell_money = de_sell_money;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public Double getDe_last_money() {
		return de_last_money;
	}
	public void setDe_last_money(Double de_last_money) {
		this.de_last_money = de_last_money;
	}
	public Double getDe_petty_cash() {
		return de_petty_cash;
	}
	public void setDe_petty_cash(Double de_petty_cash) {
		this.de_petty_cash = de_petty_cash;
	}
	public Integer getDe_gifts() {
		return de_gifts;
	}
	public void setDe_gifts(Integer de_gifts) {
		this.de_gifts = de_gifts;
	}
	public Integer getDe_vips() {
		return de_vips;
	}
	public void setDe_vips(Integer de_vips) {
		this.de_vips = de_vips;
	}
	public Double getDe_cd_money() {
		return de_cd_money;
	}
	public void setDe_cd_money(Double de_cd_money) {
		this.de_cd_money = de_cd_money;
	}
	public Double getDe_vc_money() {
		return de_vc_money;
	}
	public void setDe_vc_money(Double de_vc_money) {
		this.de_vc_money = de_vc_money;
	}
	public Double getDe_oc_money() {
		return de_oc_money;
	}
	public void setDe_oc_money(Double de_oc_money) {
		this.de_oc_money = de_oc_money;
	}
	public Double getDe_sd_money() {
		return de_sd_money;
	}
	public void setDe_sd_money(Double de_sd_money) {
		this.de_sd_money = de_sd_money;
	}
	public Double getDe_point_money() {
		return de_point_money;
	}
	public void setDe_point_money(Double de_point_money) {
		this.de_point_money = de_point_money;
	}
	public Double getDe_ec_money() {
		return de_ec_money;
	}
	public void setDe_ec_money(Double de_ec_money) {
		this.de_ec_money = de_ec_money;
	}
	public Double getDe_bank_money() {
		return de_bank_money;
	}
	public void setDe_bank_money(Double de_bank_money) {
		this.de_bank_money = de_bank_money;
	}
	public Double getDe_mall_money() {
		return de_mall_money;
	}
	public void setDe_mall_money(Double de_mall_money) {
		this.de_mall_money = de_mall_money;
	}
	public Double getDe_lost_money() {
		return de_lost_money;
	}
	public void setDe_lost_money(Double de_lost_money) {
		this.de_lost_money = de_lost_money;
	}
	public Integer getDe_cd_sends() {
		return de_cd_sends;
	}
	public void setDe_cd_sends(Integer de_cd_sends) {
		this.de_cd_sends = de_cd_sends;
	}
	public Double getDe_cd_send_cash() {
		return de_cd_send_cash;
	}
	public void setDe_cd_send_cash(Double de_cd_send_cash) {
		this.de_cd_send_cash = de_cd_send_cash;
	}
	public Double getDe_cd_send_bank() {
		return de_cd_send_bank;
	}
	public void setDe_cd_send_bank(Double de_cd_send_bank) {
		this.de_cd_send_bank = de_cd_send_bank;
	}
	public Double getDe_cd_fill_cash() {
		return de_cd_fill_cash;
	}
	public void setDe_cd_fill_cash(Double de_cd_fill_cash) {
		this.de_cd_fill_cash = de_cd_fill_cash;
	}
	public Double getDe_cd_fill_bank() {
		return de_cd_fill_bank;
	}
	public void setDe_cd_fill_bank(Double de_cd_fill_bank) {
		this.de_cd_fill_bank = de_cd_fill_bank;
	}
	public Integer getDe_vc_sends() {
		return de_vc_sends;
	}
	public void setDe_vc_sends(Integer de_vc_sends) {
		this.de_vc_sends = de_vc_sends;
	}
	public Double getDe_vc_send_money() {
		return de_vc_send_money;
	}
	public void setDe_vc_send_money(Double de_vc_send_money) {
		this.de_vc_send_money = de_vc_send_money;
	}
	public Double getDe_sd_send_money() {
		return de_sd_send_money;
	}
	public void setDe_sd_send_money(Double de_sd_send_money) {
		this.de_sd_send_money = de_sd_send_money;
	}
	public Double getDe_sd_back_money() {
		return de_sd_back_money;
	}
	public void setDe_sd_back_money(Double de_sd_back_money) {
		this.de_sd_back_money = de_sd_back_money;
	}
	public Integer getDe_vip_point() {
		return de_vip_point;
	}
	public void setDe_vip_point(Integer de_vip_point) {
		this.de_vip_point = de_vip_point;
	}
	public String getDe_begindate() {
		return de_begindate;
	}
	public void setDe_begindate(String de_begindate) {
		this.de_begindate = de_begindate;
	}
	public String getDe_enddate() {
		return de_enddate;
	}
	public void setDe_enddate(String de_enddate) {
		this.de_enddate = de_enddate;
	}
	public String getDe_man() {
		return de_man;
	}
	public void setDe_man(String de_man) {
		this.de_man = de_man;
	}
	public Integer getDe_in_amount() {
		return de_in_amount;
	}
	public void setDe_in_amount(Integer de_in_amount) {
		this.de_in_amount = de_in_amount;
	}
	public Integer getDe_out_amount() {
		return de_out_amount;
	}
	public void setDe_out_amount(Integer de_out_amount) {
		this.de_out_amount = de_out_amount;
	}
	public Integer getDe_stock_amount() {
		return de_stock_amount;
	}
	public void setDe_stock_amount(Integer de_stock_amount) {
		this.de_stock_amount = de_stock_amount;
	}
	public String getDe_in_cash() {
		return de_in_cash;
	}
	public void setDe_in_cash(String de_in_cash) {
		this.de_in_cash = de_in_cash;
	}
	public String getDe_in_bank() {
		return de_in_bank;
	}
	public void setDe_in_bank(String de_in_bank) {
		this.de_in_bank = de_in_bank;
	}
	public Integer getDe_amount() {
		return de_amount;
	}
	public void setDe_amount(Integer de_amount) {
		this.de_amount = de_amount;
	}
	public Double getDe_money() {
		return de_money;
	}
	public void setDe_money(Double de_money) {
		this.de_money = de_money;
	}
	public Double getDe_cash() {
		return de_cash;
	}
	public void setDe_cash(Double de_cash) {
		this.de_cash = de_cash;
	}
	public Integer getDe_sell_bills() {
		return de_sell_bills;
	}
	public void setDe_sell_bills(Integer de_sell_bills) {
		this.de_sell_bills = de_sell_bills;
	}
	public Integer getDe_back_bills() {
		return de_back_bills;
	}
	public void setDe_back_bills(Integer de_back_bills) {
		this.de_back_bills = de_back_bills;
	}
	public Integer getDe_change_bills() {
		return de_change_bills;
	}
	public void setDe_change_bills(Integer de_change_bills) {
		this.de_change_bills = de_change_bills;
	}
	public Integer getDe_last_stock() {
		return de_last_stock;
	}
	public void setDe_last_stock(Integer de_last_stock) {
		this.de_last_stock = de_last_stock;
	}
	public Double getTotal_cash() {
		return total_cash;
	}
	public void setTotal_cash(Double total_cash) {
		this.total_cash = total_cash;
	}
	public Double getTotal_bank() {
		return total_bank;
	}
	public void setTotal_bank(Double total_bank) {
		this.total_bank = total_bank;
	}
	public String getEm_name() {
		return em_name;
	}
	public void setEm_name(String em_name) {
		this.em_name = em_name;
	}
	public String getShop_name() {
		return shop_name;
	}
	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}
	public String getBank_name() {
		return bank_name;
	}
	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}
	public Double getDe_ali_money() {
		return de_ali_money;
	}
	public void setDe_ali_money(Double de_ali_money) {
		this.de_ali_money = de_ali_money;
	}
	public Double getDe_wx_money() {
		return de_wx_money;
	}
	public void setDe_wx_money(Double de_wx_money) {
		this.de_wx_money = de_wx_money;
	}

}
