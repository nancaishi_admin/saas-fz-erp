package zy.entity.sell.ecoupon;

import java.io.Serializable;

public class T_Sell_ECouponList implements Serializable{
	private static final long serialVersionUID = -6088961791685491944L;
	private Integer ecl_id;
	private String ecl_number;
	private String ecl_code;
	private Double ecl_usedmoney;
	private Double ecl_money;
	private Double ecl_limitmoney;
	private Integer ecl_amount;
	private String ecl_remark;
	private Integer ecl_us_id;
	private String temp_code;//临时用的编号。可用于品牌、类别、商品的编号
	private Integer companyid;
	private String ec_begindate;
	private String ec_enddate;
	private Integer ecl_receive_amount;//领取数量
	private Integer ecl_my_count;//我当前优惠券的数量
	public Integer getEcl_id() {
		return ecl_id;
	}
	public void setEcl_id(Integer ecl_id) {
		this.ecl_id = ecl_id;
	}
	public String getEcl_number() {
		return ecl_number;
	}
	public void setEcl_number(String ecl_number) {
		this.ecl_number = ecl_number;
	}
	public String getEcl_code() {
		return ecl_code;
	}
	public void setEcl_code(String ecl_code) {
		this.ecl_code = ecl_code;
	}
	public Double getEcl_usedmoney() {
		return ecl_usedmoney;
	}
	public void setEcl_usedmoney(Double ecl_usedmoney) {
		this.ecl_usedmoney = ecl_usedmoney;
	}
	public Double getEcl_money() {
		return ecl_money;
	}
	public void setEcl_money(Double ecl_money) {
		this.ecl_money = ecl_money;
	}
	public Double getEcl_limitmoney() {
		return ecl_limitmoney;
	}
	public void setEcl_limitmoney(Double ecl_limitmoney) {
		this.ecl_limitmoney = ecl_limitmoney;
	}
	public Integer getEcl_amount() {
		return ecl_amount;
	}
	public void setEcl_amount(Integer ecl_amount) {
		this.ecl_amount = ecl_amount;
	}
	public String getEcl_remark() {
		return ecl_remark;
	}
	public void setEcl_remark(String ecl_remark) {
		this.ecl_remark = ecl_remark;
	}
	public Integer getEcl_us_id() {
		return ecl_us_id;
	}
	public void setEcl_us_id(Integer ecl_us_id) {
		this.ecl_us_id = ecl_us_id;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getTemp_code() {
		return temp_code;
	}
	public void setTemp_code(String temp_code) {
		this.temp_code = temp_code;
	}
	public String getEc_begindate() {
		return ec_begindate;
	}
	public void setEc_begindate(String ec_begindate) {
		this.ec_begindate = ec_begindate;
	}
	public String getEc_enddate() {
		return ec_enddate;
	}
	public void setEc_enddate(String ec_enddate) {
		this.ec_enddate = ec_enddate;
	}
	public Integer getEcl_receive_amount() {
		return ecl_receive_amount;
	}
	public void setEcl_receive_amount(Integer ecl_receive_amount) {
		this.ecl_receive_amount = ecl_receive_amount;
	}
	public Integer getEcl_my_count() {
		return ecl_my_count;
	}
	public void setEcl_my_count(Integer ecl_my_count) {
		this.ecl_my_count = ecl_my_count;
	}
}
