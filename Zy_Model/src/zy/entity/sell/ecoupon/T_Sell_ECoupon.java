package zy.entity.sell.ecoupon;

import java.io.Serializable;

public class T_Sell_ECoupon implements Serializable{
	private static final long serialVersionUID = 1741093293124129494L;
	private Integer ec_id;
	private String ec_number;
	private String ec_name;
	private String ec_manager;
	private String ec_remark;
	private Integer ec_deadline;
	private String ec_begindate;
	private String ec_enddate;
	private String ec_shop_code;
	private String ec_sysdate;
	private Integer ec_level;
	private Integer ec_type;
	private Integer ec_mode;
	private Integer ec_use_mode;
	private Integer ec_state;
	private Integer companyid;
	private String shop_name;
	private String shop_codes;
	private String shop_names;
	private String mode_codes;
	private String mode_names;
	private String use_mode_codes;
	private String use_mode_names;
	public Integer getEc_id() {
		return ec_id;
	}
	public void setEc_id(Integer ec_id) {
		this.ec_id = ec_id;
	}
	public String getEc_number() {
		return ec_number;
	}
	public void setEc_number(String ec_number) {
		this.ec_number = ec_number;
	}
	public String getEc_name() {
		return ec_name;
	}
	public void setEc_name(String ec_name) {
		this.ec_name = ec_name;
	}
	public String getEc_manager() {
		return ec_manager;
	}
	public void setEc_manager(String ec_manager) {
		this.ec_manager = ec_manager;
	}
	public String getEc_remark() {
		return ec_remark;
	}
	public void setEc_remark(String ec_remark) {
		this.ec_remark = ec_remark;
	}
	public Integer getEc_deadline() {
		return ec_deadline;
	}
	public void setEc_deadline(Integer ec_deadline) {
		this.ec_deadline = ec_deadline;
	}
	public String getEc_begindate() {
		return ec_begindate;
	}
	public void setEc_begindate(String ec_begindate) {
		this.ec_begindate = ec_begindate;
	}
	public String getEc_enddate() {
		return ec_enddate;
	}
	public void setEc_enddate(String ec_enddate) {
		this.ec_enddate = ec_enddate;
	}
	public String getEc_shop_code() {
		return ec_shop_code;
	}
	public void setEc_shop_code(String ec_shop_code) {
		this.ec_shop_code = ec_shop_code;
	}
	public String getEc_sysdate() {
		return ec_sysdate;
	}
	public void setEc_sysdate(String ec_sysdate) {
		this.ec_sysdate = ec_sysdate;
	}
	public Integer getEc_level() {
		return ec_level;
	}
	public void setEc_level(Integer ec_level) {
		this.ec_level = ec_level;
	}
	public Integer getEc_mode() {
		return ec_mode;
	}
	public void setEc_mode(Integer ec_mode) {
		this.ec_mode = ec_mode;
	}
	public Integer getEc_use_mode() {
		return ec_use_mode;
	}
	public void setEc_use_mode(Integer ec_use_mode) {
		this.ec_use_mode = ec_use_mode;
	}
	public Integer getEc_state() {
		return ec_state;
	}
	public void setEc_state(Integer ec_state) {
		this.ec_state = ec_state;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getShop_name() {
		return shop_name;
	}
	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}
	public String getShop_codes() {
		return shop_codes;
	}
	public void setShop_codes(String shop_codes) {
		this.shop_codes = shop_codes;
	}
	public String getShop_names() {
		return shop_names;
	}
	public void setShop_names(String shop_names) {
		this.shop_names = shop_names;
	}
	public String getMode_codes() {
		return mode_codes;
	}
	public void setMode_codes(String mode_codes) {
		this.mode_codes = mode_codes;
	}
	public String getMode_names() {
		return mode_names;
	}
	public void setMode_names(String mode_names) {
		this.mode_names = mode_names;
	}
	public String getUse_mode_codes() {
		return use_mode_codes;
	}
	public void setUse_mode_codes(String use_mode_codes) {
		this.use_mode_codes = use_mode_codes;
	}
	public String getUse_mode_names() {
		return use_mode_names;
	}
	public void setUse_mode_names(String use_mode_names) {
		this.use_mode_names = use_mode_names;
	}
	public Integer getEc_type() {
		return ec_type;
	}
	public void setEc_type(Integer ec_type) {
		this.ec_type = ec_type;
	}
	
}
