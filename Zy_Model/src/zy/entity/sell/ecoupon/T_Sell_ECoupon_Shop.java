package zy.entity.sell.ecoupon;

import java.io.Serializable;

public class T_Sell_ECoupon_Shop implements Serializable{
	private static final long serialVersionUID = 8765436108997422943L;
	private Integer ecs_id;
	private String ecs_ec_number;
	private String ecs_shop_code;
	private Integer companyid;
	public Integer getEcs_id() {
		return ecs_id;
	}
	public void setEcs_id(Integer ecs_id) {
		this.ecs_id = ecs_id;
	}
	public String getEcs_ec_number() {
		return ecs_ec_number;
	}
	public void setEcs_ec_number(String ecs_ec_number) {
		this.ecs_ec_number = ecs_ec_number;
	}
	public String getEcs_shop_code() {
		return ecs_shop_code;
	}
	public void setEcs_shop_code(String ecs_shop_code) {
		this.ecs_shop_code = ecs_shop_code;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
