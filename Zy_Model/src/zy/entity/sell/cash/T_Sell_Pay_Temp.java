package zy.entity.sell.cash;

import java.io.Serializable;

public class T_Sell_Pay_Temp implements Serializable{
	private static final long serialVersionUID = 1L;
	private Integer pt_id;
	private Integer companyid;
	private String pt_shop_code;
	private String pt_em_code;
	private Integer pt_state;
	private String pt_number;
	private double pt_money;
	private String pt_paycode;
	public Integer getPt_id() {
		return pt_id;
	}
	public void setPt_id(Integer pt_id) {
		this.pt_id = pt_id;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getPt_shop_code() {
		return pt_shop_code;
	}
	public void setPt_shop_code(String pt_shop_code) {
		this.pt_shop_code = pt_shop_code;
	}
	public String getPt_em_code() {
		return pt_em_code;
	}
	public void setPt_em_code(String pt_em_code) {
		this.pt_em_code = pt_em_code;
	}
	public Integer getPt_state() {
		return pt_state;
	}
	public void setPt_state(Integer pt_state) {
		this.pt_state = pt_state;
	}
	public String getPt_number() {
		return pt_number;
	}
	public void setPt_number(String pt_number) {
		this.pt_number = pt_number;
	}
	public double getPt_money() {
		return pt_money;
	}
	public void setPt_money(double pt_money) {
		this.pt_money = pt_money;
	}
	public String getPt_paycode() {
		return pt_paycode;
	}
	public void setPt_paycode(String pt_paycode) {
		this.pt_paycode = pt_paycode;
	}
	

}
