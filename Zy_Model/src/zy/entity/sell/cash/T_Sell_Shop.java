package zy.entity.sell.cash;

import java.io.Serializable;

import zy.util.NumberUtil;

public class T_Sell_Shop implements Serializable{
	private static final long serialVersionUID = 1L;
	private Integer sh_id;
	private String sh_number;
	private String sh_sysdate;
	private String sh_date;
	private String sh_em_code;
	private String em_name;
	private String sh_guide;
	private String sh_shop_code;
	private String shop_name;
	private Integer sh_vip_other;
	private String sh_vip_code;
	private Integer sh_amount;
	private Double sh_money;
	private Double sh_real_money;
	private Double sh_avg_price;
	private Integer sh_count;
	private Double sh_rate;
	private Double sh_sell_money;
	private Double sh_cash;
	private Double sh_cost_money;
	private Double sh_upcost_money;
	private Double sh_profit;
	private Double sh_profit_rate;
	private Double sh_profit_ratio;
	private Double sh_cd_money;
	private Integer sh_state;
	private Integer companyid;
	private Double sh_cd_rate;
	private Double sh_vc_money;
	private Double sh_vc_after_money;
	private Double sh_vc_rate;
	private Double sh_point_money;
	private Double sh_ec_money;
	private Double sh_sd_money;
	private String sh_sd_number;
	private Double sh_bank_money;
	private Double sh_mall_money;
	private Double sh_change_money;
	private Double sh_lost_money;
	private Double sh_wx_money;
	private Double sh_ali_money;
	private Double sh_third_money;
	private String sh_st_code;
	private Double sh_red_money;
	private Integer sh_source;
	private String sh_remark;
	
	private Double income_money;
	private Double expense_money;
	
	private Integer da_come;
	private Integer da_receive;
	private Integer da_try;
	public Integer getSh_id() {
		return sh_id;
	}
	public void setSh_id(Integer sh_id) {
		this.sh_id = sh_id;
	}
	public String getSh_number() {
		return sh_number;
	}
	public void setSh_number(String sh_number) {
		this.sh_number = sh_number;
	}
	public String getSh_sysdate() {
		return sh_sysdate;
	}
	public void setSh_sysdate(String sh_sysdate) {
		this.sh_sysdate = sh_sysdate;
	}
	public String getSh_date() {
		return sh_date;
	}
	public void setSh_date(String sh_date) {
		this.sh_date = sh_date;
	}
	public String getSh_em_code() {
		return sh_em_code;
	}
	public void setSh_em_code(String sh_em_code) {
		this.sh_em_code = sh_em_code;
	}
	public String getSh_shop_code() {
		return sh_shop_code;
	}
	public void setSh_shop_code(String sh_shop_code) {
		this.sh_shop_code = sh_shop_code;
	}
	public Integer getSh_vip_other() {
		return sh_vip_other;
	}
	public void setSh_vip_other(Integer sh_vip_other) {
		this.sh_vip_other = sh_vip_other;
	}
	public String getSh_vip_code() {
		return sh_vip_code;
	}
	public void setSh_vip_code(String sh_vip_code) {
		this.sh_vip_code = sh_vip_code;
	}
	public Integer getSh_amount() {
		return sh_amount;
	}
	public void setSh_amount(Integer sh_amount) {
		this.sh_amount = sh_amount;
	}
	public Double getSh_money() {
		return sh_money;
	}
	public void setSh_money(Double sh_money) {
		this.sh_money = sh_money;
	}
	public Double getSh_sell_money() {
		return sh_sell_money;
	}
	public void setSh_sell_money(Double sh_sell_money) {
		this.sh_sell_money = sh_sell_money;
	}
	public Double getSh_cash() {
		return sh_cash;
	}
	public void setSh_cash(Double sh_cash) {
		this.sh_cash = sh_cash;
	}
	public Double getSh_cost_money() {
		return sh_cost_money;
	}
	public void setSh_cost_money(Double sh_cost_money) {
		this.sh_cost_money = sh_cost_money;
	}
	public Double getSh_upcost_money() {
		return sh_upcost_money;
	}
	public void setSh_upcost_money(Double sh_upcost_money) {
		this.sh_upcost_money = sh_upcost_money;
	}
	public Double getSh_cd_money() {
		return sh_cd_money;
	}
	public void setSh_cd_money(Double sh_cd_money) {
		this.sh_cd_money = sh_cd_money;
	}
	public Integer getSh_state() {
		return sh_state;
	}
	public void setSh_state(Integer sh_state) {
		this.sh_state = sh_state;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public Double getSh_cd_rate() {
		return sh_cd_rate;
	}
	public void setSh_cd_rate(Double sh_cd_rate) {
		this.sh_cd_rate = sh_cd_rate;
	}
	public Double getSh_vc_money() {
		return sh_vc_money;
	}
	public void setSh_vc_money(Double sh_vc_money) {
		this.sh_vc_money = sh_vc_money;
	}
	public Double getSh_vc_rate() {
		return sh_vc_rate;
	}
	public void setSh_vc_rate(Double sh_vc_rate) {
		this.sh_vc_rate = sh_vc_rate;
	}
	public Double getSh_point_money() {
		return sh_point_money;
	}
	public void setSh_point_money(Double sh_point_money) {
		this.sh_point_money = sh_point_money;
	}
	public Double getSh_ec_money() {
		return sh_ec_money;
	}
	public void setSh_ec_money(Double sh_ec_money) {
		this.sh_ec_money = sh_ec_money;
	}
	public Double getSh_sd_money() {
		return sh_sd_money;
	}
	public void setSh_sd_money(Double sh_sd_money) {
		this.sh_sd_money = sh_sd_money;
	}
	public String getSh_sd_number() {
		return sh_sd_number;
	}
	public void setSh_sd_number(String sh_sd_number) {
		this.sh_sd_number = sh_sd_number;
	}
	public Double getSh_bank_money() {
		return sh_bank_money;
	}
	public void setSh_bank_money(Double sh_bank_money) {
		this.sh_bank_money = sh_bank_money;
	}
	public Double getSh_mall_money() {
		return sh_mall_money;
	}
	public void setSh_mall_money(Double sh_mall_money) {
		this.sh_mall_money = sh_mall_money;
	}
	public Double getSh_lost_money() {
		return NumberUtil.toDouble(sh_lost_money);
	}
	public void setSh_lost_money(Double sh_lost_money) {
		this.sh_lost_money = sh_lost_money;
	}
	public Double getSh_wx_money() {
		return sh_wx_money;
	}
	public void setSh_wx_money(Double sh_wx_money) {
		this.sh_wx_money = sh_wx_money;
	}
	public Double getSh_ali_money() {
		return sh_ali_money;
	}
	public void setSh_ali_money(Double sh_ali_money) {
		this.sh_ali_money = sh_ali_money;
	}
	public String getSh_st_code() {
		return sh_st_code;
	}
	public void setSh_st_code(String sh_st_code) {
		this.sh_st_code = sh_st_code;
	}
	public Double getSh_red_money() {
		return sh_red_money;
	}
	public void setSh_red_money(Double sh_red_money) {
		this.sh_red_money = sh_red_money;
	}
	public Integer getSh_source() {
		return sh_source;
	}
	public void setSh_source(Integer sh_source) {
		this.sh_source = sh_source;
	}
	public String getEm_name() {
		return em_name;
	}
	public void setEm_name(String em_name) {
		this.em_name = em_name;
	}
	public String getSh_guide() {
		return sh_guide;
	}
	public void setSh_guide(String sh_guide) {
		this.sh_guide = sh_guide;
	}
	public String getShop_name() {
		return shop_name;
	}
	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}
	public Double getSh_profit() {
		return sh_profit;
	}
	public void setSh_profit(Double sh_profit) {
		this.sh_profit = sh_profit;
	}
	public Double getSh_profit_rate() {
		return sh_profit_rate;
	}
	public void setSh_profit_rate(Double sh_profit_rate) {
		this.sh_profit_rate = sh_profit_rate;
	}
	public Double getSh_profit_ratio() {
		return sh_profit_ratio;
	}
	public void setSh_profit_ratio(Double sh_profit_ratio) {
		this.sh_profit_ratio = sh_profit_ratio;
	}
	public Double getSh_rate() {
		return sh_rate;
	}
	public void setSh_rate(Double sh_rate) {
		this.sh_rate = sh_rate;
	}
	public Integer getSh_count() {
		return sh_count;
	}
	public void setSh_count(Integer sh_count) {
		this.sh_count = sh_count;
	}
	public Double getSh_real_money() {
		return sh_real_money;
	}
	public void setSh_real_money(Double sh_real_money) {
		this.sh_real_money = sh_real_money;
	}
	public Double getSh_avg_price() {
		return double2Two(sh_avg_price);
	}
	public void setSh_avg_price(Double sh_avg_price) {
		this.sh_avg_price = double2Two(sh_avg_price);
	}
	
	private static double double2Two(Double doubleValue){
		try {
			if(null == doubleValue || Double.isNaN(doubleValue)){
				return 0.0;
			}
			//科学计数法转换成普通算法
			return Double.parseDouble(String.format("%.2f", doubleValue));
		} catch (RuntimeException e) {
			return 0.0;
		}
	}
	public Integer getDa_come() {
		return da_come;
	}
	public void setDa_come(Integer da_come) {
		this.da_come = da_come;
	}
	public Integer getDa_receive() {
		return da_receive;
	}
	public void setDa_receive(Integer da_receive) {
		this.da_receive = da_receive;
	}
	public Integer getDa_try() {
		return da_try;
	}
	public void setDa_try(Integer da_try) {
		this.da_try = da_try;
	}
	public Double getIncome_money() {
		return income_money;
	}
	public void setIncome_money(Double income_money) {
		this.income_money = income_money;
	}
	public Double getExpense_money() {
		return expense_money;
	}
	public void setExpense_money(Double expense_money) {
		this.expense_money = expense_money;
	}
	public Double getSh_third_money() {
		return sh_third_money;
	}
	public void setSh_third_money(Double sh_third_money) {
		this.sh_third_money = sh_third_money;
	}
	public String getSh_remark() {
		return sh_remark;
	}
	public void setSh_remark(String sh_remark) {
		this.sh_remark = sh_remark;
	}
	public Double getSh_vc_after_money() {
		return sh_vc_after_money;
	}
	public void setSh_vc_after_money(Double sh_vc_after_money) {
		this.sh_vc_after_money = sh_vc_after_money;
	}
	public Double getSh_change_money() {
		return sh_change_money;
	}
	public void setSh_change_money(Double sh_change_money) {
		this.sh_change_money = sh_change_money;
	}
	
}
