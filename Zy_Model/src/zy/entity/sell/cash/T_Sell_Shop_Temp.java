package zy.entity.sell.cash;

import java.io.Serializable;

public class T_Sell_Shop_Temp implements Serializable{
	private static final long serialVersionUID = 1L;
	private Long sht_id;
	private String sht_pd_code;
	private String pd_name;
	private String pd_no;
	private String pd_unit;
	private String pd_szg_code;
	private String sht_sub_code;
	private String sht_cr_code;
	private String cr_name;
	private String sht_sz_code;
	private String sz_name;
	private String sht_br_code;
	private String br_name;
	private String sht_bd_code;
	private String bd_name;
	private String sht_tp_code;
	private String tp_name;
	private String sht_em_code;
	private String em_name;
	private Integer companyid;
	private String sht_shop_code;
	private String shop_name;
	private String sht_da_code;
	private String da_name;
	private String sht_main;
	private String main_name;
	private String sht_slave;
	private String slave_name;
	private Integer sht_vip_other;
	private String sht_vip_code;
	private Double sht_vip_price;
	private Integer sht_amount;
	private Double sht_sell_price;
	private Double sht_sign_price;
	private Double sht_upcost_price;
	private Double sht_cost_price;
	private Double sht_price;
	private Double sht_money;
	private String sht_sale_code;
	private String sht_sale_model;
	private Double sht_sale_money;
	private Double sht_sale_send_money;
	private Integer sht_sale_priority;
	private Double sht_hand_money;
	private Double sht_vip_money;
	private Integer sht_ishand;
	private Integer sht_handtype;
	private Double sht_handvalue;
	private Integer sht_viptype;
	private Double sht_vipvalue;
	private Integer sht_isvip;
	private Integer sht_ispoint;
	private Integer sht_isgift;
	private Integer sht_isputup;
	private Integer sht_putup_no;
	private Integer sht_state;
	private String sht_remark;
	public Long getSht_id() {
		return sht_id;
	}
	public void setSht_id(Long sht_id) {
		this.sht_id = sht_id;
	}
	public String getPd_no() {
		return pd_no;
	}
	public void setPd_no(String pd_no) {
		this.pd_no = pd_no;
	}
	public String getPd_unit() {
		return pd_unit;
	}
	public void setPd_unit(String pd_unit) {
		this.pd_unit = pd_unit;
	}
	public String getSht_pd_code() {
		return sht_pd_code;
	}
	public void setSht_pd_code(String sht_pd_code) {
		this.sht_pd_code = sht_pd_code;
	}
	public String getPd_name() {
		return pd_name;
	}
	public void setPd_name(String pd_name) {
		this.pd_name = pd_name;
	}
	public String getSht_sub_code() {
		return sht_sub_code;
	}
	public void setSht_sub_code(String sht_sub_code) {
		this.sht_sub_code = sht_sub_code;
	}
	public String getSht_cr_code() {
		return sht_cr_code;
	}
	public void setSht_cr_code(String sht_cr_code) {
		this.sht_cr_code = sht_cr_code;
	}
	public String getCr_name() {
		return cr_name;
	}
	public void setCr_name(String cr_name) {
		this.cr_name = cr_name;
	}
	public String getSht_sz_code() {
		return sht_sz_code;
	}
	public void setSht_sz_code(String sht_sz_code) {
		this.sht_sz_code = sht_sz_code;
	}
	public String getSz_name() {
		return sz_name;
	}
	public void setSz_name(String sz_name) {
		this.sz_name = sz_name;
	}
	public String getSht_br_code() {
		return sht_br_code;
	}
	public void setSht_br_code(String sht_br_code) {
		this.sht_br_code = sht_br_code;
	}
	public String getBr_name() {
		return br_name;
	}
	public void setBr_name(String br_name) {
		this.br_name = br_name;
	}
	public String getBd_name() {
		return bd_name;
	}
	public void setBd_name(String bd_name) {
		this.bd_name = bd_name;
	}
	public String getSht_bd_code() {
		return sht_bd_code;
	}
	public void setSht_bd_code(String sht_bd_code) {
		this.sht_bd_code = sht_bd_code;
	}
	public String getSht_tp_code() {
		return sht_tp_code;
	}
	public void setSht_tp_code(String sht_tp_code) {
		this.sht_tp_code = sht_tp_code;
	}
	public String getTp_name() {
		return tp_name;
	}
	public void setTp_name(String tp_name) {
		this.tp_name = tp_name;
	}
	public String getSht_em_code() {
		return sht_em_code;
	}
	public void setSht_em_code(String sht_em_code) {
		this.sht_em_code = sht_em_code;
	}
	public String getEm_name() {
		return em_name;
	}
	public void setEm_name(String em_name) {
		this.em_name = em_name;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getSht_shop_code() {
		return sht_shop_code;
	}
	public void setSht_shop_code(String sht_shop_code) {
		this.sht_shop_code = sht_shop_code;
	}
	public String getShop_name() {
		return shop_name;
	}
	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}
	public String getSht_da_code() {
		return sht_da_code;
	}
	public void setSht_da_code(String sht_da_code) {
		this.sht_da_code = sht_da_code;
	}
	public String getDa_name() {
		return da_name;
	}
	public void setDa_name(String da_name) {
		this.da_name = da_name;
	}
	public String getSht_main() {
		return sht_main;
	}
	public void setSht_main(String sht_main) {
		this.sht_main = sht_main;
	}
	public String getMain_name() {
		return main_name;
	}
	public void setMain_name(String main_name) {
		this.main_name = main_name;
	}
	public String getSht_slave() {
		return sht_slave;
	}
	public void setSht_slave(String sht_slave) {
		this.sht_slave = sht_slave;
	}
	public String getSlave_name() {
		return slave_name;
	}
	public void setSlave_name(String slave_name) {
		this.slave_name = slave_name;
	}
	public Integer getSht_vip_other() {
		return sht_vip_other;
	}
	public void setSht_vip_other(Integer sht_vip_other) {
		this.sht_vip_other = sht_vip_other;
	}
	public String getSht_vip_code() {
		return sht_vip_code;
	}
	public void setSht_vip_code(String sht_vip_code) {
		this.sht_vip_code = sht_vip_code;
	}
	public Integer getSht_amount() {
		return sht_amount;
	}
	public void setSht_amount(Integer sht_amount) {
		this.sht_amount = sht_amount;
	}
	public Double getSht_sell_price() {
		return sht_sell_price;
	}
	public void setSht_sell_price(Double sht_sell_price) {
		this.sht_sell_price = sht_sell_price;
	}
	
	public Double getSht_sign_price() {
		return sht_sign_price;
	}
	public void setSht_sign_price(Double sht_sign_price) {
		this.sht_sign_price = sht_sign_price;
	}
	public Double getSht_upcost_price() {
		return sht_upcost_price;
	}
	public void setSht_upcost_price(Double sht_upcost_price) {
		this.sht_upcost_price = sht_upcost_price;
	}
	public Double getSht_cost_price() {
		return sht_cost_price;
	}
	public void setSht_cost_price(Double sht_cost_price) {
		this.sht_cost_price = sht_cost_price;
	}
	public Double getSht_price() {
		return sht_price;
	}
	public void setSht_price(Double sht_price) {
		this.sht_price = sht_price;
	}
	public Double getSht_money() {
		return sht_money;
	}
	public void setSht_money(Double sht_money) {
		this.sht_money = sht_money;
	}
	public String getSht_sale_code() {
		return sht_sale_code;
	}
	public void setSht_sale_code(String sht_sale_code) {
		this.sht_sale_code = sht_sale_code;
	}
	public String getSht_sale_model() {
		return sht_sale_model;
	}
	public void setSht_sale_model(String sht_sale_model) {
		this.sht_sale_model = sht_sale_model;
	}
	public Double getSht_sale_money() {
		return sht_sale_money;
	}
	public void setSht_sale_money(Double sht_sale_money) {
		this.sht_sale_money = sht_sale_money;
	}
	public Double getSht_sale_send_money() {
		return sht_sale_send_money;
	}
	public void setSht_sale_send_money(Double sht_sale_send_money) {
		this.sht_sale_send_money = sht_sale_send_money;
	}
	public Double getSht_hand_money() {
		return sht_hand_money;
	}
	public void setSht_hand_money(Double sht_hand_money) {
		this.sht_hand_money = sht_hand_money;
	}
	public Double getSht_vip_money() {
		return sht_vip_money;
	}
	public void setSht_vip_money(Double sht_vip_money) {
		this.sht_vip_money = sht_vip_money;
	}
	public Integer getSht_ishand() {
		return sht_ishand;
	}
	public void setSht_ishand(Integer sht_ishand) {
		this.sht_ishand = sht_ishand;
	}
	public Integer getSht_isvip() {
		return sht_isvip;
	}
	public void setSht_isvip(Integer sht_isvip) {
		this.sht_isvip = sht_isvip;
	}
	public Integer getSht_ispoint() {
		return sht_ispoint;
	}
	public void setSht_ispoint(Integer sht_ispoint) {
		this.sht_ispoint = sht_ispoint;
	}
	public Integer getSht_isgift() {
		return sht_isgift;
	}
	public void setSht_isgift(Integer sht_isgift) {
		this.sht_isgift = sht_isgift;
	}
	public Integer getSht_isputup() {
		return sht_isputup;
	}
	public void setSht_isputup(Integer sht_isputup) {
		this.sht_isputup = sht_isputup;
	}
	public Integer getSht_putup_no() {
		return sht_putup_no;
	}
	public void setSht_putup_no(Integer sht_putup_no) {
		this.sht_putup_no = sht_putup_no;
	}
	public Integer getSht_state() {
		return sht_state;
	}
	public void setSht_state(Integer sht_state) {
		this.sht_state = sht_state;
	}
	public Double getSht_vip_price() {
		return sht_vip_price;
	}
	public void setSht_vip_price(Double sht_vip_price) {
		this.sht_vip_price = sht_vip_price;
	}
	public Integer getSht_handtype() {
		return sht_handtype;
	}
	public void setSht_handtype(Integer sht_handtype) {
		this.sht_handtype = sht_handtype;
	}
	public Double getSht_handvalue() {
		return sht_handvalue;
	}
	public void setSht_handvalue(Double sht_handvalue) {
		this.sht_handvalue = sht_handvalue;
	}
	public Integer getSht_viptype() {
		return sht_viptype;
	}
	public void setSht_viptype(Integer sht_viptype) {
		this.sht_viptype = sht_viptype;
	}
	public Double getSht_vipvalue() {
		return sht_vipvalue;
	}
	public void setSht_vipvalue(Double sht_vipvalue) {
		this.sht_vipvalue = sht_vipvalue;
	}
	public Integer getSht_sale_priority() {
		return sht_sale_priority;
	}
	public void setSht_sale_priority(Integer sht_sale_priority) {
		this.sht_sale_priority = sht_sale_priority;
	}
	public String getSht_remark() {
		return sht_remark;
	}
	public void setSht_remark(String sht_remark) {
		this.sht_remark = sht_remark;
	}
	public String getPd_szg_code() {
		return pd_szg_code;
	}
	public void setPd_szg_code(String pd_szg_code) {
		this.pd_szg_code = pd_szg_code;
	}
	
}
