package zy.entity.sell.voucher;

import java.io.Serializable;

public class T_Sell_Voucher implements Serializable,Cloneable{
	private static final long serialVersionUID = -7348724961522222374L;
	private Integer vc_id;
	private String vc_code;
	private String vc_cardcode;
	private String vc_shop_code;
	private String shop_name;
	private String vc_grantdate;
	private String vc_enddate;
	private Double vc_money;
	private Double vc_used_money;
	private Double vc_realcash;
	private Double vc_cashrate;
	private String vcl_ba_code;
	private Integer vc_state;
	private String vc_remark;
	private String vc_name;
	private String vc_mobile;
	private String vc_manager;
	private Integer companyid;
	public Integer getVc_id() {
		return vc_id;
	}
	public void setVc_id(Integer vc_id) {
		this.vc_id = vc_id;
	}
	public String getVc_code() {
		return vc_code;
	}
	public void setVc_code(String vc_code) {
		this.vc_code = vc_code;
	}
	public String getVc_cardcode() {
		return vc_cardcode;
	}
	public void setVc_cardcode(String vc_cardcode) {
		this.vc_cardcode = vc_cardcode;
	}
	public String getVc_shop_code() {
		return vc_shop_code;
	}
	public void setVc_shop_code(String vc_shop_code) {
		this.vc_shop_code = vc_shop_code;
	}
	public String getShop_name() {
		return shop_name;
	}
	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}
	public String getVc_grantdate() {
		return vc_grantdate;
	}
	public void setVc_grantdate(String vc_grantdate) {
		this.vc_grantdate = vc_grantdate;
	}
	public String getVc_enddate() {
		return vc_enddate;
	}
	public void setVc_enddate(String vc_enddate) {
		this.vc_enddate = vc_enddate;
	}
	public Double getVc_money() {
		return vc_money;
	}
	public void setVc_money(Double vc_money) {
		this.vc_money = vc_money;
	}
	public Double getVc_used_money() {
		return vc_used_money;
	}
	public void setVc_used_money(Double vc_used_money) {
		this.vc_used_money = vc_used_money;
	}
	public Double getVc_realcash() {
		return vc_realcash;
	}
	public void setVc_realcash(Double vc_realcash) {
		this.vc_realcash = vc_realcash;
	}
	public String getVcl_ba_code() {
		return vcl_ba_code;
	}
	public void setVcl_ba_code(String vcl_ba_code) {
		this.vcl_ba_code = vcl_ba_code;
	}
	public Double getVc_cashrate() {
		return vc_cashrate;
	}
	public void setVc_cashrate(Double vc_cashrate) {
		this.vc_cashrate = vc_cashrate;
	}
	public Integer getVc_state() {
		return vc_state;
	}
	public void setVc_state(Integer vc_state) {
		this.vc_state = vc_state;
	}
	public String getVc_remark() {
		return vc_remark;
	}
	public void setVc_remark(String vc_remark) {
		this.vc_remark = vc_remark;
	}
	public String getVc_name() {
		return vc_name;
	}
	public void setVc_name(String vc_name) {
		this.vc_name = vc_name;
	}
	public String getVc_mobile() {
		return vc_mobile;
	}
	public void setVc_mobile(String vc_mobile) {
		this.vc_mobile = vc_mobile;
	}
	public String getVc_manager() {
		return vc_manager;
	}
	public void setVc_manager(String vc_manager) {
		this.vc_manager = vc_manager;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
