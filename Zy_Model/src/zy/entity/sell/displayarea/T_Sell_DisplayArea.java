package zy.entity.sell.displayarea;

import java.io.Serializable;

public class T_Sell_DisplayArea implements Serializable{
	private static final long serialVersionUID = 1710187511257288238L;
	private Integer da_id;
	private String da_code;
	private String da_name;
	private String da_shop_code;
	private String shop_name;
	private Integer companyid;
	public Integer getDa_id() {
		return da_id;
	}
	public void setDa_id(Integer da_id) {
		this.da_id = da_id;
	}
	public String getDa_code() {
		return da_code;
	}
	public void setDa_code(String da_code) {
		this.da_code = da_code;
	}
	public String getDa_name() {
		return da_name;
	}
	public void setDa_name(String da_name) {
		this.da_name = da_name;
	}
	public String getDa_shop_code() {
		return da_shop_code;
	}
	public void setDa_shop_code(String da_shop_code) {
		this.da_shop_code = da_shop_code;
	}
	public String getShop_name() {
		return shop_name;
	}
	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
