package zy.entity.sell.cart;

import java.io.Serializable;

public class T_Sell_Cart implements Serializable{
	private static final long serialVersionUID = 4179079823069103517L;
	private Integer sc_id;
	private String sc_vm_code;
	private String sc_number;
	private String sc_em_code;
	private String sc_shop_code;
	private String sc_sysdate;
	private Integer sc_amount;
	private Double sc_money;
	private Integer companyid;
	public Integer getSc_id() {
		return sc_id;
	}
	public void setSc_id(Integer sc_id) {
		this.sc_id = sc_id;
	}
	public String getSc_vm_code() {
		return sc_vm_code;
	}
	public void setSc_vm_code(String sc_vm_code) {
		this.sc_vm_code = sc_vm_code;
	}
	public String getSc_number() {
		return sc_number;
	}
	public void setSc_number(String sc_number) {
		this.sc_number = sc_number;
	}
	public String getSc_em_code() {
		return sc_em_code;
	}
	public void setSc_em_code(String sc_em_code) {
		this.sc_em_code = sc_em_code;
	}
	public String getSc_shop_code() {
		return sc_shop_code;
	}
	public void setSc_shop_code(String sc_shop_code) {
		this.sc_shop_code = sc_shop_code;
	}
	public String getSc_sysdate() {
		return sc_sysdate;
	}
	public void setSc_sysdate(String sc_sysdate) {
		this.sc_sysdate = sc_sysdate;
	}
	public Integer getSc_amount() {
		return sc_amount;
	}
	public void setSc_amount(Integer sc_amount) {
		this.sc_amount = sc_amount;
	}
	public Double getSc_money() {
		return sc_money;
	}
	public void setSc_money(Double sc_money) {
		this.sc_money = sc_money;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
