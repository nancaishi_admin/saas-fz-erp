package zy.entity.sell.cart;

import java.io.Serializable;

public class T_Sell_CartList implements Serializable{
	private static final long serialVersionUID = -4607916889697287480L;
	private Long scl_id;
	private String scl_number;
	private String scl_sysdate;
	private String scl_pd_code;
	private String scl_sub_code;
	private String scl_cr_code;
	private String scl_sz_code;
	private String scl_br_code;
	private Integer scl_amount;
	private Double scl_sell_price;
	private Integer scl_state;
	private String scl_vm_code;
	private String scl_em_code;
	private String scl_shop_code;
	private Integer companyid;
	private String pd_no;
	private String pd_name;
	private String pd_unit;
	private String pd_season;
	private Integer pd_year;
	private String pd_img_path;
	private String cr_name;
	private String sz_name;
	private String br_name;
	public Long getScl_id() {
		return scl_id;
	}
	public void setScl_id(Long scl_id) {
		this.scl_id = scl_id;
	}
	public String getScl_number() {
		return scl_number;
	}
	public void setScl_number(String scl_number) {
		this.scl_number = scl_number;
	}
	public String getScl_sysdate() {
		return scl_sysdate;
	}
	public void setScl_sysdate(String scl_sysdate) {
		this.scl_sysdate = scl_sysdate;
	}
	public String getScl_pd_code() {
		return scl_pd_code;
	}
	public void setScl_pd_code(String scl_pd_code) {
		this.scl_pd_code = scl_pd_code;
	}
	public String getScl_sub_code() {
		return scl_sub_code;
	}
	public void setScl_sub_code(String scl_sub_code) {
		this.scl_sub_code = scl_sub_code;
	}
	public String getScl_cr_code() {
		return scl_cr_code;
	}
	public void setScl_cr_code(String scl_cr_code) {
		this.scl_cr_code = scl_cr_code;
	}
	public String getScl_sz_code() {
		return scl_sz_code;
	}
	public void setScl_sz_code(String scl_sz_code) {
		this.scl_sz_code = scl_sz_code;
	}
	public String getScl_br_code() {
		return scl_br_code;
	}
	public void setScl_br_code(String scl_br_code) {
		this.scl_br_code = scl_br_code;
	}
	public Integer getScl_amount() {
		return scl_amount;
	}
	public void setScl_amount(Integer scl_amount) {
		this.scl_amount = scl_amount;
	}
	public Double getScl_sell_price() {
		return scl_sell_price;
	}
	public void setScl_sell_price(Double scl_sell_price) {
		this.scl_sell_price = scl_sell_price;
	}
	public Integer getScl_state() {
		return scl_state;
	}
	public void setScl_state(Integer scl_state) {
		this.scl_state = scl_state;
	}
	public String getScl_vm_code() {
		return scl_vm_code;
	}
	public void setScl_vm_code(String scl_vm_code) {
		this.scl_vm_code = scl_vm_code;
	}
	public String getScl_em_code() {
		return scl_em_code;
	}
	public void setScl_em_code(String scl_em_code) {
		this.scl_em_code = scl_em_code;
	}
	public String getScl_shop_code() {
		return scl_shop_code;
	}
	public void setScl_shop_code(String scl_shop_code) {
		this.scl_shop_code = scl_shop_code;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getPd_no() {
		return pd_no;
	}
	public void setPd_no(String pd_no) {
		this.pd_no = pd_no;
	}
	public String getPd_name() {
		return pd_name;
	}
	public void setPd_name(String pd_name) {
		this.pd_name = pd_name;
	}
	public String getPd_unit() {
		return pd_unit;
	}
	public void setPd_unit(String pd_unit) {
		this.pd_unit = pd_unit;
	}
	public String getPd_season() {
		return pd_season;
	}
	public void setPd_season(String pd_season) {
		this.pd_season = pd_season;
	}
	public Integer getPd_year() {
		return pd_year;
	}
	public void setPd_year(Integer pd_year) {
		this.pd_year = pd_year;
	}
	public String getPd_img_path() {
		return pd_img_path;
	}
	public void setPd_img_path(String pd_img_path) {
		this.pd_img_path = pd_img_path;
	}
	public String getCr_name() {
		return cr_name;
	}
	public void setCr_name(String cr_name) {
		this.cr_name = cr_name;
	}
	public String getSz_name() {
		return sz_name;
	}
	public void setSz_name(String sz_name) {
		this.sz_name = sz_name;
	}
	public String getBr_name() {
		return br_name;
	}
	public void setBr_name(String br_name) {
		this.br_name = br_name;
	}
}
