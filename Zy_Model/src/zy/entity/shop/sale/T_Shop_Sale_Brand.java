package zy.entity.shop.sale;

import java.io.Serializable;

public class T_Shop_Sale_Brand implements Serializable{
	private static final long serialVersionUID = -5933914747225546123L;
	private Integer ssb_id;
	private String ssb_ss_code;
	private String ssb_bd_code;
	private Double ssb_discount;
	private Double ssb_buy_full;
	private Integer ssb_buy_number;
	private Double ssb_donation_amount;
	private Double ssb_reduce_amount;
	private Double ssb_increased_amount;
	private Integer ssb_index;
	private Integer companyid;
	private String bd_name;
	public Integer getSsb_id() {
		return ssb_id;
	}
	public void setSsb_id(Integer ssb_id) {
		this.ssb_id = ssb_id;
	}
	public String getSsb_ss_code() {
		return ssb_ss_code;
	}
	public void setSsb_ss_code(String ssb_ss_code) {
		this.ssb_ss_code = ssb_ss_code;
	}
	public String getSsb_bd_code() {
		return ssb_bd_code;
	}
	public void setSsb_bd_code(String ssb_bd_code) {
		this.ssb_bd_code = ssb_bd_code;
	}
	public Double getSsb_discount() {
		return ssb_discount;
	}
	public void setSsb_discount(Double ssb_discount) {
		this.ssb_discount = ssb_discount;
	}
	public Double getSsb_buy_full() {
		return ssb_buy_full;
	}
	public void setSsb_buy_full(Double ssb_buy_full) {
		this.ssb_buy_full = ssb_buy_full;
	}
	public Integer getSsb_buy_number() {
		return ssb_buy_number;
	}
	public void setSsb_buy_number(Integer ssb_buy_number) {
		this.ssb_buy_number = ssb_buy_number;
	}
	public Double getSsb_donation_amount() {
		return ssb_donation_amount;
	}
	public void setSsb_donation_amount(Double ssb_donation_amount) {
		this.ssb_donation_amount = ssb_donation_amount;
	}
	public Double getSsb_reduce_amount() {
		return ssb_reduce_amount;
	}
	public void setSsb_reduce_amount(Double ssb_reduce_amount) {
		this.ssb_reduce_amount = ssb_reduce_amount;
	}
	public Double getSsb_increased_amount() {
		return ssb_increased_amount;
	}
	public void setSsb_increased_amount(Double ssb_increased_amount) {
		this.ssb_increased_amount = ssb_increased_amount;
	}
	public Integer getSsb_index() {
		return ssb_index;
	}
	public void setSsb_index(Integer ssb_index) {
		this.ssb_index = ssb_index;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getBd_name() {
		return bd_name;
	}
	public void setBd_name(String bd_name) {
		this.bd_name = bd_name;
	}
}
