package zy.entity.shop.sale;

import java.io.Serializable;

public class T_Shop_Sale_Product implements Serializable{
	private static final long serialVersionUID = -5933914747225546123L;
	private Integer ssp_id;
	private String ssp_ss_code;
	private String ssp_pd_code;
	private Double ssp_discount;
	private Double ssp_buy_full;
	private Integer ssp_buy_number;
	private Double ssp_donation_amount;
	private Double ssp_reduce_amount;
	private Double ssp_increased_amount;
	private Double ssp_special_offer;
	private Integer ssp_index;
	private Integer companyid;
	private String pd_no;
	private String pd_name;
	private Double pd_sell_price;
	public Integer getSsp_id() {
		return ssp_id;
	}
	public void setSsp_id(Integer ssp_id) {
		this.ssp_id = ssp_id;
	}
	public String getSsp_ss_code() {
		return ssp_ss_code;
	}
	public void setSsp_ss_code(String ssp_ss_code) {
		this.ssp_ss_code = ssp_ss_code;
	}
	public String getSsp_pd_code() {
		return ssp_pd_code;
	}
	public void setSsp_pd_code(String ssp_pd_code) {
		this.ssp_pd_code = ssp_pd_code;
	}
	public Double getSsp_discount() {
		return ssp_discount;
	}
	public void setSsp_discount(Double ssp_discount) {
		this.ssp_discount = ssp_discount;
	}
	public Double getSsp_buy_full() {
		return ssp_buy_full;
	}
	public void setSsp_buy_full(Double ssp_buy_full) {
		this.ssp_buy_full = ssp_buy_full;
	}
	public Integer getSsp_buy_number() {
		return ssp_buy_number;
	}
	public void setSsp_buy_number(Integer ssp_buy_number) {
		this.ssp_buy_number = ssp_buy_number;
	}
	public Double getSsp_donation_amount() {
		return ssp_donation_amount;
	}
	public void setSsp_donation_amount(Double ssp_donation_amount) {
		this.ssp_donation_amount = ssp_donation_amount;
	}
	public Double getSsp_reduce_amount() {
		return ssp_reduce_amount;
	}
	public void setSsp_reduce_amount(Double ssp_reduce_amount) {
		this.ssp_reduce_amount = ssp_reduce_amount;
	}
	public Double getSsp_increased_amount() {
		return ssp_increased_amount;
	}
	public void setSsp_increased_amount(Double ssp_increased_amount) {
		this.ssp_increased_amount = ssp_increased_amount;
	}
	public Double getSsp_special_offer() {
		return ssp_special_offer;
	}
	public void setSsp_special_offer(Double ssp_special_offer) {
		this.ssp_special_offer = ssp_special_offer;
	}
	public Integer getSsp_index() {
		return ssp_index;
	}
	public void setSsp_index(Integer ssp_index) {
		this.ssp_index = ssp_index;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getPd_name() {
		return pd_name;
	}
	public void setPd_name(String pd_name) {
		this.pd_name = pd_name;
	}
	public String getPd_no() {
		return pd_no;
	}
	public void setPd_no(String pd_no) {
		this.pd_no = pd_no;
	}
	public Double getPd_sell_price() {
		return pd_sell_price;
	}
	public void setPd_sell_price(Double pd_sell_price) {
		this.pd_sell_price = pd_sell_price;
	}
}
