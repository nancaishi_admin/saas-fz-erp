package zy.entity.shop.sale;

import java.io.Serializable;

public class T_Shop_Sale_Shop implements Serializable{
	private static final long serialVersionUID = -5933914747225546123L;
	private Integer sss_id;
	private String sss_ss_code;
	private String sss_shop_code;
	private Integer companyid;
	public Integer getSss_id() {
		return sss_id;
	}
	public void setSss_id(Integer sss_id) {
		this.sss_id = sss_id;
	}
	public String getSss_ss_code() {
		return sss_ss_code;
	}
	public void setSss_ss_code(String sss_ss_code) {
		this.sss_ss_code = sss_ss_code;
	}
	public String getSss_shop_code() {
		return sss_shop_code;
	}
	public void setSss_shop_code(String sss_shop_code) {
		this.sss_shop_code = sss_shop_code;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
