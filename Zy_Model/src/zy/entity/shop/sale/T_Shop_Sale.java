package zy.entity.shop.sale;

import java.io.Serializable;

public class T_Shop_Sale implements Serializable{
	private static final long serialVersionUID = -5933914747225546123L;
	private Integer ss_id;
	private String ss_code;
	private String ss_name;
	private String ss_begin_date;
	private String ss_end_date;
	private String ss_week;
	private String ss_mt_code;
	private String ss_mt_name;
	private Integer ss_point;
	private String ss_manager;
	private String ss_sysdate;
	private Integer ss_state;
	private Integer ss_priority;
	private String ss_model;
	private String ss_current_spcode;
	private Integer companyid;
	private String ss_sp_name;//店铺名称
	private String ss_shop_code;
	private String curdate;
	private Integer ssm_model;
	private String ssm_model_explain;
	private Integer ssm_scope;
	private String ssm_scope_explain;
	private Integer ssm_rule;
	private String ssm_rule_explain;
	public Integer getSs_id() {
		return ss_id;
	}
	public void setSs_id(Integer ss_id) {
		this.ss_id = ss_id;
	}
	public String getSs_code() {
		return ss_code;
	}
	public void setSs_code(String ss_code) {
		this.ss_code = ss_code;
	}
	public String getSs_name() {
		return ss_name;
	}
	public void setSs_name(String ss_name) {
		this.ss_name = ss_name;
	}
	public String getSs_begin_date() {
		return ss_begin_date;
	}
	public void setSs_begin_date(String ss_begin_date) {
		this.ss_begin_date = ss_begin_date;
	}
	public String getSs_end_date() {
		return ss_end_date;
	}
	public void setSs_end_date(String ss_end_date) {
		this.ss_end_date = ss_end_date;
	}
	public String getSs_week() {
		return ss_week;
	}
	public void setSs_week(String ss_week) {
		this.ss_week = ss_week;
	}
	public String getSs_mt_code() {
		return ss_mt_code;
	}
	public void setSs_mt_code(String ss_mt_code) {
		this.ss_mt_code = ss_mt_code;
	}
	public Integer getSs_point() {
		return ss_point;
	}
	public void setSs_point(Integer ss_point) {
		this.ss_point = ss_point;
	}
	public String getSs_manager() {
		return ss_manager;
	}
	public void setSs_manager(String ss_manager) {
		this.ss_manager = ss_manager;
	}
	public String getSs_sysdate() {
		return ss_sysdate;
	}
	public void setSs_sysdate(String ss_sysdate) {
		this.ss_sysdate = ss_sysdate;
	}
	public Integer getSs_state() {
		return ss_state;
	}
	public void setSs_state(Integer ss_state) {
		this.ss_state = ss_state;
	}
	public Integer getSs_priority() {
		return ss_priority;
	}
	public void setSs_priority(Integer ss_priority) {
		this.ss_priority = ss_priority;
	}
	public String getSs_model() {
		return ss_model;
	}
	public void setSs_model(String ss_model) {
		this.ss_model = ss_model;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getSs_sp_name() {
		return ss_sp_name;
	}
	public void setSs_sp_name(String ss_sp_name) {
		this.ss_sp_name = ss_sp_name;
	}
	public String getSs_shop_code() {
		return ss_shop_code;
	}
	public void setSs_shop_code(String ss_shop_code) {
		this.ss_shop_code = ss_shop_code;
	}
	public String getCurdate() {
		return curdate;
	}
	public void setCurdate(String curdate) {
		this.curdate = curdate;
	}
	public Integer getSsm_model() {
		return ssm_model;
	}
	public void setSsm_model(Integer ssm_model) {
		this.ssm_model = ssm_model;
	}
	public String getSsm_model_explain() {
		return ssm_model_explain;
	}
	public void setSsm_model_explain(String ssm_model_explain) {
		this.ssm_model_explain = ssm_model_explain;
	}
	public Integer getSsm_rule() {
		return ssm_rule;
	}
	public void setSsm_rule(Integer ssm_rule) {
		this.ssm_rule = ssm_rule;
	}
	public Integer getSsm_scope() {
		return ssm_scope;
	}
	public void setSsm_scope(Integer ssm_scope) {
		this.ssm_scope = ssm_scope;
	}
	public String getSsm_scope_explain() {
		return ssm_scope_explain;
	}
	public void setSsm_scope_explain(String ssm_scope_explain) {
		this.ssm_scope_explain = ssm_scope_explain;
	}
	public String getSsm_rule_explain() {
		return ssm_rule_explain;
	}
	public void setSsm_rule_explain(String ssm_rule_explain) {
		this.ssm_rule_explain = ssm_rule_explain;
	}
	public String getSs_mt_name() {
		return ss_mt_name;
	}
	public void setSs_mt_name(String ss_mt_name) {
		this.ss_mt_name = ss_mt_name;
	}
	public String getSs_current_spcode() {
		return ss_current_spcode;
	}
	public void setSs_current_spcode(String ss_current_spcode) {
		this.ss_current_spcode = ss_current_spcode;
	}
}
