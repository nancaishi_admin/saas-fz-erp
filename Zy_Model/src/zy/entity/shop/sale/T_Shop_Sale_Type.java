package zy.entity.shop.sale;

import java.io.Serializable;

public class T_Shop_Sale_Type implements Serializable{
	private static final long serialVersionUID = -5933914747225546123L;
	private Integer sst_id;
	private String sst_ss_code;
	private String sst_tp_code;
	private String tp_name;
	private Double sst_discount;
	private Double sst_buy_full;
	private Integer sst_buy_number;
	private Double sst_donation_amount;
	private Double sst_reduce_amount;
	private Double sst_increased_amount;
	private Integer sst_index;
	private Integer companyid;
	public Integer getSst_id() {
		return sst_id;
	}
	public void setSst_id(Integer sst_id) {
		this.sst_id = sst_id;
	}
	public String getSst_ss_code() {
		return sst_ss_code;
	}
	public void setSst_ss_code(String sst_ss_code) {
		this.sst_ss_code = sst_ss_code;
	}
	public String getSst_tp_code() {
		return sst_tp_code;
	}
	public void setSst_tp_code(String sst_tp_code) {
		this.sst_tp_code = sst_tp_code;
	}
	public Double getSst_discount() {
		return sst_discount;
	}
	public void setSst_discount(Double sst_discount) {
		this.sst_discount = sst_discount;
	}
	public Double getSst_buy_full() {
		return sst_buy_full;
	}
	public void setSst_buy_full(Double sst_buy_full) {
		this.sst_buy_full = sst_buy_full;
	}
	public Integer getSst_buy_number() {
		return sst_buy_number;
	}
	public void setSst_buy_number(Integer sst_buy_number) {
		this.sst_buy_number = sst_buy_number;
	}
	public Double getSst_donation_amount() {
		return sst_donation_amount;
	}
	public void setSst_donation_amount(Double sst_donation_amount) {
		this.sst_donation_amount = sst_donation_amount;
	}
	public Double getSst_reduce_amount() {
		return sst_reduce_amount;
	}
	public void setSst_reduce_amount(Double sst_reduce_amount) {
		this.sst_reduce_amount = sst_reduce_amount;
	}
	public Double getSst_increased_amount() {
		return sst_increased_amount;
	}
	public void setSst_increased_amount(Double sst_increased_amount) {
		this.sst_increased_amount = sst_increased_amount;
	}
	public Integer getSst_index() {
		return sst_index;
	}
	public void setSst_index(Integer sst_index) {
		this.sst_index = sst_index;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getTp_name() {
		return tp_name;
	}
	public void setTp_name(String tp_name) {
		this.tp_name = tp_name;
	}
}
