package zy.entity.shop.sale;

import java.io.Serializable;

public class T_Shop_Sale_All implements Serializable{
	private static final long serialVersionUID = -5933914747225546123L;
	private Integer ssa_id;
	private String ssa_ss_code;
	private Double ssa_discount;
	private Double ssa_buy_full;
	private Integer ssa_buy_number;
	private Integer ssa_donation_number;
	private Double ssa_donation_amount;
	private Double ssa_reduce_amount;
	private Double ssa_increased_amount;
	private Integer ssa_index;
	private Integer companyid;
	public Integer getSsa_id() {
		return ssa_id;
	}
	public void setSsa_id(Integer ssa_id) {
		this.ssa_id = ssa_id;
	}
	public String getSsa_ss_code() {
		return ssa_ss_code;
	}
	public void setSsa_ss_code(String ssa_ss_code) {
		this.ssa_ss_code = ssa_ss_code;
	}
	public Double getSsa_discount() {
		return ssa_discount;
	}
	public void setSsa_discount(Double ssa_discount) {
		this.ssa_discount = ssa_discount;
	}
	public Double getSsa_buy_full() {
		return ssa_buy_full;
	}
	public void setSsa_buy_full(Double ssa_buy_full) {
		this.ssa_buy_full = ssa_buy_full;
	}
	public Integer getSsa_buy_number() {
		return ssa_buy_number;
	}
	public void setSsa_buy_number(Integer ssa_buy_number) {
		this.ssa_buy_number = ssa_buy_number;
	}
	public Double getSsa_donation_amount() {
		return ssa_donation_amount;
	}
	public void setSsa_donation_amount(Double ssa_donation_amount) {
		this.ssa_donation_amount = ssa_donation_amount;
	}
	public Double getSsa_reduce_amount() {
		return ssa_reduce_amount;
	}
	public void setSsa_reduce_amount(Double ssa_reduce_amount) {
		this.ssa_reduce_amount = ssa_reduce_amount;
	}
	public Double getSsa_increased_amount() {
		return ssa_increased_amount;
	}
	public void setSsa_increased_amount(Double ssa_increased_amount) {
		this.ssa_increased_amount = ssa_increased_amount;
	}
	public Integer getSsa_index() {
		return ssa_index;
	}
	public void setSsa_index(Integer ssa_index) {
		this.ssa_index = ssa_index;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public Integer getSsa_donation_number() {
		return ssa_donation_number;
	}
	public void setSsa_donation_number(Integer ssa_donation_number) {
		this.ssa_donation_number = ssa_donation_number;
	}
}
