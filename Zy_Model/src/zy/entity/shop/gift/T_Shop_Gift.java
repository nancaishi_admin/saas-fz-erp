package zy.entity.shop.gift;

import java.io.Serializable;

public class T_Shop_Gift implements Serializable{
	private static final long serialVersionUID = -5933914747225546123L;
	private Integer gi_id;
	private String gi_pd_code;
	private String gi_shop_code;
	private String gi_begindate;
	private String gi_enddate;
	private String gi_sysdate;
	private Integer gi_state;
	private Integer gi_point;
	private Integer companyid;
	private String pd_no;
	private String pd_name;
	private String sp_name;
	private String sp_upcode;
	private String sp_shop_type;
	private Integer totalamount;
	private Integer getamount;
	public Integer getGi_id() {
		return gi_id;
	}
	public void setGi_id(Integer gi_id) {
		this.gi_id = gi_id;
	}
	public String getGi_pd_code() {
		return gi_pd_code;
	}
	public void setGi_pd_code(String gi_pd_code) {
		this.gi_pd_code = gi_pd_code;
	}
	public String getGi_shop_code() {
		return gi_shop_code;
	}
	public void setGi_shop_code(String gi_shop_code) {
		this.gi_shop_code = gi_shop_code;
	}
	public String getGi_begindate() {
		return gi_begindate;
	}
	public void setGi_begindate(String gi_begindate) {
		this.gi_begindate = gi_begindate;
	}
	public String getGi_enddate() {
		return gi_enddate;
	}
	public void setGi_enddate(String gi_enddate) {
		this.gi_enddate = gi_enddate;
	}
	public String getGi_sysdate() {
		return gi_sysdate;
	}
	public void setGi_sysdate(String gi_sysdate) {
		this.gi_sysdate = gi_sysdate;
	}
	public Integer getGi_state() {
		return gi_state;
	}
	public void setGi_state(Integer gi_state) {
		this.gi_state = gi_state;
	}
	public Integer getGi_point() {
		return gi_point;
	}
	public void setGi_point(Integer gi_point) {
		this.gi_point = gi_point;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getPd_no() {
		return pd_no;
	}
	public void setPd_no(String pd_no) {
		this.pd_no = pd_no;
	}
	public String getPd_name() {
		return pd_name;
	}
	public void setPd_name(String pd_name) {
		this.pd_name = pd_name;
	}
	public String getSp_name() {
		return sp_name;
	}
	public void setSp_name(String sp_name) {
		this.sp_name = sp_name;
	}
	public Integer getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(Integer totalamount) {
		this.totalamount = totalamount;
	}
	public Integer getGetamount() {
		return getamount;
	}
	public void setGetamount(Integer getamount) {
		this.getamount = getamount;
	}
	public String getSp_upcode() {
		return sp_upcode;
	}
	public void setSp_upcode(String sp_upcode) {
		this.sp_upcode = sp_upcode;
	}
	public String getSp_shop_type() {
		return sp_shop_type;
	}
	public void setSp_shop_type(String sp_shop_type) {
		this.sp_shop_type = sp_shop_type;
	}
}
