package zy.entity.shop.service;

import java.io.Serializable;

public class T_Shop_Service_Item implements Serializable{
	private static final long serialVersionUID = 7724350842814933915L;
	private Integer ssi_id;
	private String ssi_name;
	private Integer companyid;
	public Integer getSsi_id() {
		return ssi_id;
	}
	public void setSsi_id(Integer ssi_id) {
		this.ssi_id = ssi_id;
	}
	public String getSsi_name() {
		return ssi_name;
	}
	public void setSsi_name(String ssi_name) {
		this.ssi_name = ssi_name;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
