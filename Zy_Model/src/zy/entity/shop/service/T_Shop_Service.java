package zy.entity.shop.service;

import java.io.Serializable;

public class T_Shop_Service implements Serializable{
	private static final long serialVersionUID = 7724350842814933915L;
	private Integer ss_id;
	private String ss_pd_no;
	private String ss_name;
	private String ss_tel;
	private String ss_startdate;
	private String ss_servicedate;
	private String ss_enddate;
	private String ss_manager;
	private String ss_pickupname;
	private String ss_question;
	private String ss_hadsolve;
	private Integer ss_state;
	private Integer ss_sms_count;
	private String ss_satis;
	private String ss_result;
	private String ss_shop_code;
	private String ss_ca_emcode;
	private String ss_img_path;
	private Integer ss_ssi_id;
	private String ss_sysdate;
	private Integer companyid;
	private String ssi_name;
	private String ss_shop_name;
	public Integer getSs_id() {
		return ss_id;
	}
	public void setSs_id(Integer ss_id) {
		this.ss_id = ss_id;
	}
	public String getSs_pd_no() {
		return ss_pd_no;
	}
	public void setSs_pd_no(String ss_pd_no) {
		this.ss_pd_no = ss_pd_no;
	}
	public String getSs_name() {
		return ss_name;
	}
	public void setSs_name(String ss_name) {
		this.ss_name = ss_name;
	}
	public String getSs_tel() {
		return ss_tel;
	}
	public void setSs_tel(String ss_tel) {
		this.ss_tel = ss_tel;
	}
	public String getSs_startdate() {
		return ss_startdate;
	}
	public void setSs_startdate(String ss_startdate) {
		this.ss_startdate = ss_startdate;
	}
	public String getSs_servicedate() {
		return ss_servicedate;
	}
	public void setSs_servicedate(String ss_servicedate) {
		this.ss_servicedate = ss_servicedate;
	}
	public String getSs_enddate() {
		return ss_enddate;
	}
	public void setSs_enddate(String ss_enddate) {
		this.ss_enddate = ss_enddate;
	}
	public String getSs_manager() {
		return ss_manager;
	}
	public void setSs_manager(String ss_manager) {
		this.ss_manager = ss_manager;
	}
	public String getSs_pickupname() {
		return ss_pickupname;
	}
	public void setSs_pickupname(String ss_pickupname) {
		this.ss_pickupname = ss_pickupname;
	}
	public String getSs_question() {
		return ss_question;
	}
	public void setSs_question(String ss_question) {
		this.ss_question = ss_question;
	}
	public String getSs_hadsolve() {
		return ss_hadsolve;
	}
	public void setSs_hadsolve(String ss_hadsolve) {
		this.ss_hadsolve = ss_hadsolve;
	}
	public Integer getSs_state() {
		return ss_state;
	}
	public void setSs_state(Integer ss_state) {
		this.ss_state = ss_state;
	}
	public Integer getSs_sms_count() {
		return ss_sms_count;
	}
	public void setSs_sms_count(Integer ss_sms_count) {
		this.ss_sms_count = ss_sms_count;
	}
	public String getSs_satis() {
		return ss_satis;
	}
	public void setSs_satis(String ss_satis) {
		this.ss_satis = ss_satis;
	}
	public String getSs_result() {
		return ss_result;
	}
	public void setSs_result(String ss_result) {
		this.ss_result = ss_result;
	}
	public String getSs_shop_code() {
		return ss_shop_code;
	}
	public void setSs_shop_code(String ss_shop_code) {
		this.ss_shop_code = ss_shop_code;
	}
	public String getSs_ca_emcode() {
		return ss_ca_emcode;
	}
	public void setSs_ca_emcode(String ss_ca_emcode) {
		this.ss_ca_emcode = ss_ca_emcode;
	}
	public String getSs_img_path() {
		return ss_img_path;
	}
	public void setSs_img_path(String ss_img_path) {
		this.ss_img_path = ss_img_path;
	}
	public Integer getSs_ssi_id() {
		return ss_ssi_id;
	}
	public void setSs_ssi_id(Integer ss_ssi_id) {
		this.ss_ssi_id = ss_ssi_id;
	}
	public String getSs_sysdate() {
		return ss_sysdate;
	}
	public void setSs_sysdate(String ss_sysdate) {
		this.ss_sysdate = ss_sysdate;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getSsi_name() {
		return ssi_name;
	}
	public void setSsi_name(String ssi_name) {
		this.ssi_name = ssi_name;
	}
	public String getSs_shop_name() {
		return ss_shop_name;
	}
	public void setSs_shop_name(String ss_shop_name) {
		this.ss_shop_name = ss_shop_name;
	}
}
