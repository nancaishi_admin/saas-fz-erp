package zy.entity.shop.program;

import java.io.Serializable;

public class T_Shop_Program_Shop implements Serializable{
	private static final long serialVersionUID = -5933914747225546123L;
	private Integer sps_id;
	private Integer sps_sp_id;
	private Integer sps_state;
	private String sps_shop_code;
	private Integer companyid;
	public Integer getSps_id() {
		return sps_id;
	}
	public void setSps_id(Integer sps_id) {
		this.sps_id = sps_id;
	}
	public Integer getSps_sp_id() {
		return sps_sp_id;
	}
	public void setSps_sp_id(Integer sps_sp_id) {
		this.sps_sp_id = sps_sp_id;
	}
	public Integer getSps_state() {
		return sps_state;
	}
	public void setSps_state(Integer sps_state) {
		this.sps_state = sps_state;
	}
	public String getSps_shop_code() {
		return sps_shop_code;
	}
	public void setSps_shop_code(String sps_shop_code) {
		this.sps_shop_code = sps_shop_code;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
