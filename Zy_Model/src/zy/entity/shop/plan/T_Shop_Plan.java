package zy.entity.shop.plan;

import java.io.Serializable;

public class T_Shop_Plan implements Serializable{
	private static final long serialVersionUID = -5459462170908745161L;
	private Integer pl_id;
	private String pl_number;
	private String pl_sysdate;
	private String pl_date;
	private String pl_shop_code;
	private String shop_name;
	private String pl_name;
	private Integer pl_year;
	private Integer pl_type;
	private Double pl_gross_profit_rate;
	private Double pl_sell_grow_percent;
	private Double pl_expense_grow_percent;
	private Double pl_breakeven_grow_percent;
	private Double pl_sell_money;
	private Double pl_cost_money;
	private Double pl_gross_money;
	private Double pl_net_money;
	private Double pl_breakeven;
	private Double pl_expense;
	private Double pl_sell_money_pre;
	private Double pl_cost_money_pre;
	private Double pl_gross_money_pre;
	private Double pl_net_money_pre;
	private Double pl_breakeven_pre;
	private Double pl_expense_pre;
	private Double pl_gross_profit_rate_pre;
	private Integer pl_us_id;
	private Integer pl_ar_state;
	private String pl_ar_date;
	private String pl_remark;
	private String pl_maker;
	private Integer companyid;
	public Integer getPl_id() {
		return pl_id;
	}
	public void setPl_id(Integer pl_id) {
		this.pl_id = pl_id;
	}
	public String getPl_number() {
		return pl_number;
	}
	public void setPl_number(String pl_number) {
		this.pl_number = pl_number;
	}
	public String getPl_sysdate() {
		return pl_sysdate;
	}
	public void setPl_sysdate(String pl_sysdate) {
		this.pl_sysdate = pl_sysdate;
	}
	public String getPl_date() {
		return pl_date;
	}
	public void setPl_date(String pl_date) {
		this.pl_date = pl_date;
	}
	public String getPl_shop_code() {
		return pl_shop_code;
	}
	public void setPl_shop_code(String pl_shop_code) {
		this.pl_shop_code = pl_shop_code;
	}
	public String getShop_name() {
		return shop_name;
	}
	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}
	public String getPl_name() {
		return pl_name;
	}
	public void setPl_name(String pl_name) {
		this.pl_name = pl_name;
	}
	public Integer getPl_year() {
		return pl_year;
	}
	public void setPl_year(Integer pl_year) {
		this.pl_year = pl_year;
	}
	public Integer getPl_type() {
		return pl_type;
	}
	public void setPl_type(Integer pl_type) {
		this.pl_type = pl_type;
	}
	public Double getPl_gross_profit_rate() {
		return pl_gross_profit_rate;
	}
	public void setPl_gross_profit_rate(Double pl_gross_profit_rate) {
		this.pl_gross_profit_rate = pl_gross_profit_rate;
	}
	public Double getPl_sell_grow_percent() {
		return pl_sell_grow_percent;
	}
	public void setPl_sell_grow_percent(Double pl_sell_grow_percent) {
		this.pl_sell_grow_percent = pl_sell_grow_percent;
	}
	public Double getPl_expense_grow_percent() {
		return pl_expense_grow_percent;
	}
	public void setPl_expense_grow_percent(Double pl_expense_grow_percent) {
		this.pl_expense_grow_percent = pl_expense_grow_percent;
	}
	public Double getPl_breakeven_grow_percent() {
		return pl_breakeven_grow_percent;
	}
	public void setPl_breakeven_grow_percent(Double pl_breakeven_grow_percent) {
		this.pl_breakeven_grow_percent = pl_breakeven_grow_percent;
	}
	public Double getPl_sell_money() {
		return pl_sell_money;
	}
	public void setPl_sell_money(Double pl_sell_money) {
		this.pl_sell_money = pl_sell_money;
	}
	public Double getPl_cost_money() {
		return pl_cost_money;
	}
	public void setPl_cost_money(Double pl_cost_money) {
		this.pl_cost_money = pl_cost_money;
	}
	public Double getPl_gross_money() {
		return pl_gross_money;
	}
	public void setPl_gross_money(Double pl_gross_money) {
		this.pl_gross_money = pl_gross_money;
	}
	public Double getPl_net_money() {
		return pl_net_money;
	}
	public void setPl_net_money(Double pl_net_money) {
		this.pl_net_money = pl_net_money;
	}
	public Double getPl_breakeven() {
		return pl_breakeven;
	}
	public void setPl_breakeven(Double pl_breakeven) {
		this.pl_breakeven = pl_breakeven;
	}
	public Double getPl_expense() {
		return pl_expense;
	}
	public void setPl_expense(Double pl_expense) {
		this.pl_expense = pl_expense;
	}
	public Double getPl_sell_money_pre() {
		return pl_sell_money_pre;
	}
	public void setPl_sell_money_pre(Double pl_sell_money_pre) {
		this.pl_sell_money_pre = pl_sell_money_pre;
	}
	public Double getPl_cost_money_pre() {
		return pl_cost_money_pre;
	}
	public void setPl_cost_money_pre(Double pl_cost_money_pre) {
		this.pl_cost_money_pre = pl_cost_money_pre;
	}
	public Double getPl_gross_money_pre() {
		return pl_gross_money_pre;
	}
	public void setPl_gross_money_pre(Double pl_gross_money_pre) {
		this.pl_gross_money_pre = pl_gross_money_pre;
	}
	public Double getPl_net_money_pre() {
		return pl_net_money_pre;
	}
	public void setPl_net_money_pre(Double pl_net_money_pre) {
		this.pl_net_money_pre = pl_net_money_pre;
	}
	public Double getPl_breakeven_pre() {
		return pl_breakeven_pre;
	}
	public void setPl_breakeven_pre(Double pl_breakeven_pre) {
		this.pl_breakeven_pre = pl_breakeven_pre;
	}
	public Double getPl_expense_pre() {
		return pl_expense_pre;
	}
	public void setPl_expense_pre(Double pl_expense_pre) {
		this.pl_expense_pre = pl_expense_pre;
	}
	public Double getPl_gross_profit_rate_pre() {
		return pl_gross_profit_rate_pre;
	}
	public void setPl_gross_profit_rate_pre(Double pl_gross_profit_rate_pre) {
		this.pl_gross_profit_rate_pre = pl_gross_profit_rate_pre;
	}
	public Integer getPl_us_id() {
		return pl_us_id;
	}
	public void setPl_us_id(Integer pl_us_id) {
		this.pl_us_id = pl_us_id;
	}
	public Integer getPl_ar_state() {
		return pl_ar_state;
	}
	public void setPl_ar_state(Integer pl_ar_state) {
		this.pl_ar_state = pl_ar_state;
	}
	public String getPl_ar_date() {
		return pl_ar_date;
	}
	public void setPl_ar_date(String pl_ar_date) {
		this.pl_ar_date = pl_ar_date;
	}
	public String getPl_remark() {
		return pl_remark;
	}
	public void setPl_remark(String pl_remark) {
		this.pl_remark = pl_remark;
	}
	public String getPl_maker() {
		return pl_maker;
	}
	public void setPl_maker(String pl_maker) {
		this.pl_maker = pl_maker;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
