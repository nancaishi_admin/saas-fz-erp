package zy.entity.shop.plan;

import java.io.Serializable;

public class T_Shop_MonthPlan implements Serializable{
	private static final long serialVersionUID = -7965779234807311984L;
	private Integer mp_id;
	private String mp_number;
	private String mp_shop_code;
	private String shop_name;
	private Integer mp_year;
	private Integer mp_month;
	private Double mp_sell_money;
	private String mp_remark;
	private Integer mp_us_id;
	private Integer mp_ar_state;
	private String mp_ar_date;
	private String mp_maker;
	private String mp_sysdate;
	private Integer companyid;
	public Integer getMp_id() {
		return mp_id;
	}
	public void setMp_id(Integer mp_id) {
		this.mp_id = mp_id;
	}
	public String getMp_number() {
		return mp_number;
	}
	public void setMp_number(String mp_number) {
		this.mp_number = mp_number;
	}
	public String getMp_shop_code() {
		return mp_shop_code;
	}
	public void setMp_shop_code(String mp_shop_code) {
		this.mp_shop_code = mp_shop_code;
	}
	public String getShop_name() {
		return shop_name;
	}
	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}
	public Integer getMp_year() {
		return mp_year;
	}
	public void setMp_year(Integer mp_year) {
		this.mp_year = mp_year;
	}
	public Integer getMp_month() {
		return mp_month;
	}
	public void setMp_month(Integer mp_month) {
		this.mp_month = mp_month;
	}
	public Double getMp_sell_money() {
		return mp_sell_money;
	}
	public void setMp_sell_money(Double mp_sell_money) {
		this.mp_sell_money = mp_sell_money;
	}
	public String getMp_remark() {
		return mp_remark;
	}
	public void setMp_remark(String mp_remark) {
		this.mp_remark = mp_remark;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public Integer getMp_us_id() {
		return mp_us_id;
	}
	public void setMp_us_id(Integer mp_us_id) {
		this.mp_us_id = mp_us_id;
	}
	public Integer getMp_ar_state() {
		return mp_ar_state;
	}
	public void setMp_ar_state(Integer mp_ar_state) {
		this.mp_ar_state = mp_ar_state;
	}
	public String getMp_ar_date() {
		return mp_ar_date;
	}
	public void setMp_ar_date(String mp_ar_date) {
		this.mp_ar_date = mp_ar_date;
	}
	public String getMp_maker() {
		return mp_maker;
	}
	public void setMp_maker(String mp_maker) {
		this.mp_maker = mp_maker;
	}
	public String getMp_sysdate() {
		return mp_sysdate;
	}
	public void setMp_sysdate(String mp_sysdate) {
		this.mp_sysdate = mp_sysdate;
	}
}
