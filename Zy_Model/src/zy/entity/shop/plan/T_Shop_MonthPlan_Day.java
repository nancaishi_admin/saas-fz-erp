package zy.entity.shop.plan;

import java.io.Serializable;

public class T_Shop_MonthPlan_Day implements Serializable{
	private static final long serialVersionUID = -6520226936871524608L;
	private Integer mpd_id;
	private String mpd_number;
	private Integer mpd_year;
	private Integer mpd_month;
	private Integer mpd_day;
	private Double mpd_sell_money_plan;
	private Double mpd_sell_money_pre;
	private String mpd_remark;
	private Integer companyid;
	public Integer getMpd_id() {
		return mpd_id;
	}
	public void setMpd_id(Integer mpd_id) {
		this.mpd_id = mpd_id;
	}
	public String getMpd_number() {
		return mpd_number;
	}
	public void setMpd_number(String mpd_number) {
		this.mpd_number = mpd_number;
	}
	public Integer getMpd_year() {
		return mpd_year;
	}
	public void setMpd_year(Integer mpd_year) {
		this.mpd_year = mpd_year;
	}
	public Integer getMpd_month() {
		return mpd_month;
	}
	public void setMpd_month(Integer mpd_month) {
		this.mpd_month = mpd_month;
	}
	public Integer getMpd_day() {
		return mpd_day;
	}
	public void setMpd_day(Integer mpd_day) {
		this.mpd_day = mpd_day;
	}
	public Double getMpd_sell_money_plan() {
		return mpd_sell_money_plan;
	}
	public void setMpd_sell_money_plan(Double mpd_sell_money_plan) {
		this.mpd_sell_money_plan = mpd_sell_money_plan;
	}
	public Double getMpd_sell_money_pre() {
		return mpd_sell_money_pre;
	}
	public void setMpd_sell_money_pre(Double mpd_sell_money_pre) {
		this.mpd_sell_money_pre = mpd_sell_money_pre;
	}
	public String getMpd_remark() {
		return mpd_remark;
	}
	public void setMpd_remark(String mpd_remark) {
		this.mpd_remark = mpd_remark;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
