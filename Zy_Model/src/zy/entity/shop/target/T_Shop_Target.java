package zy.entity.shop.target;

import java.io.Serializable;

public class T_Shop_Target implements Serializable{
	private Integer ta_id;
	private String ta_number;
	private Integer ta_type;
	private String ta_shop_code;
	private String ta_begintime;
	private String ta_endtime;
	private Double ta_money;
	private Double ta_realmoney;
	private Integer ta_vipcount;
	private Integer ta_realcount;
	private Integer ta_state;
	private String ta_complete;
	private String ta_hinder;
	private String ta_method;
	private String ta_gain;
	private String ta_lead;
	private String ta_createtime;
	private String ta_summary;
	private Integer companyid;
	public Integer getTa_id() {
		return ta_id;
	}
	public void setTa_id(Integer ta_id) {
		this.ta_id = ta_id;
	}
	public String getTa_number() {
		return ta_number;
	}
	public void setTa_number(String ta_number) {
		this.ta_number = ta_number;
	}
	public Integer getTa_type() {
		return ta_type;
	}
	public void setTa_type(Integer ta_type) {
		this.ta_type = ta_type;
	}
	public String getTa_shop_code() {
		return ta_shop_code;
	}
	public void setTa_shop_code(String ta_shop_code) {
		this.ta_shop_code = ta_shop_code;
	}
	public String getTa_begintime() {
		return ta_begintime;
	}
	public void setTa_begintime(String ta_begintime) {
		this.ta_begintime = ta_begintime;
	}
	public String getTa_endtime() {
		return ta_endtime;
	}
	public void setTa_endtime(String ta_endtime) {
		this.ta_endtime = ta_endtime;
	}
	public Double getTa_money() {
		return ta_money;
	}
	public void setTa_money(Double ta_money) {
		this.ta_money = ta_money;
	}
	public Double getTa_realmoney() {
		return ta_realmoney;
	}
	public void setTa_realmoney(Double ta_realmoney) {
		this.ta_realmoney = ta_realmoney;
	}
	public Integer getTa_vipcount() {
		return ta_vipcount;
	}
	public void setTa_vipcount(Integer ta_vipcount) {
		this.ta_vipcount = ta_vipcount;
	}
	public Integer getTa_realcount() {
		return ta_realcount;
	}
	public void setTa_realcount(Integer ta_realcount) {
		this.ta_realcount = ta_realcount;
	}
	public Integer getTa_state() {
		return ta_state;
	}
	public void setTa_state(Integer ta_state) {
		this.ta_state = ta_state;
	}
	public String getTa_complete() {
		return ta_complete;
	}
	public void setTa_complete(String ta_complete) {
		this.ta_complete = ta_complete;
	}
	public String getTa_hinder() {
		return ta_hinder;
	}
	public void setTa_hinder(String ta_hinder) {
		this.ta_hinder = ta_hinder;
	}
	public String getTa_method() {
		return ta_method;
	}
	public void setTa_method(String ta_method) {
		this.ta_method = ta_method;
	}
	public String getTa_gain() {
		return ta_gain;
	}
	public void setTa_gain(String ta_gain) {
		this.ta_gain = ta_gain;
	}
	public String getTa_lead() {
		return ta_lead;
	}
	public void setTa_lead(String ta_lead) {
		this.ta_lead = ta_lead;
	}
	public String getTa_createtime() {
		return ta_createtime;
	}
	public void setTa_createtime(String ta_createtime) {
		this.ta_createtime = ta_createtime;
	}
	public String getTa_summary() {
		return ta_summary;
	}
	public void setTa_summary(String ta_summary) {
		this.ta_summary = ta_summary;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}

}
