package zy.entity.shop.target;

import java.io.Serializable;

public class T_Shop_Target_Detail implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer tad_id;
	private String tad_ta_number;
	private String tad_em_code;
	private String tad_em_name;
	private Double tad_target_money;
	private Double tad_real_money;
	private Integer tad_vip_count;
	private Integer tad_real_count;
	private Integer tad_project_type;
	private String tad_remark;
	private String tad_select;
	private String tad_target_content;
	private Integer companyid;
	public Integer getTad_id() {
		return tad_id;
	}
	public void setTad_id(Integer tad_id) {
		this.tad_id = tad_id;
	}
	public String getTad_ta_number() {
		return tad_ta_number;
	}
	public void setTad_ta_number(String tad_ta_number) {
		this.tad_ta_number = tad_ta_number;
	}
	public String getTad_em_code() {
		return tad_em_code;
	}
	public void setTad_em_code(String tad_em_code) {
		this.tad_em_code = tad_em_code;
	}
	public String getTad_em_name() {
		return tad_em_name;
	}
	public void setTad_em_name(String tad_em_name) {
		this.tad_em_name = tad_em_name;
	}
	public Double getTad_target_money() {
		return tad_target_money;
	}
	public void setTad_target_money(Double tad_target_money) {
		this.tad_target_money = tad_target_money;
	}
	public Double getTad_real_money() {
		return tad_real_money;
	}
	public void setTad_real_money(Double tad_real_money) {
		this.tad_real_money = tad_real_money;
	}
	public Integer getTad_vip_count() {
		return tad_vip_count;
	}
	public void setTad_vip_count(Integer tad_vip_count) {
		this.tad_vip_count = tad_vip_count;
	}
	public Integer getTad_real_count() {
		return tad_real_count;
	}
	public void setTad_real_count(Integer tad_real_count) {
		this.tad_real_count = tad_real_count;
	}
	public Integer getTad_project_type() {
		return tad_project_type;
	}
	public void setTad_project_type(Integer tad_project_type) {
		this.tad_project_type = tad_project_type;
	}
	public String getTad_remark() {
		return tad_remark;
	}
	public void setTad_remark(String tad_remark) {
		this.tad_remark = tad_remark;
	}
	public String getTad_select() {
		return tad_select;
	}
	public void setTad_select(String tad_select) {
		this.tad_select = tad_select;
	}
	public String getTad_target_content() {
		return tad_target_content;
	}
	public void setTad_target_content(String tad_target_content) {
		this.tad_target_content = tad_target_content;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}

}
