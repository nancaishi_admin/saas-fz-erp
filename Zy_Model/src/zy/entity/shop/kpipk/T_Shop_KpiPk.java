package zy.entity.shop.kpipk;

import java.io.Serializable;

public class T_Shop_KpiPk implements Serializable{
	private static final long serialVersionUID = -8897791949178533280L;
	private Integer kp_id;
	private String kp_number;
	private Integer kp_type;
	private String kp_begin;
	private String kp_end;
	private Integer kp_score;
	private String kp_remark;
	private Integer kp_state;
	private String kp_summary;
	private String kp_sysdate;
	private String kp_shop_code;
	private Integer kp_us_id;
	private Integer companyid;
	private String kpl_name;
	private String reward_codes;
	private String reward_names;
	public Integer getKp_id() {
		return kp_id;
	}
	public void setKp_id(Integer kp_id) {
		this.kp_id = kp_id;
	}
	public String getKp_number() {
		return kp_number;
	}
	public void setKp_number(String kp_number) {
		this.kp_number = kp_number;
	}
	public Integer getKp_type() {
		return kp_type;
	}
	public void setKp_type(Integer kp_type) {
		this.kp_type = kp_type;
	}
	public String getKp_begin() {
		return kp_begin;
	}
	public void setKp_begin(String kp_begin) {
		this.kp_begin = kp_begin;
	}
	public String getKp_end() {
		return kp_end;
	}
	public void setKp_end(String kp_end) {
		this.kp_end = kp_end;
	}
	public Integer getKp_score() {
		return kp_score;
	}
	public void setKp_score(Integer kp_score) {
		this.kp_score = kp_score;
	}
	public String getKp_remark() {
		return kp_remark;
	}
	public void setKp_remark(String kp_remark) {
		this.kp_remark = kp_remark;
	}
	public Integer getKp_state() {
		return kp_state;
	}
	public void setKp_state(Integer kp_state) {
		this.kp_state = kp_state;
	}
	public String getKp_summary() {
		return kp_summary;
	}
	public void setKp_summary(String kp_summary) {
		this.kp_summary = kp_summary;
	}
	public String getKp_sysdate() {
		return kp_sysdate;
	}
	public void setKp_sysdate(String kp_sysdate) {
		this.kp_sysdate = kp_sysdate;
	}
	public String getKp_shop_code() {
		return kp_shop_code;
	}
	public void setKp_shop_code(String kp_shop_code) {
		this.kp_shop_code = kp_shop_code;
	}
	public Integer getKp_us_id() {
		return kp_us_id;
	}
	public void setKp_us_id(Integer kp_us_id) {
		this.kp_us_id = kp_us_id;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getKpl_name() {
		return kpl_name;
	}
	public void setKpl_name(String kpl_name) {
		this.kpl_name = kpl_name;
	}
	public String getReward_codes() {
		return reward_codes;
	}
	public void setReward_codes(String reward_codes) {
		this.reward_codes = reward_codes;
	}
	public String getReward_names() {
		return reward_names;
	}
	public void setReward_names(String reward_names) {
		this.reward_names = reward_names;
	}
}
