package zy.entity.shop.kpiassess;

import java.io.Serializable;

public class T_Shop_KpiAssessReward implements Serializable{
	private static final long serialVersionUID = 1300796973029720050L;
	private Integer kar_id;
	private String kar_number;
	private String kar_kal_code;
	private String kar_ki_code;
	private String kar_rw_code;
	private Integer companyid;
	public Integer getKar_id() {
		return kar_id;
	}
	public void setKar_id(Integer kar_id) {
		this.kar_id = kar_id;
	}
	public String getKar_number() {
		return kar_number;
	}
	public void setKar_number(String kar_number) {
		this.kar_number = kar_number;
	}
	public String getKar_kal_code() {
		return kar_kal_code;
	}
	public void setKar_kal_code(String kar_kal_code) {
		this.kar_kal_code = kar_kal_code;
	}
	public String getKar_ki_code() {
		return kar_ki_code;
	}
	public void setKar_ki_code(String kar_ki_code) {
		this.kar_ki_code = kar_ki_code;
	}
	public String getKar_rw_code() {
		return kar_rw_code;
	}
	public void setKar_rw_code(String kar_rw_code) {
		this.kar_rw_code = kar_rw_code;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
