package zy.entity.shop.kpiassess;

import java.io.Serializable;

public class T_Shop_KpiAssessList implements Serializable{
	private static final long serialVersionUID = 3074596856086812809L;
	private Integer kal_id;
	private String kal_number;
	private String kal_code;
	private String kal_name;
	private String kal_ki_code;
	private String kal_complete;
	private Integer kal_score;
	private Integer companyid;
	private String ki_name;
	private String ki_identity;
	private String reward_codes;
	private String reward_names;
	public Integer getKal_id() {
		return kal_id;
	}
	public void setKal_id(Integer kal_id) {
		this.kal_id = kal_id;
	}
	public String getKal_number() {
		return kal_number;
	}
	public void setKal_number(String kal_number) {
		this.kal_number = kal_number;
	}
	public String getKal_code() {
		return kal_code;
	}
	public void setKal_code(String kal_code) {
		this.kal_code = kal_code;
	}
	public String getKal_name() {
		return kal_name;
	}
	public void setKal_name(String kal_name) {
		this.kal_name = kal_name;
	}
	public String getKal_ki_code() {
		return kal_ki_code;
	}
	public void setKal_ki_code(String kal_ki_code) {
		this.kal_ki_code = kal_ki_code;
	}
	public String getKal_complete() {
		return kal_complete;
	}
	public void setKal_complete(String kal_complete) {
		this.kal_complete = kal_complete;
	}
	public Integer getKal_score() {
		return kal_score;
	}
	public void setKal_score(Integer kal_score) {
		this.kal_score = kal_score;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getKi_identity() {
		return ki_identity;
	}
	public void setKi_identity(String ki_identity) {
		this.ki_identity = ki_identity;
	}
	public String getKi_name() {
		return ki_name;
	}
	public void setKi_name(String ki_name) {
		this.ki_name = ki_name;
	}
	public String getReward_codes() {
		return reward_codes;
	}
	public void setReward_codes(String reward_codes) {
		this.reward_codes = reward_codes;
	}
	public String getReward_names() {
		return reward_names;
	}
	public void setReward_names(String reward_names) {
		this.reward_names = reward_names;
	}
}
