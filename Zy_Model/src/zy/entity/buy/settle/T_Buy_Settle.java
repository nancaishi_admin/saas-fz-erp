package zy.entity.buy.settle;

import java.io.Serializable;

public class T_Buy_Settle implements Serializable{
	private static final long serialVersionUID = -4132956415174234737L;
	private Integer st_id;
	private String st_number;
	private String st_date;
	private String st_supply_code;
	private String supply_name;
	private Double st_discount_money;
	private Double st_prepay;
	private Double st_paid;
	private Double st_paidmore;
	private String st_maker;
	private String st_manager;
	private String st_ba_code;
	private String ba_name;
	private Integer st_ar_state;
	private String st_ar_date;
	private Integer st_entire;
	private String st_pp_number;
	private Double st_leftdebt;
	private String st_remark;
	private String st_sysdate;
	private Integer st_us_id;
	private Integer companyid;
	private String ar_describe;
	public Integer getSt_id() {
		return st_id;
	}
	public void setSt_id(Integer st_id) {
		this.st_id = st_id;
	}
	public String getSt_number() {
		return st_number;
	}
	public void setSt_number(String st_number) {
		this.st_number = st_number;
	}
	public String getSt_date() {
		return st_date;
	}
	public void setSt_date(String st_date) {
		this.st_date = st_date;
	}
	public String getSt_supply_code() {
		return st_supply_code;
	}
	public void setSt_supply_code(String st_supply_code) {
		this.st_supply_code = st_supply_code;
	}
	public String getSupply_name() {
		return supply_name;
	}
	public void setSupply_name(String supply_name) {
		this.supply_name = supply_name;
	}
	public Double getSt_discount_money() {
		return st_discount_money;
	}
	public void setSt_discount_money(Double st_discount_money) {
		this.st_discount_money = st_discount_money;
	}
	public Double getSt_prepay() {
		return st_prepay;
	}
	public void setSt_prepay(Double st_prepay) {
		this.st_prepay = st_prepay;
	}
	public Double getSt_paid() {
		return st_paid;
	}
	public void setSt_paid(Double st_paid) {
		this.st_paid = st_paid;
	}
	public Double getSt_paidmore() {
		return st_paidmore;
	}
	public void setSt_paidmore(Double st_paidmore) {
		this.st_paidmore = st_paidmore;
	}
	public String getSt_maker() {
		return st_maker;
	}
	public void setSt_maker(String st_maker) {
		this.st_maker = st_maker;
	}
	public String getSt_manager() {
		return st_manager;
	}
	public void setSt_manager(String st_manager) {
		this.st_manager = st_manager;
	}
	public String getSt_ba_code() {
		return st_ba_code;
	}
	public void setSt_ba_code(String st_ba_code) {
		this.st_ba_code = st_ba_code;
	}
	public String getBa_name() {
		return ba_name;
	}
	public void setBa_name(String ba_name) {
		this.ba_name = ba_name;
	}
	public Integer getSt_ar_state() {
		return st_ar_state;
	}
	public void setSt_ar_state(Integer st_ar_state) {
		this.st_ar_state = st_ar_state;
	}
	public String getSt_ar_date() {
		return st_ar_date;
	}
	public void setSt_ar_date(String st_ar_date) {
		this.st_ar_date = st_ar_date;
	}
	public Integer getSt_entire() {
		return st_entire;
	}
	public void setSt_entire(Integer st_entire) {
		this.st_entire = st_entire;
	}
	public String getSt_pp_number() {
		return st_pp_number;
	}
	public void setSt_pp_number(String st_pp_number) {
		this.st_pp_number = st_pp_number;
	}
	public Double getSt_leftdebt() {
		return st_leftdebt;
	}
	public void setSt_leftdebt(Double st_leftdebt) {
		this.st_leftdebt = st_leftdebt;
	}
	public String getSt_remark() {
		return st_remark;
	}
	public void setSt_remark(String st_remark) {
		this.st_remark = st_remark;
	}
	public String getSt_sysdate() {
		return st_sysdate;
	}
	public void setSt_sysdate(String st_sysdate) {
		this.st_sysdate = st_sysdate;
	}
	public Integer getSt_us_id() {
		return st_us_id;
	}
	public void setSt_us_id(Integer st_us_id) {
		this.st_us_id = st_us_id;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getAr_describe() {
		return ar_describe;
	}
	public void setAr_describe(String ar_describe) {
		this.ar_describe = ar_describe;
	}
}
