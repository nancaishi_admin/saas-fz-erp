package zy.entity.buy.enter;

import java.io.Serializable;

public class T_Buy_EnterList implements Serializable{
	private static final long serialVersionUID = -8816356232022438044L;
	private Integer etl_id;
	private String etl_number;
	private String etl_pd_code;
	private String etl_sub_code;
	private String etl_sz_code;
	private String etl_szg_code;
	private String etl_cr_code;
	private String etl_br_code;
	private Integer etl_amount;
	private Double etl_unitprice;
	private Double etl_unitmoney;
	private Double etl_retailprice;
	private Double etl_retailmoney;
	private Double etl_rate;
	private String etl_remark;
	private String etl_order_number;
	private Integer etl_pi_type;
	private Integer etl_us_id;
	private Integer etl_type;
	private Integer companyid;
	
	private String pd_no;
	private String pd_name;
	private String pd_unit;
	private String pd_season;
	private Integer pd_year;
	private String cr_name;
	private String sz_name;
	private String br_name;
	private String bd_code;
	private String bd_name;
	private String tp_code;
	private String tp_name;
	/**
	 * 操作类型 update,add
	 */
	private String operate_type;
	public Integer getEtl_id() {
		return etl_id;
	}
	public void setEtl_id(Integer etl_id) {
		this.etl_id = etl_id;
	}
	public String getEtl_number() {
		return etl_number;
	}
	public void setEtl_number(String etl_number) {
		this.etl_number = etl_number;
	}
	public String getEtl_pd_code() {
		return etl_pd_code;
	}
	public void setEtl_pd_code(String etl_pd_code) {
		this.etl_pd_code = etl_pd_code;
	}
	public String getEtl_sub_code() {
		return etl_sub_code;
	}
	public void setEtl_sub_code(String etl_sub_code) {
		this.etl_sub_code = etl_sub_code;
	}
	public String getEtl_sz_code() {
		return etl_sz_code;
	}
	public void setEtl_sz_code(String etl_sz_code) {
		this.etl_sz_code = etl_sz_code;
	}
	public String getEtl_szg_code() {
		return etl_szg_code;
	}
	public void setEtl_szg_code(String etl_szg_code) {
		this.etl_szg_code = etl_szg_code;
	}
	public String getEtl_cr_code() {
		return etl_cr_code;
	}
	public void setEtl_cr_code(String etl_cr_code) {
		this.etl_cr_code = etl_cr_code;
	}
	public String getEtl_br_code() {
		return etl_br_code;
	}
	public void setEtl_br_code(String etl_br_code) {
		this.etl_br_code = etl_br_code;
	}
	public Integer getEtl_amount() {
		return etl_amount;
	}
	public void setEtl_amount(Integer etl_amount) {
		this.etl_amount = etl_amount;
	}
	public Double getEtl_unitprice() {
		return etl_unitprice;
	}
	public void setEtl_unitprice(Double etl_unitprice) {
		this.etl_unitprice = etl_unitprice;
	}
	public Double getEtl_unitmoney() {
		if (etl_amount != null && etl_unitprice != null) {
			return etl_amount * etl_unitprice;
		}
		return etl_unitmoney;
	}
	public Double getEtl_retailprice() {
		return etl_retailprice;
	}
	public void setEtl_retailprice(Double etl_retailprice) {
		this.etl_retailprice = etl_retailprice;
	}
	public Double getEtl_retailmoney() {
		if (etl_amount != null && etl_retailprice != null) {
			return etl_amount * etl_retailprice;
		}
		return etl_retailmoney;
	}
	public Double getEtl_rate() {
		if (etl_unitprice != null && etl_retailprice != null && etl_retailprice != 0) {
			return etl_unitprice / etl_retailprice;
		}
		return etl_rate;
	}
	public String getEtl_remark() {
		return etl_remark;
	}
	public void setEtl_remark(String etl_remark) {
		this.etl_remark = etl_remark;
	}
	public String getEtl_order_number() {
		return etl_order_number;
	}
	public void setEtl_order_number(String etl_order_number) {
		this.etl_order_number = etl_order_number;
	}
	public Integer getEtl_pi_type() {
		return etl_pi_type;
	}
	public void setEtl_pi_type(Integer etl_pi_type) {
		this.etl_pi_type = etl_pi_type;
	}
	public Integer getEtl_us_id() {
		return etl_us_id;
	}
	public void setEtl_us_id(Integer etl_us_id) {
		this.etl_us_id = etl_us_id;
	}
	public Integer getEtl_type() {
		return etl_type;
	}
	public void setEtl_type(Integer etl_type) {
		this.etl_type = etl_type;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getPd_no() {
		return pd_no;
	}
	public void setPd_no(String pd_no) {
		this.pd_no = pd_no;
	}
	public String getPd_name() {
		return pd_name;
	}
	public void setPd_name(String pd_name) {
		this.pd_name = pd_name;
	}
	public String getPd_unit() {
		return pd_unit;
	}
	public void setPd_unit(String pd_unit) {
		this.pd_unit = pd_unit;
	}
	public String getPd_season() {
		return pd_season;
	}
	public void setPd_season(String pd_season) {
		this.pd_season = pd_season;
	}
	public Integer getPd_year() {
		return pd_year;
	}
	public void setPd_year(Integer pd_year) {
		this.pd_year = pd_year;
	}
	public String getCr_name() {
		return cr_name;
	}
	public void setCr_name(String cr_name) {
		this.cr_name = cr_name;
	}
	public String getSz_name() {
		return sz_name;
	}
	public void setSz_name(String sz_name) {
		this.sz_name = sz_name;
	}
	public String getBr_name() {
		return br_name;
	}
	public void setBr_name(String br_name) {
		this.br_name = br_name;
	}
	public String getBd_name() {
		return bd_name;
	}
	public void setBd_name(String bd_name) {
		this.bd_name = bd_name;
	}
	public String getTp_name() {
		return tp_name;
	}
	public void setTp_name(String tp_name) {
		this.tp_name = tp_name;
	}
	public String getOperate_type() {
		return operate_type;
	}
	public void setOperate_type(String operate_type) {
		this.operate_type = operate_type;
	}
	public String getBd_code() {
		return bd_code;
	}
	public void setBd_code(String bd_code) {
		this.bd_code = bd_code;
	}
	public String getTp_code() {
		return tp_code;
	}
	public void setTp_code(String tp_code) {
		this.tp_code = tp_code;
	}
	
}
