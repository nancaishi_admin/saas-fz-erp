package zy.entity.buy.enter;

import java.io.Serializable;

public class T_Buy_Enter implements Serializable{
	private static final long serialVersionUID = -4079789390317387101L;
	private Integer et_id;
	private String et_number;
	private String et_make_date;
	private String et_supply_code;
	private String supply_name;
	private String et_depot_code;
	private String depot_name;
	private String et_maker;
	private String et_manager;
	private String et_handnumber;
	private Integer et_amount;
	private Double et_money;
	private Double et_retailmoney;
	private Double et_discount_money;
	private String et_ba_code;
	private String bank_name;
	private String et_property;
	private String et_remark;
	private Integer et_ar_state;
	private String et_ar_date;
	private String et_ar_name;
	private Integer et_isdraft;
	private Integer et_pay_state;
	private Double et_payable;
	private Double et_payabled;
	private Double et_prepay;
	private String et_order_number;
	private Integer et_type;
	private String et_sysdate;
	private Integer et_us_id;
	private Integer companyid;
	
	private String ar_describe;
	public Integer getEt_id() {
		return et_id;
	}
	public void setEt_id(Integer et_id) {
		this.et_id = et_id;
	}
	public String getEt_number() {
		return et_number;
	}
	public void setEt_number(String et_number) {
		this.et_number = et_number;
	}
	public String getEt_make_date() {
		return et_make_date;
	}
	public void setEt_make_date(String et_make_date) {
		this.et_make_date = et_make_date;
	}
	public String getEt_supply_code() {
		return et_supply_code;
	}
	public void setEt_supply_code(String et_supply_code) {
		this.et_supply_code = et_supply_code;
	}
	public String getSupply_name() {
		return supply_name;
	}
	public void setSupply_name(String supply_name) {
		this.supply_name = supply_name;
	}
	public String getEt_depot_code() {
		return et_depot_code;
	}
	public void setEt_depot_code(String et_depot_code) {
		this.et_depot_code = et_depot_code;
	}
	public String getDepot_name() {
		return depot_name;
	}
	public void setDepot_name(String depot_name) {
		this.depot_name = depot_name;
	}
	public String getEt_maker() {
		return et_maker;
	}
	public void setEt_maker(String et_maker) {
		this.et_maker = et_maker;
	}
	public String getEt_manager() {
		return et_manager;
	}
	public void setEt_manager(String et_manager) {
		this.et_manager = et_manager;
	}
	public String getEt_handnumber() {
		return et_handnumber;
	}
	public void setEt_handnumber(String et_handnumber) {
		this.et_handnumber = et_handnumber;
	}
	public Integer getEt_amount() {
		return et_amount;
	}
	public void setEt_amount(Integer et_amount) {
		this.et_amount = et_amount;
	}
	public Double getEt_money() {
		return et_money;
	}
	public void setEt_money(Double et_money) {
		this.et_money = et_money;
	}
	public Double getEt_retailmoney() {
		return et_retailmoney;
	}
	public void setEt_retailmoney(Double et_retailmoney) {
		this.et_retailmoney = et_retailmoney;
	}
	public Double getEt_discount_money() {
		return et_discount_money;
	}
	public void setEt_discount_money(Double et_discount_money) {
		this.et_discount_money = et_discount_money;
	}
	public String getEt_ba_code() {
		return et_ba_code;
	}
	public void setEt_ba_code(String et_ba_code) {
		this.et_ba_code = et_ba_code;
	}
	public String getBank_name() {
		return bank_name;
	}
	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}
	public String getEt_property() {
		return et_property;
	}
	public void setEt_property(String et_property) {
		this.et_property = et_property;
	}
	public String getEt_remark() {
		return et_remark;
	}
	public void setEt_remark(String et_remark) {
		this.et_remark = et_remark;
	}
	public Integer getEt_ar_state() {
		return et_ar_state;
	}
	public void setEt_ar_state(Integer et_ar_state) {
		this.et_ar_state = et_ar_state;
	}
	public String getEt_ar_date() {
		return et_ar_date;
	}
	public void setEt_ar_date(String et_ar_date) {
		this.et_ar_date = et_ar_date;
	}
	public String getEt_ar_name() {
		return et_ar_name;
	}
	public void setEt_ar_name(String et_ar_name) {
		this.et_ar_name = et_ar_name;
	}
	public Integer getEt_isdraft() {
		return et_isdraft;
	}
	public void setEt_isdraft(Integer et_isdraft) {
		this.et_isdraft = et_isdraft;
	}
	public Integer getEt_pay_state() {
		return et_pay_state;
	}
	public void setEt_pay_state(Integer et_pay_state) {
		this.et_pay_state = et_pay_state;
	}
	public Double getEt_payable() {
		return et_payable;
	}
	public void setEt_payable(Double et_payable) {
		this.et_payable = et_payable;
	}
	public Double getEt_payabled() {
		return et_payabled;
	}
	public void setEt_payabled(Double et_payabled) {
		this.et_payabled = et_payabled;
	}
	public Double getEt_prepay() {
		return et_prepay;
	}
	public void setEt_prepay(Double et_prepay) {
		this.et_prepay = et_prepay;
	}
	public String getEt_order_number() {
		return et_order_number;
	}
	public void setEt_order_number(String et_order_number) {
		this.et_order_number = et_order_number;
	}
	public Integer getEt_type() {
		return et_type;
	}
	public void setEt_type(Integer et_type) {
		this.et_type = et_type;
	}
	public String getEt_sysdate() {
		return et_sysdate;
	}
	public void setEt_sysdate(String et_sysdate) {
		this.et_sysdate = et_sysdate;
	}
	public Integer getEt_us_id() {
		return et_us_id;
	}
	public void setEt_us_id(Integer et_us_id) {
		this.et_us_id = et_us_id;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getAr_describe() {
		return ar_describe;
	}
	public void setAr_describe(String ar_describe) {
		this.ar_describe = ar_describe;
	}
}
