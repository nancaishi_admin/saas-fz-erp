package zy.entity.buy.dealings;

import java.io.Serializable;

public class T_Buy_Dealings implements Serializable{
	private static final long serialVersionUID = -8863070375338623418L;
	private Long dl_id;
	private String dl_supply_code;
	private String supply_name;
	private String dl_number;
	private Integer dl_type;
	private Double dl_discount_money;
	private Double dl_payable;
	private Double dl_payabled;
	private Double dl_debt;
	private String dl_date;
	private String dl_manager;
	private String dl_remark;
	private String dl_sysdate;
	private Integer dl_buyamount;
	private Integer companyid;
	public Long getDl_id() {
		return dl_id;
	}
	public void setDl_id(Long dl_id) {
		this.dl_id = dl_id;
	}
	public String getDl_supply_code() {
		return dl_supply_code;
	}
	public void setDl_supply_code(String dl_supply_code) {
		this.dl_supply_code = dl_supply_code;
	}
	public String getSupply_name() {
		return supply_name;
	}
	public void setSupply_name(String supply_name) {
		this.supply_name = supply_name;
	}
	public String getDl_number() {
		return dl_number;
	}
	public void setDl_number(String dl_number) {
		this.dl_number = dl_number;
	}
	public Integer getDl_type() {
		return dl_type;
	}
	public void setDl_type(Integer dl_type) {
		this.dl_type = dl_type;
	}
	public Double getDl_discount_money() {
		return dl_discount_money;
	}
	public void setDl_discount_money(Double dl_discount_money) {
		this.dl_discount_money = dl_discount_money;
	}
	public Double getDl_payable() {
		return dl_payable;
	}
	public void setDl_payable(Double dl_payable) {
		this.dl_payable = dl_payable;
	}
	public Double getDl_payabled() {
		return dl_payabled;
	}
	public void setDl_payabled(Double dl_payabled) {
		this.dl_payabled = dl_payabled;
	}
	public Double getDl_debt() {
		return dl_debt;
	}
	public void setDl_debt(Double dl_debt) {
		this.dl_debt = dl_debt;
	}
	public String getDl_date() {
		return dl_date;
	}
	public void setDl_date(String dl_date) {
		this.dl_date = dl_date;
	}
	public String getDl_manager() {
		return dl_manager;
	}
	public void setDl_manager(String dl_manager) {
		this.dl_manager = dl_manager;
	}
	public String getDl_remark() {
		return dl_remark;
	}
	public void setDl_remark(String dl_remark) {
		this.dl_remark = dl_remark;
	}
	public String getDl_sysdate() {
		return dl_sysdate;
	}
	public void setDl_sysdate(String dl_sysdate) {
		this.dl_sysdate = dl_sysdate;
	}
	public Integer getDl_buyamount() {
		return dl_buyamount;
	}
	public void setDl_buyamount(Integer dl_buyamount) {
		this.dl_buyamount = dl_buyamount;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
