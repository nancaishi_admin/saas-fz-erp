package com.zy.view.fragment;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.zy.R;
import com.zy.activity.index.LoginActivity;
import com.zy.activity.sys.ModifyPwdActivity;
import com.zy.util.ActivityUtil;
import com.zy.util.Constants;
import com.zy.util.SPUtil;

public class Fragment_My extends Fragment implements OnClickListener{
	private Activity ctx;
	private View layout;
	private TextView tv_username,tv_rolename;
	private TextView icon_modifypwd,icon_checkupdate;
	private Button btn_exit;
	private Typeface iconfont;
	private Map<String,String> keyMap;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (layout == null) {
			ctx = this.getActivity();
			layout = ctx.getLayoutInflater().inflate(R.layout.fragment_my,null);
			initViews();
			initData();
			setOnListener();
		} else {
			ViewGroup parent = (ViewGroup) layout.getParent();
			if (parent != null) {
				parent.removeView(layout);
			}
		}
		return layout;
	}

	private void initViews() {
		tv_username = (TextView)layout.findViewById(R.id.tv_username);
		tv_rolename = (TextView)layout.findViewById(R.id.tv_rolename);
		btn_exit = (Button)layout.findViewById(R.id.btn_exit);
		icon_modifypwd = (TextView)layout.findViewById(R.id.icon_modifypwd);
		icon_checkupdate = (TextView)layout.findViewById(R.id.icon_checkupdate);
		iconfont = Typeface.createFromAsset(ctx.getAssets(), "iconfont/iconfont.ttf");
		icon_modifypwd.setTypeface(iconfont);
		icon_checkupdate.setTypeface(iconfont);
//		T_Sys_User user = SPUtil.getUser(ctx);
//		tv_username.setText("姓名:"+StringUtil.trimString(user.getUs_name()));
//		tv_rolename.setText("角色:"+StringUtil.trimString(user.getRo_name()));
		keyMap = new HashMap<String, String>(3);
	}

	private void setOnListener() {
		layout.findViewById(R.id.view_user).setOnClickListener(this);
		layout.findViewById(R.id.tr_modifypwd).setOnClickListener(this);
		layout.findViewById(R.id.tr_checkupdate).setOnClickListener(this);
		btn_exit.setOnClickListener(this);
	}

	private void initData() {

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.tr_modifypwd:
				keyMap.clear();
				keyMap.put(Constants.TITLE, "修改密码");
				ActivityUtil.startActivity(ctx, ModifyPwdActivity.class, keyMap);
				break;
			case R.id.tr_checkupdate:
//				new UpdateManager(ctx, false).checkUpdate();
				break;
			case R.id.btn_exit:
				doExit();
				break;
			default:
				
			break;
		}
	}
	/**
	 * 当前用户
	 */
	private void doExit(){
		SPUtil.clear(ctx);
		Intent intent = new Intent(ctx, LoginActivity.class);
		ctx.startActivity(intent); 
		ctx.finish();
	}
}
