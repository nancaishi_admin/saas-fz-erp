package com.zy.view.fragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import zy.entity.sys.user.T_Sys_User;
import zy.util.CommonUtil;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager.LayoutParams;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.zy.R;
import com.zy.util.ActivityUtil;
import com.zy.util.CommonParam;
import com.zy.util.StringComparator;
import com.zy.view.dialog.WarnTipDialog;

public class Fragment_Friends extends Fragment implements OnClickListener,OnItemClickListener {
	private Handler handler;
//	private UserAPI userAPI;
//	private FirendsAdapter contactAdapter;
	private Activity context;
	private View layout,layout_head;
	private ListView lvContact;
//	private SideBar indexBar;
	private TextView mDialogText;
	private WindowManager mWindowManager;
	private EditText txt_search;
	private List<T_Sys_User> userList = new ArrayList<T_Sys_User>();
	private WarnTipDialog warnTipDialog;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (layout == null) {
			context = this.getActivity();
			layout = context.getLayoutInflater().inflate(R.layout.fragment_friends,null);
			mWindowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
			initViews();
			initListener();
			initHandler();
			loadData();
		} else {
			ViewGroup parent = (ViewGroup) layout.getParent();
			if (parent != null) {
				parent.removeView(layout);
			}
		}
		return layout;
	}

	private void initViews() {
		lvContact = (ListView) layout.findViewById(R.id.lvContact);
		mDialogText = (TextView) LayoutInflater.from(getActivity()).inflate(
				R.layout.list_position, null);
		mDialogText.setVisibility(View.INVISIBLE);
		mDialogText.setTextSize(40);
//		indexBar = (SideBar) layout.findViewById(R.id.sideBar);
//		indexBar.setListView(lvContact);
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,
				WindowManager.LayoutParams.TYPE_APPLICATION,
				WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
						| WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
				PixelFormat.TRANSLUCENT);
		mWindowManager.addView(mDialogText, lp);
//		indexBar.setTextView(mDialogText);
		layout_head = context.getLayoutInflater().inflate(R.layout.layout_head_friend, null);
		lvContact.addHeaderView(layout_head);
		txt_search = (EditText)layout_head.findViewById(R.id.txt_search);
	}
	
	private void initListener() {
//		userAPI = new UserAPI(context);
//		contactAdapter = new FirendsAdapter(context, userList);
//		lvContact.setAdapter(contactAdapter);
		lvContact.setOnItemClickListener(this);
		layout_head.findViewById(R.id.btn_search).setOnClickListener(this);
	}
	@SuppressLint("HandlerLeak")
	private void initHandler(){
		handler = new Handler(){
			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				switch (msg.what) {
					case CommonParam.STATE_LOAD:
						if(msg.obj == null){
							ActivityUtil.showShortToast(context, "未查询到联系人?");
							return;
						}
						@SuppressWarnings("unchecked")
						List<T_Sys_User> result = (List<T_Sys_User>)msg.obj;
						userList.addAll(result);
						Collections.sort(userList, new StringComparator());
//						contactAdapter.notifyDataSetChanged();
					break;
					case CommonParam.STATE_ERROR:
						ActivityUtil.showShortToast(context, "数据加载错误");
						break;
				}
			}
		};
	}
	
	private void loadData() {
		Map<String, Object> params = new HashMap<String, Object>();
//		params.put(CommonUtil.SEARCHCONTENT, txt_search.getText().toString().trim());
//		userAPI.list(params, handler, CommonParam.STATE_LOAD);
	}
	
	/**
	 * 刷新页面
	 */
	public void refresh() {
		userList.clear();
		loadData();
	}
	
	@Override
	public void onDestroy() {
		mWindowManager.removeView(mDialogText);
		super.onDestroy();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btn_search:
				refresh();
				break;
			default:
				break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		final T_Sys_User user = userList.get(position - 1);
//		if(user.getUsermobile() == null || "".equals(user.getUsermobile())){
//			ActivityUtil.showShortToast(context, user.getUsername() + "的联系电话为空！");
//			return;
//		}
//		if(warnTipDialog == null){
//			warnTipDialog = new WarnTipDialog(context, "是否联系"+user.getUs_name()+"?");
//			warnTipDialog.setBtnOkLinstener(new DialogInterface.OnClickListener() {
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					warnTipDialog.dismiss();
//					Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + user.getUsermobile()));
//					startActivity(intent);
//				}
//			});
//		}
//		warnTipDialog.setText("是否联系"+user.getUs_name()+"?");
//		warnTipDialog.show();
		
	}
}
