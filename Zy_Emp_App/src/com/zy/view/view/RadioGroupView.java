package com.zy.view.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RadioGroup;

public class RadioGroupView extends RadioGroup {
	private final static int SpaceH = 20,SpaceV=15;
    public RadioGroupView(Context context) {
        super(context);
    }

    public RadioGroupView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    	int maxWidth = MeasureSpec.getSize(widthMeasureSpec);
        int childCount = getChildCount();
        int x = 0;
        int y = 0;
        int row = 0;

        for (int index = 0; index < childCount; index++) {
            final View child = getChildAt(index);
            if (child.getVisibility() != View.GONE) {
                child.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
                int width = child.getMeasuredWidth();
                int height = child.getMeasuredHeight();
                x += width+SpaceH;
                y = row * (height+SpaceV) + height;
                if (x > maxWidth) {
                    x = width+SpaceH;
                    row++;
                    y = row * (height+SpaceV) + height;
                }
            }
        }
        setMeasuredDimension(maxWidth, y);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
    	final int childCount = getChildCount();
        int maxWidth = r - l;
        int x = 0;
        int y = 0;
        int row = 0;
        for (int i = 0; i < childCount; i++) {
            final View child = this.getChildAt(i);
            if (child.getVisibility() != View.GONE) {
                int width = child.getMeasuredWidth();
                int height = child.getMeasuredHeight();
                x += width+SpaceH;
                y = row * (height+SpaceV) + height;
                if (x > maxWidth) {
                    x = width+SpaceH;
                    row++;
                    y = row * (height+SpaceV) + height;
                }
                child.layout(x - width, y - height, x, y);
            }
        }
    }
}