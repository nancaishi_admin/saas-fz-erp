package com.zy.view.dialog;

import java.util.Calendar;

import zy.util.DateUtil;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.TimePicker.OnTimeChangedListener;

import com.zy.R;

public class DateTimePickerDialog extends Dialog implements OnDateChangedListener,  OnTimeChangedListener, android.view.View.OnClickListener{
	private DatePicker datePicker;  
    private TimePicker timePicker;  
    private String dateTime;  
    private String initDateTime;  
    private TextView btn_cancel, btn_ok,dialog_title;
    
    public DateTimePickerDialog(Context context,String initDateTime){
    	super(context);
    	requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_dialog_datetime);
		this.initDateTime = initDateTime;
		initViews();
		initListener();
		initDateTime();
		initEvents();
		setCancelable(true);
		setCanceledOnTouchOutside(true);
    }
    
	private void initViews() {
		datePicker = (DatePicker)findViewById(R.id.datepicker);
		timePicker = (TimePicker)findViewById(R.id.timepicker);
		dialog_title = (TextView)findViewById(R.id.dialog_title);
		btn_cancel = (TextView) findViewById(R.id.btn_cancel);
		btn_ok = (TextView) findViewById(R.id.btn_ok);
	}
	private void initListener(){
		btn_cancel.setOnClickListener(this);
    }
	private void initDateTime(){
      Calendar calendar = Calendar.getInstance();  
      if (!(null == initDateTime || "".equals(initDateTime))) {  
          calendar = DateUtil.parseDateString(initDateTime, DateUtil.FORMAT_YEAR_MON_DAY_HOUR_MIN);  
      } else {  
          initDateTime = calendar.get(Calendar.YEAR) + "-"  
                  + calendar.get(Calendar.MONTH) + "-"  
                  + calendar.get(Calendar.DAY_OF_MONTH) + " "  
                  + calendar.get(Calendar.HOUR_OF_DAY) + ":"  
                  + calendar.get(Calendar.MINUTE);  
      }  
      datePicker.init(calendar.get(Calendar.YEAR),  
              calendar.get(Calendar.MONTH),  
              calendar.get(Calendar.DAY_OF_MONTH), this);  
      timePicker.setIs24HourView(true);
      timePicker.setCurrentHour(calendar.get(Calendar.HOUR_OF_DAY));  
      timePicker.setCurrentMinute(calendar.get(Calendar.MINUTE)); 
	}
	private void initEvents() {
		onDateChanged(null, 0, 0, 0);
		timePicker.setOnTimeChangedListener(this);
	}
    
	@Override
	public void onDateChanged(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		Calendar calendar = Calendar.getInstance();  
        calendar.set(datePicker.getYear(), datePicker.getMonth(),  
                datePicker.getDayOfMonth(), timePicker.getCurrentHour(),  
                timePicker.getCurrentMinute());
        dateTime = DateUtil.format(calendar, DateUtil.FORMAT_YEAR_MON_DAY_HOUR_MIN);  
        dialog_title.setText(dateTime);
	}

	@Override
	public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
		onDateChanged(null, 0, 0, 0); 
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btn_cancel:
				if(isShowing()){
					super.dismiss();
				}
			break;
		}
	}

	public TextView getOkButton(){
		return btn_ok;
	}
	public String getDateTime(){
		return dateTime;
	}
}
