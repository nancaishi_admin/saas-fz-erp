package com.zy.view.dialog;

import zy.util.StringUtil;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import com.zy.R;

public class CheckItemResultDialog extends Dialog implements android.view.View.OnClickListener{
	private TextView btn_cancel, btn_ok;
	private EditText et_remark;
	private String remark;
	private Context context;
	
	public CheckItemResultDialog(Context context,String remark) {
		super(context);
		this.context = context;
		this.remark = remark;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_dialog_checkitem_result);
		initViews();
		initListener();
	}
	
	private void initViews() {
		btn_cancel = (TextView) findViewById(R.id.btn_cancel);
		btn_ok = (TextView) findViewById(R.id.btn_ok);
		et_remark = (EditText)findViewById(R.id.et_remark);
		et_remark.setText(StringUtil.trimString(remark));
	}
	private void initListener() {
		btn_cancel.setOnClickListener(this);
	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btn_cancel:
				if(isShowing()){
					super.dismiss();
				}
				break;
		}
	}
	public TextView getOkButton(){
		return btn_ok;
	}
	public String getRemark(){
		return et_remark.getText().toString().trim();
	}
}
