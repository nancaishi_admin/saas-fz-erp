package com.zy.activity.stock;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import zy.entity.stock.data.T_Stock_Data;
import zy.util.CommonUtil;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.zy.R;
import com.zy.activity.BaseActivity;
import com.zy.adapter.stock.StockAdapter;
import com.zy.api.stock.StockAPI;
import com.zy.util.ActivityUtil;
import com.zy.util.CommonParam;
import com.zy.util.Constants;
import com.zy.view.view.AutoListView;
import com.zy.view.view.AutoListView.OnLoadListener;
import com.zy.view.zxing.CaptureActivity;

public class StockActivity extends BaseActivity implements OnLoadListener,OnClickListener{
	private TextView txt_title,tv_barcode;
	private Button btn_search;
	private ImageView img_back;
	private Handler handler;
	private int pageindex = 1;
	private AutoListView autoListView;
	private StockAdapter stockAdapter;
	private StockAPI stockAPI;
	private List<T_Stock_Data> stockList = new ArrayList<T_Stock_Data>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_stock);
		initView();
		initListener();
		initHandle();
		loadData();
	}
	private void initView(){
		txt_title = (TextView)findViewById(R.id.txt_title);
		txt_title.setText(getIntent().getStringExtra(Constants.TITLE));
		tv_barcode = (TextView)findViewById(R.id.tv_barcode);
		tv_barcode.setTypeface(iconfont);
		btn_search = (Button)findViewById(R.id.btn_search);
		img_back = (ImageView) findViewById(R.id.img_back);
		img_back.setVisibility(View.VISIBLE);
		autoListView = (AutoListView)findViewById(R.id.lv_data);
	}
	private void initListener(){
		img_back.setOnClickListener(this);
		tv_barcode.setOnClickListener(this);
		btn_search.setOnClickListener(this);
		stockAdapter = new StockAdapter(this,stockList);
		autoListView.setOnLoadListener(this);
		autoListView.setAdapter(stockAdapter);
		autoListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if (position == (autoListView.getCount() - 1))
					return;
				Bundle bundle = new Bundle();
				bundle.putString(Constants.TITLE, "库存明细");
				bundle.putString("pd_code", stockList.get(position).getSd_pd_code());
				ActivityUtil.start_Activity(StockActivity.this, StockInfoActivity.class, bundle);
			}
		});
	}
	private void initHandle(){
		handler = new Handler() {
			@SuppressWarnings("unchecked")
			public void handleMessage(Message msg) {
				switch (msg.what) {
					case STATE_LOAD:{
						if(msg.obj == null){
							return;
						}
						List<T_Stock_Data> result = (List<T_Stock_Data>) msg.obj;
						autoListView.onLoadComplete();
						stockList.addAll(result);
						if (stockList.size() > 0) {
							autoListView.setResultSize(result.size(), stockList.size());
			                pageindex = stockList.size()/CommonParam.pagesize+1;
			                stockAdapter.notifyDataSetChanged();
						}else {
							autoListView.setResultSize(result.size(), stockList.size());
						}
		                break;
					}
					case R.id.tv_barcode://条码扫描
						if(msg.obj == null){
							return;
						}
						T_Stock_Data stockData = (T_Stock_Data)msg.obj;
						autoListView.onLoadComplete();
						stockList.add(stockData);
						autoListView.setResultSize(1, stockList.size());
						stockAdapter.notifyDataSetChanged();
						break;
					case STATE_ERROR:
						String message = null;
						if(msg.obj != null){
							message = msg.obj.toString();
						}
						ActivityUtil.showShortToast(StockActivity.this, message);
						break;
				}
				super.handleMessage(msg);
			}
		};
	}
	private void flush(){
		pageindex = 1;
		stockList.clear();
		stockAdapter.notifyDataSetChanged();
	}

	private void loadData(){
		Map<String, Object> paramMap = buildBaseParam();
		paramMap.put(CommonUtil.PAGEINDEX, pageindex);
		paramMap.put(CommonUtil.PAGESIZE, CommonUtil.PAGE_SIZE);
		stockAPI = new StockAPI(this);
		stockAPI.list(paramMap, handler, STATE_LOAD);
	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.img_back:
			ActivityUtil.finish(StockActivity.this);
			break;
		case R.id.btn_search:
			flush();
			loadData();
			break;
		case R.id.tv_barcode:
			Intent intent = new Intent(StockActivity.this, CaptureActivity.class);
			startActivityForResult(intent, REQUEST_CODE_SCANNER);
			break;
		default:
			break;
	}
	}
	@Override
	public void onLoad() {
		loadData();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			if(requestCode == REQUEST_CODE_SCANNER){//扫一扫
				String code = data.getStringExtra("code");
				Map<String, Object> paramMap = buildBaseParam();
				paramMap.put("barcode", code);
				flush();
				stockAPI.loadByBarcode(paramMap, handler, R.id.tv_barcode);
			}
		}
	}
	
}
