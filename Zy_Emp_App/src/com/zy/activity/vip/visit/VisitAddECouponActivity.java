package com.zy.activity.vip.visit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import zy.dto.base.emp.EmpLoginDto;
import zy.dto.sell.ecoupon.SellECouponDto;
import zy.entity.sell.ecoupon.T_Sell_Ecoupon_User;
import zy.entity.vip.member.T_Vip_Member;
import zy.entity.vip.visit.T_Vip_Visit;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.zy.R;
import com.zy.activity.BaseActivity;
import com.zy.adapter.vip.visit.ECouponAdapter;
import com.zy.api.sell.SellECouponAPI;
import com.zy.api.vip.VisitAPI;
import com.zy.util.ActivityUtil;
import com.zy.util.CommonParam;
import com.zy.util.Constants;
import com.zy.view.view.AutoListView;
import com.zy.view.view.AutoListView.OnLoadListener;

public class VisitAddECouponActivity extends BaseActivity implements OnLoadListener,OnClickListener{
	private Handler handler;
	private AutoListView autoListView;
	private ECouponAdapter eCouponAdapter;
	private TextView txt_title;
	private ImageView img_back;
	private Button btn_save;
	private int pageindex = 1;
	private List<SellECouponDto> eCouponDtos = new ArrayList<SellECouponDto>();
	private SellECouponAPI sellECouponAPI;
	private VisitAPI visitAPI;
	private T_Vip_Member member;
	private T_Vip_Visit visit = new T_Vip_Visit();
	private T_Sell_Ecoupon_User ecouponUser = new T_Sell_Ecoupon_User();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_vip_visitadd_ecoupon);
		initData();
		initView();
		initListener();
		initHandle();
		loadData();
	}
	private void initData() {
		progress = new ProgressDialog(this);
		sellECouponAPI = new SellECouponAPI(this);
		visitAPI = new VisitAPI(this);
		member = (T_Vip_Member)getIntent().getSerializableExtra("member");
	}
	private void initView(){
		
		autoListView = (AutoListView) findViewById(R.id.lv_data);
		txt_title = (TextView) findViewById(R.id.txt_title);
		txt_title.setText(getIntent().getStringExtra(Constants.TITLE));
		img_back = (ImageView) findViewById(R.id.img_back);
		img_back.setVisibility(View.VISIBLE);
		btn_save = (Button) findViewById(R.id.btn_save);
	}
	
	private void initListener(){
		eCouponAdapter = new ECouponAdapter(VisitAddECouponActivity.this,eCouponDtos);
		autoListView.setOnLoadListener(this);
		autoListView.setAdapter(eCouponAdapter);
		img_back.setOnClickListener(this);
		btn_save.setOnClickListener(this);
	}
	@SuppressLint("HandlerLeak")
	private void initHandle(){
		handler = new Handler() {
			@SuppressWarnings("unchecked")
			public void handleMessage(Message msg) {
				if(progress != null && progress.isShowing()){
					progress.dismiss();
				}
				switch (msg.what) {
					case STATE_LOAD:{
						if(msg.obj == null){
							return;
						}
						List<SellECouponDto> result = (List<SellECouponDto>) msg.obj;
						autoListView.onLoadComplete();
						autoListView.onRefreshComplete();
						eCouponDtos.addAll(result);
						if (eCouponDtos.size() > 0) {
							autoListView.setResultSize(result.size(), eCouponDtos.size());
			                pageindex = eCouponDtos.size()/CommonParam.pagesize+1;
			                eCouponAdapter.notifyDataSetChanged();
						}else {
							autoListView.setResultSize(result.size(), eCouponDtos.size());
						}
		                break;
					}
					case R.id.btn_save:
						ActivityUtil.showShortToast(VisitAddECouponActivity.this, "回访成功");
						setResult(RESULT_OK);
						ActivityUtil.finish(VisitAddECouponActivity.this);
						break;
					case STATE_ERROR:
						String message = null;
						if(msg.obj != null){
							message = msg.obj.toString();
						}
						ActivityUtil.showShortToast(VisitAddECouponActivity.this, message);
						break;
				}
				super.handleMessage(msg);
			}
		};
	}
	private void loadData() {
		Map<String, Object> paramMap = buildBaseParam();
		paramMap.put(CommonUtil.PAGEINDEX, pageindex);
		paramMap.put(CommonUtil.PAGESIZE, CommonUtil.PAGE_SIZE);
		sellECouponAPI.listEcoupon4Push(paramMap, handler, STATE_LOAD);
	}
	
	@Override
	public void onLoad() {
		loadData();
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.img_back:
				ActivityUtil.finish(VisitAddECouponActivity.this);
				break;
			case R.id.btn_save:
				save();
				break;
			default:
				break;
		}
	}
	
	private void buildModel(int selectedIndex){
		SellECouponDto eCouponDto = eCouponDtos.get(selectedIndex);
		EmpLoginDto loginDto = getUser();
		visit.setCompanyid(loginDto.getCompanyid());
		visit.setVi_vm_code(member.getVm_code());
		visit.setVi_vm_name(member.getVm_name());
		visit.setVi_vm_mobile(member.getVm_mobile());
		visit.setVi_consume_date(member.getSh_sysdate());
		visit.setVi_consume_money(member.getSh_sell_money());
		visit.setVi_visit_date(DateUtil.getCurrentTime());
		visit.setVi_manager(loginDto.getEm_name());
		visit.setVi_manager_code(loginDto.getEm_code());
		visit.setVi_content("推送优惠券["+eCouponDto.getEcl_code()+"]");
		visit.setVi_shop_code(loginDto.getEm_shop_code());
		visit.setVi_sysdate(DateUtil.getCurrentTime());
		visit.setVi_type(getIntent().getIntExtra("vi_type", 0));
		visit.setVi_remark("");
		visit.setVi_way(4);//推送优惠券
		ecouponUser.setCompanyid(loginDto.getCompanyid());
		ecouponUser.setEcu_ec_number(eCouponDto.getEc_number());
		ecouponUser.setEcu_code(eCouponDto.getEcl_code());
		ecouponUser.setEcu_date(DateUtil.getCurrentTime());
		ecouponUser.setEcu_name(member.getVm_name());
		ecouponUser.setEcu_tel(member.getVm_mobile());
		ecouponUser.setEcu_vip_code(member.getVm_code());
		ecouponUser.setEcu_money(eCouponDto.getEcl_money());
		ecouponUser.setEcu_limitmoney(eCouponDto.getEcl_limitmoney());
		ecouponUser.setEcu_shop_code(loginDto.getEm_shop_code());
		ecouponUser.setEcu_begindate(eCouponDto.getEc_begindate());
		ecouponUser.setEcu_enddate(eCouponDto.getEc_enddate());
		ecouponUser.setEcu_state(0);
	}
	
	private void save(){
		int selectedIndex = eCouponAdapter.getSelectedIndex();
		if(selectedIndex == -1){
			ActivityUtil.showShortToast(this, "请选择要推送的优惠券");
			return;
		}
		buildModel(selectedIndex);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("visit", JSON.toJSONString(visit));
		params.put("ecouponUser", JSON.toJSONString(ecouponUser));
		progress.show();
		visitAPI.save_visit_ecoupon(params, handler, R.id.btn_save);
	}
	
}
