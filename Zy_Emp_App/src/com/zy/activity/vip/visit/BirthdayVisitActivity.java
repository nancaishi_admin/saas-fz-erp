package com.zy.activity.vip.visit;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import zy.entity.vip.member.T_Vip_Member;
import zy.entity.vip.set.T_Vip_ReturnSetUp;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.zy.R;
import com.zy.activity.BaseActivity;
import com.zy.activity.vip.MemberInfoActivity;
import com.zy.adapter.vip.visit.BirthdayVisitAdapter;
import com.zy.adapter.vip.visit.ReturnSetUpAdapter;
import com.zy.api.vip.VisitAPI;
import com.zy.util.ActivityUtil;
import com.zy.util.CommonParam;
import com.zy.util.Constants;
import com.zy.view.view.AutoListView;
import com.zy.view.view.AutoListView.OnLoadListener;

public class BirthdayVisitActivity extends BaseActivity implements OnLoadListener,OnClickListener{
	private Handler handler;
	private GridView gv_returnSetUp;
	private AutoListView autoListView;
	private BirthdayVisitAdapter birthdayVisitAdapter;
	private ReturnSetUpAdapter returnSetUpAdapter;
	private TextView txt_title;
	private ImageView img_back;
	private int pageindex = 1;
	private List<T_Vip_ReturnSetUp> returnSetUps = new ArrayList<T_Vip_ReturnSetUp>();
	private List<T_Vip_Member> members = new ArrayList<T_Vip_Member>();
	private VisitAPI visitAPI;
	private int selectedIndex = 0;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_vip_birthdayvisit);
		initView();
		initListener();
		initHandle();
		loadData();
	}
	private void initView(){
		visitAPI = new VisitAPI(this);
		gv_returnSetUp = (GridView)findViewById(R.id.gv_returnSetUp);
		autoListView = (AutoListView) findViewById(R.id.lv_data);
		txt_title = (TextView) findViewById(R.id.txt_title);
		txt_title.setText(getIntent().getStringExtra(Constants.TITLE));
		img_back = (ImageView) findViewById(R.id.img_back);
		img_back.setVisibility(View.VISIBLE);
	}
	
	private void initListener(){
		T_Vip_ReturnSetUp returnSetUp = new T_Vip_ReturnSetUp();
		returnSetUp.setRts_name("今天生日");
		returnSetUp.setRts_day(0);
		returnSetUps.add(returnSetUp);
		returnSetUp = new T_Vip_ReturnSetUp();
		returnSetUp.setRts_name("本周生日");
		returnSetUp.setRts_day(1);
		returnSetUps.add(returnSetUp);
		returnSetUp = new T_Vip_ReturnSetUp();
		returnSetUp.setRts_name("本月生日");
		returnSetUp.setRts_day(2);
		returnSetUps.add(returnSetUp);
		returnSetUpAdapter = new ReturnSetUpAdapter(BirthdayVisitActivity.this, returnSetUps);
		gv_returnSetUp.setAdapter(returnSetUpAdapter);
		gv_returnSetUp.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				returnSetUpAdapter.setSelectedIndex(position);
				returnSetUpAdapter.notifyDataSetChanged();
				selectedIndex = position;
				flush();
				loadData();
			}
		});
		birthdayVisitAdapter = new BirthdayVisitAdapter(BirthdayVisitActivity.this,members);
		autoListView.setOnLoadListener(this);
		autoListView.setAdapter(birthdayVisitAdapter);
		autoListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if (position == (autoListView.getCount() - 1))
					return;
				T_Vip_Member member = members.get(position);
				Bundle bundle = new Bundle();
				bundle.putString(Constants.TITLE, "会员资料");
				bundle.putSerializable("member", member);
				ActivityUtil.start_Activity(BirthdayVisitActivity.this, MemberInfoActivity.class, bundle);
			}
		});
		img_back.setOnClickListener(this);
	}
	@SuppressLint("HandlerLeak")
	private void initHandle(){
		handler = new Handler() {
			@SuppressWarnings("unchecked")
			public void handleMessage(Message msg) {
				switch (msg.what) {
					case STATE_LOAD:{
						if(msg.obj == null){
							return;
						}
						List<T_Vip_Member> result = (List<T_Vip_Member>) msg.obj;
						autoListView.onLoadComplete();
						autoListView.onRefreshComplete();
						members.addAll(result);
						if (members.size() > 0) {
							autoListView.setResultSize(result.size(), members.size());
			                pageindex = members.size()/CommonParam.pagesize+1;
			                birthdayVisitAdapter.notifyDataSetChanged();
						}else {
							autoListView.setResultSize(result.size(), members.size());
						}
		                break;
					}
					case STATE_ERROR:
						String message = null;
						if(msg.obj != null){
							message = msg.obj.toString();
						}
						ActivityUtil.showShortToast(BirthdayVisitActivity.this, message);
						break;
				}
				super.handleMessage(msg);
			}
		};
	}
	
	private void loadData() {
		Map<String, Object> paramMap = buildBaseParam();
		paramMap.put(CommonUtil.PAGEINDEX, pageindex);
		paramMap.put(CommonUtil.PAGESIZE, CommonUtil.PAGE_SIZE);
		int type = returnSetUps.get(selectedIndex).getRts_day();
		if(type == 0){//今日
			paramMap.put("begindate", DateUtil.getYearMonthDate());
			paramMap.put("enddate", DateUtil.getYearMonthDate());
		}else if(type == 1){//本周
			paramMap.put("begindate", DateUtil.weekFirstDay(DateUtil.getYearMonthDate()));
			paramMap.put("enddate", DateUtil.weekLastDay(DateUtil.getYearMonthDate()));
		}else if(type == 2){//本月
			paramMap.put("begindate", DateUtil.monthStartDay(DateUtil.getYearMonthDate()));
			paramMap.put("enddate", DateUtil.monthEndDay(DateUtil.getYearMonthDate()));
		}
		visitAPI.listBirthdayVisit(paramMap, handler, STATE_LOAD);
	}
	
	@Override
	public void onLoad() {
		loadData();
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.img_back:
				ActivityUtil.finish(BirthdayVisitActivity.this);
				break;
			default:
				break;
		}
	}
	private void flush(){
		pageindex = 1;
		members.clear();
		birthdayVisitAdapter.notifyDataSetChanged();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode == RESULT_OK){
			if(requestCode == CommonParam.STATE_LOAD){//回访成功
				flush();
				loadData();
			}
		}
	}
}
