package com.zy.activity.vip.visit;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import zy.entity.vip.member.T_Vip_Member;
import zy.entity.vip.set.T_Vip_ReturnSetUp;
import zy.util.CommonUtil;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.zy.R;
import com.zy.activity.BaseActivity;
import com.zy.activity.vip.MemberInfoActivity;
import com.zy.adapter.vip.visit.ReturnSetUpAdapter;
import com.zy.adapter.vip.visit.SellVisitAdapter;
import com.zy.api.vip.VipSetAPI;
import com.zy.api.vip.VisitAPI;
import com.zy.util.ActivityUtil;
import com.zy.util.CommonParam;
import com.zy.util.Constants;
import com.zy.view.view.AutoListView;
import com.zy.view.view.AutoListView.OnLoadListener;

public class SellVisitActivity extends BaseActivity implements OnLoadListener,OnClickListener{
	private Handler handler;
	private GridView gv_returnSetUp;
	private AutoListView autoListView;
	private SellVisitAdapter sellVisitAdapter;
	private ReturnSetUpAdapter returnSetUpAdapter;
	private TextView txt_title;
	private ImageView img_back;
	private int pageindex = 1;
	private List<T_Vip_ReturnSetUp> returnSetUps = new ArrayList<T_Vip_ReturnSetUp>();
	private List<T_Vip_Member> members = new ArrayList<T_Vip_Member>();
	private VipSetAPI vipSetAPI;
	private VisitAPI visitAPI;
	public static final int STATE_LOAD_SETUP = 2;
	private int selectedIndex = -1;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_vip_sellvisit);
		initView();
		initListener();
		initHandle();
		initReturnSetUp();
	}
	private void initView(){
		visitAPI = new VisitAPI(this);
		vipSetAPI = new VipSetAPI(this);
		gv_returnSetUp = (GridView)findViewById(R.id.gv_returnSetUp);
		autoListView = (AutoListView) findViewById(R.id.lv_data);
		txt_title = (TextView) findViewById(R.id.txt_title);
		txt_title.setText(getIntent().getStringExtra(Constants.TITLE));
		img_back = (ImageView) findViewById(R.id.img_back);
		img_back.setVisibility(View.VISIBLE);
	}
	
	private void initListener(){
		returnSetUpAdapter = new ReturnSetUpAdapter(SellVisitActivity.this, returnSetUps);
		gv_returnSetUp.setAdapter(returnSetUpAdapter);
		gv_returnSetUp.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				returnSetUpAdapter.setSelectedIndex(position);
				returnSetUpAdapter.notifyDataSetChanged();
				selectedIndex = position;
				flush();
				loadData();
			}
		});
		
		
		sellVisitAdapter = new SellVisitAdapter(SellVisitActivity.this,members);
		autoListView.setOnLoadListener(this);
		autoListView.setAdapter(sellVisitAdapter);
		autoListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if (position == (autoListView.getCount() - 1))
					return;
				T_Vip_Member member = members.get(position);
				Bundle bundle = new Bundle();
				bundle.putString(Constants.TITLE, "会员资料");
				bundle.putSerializable("member", member);
				ActivityUtil.start_Activity(SellVisitActivity.this, MemberInfoActivity.class, bundle);
			}
		});
		img_back.setOnClickListener(this);
	}
	private void initHandle(){
		handler = new Handler() {
			@SuppressWarnings("unchecked")
			public void handleMessage(Message msg) {
				switch (msg.what) {
					case STATE_LOAD_SETUP:
						List<T_Vip_ReturnSetUp> setUps = (List<T_Vip_ReturnSetUp>)msg.obj;
						returnSetUps.addAll(setUps);
						if(returnSetUps.size() == 4){
							gv_returnSetUp.setNumColumns(4);
						}
						returnSetUpAdapter.notifyDataSetChanged();
						if (returnSetUps.size() > 0) {//默认选中第一项，并查询数据
							selectedIndex = 0;
							loadData();
						}
						break;
					case STATE_LOAD:{
						if(msg.obj == null){
							return;
						}
						List<T_Vip_Member> result = (List<T_Vip_Member>) msg.obj;
						autoListView.onLoadComplete();
						autoListView.onRefreshComplete();
						members.addAll(result);
						if (members.size() > 0) {
							autoListView.setResultSize(result.size(), members.size());
			                pageindex = members.size()/CommonParam.pagesize+1;
			                sellVisitAdapter.notifyDataSetChanged();
						}else {
							autoListView.setResultSize(result.size(), members.size());
						}
		                break;
					}
					case STATE_ERROR:
						String message = null;
						if(msg.obj != null){
							message = msg.obj.toString();
						}
						ActivityUtil.showShortToast(SellVisitActivity.this, message);
						break;
				}
				super.handleMessage(msg);
			}
		};
	}
	
	private void initReturnSetUp() {
		vipSetAPI.loadReturnSetUp(buildBaseParam(), handler, STATE_LOAD_SETUP);
	}
	
	private void loadData() {
		if (returnSetUps.size() == 0 || selectedIndex == -1) {
			return;
		}
		Map<String, Object> paramMap = buildBaseParam();
		paramMap.put(CommonUtil.PAGEINDEX, pageindex);
		paramMap.put(CommonUtil.PAGESIZE, CommonUtil.PAGE_SIZE);
		paramMap.put("setDay", returnSetUps.get(selectedIndex).getRts_day());
		visitAPI.listSellVisit(paramMap, handler, STATE_LOAD);
	}
	
	@Override
	public void onLoad() {
		loadData();
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.img_back:
				ActivityUtil.finish(SellVisitActivity.this);
				break;
			default:
				break;
		}
	}
	private void flush(){
		pageindex = 1;
		members.clear();
		sellVisitAdapter.notifyDataSetChanged();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode == RESULT_OK){
			if(requestCode == CommonParam.STATE_LOAD){//回访成功
				flush();
				loadData();
			}
		}
	}
	
}
