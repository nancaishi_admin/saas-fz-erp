package com.zy.activity.vip.visit;

import zy.entity.vip.member.T_Vip_Member;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.zy.R;
import com.zy.activity.BaseActivity;
import com.zy.adapter.vip.visit.VisitWayGridAdapter;
import com.zy.util.ActivityUtil;
import com.zy.util.Constants;
import com.zy.view.view.HomeGridView;

public class VisitWayActivity extends BaseActivity implements OnClickListener,OnItemClickListener{
	private Handler handler;
	private TextView txt_title,tv_vi_type;
	private ImageView img_back;
	private HomeGridView gridview;
	private T_Vip_Member member;
	private int vi_type;
	
	public static final int REQUEST_CODE_TEL = 0x22;
	public static final int REQUEST_CODE_SMS = 0x23;
	public static final int REQUEST_CODE_ECOUPON = 0x24;
	public static final int REQUEST_CODE_WECHAT = 0x25;
	private Bundle bundle;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_vip_visitway);
		initData();
		initView();
		initListener();
		initHandle();
	}
	private void initData() {
		member = (T_Vip_Member)getIntent().getSerializableExtra("member");
		vi_type = getIntent().getIntExtra("vi_type", 0);
	}
	private void initView(){
		txt_title = (TextView) findViewById(R.id.txt_title);
		tv_vi_type = (TextView) findViewById(R.id.tv_vi_type);
		txt_title.setText(getIntent().getStringExtra(Constants.TITLE));
		if(vi_type == 1){
			tv_vi_type.setText("会员回访");
		}else if(vi_type == 2){
			tv_vi_type.setText("生日回访");
		}else if(vi_type == 3){
			tv_vi_type.setText("消费回访");
		}
		img_back = (ImageView) findViewById(R.id.img_back);
		img_back.setVisibility(View.VISIBLE);
		gridview = (HomeGridView) findViewById(R.id.gridview);
		gridview.setAdapter(new VisitWayGridAdapter(VisitWayActivity.this));
	}
	
	private void initListener(){
		img_back.setOnClickListener(this);
		gridview.setOnItemClickListener(this);
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		switch (position) {
			case 0:
				bundle = new Bundle();
				bundle.putString(Constants.TITLE, "电话回访");
				bundle.putSerializable("member", member);
				bundle.putInt("vi_type", vi_type);
				ActivityUtil.start_ActivityForResult(VisitWayActivity.this, VisitAddTelActivity.class,REQUEST_CODE_TEL, bundle);
				break;
			case 1:
				ActivityUtil.showShortToast(VisitWayActivity.this, "暂未开放");
				break;
			case 2:
				bundle = new Bundle();
				bundle.putString(Constants.TITLE, "推送优惠券");
				bundle.putSerializable("member", member);
				bundle.putInt("vi_type", vi_type);
				ActivityUtil.start_ActivityForResult(VisitWayActivity.this, VisitAddECouponActivity.class,REQUEST_CODE_ECOUPON, bundle);
				break;
			case 3:
				ActivityUtil.showShortToast(VisitWayActivity.this, "暂未开放");
				break;
			default:
				break;
		}
	}
	
	@SuppressLint("HandlerLeak")
	private void initHandle(){
		handler = new Handler() {
			public void handleMessage(Message msg) {
				switch (msg.what) {
					case STATE_ERROR:
						String message = null;
						if(msg.obj != null){
							message = msg.obj.toString();
						}
						ActivityUtil.showShortToast(VisitWayActivity.this, message);
						break;
				}
				super.handleMessage(msg);
			}
		};
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.img_back:
				ActivityUtil.finish(VisitWayActivity.this);
				break;
			default:
				break;
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode == RESULT_OK){
			if(requestCode == REQUEST_CODE_TEL){//电话回访
				setResult(RESULT_OK);
				ActivityUtil.finish(VisitWayActivity.this);
			}
			if(requestCode == REQUEST_CODE_ECOUPON){//推送优惠券
				setResult(RESULT_OK);
				ActivityUtil.finish(VisitWayActivity.this);
			}
		}
	}
	
}
