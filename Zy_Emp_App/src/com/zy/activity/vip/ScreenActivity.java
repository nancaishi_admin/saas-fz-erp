package com.zy.activity.vip;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import zy.dto.base.emp.EmpLoginDto;
import zy.entity.vip.membertype.T_Vip_MemberType;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.zy.R;
import com.zy.activity.BaseActivity;
import com.zy.api.vip.MemberTypeAPI;
import com.zy.util.ActivityUtil;
import com.zy.util.Constants;
import com.zy.view.dialog.DateSectionPickerDialog;
import com.zy.view.dialog.popup.BottomPopup;

public class ScreenActivity extends BaseActivity implements OnClickListener{
	private BottomPopup vmDateBottomPopup,vmLastbuyDateBottomPopup,memberTypeBottomPopup;
	private Handler handler;
	private ImageView img_back;
	private TextView txt_title,txt_right;
	private TextView tv_vm_mt_code,tv_vm_date,tv_vm_lastbuy_date;
	private EditText et_vm_cardcode,et_unconsume_days,et_consume_money_min,et_consume_money_max,et_consume_times_min,et_consume_times_max;
	private List<T_Vip_MemberType> memberTypes = new ArrayList<T_Vip_MemberType>();
	private MemberTypeAPI memberTypeAPI;
	private String vm_mt_code;
	private String[] vm_date;
	private String[] vm_lastbuy_date;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_vip_screen);
		initView();
		initData();
		initListener();
		initHandle();
		initPopupWindow();
	}
	
	private void initView(){
		img_back = (ImageView) findViewById(R.id.img_back);
		img_back.setVisibility(View.VISIBLE);
		txt_title = (TextView)findViewById(R.id.txt_title);
		txt_right = (TextView)findViewById(R.id.txt_right);
		txt_title.setText(getIntent().getStringExtra(Constants.TITLE));
		txt_right.setText("确定");
		
		tv_vm_mt_code = (TextView)findViewById(R.id.tv_vm_mt_code);
		tv_vm_date = (TextView)findViewById(R.id.tv_vm_date);
		tv_vm_lastbuy_date = (TextView)findViewById(R.id.tv_vm_lastbuy_date);
		et_vm_cardcode = (EditText)findViewById(R.id.et_vm_cardcode);
		et_unconsume_days = (EditText)findViewById(R.id.et_unconsume_days);
		et_consume_money_min = (EditText)findViewById(R.id.et_consume_money_min);
		et_consume_money_max = (EditText)findViewById(R.id.et_consume_money_max);
		et_consume_times_min = (EditText)findViewById(R.id.et_consume_times_min);
		et_consume_times_max = (EditText)findViewById(R.id.et_consume_times_max);
		
	}
	private void initData(){
		memberTypeAPI = new MemberTypeAPI(this);
	}
	private void initListener(){
		img_back.setOnClickListener(this);
		txt_right.setOnClickListener(this);
		tv_vm_mt_code.setOnClickListener(this);
		tv_vm_date.setOnClickListener(this);
		tv_vm_lastbuy_date.setOnClickListener(this);
	}
	
	@SuppressLint("HandlerLeak")
	private void initHandle(){
		handler = new Handler(){
			@SuppressWarnings("unchecked")
			@Override
			public void handleMessage(Message msg) {
				if(progress != null && progress.isShowing()){
					progress.dismiss();
				}
				switch (msg.what) {
					case R.id.tv_vm_mt_code:
						List<T_Vip_MemberType> result = (List<T_Vip_MemberType>)msg.obj;
						memberTypes.addAll(result);
						String[] memberTypesOption = new String[memberTypes.size()];
						for (int i = 0; i < memberTypes.size(); i++) {
							memberTypesOption[i] = memberTypes.get(i).getMt_name();
						}
						memberTypeBottomPopup = new BottomPopup(ScreenActivity.this, "会员类别", memberTypesOption);
						memberTypeBottomPopup.setItemClickListener(new BottomPopup.onPopupWindowItemClickListener() {
							@Override
							public void onItemClick(int position) {
								vm_mt_code = memberTypes.get(position).getMt_code();
								tv_vm_mt_code.setText(memberTypes.get(position).getMt_name());
							}
						});
						memberTypeBottomPopup.showView();
						break;
					case STATE_ERROR:
						String message = null;
						if(msg.obj != null){
							message = msg.obj.toString();
						}
						ActivityUtil.showShortToast(ScreenActivity.this, message);
						break;
				}
				super.handleMessage(msg);
			}
		};
	}
	
	private String[] getDateByType(String type){
		String begindate = null;
		String enddate = null;
		if("昨天".equals(type)){
			begindate = DateUtil.getDateAddDays(-1);
			enddate = begindate;
		}else if("今天".equals(type)){
			begindate = DateUtil.getYearMonthDate();
			enddate = begindate;
		}else if("近7天".equals(type)){
			begindate = DateUtil.getDateAddDays(-7);
			enddate = DateUtil.getYearMonthDate();
		}else if("近30天".equals(type)){
			begindate = DateUtil.getDateAddDays(-30);
			enddate = DateUtil.getYearMonthDate();
		}else if("近60天".equals(type)){
			begindate = DateUtil.getDateAddDays(-60);
			enddate = DateUtil.getYearMonthDate();
		}
		return new String[] { begindate, enddate };
	}
	
	private void initPopupWindow() {
		final String[] options = new String[]{"昨天","今天","近7天","近30天","近60天","按时间段选择"};
		vmDateBottomPopup = new BottomPopup(ScreenActivity.this, "开卡日期", options);
		vmDateBottomPopup.setItemClickListener(new BottomPopup.onPopupWindowItemClickListener() {
			@Override
			public void onItemClick(int position) {
				if("按时间段选择".equals(options[position])){
					final DateSectionPickerDialog pickerDialog = new DateSectionPickerDialog(ScreenActivity.this);
					pickerDialog.getOkButton().setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							String[] dates = pickerDialog.getDate();
							if(StringUtil.isEmpty(dates[0]) || StringUtil.isEmpty(dates[1])){
								ActivityUtil.showShortToast(ScreenActivity.this, "请选择开始日期或结束日期");
								return;
							}
							if (DateUtil.getDaysMinusBetween(dates[0], dates[1]) < 0) {
								ActivityUtil.showShortToast(ScreenActivity.this, "开始日期不能大于结束日期");
								return;
							}
							vm_date = dates;
							tv_vm_date.setText(dates[0] + "~" + dates[1]);
							pickerDialog.dismiss();
						}
					});
					pickerDialog.show();
				}else {
					vm_date = getDateByType(options[position]);
					tv_vm_date.setText(options[position]);
				}
			}
		});
		vmLastbuyDateBottomPopup = new BottomPopup(ScreenActivity.this, "消费日期", options);
		vmLastbuyDateBottomPopup.setItemClickListener(new BottomPopup.onPopupWindowItemClickListener() {
			@Override
			public void onItemClick(int position) {
				if("按时间段选择".equals(options[position])){
					final DateSectionPickerDialog pickerDialog = new DateSectionPickerDialog(ScreenActivity.this);
					pickerDialog.getOkButton().setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							String[] dates = pickerDialog.getDate();
							if(StringUtil.isEmpty(dates[0]) || StringUtil.isEmpty(dates[1])){
								ActivityUtil.showShortToast(ScreenActivity.this, "请选择开始日期或结束日期");
								return;
							}
							if (DateUtil.getDaysMinusBetween(dates[0], dates[1]) < 0) {
								ActivityUtil.showShortToast(ScreenActivity.this, "开始日期不能大于结束日期");
								return;
							}
							tv_vm_lastbuy_date.setText(dates[0] + "~" + dates[1]);
							pickerDialog.dismiss();
						}
					});
					pickerDialog.show();
				}else {
					vm_lastbuy_date = getDateByType(options[position]);
					tv_vm_lastbuy_date.setText(options[position]);
				}
			}
		});
	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.tv_vm_mt_code:
				if(memberTypes == null || memberTypes.size() == 0){
					EmpLoginDto loginDto = getUser();
					Map<String, Object> params = new HashMap<String, Object>();
					params.put(CommonUtil.COMPANYID, loginDto.getCompanyid());
					params.put(CommonUtil.SHOP_CODE, loginDto.getShop_upcode());
					memberTypeAPI.list(params, handler, R.id.tv_vm_mt_code);
				}else {
					memberTypeBottomPopup.showView();
				}
				break;
			case R.id.tv_vm_date:
				vmDateBottomPopup.showView();
				break;
			case R.id.tv_vm_lastbuy_date:
				vmLastbuyDateBottomPopup.showView();
				break;
			case R.id.img_back:
				ActivityUtil.finish(ScreenActivity.this);
				break;
			case R.id.txt_right:
				HashMap<String, Object> params = new HashMap<String,Object>();
				params.put("vm_mt_code", StringUtil.trimString(vm_mt_code));
				params.put("vm_cardcode", StringUtil.trimString(et_vm_cardcode.getText()));
				params.put("unconsume_days", StringUtil.trimString(et_unconsume_days.getText()));
				params.put("consume_money_min", StringUtil.trimString(et_consume_money_min.getText()));
				params.put("consume_money_max", StringUtil.trimString(et_consume_money_max.getText()));
				params.put("consume_times_min", StringUtil.trimString(et_consume_times_min.getText()));
				params.put("consume_times_max", StringUtil.trimString(et_consume_times_max.getText()));
				if (vm_date != null && vm_date.length == 2) {
					params.put("begindate", StringUtil.trimString(vm_date[0]));
					params.put("enddate", StringUtil.trimString(vm_date[1]));
				}
				if (vm_lastbuy_date != null && vm_lastbuy_date.length == 2) {
					params.put("lastbuy_begindate", StringUtil.trimString(vm_lastbuy_date[0]));
					params.put("lastbuy_enddate", StringUtil.trimString(vm_lastbuy_date[1]));
				}
				boolean allEmpty = true;
				for (String key : params.keySet()) {
					if(StringUtil.isNotEmpty(params.get(key))){
						allEmpty = false;
					}
				}
				if(allEmpty){
					ActivityUtil.showShortToast(ScreenActivity.this, "请选择筛选条件");
					return;
				}
				Intent intent = new Intent();
				intent.putExtra("params", params);
				setResult(RESULT_OK, intent);
				ActivityUtil.finish(ScreenActivity.this);
				break;
			default:
				break;
		}
	}
}
