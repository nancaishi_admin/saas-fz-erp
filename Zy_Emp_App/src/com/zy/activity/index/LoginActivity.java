package com.zy.activity.index;

import java.util.HashMap;
import java.util.Map;

import zy.dto.base.emp.EmpLoginDto;
import zy.entity.sys.user.T_Sys_User;
import zy.util.MD5;
import zy.util.StringUtil;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.zy.R;
import com.zy.activity.BaseActivity;
import com.zy.api.index.IndexAPI;
import com.zy.util.ActivityUtil;
import com.zy.util.SPUtil;

public class LoginActivity extends BaseActivity{
	private Handler handler;
	private EditText et_code,et_username,et_userpwd;
	private Button btn_login;
	private TextView tv_logo;
	public Typeface iconfont;
	private IndexAPI indexAPI;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_login);
		initView();
		initListener();
		initHandler();
	}
	private void initView(){
		indexAPI = new IndexAPI(this);
		et_code = (EditText)findViewById(R.id.et_code);
		progress = new ProgressDialog(this);
		et_username = (EditText)findViewById(R.id.et_username);
		et_userpwd = (EditText)findViewById(R.id.et_userpwd);
		btn_login = (Button)findViewById(R.id.btn_login);
		tv_logo = (TextView)findViewById(R.id.tv_logo);
		iconfont = Typeface.createFromAsset(this.getAssets(), "iconfont/iconfont.ttf");
		tv_logo.setTypeface(iconfont);
		Intent intent = getIntent();
		String account = intent.getStringExtra("account");
		String co_code = intent.getStringExtra("co_code");
		if(StringUtil.isNotEmpty(account)){
			et_username.setText(account);
		}
		if(StringUtil.isNotEmpty(co_code)){
			et_code.setText(co_code);
		}
	}
	private void initListener(){
		MyClickListener myClickListener = new MyClickListener();
		btn_login.setOnClickListener(myClickListener);
	}
	
	private void initHandler(){
		handler = new Handler() {
			public void handleMessage(Message msg) {
				if(progress != null && progress.isShowing()){
					progress.dismiss();
				}
				switch (msg.what) {
					case STATE_LOAD:
						EmpLoginDto result = (EmpLoginDto) msg.obj;
						if (null != result ) {
							SPUtil.putUser(LoginActivity.this, result);
							ActivityUtil.showShortToast(LoginActivity.this, "��¼�ɹ�");
							ActivityUtil.start_Activity(LoginActivity.this, MainActivity.class);
							LoginActivity.this.finish();
						}
		                break;
					case STATE_ERROR:
						btn_login.setClickable(true);
						String message = (String)msg.obj;
						ActivityUtil.showShortToast(LoginActivity.this, message);
						break;
				}
				super.handleMessage(msg);
			}
		};
	}
	
	class MyClickListener implements OnClickListener{
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_login:
				String co_code = et_code.getText().toString().trim();
				String us_account = et_username.getText().toString().trim();
				String us_pass = et_userpwd.getText().toString().trim();
				if (StringUtil.isEmpty(co_code)) {
					et_code.setFocusable(true);
					ActivityUtil.showShortToast(LoginActivity.this, "请输入商户编号");
					return;
				}
				if (StringUtil.isEmpty(us_account)) {
					et_username.setFocusable(true);
					ActivityUtil.showShortToast(LoginActivity.this, "请输入帐号");
					return;
				}
				if (StringUtil.isEmpty(us_pass)) {
					et_userpwd.setFocusable(true);
					com.zy.util.ActivityUtil.showShortToast(LoginActivity.this, "请输入密码");
					return;
				}
				Map<String,String> paramMap = new HashMap<String, String>(2);
				paramMap.put("co_code", co_code);
				paramMap.put("em_code", us_account);
				paramMap.put("em_pass", MD5.getInstance().getMD5ofStr(us_pass));
				btn_login.setClickable(false);
				progress.show();
				indexAPI.login(paramMap, handler, STATE_LOAD);
				break;
			default:
				break;
			}
		}
	}
}
