package com.zy.activity.index;

import java.util.HashMap;
import java.util.Map;

import zy.dto.base.emp.EmpLoginDto;
import zy.util.StringUtil;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Window;

import com.zy.R;
import com.zy.activity.BaseActivity;
import com.zy.api.index.IndexAPI;
import com.zy.util.ActivityUtil;
import com.zy.util.SPUtil;

public class IndexActivity extends BaseActivity{
	private Handler handler;
	private IndexAPI indexAPI;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_index);
		initNet();//初始化网络
		initHandler();
	}
	private void initNet(){
        // 判断网络是否可用  
        if(!isOpenNetwork()) {  
            AlertDialog.Builder builder = new AlertDialog.Builder(IndexActivity.this);  
            builder.setTitle("没有可用的网络").setMessage("是否对网络进行设置?");  
            builder.setCancelable(false);  
            builder.setPositiveButton("是", new DialogInterface.OnClickListener() {  
                @Override  
                public void onClick(DialogInterface dialog, int which) {  
                    Intent intent = null;  
                    try {
                        @SuppressWarnings("deprecation")
						String sdkVersion = android.os.Build.VERSION.SDK;  
                        if(Integer.valueOf(sdkVersion) > 10) {  
                            intent = new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS);  
                        }else {  
                            intent = new Intent();  
                            ComponentName comp = new ComponentName("com.android.settings", "com.android.settings.WirelessSettings");  
                            intent.setComponent(comp);  
                            intent.setAction("android.intent.action.VIEW");  
                        }  
                        IndexActivity.this.startActivity(intent);  
                    } catch (Exception e) {  
                        e.printStackTrace();  
                    }  
                }  
            }).setNegativeButton("否", new DialogInterface.OnClickListener() {  
                @Override  
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();          
                    finish();  
                }  
            }).show(); 
        }
	}
	
	@SuppressLint("HandlerLeak")
	private void initHandler(){
		handler = new Handler(){
			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				switch (msg.what) {
				case STATE_LOAD:
					EmpLoginDto result = (EmpLoginDto) msg.obj;
					if (null != result ) {
						try {
							SPUtil.putUser(IndexActivity.this, result);
							Thread.sleep(1000);
							ActivityUtil.start_Activity(IndexActivity.this, MainActivity.class);
							IndexActivity.this.finish();
						} catch (InterruptedException e) {
						}
					}
	                break;
				case STATE_ERROR:
					handler.postDelayed(indexThread, 1000);
					break;
				}
			}
		};
		initCheck();
	}
	
	private Thread indexThread = new Thread(new Runnable() {
		@Override
		public void run() {
			if(isOpenNetwork()){
				EmpLoginDto user = getUser();
				String account = "",co_code = "";
				if(user != null && user.getEm_id() != null && !user.getEm_id().equals(0)){
					account = user.getEm_code();
					co_code = user.getCo_code();
				}
				Intent intent = new Intent(IndexActivity.this, LoginActivity.class);
				intent.putExtra("account", account);
				intent.putExtra("co_code", co_code);
				IndexActivity.this.startActivity(intent); 
				IndexActivity.this.finish();
			}
		}
	});
	private void initCheck() {
		indexAPI = new IndexAPI(this);
		EmpLoginDto user = getUser();
		if(user != null && user.getEm_id() != null && !user.getEm_id().equals(0)){
			Map<String,String> paramMap = new HashMap<String, String>(2);
			paramMap.put("co_code", user.getCo_code());
			paramMap.put("em_code", user.getEm_code());
			paramMap.put("em_pass", StringUtil.trimString(user.getEm_pass()));
			indexAPI.login(paramMap, handler, STATE_LOAD);
		}else{
			handler.postDelayed(indexThread, 1000);
		}
	}
	 /** 
     * 对网络连接状态进行判断 
     * @return  true, 可用； false， 不可用 
     */  
    private boolean isOpenNetwork() {  
        ConnectivityManager connManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);  
        if(connManager.getActiveNetworkInfo() != null) {  
            return connManager.getActiveNetworkInfo().isAvailable();  
        }  
        return false;  
    }  
}
