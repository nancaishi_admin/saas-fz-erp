package com.zy.activity.index;

import java.util.Timer;
import java.util.TimerTask;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.zy.R;
import com.zy.util.ActivityUtil;
import com.zy.util.CommonParam;
import com.zy.view.fragment.Fragment_Friends;
import com.zy.view.fragment.Fragment_Home;
import com.zy.view.fragment.Fragment_My;
import com.zy.view.fragment.Fragment_Warn;

public class MainActivity extends FragmentActivity implements OnClickListener{
	private Handler handler;
//	private ConfigAPI configAPI;
	private TextView txt_title;
	private ImageView img_right;
	private Fragment[] fragments;
	private Fragment_Home homefragment;
	private Fragment_Friends friendsFragment;
	private Fragment_Warn warnfragment;
	private Fragment_My myfragment;
	private ImageView[] imagebuttons;
	private TextView[] textviews;
	private TextView unread_warn_number;
	private int index;
	private int currentTabIndex;// 当前fragment的index
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		findViewById();
		initViews();
		initTabView();
		setOnListener();
		initHandler();
		initConfig();
	}
	
	private void initHandler(){
		handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				switch (msg.what) {
					case CommonParam.STATE_LOAD:
//						List<T_Sys_Config> sysConfigs = (List<T_Sys_Config>)msg.obj;
//						AllApplication.getInstance().setSys_configs(sysConfigs);
//						checkUpdate();//检查更新
//						warnfragment.initConfigAndLoadWarnCount();//查询预警数量
						warnfragment.refresh();
						break;
					default:
						break;
				}
			}
		};
	}
	private void initConfig(){
		Message message = Message.obtain(handler);
		message.what = CommonParam.STATE_LOAD;
		handler.sendMessage(message);
	}
	
	private void checkUpdate(){
//		new UpdateManager(this, true).checkUpdate();
	}
	private void findViewById() {
		txt_title = (TextView) findViewById(R.id.txt_title);
		img_right = (ImageView) findViewById(R.id.img_right);
		unread_warn_number = (TextView)findViewById(R.id.unread_warn_number);
	}
	private void initViews() {
		// 设置消息页面为初始页面
	}
	private void initTabView() {
		homefragment = new Fragment_Home();
		friendsFragment = new Fragment_Friends();
		warnfragment = new Fragment_Warn();
		myfragment = new Fragment_My();
		fragments = new Fragment[] { homefragment, friendsFragment,
				warnfragment, myfragment };
		imagebuttons = new ImageView[4];
		imagebuttons[0] = (ImageView) findViewById(R.id.img_home);
		imagebuttons[1] = (ImageView) findViewById(R.id.img_friends);
		imagebuttons[2] = (ImageView) findViewById(R.id.img_warn);
		imagebuttons[3] = (ImageView) findViewById(R.id.img_my);

		imagebuttons[0].setSelected(true);
		textviews = new TextView[4];
		textviews[0] = (TextView) findViewById(R.id.tv_home);
		textviews[1] = (TextView) findViewById(R.id.tv_friends_list);
		textviews[2] = (TextView) findViewById(R.id.tv_warn);
		textviews[3] = (TextView) findViewById(R.id.tv_my);
		textviews[0].setTextColor(0xFF45C01A);
		// 添加显示第一个fragment
		getSupportFragmentManager().beginTransaction()
				.add(R.id.fragment_container, homefragment)
				.add(R.id.fragment_container, friendsFragment)
				.add(R.id.fragment_container, warnfragment)
				.add(R.id.fragment_container, myfragment)
				.hide(friendsFragment).hide(myfragment)
				.hide(warnfragment).show(homefragment).commit();
	}
	private void setOnListener() {
		img_right.setOnClickListener(this);

	}
	private int keyBackClickCount = 0;

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			switch (keyBackClickCount++) {
			case 0:
				ActivityUtil.showShortToast(MainActivity.this, "再次按返回键退出");
				Timer timer = new Timer();
				timer.schedule(new TimerTask() {
					@Override
					public void run() {
						keyBackClickCount = 0;
					}
				}, 3000);
				break;
			case 1:
				finish();
				overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
				break;
			}
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.img_right:
			if (index == 0) {
			}
			break;
		default:
			break;
		}
	}
	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	public void onTabClicked(View view) {
		img_right.setVisibility(View.GONE);
		switch (view.getId()) {
		case R.id.re_home:
			index = 0;
			txt_title.setText(R.string.app_name);
			break;
		case R.id.re_friends:
			index = 1;
			txt_title.setText(R.string.friends);
			break;
		case R.id.re_warn:
			index = 2;
			if (homefragment != null) {
				warnfragment.refresh();
			}
			txt_title.setText(R.string.warninfo);
			break;
		case R.id.re_my:
			index = 3;
			txt_title.setText(R.string.me);
			break;
		}
		if (currentTabIndex != index) {
			FragmentTransaction trx = getSupportFragmentManager()
					.beginTransaction();
			trx.hide(fragments[currentTabIndex]);
			if (!fragments[index].isAdded()) {
				trx.add(R.id.fragment_container, fragments[index]);
			}
			trx.show(fragments[index]).commit();
		}
		imagebuttons[currentTabIndex].setSelected(false);
		// 把当前tab设为选中状态
		imagebuttons[index].setSelected(true);
		textviews[currentTabIndex].setTextColor(0xFF999999);
		textviews[index].setTextColor(0xFF45C01A);
		currentTabIndex = index;
	}
	
	public void showWarn(){
		unread_warn_number.setVisibility(View.VISIBLE);
	}
}
