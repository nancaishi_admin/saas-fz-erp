package com.zy.activity.util;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.LinearLayout.LayoutParams;

import com.zy.R;
import com.polites.android.GestureImageView;
import com.zy.activity.AllApplication;
import com.zy.activity.BaseActivity;
import com.zy.util.ActivityUtil;
import com.zy.util.FileUtil;


public class ShowImageActivity extends BaseActivity{
private GestureImageView imageView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_empty);
		initViews();
		initListener();
	}
	
	private void initViews(){
		WindowManager manager = getWindowManager();
		int width = manager.getDefaultDisplay().getWidth();
		int height = manager.getDefaultDisplay().getHeight();
		imageView = new GestureImageView(this);
		LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
		imageView.setLayoutParams(params);
		Bitmap bitmap = AllApplication.getInstance().getImage();
		String imagePath = getIntent().getStringExtra("imagePath");
		if(bitmap != null){
			imageView.setImageBitmap(bitmap);
		}else if(imagePath != null){
			imageView.setImageBitmap(FileUtil.loadBitmap(imagePath,width,height));
		}
		ViewGroup layout = (ViewGroup) findViewById(R.id.layout);
		layout.addView(imageView);
	}
	private void initListener() {
		imageView.setClickable(true);
		imageView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ActivityUtil.finish(ShowImageActivity.this);
			}
		});
	}
	
	@Override
	public void finish() {
		super.finish();
		AllApplication.getInstance().setImage(null);
	}
	@Override
	protected void onDestroy() {
		super.onDestroy();
		AllApplication.getInstance().setImage(null);
	}
}
