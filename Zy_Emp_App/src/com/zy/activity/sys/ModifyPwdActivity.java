package com.zy.activity.sys;

import java.util.HashMap;
import java.util.Map;

import zy.dto.base.emp.EmpLoginDto;
import zy.util.MD5;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.zy.R;
import com.zy.activity.BaseActivity;
import com.zy.api.base.EmpAPI;
import com.zy.util.ActivityUtil;
import com.zy.util.Constants;
import com.zy.util.SPUtil;

public class ModifyPwdActivity extends BaseActivity implements OnClickListener{
	private Handler handler;
	private EmpAPI empAPI;
	private TextView txt_title;
	private ImageView img_back;
	private EditText et_oldpwd,et_newpwd,et_newpwd2;
	private Button btn_submit;
	private EmpLoginDto user;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_sys_modifypwd);
		init();
		initView();
		initListener();
		handler = new Handler() {
			public void handleMessage(Message msg) {
				switch (msg.what) {
					case STATE_UPDATE:
						Toast.makeText(ModifyPwdActivity.this, "�����޸ĳɹ�", Toast.LENGTH_LONG).show();
						user.setEm_pass(MD5.encryptMd5(et_newpwd.getText().toString().trim()));
						SPUtil.putUser(ModifyPwdActivity.this, user);
						ActivityUtil.finish(ModifyPwdActivity.this);
		                break;
					case STATE_ERROR:
						String message = msg.obj.toString();
						ActivityUtil.showShortToast(ModifyPwdActivity.this, message);
						break;
				}
				super.handleMessage(msg);
			}
		};
	}
	private void init() {
		empAPI = new EmpAPI(this);
		user = getUser();
	}
	private void initView(){
		img_back = (ImageView) findViewById(R.id.img_back);
		img_back.setVisibility(View.VISIBLE);
		txt_title = (TextView)findViewById(R.id.txt_title);
		txt_title.setText(getIntent().getStringExtra(Constants.TITLE));
		et_oldpwd = (EditText)findViewById(R.id.et_oldpwd);
		et_newpwd = (EditText)findViewById(R.id.et_newpwd);
		et_newpwd2 = (EditText)findViewById(R.id.et_newpwd2);
		btn_submit = (Button)findViewById(R.id.btn_submit);
	}
	private void initListener(){
		img_back.setOnClickListener(this);
		btn_submit.setOnClickListener(this);
	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.img_back:
				ActivityUtil.finish(ModifyPwdActivity.this);
				break;
			case R.id.btn_submit:
				doModifyPwd();
				break;
			default:
				break;
		}
	}
	private void doModifyPwd(){
		String oldpwd = et_oldpwd.getText().toString().trim();
		String newpwd = et_newpwd.getText().toString().trim();
		String newpwd2 = et_newpwd2.getText().toString().trim();
		if (null == oldpwd || "".equals(oldpwd)) {
			et_oldpwd.setFocusable(true);
			ActivityUtil.showShortToast(ModifyPwdActivity.this, "�����������");
			return;
		}
		if (null == newpwd || "".equals(newpwd)) {
			et_newpwd.setFocusable(true);
			ActivityUtil.showShortToast(ModifyPwdActivity.this, "������������");
			return;
		}
		if (null == newpwd2 || "".equals(newpwd2)) {
			et_oldpwd.setFocusable(true);
			ActivityUtil.showShortToast(ModifyPwdActivity.this, "���ٴ�����ȷ��");
			return;
		}
		if(!newpwd2.equals(newpwd)){
			et_newpwd.setFocusable(true);
			ActivityUtil.showShortToast(ModifyPwdActivity.this, "�������벻һ��");
			return;
		}
		Map<String,Object> paramMap = new HashMap<String, Object>(3);
		paramMap.put("em_id", user.getEm_id());
		paramMap.put("old_pass", oldpwd);
		paramMap.put("new_pass", newpwd);
		empAPI.updatePwd(paramMap, handler, STATE_UPDATE);
	}
}
