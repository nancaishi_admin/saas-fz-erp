package com.zy.activity.emp;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.message.BasicNameValuePair;

import zy.dto.base.emp.EmpPerformanceDto;
import zy.util.DateUtil;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.zy.R;
import com.zy.activity.BaseActivity;
import com.zy.adapter.emp.PerformanceAdapter;
import com.zy.api.base.EmpAPI;
import com.zy.util.ActivityUtil;
import com.zy.util.Constants;

public class PerformanceActivity extends BaseActivity implements OnClickListener{
	private Handler handler;
	private ListView listView;
	private PerformanceAdapter performanceAdapter;
	private List<EmpPerformanceDto> performanceDtos = new ArrayList<EmpPerformanceDto>();
	private TextView txt_title;
	private ImageView img_back;
	private EmpAPI empAPI;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_emp_performance);
		initView();
		initListener();
		initHandle();
		loadData();
	}
	private void initView(){
		listView = (ListView) findViewById(R.id.lv_data);
		txt_title = (TextView) findViewById(R.id.txt_title);
		txt_title.setText(getIntent().getStringExtra(Constants.TITLE));
		img_back = (ImageView) findViewById(R.id.img_back);
		img_back.setVisibility(View.VISIBLE);
	}
	
	private void initListener(){
		empAPI = new EmpAPI(this);
		performanceAdapter = new PerformanceAdapter(PerformanceActivity.this,performanceDtos);
		listView.setAdapter(performanceAdapter);
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if (position == 0){
					ActivityUtil.start_Activity(PerformanceActivity.this, SortActivity.class, new BasicNameValuePair(Constants.TITLE, "��������"));
				}
			}
		});
		img_back.setOnClickListener(this);
	}
	private void initHandle(){
		handler = new Handler() {
			@SuppressWarnings("unchecked")
			public void handleMessage(Message msg) {
				switch (msg.what) {
					case STATE_LOAD:
						if(msg.obj == null){
							return;
						}
						List<EmpPerformanceDto> result = (List<EmpPerformanceDto>) msg.obj;
						if(result != null && result.size()>0){
							performanceDtos.addAll(result);
						}
						performanceAdapter.notifyDataSetChanged();
		                break;
					case STATE_ERROR:
						String message = null;
						if(msg.obj != null){
							message = msg.obj.toString();
						}
						ActivityUtil.showShortToast(PerformanceActivity.this, message);
						break;
				}
				super.handleMessage(msg);
			}
		};
	}
	private void loadData() {
		Map<String, Object> params = buildBaseParam();
		params.put("begindate", DateUtil.monthStartDay(DateUtil.getYearMonthDate()));
		params.put("enddate", DateUtil.monthEndDay(DateUtil.getYearMonthDate())+" 23:59:59");
		params.put("em_code", getUser().getEm_code());
		empAPI.loadPerformanceByEmp(params, handler, STATE_LOAD);
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.img_back:
				ActivityUtil.finish(PerformanceActivity.this);
				break;
			default:
				break;
		}
	}

}
