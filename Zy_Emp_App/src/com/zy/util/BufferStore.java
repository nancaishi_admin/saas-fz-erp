package com.zy.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.os.Environment;

/** 
 * 用于做本地缓存，T需要覆盖equals()方法和hashCode()方法 
 */  
public class BufferStore<T extends Serializable>{
	private final String mBuffPath;
	private final String mFileName;//文件名
    /** 
     * @param buffPath 存放缓存的路径 
     * */  
    public BufferStore(String buffPath,String fileName) {
		mBuffPath = Environment.getExternalStorageDirectory() + File.separator+ "zy_emp" + File.separator + buffPath;
		mFileName = mBuffPath +File.separator+fileName;
    }  
  
    /** 
     * @param list 
     * 向本地写入的缓存数据 
     * */  
    public synchronized void write(List<T> list) {  
        put(list);  
    }  
  
    /** 
     * 读取缓存数据 
     *  
     * @return 缓存数据，数据为空时返回长度为0的list
     * */  
    public synchronized List<T> read() {  
        return get();  
    }  
    /** 
     * 向本地写入数据 
     * */  
    private void put(List<T> list) {  
    	FileOutputStream fos = null; 
    	 ObjectOutputStream oos = null;
        try { 
            FileUtil.createFile(mBuffPath);
            File fileUser = new File(mFileName);
            fileUser.createNewFile();
            // 打开文件   
            fos = new FileOutputStream(mFileName);  
            // 将数据写入文件
            oos = new ObjectOutputStream(fos);  
            oos.writeObject(list);  
            // 释放资源    
            oos.close();  
            fos.close();  
        } catch (FileNotFoundException e) {  
            e.printStackTrace();  
        } catch (IOException e) {  
            e.printStackTrace();  
        } finally{
        	try {
          	  // 释放资源   
        		if(oos != null)oos.close();
        		if(fos != null)fos.close();
  			} catch (IOException e) {
  			}
        }
    }  
    public void clear() {  
        try { 
        	File file = new File(mBuffPath);  
        	deleteFile(file);
        } catch (Exception e) {  
            e.printStackTrace();  
        }
    }  
    private void deleteFile(File file) {
		if (file.exists()) { // 判断文件是否存在
			if (file.isFile()) { // 判断是否是文件
				file.delete(); // delete()方法 你应该知道 是删除的意思;
			} else if (file.isDirectory()) { // 否则如果它是一个目录
				File files[] = file.listFiles(); // 声明目录下所有的文件 files[];
				for (int i = 0; i < files.length; i++) { // 遍历目录下所有的文件
					this.deleteFile(files[i]); // 把每个文件 用这个方法进行迭代
				}
			}
			file.delete();
		}
	}
    /** 
     * 从本地读取数据 
     * */  
    @SuppressWarnings("unchecked")  
    private List<T> get() {  
        List<T> list = new ArrayList<T>();  
        ObjectInputStream ois = null;
        FileInputStream fis = null;
        try {  
            File file = new File(mFileName);  
            if (!file.exists()) {  
                return list;  
            }  
            fis = new FileInputStream(mFileName);  
            ois = new ObjectInputStream(fis);  
            list = (List<T>) ois.readObject();  
            ois.close();  
            fis.close();  
        } catch (FileNotFoundException e) {  
            e.printStackTrace();  
        } catch (IOException e) {  
            e.printStackTrace();  
        } catch (ClassNotFoundException e) {  
            e.printStackTrace();  
        } finally{
        	try {
		        if(ois != null)ois.close();  
		        if(fis != null)fis.close();
			} catch (IOException e) {
			}
        }  
        return list;  
    } 
}
