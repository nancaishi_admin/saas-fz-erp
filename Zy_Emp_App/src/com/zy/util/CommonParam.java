package com.zy.util;

public class CommonParam {
	public final static String SIGN = "/";
	public final static String AND = "&";
//	private final static String IP = "192.168.3.19";
	private final static String IP = "server.zhjr100.com";
	private final static String PORT = "80";
	private final static String PROJECT = "";//Zy_Server
	public final static String IMG_SUFFIX = ".png";
			
	public final static String http_url = "http://"+IP+":"+PORT+SIGN+PROJECT+SIGN;
	public final static String file_url = "http://"+IP+":"+PORT+SIGN;
	
	public final static String PATH_MEMBER_IMG = "/zy_emp/member/";
	
	public static final int STATE_LOAD = 1;
	public static final int STATE_DELETE = 2;
	public static final int STATE_SAVE = 3;
	public static final int STATE_ERROR = 10;
	
	public static final int pagesize = 10;
}
