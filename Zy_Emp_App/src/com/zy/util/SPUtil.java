package com.zy.util;

import zy.dto.base.emp.EmpLoginDto;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SPUtil {
	public static final String FILE_NAME = "nc_user";
	
	public static EmpLoginDto getUser(Context context){
		SharedPreferences sp = context.getSharedPreferences(FILE_NAME, Activity.MODE_PRIVATE);
		EmpLoginDto user = null;
		try {
			user = new EmpLoginDto();
			user.setCompanyid(sp.getInt("companyid", 0));
			user.setEm_id(sp.getInt("em_id", 0));
			user.setEm_code(sp.getString("em_code", null));
			user.setEm_name(sp.getString("em_name", null));
			user.setEm_pass(sp.getString("em_pass", null));
			user.setCo_code(sp.getString("co_code", null));
			user.setEm_shop_code(sp.getString("em_shop_code", null));
			user.setEm_shop_name(sp.getString("em_shop_name", null));
			user.setShoptype(sp.getInt("shoptype", 0));
			user.setShop_upcode(sp.getString("shop_upcode", null));
			user.setShop_uptype(sp.getInt("shop_uptype", 0));
		} catch (Exception e) {
		}
		return user;
	}
	public static void putUser(Context context,EmpLoginDto user){
		SharedPreferences sp = context.getSharedPreferences(FILE_NAME, Activity.MODE_PRIVATE);
		Editor sharedata = sp.edit();
		sharedata.putInt("em_id", user.getEm_id());
        sharedata.putString("em_code", user.getEm_code());
        sharedata.putString("em_name", user.getEm_name());
        sharedata.putString("em_pass", user.getEm_pass());
        sharedata.putString("co_code", user.getCo_code());
        sharedata.putString("em_shop_code", user.getEm_shop_code());
        sharedata.putString("em_shop_name", user.getEm_shop_name());
        sharedata.putInt("companyid", user.getCompanyid());
        sharedata.putInt("shoptype", user.getShoptype());
        sharedata.putInt("shop_uptype", user.getShop_uptype());
        sharedata.putString("shop_upcode", user.getShop_upcode());
        sharedata.commit();
	}
	public static void clear(Context context){
		SharedPreferences sp = context.getSharedPreferences(FILE_NAME, Activity.MODE_PRIVATE);
		Editor sharedata = sp.edit();
		sharedata.clear();
		sharedata.commit();
	}
}
