package com.zy.util;

public interface Constants {
	String NAME = "name";
	String TITLE = "title";
	String TYPE = "type";
	String ID = "id";
	String IMAGE = "image";
	String CHOOSE = "choose";//选择
	String STALL_COMPANY = "stall_company";
	String CHECK = "check";
	String CHECK_DATA = "check_data";
	
	/**
	 * 检查项评定结果：0-通过，1-未通过
	 */
	public static Integer CHECK_ITEM_RESULT_PASS = 0;
	public static Integer CHECK_ITEM_RESULT_NOTPASS = 1;
}
