package com.zy.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;

import zy.util.StringUtil;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;

public class MultipartRequest extends Request<String>{
private MultipartEntity entity = new MultipartEntity();
	
	private final Response.Listener<String> mListener; 
	
	public MultipartRequest(String url,Response.Listener<String> listener,Response.ErrorListener errorListener){
		super(Method.POST, url,errorListener);
		mListener = listener;
	}
	
	public void addStringPart(String key,String value){
		try {
			entity.addPart(key, new StringBody(value, Charset.forName("UTF-8")));
		} catch (UnsupportedEncodingException e) {
			VolleyLog.e("UnsupportedEncodingException");
		}
	}
	public void addStringPart(Map<String, String> params){
		if (params != null && !params.isEmpty()) {
			try {
				for (Map.Entry<String, String> entry : params.entrySet()) {
					entity.addPart(entry.getKey(),new StringBody(entry.getValue(), Charset.forName("UTF-8")));
				}
			} catch (UnsupportedEncodingException e) {
				VolleyLog.e("UnsupportedEncodingException");
			}
		}
	}
	public void addStringPart4Object(Map<String, Object> params){
		if (params != null && !params.isEmpty()) {
			try {
				for (Map.Entry<String, Object> entry : params.entrySet()) {
					entity.addPart(entry.getKey(),new StringBody(StringUtil.trimString(entry.getValue()), Charset.forName("UTF-8")));
				}
			} catch (UnsupportedEncodingException e) {
				VolleyLog.e("UnsupportedEncodingException");
			}
		}
	}
	public void addFilePart(String filePartName,File file){
		if(file != null && file.exists()){
			entity.addPart(filePartName, new FileBody(file));
		}
	}
	public void addFilePart(String filePartName,List<File> files){
		if (files != null && files.size() > 0) {
			for(File file :files){
				if(file.exists()){
					entity.addPart(filePartName,new FileBody(file));
				}
			}
		}
	}
	
	@Override
	public String getBodyContentType() {
		return entity.getContentType().getValue();
	}
	
	@Override
	public byte[] getBody() throws AuthFailureError {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			// 将mMultiPartEntity中的参数写入到bos中
			entity.writeTo(bos);
		} catch (IOException e) {
			Log.e("error", "IOException writing to ByteArrayOutputStream");
		}
        return bos.toByteArray();
	}
	
	@Override
	protected void deliverResponse(String response) {
		mListener.onResponse(response);
	}

	@Override
	protected Response<String> parseNetworkResponse(NetworkResponse response) {
		String parsed;
		try {
			parsed = new String(response.data,HttpHeaderParser.parseCharset(response.headers));
		} catch (UnsupportedEncodingException e) {
			parsed = new String(response.data);
		}
		return Response.success(parsed,HttpHeaderParser.parseCacheHeaders(response));
	}
}
