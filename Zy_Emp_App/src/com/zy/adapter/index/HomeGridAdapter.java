package com.zy.adapter.index;

import com.zy.R;
import com.zy.view.ViewHolder;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class HomeGridAdapter extends BaseAdapter{
	private Context mContext;
	private Typeface iconfont;
	public String[] img_text = {
			"接待顾客","会员查询","会员发卡","库存查询",
			"销售回访","会员生日","节日祝福","促销查询"
			
			};
	public int[] imgs = { 
			R.string.icon_font_receive, R.string.icon_font_vip, R.string.icon_font_add2,
			R.string.icon_font_data,R.string.icon_font_return,R.string.icon_font_vip_day,
			R.string.icon_font_day,R.string.icon_font_sale
	};
	public int[] colors = {
			R.color.icon_blue,R.color.icon_gray,R.color.icon_green,R.color.icon_orange,
			R.color.icon_red,R.color.icon_yellow,R.color.icon_zise,R.color.icon_red,
			R.color.icon_blue,R.color.icon_green
	};
	public HomeGridAdapter(Context mContext) {
		super();
		this.mContext = mContext;
		iconfont = Typeface.createFromAsset(mContext.getAssets(), "iconfont/iconfont.ttf");
	}

	@Override
	public int getCount() {
		return img_text.length;
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(
					R.layout.home_grid_item, parent, false);
			TextView tv = ViewHolder.get(convertView, R.id.tv_item);
			TextView iv = ViewHolder.get(convertView, R.id.iv_item);
			iv.setTypeface(iconfont);
			iv.setText(imgs[position]);
			iv.setTextColor(mContext.getResources().getColor(colors[position]));
			tv.setText(img_text[position]);
		}
		return convertView;
	}
}
