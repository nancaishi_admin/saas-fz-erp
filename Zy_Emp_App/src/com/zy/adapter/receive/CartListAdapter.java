package com.zy.adapter.receive;

import java.util.ArrayList;
import java.util.List;

import zy.entity.sell.cart.T_Sell_CartList;
import zy.util.StringUtil;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.android.volley.toolbox.ImageLoader.ImageListener;
import com.zy.R;
import com.zy.activity.receive.ReceiveActivity;
import com.zy.util.ActivityUtil;
import com.zy.util.BitmapCache;
import com.zy.util.CommonParam;
import com.zy.util.ImageUtil;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CartListAdapter extends BaseAdapter{
	private Context context;
	private List<T_Sell_CartList> cartLists = new ArrayList<T_Sell_CartList>();
	private LayoutInflater listContainer; 
	private ImageLoader imageLoader;
	public CartListAdapter(Context context,List<T_Sell_CartList> cartLists) {
		this.context = context;
		this.cartLists = cartLists;
		listContainer = LayoutInflater.from(context);
		imageLoader = new ImageLoader(Volley.newRequestQueue(context), new BitmapCache());
	}
	
	@Override
	public int getCount() {
		return cartLists.size();
	}

	@Override
	public Object getItem(int position) {
		return cartLists.get(position);
	}

	@Override
	public long getItemId(int position) {
		return cartLists.get(position).getScl_id();
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		
		// 自定义视图
		final ListItemView listItemView;
		if (convertView == null) {
			listItemView = new ListItemView();
			convertView = listContainer.inflate(R.layout.layout_receive_item,null);
			listItemView.iv_product_img = (ImageView) convertView.findViewById(R.id.iv_product_img);
			listItemView.tv_pd_name = (TextView) convertView.findViewById(R.id.tv_pd_name);
			listItemView.tv_product_info = (TextView) convertView.findViewById(R.id.tv_product_info);
			listItemView.tv_sell_price = (TextView) convertView.findViewById(R.id.tv_sell_price);
			listItemView.tv_minus = (TextView) convertView.findViewById(R.id.tv_minus);
			listItemView.tv_add = (TextView) convertView.findViewById(R.id.tv_add);
			listItemView.tv_amount = (TextView) convertView.findViewById(R.id.tv_amount);
			convertView.setTag(listItemView);
		} else {
			listItemView = (ListItemView) convertView.getTag();
		}
		
		if (cartLists.size() > 0) {
			final T_Sell_CartList cartList = cartLists.get(position);
			
			listItemView.tv_pd_name.setText(StringUtil.trimString(cartList.getPd_name()));
			StringBuffer productinfo = new StringBuffer();
			productinfo.append(StringUtil.trimString(cartList.getCr_name()));
			productinfo.append("/").append(StringUtil.trimString(cartList.getSz_name()));
			if(StringUtil.isNotEmpty(cartList.getBr_name())){
				productinfo.append("/").append(StringUtil.trimString(cartList.getBr_name()));
			}
			listItemView.tv_product_info.setText(productinfo.toString());
			listItemView.tv_sell_price.setText("￥"+cartList.getScl_sell_price());
			listItemView.tv_amount.setText(String.valueOf(cartList.getScl_amount()));
			if(StringUtil.isNotEmpty(cartList.getPd_img_path())){
				loadBitmap(CommonParam.file_url+cartList.getPd_img_path(), listItemView.iv_product_img);
			}else {
				listItemView.iv_product_img.setImageResource(R.drawable.nophoto);
			}
			listItemView.tv_minus.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if(cartList.getScl_amount() <= 1){
						ActivityUtil.showShortToast(context, "购买数量必须大于0");
						return;
					}
					cartList.setScl_amount(cartList.getScl_amount() - 1);
					listItemView.tv_amount.setText(String.valueOf(cartList.getScl_amount()));
					((ReceiveActivity)context).updateAmount(position);
				}
			});
			listItemView.tv_add.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					cartList.setScl_amount(cartList.getScl_amount() + 1);
					listItemView.tv_amount.setText(String.valueOf(cartList.getScl_amount()));
					((ReceiveActivity)context).updateAmount(position);
				}
			});
		}
		convertView.setOnLongClickListener(new View.OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				((ReceiveActivity)context).delCartList(position);
				return false;
			}
		});
		return convertView;
	}
	public final class ListItemView { // 自定义控件集合
		private ImageView iv_product_img;
		public TextView tv_pd_name, tv_product_info,tv_sell_price,tv_minus,tv_add,tv_amount;
	}
	private void loadBitmap(String url,final ImageView imageView){
		imageLoader.get(url, new ImageListener() {
			@Override
			public void onResponse(ImageContainer response, boolean isImmediate) {
				Bitmap bitmap = response.getBitmap();
				if(bitmap != null){
					imageView.setImageBitmap(ImageUtil.getCircleBitmap(bitmap));
				}
			}
			@Override
			public void onErrorResponse(VolleyError arg0) {
				
			}
		});
	}
}
