package com.zy.adapter.emp;

import java.util.ArrayList;
import java.util.List;

import zy.dto.base.emp.EmpSortDto;
import zy.util.StringUtil;

import com.zy.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SortAdapter extends BaseAdapter{
	private Context context;
	private List<EmpSortDto> sortDtos = new ArrayList<EmpSortDto>();
	private LayoutInflater listContainer; 
	public SortAdapter(Context context,List<EmpSortDto> sortDtos) {
		this.context = context;
		this.sortDtos = sortDtos;
		listContainer = LayoutInflater.from(context);
	}
	
	@Override
	public int getCount() {
		return sortDtos.size();
	}

	@Override
	public Object getItem(int position) {
		return sortDtos.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// 自定义视图
		ListItemView listItemView = null;
		if (convertView == null) {
			listItemView = new ListItemView();
			convertView = listContainer.inflate(R.layout.layout_emp_sort_item,null);
			listItemView.iv_em_img = (ImageView) convertView.findViewById(R.id.iv_em_img);
			listItemView.tv_index = (TextView) convertView.findViewById(R.id.tv_index);
			listItemView.tv_em_name = (TextView) convertView.findViewById(R.id.tv_em_name);
			listItemView.tv_info = (TextView) convertView.findViewById(R.id.tv_info);
			convertView.setTag(listItemView);
		} else {
			listItemView = (ListItemView) convertView.getTag();
		}
		if (sortDtos.size() > 0) {
			EmpSortDto item = sortDtos.get(position);
			listItemView.tv_index.setText(String.valueOf(position+1));
			listItemView.tv_em_name.setText(item.getEm_name());
			StringBuffer info = new StringBuffer();//销售额：0.00 成交额：0%
			info.append("销售额：").append(String.format("%.2f", item.getSellmoney()));
			info.append(" 成交率：");
			if(item.getReceivecount().intValue() != 0){
				info.append(StringUtil.doubleRtTwo((double)item.getDealcount()/item.getReceivecount()*100)).append("%");
			}else {
				info.append("0.00%");
			}
			listItemView.tv_info.setText(info.toString());
			if(position == 0){
				listItemView.tv_index.setBackgroundResource(R.drawable.circle_bg_red);
			}else if(position == 1){
				listItemView.tv_index.setBackgroundResource(R.drawable.circle_bg_orange);
			}else if(position == 2){
				listItemView.tv_index.setBackgroundResource(R.drawable.circle_bg_green);
			}else {
				listItemView.tv_index.setBackgroundResource(R.drawable.circle_bg_gray);
			}
		}
		return convertView;
	}
	public final class ListItemView { // 自定义控件集合
		public ImageView iv_em_img;
		public TextView tv_index, tv_em_name, tv_info;
	}
}
