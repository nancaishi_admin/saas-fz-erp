package com.zy.adapter.vip.visit;

import java.util.ArrayList;
import java.util.List;

import zy.entity.vip.set.T_Vip_ReturnSetUp;

import com.zy.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ReturnSetUpAdapter extends BaseAdapter{
	private Context context;                        //运行上下文   
	private List<T_Vip_ReturnSetUp> returnSetUps = new ArrayList<T_Vip_ReturnSetUp>();
	private LayoutInflater listContainer;
	private int selectedIndex = 0; //默认选中第一个
	
    public ReturnSetUpAdapter(Context context, List<T_Vip_ReturnSetUp> returnSetUps) {
		this.context = context;
		listContainer = LayoutInflater.from(context); // 创建视图容器并设置上下文
		this.returnSetUps = returnSetUps;
	}

	@Override
	public int getCount() {
		return returnSetUps.size();
	}

	@Override
	public T_Vip_ReturnSetUp getItem(int position) {
		return returnSetUps.get(position);
	}
	   
	@Override
	public long getItemId(int position) {
		return position;
	}
	
	public void setSelectedIndex(int selectedIndex) {
		this.selectedIndex = selectedIndex;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        //自定义视图   
        final ListItemView  listItemView;   
        if (convertView == null) {   
            listItemView = new ListItemView();    
            //获取list_item布局文件的视图   
            convertView = listContainer.inflate(R.layout.layout_vip_returnsetup_item, null);   
            //获取控件对象
            listItemView.lviName = (TextView)convertView.findViewById(R.id.lviName);   
            convertView.setTag(listItemView);   
        }else {   
            listItemView = (ListItemView)convertView.getTag();   
        }      
        if(selectedIndex == position){
        	listItemView.lviName.setBackgroundColor(context.getResources().getColor(R.color.app_main));
        	listItemView.lviName.setTextColor(context.getResources().getColor(R.color.white));
		}else{
			listItemView.lviName.setBackgroundColor(context.getResources().getColor(R.color.white));
			listItemView.lviName.setTextColor(context.getResources().getColor(R.color.font_value));
		}
		if (returnSetUps != null&&returnSetUps.size()>0) {
			T_Vip_ReturnSetUp returnSetUp = returnSetUps.get(position);
	        listItemView.lviName.setText(returnSetUp.getRts_name());
		}
        return convertView;  
	}

	public final class ListItemView { // 自定义控件集合
		public TextView lviName;
	}
	
}
