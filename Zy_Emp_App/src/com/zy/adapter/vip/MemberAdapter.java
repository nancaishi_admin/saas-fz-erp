package com.zy.adapter.vip;

import java.util.ArrayList;
import java.util.List;

import zy.entity.vip.member.T_Vip_Member;
import zy.util.DateUtil;
import zy.util.StringUtil;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.android.volley.toolbox.ImageLoader.ImageListener;
import com.zy.R;
import com.zy.activity.BaseActivity;
import com.zy.util.BitmapCache;
import com.zy.util.CommonParam;
import com.zy.util.ImageUtil;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MemberAdapter extends BaseAdapter{
	private Context context;
	private List<T_Vip_Member> members = new ArrayList<T_Vip_Member>();
	private LayoutInflater listContainer; 
	private ImageLoader imageLoader;
	public MemberAdapter(Context context,List<T_Vip_Member> members) {
		this.context = context;
		this.members = members;
		listContainer = LayoutInflater.from(context);
		imageLoader = new ImageLoader(Volley.newRequestQueue(context), new BitmapCache());
	}
	
	@Override
	public int getCount() {
		return members.size();
	}

	@Override
	public Object getItem(int position) {
		return members.get(position);
	}

	@Override
	public long getItemId(int position) {
		return members.get(position).getVm_id();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		T_Vip_Member member = members.get(position);
		// 自定义视图
		ListItemView listItemView = null;
		if (convertView == null) {
			listItemView = new ListItemView();
			convertView = listContainer.inflate(R.layout.layout_vip_member_item,null);
			listItemView.iv_vm_img = (ImageView) convertView.findViewById(R.id.iv_vm_img);
			listItemView.tv_vm_name = (TextView) convertView.findViewById(R.id.tv_vm_name);
			listItemView.tv_consume_info = (TextView) convertView.findViewById(R.id.tv_consume_info);
			listItemView.tv_icon_next = (TextView) convertView.findViewById(R.id.tv_icon_next);
			convertView.setTag(listItemView);
		} else {
			listItemView = (ListItemView) convertView.getTag();
		}
		listItemView.tv_vm_name.setText(StringUtil.trimString(member.getVm_name())+"("+StringUtil.trimString(member.getVm_mobile())+")");
		StringBuffer info = new StringBuffer();
		info.append("消费"+StringUtil.trimString(member.getVm_times())+"次，");
		info.append("￥:"+StringUtil.trimString(member.getVm_total_money())+"元，");
		if(StringUtil.isNotEmpty(member.getVm_lastbuy_date())){
			info.append(DateUtil.getDaysBetween(member.getVm_lastbuy_date(), DateUtil.getYearMonthDate())).append("天未消费");
		}else {
			info.append(DateUtil.getDaysBetween(member.getVm_date(), DateUtil.getYearMonthDate())).append("天未消费");
		}
		listItemView.tv_consume_info.setText(info.toString());
		listItemView.tv_icon_next.setTypeface(((BaseActivity)context).iconfont);
		if(StringUtil.isNotEmpty(member.getVm_img_path())){
			loadBitmap(CommonParam.file_url+member.getVm_img_path(), listItemView.iv_vm_img);
		}else {
			listItemView.iv_vm_img.setImageResource(R.drawable.head1);
		}
		return convertView;
	}
	public final class ListItemView { // 自定义控件集合
		private ImageView iv_vm_img;
		public TextView tv_vm_name, tv_consume_info,tv_icon_next;
	}
	private void loadBitmap(String url,final ImageView imageView){
		imageLoader.get(url, new ImageListener() {
			@Override
			public void onResponse(ImageContainer response, boolean isImmediate) {
				Bitmap bitmap = response.getBitmap();
				if(bitmap != null){
					imageView.setImageBitmap(ImageUtil.getCircleBitmap(bitmap));
				}
			}
			@Override
			public void onErrorResponse(VolleyError arg0) {
				
			}
		});
	}
}
