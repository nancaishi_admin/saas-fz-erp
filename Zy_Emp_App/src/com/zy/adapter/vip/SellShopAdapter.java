package com.zy.adapter.vip;

import java.util.ArrayList;
import java.util.List;

import zy.entity.sell.cash.T_Sell_Shop;
import zy.util.StringUtil;

import com.zy.R;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class SellShopAdapter extends BaseAdapter{
	private Context context;
	public Typeface iconfont;
	private List<T_Sell_Shop> sellShops = new ArrayList<T_Sell_Shop>();
	private LayoutInflater listContainer; 
	public SellShopAdapter(Context context,List<T_Sell_Shop> sellShops) {
		this.context = context;
		this.sellShops = sellShops;
		listContainer = LayoutInflater.from(context);
		iconfont = Typeface.createFromAsset(context.getAssets(), "iconfont/iconfont.ttf");
	}
	
	@Override
	public int getCount() {
		return sellShops.size();
	}

	@Override
	public Object getItem(int position) {
		return sellShops.get(position);
	}

	@Override
	public long getItemId(int position) {
		return sellShops.get(position).getSh_id();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// 自定义视图
		ListItemView listItemView = null;
		if (convertView == null) {
			listItemView = new ListItemView();
			convertView = listContainer.inflate(R.layout.fragment_sell_item,null);
			listItemView.tv_sh_date = (TextView) convertView.findViewById(R.id.tv_sh_date);
			listItemView.tv_consume_info = (TextView) convertView.findViewById(R.id.tv_consume_info);
			listItemView.tv_icon_next = (TextView) convertView.findViewById(R.id.tv_icon_next);
			convertView.setTag(listItemView);
		} else {
			listItemView = (ListItemView) convertView.getTag();
		}
		if (null != sellShops && sellShops.size() > 0) {
			T_Sell_Shop item = sellShops.get(position);
			listItemView.tv_sh_date.setText(StringUtil.trimString(item.getSh_date())+"("+formatShState(item.getSh_state())+")");
			StringBuffer info = new StringBuffer();
			info.append("销售件数："+item.getSh_amount());
			info.append("，￥:"+String.format("%.2f", item.getSh_money()));
			listItemView.tv_consume_info.setText(info.toString());
			listItemView.tv_icon_next.setTypeface(iconfont);
		}
		return convertView;
	}
	private String formatShState(Integer shState){//0零售1退货2换货
		if(shState == null){
			return "";
		}
		if(shState.intValue() == 0){
			return "销售";
		}else if(shState.intValue() == 1){
			return "退货";
		}else if(shState.intValue() == 2){
			return "换货";
		}
		return "";
	}
	public final class ListItemView { // 自定义控件集合
		public TextView tv_sh_date, tv_consume_info,tv_icon_next;
	}
}
