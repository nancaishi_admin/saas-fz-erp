package com.zy.adapter.common;

import java.util.ArrayList;
import java.util.List;

import zy.dto.common.TabDto;

import com.zy.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class TabGridAdapter extends BaseAdapter{
	private Context context;                        //运行上下文   
	private List<TabDto> tabDtos = new ArrayList<TabDto>();
	private LayoutInflater listContainer;
	private int selectedIndex = 0; //默认选中第一个
	
    public TabGridAdapter(Context context, List<TabDto> tabDtos) {
		this.context = context;
		listContainer = LayoutInflater.from(context); // 创建视图容器并设置上下文
		this.tabDtos = tabDtos;
	}

	@Override
	public int getCount() {
		return tabDtos.size();
	}
	
	@Override
	public Object getItem(int position) {
		return tabDtos.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	public void setSelectedIndex(int selectedIndex) {
		this.selectedIndex = selectedIndex;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        //自定义视图   
        final ListItemView  listItemView;   
        if (convertView == null) {   
            listItemView = new ListItemView();    
            //获取list_item布局文件的视图   
            convertView = listContainer.inflate(R.layout.layout_common_grid_item, null);   
            //获取控件对象
            listItemView.tv_name = (TextView)convertView.findViewById(R.id.tv_name);   
            convertView.setTag(listItemView);   
        }else {   
            listItemView = (ListItemView)convertView.getTag();   
        }      
        if(selectedIndex == position){
        	listItemView.tv_name.setBackgroundColor(context.getResources().getColor(R.color.app_main));
        	listItemView.tv_name.setTextColor(context.getResources().getColor(R.color.white));
		}else{
			listItemView.tv_name.setBackgroundColor(context.getResources().getColor(R.color.white));
			listItemView.tv_name.setTextColor(context.getResources().getColor(R.color.font_value));
		}
		if (tabDtos != null&&tabDtos.size()>0) {
			TabDto item = tabDtos.get(position);
	        listItemView.tv_name.setText(item.getName());
		}
        return convertView;  
	}

	public final class ListItemView { // 自定义控件集合
		public TextView tv_name;
	}
}
