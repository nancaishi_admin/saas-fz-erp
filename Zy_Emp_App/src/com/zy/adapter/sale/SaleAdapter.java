package com.zy.adapter.sale;

import java.util.ArrayList;
import java.util.List;

import zy.entity.shop.sale.T_Shop_Sale;
import zy.util.StringUtil;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.zy.R;
import com.zy.activity.BaseActivity;

@SuppressLint("InflateParams")
public class SaleAdapter extends BaseAdapter{
	private Context context;
	private List<T_Shop_Sale> sales = new ArrayList<T_Shop_Sale>();
	private LayoutInflater listContainer; 
	public SaleAdapter(Context context) {
		this.context = context;
		listContainer = LayoutInflater.from(context);
	}
	
	@Override
	public int getCount() {
		return sales.size();
	}

	@Override
	public Object getItem(int position) {
		return sales.get(position);
	}

	@Override
	public long getItemId(int position) {
		return sales.get(position).getSs_id();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// 自定义视图
		ListItemView item = null;
		if (convertView == null) {
			item = new ListItemView();
			convertView = listContainer.inflate(R.layout.layout_salelist,null);
			item.tv_ss_name = (TextView) convertView.findViewById(R.id.tv_ss_name);
			item.tv_ss_type = (TextView) convertView.findViewById(R.id.tv_ss_type);
			item.tv_ss_date = (TextView) convertView.findViewById(R.id.tv_ss_date);
//			item.tv_icon_next = (TextView) convertView.findViewById(R.id.tv_icon_next);
			convertView.setTag(item);
		} else {
			item = (ListItemView) convertView.getTag();
		}
		if (null != sales && sales.size() > 0) {
			T_Shop_Sale sale = sales.get(position);
			item.tv_ss_name.setText("名称："+sale.getSs_name());
			item.tv_ss_date.setText("时间："+StringUtil.trimString(sale.getSs_begin_date())+
					"~"+StringUtil.trimString(sale.getSs_end_date()));
			item.tv_ss_type.setText("类型："+sale.getSs_model());
//			item.tv_icon_next.setTypeface(((BaseActivity)context).iconfont);
		}
		return convertView;
	}
	public final class ListItemView { // 自定义控件集合
		public TextView tv_ss_name, tv_ss_type,tv_ss_date;
	}
	public List<T_Shop_Sale> getSales() {
		return sales;
	}

	public void setSales(List<T_Shop_Sale> sales) {
		this.sales = sales;
	}
	
}
