package com.zy.api.base;

import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import zy.dto.base.emp.EmpPerformanceDto;
import zy.dto.base.emp.EmpSortDto;
import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.zy.api.BaseAPI;
import com.zy.api.VolleyErrorListener;
import com.zy.util.CommonParam;

public class EmpAPI extends BaseAPI{
	
	public EmpAPI(Context context){
		super(context);
	}
	
	public void updatePwd(Map<String, Object> params, final Handler handler, final int SUCC_STATE){
		update(params, CommonParam.http_url + "api/base/emp/updatePwd", handler, SUCC_STATE);
	}
	
	public void loadPerformanceByEmp(Map<String, Object> params, final Handler handler, final int SUCC_STATE) {
		list(params, CommonParam.http_url+"api/base/emp/loadPerformanceByEmp", EmpPerformanceDto.class, handler, SUCC_STATE);
	}
	
	public void listEmpSort(Map<String, Object> params, final Handler handler, final int SUCC_STATE) {
		list(params, CommonParam.http_url+"api/base/emp/listEmpSort", EmpSortDto.class, handler, SUCC_STATE);
	}
	
	public void loadEmpSellMoney(Map<String, Object> params,final Handler handler,final int SUCC_STATE){
		String url = CommonParam.http_url+"api/base/emp/loadEmpSellMoney";
		final Message message = Message.obtain();
		JSONObject jsonObject = new JSONObject(params);
		JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Method.POST,url,
				jsonObject, new Response.Listener<JSONObject>(){
					@Override
					public void onResponse(JSONObject resultJson) {
						try {
							int stat = resultJson.getInt("stat");
							if(stat == 200){
								message.what = SUCC_STATE;
								message.obj = Double.parseDouble(resultJson.getString("data"));
							}else {
								message.what = CommonParam.STATE_ERROR;
								message.obj = resultJson.getString("message");
							}
						} catch (JSONException e) {
							message.what = CommonParam.STATE_ERROR;
							message.obj = "��ѯ���ݴ���!";
						} finally{
							handler.sendMessage(message);
						}
					}
				}, new VolleyErrorListener(handler));
		jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		Volley.newRequestQueue(context).add(jsonObjectRequest);
	}
	
}
