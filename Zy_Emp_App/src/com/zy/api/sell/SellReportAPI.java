package com.zy.api.sell;

import java.util.Map;

import zy.dto.vip.member.ConsumeSnapshotDto;
import zy.entity.sell.cash.T_Sell_Shop;
import zy.entity.sell.cash.T_Sell_ShopList;
import android.content.Context;
import android.os.Handler;

import com.zy.api.BaseAPI;
import com.zy.util.CommonParam;

public class SellReportAPI extends BaseAPI{
	
	public SellReportAPI(Context context){
		super(context);
	}
	
	public void listShopByVip(Map<String, Object> params, final Handler handler, final int SUCC_STATE) {
		list(params, CommonParam.http_url+"api/sell/report/listShopByVip", T_Sell_Shop.class, handler, SUCC_STATE);
	}
	
	public void listShopList(Map<String, Object> params, final Handler handler, final int SUCC_STATE) {
		list(params, CommonParam.http_url+"api/sell/report/listShopList", T_Sell_ShopList.class, handler, SUCC_STATE);
	}
	
	public void loadConsumeSnapshot(Map<String, Object> params, final Handler handler, final int SUCC_STATE) {
		load(params, CommonParam.http_url+"api/sell/report/loadConsumeSnapshot", ConsumeSnapshotDto.class, handler, SUCC_STATE);
	}
	
}
