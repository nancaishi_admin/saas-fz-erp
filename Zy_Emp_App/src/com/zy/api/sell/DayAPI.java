package com.zy.api.sell;

import java.util.Map;

import zy.entity.sell.day.T_Sell_Day;
import android.content.Context;
import android.os.Handler;

import com.zy.api.BaseAPI;
import com.zy.util.CommonParam;

public class DayAPI extends BaseAPI{
	public DayAPI(Context context) {
		super(context);
	}
	
	public void queryDay(Map<String, Object> params, final Handler handler, final int SUCC_STATE){
		load(params, CommonParam.http_url + "api/sell/day/queryDay", T_Sell_Day.class, handler, SUCC_STATE);
	}
	
	public void comereceive(Map<String, Object> params, final Handler handler, final int SUCC_STATE){
		update(params, CommonParam.http_url + "api/sell/day/comereceive", handler, SUCC_STATE);
	}
	
	public void doTry(Map<String, Object> params, final Handler handler, final int SUCC_STATE){
		update(params, CommonParam.http_url + "api/sell/day/doTry", handler, SUCC_STATE);
	}
	
}
