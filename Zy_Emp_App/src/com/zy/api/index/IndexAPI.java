package com.zy.api.index;

import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import zy.dto.base.emp.EmpLoginDto;
import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.alibaba.fastjson.JSON;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.zy.api.VolleyErrorListener;
import com.zy.util.CommonParam;

public class IndexAPI {
	private Context context;
	public IndexAPI(Context context){
		this.context = context;
	}
	public void login(Map<String,String> param,final Handler handler,final int SUCC_STATE){
		final Message message = Message.obtain();
		String url = com.zy.util.CommonParam.http_url+"api/base/emp/login";
		JSONObject jsonObject = new JSONObject(param);
		JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Method.POST,url,
				jsonObject, new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject resultJson) {
						try {
							int stat = resultJson.getInt("stat");
							if(stat == 200){
								EmpLoginDto user = JSON.parseObject(resultJson.getString("data"), EmpLoginDto.class);
								if(user != null){
									message.what = SUCC_STATE;
									message.obj = user;
								}else{
									message.what = CommonParam.STATE_ERROR;
									message.obj = "用户名或密码错误";
								}
							}else {
								message.what = CommonParam.STATE_ERROR;
								message.obj = resultJson.getString("message");
							}
						} catch (JSONException e) {
							message.what = CommonParam.STATE_ERROR;
							message.obj = "查询数据错误!";
						} finally{
							handler.sendMessage(message);
						}
					}
				}, new VolleyErrorListener(handler));
		jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		Volley.newRequestQueue(context).add(jsonObjectRequest);
	}
}
