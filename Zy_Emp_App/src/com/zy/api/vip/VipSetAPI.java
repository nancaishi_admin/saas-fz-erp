package com.zy.api.vip;

import java.util.Map;

import zy.entity.vip.set.T_Vip_ReturnSetUp;
import android.content.Context;
import android.os.Handler;

import com.zy.api.BaseAPI;
import com.zy.util.CommonParam;

public class VipSetAPI extends BaseAPI{
	
	public VipSetAPI(Context context){
		super(context);
	}
	
	public void loadReturnSetUp(Map<String, Object> params, final Handler handler, final int SUCC_STATE) {
		list(params, CommonParam.http_url+"api/vip/set/loadReturnSetUp", T_Vip_ReturnSetUp.class, handler, SUCC_STATE);
	}
	
	
}
