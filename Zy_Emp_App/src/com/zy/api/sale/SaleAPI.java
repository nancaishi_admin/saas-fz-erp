package com.zy.api.sale;

import java.util.Map;

import zy.entity.shop.sale.T_Shop_Sale;
import android.content.Context;
import android.os.Handler;

import com.zy.api.BaseAPI;
import com.zy.util.CommonParam;

public class SaleAPI extends BaseAPI{
	public SaleAPI(Context context){
		super(context);
	}
	
	public void list(Map<String, Object> params, final Handler handler, final int SUCC_STATE) {
		list(params, CommonParam.http_url+"api/shop/sale/list", T_Shop_Sale.class, handler, SUCC_STATE);
	}
}
