package com.zy.api;

import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.os.Handler;
import android.os.Message;

import com.alibaba.fastjson.JSON;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.zy.util.CommonParam;

public class BaseAPI {
	protected Context context;
	
	public BaseAPI(Context context){
		this.context = context;
	}
	
	@SuppressWarnings("rawtypes")
	protected void list(Map<String, Object> params,String url,final Class cls, final Handler handler, final int SUCC_STATE) {
		final Message message = Message.obtain();
		JSONObject jsonObject = new JSONObject(params);
		JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Method.POST,url,
				jsonObject, new Response.Listener<JSONObject>(){
					@SuppressWarnings("unchecked")
					@Override
					public void onResponse(JSONObject resultJson) {
						try {
							int stat = resultJson.getInt("stat");
							if(stat == 200){
								message.what = SUCC_STATE;
								message.obj = JSON.parseArray(resultJson.getString("data"),cls);
							}else {
								message.what = CommonParam.STATE_ERROR;
								message.obj = resultJson.getString("message");
							}
						} catch (JSONException e) {
							message.what = CommonParam.STATE_ERROR;
							message.obj = "查询数据错误!";
						} finally{
							handler.sendMessage(message);
						}
					}
				}, new VolleyErrorListener(handler));
		jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		Volley.newRequestQueue(context).add(jsonObjectRequest);
	}
	
	@SuppressWarnings("rawtypes")
	protected void load(Map<String, Object> params,String url,final Class cls,final Handler handler,final int SUCC_STATE){
		final Message message = Message.obtain();
		JSONObject jsonObject = new JSONObject(params);
		JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Method.POST,url,
				jsonObject, new Response.Listener<JSONObject>(){
					@SuppressWarnings("unchecked")
					@Override
					public void onResponse(JSONObject resultJson) {
						try {
							int stat = resultJson.getInt("stat");
							if(stat == 200){
								message.what = SUCC_STATE;
								message.obj = JSON.parseObject(resultJson.getString("data"),cls);
							}else {
								message.what = CommonParam.STATE_ERROR;
								message.obj = resultJson.getString("message");
							}
						} catch (JSONException e) {
							message.what = CommonParam.STATE_ERROR;
							message.obj = "查询数据错误!";
						} finally{
							handler.sendMessage(message);
						}
					}
				}, new VolleyErrorListener(handler));
		jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		Volley.newRequestQueue(context).add(jsonObjectRequest);
	}
	
	protected void save(Map<String, Object> params,String url,final Handler handler,final int SUCC_STATE){
		final Message message = Message.obtain();
		JSONObject jsonObject = new JSONObject(params);
		JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Method.POST,url,
				jsonObject, new Response.Listener<JSONObject>(){
					@Override
					public void onResponse(JSONObject resultJson) {
						try {
							int stat = resultJson.getInt("stat");
							if(stat == 200){
								message.what = SUCC_STATE;
							}else {
								message.what = CommonParam.STATE_ERROR;
								message.obj = resultJson.getString("message");
							}
						} catch (JSONException e) {
							message.what = CommonParam.STATE_ERROR;
							message.obj = "数据请求异常";
						} finally {
							handler.sendMessage(message);
						}
					}
				}, new VolleyErrorListener(handler));
		jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(10 * 1000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		Volley.newRequestQueue(context).add(jsonObjectRequest);
	}
	
	protected void update(Map<String, Object> params,String url,final Handler handler,final int SUCC_STATE){
		final Message message = Message.obtain();
		JSONObject jsonObject = new JSONObject(params);
		JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Method.POST,url,
				jsonObject, new Response.Listener<JSONObject>(){
					@Override
					public void onResponse(JSONObject resultJson) {
						try {
							int stat = resultJson.getInt("stat");
							if(stat == 200){
								message.what = SUCC_STATE;
							}else {
								message.what = CommonParam.STATE_ERROR;
								message.obj = resultJson.getString("message");
							}
						} catch (JSONException e) {
							message.what = CommonParam.STATE_ERROR;
							message.obj = "数据请求异常";
						} finally {
							handler.sendMessage(message);
						}
					}
				}, new VolleyErrorListener(handler));
		jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(10 * 1000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		Volley.newRequestQueue(context).add(jsonObjectRequest);
	}
	
	public void loadImg(String url,final Handler handler,final int SUCC_STATE){
		final Message message = Message.obtain();
		ImageRequest request = new ImageRequest(url, 
				new Listener<Bitmap>() {
					@Override
					public void onResponse(Bitmap bit) {
						message.what = SUCC_STATE;
						message.obj = bit;
						handler.sendMessage(message);
					}
				}, 0, 0, Config.ARGB_8888,
				new ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
					}
				});
		Volley.newRequestQueue(context).add(request);
	}
}
