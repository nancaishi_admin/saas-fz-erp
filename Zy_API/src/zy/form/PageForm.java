package zy.form;

import zy.util.StringUtil;

public class PageForm {
	private Integer page;
	private Integer rows;
	private String sidx;
	private String sord;
	private String searchContent;
	private String begindate;
	private String enddate;
	public Integer getPage() {
		if(page== null){
			return null;
		}
		return page.equals(0)? 1 : page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	public Integer getRows() {
		return rows;
	}
	public void setRows(Integer rows) {
		this.rows = rows;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
	public String getSearchContent() {
		return StringUtil.decodeString(searchContent);
	}
	public void setSearchContent(String searchContent) {
		this.searchContent = searchContent;
	}
	public String getBegindate() {
		return begindate;
	}
	public void setBegindate(String begindate) {
		this.begindate = begindate;
	}
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
}
