package zy.vo.sell;

import zy.entity.sell.dayend.T_Sell_DayEnd;
import zy.util.NumberUtil;

public class EndVO {
	public static String buildEnd(T_Sell_DayEnd dayend){
		StringBuffer html = new StringBuffer("");
		html.append("");
		html.append("<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
		html.append("<tr><td>店铺："+dayend.getShop_name()+"</td></tr>");
		html.append("<tr><td >收银员："+dayend.getEm_name()+"&nbsp;&nbsp;班次："+dayend.getSt_name()+"</td></tr>");
		html.append("<tr><td><hr/></td></tr>");
		html.append("<tr><td>单据数："+NumberUtil.toInteger(dayend.getDe_bills())+"</td></tr>");
		html.append("<tr><td>零售单："+NumberUtil.toInteger(dayend.getDe_sell_bills())+"</td></tr>");
		html.append("<tr><td>退货单："+NumberUtil.toInteger(dayend.getDe_back_bills())+"</td></tr>");
		html.append("<tr><td>换货单："+NumberUtil.toInteger(dayend.getDe_change_bills())+"</td></tr>");
		html.append("<tr><td><hr/></td></tr>");
		html.append("<tr><td>销售数量："+NumberUtil.toInteger(dayend.getDe_amount())+"</td></tr>");
		html.append("<tr><td>销售金额："+dayend.getDe_money()+"</td></tr>");
		html.append("<tr><td>调&nbsp;&nbsp;出："+NumberUtil.toInteger(dayend.getDe_out_amount())+"</td></tr>");
		html.append("<tr><td>调&nbsp;&nbsp;入："+NumberUtil.toInteger(dayend.getDe_in_amount())+"</td></tr>");
		html.append("<tr><td><hr/></td></tr>");
		html.append("<tr><td>现金："+NumberUtil.toDouble(dayend.getDe_cash())+"</td></tr>");
		html.append("<tr><td>用微支付："+NumberUtil.toDouble(dayend.getDe_wx_money())+"</td></tr>");
		html.append("<tr><td>用支付宝："+NumberUtil.toDouble(dayend.getDe_ali_money())+"</td></tr>");
		html.append("<tr><td>用储值卡："+NumberUtil.toDouble(dayend.getDe_cd_money())+"</td></tr>");
		html.append("<tr><td>用银行卡："+NumberUtil.toDouble(dayend.getDe_bank_money())+"</td></tr>");
		html.append("<tr><td>用商场卡："+NumberUtil.toDouble(dayend.getDe_mall_money())+"</td></tr>");
		html.append("<tr><td>用优惠券："+NumberUtil.toDouble(dayend.getDe_ec_money())+"</td></tr>");
		html.append("<tr><td>用代金券："+NumberUtil.toDouble(dayend.getDe_vc_money())+"</td></tr>");
		html.append("<tr><td>用订金："+NumberUtil.toDouble(dayend.getDe_sd_money())+"</td></tr>");
		html.append("<tr><td>积分抵现："+NumberUtil.toDouble(dayend.getDe_point_money())+"</td></tr>");
		html.append("<tr><td><hr/></td></tr>");
		html.append("<tr><td>会员办卡："+NumberUtil.toInteger(dayend.getDe_vips())+"</td></tr>");
		html.append("<tr><td>办储值卡："+NumberUtil.toInteger(dayend.getDe_cd_sends())+"</td></tr>");
		html.append("<tr><td>发储值现金："+NumberUtil.toDouble(dayend.getDe_cd_send_cash())+"</td></tr>");
		html.append("<tr><td>发储值刷卡："+NumberUtil.toDouble(dayend.getDe_cd_send_bank())+"</td></tr>");
		html.append("<tr><td>会员积分："+NumberUtil.toInteger(dayend.getDe_vip_point())+"</td></tr>");
		html.append("<tr><td>兑礼品数："+NumberUtil.toInteger(dayend.getDe_gifts())+"</td></tr>");
		html.append("<tr><td>充储值现金："+NumberUtil.toDouble(dayend.getDe_cd_fill_cash())+"</td></tr>");
		html.append("<tr><td>充储值刷卡："+NumberUtil.toDouble(dayend.getDe_cd_fill_bank())+"</td></tr>");
		html.append("<tr><td>发代金券："+NumberUtil.toDouble(dayend.getDe_vc_sends())+"</td></tr>");
		html.append("<tr><td>发券金额："+NumberUtil.toDouble(dayend.getDe_vc_send_money())+"</td></tr>");
		html.append("<tr><td>收订金金额："+NumberUtil.toDouble(dayend.getDe_sd_send_money())+"</td></tr>");
		html.append("<tr><td>退订金金额："+NumberUtil.toDouble(dayend.getDe_sd_back_money())+"</td></tr>");
		html.append("<tr><td><hr/></td></tr>");
		html.append("<tr><td>上班结存："+NumberUtil.toInteger(dayend.getDe_last_money())+"</td></tr>");
		html.append("<tr><td>上班备用："+NumberUtil.toDouble(dayend.getDe_petty_cash())+"</td></tr>");
		html.append("<tr><td>抹零金额："+NumberUtil.toDouble(dayend.getDe_lost_money())+"</td></tr>");
		html.append("<tr><td><hr/></td></tr>");
		double totalCash = NumberUtil.toDouble(dayend.getDe_cash());
		totalCash += NumberUtil.toDouble(dayend.getDe_cd_fill_cash());
		totalCash += NumberUtil.toDouble(dayend.getDe_cd_send_cash());
		totalCash += NumberUtil.toDouble(dayend.getDe_vc_send_money());
		totalCash += NumberUtil.toDouble(dayend.getDe_sd_send_money());
		totalCash -= NumberUtil.toDouble(dayend.getDe_sd_back_money());
		//总刷卡=收银刷卡+储值卡充值刷卡+储值卡发卡刷卡
		double totalBank = NumberUtil.toDouble(dayend.getDe_bank_money());
		totalBank += NumberUtil.toDouble(dayend.getDe_cd_fill_bank());
		totalBank += NumberUtil.toDouble(dayend.getDe_cd_send_bank());
		html.append("<tr><td>合计现金："+totalCash+"</td></tr>");
		html.append("<tr><td>合计刷卡："+totalBank+"</td></tr>");
		html.append("<tr><td>交班时间："+dayend.getDe_enddate()+"</td></tr>");
		html.append("</table>");
		return html.toString();
	}
}
