package zy.vo.sell;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import zy.entity.base.shop.T_Base_Shop;
import zy.entity.base.size.T_Base_Size;
import zy.entity.buy.order.T_Buy_Product;
import zy.entity.sell.cash.T_Sell_Shop;
import zy.entity.sell.cash.T_Sell_ShopList;
import zy.entity.sell.cash.T_Sell_Shop_Temp;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.entity.sell.cashier.T_Sell_Cashier_Set;
import zy.entity.sell.deposit.T_Sell_DepositList;
import zy.entity.sell.set.T_Sell_Print;
import zy.entity.sell.set.T_Sell_PrintData;
import zy.entity.sell.set.T_Sell_PrintField;
import zy.entity.sell.set.T_Sell_Set;
import zy.entity.stock.data.T_Stock_Data;
import zy.entity.vip.member.T_Vip_Member;
import zy.form.NumberForm;
import zy.util.Arith;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.NumberUtil;
import zy.util.PrintUtil;
import zy.util.StringUtil;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;


public class SellVO {
	public static List<T_Sell_Cashier_Set> buildMap(String data,String em_code,Integer companyid){
		JSONObject jsonObj = JSONObject.parseObject(data);
		List<T_Sell_Cashier_Set> list = new ArrayList<T_Sell_Cashier_Set>();
		T_Sell_Cashier_Set set = null;
		for (Set<?> iter = jsonObj.keySet(); iter.iterator().hasNext();) {
			set = new T_Sell_Cashier_Set();
		    String key = (String)iter.iterator().next();  
		    set.setCs_key(key);
		    set.setCs_value(jsonObj.getString(key));
		    set.setCs_em_code(em_code);
		    set.setCompanyid(companyid);
		    list.add(set);
		    set = null;
		}
		return list;
	}
	
	public static List<T_Sell_Set> buildSellMap(String data,T_Sell_Cashier cashier){
		JSONObject jsonObj = JSONObject.parseObject(data);
		List<T_Sell_Set> list = new ArrayList<T_Sell_Set>();
		T_Sell_Set set = null;
		for (Iterator<String> iter = jsonObj.keySet().iterator(); iter.hasNext();) {
			set = new T_Sell_Set();
		    String key = (String)iter.next();  
		    set.setSt_key(key);
		    set.setSt_value(jsonObj.getString(key));
		    set.setSt_shop_code(cashier.getCa_shop_code());
		    set.setCompanyid(cashier.getCompanyid());
		    list.add(set);
		    set = null;
		}
		return list;
	}
	
	public static void buildShopTemp(Map<String,Object> param){
		String data = (String)param.get("data");
		String sell_type = (String)param.get(CommonUtil.SELL_TYPE);
		Object shop_code = param.get(CommonUtil.SHOP_CODE);
		Object emp_code = param.get(CommonUtil.EMP_CODE);
		Object em_name = param.get(CommonUtil.EMP_NAME);
		Object companyid = param.get(CommonUtil.COMPANYID);	
		T_Vip_Member vip = (T_Vip_Member)param.get(CommonUtil.KEY_VIP);
		List<T_Sell_Shop_Temp> list = new ArrayList<T_Sell_Shop_Temp>();
		T_Sell_Shop_Temp item = null;
		JSONArray jsonArray = JSONArray.parseArray(data);
		List<T_Sell_Shop_Temp> _list = JSON.parseArray(data, T_Sell_Shop_Temp.class);
		System.out.println("-----------------------------"+_list.size());
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject json = jsonArray.getJSONObject(i);
			item = new T_Sell_Shop_Temp();
			item.setSht_sub_code(json.getString("sht_sub_code"));
			item.setSht_pd_code(json.getString("sht_pd_code"));
			item.setSht_ishand(json.getInteger("sht_ishand"));
			item.setSht_isvip(json.getInteger("sht_isvip"));
			item.setSht_ispoint(json.getInteger("sht_ispoint"));
			item.setSht_isgift(json.getInteger("sht_isgift"));
			item.setSht_cost_price(NumberUtil.toDouble(json.getString("sht_cost_price")));
			item.setSht_upcost_price(NumberUtil.toDouble(json.getString("sht_upcost_price")));
			item.setSht_vip_price(NumberUtil.toDouble(json.getString("sht_vip_price")));
			if(json.containsKey("sht_vip_code")){
				item.setSht_vip_code(json.getString("sht_vip_code"));
			}
			item.setSht_bd_code(json.getString("sht_bd_code"));
			item.setSht_tp_code(json.getString("sht_tp_code"));
			item.setSht_cr_code(json.getString("sht_cr_code"));
			item.setSht_sz_code(json.getString("sht_sz_code"));
			item.setSht_br_code(json.getString("sht_br_code"));
			item.setPd_no(json.getString("pd_no"));
			item.setPd_name(json.getString("pd_name"));
			item.setCr_name(json.getString("cr_name"));
			item.setSz_name(json.getString("sz_name"));
			item.setBr_name(json.getString("br_name"));
			
			item.setSht_sell_price(json.getDouble("sht_sell_price"));
			item.setSht_sign_price(json.getDouble("sht_sign_price"));
			item.setSht_shop_code(StringUtil.trimString(shop_code));
			item.setSht_em_code(StringUtil.trimString(emp_code));
			item.setEm_name(StringUtil.trimString(em_name));
			item.setSht_amount(json.getInteger("sht_amount"));
			item.setSht_price(json.getDouble("sht_sell_price"));
			item.setSht_money(json.getDouble("sht_sell_price")*json.getInteger("sht_amount"));
			item.setSht_isputup(0);
			item.setSht_handtype(0);
			item.setSht_handvalue(0d);
			item.setSht_state(Integer.parseInt(sell_type));
			item.setCompanyid(Integer.parseInt(StringUtil.trimString(companyid)));
			if(null != vip){
				item.setSht_vip_code(vip.getVm_code());
				item.setSht_vip_other(vip.getVip_other());
//				item.setVip_info(vip.getVm_cardcode()+"/"+vip.getVm_name());
			}
			list.add(item);
			item = null;
		}
		param.put("tempList", list);
	}
	
	public static List<String> buildDepot(String data){
		JSONArray arr = JSONArray.parseArray(data);
		List<String> list = new ArrayList<String>();
		for (int i = 0; i < arr.size(); i++) {
			JSONObject json = arr.getJSONObject(i);
			list.add(json.getString("dp_code"));
		}
		return list;
	}
	/**
	 * 核对会员是否生日，启用生日打折，则获取生日的折扣
	 * 
	 * */
	@SuppressWarnings("unchecked")
	public static double getVipRate(String vip_rate_type,T_Sell_Shop_Temp temp,Map<String,Object> param){
		boolean isbirthday = false;
		double vip_rate = -1d;
		Map<String,Object> setMap = (Map<String,Object>)param.get(CommonUtil.KEY_CASHIER_SET);
		T_Vip_Member vip = (T_Vip_Member)param.get(CommonUtil.KEY_VIP);
		if(null != setMap.get("KEY_VIP_BIRTHDAY") && "1".equals(setMap.get("KEY_VIP_BIRTHDAY"))){//启用了会员打折
			if(null != vip.getIsbirthday() && "1".equals(vip.getIsbirthday())){
				isbirthday = true;
			}
		}
		if(isbirthday){//如果为0的情况，则不在会员生日折扣范围内
			vip_rate = (Double)param.get("vip_rate");
		}else{
			if("1".equals(vip_rate_type)){//会员类别
				vip_rate = (Double)param.get("vip_rate");
			}
			if("2".equals(vip_rate_type)){//会员商品类别
				List<NumberForm> typeList = (List<NumberForm>)param.get("typeList");
				for(NumberForm type:typeList){
					String tp_code = type.getCode();
					if(temp.getSht_tp_code().equals(tp_code)){
						vip_rate = type.getNumber();
						break;
					}
				}
			}
			if("3".equals(vip_rate_type)){//会员商品品牌
				List<NumberForm> brandList = (List<NumberForm>)param.get("brandList");
				for(NumberForm brand:brandList){
					String bd_code = brand.getCode();
					if(temp.getSht_bd_code().equals(bd_code)){
						vip_rate = brand.getNumber();
						break;
					}
				}
			}
		}
		return vip_rate;
	}
	/**
	 * 会员促销折上折处理
	 * 此处是先促销后会员折的
	 * @param temp 临时表数据，money_decimal 小数位 ，money_type 精度，vip_rate 折扣率
	 * */
	public static void bulidSaleAndVipRate(T_Sell_Shop_Temp temp,String money_decimal,String money_type,double vip_rate){
		double sell_money = temp.getSht_sell_price()*temp.getSht_amount();//零售价
		double money =  NumberUtil.buildMoney(money_decimal, money_type,(sell_money-temp.getSht_sale_money())*vip_rate);
		double price = NumberUtil.buildMoney(money_decimal, money_type, money/temp.getSht_amount()) ;
		double vip_money = NumberUtil.buildMoney(money_decimal, money_type, sell_money-money-temp.getSht_sale_money());
		temp.setSht_price(price);
		temp.setSht_money(money);
		temp.setSht_vip_money(vip_money);
		temp.setSht_vipvalue(vip_rate);
		temp.setSht_hand_money(0d);
		temp.setSht_handtype(0);//把手动折扣的记录去掉
		temp.setSht_handvalue(0d);
	}
	
	/**
	 * 会员折扣的处理，不是价格
	 * @param temp 临时表数据，money_decimal 小数位 ，money_type 精度，vip_rate 折扣率
	 * */
	public static void buildVipRate(T_Sell_Shop_Temp temp,String money_decimal,String money_type,double vip_rate){
		double sell_money = temp.getSht_sell_price()*temp.getSht_amount();//零售价
		double price = NumberUtil.buildMoney(money_decimal, money_type, temp.getSht_sell_price()*vip_rate) ;//折后价
		double money = NumberUtil.buildMoney(money_decimal, money_type, price*temp.getSht_amount());//折后金额
		temp.setSht_price(price);
		temp.setSht_money(money);
		temp.setSht_vip_money(NumberUtil.buildMoney(money_decimal, money_type, sell_money-money));//会员让利金额
		temp.setSht_sale_money(0d);//未启用折上折，会员打折时，促销金额为0
		temp.setSht_hand_money(0d);//未启用折上折，会员打折时，手动让利金额为0
		temp.setSht_vipvalue(vip_rate);
		temp.setSht_handtype(0);//把手动折扣的记录去掉
		temp.setSht_handvalue(0d);
	}
	
	/**
	 * 无折扣处理，针对商品类别或品牌，这个要设置的，如果没有设置，无折上折，则要把手动打折清零
	 * @param temp 临时表数据，money_decimal 小数位 ，money_type 精度，vip_rate 折扣率
	 * */
	public static void buildNoVipRate(T_Sell_Shop_Temp temp,String money_decimal,String money_type){
		double price = NumberUtil.buildMoney(money_decimal, money_type, temp.getSht_sell_price()) ;//折后价
		double money = NumberUtil.buildMoney(money_decimal, money_type, price*temp.getSht_amount());//折后金额
		temp.setSht_price(price);
		temp.setSht_money(money);
		temp.setSht_vip_money(0d);//会员让利金额
		temp.setSht_sale_money(0d);//未启用折上折，会员打折时，促销金额为0
		temp.setSht_hand_money(0d);//未启用折上折，会员打折时，手动让利金额为0
		temp.setSht_vipvalue(0d);
		temp.setSht_handtype(0);//把手动折扣的记录去掉
		temp.setSht_handvalue(0d);
	}
	
	/**
	 * 会员打折页面功能
	 * 会员折上折的功能，先进行手动打折的再进行会员折扣的
	 * */
	public static void buildVipDoubleRate(T_Sell_Shop_Temp temp,String money_decimal,String money_type,double vip_rate){
		double sell_money = temp.getSht_sell_price()*temp.getSht_amount();//零售价
		if(1 != temp.getSht_handtype()){//让利或实际金额
			double money = NumberUtil.buildMoney(money_decimal, money_type, (sell_money-temp.getSht_hand_money())*vip_rate);//折后金额
			double price = NumberUtil.buildMoney(money_decimal, money_type, money/temp.getSht_amount()) ;//折后价
			temp.setSht_price(price);
			temp.setSht_money(money);
			temp.setSht_vip_money(NumberUtil.buildMoney(money_decimal, money_type, sell_money-money));//会员让利金额
			temp.setSht_sale_money(0d);//未启用折上折，会员打折时，促销金额为0
			temp.setSht_vipvalue(vip_rate);
		}else{//打折
			double money = NumberUtil.buildMoney(money_decimal, money_type, sell_money*temp.getSht_handvalue()*vip_rate);//折后金额
			double price = NumberUtil.buildMoney(money_decimal, money_type, money/temp.getSht_amount()) ;//折后价
			temp.setSht_price(price);
			temp.setSht_money(money);
			temp.setSht_vip_money(NumberUtil.buildMoney(money_decimal, money_type, sell_money-money));//会员让利金额
			temp.setSht_sale_money(0d);//未启用折上折，会员打折时，促销金额为0
			temp.setSht_vipvalue(vip_rate);
		}
	}
	@SuppressWarnings("unchecked")
	public static Map<String, Object> buildProductInput(Map<String,Object> map){
		List<T_Buy_Product> inputs = (List<T_Buy_Product>)map.get("inputs");
		List<T_Stock_Data> stocks = (List<T_Stock_Data>)map.get("stocks");
		List<T_Base_Size> sizes = (List<T_Base_Size>)map.get("sizes");
		Map<String, Object> item = null;
		List<Map<String, Object>> rows = new ArrayList<Map<String,Object>>();
		int rowIndex=1;
		for (T_Buy_Product product : inputs) {
			item = new HashMap<String, Object>();
			item.put("id", StringUtil.trimString(rowIndex));rowIndex++;
			item.put("cr_code", product.getCr_code());
			item.put("cr_name", product.getCr_name());
			item.put("br_code", product.getBr_code());
			item.put("br_name", product.getBr_name());
			int amountStock=0;
			for (T_Base_Size size : sizes) {
				for (int i = 0; i <stocks.size(); i++) {
					T_Stock_Data stock=stocks.get(i);
					if (product.getCr_code().equals(stock.getSd_cr_code())
							&& size.getSz_code().equals(stock.getSd_sz_code())
							&& product.getBr_code().equals(stock.getSd_br_code())) {
						amountStock = stock.getSd_amount();
						stocks.remove(stock);
						break;
					}
				}
				if (amountStock != 0) {
					item.put(size.getSz_code(), amountStock+"/");
				}
				amountStock = 0;
			}
			rows.add(item);
		}
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("columns", sizes);
		resultMap.put("rows", rows);
		return resultMap;
	}
	public static List<T_Sell_DepositList> buildDepositList(Map<String,Object> param,List<T_Sell_Shop_Temp> tempList){
		List<T_Sell_DepositList> list = null;
		if(null != tempList && tempList.size() > 0){
			Object companyid = param.get(CommonUtil.COMPANYID);
			Object emp_code = param.get(CommonUtil.EMP_CODE);
			Object shop_code = param.get(CommonUtil.SHOP_CODE);
			String number = StringUtil.trimString(param.get("number"));
			list = new ArrayList<T_Sell_DepositList>();
			for(T_Sell_Shop_Temp temp:tempList){
				T_Sell_DepositList deposit = new T_Sell_DepositList();
				deposit.setCompanyid(Integer.parseInt(StringUtil.trimString(companyid)));
				deposit.setSdl_amount(temp.getSht_amount());
				deposit.setSdl_area(temp.getSht_da_code());
				deposit.setSdl_bd_code(temp.getSht_bd_code());
				deposit.setSdl_br_code(temp.getSht_br_code());
				deposit.setSdl_cost_price(temp.getSht_cost_price());
				deposit.setSdl_cr_code(temp.getSht_cr_code());
				deposit.setSdl_em_code(StringUtil.trimString(emp_code));
				deposit.setSdl_hand_money(temp.getSht_hand_money());
				deposit.setSdl_isgift(temp.getSht_isgift());
				deposit.setSdl_ispoint(temp.getSht_ispoint());
				deposit.setSdl_main(temp.getSht_main());
				deposit.setSdl_money(temp.getSht_money());
				deposit.setSdl_number(number);
				deposit.setSdl_pd_code(temp.getSht_pd_code());
				deposit.setSdl_price(temp.getSht_price());
				deposit.setSdl_remark(temp.getSht_remark());
				deposit.setSdl_sale_code(temp.getSht_sale_code());
				deposit.setSdl_sale_model(temp.getSht_sale_model());
				deposit.setSdl_sale_money(temp.getSht_sale_money());
				deposit.setSdl_sell_price(temp.getSht_sell_price());
				deposit.setSdl_shop_code(StringUtil.trimString(shop_code));
				deposit.setSdl_sign_price(temp.getSht_sign_price());
				deposit.setSdl_slave(temp.getSht_slave());
				deposit.setSdl_state(0);
				deposit.setSdl_sub_code(temp.getSht_sub_code());
				deposit.setSdl_sz_code(temp.getSht_sz_code());
				deposit.setSdl_tp_code(temp.getSht_tp_code());
				deposit.setSdl_vip_code(temp.getSht_vip_code());
				deposit.setSdl_vip_money(temp.getSht_vip_money());
				deposit.setSdl_upcost_price(temp.getSht_upcost_price());
				list.add(deposit);
				deposit = null;
			}
		}
		return list;
	}
	@SuppressWarnings("unchecked")
	public static void list2Map(Map<String,Object> paramMap){
		Map<String,Object> printDataMap = null;
		List<T_Sell_PrintData> printDatas = (List<T_Sell_PrintData>)(paramMap.get("datas"));
		try {
			printDataMap = new HashMap<String, Object>(printDatas.size());
			Map<String,Object> subDataMap = null;
			for(T_Sell_PrintData printData:printDatas){
				subDataMap = new HashMap<String, Object>(2);
				subDataMap.put(PrintUtil.WIDTH, printData.getSpd_width());
				subDataMap.put(PrintUtil.ALIGN, printData.getSpd_align());
				printDataMap.put(printData.getSpd_code(), subDataMap);
			}
			paramMap.put("printData", printDataMap);
		} catch (Exception e) {
		}
	}
	
	@SuppressWarnings("unchecked")
	private static String buildHeader(Map<String,Object> paramMap,T_Sell_Shop sell,
			T_Vip_Member vip,T_Base_Shop shop,T_Sell_Print print){
		StringBuffer headHtml = new StringBuffer("");
		List<T_Sell_PrintField> printFields = (List<T_Sell_PrintField>)paramMap.get("fields");
		headHtml.append("<table cellpadding=\"0\" cellspacing=\"0\" class=\"head-print\">");
		headHtml.append("<tbody>");
		headHtml.append("<tr>");
		headHtml.append("<td colspan=\"3\" class=\"head-title\"><b class=\"head-font\">"+print.getSp_title()+"</b></td>");
		headHtml.append("</tr>");
		int index = -1;
		for(T_Sell_PrintField field:printFields){
			if(field.getSpf_show() == 1){
				if(PrintUtil.SellPrint_Field.VIP_CODE.equals(field.getSpf_code()) || PrintUtil.SellPrint_Field.VIP_NAME.equals(field.getSpf_code())
						||PrintUtil.SellPrint_Field.VIP_TEL.equals(field.getSpf_code())||PrintUtil.SellPrint_Field.VIP_POINT.equals(field.getSpf_code())){
					if(null == vip || StringUtil.isEmpty(vip.getVm_cardcode())){
						continue;
					}
				}
				if(field.getSpf_position() == 0){
					String colspan = "";
					if(index != field.getSpf_line()){
						if(index == -1){
							headHtml.append("<tr>");
						}else{
							headHtml.append("</tr><tr>");
						}
					}
					if(field.getSpf_colspan() > 1){
						colspan = "colspan='"+field.getSpf_colspan()+"'";
					}
					if(PrintUtil.SellPrint_Field.NUMBER.equals(field.getSpf_code())){
						headHtml.append("<td "+colspan+">").append(field.getSpf_name()).append(":").append(sell.getSh_number()).append("</td>");
					}
					if(PrintUtil.SellPrint_Field.CASHDATE.equals(field.getSpf_code())){
						headHtml.append("<td "+colspan+">").append(field.getSpf_name()).append(":").append(sell.getSh_date()).append("</td>");
					}
					if(PrintUtil.SellPrint_Field.CASHMAN.equals(field.getSpf_code())){
						headHtml.append("<td "+colspan+">").append(field.getSpf_name()).append(":").append(sell.getEm_name()).append("</td>");
					}
					if(PrintUtil.SellPrint_Field.GUIDE.equals(field.getSpf_code())){
						headHtml.append("<td "+colspan+">").append(field.getSpf_name()).append(":").append(sell.getSh_guide()).append("</td>");
					}
					if(PrintUtil.SellPrint_Field.SELL_MONEY.equals(field.getSpf_code())){
						headHtml.append("<td "+colspan+">").append(field.getSpf_name()).append(":").append(sell.getSh_money()).append("</td>");
					}
					if(PrintUtil.SellPrint_Field.REAL_MONEY.equals(field.getSpf_code())){
						headHtml.append("<td "+colspan+">").append(field.getSpf_name()).append(":").append(sell.getSh_cash()+sell.getSh_change_money()).append("</td>");
					}
					if(PrintUtil.SellPrint_Field.LOST_MONEY.equals(field.getSpf_code())){
						headHtml.append("<td "+colspan+">").append(field.getSpf_name()).append(":").append(sell.getSh_lost_money()).append("</td>");
					}
					if(PrintUtil.SellPrint_Field.CHANGE_MONEY.equals(field.getSpf_code())){
						headHtml.append("<td "+colspan+">").append(field.getSpf_name()).append(":").append(sell.getSh_change_money()).append("</td>");
					}
					if(PrintUtil.SellPrint_Field.CD_MONEY.equals(field.getSpf_code())){
						headHtml.append("<td "+colspan+">").append(field.getSpf_name()+":").append(sell.getSh_cd_money()).append("</td>");
					}
					if(PrintUtil.SellPrint_Field.VC_MONEY.equals(field.getSpf_code())){
						headHtml.append("<td "+colspan+">").append(field.getSpf_name()+":").append(sell.getSh_vc_money()).append("</td>");
					}
					if(PrintUtil.SellPrint_Field.EC_MONEY.equals(field.getSpf_code())){
						headHtml.append("<td "+colspan+">").append(field.getSpf_name()+":").append(sell.getSh_ec_money()).append("</td>");
					}
					if(null != vip && !StringUtil.isEmpty(vip.getVm_cardcode())){
						if(PrintUtil.SellPrint_Field.VIP_CODE.equals(field.getSpf_code())){
							headHtml.append("<td "+colspan+">").append(field.getSpf_name()).append(":").append(StringUtil.addPass(vip.getVm_cardcode(),"card")).append("</td>");
						}
						if(PrintUtil.SellPrint_Field.VIP_NAME.equals(field.getSpf_code())){
							headHtml.append("<td "+colspan+">").append(field.getSpf_name()).append(":").append(StringUtil.addPass(vip.getVm_name(),"name")).append("</td>");
						}
						if(PrintUtil.SellPrint_Field.VIP_TEL.equals(field.getSpf_code())){
							headHtml.append("<td "+colspan+">").append(field.getSpf_name()).append(":").append(StringUtil.addPass(vip.getVm_mobile(),"mobile")).append("</td>");
						}
						if(PrintUtil.SellPrint_Field.VIP_POINT.equals(field.getSpf_code())){
							headHtml.append("<td "+colspan+">").append(field.getSpf_name()).append(":").append(vip.getVm_points()).append("</td>");
						}
					}
					if(PrintUtil.SellPrint_Field.PRINT_TIME.equals(field.getSpf_code())){
						headHtml.append("<td "+colspan+">").append(field.getSpf_name()+":").append(DateUtil.getCurrentTime()).append("</td>");
					}
					index = field.getSpf_line();
				}
			}
		}
		headHtml.append("</tr></tbody></table>");
		return headHtml.toString();
	}
	
	@SuppressWarnings("unchecked")
	private static String buildFooter(Map<String,Object> paramMap,T_Sell_Shop sell,
			T_Vip_Member vip,T_Base_Shop shop,T_Sell_Print print){
		StringBuffer footHtml = new StringBuffer("");
		List<T_Sell_PrintField> printFields = (List<T_Sell_PrintField>)paramMap.get("fields");
		footHtml.append("<table cellpadding=\"0\" cellspacing=\"0\" class=\"head-print\">");
		footHtml.append("<tbody>");
		int index = -1;
		for(T_Sell_PrintField field:printFields){
			if(field.getSpf_show() == 1){
				if(field.getSpf_position() == 1){
					String colspan = "";
					if(index != field.getSpf_line()){
						if(index == -1){
							footHtml.append("<tr>");
						}else{
							footHtml.append("</tr><tr>");
						}
					}
					if(field.getSpf_colspan() > 1){
						colspan = "colspan='"+field.getSpf_colspan()+"'";
					}
					if(PrintUtil.SellPrint_Field.SHOP_NAME.equals(field.getSpf_code())){
						footHtml.append("<td "+colspan+">").append(field.getSpf_name()).append(":").append(shop.getSp_name()).append("</td>");
					}
					if(PrintUtil.SellPrint_Field.SHOP_TEL.equals(field.getSpf_code())){
						footHtml.append("<td "+colspan+">").append(field.getSpf_name()).append(":").append(shop.getSpi_tel()).append("</td>");
					}
					if(PrintUtil.SellPrint_Field.SHOP_MOBILE.equals(field.getSpf_code())){
						footHtml.append("<td "+colspan+">").append(field.getSpf_name()).append(":").append(shop.getSpi_mobile()).append("</td>");
					}
					if(PrintUtil.SellPrint_Field.ADDRESS.equals(field.getSpf_code())){
						footHtml.append("<td "+colspan+">").append(field.getSpf_name()).append(":").append(shop.getSpi_addr()).append("</td>");
					}
					if(PrintUtil.SellPrint_Field.REMARK.equals(field.getSpf_code())){
						footHtml.append("<td "+colspan+">").append(field.getSpf_name()).append(":").append(print.getSp_remark()).append("</td>");
					}
					index = field.getSpf_line();
				}
			}
		}
		footHtml.append("</tr></tbody></table>");
		return footHtml.toString();
	}
	private static String buildEmp(List<T_Sell_ShopList> list){
		StringBuffer name = new StringBuffer();
		for(T_Sell_ShopList item:list){
			if((","+name.toString()).indexOf(","+item.getMain_name()+",") > -1){
				continue;
			}else{
				name.append(item.getMain_name()).append(",");
			}
		}
		if(null != name && name.length() > 0){
			name.deleteCharAt(name.length()-1);
		}
		return name.toString();
	}
	
	@SuppressWarnings("unchecked")
	public static Map<String,Object> buildPrint(List<T_Sell_ShopList> list,T_Sell_Shop sell,
			Map<String,Object> printData,T_Base_Shop shop,T_Vip_Member vip){
		Map<String,Object> htmlMap = null;
		
		try{
			if(!printData.containsKey("print")){
				throw new IllegalArgumentException("没有打印设置，请先设置!");
			}
			T_Sell_Print print = (T_Sell_Print)printData.get("print");
			list2Map(printData);
			Map<String, Object> printSet = (Map<String, Object>)printData.get("sets");
			String emp_name = buildEmp(list);
			sell.setSh_guide(emp_name);
			//获取表头与表尾的数据
			String headerHtml = buildHeader(printData,sell,vip,shop,print);
			String footer = buildFooter(printData, sell, vip, shop, print);
			String table = buildTable(printData,list);
			htmlMap = new HashMap<String, Object>(5);
			htmlMap.put("html", defaultStyleHTML(printData)+headerHtml+table+footer);
			htmlMap.put("number", sell.getSh_number());
			htmlMap.putAll(printSet);
		}catch(Exception e){
			e.printStackTrace();
		}
		return htmlMap;
	}
	
	private static String buildTable(Map<String,Object> printData,List<T_Sell_ShopList> list){
		@SuppressWarnings("unchecked")
		List<T_Sell_PrintData> printDatas = (List<T_Sell_PrintData>)(printData.get("datas"));
		StringBuffer html = new StringBuffer("");
		html.append("<table cellpadding=\"4\" cellspacing=\"0\" class=\"data-print\">");
		html.append("<thead>");
		html.append("<tr>");
		
		for(T_Sell_PrintData prints:printDatas){
			if(prints.getSpd_show() == 1){
				html.append("<th>");
				html.append("<div style=\"width='"+prints.getSpd_width()+"mm'\">");
				html.append(prints.getSpd_name());
				html.append("</div>");
				html.append("</th>");
			}
		}
		html.append("</tr>");
		html.append("</thead>");
		int amount = 0;
		double unitMoney = 0d;
		html.append("<tbody>");			
		for(T_Sell_ShopList item: list){
			html.append("<tr>");
			for (int j = 0; j < printDatas.size(); j++) {
				T_Sell_PrintData _printData = printDatas.get(j);
				if(_printData.getSpd_show() == 1){
					if(PrintUtil.SellPrint_Data.PD_NAME.equals(_printData.getSpd_code())){
						html.append("<td").append(dataStyle(_printData)).append(">");
						html.append(item.getPd_no()+"#"+item.getPd_name()+"</td>");
					}
					if(PrintUtil.SellPrint_Data.PD_STYLE.equals(_printData.getSpd_code())){
						html.append("<td").append(dataStyle(_printData)).append(">");
						html.append(item.getCr_name()+"/"+item.getSz_name());
						if(!StringUtil.isEmpty(item.getBr_name())){
							html.append("/"+item.getBr_name());
						}
						html.append("</td>");
					}
					if(PrintUtil.PrintData.AMOUNT.equals(_printData.getSpd_code())){
						html.append("<td").append(dataStyle(_printData)).append(">");
						html.append(item.getShl_amount()+"</td>");
						amount += item.getShl_amount();
					}
					if(PrintUtil.SellPrint_Data.SIGN_PRICE.equals(_printData.getSpd_code())){
						html.append("<td").append(dataStyle(_printData)).append(">");
						html.append(item.getShl_sign_price()+"</td>");
					}
					if(PrintUtil.SellPrint_Data.SELL_PRICE.equals(_printData.getSpd_code())){
						html.append("<td").append(dataStyle(_printData)).append(">");
						html.append(item.getShl_sell_price()+"</td>");
					}
					if(PrintUtil.SellPrint_Data.PRICE.equals(_printData.getSpd_code())){
						html.append("<td").append(dataStyle(_printData)).append(">");
						html.append(item.getShl_price()+"</td>");
					}
					if(PrintUtil.SellPrint_Data.MONEY.equals(_printData.getSpd_code())){
						html.append("<td").append(dataStyle(_printData)).append(">");
						html.append(item.getShl_money()+"</td>");
						unitMoney = Arith.add(unitMoney, item.getShl_money());
					}
				}
			}
			html.append("</tr>");
		}
		html.append("</tbody>");
		html.append("<tfoot class=\"foot\">");
		//列表模式下当前页合计算输出
		html.append("<tr>");
		for (int j = 0; j < printDatas.size(); j++) {
			if(printDatas.get(j).getSpd_show() == 1){
				if(PrintUtil.SellPrint_Data.PD_NAME.equals(printDatas.get(j).getSpd_code())){
					html.append("<td>小计：</td>");
				}
				if(PrintUtil.SellPrint_Data.PD_STYLE.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.AMOUNT.equals(printDatas.get(j).getSpd_code())){
					html.append("<td style='text-align:right;'>"+amount+"</td>");
				}
				if(PrintUtil.SellPrint_Data.SIGN_PRICE.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.SellPrint_Data.SELL_PRICE.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.SellPrint_Data.PRICE.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.SellPrint_Data.MONEY.equals(printDatas.get(j).getSpd_code())){
					html.append("<td style='text-align:right;'>"+unitMoney+"</td>");
				}
			}
		}
		html.append("</tr>");
		html.append("</tfoot>");
		html.append("</table>");
		return html.toString();
	}
	public static String dataStyle(T_Sell_PrintData data){
		StringBuffer html = new StringBuffer();
		html.append(" style='text-align:"+PrintUtil.doAlign(""+data.getSpd_align())+";min-width:"+data.getSpd_width()+"mm;'");
		return html.toString();
	}
	private static String defaultStyleHTML(Map<String,Object> paramMap){
		StringBuffer styleHtml = new StringBuffer();
		try {
			double page_left = 0d;
			double page_right = 0d;
			double page_top = 0d;
			double page_bottom = 0d;
			styleHtml.append("<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" </head>");
			styleHtml.append("<style>");
			styleHtml.append(" body{padding-left:"+page_left+"mm;padding-right:"+page_right+"mm;padding-top:"+page_top+"mm;padding-bottom:"+page_bottom+"mm;}");
			styleHtml.append(" table{border-collapse:collapse;}");
			styleHtml.append(" p{margin:0 auto;line-height:18px;}");
			styleHtml.append(" .head-title{text-align:center;height:6mm;}");
			styleHtml.append(" .head-font{font-size:16px;");
			styleHtml.append(" font-family:\"楷体\",Arial,Helvetica,sans-serif;}");
			styleHtml.append(" .head-print{border:0;width:100%;}");
			styleHtml.append(" .head-print tr td{width:33%;height:4.5mm;");
			styleHtml.append(" font-size:14px;");
			styleHtml.append(" font-family:\"宋体\",Arial,Helvetica,sans-serif;}");
			styleHtml.append(" .data-print{width:100%;font-size:12px;border:#000 solid 1px;border-width:1px 0 0 0px;}");
			styleHtml.append(" .data-print th{height:5mm;font-weight:700;border:#000 solid 1px;border-width:0 0px 1px 0px;}");
			styleHtml.append(" .data-print td{line-height:6mm;font-size:12px;}");
			styleHtml.append(" .foot tr td{border:#000 solid 1px;border-width:1px 0px 1px 0px;}");
			styleHtml.append("</style>");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return styleHtml.toString();
	}
	
	public static List<String> listToString(List<T_Sell_ShopList> list){
		List<String> _list = new ArrayList<String>();
		if(null != list && list.size() > 0){
			for(T_Sell_ShopList item:list){
				_list.add(item.getShl_pd_code());
			}
		}
		return _list;
	}
	
	public static List<String> listToSubString(List<T_Sell_ShopList> list){
		List<String> _list = new ArrayList<String>();
		if(null != list && list.size() > 0){
			for(T_Sell_ShopList item:list){
				_list.add(item.getShl_sub_code());
			}
		}
		return _list;
	}
	
	public static void buildStock(List<T_Sell_ShopList> list,List<T_Stock_Data> datas){
		if(null != list && list.size() > 0 && null != datas && datas.size() > 0){
			for(T_Stock_Data data:datas){
				for(T_Sell_ShopList item:list){
					if(item.getShl_pd_code().equals(data.getSd_pd_code())){
						item.setSd_amount(data.getSd_amount());
					}
				}
			}
		}
	}
	
	public static void buildSubStock(List<T_Sell_ShopList> list,List<T_Stock_Data> datas){
		if(null != list && list.size() > 0 && null != datas && datas.size() > 0){
			for(T_Stock_Data data:datas){
				for(T_Sell_ShopList item:list){
					if(item.getShl_sub_code().equals(data.getSd_code())){
						item.setSd_amount(data.getSd_amount());
						item.setSd_date(data.getSd_date());
//						item.setShl_shop_code(data.getShop_code());
					}
				}
			}
		}
	}
	public static void main(String[] args) {
		double price = 0.95d,b = 1;
		double amount = 3.89d,a = 2;
		double c = Arith.mul(price, b);
		System.out.println(c);
		double d = Arith.mul(amount, a);
		System.out.println(d);
		double money = Arith.add(c, d);
		System.out.println(money);
	}
}
