package zy.vo.vip;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import zy.entity.vip.rate.T_Vip_Brand_Rate;
import zy.entity.vip.rate.T_Vip_Type_Rate;
import zy.entity.vip.set.T_Vip_AgeGroupSetUp;
import zy.util.CommonUtil;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class VipVO {
	public static void buildBrand(Map<String,Object> param){
		String data = (String)param.get("data");
		String bd_code = (String)param.get("bd_code");
		Integer companyid = (Integer)param.get(CommonUtil.COMPANYID);
		List<T_Vip_Brand_Rate> list = new ArrayList<T_Vip_Brand_Rate>();
		T_Vip_Brand_Rate item = null;
		JSONArray jsonArray=JSONArray.parseArray(data);
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject json = jsonArray.getJSONObject(i);
			item = new T_Vip_Brand_Rate();
			item.setBr_bd_code(bd_code);
			item.setBr_mt_code(json.getString("br_mt_code"));
			item.setBr_rate(json.getDouble("br_rate"));
			item.setCompanyid(companyid);
			list.add(item);
			item = null;
		}
		param.put("brandList", list);
	}
	public static void buildType(Map<String,Object> param){
		String data = (String)param.get("data");
		String tp_code = (String)param.get("tp_code");
		Integer companyid = (Integer)param.get(CommonUtil.COMPANYID);
		List<T_Vip_Type_Rate> list = new ArrayList<T_Vip_Type_Rate>();
		T_Vip_Type_Rate item = null;
		JSONArray jsonArray=JSONArray.parseArray(data);
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject json = jsonArray.getJSONObject(i);
			item = new T_Vip_Type_Rate();
			item.setTr_tp_code(tp_code);
			item.setTr_mt_code(json.getString("tr_mt_code"));
			item.setTr_rate(json.getDouble("tr_rate"));
			item.setCompanyid(companyid);
			list.add(item);
			item = null;
		}
		param.put("typeList", list);
	}
	
	public static String getMonthCompareColumnVO(Map<String, List<Double>> result){
		StringBuffer vo = new StringBuffer();
		vo.append(" chart: {type: 'column',margin: 75,options3d: {enabled: true,alpha: 0,beta: 0,depth: 50,viewDistance: 25}}, ");
		vo.append(" title: {text: '会员消费同比'}, ");
		vo.append(" xAxis: {categories: ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月']}, ");
		vo.append(" yAxis: {min: 0,title: {text: '金额(元)'}}, ");
		vo.append(" tooltip: {");
		vo.append(" headerFormat: '<span style=\"font-size:13px;font-weight:bold\">{point.key}</br></span>',");
		vo.append(" pointFormat: '<b>{series.name}</b>:<b>{point.y:.2f}</b>'}, ");
		vo.append(" plotOptions: {column: {pointPadding: 0.2,borderWidth: 0}}, ");
		vo.append(" series: [ ");
		vo.append(" { ");
		vo.append(" name: ' 同期消费', ");
		List<Double> list1 = (List<Double>)result.get("1");
		List<Double> list2 = (List<Double>)result.get("2");
		List<Double> list3 = (List<Double>)result.get("3");
		List<Double> list4 = (List<Double>)result.get("4");
		List<Double> list5 = (List<Double>)result.get("5");
		List<Double> list6 = (List<Double>)result.get("6");
		List<Double> list7 = (List<Double>)result.get("7");
		List<Double> list8 = (List<Double>)result.get("8");
		List<Double> list9 = (List<Double>)result.get("9");
		List<Double> list10 = (List<Double>)result.get("10");
		List<Double> list11 = (List<Double>)result.get("11");
		List<Double> list12 = (List<Double>)result.get("12");
		vo.append(" data: [");
		vo.append(list1 !=null?list1.get(0):0);
		vo.append(", ");
		vo.append(list2 !=null?list2.get(0):0);
		vo.append(", ");
		vo.append(list3 !=null?list3.get(0):0);
		vo.append(", ");
		vo.append(list4 !=null?list4.get(0):0);
		vo.append(", ");
		vo.append(list5 !=null?list5.get(0):0);
		vo.append(", ");
		vo.append(list6 !=null?list6.get(0):0);
		vo.append(", ");
		vo.append(list7 !=null?list7.get(0):0);
		vo.append(", ");
		vo.append(list8 !=null?list8.get(0):0);
		vo.append(", ");
		vo.append(list9 !=null?list9.get(0):0);
		vo.append(", ");
		vo.append(list10 !=null?list10.get(0):0);
		vo.append(", ");
		vo.append(list11 !=null?list11.get(0):0);
		vo.append(", ");
		vo.append(list12 !=null?list12.get(0):0);
		vo.append("], ");
		vo.append(" dataLabels: { ");
		vo.append(" enabled: false,rotation: -45,color: '#FFFFFF',align: 'right',x: 4,y: 10, ");
		vo.append(" style: { ");
		vo.append(" fontSize: '10px',fontFamily: 'Verdana, sans-serif',textShadow: '0 0 3px black'} ");
		vo.append(" } ");
		vo.append(" }, ");
		vo.append(" { ");
		vo.append(" name: ' 本期消费', ");
		vo.append(" data: [");
		vo.append(list1 !=null?list1.get(1):0);
		vo.append(", ");
		vo.append(list2 !=null?list2.get(1):0);
		vo.append(", ");
		vo.append(list3 !=null?list3.get(1):0);
		vo.append(", ");
		vo.append(list4 !=null?list4.get(1):0);
		vo.append(", ");
		vo.append(list5 !=null?list5.get(1):0);
		vo.append(", ");
		vo.append(list6 !=null?list6.get(1):0);
		vo.append(", ");
		vo.append(list7 !=null?list7.get(1):0);
		vo.append(", ");
		vo.append(list8 !=null?list8.get(1):0);
		vo.append(", ");
		vo.append(list9 !=null?list9.get(1):0);
		vo.append(", ");
		vo.append(list10 !=null?list10.get(1):0);
		vo.append(", ");
		vo.append(list11 !=null?list11.get(1):0);
		vo.append(", ");
		vo.append(list12 !=null?list12.get(1):0);
		vo.append("], color: '#ff0000',");
		
		vo.append(" dataLabels: { ");
		vo.append(" enabled: false,rotation: -70,color: '#FFFFFF',align: 'right',x: 4,y: 10, ");
		vo.append(" style: { ");
		vo.append(" fontSize: '10px',fontFamily: 'Verdana, sans-serif',textShadow: '0 0 3px black' ");
		vo.append(" } ");
		vo.append(" } ");
		vo.append(" }] ");
		
		return vo.toString();
	}
	
	public static String getVipageAnalysisColumnVO(List<T_Vip_AgeGroupSetUp> ageGroupSetUps){
		StringBuffer vo = new StringBuffer();
		vo.append(" chart: {type: 'column',margin: 75,options3d: {enabled: true,alpha: 0,beta: 0,depth: 50,viewDistance: 25}}, ");
		vo.append(" title: {text: '<span style=\"font-size:14px;font-weight:bold\">会员年龄分析</span>'}, ");
		vo.append(" xAxis: {categories: [");
		if(ageGroupSetUps != null && ageGroupSetUps.size()>0){
			for(int i=0;i<ageGroupSetUps.size();i++){
				vo.append("'").append(ageGroupSetUps.get(i).getAgs_beg_age()).append("~").append(ageGroupSetUps.get(i).getAgs_end_age()).append("'");
				if(i < ageGroupSetUps.size()-1){
					vo.append(", ");
				}
			}
		}
		vo.append("]}, ");
		vo.append(" yAxis: {min: 0,title: {text: '人数(个)'}}, ");
		vo.append(" tooltip: {");
		vo.append(" headerFormat: '<span style=\"font-size:13px;font-weight:bold\">{point.key}</br></span>',");
		vo.append(" pointFormat: '<b>岁总计</b><b>{point.y}人</b>'}, ");
		vo.append(" plotOptions: {column: {pointPadding: 0.2,borderWidth: 0}}, ");
		vo.append(" series: [ ");
		vo.append(" { ");
		vo.append(" name: '年龄段', ");
		vo.append(" data: [");
		if(ageGroupSetUps != null && ageGroupSetUps.size()>0){
			for(int i=0;i<ageGroupSetUps.size();i++){
				vo.append(ageGroupSetUps.get(i).getVip_count());
				if(i < ageGroupSetUps.size()-1){
					vo.append(", ");
				}
			}
		}
		vo.append("], ");
		vo.append(" dataLabels: { ");
		vo.append(" enabled: false,rotation: -45,color: '#FFFFFF',align: 'right',x: 4,y: 10, ");
		vo.append(" style: { ");
		vo.append(" fontSize: '10px',fontFamily: 'Verdana, sans-serif',textShadow: '0 0 3px black'} ");
		vo.append(" } ");
		vo.append(" }] ");
		return vo.toString();
	}
	
	public static String getVipageAnalysisListVO(List<T_Vip_AgeGroupSetUp> ageGroupSetUps){
		Double all_sell_moneyDouble = 0.0d;
		for(int i=0;i<ageGroupSetUps.size();i++){
			Double sh_sell_money = ageGroupSetUps.get(i).getSh_sell_money();
			if(sh_sell_money == null){
				sh_sell_money = 0.0d;
			}
			all_sell_moneyDouble += sh_sell_money;
		}
		StringBuffer vo = new StringBuffer();
		for(int i=0;i<ageGroupSetUps.size();i++){
			Integer ags_beg_age = ageGroupSetUps.get(i).getAgs_beg_age();
			Integer ags_end_age = ageGroupSetUps.get(i).getAgs_end_age();
			Integer vip_count = ageGroupSetUps.get(i).getVip_count();
			Double sh_sell_money = ageGroupSetUps.get(i).getSh_sell_money();
			if(sh_sell_money == null){
				sh_sell_money = 0.0d;
			}
			Double scale = 0d;
			if(all_sell_moneyDouble != 0){
				scale = (sh_sell_money/all_sell_moneyDouble)*100;
			}
			vo.append("<dl onclick=\"handle.clickTag('"+ags_beg_age+"','"+ags_end_age+"');\">");
			vo.append("<dt>"+ags_beg_age+ "~"+ags_end_age+"岁（"+vip_count+"人）</dt>");
			vo.append("<dd>月总消费："+sh_sell_money+"元</dd>");
			vo.append("<dd>消费占比："+scale+"%</dd>");
			vo.append("</dl>");
		}
		return vo.toString();
	}
}
