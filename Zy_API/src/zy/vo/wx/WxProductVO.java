package zy.vo.wx;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import zy.entity.base.shop.T_Base_Shop;
import zy.entity.wx.product.T_Wx_ProductShop;

public class WxProductVO {
	public static String buildShopTree(List<T_Base_Shop> list,List<T_Wx_ProductShop> existShops){
		Set<String> shopSet = new HashSet<String>();
		for (T_Wx_ProductShop shop : existShops) {
			shopSet.add(shop.getPs_shop_code());
		}
		StringBuffer treeHtml = new StringBuffer();
		treeHtml.append("[{id:0,name:'所有门店',open:true},");
		if(list != null && list.size() >0){
			for (T_Base_Shop node : list) {
				treeHtml.append("{id:'" + node.getSp_code() + "',pId:'0',name:'"+node.getSp_name()+"'");
				treeHtml.append(",open:false");
				if(shopSet.contains(node.getSp_code())){
					treeHtml.append(",checked:true");
				}
				treeHtml.append("},");
			}
		}
		treeHtml.deleteCharAt(treeHtml.length()-1);
		treeHtml.append("]");
		return treeHtml.toString();
	}
}
