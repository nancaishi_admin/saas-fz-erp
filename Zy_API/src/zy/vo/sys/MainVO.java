package zy.vo.sys;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import zy.entity.sys.menu.T_Sys_Menu;
import zy.entity.sys.role.T_Sys_Role;
import zy.util.StringUtil;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;


public class MainVO {
	public static String buildMenuLimit(List<T_Sys_Menu> menulist){
		Map<String,String> menuMap = new HashMap<String, String>();
		for(T_Sys_Menu menu:menulist){
			if("N".equals(menu.getMn_child())){
				menuMap.put(StringUtil.trimString(menu.getMn_code()), StringUtil.trimString(menu.getMn_limit()));
			}
		}
		return com.alibaba.fastjson.JSONObject.toJSONString(menuMap);
	}
	
	public static String buildRoleTree(List<T_Sys_Role> list){
		StringBuffer treeHtml = new StringBuffer();
		treeHtml.append("[{id:0,name:'所有角色',open:true},");
		if(list != null && list.size() >0){
			for (T_Sys_Role item : list) {
				treeHtml.append("{id:'" + item.getRo_code() + "',pId:'0',name:'"+item.getRo_name()+"'");
				treeHtml.append(",open:true");
				treeHtml.append("},");
			}
		}
		treeHtml.deleteCharAt(treeHtml.length()-1);
		treeHtml.append("]");
		return treeHtml.toString();
	}
	
	public static List<T_Sys_Menu> buildRoleMenu(String jsonStr){
		List<T_Sys_Menu> list =new ArrayList<T_Sys_Menu>();
		T_Sys_Menu item = null;
		JSONArray jsonArray=JSONArray.parseArray(jsonStr);
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			item = new T_Sys_Menu();
			item.setMn_id(jsonObject.getInteger("mn_id"));
			item.setMn_state(jsonObject.getInteger("mn_state"));
			item.setRm_limit(jsonObject.getString("rm_limit"));
			list.add(item);
		}
		return list;
	}
}
