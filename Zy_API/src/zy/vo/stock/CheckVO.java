package zy.vo.stock;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import zy.entity.base.size.T_Base_SizeList;
import zy.entity.stock.T_Stock_SizeCommon;
import zy.entity.stock.check.T_Stock_CheckList;
import zy.entity.sys.user.T_Sys_User;
import zy.util.StringUtil;
import zy.vo.common.SizeHorizontalVO;

public class CheckVO {
	
	private static List<T_Stock_SizeCommon> getSizeModeList(List<T_Stock_CheckList> list,int maxColNumArray, Map<String, List<String>> map) {
		// 尺码横排结果
		List<T_Stock_SizeCommon> lstRes = new ArrayList<T_Stock_SizeCommon>();
		int[] subArray = null;
		T_Stock_SizeCommon temp = null;
		int Amount_Total = 0;
		double UnitMoney_Total = 0d,retailMoney_Total = 0d;
		String pd_code = "";
		String cr_code = "";
		String br_code = "";
		List<String> sizeListWithSzgIdList = null;
		for (T_Stock_CheckList item : list) {
			if (!pd_code.equals(item.getCkl_pd_code())
					|| !cr_code.equals(item.getCkl_cr_code())
					|| !br_code.equals(item.getCkl_br_code())
					) {
				pd_code = StringUtil.trimString(item.getCkl_pd_code());
				cr_code = StringUtil.trimString(item.getCkl_cr_code());
				br_code = StringUtil.trimString(item.getCkl_br_code());
				Amount_Total = 0;
				UnitMoney_Total = 0d;
				retailMoney_Total = 0d;
				temp = new T_Stock_SizeCommon();
				subArray = new int[maxColNumArray];
				temp.setSingle_amount(subArray);
				lstRes.add(temp);
			}
			temp.setId(item.getCkl_id());
			temp.setPd_code(item.getCkl_pd_code());
			temp.setPd_no(item.getPd_no());
			temp.setPd_name(item.getPd_name());
			temp.setPd_unit(item.getPd_unit());
			temp.setBd_name(item.getBd_name());
			temp.setTp_name(item.getTp_name());
			temp.setPd_year(item.getPd_year());
			temp.setPd_season(item.getPd_season());
			temp.setSzg_code(item.getCkl_szg_code());
			temp.setCr_code(item.getCkl_cr_code());
			temp.setCr_name(item.getCr_name());
			temp.setBr_code(item.getCkl_br_code());
			temp.setBr_name(item.getBr_name());
			temp.setUnitprice(item.getCkl_unitprice());
			temp.setRetailprice(item.getCkl_retailprice());
			Amount_Total += item.getCkl_amount();
			UnitMoney_Total += item.getCkl_amount()*item.getCkl_unitprice();
			retailMoney_Total += item.getCkl_amount()*item.getCkl_retailprice();
			temp.setTotalamount(Amount_Total);
			temp.setUnitmoney(UnitMoney_Total);
			temp.setRetailmoney(retailMoney_Total);
			
			//获得尺码组编号对应的尺码编号信息
			sizeListWithSzgIdList = (List<String>)map.get(item.getCkl_szg_code());
			//然后根据对应的下标确定其位置
			int szIdIndex = sizeListWithSzgIdList.indexOf(item.getCkl_sz_code());
			//根据下标位置得到，其对应的尺码数量，然后进行累加
			subArray[szIdIndex] += item.getCkl_amount();
		}
		return lstRes;
	}

	public static Map<String, Object> getJsonSizeData(List<T_Base_SizeList> sizeGroupList,List<T_Stock_CheckList> dataList){
		if (sizeGroupList != null && sizeGroupList.size() > 0) {
			int[] maxColNumArray = new int[1];
            Map<String, List<String>> map = new HashMap<String, List<String>>();
            
            List<List<String>> lstRes = SizeHorizontalVO.getSizeModeTitleInfoList(map, maxColNumArray, sizeGroupList);
            List<T_Stock_SizeCommon> list = getSizeModeList(dataList,maxColNumArray[0],map);
            int sizeMaxSize = maxColNumArray[0];
			int[] totalSizeBySingle = new int[sizeMaxSize];
			Collections.sort(lstRes, new Comparator<List<String>>() {
				@Override
				public int compare(List<String> o1, List<String> o2) {
					return o1.size() > o2.size() ? -1 : 0;
				}
			});
			int Amount_Total = 0;
			double UnitMoney_Total = 0d,retailMoney_Total=0d;
			HashMap<String, Object> sizeGroupMap = new HashMap<String, Object>();
			List<Map<String, Object>> values = new ArrayList<Map<String, Object>>();
			HashMap<String, Object> entry = null;
			if (list == null || list.isEmpty()) {
			} else {
				for (T_Stock_SizeCommon item : list) {
					entry = new HashMap<String, Object>();
					entry.put("id", item.getId());
					entry.put("pd_code", item.getPd_code());
					entry.put("pd_no", item.getPd_no());
					entry.put("pd_name", item.getPd_name());
					entry.put("pd_unit", item.getPd_unit());
					entry.put("cr_code", item.getCr_code());
					entry.put("cr_name", item.getCr_name());
					entry.put("br_code", item.getBr_code());
					entry.put("br_name", item.getBr_name());
					int[] amounts = item.getSingle_amount();
					if (lstRes == null || lstRes.size() <= 0) {
					} else {
						String[] header = null;
						int amountSize = 0;
						if (map.containsKey(item.getSzg_code())) {
							amountSize = map.get(item.getSzg_code()).size();
						}
						List<String> headers = lstRes.get(0);
						for (int i = 0; i < headers.size(); i++) {
							header = headers.get(i).split("_AND_");
							if (i >= amountSize) {
								entry.put(header[0], "");
							} else {
								entry.put(header[0],amounts[i] == 0 ? "": amounts[i] + "");
								totalSizeBySingle[i] = totalSizeBySingle[i] + amounts[i];
							}
						}
					}
					entry.put("Amount_Total", item.getTotalamount());
					entry.put("unit_price", item.getUnitprice());
					entry.put("retail_price", item.getRetailprice());
					entry.put("unit_money", item.getUnitmoney());
					entry.put("retail_money", item.getRetailmoney());
					entry.put("sizeGroupCode", item.getSzg_code());
					Amount_Total += item.getTotalamount();
					UnitMoney_Total += item.getUnitmoney();
					retailMoney_Total += item.getRetailmoney();
					values.add(entry);
				}
			}
			if (lstRes == null || lstRes.size() <= 0) {
			} else {
				String[] header = null;
                List<String> headers = lstRes.get(0);
                for (int i = 0; i < headers.size(); i++) {
                    header = headers.get(i).split("_AND_");
                    sizeGroupMap.put(header[0], totalSizeBySingle[i] == 0 ? "" : totalSizeBySingle[i]);
                }
			}
			HashMap<String, Object> userData = new HashMap<String, Object>();
			userData.put("pd_no", "合计：");
			userData.put("Amount_Total", Amount_Total);
			userData.put("unit_money", UnitMoney_Total);
			userData.put("retail_money", retailMoney_Total);
			userData.putAll(sizeGroupMap);
			
			Map<String, Object> resultMap = new HashMap<String, Object>();
			resultMap.put("data", values);
			resultMap.put("userData", userData);
			return resultMap;
		}
		return null;
	}
	
	@SuppressWarnings("rawtypes")
	public static List<T_Stock_CheckList> convertMap2Model(Map<String, Object> data,T_Sys_User user,String ba_number,Integer ckl_isrepair){
		List<T_Stock_CheckList> resultList = new ArrayList<T_Stock_CheckList>();
		List products = (List)data.get("products");
		if (products == null || products.size() == 0) {
			return resultList;
		}
		T_Stock_CheckList model = null;
		for (int i = 0; i < products.size(); i++) {
			Map product = (Map)products.get(i);
			model = new T_Stock_CheckList();
			model.setCkl_pd_code(StringUtil.trimString(product.get("pd_code")));
			model.setCkl_cr_code(StringUtil.trimString(product.get("cr_code")));
			model.setCkl_sz_code(StringUtil.trimString(product.get("sz_code")));
			model.setCkl_szg_code(StringUtil.trimString(product.get("pd_szg_code")));
			model.setCkl_br_code(StringUtil.trimString(product.get("br_code")));
			model.setCkl_sub_code(model.getCkl_pd_code()+model.getCkl_cr_code()+model.getCkl_sz_code()+model.getCkl_br_code());
			model.setCkl_amount(Integer.parseInt(StringUtil.trimString(product.get("amount"))));
			model.setCkl_unitprice(Double.parseDouble(StringUtil.trimString(product.get("unitPrice"))));
			model.setCkl_retailprice(Double.parseDouble(StringUtil.trimString(product.get("retailPrice"))));
			model.setCkl_remark("");
			model.setCkl_us_id(user.getUs_id());
			model.setCompanyid(user.getCompanyid());
			model.setOperate_type(StringUtil.trimString(product.get("operate_type")));
			model.setCkl_ba_number(ba_number);
			model.setCkl_isrepair(ckl_isrepair);
			resultList.add(model);
		}
		return resultList;
	}
	
	@SuppressWarnings("rawtypes")
	public static List<String[]> convertMap2Model_import(Map<String, Object> data){
		List<String[]> resultList = new ArrayList<String[]>();
		List products = (List)data.get("datas");
		if (products == null || products.size() == 0) {
			return resultList;
		}
		String[] item = null;
		for (int i = 0; i < products.size(); i++) {
			Map product = (Map)products.get(i);
			item = new String[2];
			item[0] = StringUtil.trimString(product.get("barCode"));
			item[1] = StringUtil.trimString(product.get("amount"));
			resultList.add(item);
		}
		return resultList;
	}
	
}
