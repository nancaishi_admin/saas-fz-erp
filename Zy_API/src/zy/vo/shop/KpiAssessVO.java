package zy.vo.shop;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import zy.entity.shop.kpiassess.T_Shop_KpiAssessList;

public class KpiAssessVO {
	public static List<Map<String, Object>> buildDetailJson(List<T_Shop_KpiAssessList> kpiAssessLists){
		List<Map<String, Object>> resultList = new ArrayList<Map<String,Object>>();
		List<String> kiCodes = new ArrayList<String>();
		Map<String, List<T_Shop_KpiAssessList>> detailsMap = new HashMap<String, List<T_Shop_KpiAssessList>>();
		for (T_Shop_KpiAssessList item : kpiAssessLists) {
			if(!kiCodes.contains(item.getKal_ki_code())){
				kiCodes.add(item.getKal_ki_code());
			}
			if(!detailsMap.containsKey(item.getKal_ki_code())){
				detailsMap.put(item.getKal_ki_code(), new ArrayList<T_Shop_KpiAssessList>());
			}
			detailsMap.get(item.getKal_ki_code()).add(item);
		}
		for (String kiCode : kiCodes) {
			Map<String, Object> item = new HashMap<String, Object>();
			List<T_Shop_KpiAssessList> temps = detailsMap.get(kiCode);
			item.put("kal_ki_code", kiCode);
			item.put("ki_name", temps.get(0).getKi_name());
			for (T_Shop_KpiAssessList temp : temps) {
				item.put("id"+temp.getKal_code(), temp.getKal_id());
				item.put("complete"+temp.getKal_code(), temp.getKal_complete());
				item.put("score"+temp.getKal_code(), temp.getKal_score());
				item.put("reward"+temp.getKal_code(), temp.getReward_names());
				item.put("reward_codes"+temp.getKal_code(), temp.getReward_codes());
			}
			resultList.add(item);
		}
		return resultList;
	}
}
