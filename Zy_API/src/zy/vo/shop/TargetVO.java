package zy.vo.shop;

import java.util.ArrayList;
import java.util.List;

import zy.entity.shop.target.T_Shop_Target_Detail;
import zy.entity.shop.target.T_Shop_Target_DetailList;
import zy.entity.sys.user.T_Sys_User;
import zy.util.StringUtil;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class TargetVO {
	
	public static List<T_Shop_Target_Detail> convertJsonToTargetDetail(String jsonStr,T_Sys_User user){
		List<T_Shop_Target_Detail> list =new ArrayList<T_Shop_Target_Detail>();
		T_Shop_Target_Detail model;
		JSONArray jsonArray=JSONArray.parseArray(jsonStr);
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			model=new T_Shop_Target_Detail();
			model.setTad_em_code(StringUtil.trimString(jsonObject.getString("tad_em_code")));
			model.setTad_em_name(StringUtil.trimString(jsonObject.getString("tad_em_name")));
			model.setTad_target_money((double)jsonObject.getDouble("tad_target_money"));
			model.setTad_vip_count(jsonObject.getInteger("tad_vip_count"));
			model.setTad_remark(StringUtil.trimString(jsonObject.getString("tad_remark")));
			int tad_project_type = 0;
			if(jsonObject.getString("tad_project_type") != null 
					&& !"".equals(jsonObject.getString("tad_project_type"))){
				tad_project_type = jsonObject.getInteger("tad_project_type");
			}
			model.setTad_project_type(tad_project_type);
			model.setCompanyid(user.getCompanyid());
			list.add(model);
		}
		return list;
	}
	
	public static List<T_Shop_Target_DetailList> convertJsonToTargetDetailList(String jsonStr,T_Sys_User user){
		List<T_Shop_Target_DetailList> list =new ArrayList<T_Shop_Target_DetailList>();
		T_Shop_Target_DetailList model;
		JSONArray jsonArray=JSONArray.parseArray(jsonStr);
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			model=new T_Shop_Target_DetailList();
			model.setTadl_em_code(StringUtil.trimString(jsonObject.getString("em_code")));
			model.setTadl_project_code(StringUtil.trimString(jsonObject.getString("code")));
			model.setTadl_money(jsonObject.getDouble("tad_target_money"));
			model.setTadl_remark(StringUtil.trimString(jsonObject.getString("tadl_remark")));
			model.setCompanyid(user.getCompanyid());
			list.add(model);
		}
		return list;
	}
	
	public static List<T_Shop_Target_Detail> convertJsonToTargetDetailUpdate(String jsonStr,T_Sys_User user,String ta_number){
		List<T_Shop_Target_Detail> list =new ArrayList<T_Shop_Target_Detail>();
		T_Shop_Target_Detail model;
		JSONArray jsonArray=JSONArray.parseArray(jsonStr);
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			model=new T_Shop_Target_Detail();
			model.setTad_em_code(jsonObject.getString("tad_em_code"));
			model.setTad_real_money(jsonObject.getDouble("tad_real_money"));
			model.setTad_real_count(jsonObject.getInteger("tad_real_count"));
			model.setTad_ta_number(ta_number);
			model.setCompanyid(user.getCompanyid());
			list.add(model);
		}
		return list;
	}
}
