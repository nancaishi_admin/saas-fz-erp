package zy.vo.base;

import java.util.List;

import zy.entity.base.size.T_Base_SizeList;
import zy.util.StringUtil;

public class SizeVO {
	/**
	 * 查询尺码转换成HTML
	 * */
	public static String toSizeHtmlByList(List<T_Base_SizeList> list) throws Exception{
		StringBuffer html = new StringBuffer("");
		for(T_Base_SizeList  sizeList:list){
			html.append("<option value=\""+StringUtil.trimString(sizeList.getSzl_sz_code())+"\">");
			html.append("("+StringUtil.trimString(sizeList.getSzl_sz_code())+")");
			html.append(StringUtil.trimString(sizeList.getSz_name()));
			html.append("</option>");
		}
		return html.toString();
	}
}
