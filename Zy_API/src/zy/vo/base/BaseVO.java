package zy.vo.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import zy.entity.base.size.T_Base_SizeList;
import zy.entity.base.size.T_Base_Size_Group;
import zy.entity.base.type.T_Base_Type;
import zy.entity.sys.user.T_Sys_User;
import zy.util.CommonUtil;
import zy.util.StringUtil;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class BaseVO {
	
	public static String buildBaseTypeTree(List<T_Base_Type> list){
		StringBuffer treeHtml = new StringBuffer();
		treeHtml.append("[{id:0,name:'所有类别',open:true},");
		String nodes="";
		if(list != null && list.size() >0){
			nodes = (initBaseTypeToTree(list,CommonUtil.DEFAULT_UPCODE));
		}
		treeHtml.append(nodes);
		treeHtml.deleteCharAt(treeHtml.length()-1);
		treeHtml.append("]");
		return treeHtml.toString();
	}
	
	public static List<T_Base_Type> hasBaseTypeChild(List<T_Base_Type> list, String upcode) {
		  List<T_Base_Type> nodeList = new ArrayList<T_Base_Type>();
		  for (T_Base_Type comm : list) {
		   // 如果数据的父节点==Parent的值，则该数据为子节点，存入list<SysRole>
			   if (comm.getTp_upcode().equals(upcode)) {
				   nodeList.add(comm);
			   }
		  }
		  return nodeList;
	 }
	
	public static String initBaseTypeToTree(List<T_Base_Type> list, String rootCode){
		StringBuffer json = new StringBuffer(""); 
		List<T_Base_Type> nodeList = hasBaseTypeChild(list, rootCode); // 获得集合中的父节点下的子集list  mn_IsHaveChildMenuItem
		list.remove(nodeList);
		// 循环子集
		//json.append("{");
		for (T_Base_Type node : nodeList) {
			List<T_Base_Type> Nodes = hasBaseTypeChild(list, node.getTp_code());
			json.append("{id:'" + node.getTp_code() + "',pId:'"+node.getTp_upcode()+"',name:'"+node.getTp_name()+"'");
			if (Nodes.size() > 0) {
				json.append(",open:false");
			}
			json.append("},");
			// 查询全部集合下，是否存在该子集OID的子子集
			if (Nodes.size() > 0) {
				json.append(initBaseTypeToTree(list, node.getTp_code())); // 递归树，传入子子集，子集父节点
			}
		}
		return json.toString();
	}
	
	public static Map<String,Object> buildSizeGroup(String jsondata,T_Sys_User user){
		Map<String,Object> map = new HashMap<String,Object>();
		T_Base_Size_Group group = new T_Base_Size_Group();
		JSONObject jsonObj = JSONObject.parseObject(jsondata);
		List<T_Base_SizeList> tempList = new ArrayList<T_Base_SizeList>();
		T_Base_SizeList temp = null; 
		if(null != jsonObj && jsonObj.size() > 0){
			group.setCompanyid(user.getCompanyid());
			if(jsonObj.containsKey("szg_id")){
				group.setSzg_id(jsonObj.getInteger("szg_id"));
			}
			if(jsonObj.containsKey("szg_code")){
				group.setSzg_code(jsonObj.getString("szg_code"));
			}
			group.setSzg_name(StringUtil.trimString(jsonObj.getString("szg_name")));
			JSONArray jsonArr = jsonObj.getJSONArray("data");
			if(null != jsonArr && jsonArr.size() > 0){
				for(int i = 0; i < jsonArr.size(); i++) {
					JSONObject json = jsonArr.getJSONObject(i);
					temp = new T_Base_SizeList();
					if(null != group.getSzg_code() && !"".equals(group.getSzg_code())){
						temp.setSzl_szg_code(group.getSzg_code());
					}
					temp.setSzl_sz_code(StringUtil.trimString(json.getString("sz_code")));
					temp.setCompanyid(user.getCompanyid());
					temp.setSzl_order(i+2);
					tempList.add(temp);
					temp = null;
				}
			}
			group.setSzg_number(tempList.size());
		}
		map.put("size_group", group);
		map.put("size_list", tempList);
		return map;
	}
}
