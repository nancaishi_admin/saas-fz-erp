package zy.vo.sort;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import zy.entity.base.shop.T_Base_Shop;
import zy.entity.base.size.T_Base_SizeList;
import zy.entity.sort.allot.T_Sort_Allot;
import zy.entity.sort.allot.T_Sort_AllotList;
import zy.entity.sort.allot.T_Sort_SizeCommon;
import zy.entity.sys.print.T_Sys_Print;
import zy.entity.sys.print.T_Sys_PrintData;
import zy.entity.sys.print.T_Sys_PrintField;
import zy.entity.sys.user.T_Sys_User;
import zy.util.CommonUtil;
import zy.util.PriceLimitUtil;
import zy.util.PrintUtil;
import zy.util.StringUtil;
import zy.vo.common.PrintVO;
import zy.vo.common.SizeHorizontalVO;

public class AllotVO {
	private static List<T_Sort_SizeCommon> getSizeModeList(List<T_Sort_AllotList> list,int maxColNumArray, Map<String, List<String>> map,boolean sendAmount) {
		// 尺码横排结果
		List<T_Sort_SizeCommon> lstRes = new ArrayList<T_Sort_SizeCommon>();
		int[] subArray = null;
		T_Sort_SizeCommon temp = null;
		int Amount_Total = 0;
		double UnitMoney_Total = 0d;
		double CostMoney_Total = 0d;
		double SellMoney_Total = 0d;
		String pd_code = "";
		String cr_code = "";
		String br_code = "";
		List<String> sizeListWithSzgIdList = null;
		for (T_Sort_AllotList item : list) {
			if (!pd_code.equals(item.getAtl_pd_code())
					|| !cr_code.equals(item.getAtl_cr_code())
					|| !br_code.equals(item.getAtl_br_code())
					) {
				pd_code = StringUtil.trimString(item.getAtl_pd_code());
				cr_code = StringUtil.trimString(item.getAtl_cr_code());
				br_code = StringUtil.trimString(item.getAtl_br_code());
				Amount_Total = 0;
				UnitMoney_Total = 0d;
				CostMoney_Total = 0d;
				SellMoney_Total = 0d;
				temp = new T_Sort_SizeCommon();
				subArray = new int[maxColNumArray];
				temp.setSingle_amount(subArray);
				lstRes.add(temp);
			}
			temp.setId(item.getAtl_id());
			temp.setPd_code(item.getAtl_pd_code());
			temp.setPd_no(item.getPd_no());
			temp.setPd_name(item.getPd_name());
			temp.setPd_unit(item.getPd_unit());
			temp.setBd_name(item.getBd_name());
			temp.setTp_name(item.getTp_name());
			temp.setPd_year(item.getPd_year());
			temp.setPd_season(item.getPd_season());
			temp.setSzg_code(item.getAtl_szg_code());
			temp.setCr_code(item.getAtl_cr_code());
			temp.setCr_name(item.getCr_name());
			temp.setBr_code(item.getAtl_br_code());
			temp.setBr_name(item.getBr_name());
			temp.setUnitprice(item.getAtl_unitprice());
			temp.setCostprice(item.getAtl_costprice());
			temp.setSellprice(item.getAtl_sellprice());
			if(sendAmount){
				Amount_Total += item.getAtl_sendamount();
				UnitMoney_Total += item.getAtl_sendamount()*item.getAtl_unitprice();
				CostMoney_Total += item.getAtl_sendamount()*item.getAtl_costprice();
				SellMoney_Total += item.getAtl_sendamount()*item.getAtl_sellprice();
			}else {
				Amount_Total += item.getAtl_applyamount();
				UnitMoney_Total += item.getAtl_applyamount()*item.getAtl_unitprice();
				CostMoney_Total += item.getAtl_applyamount()*item.getAtl_costprice();
				SellMoney_Total += item.getAtl_applyamount()*item.getAtl_sellprice();
			}
			temp.setTotalamount(Amount_Total);
			temp.setUnitmoney(UnitMoney_Total);
			temp.setCostmoney(CostMoney_Total);
			temp.setSellmoney(SellMoney_Total);
			//获得尺码组编号对应的尺码编号信息
			sizeListWithSzgIdList = (List<String>)map.get(item.getAtl_szg_code());
			//然后根据对应的下标确定其位置
			int szIdIndex = sizeListWithSzgIdList.indexOf(item.getAtl_sz_code());
			//根据下标位置得到，其对应的尺码数量，然后进行累加
			if(sendAmount){
				subArray[szIdIndex] += item.getAtl_sendamount();
			}else {
				subArray[szIdIndex] += item.getAtl_applyamount();
			}
		}
		return lstRes;
	}
	
	public static Map<String, Object> getJsonSizeData(List<T_Base_SizeList> sizeGroupList,List<T_Sort_AllotList> dataList,boolean sendAmount){
		if (sizeGroupList != null && sizeGroupList.size() > 0) {
			int[] maxColNumArray = new int[1];
            Map<String, List<String>> map = new HashMap<String, List<String>>();
            List<List<String>> lstRes = SizeHorizontalVO.getSizeModeTitleInfoList(map, maxColNumArray, sizeGroupList);
            List<T_Sort_SizeCommon> list = getSizeModeList(dataList,maxColNumArray[0],map,sendAmount);
            int sizeMaxSize = maxColNumArray[0];
			int[] totalSizeBySingle = new int[sizeMaxSize];
			Collections.sort(lstRes, new Comparator<List<String>>() {
				@Override
				public int compare(List<String> o1, List<String> o2) {
					return o1.size() > o2.size() ? -1 : 0;
				}
			});
			int Amount_Total = 0;
			double UnitMoney_Total = 0d;
			double CostMoney_Total = 0d;
			double SellMoney_Total = 0d;
			HashMap<String, Object> sizeGroupMap = new HashMap<String, Object>();
			List<Map<String, Object>> values = new ArrayList<Map<String, Object>>();
			HashMap<String, Object> entry = null;
			if (list == null || list.isEmpty()) {
			} else {
				for (T_Sort_SizeCommon item : list) {
					entry = new HashMap<String, Object>();
					entry.put("id", item.getId());
					entry.put("pd_code", item.getPd_code());
					entry.put("pd_no", item.getPd_no());
					entry.put("pd_name", item.getPd_name());
					entry.put("pd_unit", item.getPd_unit());
					entry.put("cr_code", item.getCr_code());
					entry.put("cr_name", item.getCr_name());
					entry.put("br_code", item.getBr_code());
					entry.put("br_name", item.getBr_name());
					int[] amounts = item.getSingle_amount();
					if (lstRes == null || lstRes.size() <= 0) {
					} else {
						String[] header = null;
						int amountSize = 0;
						if (map.containsKey(item.getSzg_code())) {
							amountSize = map.get(item.getSzg_code()).size();
						}
						List<String> headers = lstRes.get(0);
						for (int i = 0; i < headers.size(); i++) {
							header = headers.get(i).split("_AND_");
							if (i >= amountSize) {
								entry.put(header[0], "");
							} else {
								entry.put(header[0],amounts[i] == 0 ? "": amounts[i] + "");
								totalSizeBySingle[i] = totalSizeBySingle[i] + amounts[i];
							}
						}
					}
					entry.put("Amount_Total", item.getTotalamount());
					entry.put("unit_price", item.getUnitprice());
					entry.put("unit_money", item.getUnitmoney());
					entry.put("cost_price", item.getCostprice());
					entry.put("cost_money", item.getCostmoney());
					entry.put("sell_price", item.getSellprice());
					entry.put("sell_money", item.getSellmoney());
					entry.put("sizeGroupCode", item.getSzg_code());
					Amount_Total += item.getTotalamount();
					UnitMoney_Total += item.getUnitmoney();
					CostMoney_Total += item.getCostmoney();
					SellMoney_Total += item.getSellmoney();
					values.add(entry);
				}
			}
			if (lstRes == null || lstRes.size() <= 0) {
			} else {
				String[] header = null;
                List<String> headers = lstRes.get(0);
                for (int i = 0; i < headers.size(); i++) {
                    header = headers.get(i).split("_AND_");
                    sizeGroupMap.put(header[0], totalSizeBySingle[i] == 0 ? "" : totalSizeBySingle[i]);
                }
			}
			HashMap<String, Object> userData = new HashMap<String, Object>();
			userData.put("pd_no", "合计：");
			userData.put("Amount_Total", Amount_Total);
			userData.put("unit_money", UnitMoney_Total);
			userData.put("cost_money", CostMoney_Total);
			userData.put("sell_money", SellMoney_Total);
			userData.putAll(sizeGroupMap);
			
			Map<String, Object> resultMap = new HashMap<String, Object>();
			resultMap.put("data", values);
			resultMap.put("userData", userData);
			return resultMap;
		}
		return null;
	}

	@SuppressWarnings("rawtypes")
	public static List<T_Sort_AllotList> convertMap2Model(Map<String, Object> data,Integer at_type,Integer allot_up,T_Sys_User user){
		List<T_Sort_AllotList> resultList = new ArrayList<T_Sort_AllotList>();
		List products = (List)data.get("products");
		if (products == null || products.size() == 0) {
			return resultList;
		}
		T_Sort_AllotList model = null;
		for (int i = 0; i < products.size(); i++) {
			Map product = (Map)products.get(i);
			model = new T_Sort_AllotList();
			model.setAtl_pd_code(StringUtil.trimString(product.get("pd_code")));
			model.setAtl_cr_code(StringUtil.trimString(product.get("cr_code")));
			model.setAtl_sz_code(StringUtil.trimString(product.get("sz_code")));
			model.setAtl_szg_code(StringUtil.trimString(product.get("pd_szg_code")));
			model.setAtl_br_code(StringUtil.trimString(product.get("br_code")));
			model.setAtl_sub_code(model.getAtl_pd_code()+model.getAtl_cr_code()+model.getAtl_sz_code()+model.getAtl_br_code());
			model.setAtl_applyamount(Integer.parseInt(StringUtil.trimString(product.get("amount"))));
			model.setAtl_unitprice(Double.parseDouble(StringUtil.trimString(product.get("unitPrice"))));
			model.setAtl_sellprice(Double.parseDouble(StringUtil.trimString(product.get("sellPrice"))));
			model.setAtl_costprice(Double.parseDouble(StringUtil.trimString(product.get("costPrice"))));
			model.setAtl_remark("");
			model.setAtl_type(at_type);
			model.setAtl_up(allot_up);
			model.setAtl_us_id(user.getUs_id());
			model.setCompanyid(user.getCompanyid());
			model.setOperate_type(StringUtil.trimString(product.get("operate_type")));
			resultList.add(model);
		}
		return resultList;
	}
	
	@SuppressWarnings("rawtypes")
	public static List<T_Sort_AllotList> convertMap2Model_Send(Map<String, Object> data,Integer at_type,String at_number,T_Sys_User user){
		List<T_Sort_AllotList> resultList = new ArrayList<T_Sort_AllotList>();
		List products = (List)data.get("products");
		if (products == null || products.size() == 0) {
			return resultList;
		}
		T_Sort_AllotList model = null;
		for (int i = 0; i < products.size(); i++) {
			Map product = (Map)products.get(i);
			model = new T_Sort_AllotList();
			model.setAtl_pd_code(StringUtil.trimString(product.get("pd_code")));
			model.setAtl_cr_code(StringUtil.trimString(product.get("cr_code")));
			model.setAtl_sz_code(StringUtil.trimString(product.get("sz_code")));
			model.setAtl_szg_code(StringUtil.trimString(product.get("pd_szg_code")));
			model.setAtl_br_code(StringUtil.trimString(product.get("br_code")));
			model.setAtl_sub_code(model.getAtl_pd_code()+model.getAtl_cr_code()+model.getAtl_sz_code()+model.getAtl_br_code());
			model.setAtl_applyamount(0);
			if(at_type.intValue() == 0){
				model.setAtl_sendamount(Integer.parseInt(StringUtil.trimString(product.get("amount"))));
			}else {
				model.setAtl_sendamount(-Integer.parseInt(StringUtil.trimString(product.get("amount"))));
			}
			model.setAtl_unitprice(Double.parseDouble(StringUtil.trimString(product.get("unitPrice"))));
			model.setAtl_sellprice(Double.parseDouble(StringUtil.trimString(product.get("sellPrice"))));
			model.setAtl_costprice(Double.parseDouble(StringUtil.trimString(product.get("costPrice"))));
			model.setAtl_remark("");
			model.setAtl_type(at_type);
			model.setAtl_number(at_number);
			model.setAtl_us_id(user.getUs_id());
			model.setCompanyid(user.getCompanyid());
			model.setOperate_type(StringUtil.trimString(product.get("operate_type")));
			resultList.add(model);
		}
		return resultList;
	}
	
	@SuppressWarnings("rawtypes")
	public static List<String[]> convertMap2Model_import(Map<String, Object> data,T_Sys_User user){
		List<String[]> resultList = new ArrayList<String[]>();
		List products = (List)data.get("datas");
		if (products == null || products.size() == 0) {
			return resultList;
		}
		String[] item = null;
		for (int i = 0; i < products.size(); i++) {
			Map product = (Map)products.get(i);
			item = new String[2];
			item[0] = StringUtil.trimString(product.get("barCode"));
			item[1] = StringUtil.trimString(product.get("amount"));
			resultList.add(item);
		}
		return resultList;
	}
	
	public static Map<String,Object> buildPrintJson(Map<String,Object> paramMap,Integer printMode){
		Map<String,Object> htmlMap = null;
		PrintVO.List2Map(paramMap);
		if(printMode != null){
			if(printMode.intValue() == 0){
				htmlMap = getPrintListHTML(paramMap);
			}
			if(printMode.intValue() == 1){
				htmlMap = getPrintSizeHTML(paramMap);
			}
			if(printMode.intValue() == 2){
				htmlMap = getPrintSumHTML(paramMap);
			}
		}
		return htmlMap;
	}
	
	@SuppressWarnings("unchecked")
	public static String[] getHeadFieldHTML(Map<String,Object> paramMap){
		StringBuffer headHtml = new StringBuffer(""),footHtml = new StringBuffer("");
		List<T_Sys_PrintField> printFields = (List<T_Sys_PrintField>)paramMap.get("fields");
		Map<String, Object> printSetMap = (Map<String, Object>)paramMap.get("printSetMap");
		T_Sort_Allot allot = (T_Sort_Allot)paramMap.get("allot");
		T_Base_Shop shop = (T_Base_Shop)paramMap.get("shop");
		T_Sys_User user = (T_Sys_User)paramMap.get("user");
		headHtml.append("<table cellpadding=\"0\" cellspacing=\"0\" class=\"head-print\">");
		headHtml.append("<tbody>");
		headHtml.append("<tr>");
		headHtml.append("<td colspan=\"3\" class=\"head-title\"><b class=\"head-font\">"+printSetMap.get(PrintUtil.PrintSet.PAGE_TITLE)+"&nbsp;</b></td>");
		headHtml.append("</tr>");
		int index = -1;
		double i = 1;
		for(T_Sys_PrintField printField:printFields){
			String colspan = "";
			if(printField.getSpf_position().intValue() == PrintUtil.FALSE){
				if(index != printField.getSpf_line()){
					if(index == -1){
						headHtml.append("<tr>");
					}else{
						i ++;
						headHtml.append("</tr><tr>");
					}
				}
				if(printField.getSpf_colspan() > 1){
					colspan = "colspan='"+printField.getSpf_colspan()+"'";
				}
				
				if(PrintUtil.PrintField.MAKEDATE.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(allot.getAt_date()).append("</td>");
				}
				if(PrintUtil.PrintField.MANAGER.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(allot.getAt_manager()).append("</td>");
				}
				if(PrintUtil.PrintField.NUMBER.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(allot.getAt_number()).append("</td>");
				}
				if(PrintUtil.PrintField.SHOP_NAME.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(allot.getShop_name()).append("</td>");
				}
				if(PrintUtil.PrintField.TEL.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(shop.getSpi_tel()).append("</td>");
				}
				if(PrintUtil.PrintField.MOBILE.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(shop.getSpi_mobile()).append("</td>");
				}
				if(PrintUtil.PrintField.OUTDEPOT_NAME.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(allot.getOutdepot_name()).append("</td>");
				}
				if(PrintUtil.PrintField.INDEPOT_NAME.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(allot.getIndepot_name()).append("</td>");
				}
				if(PrintUtil.PrintField.PROPERTY.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(allot.getAt_property()).append("</td>");
				}
				if(PrintUtil.PrintField.LINKMAN.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(shop.getSpi_man()).append("</td>");
				}
				if(PrintUtil.PrintField.HAND_NUMBER.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(allot.getAt_handnumber()).append("</td>");
				}
				if(PrintUtil.PrintField.APPLY_AMOUNT.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(allot.getAt_applyamount()).append("</td>");
				}
				if(PrintUtil.PrintField.SEND_AMOUNT.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(allot.getAt_sendamount()).append("</td>");
				}
				if(PrintUtil.PrintField.APPLY_MONEY.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(PriceLimitUtil.checkPriceLimit(allot.getAt_applymoney(),CommonUtil.PRICE_LIMIT_SEND, user.getUs_limit())).append("</td>");
				}
				if(PrintUtil.PrintField.SEND_MONEY.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(PriceLimitUtil.checkPriceLimit(allot.getAt_sendmoney(),CommonUtil.PRICE_LIMIT_SEND, user.getUs_limit())).append("</td>");
				}
				if(PrintUtil.PrintField.RECEIVABLE.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(PriceLimitUtil.checkPriceLimit(allot.getAt_receivable(),CommonUtil.PRICE_LIMIT_SEND, user.getUs_limit())).append("</td>");
				}
				if(PrintUtil.PrintField.DEPT.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(shop.getSp_receivable()-shop.getSp_received()).append("</td>");
				}
				if(PrintUtil.PrintField.ADDRESS.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(StringUtil.trimString(shop.getSpi_addr())).append("</td>");
				}
				if(PrintUtil.PrintField.REMARK.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(allot.getAt_remark()).append("</td>");
				}
				index = printField.getSpf_line();
			}else if(printField.getSpf_position().intValue() == PrintUtil.TRUE){
				if(PrintUtil.PrintField.END_MAKER.equals(printField.getSpf_code())){
					footHtml.append("<span style='float:left;width:45mm;'>").append(printField.getSpf_name()).append("：").append(StringUtil.trimString(allot.getAt_maker())).append("</span>");
				}
				if(PrintUtil.PrintField.END_APPROVER.equals(printField.getSpf_code())){
					footHtml.append("<span style='float:left;width:45mm;'>").append(printField.getSpf_name()).append("：").append(StringUtil.trimString("TODO")).append("</span>");
				}
			}
		}
		headHtml.append("</tr>");
		//标题高度
		double titleheight = Double.parseDouble((String)printSetMap.get(PrintUtil.PrintSet.TITLE_HEIGHT));
		//表头每行高度
		double head_height = Double.parseDouble((String)printSetMap.get(PrintUtil.PrintSet.HEAD_HEIGHT));
		//表格数据与表头间距
		double table_top = Double.parseDouble((String)printSetMap.get(PrintUtil.PrintSet.TABLE_TOP));
		double page_top = Double.parseDouble((String)printSetMap.get(PrintUtil.PrintSet.PAGE_TOP));
		//总高度
		double sum_height = page_top + table_top + titleheight + (head_height * i);
		headHtml.append(" </tbody>");
		headHtml.append("</table>");
		String[] html = new String[3];
		html[0] = headHtml.toString();
		html[1] = footHtml.toString();
		html[2] = sum_height + "";
		return html;
	}
	
	public static boolean IsExistBras(List<T_Sort_AllotList> allotLists){
		boolean flag = false;
		for(T_Sort_AllotList item :allotLists){
			if(StringUtil.isNotEmpty(item.getAtl_br_code())){
				flag = true;
				break;
			}
		}
		return flag;
	}
	
	@SuppressWarnings("unchecked")
	public static Map<String,Object> getPrintListHTML(Map<String,Object> paramMap){
		Map<String,Object> htmlMap = null;
		StringBuffer html = new StringBuffer("");
		try{
			T_Sys_Print print = (T_Sys_Print)paramMap.get("print");
			List<T_Sys_PrintData> printDatas = (List<T_Sys_PrintData>)(paramMap.get("datas"));
			Map<String, Object> printSetMap = (Map<String, Object>)paramMap.get("printSetMap");
			Map<String, Object> printDataMap = (Map<String, Object>)paramMap.get("printDataMap");
			List<T_Sort_AllotList> allotList = (List<T_Sort_AllotList>)paramMap.get("allotList");
			T_Sys_User user = (T_Sys_User)paramMap.get("user");
			
			//获取表头与表尾的数据
			String[] htmlArray = getHeadFieldHTML(paramMap);
			
			html.append("<table cellpadding=\"4\" cellspacing=\"0\" class=\"data-print\">");
			html.append("<thead>");
			html.append("<tr>");
			
			int dataSize = printDatas.size();
			boolean isHas = IsExistBras(allotList);
			for(T_Sys_PrintData printData:printDatas){
				if(PrintUtil.PrintData.BR_NAME.equals(printData.getSpd_code())){
					if(isHas){
						html.append("<th>");
						html.append("<div style=\"width='"+printData.getSpd_width()+"mm'\">");
						html.append(printData.getSpd_name());
						html.append("</div>");
						html.append("</th>");
					}
				}else{
					html.append("<th>");
					html.append("<div style=\"width='"+printData.getSpd_width()+"mm'\">");
					html.append(printData.getSpd_name());
					html.append("</div>");
					html.append("</th>");
				}
			}
			html.append("</tr>");
			html.append("</thead>");
			int i = 0,applyamount = 0,sendamount=0;
			double applyMoney = 0d,sendMoney = 0d,retailMoney = 0d;
			html.append("<tbody>");			
			for(T_Sort_AllotList item: allotList){
				html.append("<tr>");
				for (int j = 0; j < printDatas.size(); j++) {
					if(PrintUtil.PrintData.INDEX.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.INDEX)).append(">");
						html.append((i+1)+"</td>");
					}
					if(PrintUtil.PrintData.PD_NO.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_NO)).append(">");
						html.append(item.getPd_no()+"</td>");
					}
					if(PrintUtil.PrintData.PD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_NAME)).append(">");
						html.append(item.getPd_name()+"</td>");
					}
					if(PrintUtil.PrintData.BD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.BD_NAME)).append(">");
						html.append(item.getBd_name()+"</td>");
					}
					if(PrintUtil.PrintData.TP_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.TP_NAME)).append(">");
						html.append(item.getTp_name()+"</td>");
					}
					if(PrintUtil.PrintData.PD_SEASON.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_SEASON)).append(">");
						html.append(item.getPd_season()+"</td>");
					}
					if(PrintUtil.PrintData.PD_YEAR.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_YEAR)).append(">");
						html.append(item.getPd_year()+"</td>");
					}
					if(PrintUtil.PrintData.CR_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.CR_NAME)).append(">");
						html.append(item.getCr_name()+"</td>");
					}
					if(PrintUtil.PrintData.BR_NAME.equals(printDatas.get(j).getSpd_code())){
						if (isHas){
							html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.BR_NAME)).append(">");
							html.append(item.getBr_name()+"</td>");
						}
					}
					if(PrintUtil.PrintData.SZ_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.SZ_NAME)).append(">");
						html.append(item.getSz_name()+"</td>");
					}
					if(PrintUtil.PrintData.PD_UNIT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_UNIT)).append(">");
						html.append(item.getPd_unit()+"</td>");
					}
					if(PrintUtil.PrintData.APPLY_AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.APPLY_AMOUNT)).append(">");
						html.append(item.getAtl_applyamount()+"</td>");
						applyamount += item.getAtl_applyamount();
					}
					if(PrintUtil.PrintData.SEND_AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.SEND_AMOUNT)).append(">");
						html.append(item.getAtl_sendamount()+"</td>");
						sendamount += item.getAtl_sendamount();
					}
					if(PrintUtil.PrintData.UNIT_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.UNIT_PRICE)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getAtl_unitprice(),CommonUtil.PRICE_LIMIT_SEND, user.getUs_limit())+"</td>");
					}
					if(PrintUtil.PrintData.APPLY_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.APPLY_MONEY)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getAtl_unitprice()*item.getAtl_applyamount(),CommonUtil.PRICE_LIMIT_SEND, user.getUs_limit())+"</td>");
						applyMoney += item.getAtl_unitprice()*item.getAtl_applyamount();
					}
					if(PrintUtil.PrintData.SEND_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.SEND_MONEY)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getAtl_unitprice()*item.getAtl_sendamount(),CommonUtil.PRICE_LIMIT_SEND, user.getUs_limit())+"</td>");
						sendMoney += item.getAtl_unitprice()*item.getAtl_sendamount();
					}
					if(PrintUtil.PrintData.RETAIL_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.RETAIL_PRICE)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getAtl_sellprice(),CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit())+"</td>");
					}
					if(PrintUtil.PrintData.RETAIL_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.RETAIL_MONEY)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getAtl_sellprice()*item.getAtl_applyamount(),CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit())+"</td>");
						retailMoney += item.getAtl_sellprice()*item.getAtl_applyamount();
					}
					if(PrintUtil.PrintData.REMARK.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.REMARK)).append(">");
						html.append(item.getAtl_remark()+"</td>");
					}
				}
				html.append("</tr>");
				i++;
			}
			html.append("</tbody>");
			String subtotal = (String)printSetMap.get(PrintUtil.PrintSet.TABLE_SUBTOTAL);
			if(String.valueOf(PrintUtil.TRUE).equals(subtotal)){
				html.append("<tfoot>");
				//列表模式下当前页合计算输出
				html.append("<tr>");
				for (int j = 0; j < printDatas.size(); j++) {
					if(PrintUtil.PrintData.INDEX.equals(printDatas.get(j).getSpd_code())){
						html.append("<td>小计</td>");
					}
					if(PrintUtil.PrintData.PD_NO.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.BD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.TP_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_SEASON.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_YEAR.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.CR_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.BR_NAME.equals(printDatas.get(j).getSpd_code())){
						if (isHas){
							html.append("<td></td>");
						}
					}
					if(PrintUtil.PrintData.SZ_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_UNIT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.APPLY_AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;' tdata='subSum' format='#'>###</td>");
					}
					if(PrintUtil.PrintData.SEND_AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;' tdata='subSum' format='#'>###</td>");
					}
					if(PrintUtil.PrintData.UNIT_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.APPLY_MONEY.equals(printDatas.get(j).getSpd_code())){
						if(PriceLimitUtil.hasPriceLimit(CommonUtil.PRICE_LIMIT_SEND, user.getUs_limit())){
							html.append("<td style='text-align:right;' tdata='subSum' format='#,##0.00'>###</td>");
						}else {
							html.append("<td style='text-align:right;'>***</td>");
						}
					}
					if(PrintUtil.PrintData.SEND_MONEY.equals(printDatas.get(j).getSpd_code())){
						if(PriceLimitUtil.hasPriceLimit(CommonUtil.PRICE_LIMIT_SEND, user.getUs_limit())){
							html.append("<td style='text-align:right;' tdata='subSum' format='#,##0.00'>###</td>");
						}else {
							html.append("<td style='text-align:right;'>***</td>");
						}
					}
					if(PrintUtil.PrintData.RETAIL_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.RETAIL_MONEY.equals(printDatas.get(j).getSpd_code())){
						if(PriceLimitUtil.hasPriceLimit(CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit())){
							html.append("<td style='text-align:right;' tdata='subSum' format='#,##0.00'>###</td>");
						}else {
							html.append("<td style='text-align:right;'>***</td>");
						}
					}
					if(PrintUtil.PrintData.REMARK.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
				}
				html.append("</tr>");
			}
			
			String total = (String)printSetMap.get(PrintUtil.PrintSet.TABLE_TOTAL);
			if(String.valueOf(PrintUtil.TRUE).equals(total)){
				html.append("<tr>");
				for (int j = 0; j < printDatas.size(); j++) {
					if(PrintUtil.PrintData.INDEX.equals(printDatas.get(j).getSpd_code())){
						html.append("<td>总计</td>");
					}
					if(PrintUtil.PrintData.PD_NO.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.BD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.TP_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_SEASON.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_YEAR.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.CR_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.BR_NAME.equals(printDatas.get(j).getSpd_code())){
						if (isHas){
							html.append("<td></td>");
						}
					}
					if(PrintUtil.PrintData.SZ_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_UNIT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.APPLY_AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+applyamount+"</td>");
					}
					if(PrintUtil.PrintData.SEND_AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+sendamount+"</td>");
					}
					if(PrintUtil.PrintData.UNIT_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.APPLY_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+PriceLimitUtil.checkPriceLimit(applyMoney, CommonUtil.PRICE_LIMIT_SEND, user.getUs_limit())+"</td>");
					}
					if(PrintUtil.PrintData.SEND_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+PriceLimitUtil.checkPriceLimit(sendMoney, CommonUtil.PRICE_LIMIT_SEND, user.getUs_limit())+"</td>");
					}
					if(PrintUtil.PrintData.RETAIL_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.RETAIL_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+PriceLimitUtil.checkPriceLimit(retailMoney, CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit())+"</td>");
					}
					if(PrintUtil.PrintData.REMARK.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
				}
				html.append("</tr>");
			}
			html.append("<tr>");
			int colspan = 0;
			if(isHas){
				colspan = dataSize+1;
			}else{
				colspan = dataSize;
			}
			html.append("<td colspan='"+colspan+"'>");
			html.append(htmlArray[1]);
			html.append("<div style='float:right;'>");
			html.append("<span  tdata='pageNO' format=\"#\">第#页</span>");
			html.append("<b>/</b>");
			html.append("<span tdata='pageCount' format=\"#\">总#页</span>");
			html.append("</div>");
			String end_endshow = (String)printSetMap.get(PrintUtil.PrintSet.END_ENDSHOW);
			if(String.valueOf(PrintUtil.FALSE).equals(end_endshow)){
				if(StringUtil.isNotEmpty(print.getSp_remark())){
					html.append("<br/><div style='float:left;'>");
	                html.append(StringUtil.trimString(print.getSp_remark()));
	                html.append("</div>");
				}
            }
			html.append("</td>");
			html.append("</tfoot>");
			html.append("</table>");
			htmlMap = new HashMap<String, Object>(27);
			htmlMap.put("listHtml", html.toString());
			htmlMap.put("headHtml", htmlArray[0]);
			htmlMap.put("styleHtml", PrintVO.getPrintStyleHTML(paramMap));
			htmlMap.put("totalHeadHeight", htmlArray[2]);
			htmlMap.putAll(printSetMap);
		}catch(Exception e){
			e.printStackTrace();
		}
		return htmlMap;
	}
	
	@SuppressWarnings("unchecked")
	public static Map<String,Object> getPrintSizeHTML(Map<String,Object> paramMap){
		StringBuffer html = new StringBuffer("");
		Map<String,Object> htmlMap = null;
		try{
			T_Sys_Print print = (T_Sys_Print)paramMap.get("print");
			List<T_Sys_PrintData> printDatas = (List<T_Sys_PrintData>)(paramMap.get("datas"));
			Map<String, Object> printSetMap = (Map<String, Object>)paramMap.get("printSetMap");
			Map<String, Object> printDataMap = (Map<String, Object>)paramMap.get("printDataMap");
			List<T_Sort_AllotList> allotList = (List<T_Sort_AllotList>)paramMap.get("allotList");
			T_Sort_Allot allot = (T_Sort_Allot)paramMap.get("allot");
			Integer isSend = (Integer)paramMap.get("isSend");
			T_Sys_User user = (T_Sys_User)paramMap.get("user");
			List<T_Base_SizeList> sizeGroupList = (List<T_Base_SizeList>)paramMap.get("sizeGroupList");
			boolean useSendAmount = false;//尺码横排是否使用发货数量
			if(isSend != null && isSend.intValue() == 1){
				useSendAmount = true;
			}else if(allot.getAt_applyamount() == 0){
				useSendAmount = true;
			}else if(allot.getAt_ar_state().intValue() == 3 || allot.getAt_ar_state().intValue() == 4 || allot.getAt_ar_state().intValue() == 5){
				useSendAmount = true;
			}
			int[] maxColNumArray = new int[1];
			Map<String, List<String>> sizeMap = new HashMap<String, List<String>>();
			List<List<String>> listResult = SizeHorizontalVO.getSizeModeTitleInfoList(sizeMap, maxColNumArray, sizeGroupList);
			//获取表头与表尾的数据
			String[] htmlArray = getHeadFieldHTML(paramMap);
			List<T_Sort_SizeCommon> sizeCommonList = getSizeModeList(allotList, maxColNumArray[0], sizeMap,useSendAmount);
			if(listResult.size()==0 || sizeCommonList.size()==0) {
				return null;
			}
			int[] totalSizeBySingle = new int[maxColNumArray[0]];
			boolean isHas = IsExistBras(allotList);
			
			//根据商家ID和货号查询出对应的尺码组信息在title输出
		    int rowSize = listResult.size()+1;
		    html.append("<table cellpadding=\"4\" cellspacing=\"0\" class=\"data-print\">");
		    html.append("<thead>");
		    html.append("<tr>");
			for(T_Sys_PrintData printData:printDatas){
				if(PrintUtil.PrintData.BR_NAME.equals(printData.getSpd_code())){
					if(isHas){
						html.append("<th rowspan=\""+rowSize+"\">");
						html.append(printData.getSpd_name());
						html.append("</th>");
					}
		    	}else if(PrintUtil.PrintData.SZ_NAME.equals(printData.getSpd_code())){
		    		html.append("<th colspan=\""+maxColNumArray[0]+"\">");
					html.append(printData.getSpd_name());
					html.append("</th>");
				}else if(!PrintUtil.PrintData.REMARK.equals(printData.getSpd_code())
						&&!PrintUtil.PrintData.APPLY_AMOUNT.equals(printData.getSpd_code())
						&&!PrintUtil.PrintData.APPLY_MONEY.equals(printData.getSpd_code())
						){
					html.append("<th rowspan=\""+(rowSize)+"\">");
					html.append(printData.getSpd_name());
					html.append("</th>");
				}
			}

		    html.append("</tr>"); 
		    if(printDataMap.containsKey(PrintUtil.PrintData.SZ_NAME)){
			    for(int i=0;i<listResult.size();i++){  
			    	html.append("<tr>");
			    	List<String> sizeList = (List<String>)listResult.get(i);
			    	for (int k = 0;k<maxColNumArray[0];k++){
			    		html.append("<th>");//入库单详细信息
			    		if (k < sizeList.size()){
			    			html.append(sizeList.get(k).split("_AND_")[1]);
			    		}else{
			    			html.append("-");
			    		}
			    		html.append("</th>");
			    	}
			    	html.append("</tr>");
			    }
		    }
		    html.append("</thead>");
		    int i = 0,amount = 0;
		    double unitMoney = 0d,retailMoney = 0d;
			for (T_Sort_SizeCommon item : sizeCommonList) {
				i++;
				html.append("<tr>");
				for (int j = 0; j < printDatas.size(); j++) {
					if(PrintUtil.PrintData.INDEX.equals(printDatas.get(j).getSpd_code())){
						html.append("<td nowrap").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.INDEX)).append(">");
						html.append((i+1)+"</td>");
					}
					if(PrintUtil.PrintData.PD_NO.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_NO)).append(">");
						html.append(item.getPd_no()+"</td>");
					}
					if(PrintUtil.PrintData.PD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_NAME)).append(">");
						html.append(item.getPd_name()+"</td>");
					}
					if(PrintUtil.PrintData.BD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.BD_NAME)).append(">");
						html.append(item.getBd_name()+"</td>");
					}
					if(PrintUtil.PrintData.TP_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.TP_NAME)).append(">");
						html.append(item.getTp_name()+"</td>");
					}
					if(PrintUtil.PrintData.PD_SEASON.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_SEASON)).append(">");
						html.append(item.getPd_season()+"</td>");
					}
					if(PrintUtil.PrintData.PD_YEAR.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_YEAR)).append(">");
						html.append(item.getPd_year()+"</td>");
					}
					if(PrintUtil.PrintData.CR_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.CR_NAME)).append(">");
						html.append(item.getCr_name()+"</td>");
					}
					if(PrintUtil.PrintData.BR_NAME.equals(printDatas.get(j).getSpd_code())){
						if (isHas){
							html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.BR_NAME)).append(">");
							html.append(item.getBr_name()+"</td>");
						}
					}
					if(PrintUtil.PrintData.SZ_NAME.equals(printDatas.get(j).getSpd_code())){
						
						int[] arr = item.getSingle_amount();
						for(int k = 0;k< maxColNumArray[0];k++){	
							html.append("<td align='center'>");
							if (k<arr.length){
								html.append(arr[k] == 0 ? "" : arr[k]);//单个尺码数量
								totalSizeBySingle[k] = totalSizeBySingle[k]+ arr[k];
							}
							html.append("</td>");
						}
					}
					if(PrintUtil.PrintData.PD_UNIT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_UNIT)).append(">");
						html.append(item.getPd_unit()+"</td>");
					}
					
					if(PrintUtil.PrintData.SEND_AMOUNT.equals(printDatas.get(j).getSpd_code())){//数量合计
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.SEND_AMOUNT)).append(">");
						html.append(item.getTotalamount()+"</td>");
						amount += item.getTotalamount();
					}
					
					if(PrintUtil.PrintData.UNIT_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.UNIT_PRICE)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getUnitprice(),CommonUtil.PRICE_LIMIT_SEND, user.getUs_limit()));
						html.append("</td>");
					}
					if(PrintUtil.PrintData.SEND_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.SEND_MONEY)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getUnitmoney(),CommonUtil.PRICE_LIMIT_SEND, user.getUs_limit()));
						html.append("</td>");
						unitMoney += item.getUnitmoney();
					}
					if(PrintUtil.PrintData.RETAIL_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.RETAIL_PRICE)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getSellprice(),CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit()));
						html.append("</td>");
					}
					if(PrintUtil.PrintData.RETAIL_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.RETAIL_MONEY)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getSellmoney(),CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit()));
						html.append("</td>");
						retailMoney += item.getSellmoney();
					}
				}
				html.append("</tr>");
			}
			html.append("<tfoot>");
			boolean hasMaxSize = false;
			html.append("<tr>");
			for (int j = 0; j < printDatas.size(); j++) {
				if(PrintUtil.PrintData.INDEX.equals(printDatas.get(j).getSpd_code())){
					html.append("<td>总计</td>");
				}
				if(PrintUtil.PrintData.PD_NO.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.PD_NAME.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.BD_NAME.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.TP_NAME.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.PD_SEASON.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.PD_YEAR.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.CR_NAME.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.BR_NAME.equals(printDatas.get(j).getSpd_code())){
					if (isHas){
						html.append("<td></td>");
					}
				}
				if(PrintUtil.PrintData.SZ_NAME.equals(printDatas.get(j).getSpd_code())){
					for(int k = 0;k< maxColNumArray[0];k++){	
						hasMaxSize = true;
						html.append("<td style=\"text-align:center;\">");
						if(totalSizeBySingle[k] != 0){
							html.append(totalSizeBySingle[k]);
						}
						html.append("</td>");
					}	
					if (!hasMaxSize){
						html.append("<td>");html.append("</td>");
					} 
				}
				if(PrintUtil.PrintData.PD_UNIT.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.SEND_AMOUNT.equals(printDatas.get(j).getSpd_code())){//数量合计
					html.append("<td style=\"text-align:right;\">");
					html.append(amount+"</td>");
				}
				if(PrintUtil.PrintData.UNIT_PRICE.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.SEND_MONEY.equals(printDatas.get(j).getSpd_code())){
					html.append("<td style=\"text-align:right;\"><span>"+PriceLimitUtil.checkPriceLimit(unitMoney,CommonUtil.PRICE_LIMIT_SEND, user.getUs_limit())+"</span></td>");
				}
				if(PrintUtil.PrintData.RETAIL_PRICE.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.RETAIL_MONEY.equals(printDatas.get(j).getSpd_code())){
					html.append("<td style=\"text-align:right;\"><span>"+PriceLimitUtil.checkPriceLimit(retailMoney,CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit())+"</span></td>");
				}
			}
			html.append("</tr>");
			
			int dataSize = printDatas.size()-1;
			if(printDataMap.containsKey(PrintUtil.PrintData.REMARK)){
				dataSize -= 1;
			}
			int colspan = 0;
			if(isHas){
				colspan = dataSize;
			}else if(printDataMap.containsKey(PrintUtil.PrintData.BR_NAME)){
				colspan = dataSize-1;
			}else{
				colspan = dataSize;
			}
			colspan += maxColNumArray[0];
			html.append("<td colspan='"+colspan+"'>");
			html.append(htmlArray[1]);
			html.append("<div style='float:right;'>");
			html.append("<span tdata='pageNO' format=\"#\">第#页</span>");
			html.append("<b>/</b>");
			html.append("<span tdata='pageCount' format=\"#\">总#页</span>");
			html.append("</div>");
			String end_endshow = (String)printSetMap.get(PrintUtil.PrintSet.END_ENDSHOW);
			if(String.valueOf(PrintUtil.FALSE).equals(end_endshow)){
				if(StringUtil.isNotEmpty(print.getSp_remark())){
					html.append("<br/><div style='float:left;'>");
	                html.append(StringUtil.trimString(print.getSp_remark()));
	                html.append("</div>");
				}
            }
			html.append("</td>");
			html.append("</tr>");
			html.append("</tfoot>");
			html.append("</table>");
			htmlMap = new HashMap<String, Object>(27);
			htmlMap.put("listHtml", html.toString());
			htmlMap.put("headHtml", htmlArray[0]);
			htmlMap.put("styleHtml", PrintVO.getPrintStyleHTML(paramMap));
			htmlMap.put("totalHeadHeight", htmlArray[2]);
			htmlMap.putAll(printSetMap);
		}catch(Exception e){
			e.printStackTrace();
		}
		return htmlMap;
	}
	
	@SuppressWarnings("unchecked")
	public static Map<String,Object> getPrintSumHTML(Map<String,Object> paramMap){
		Map<String,Object> htmlMap = null;
		StringBuffer html = new StringBuffer("");
		try{
			T_Sys_Print print = (T_Sys_Print)paramMap.get("print");
			List<T_Sys_PrintData> printDatas = (List<T_Sys_PrintData>)(paramMap.get("datas"));
			Map<String, Object> printSetMap = (Map<String, Object>)paramMap.get("printSetMap");
			Map<String, Object> printDataMap = (Map<String, Object>)paramMap.get("printDataMap");
			List<T_Sort_AllotList> allotList = (List<T_Sort_AllotList>)paramMap.get("allotList");
			T_Sys_User user = (T_Sys_User)paramMap.get("user");
			
			//获取表头与表尾的数据
			String[] htmlArray = getHeadFieldHTML(paramMap);
			
			html.append("<table cellpadding=\"4\" cellspacing=\"0\" class=\"data-print\">");
			html.append("<thead>");
			html.append("<tr>");
			
			int dataSize = printDatas.size();
			for(T_Sys_PrintData printData:printDatas){
				if(PrintUtil.PrintData.BR_NAME.equals(printData.getSpd_code())||
						PrintUtil.PrintData.CR_NAME.equals(printData.getSpd_code())||
						PrintUtil.PrintData.SZ_NAME.equals(printData.getSpd_code())){//汇总模式不显示尺码杯型颜色
				}else{
					html.append("<th>");
					html.append("<div style=\"width='"+printData.getSpd_width()+"mm'\">");
					html.append(printData.getSpd_name());
					html.append("</div>");
					html.append("</th>");
				}
			}
			html.append("</tr>");
			html.append("</thead>");
			int i = 0,applyamount = 0,sendamount=0;
			double applyMoney = 0d,sendMoney = 0d,retailMoney = 0d;
			html.append("<tbody>");			
			for(T_Sort_AllotList item: allotList){
				html.append("<tr>");
				for (int j = 0; j < printDatas.size(); j++) {
					if(PrintUtil.PrintData.INDEX.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.INDEX)).append(">");
						html.append((i+1)+"</td>");
					}
					if(PrintUtil.PrintData.PD_NO.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_NO)).append(">");
						html.append(item.getPd_no()+"</td>");
					}
					if(PrintUtil.PrintData.PD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_NAME)).append(">");
						html.append(item.getPd_name()+"</td>");
					}
					if(PrintUtil.PrintData.BD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.BD_NAME)).append(">");
						html.append(item.getBd_name()+"</td>");
					}
					if(PrintUtil.PrintData.TP_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.TP_NAME)).append(">");
						html.append(item.getTp_name()+"</td>");
					}
					if(PrintUtil.PrintData.PD_SEASON.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_SEASON)).append(">");
						html.append(item.getPd_season()+"</td>");
					}
					if(PrintUtil.PrintData.PD_YEAR.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_YEAR)).append(">");
						html.append(item.getPd_year()+"</td>");
					}
					if(PrintUtil.PrintData.PD_UNIT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_UNIT)).append(">");
						html.append(item.getPd_unit()+"</td>");
					}
					if(PrintUtil.PrintData.APPLY_AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.APPLY_AMOUNT)).append(">");
						html.append(item.getAtl_applyamount()+"</td>");
						applyamount += item.getAtl_applyamount();
					}
					if(PrintUtil.PrintData.SEND_AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.SEND_AMOUNT)).append(">");
						html.append(item.getAtl_sendamount()+"</td>");
						sendamount += item.getAtl_sendamount();
					}
					if(PrintUtil.PrintData.UNIT_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.UNIT_PRICE)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getAtl_unitprice(),CommonUtil.PRICE_LIMIT_SEND, user.getUs_limit()));
						html.append("</td>");
					}
					if(PrintUtil.PrintData.APPLY_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.APPLY_MONEY)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getAtl_unitprice()*item.getAtl_applyamount(),CommonUtil.PRICE_LIMIT_SEND, user.getUs_limit()));
						html.append("</td>");
						applyMoney += item.getAtl_unitprice()*item.getAtl_applyamount();
					}
					if(PrintUtil.PrintData.SEND_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.SEND_MONEY)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getAtl_unitprice()*item.getAtl_sendamount(),CommonUtil.PRICE_LIMIT_SEND, user.getUs_limit()));
						html.append("</td>");
						applyMoney += item.getAtl_unitprice()*item.getAtl_sendamount();
					}
					if(PrintUtil.PrintData.RETAIL_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.RETAIL_PRICE)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getAtl_sellprice(),CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit()));
						html.append("</td>");
					}
					if(PrintUtil.PrintData.RETAIL_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.RETAIL_MONEY)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getAtl_sellprice()*item.getAtl_applyamount(),CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit()));
						html.append("</td>");
						retailMoney += item.getAtl_sellprice()*item.getAtl_applyamount();
					}
					if(PrintUtil.PrintData.REMARK.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.REMARK)).append(">");
						html.append(item.getAtl_remark()+"</td>");
					}
				}
				html.append("</tr>");
				i++;
			}
			html.append("</tbody>");
			String subtotal = (String)printSetMap.get(PrintUtil.PrintSet.TABLE_SUBTOTAL);
			if(String.valueOf(PrintUtil.TRUE).equals(subtotal)){
				html.append("<tfoot>");
				//列表模式下当前页合计算输出
				html.append("<tr>");
				for (int j = 0; j < printDatas.size(); j++) {
					if(PrintUtil.PrintData.INDEX.equals(printDatas.get(j).getSpd_code())){
						html.append("<td>小计</td>");
					}
					if(PrintUtil.PrintData.PD_NO.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.BD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.TP_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_SEASON.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_YEAR.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_UNIT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.APPLY_AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;' tdata='subSum' format='#'>###</td>");
					}
					if(PrintUtil.PrintData.SEND_AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;' tdata='subSum' format='#'>###</td>");
					}
					if(PrintUtil.PrintData.UNIT_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.APPLY_MONEY.equals(printDatas.get(j).getSpd_code())){
						if(PriceLimitUtil.hasPriceLimit(CommonUtil.PRICE_LIMIT_SEND, user.getUs_limit())){
							html.append("<td style='text-align:right;' tdata='subSum' format='#,##0.00'>###</td>");
						}else {
							html.append("<td style='text-align:right;'>***</td>");
						}
					}
					if(PrintUtil.PrintData.SEND_MONEY.equals(printDatas.get(j).getSpd_code())){
						if(PriceLimitUtil.hasPriceLimit(CommonUtil.PRICE_LIMIT_SEND, user.getUs_limit())){
							html.append("<td style='text-align:right;' tdata='subSum' format='#,##0.00'>###</td>");
						}else {
							html.append("<td style='text-align:right;'>***</td>");
						}
					}
					if(PrintUtil.PrintData.RETAIL_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.RETAIL_MONEY.equals(printDatas.get(j).getSpd_code())){
						if(PriceLimitUtil.hasPriceLimit(CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit())){
							html.append("<td style='text-align:right;' tdata='subSum' format='#,##0.00'>###</td>");
						}else {
							html.append("<td style='text-align:right;'>***</td>");
						}
					}
					if(PrintUtil.PrintData.REMARK.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
				}
				html.append("</tr>");
			}
			
			String total = (String)printSetMap.get(PrintUtil.PrintSet.TABLE_TOTAL);
			if(String.valueOf(PrintUtil.TRUE).equals(total)){
				html.append("<tr>");
				for (int j = 0; j < printDatas.size(); j++) {
					if(PrintUtil.PrintData.INDEX.equals(printDatas.get(j).getSpd_code())){
						html.append("<td>总计</td>");
					}
					if(PrintUtil.PrintData.PD_NO.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.BD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.TP_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_SEASON.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_YEAR.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_UNIT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.APPLY_AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+applyamount+"</td>");
					}
					if(PrintUtil.PrintData.SEND_AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+sendamount+"</td>");
					}
					if(PrintUtil.PrintData.UNIT_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.APPLY_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+PriceLimitUtil.checkPriceLimit(applyMoney,CommonUtil.PRICE_LIMIT_SEND, user.getUs_limit())+"</td>");
					}
					if(PrintUtil.PrintData.SEND_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+PriceLimitUtil.checkPriceLimit(sendMoney,CommonUtil.PRICE_LIMIT_SEND, user.getUs_limit())+"</td>");
					}
					if(PrintUtil.PrintData.RETAIL_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.RETAIL_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+PriceLimitUtil.checkPriceLimit(retailMoney,CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit())+"</td>");
					}
					if(PrintUtil.PrintData.REMARK.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
				}
				html.append("</tr>");
			}
			html.append("<tr>");
			int colspan = 0;
			colspan = dataSize-2;
			html.append("<td colspan='"+colspan+"'>");
			html.append(htmlArray[1]);
			html.append("<div style='float:right;'>");
			html.append("<span  tdata='pageNO' format=\"#\">第#页</span>");
			html.append("<b>/</b>");
			html.append("<span tdata='pageCount' format=\"#\">总#页</span>");
			html.append("</div>");
			String end_endshow = (String)printSetMap.get(PrintUtil.PrintSet.END_ENDSHOW);
			if(String.valueOf(PrintUtil.FALSE).equals(end_endshow)){
				if(StringUtil.isNotEmpty(print.getSp_remark())){
					html.append("<br/><div style='float:left;'>");
	                html.append(StringUtil.trimString(print.getSp_remark()));
	                html.append("</div>");
				}
            }
			html.append("</td>");
			html.append("</tfoot>");
			html.append("</table>");
			htmlMap = new HashMap<String, Object>(27);
			htmlMap.put("listHtml", html.toString());
			htmlMap.put("headHtml", htmlArray[0]);
			htmlMap.put("styleHtml", PrintVO.getPrintStyleHTML(paramMap));
			htmlMap.put("totalHeadHeight", htmlArray[2]);
			htmlMap.putAll(printSetMap);
		}catch(Exception e){
			e.printStackTrace();
		}
		return htmlMap;
	}
	
}
