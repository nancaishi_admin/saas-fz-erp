package zy.vo.batch;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import zy.entity.base.size.T_Base_SizeList;
import zy.entity.batch.client.T_Batch_Client;
import zy.entity.batch.order.T_Batch_SizeCommon;
import zy.entity.batch.sell.T_Batch_Sell;
import zy.entity.batch.sell.T_Batch_SellList;
import zy.entity.sys.print.T_Sys_Print;
import zy.entity.sys.print.T_Sys_PrintData;
import zy.entity.sys.print.T_Sys_PrintField;
import zy.entity.sys.user.T_Sys_User;
import zy.util.CommonUtil;
import zy.util.PriceLimitUtil;
import zy.util.PrintUtil;
import zy.util.StringUtil;
import zy.vo.common.PrintVO;
import zy.vo.common.SizeHorizontalVO;

public class BatchSellVO {
	private static List<T_Batch_SizeCommon> getSizeModeList(List<T_Batch_SellList> list,int maxColNumArray, Map<String, List<String>> map) {
		// 尺码横排结果
		List<T_Batch_SizeCommon> lstRes = new ArrayList<T_Batch_SizeCommon>();
		int[] subArray = null;
		T_Batch_SizeCommon temp = null;
		int Amount_Total = 0;
		double RetailMoney_Total = 0d;
		double UnitMoney_Total = 0d;
		double CostMoney_Total = 0d;
		double RebateMoney_Total = 0d;
		String pd_code = "";
		String cr_code = "";
		String br_code = "";
		String pd_type = "";
		List<String> sizeListWithSzgIdList = null;
		for (T_Batch_SellList item : list) {
			if (!pd_code.equals(item.getSel_pd_code())
					|| !cr_code.equals(item.getSel_cr_code())
					|| !br_code.equals(item.getSel_br_code())
					|| !pd_type.equals(StringUtil.trimString(item.getSel_pi_type()))
					) {
				pd_code = StringUtil.trimString(item.getSel_pd_code());
				cr_code = StringUtil.trimString(item.getSel_cr_code());
				br_code = StringUtil.trimString(item.getSel_br_code());
				pd_type = StringUtil.trimString(item.getSel_pi_type());
				Amount_Total = 0;
				RetailMoney_Total = 0d;
				UnitMoney_Total = 0d;
				temp = new T_Batch_SizeCommon();
				subArray = new int[maxColNumArray];
				temp.setSingle_amount(subArray);
				lstRes.add(temp);
			}
			temp.setId(item.getSel_id());
			temp.setPd_code(item.getSel_pd_code());
			temp.setPd_no(item.getPd_no());
			temp.setPd_name(item.getPd_name());
			temp.setPd_unit(item.getPd_unit());
			temp.setPd_type(pd_type);
			temp.setBd_name(item.getBd_name());
			temp.setTp_name(item.getTp_name());
			temp.setPd_year(item.getPd_year());
			temp.setPd_season(item.getPd_season());
			temp.setSzg_code(item.getSel_szg_code());
			temp.setCr_code(item.getSel_cr_code());
			temp.setCr_name(item.getCr_name());
			temp.setBr_code(item.getSel_br_code());
			temp.setBr_name(item.getBr_name());
			temp.setUnitprice(item.getSel_unitprice());
			temp.setRetailprice(item.getSel_retailprice());
			temp.setCostprice(item.getSel_costprice());
			temp.setRebateprice(item.getSel_rebateprice());
			Amount_Total += item.getSel_amount();
			RetailMoney_Total += item.getSel_amount()*item.getSel_retailprice();
			UnitMoney_Total += item.getSel_amount()*item.getSel_unitprice();
			CostMoney_Total += item.getSel_amount()*item.getSel_costprice();
			RebateMoney_Total += item.getSel_amount()*item.getSel_rebateprice();
			temp.setTotalamount(Amount_Total);
			temp.setUnitmoney(UnitMoney_Total);
			temp.setRetailmoney(RetailMoney_Total);
			temp.setCostmoney(CostMoney_Total);
			temp.setRebatemoney(RebateMoney_Total);
			
			//获得尺码组编号对应的尺码编号信息
			sizeListWithSzgIdList = (List<String>)map.get(item.getSel_szg_code());
			//然后根据对应的下标确定其位置
			int szIdIndex = sizeListWithSzgIdList.indexOf(item.getSel_sz_code());
			//根据下标位置得到，其对应的尺码数量，然后进行累加
			subArray[szIdIndex] += item.getSel_amount();
		}
		return lstRes;
	}
	
	public static Map<String, Object> getJsonSizeData(List<T_Base_SizeList> sizeGroupList,List<T_Batch_SellList> dataList){
		if (sizeGroupList != null && sizeGroupList.size() > 0) {
			int[] maxColNumArray = new int[1];
            Map<String, List<String>> map = new HashMap<String, List<String>>();
            List<List<String>> lstRes = SizeHorizontalVO.getSizeModeTitleInfoList(map, maxColNumArray, sizeGroupList);
            List<T_Batch_SizeCommon> list = getSizeModeList(dataList,maxColNumArray[0],map);
            int sizeMaxSize = maxColNumArray[0];
			int[] totalSizeBySingle = new int[sizeMaxSize];
			Collections.sort(lstRes, new Comparator<List<String>>() {
				@Override
				public int compare(List<String> o1, List<String> o2) {
					return o1.size() > o2.size() ? -1 : 0;
				}
			});
			int Amount_Total = 0;
			double RetailMoney_Total = 0d;
			double UnitMoney_Total = 0d;
			double CostMoney_Total = 0d;
			double RebateMoney_Total = 0d;
			HashMap<String, Object> sizeGroupMap = new HashMap<String, Object>();
			List<Map<String, Object>> values = new ArrayList<Map<String, Object>>();
			HashMap<String, Object> entry = null;
			if (list == null || list.isEmpty()) {
			} else {
				for (T_Batch_SizeCommon item : list) {
					entry = new HashMap<String, Object>();
					entry.put("id", item.getId());
					entry.put("pd_code", item.getPd_code());
					entry.put("pd_no", item.getPd_no());
					entry.put("pd_name", item.getPd_name());
					entry.put("pd_unit", item.getPd_unit());
					entry.put("cr_code", item.getCr_code());
					entry.put("cr_name", item.getCr_name());
					entry.put("br_code", item.getBr_code());
					entry.put("br_name", item.getBr_name());
					entry.put("sel_pi_type", item.getPd_type());
					int[] amounts = item.getSingle_amount();
					if (lstRes == null || lstRes.size() <= 0) {
					} else {
						String[] header = null;
						int amountSize = 0;
						if (map.containsKey(item.getSzg_code())) {
							amountSize = map.get(item.getSzg_code()).size();
						}
						List<String> headers = lstRes.get(0);
						for (int i = 0; i < headers.size(); i++) {
							header = headers.get(i).split("_AND_");
							if (i >= amountSize) {
								entry.put(header[0], "");
							} else {
								entry.put(header[0],amounts[i] == 0 ? "": amounts[i] + "");
								totalSizeBySingle[i] = totalSizeBySingle[i] + amounts[i];
							}
						}
					}
					entry.put("Amount_Total", item.getTotalamount());
					entry.put("unit_price", item.getUnitprice());
					entry.put("unit_money", item.getUnitmoney());
					entry.put("retail_price", item.getRetailprice());
					entry.put("retail_money", item.getRetailmoney());
					entry.put("cost_price", item.getCostprice());
					entry.put("cost_money", item.getCostmoney());
					entry.put("rebate_price", item.getRebateprice());
					entry.put("rebate_money", item.getRebatemoney());
					entry.put("sizeGroupCode", item.getSzg_code());
					Amount_Total += item.getTotalamount();
					UnitMoney_Total += item.getUnitmoney();
					RetailMoney_Total += item.getRetailmoney();
					CostMoney_Total += item.getCostmoney();
					RebateMoney_Total += item.getRebatemoney();
					values.add(entry);
				}
			}
			if (lstRes == null || lstRes.size() <= 0) {
			} else {
				String[] header = null;
                List<String> headers = lstRes.get(0);
                for (int i = 0; i < headers.size(); i++) {
                    header = headers.get(i).split("_AND_");
                    sizeGroupMap.put(header[0], totalSizeBySingle[i] == 0 ? "" : totalSizeBySingle[i]);
                }
			}
			HashMap<String, Object> userData = new HashMap<String, Object>();
			userData.put("pd_no", "合计：");
			userData.put("Amount_Total", Amount_Total);
			userData.put("unit_money", UnitMoney_Total);
			userData.put("retail_money", RetailMoney_Total);
			userData.put("cost_money", CostMoney_Total);
			userData.put("rebate_money", RebateMoney_Total);
			userData.putAll(sizeGroupMap);
			
			Map<String, Object> resultMap = new HashMap<String, Object>();
			resultMap.put("data", values);
			resultMap.put("userData", userData);
			return resultMap;
		}
		return null;
	}

	@SuppressWarnings("rawtypes")
	public static List<T_Batch_SellList> convertMap2Model(Map<String, Object> data,Integer se_type,T_Sys_User user){
		List<T_Batch_SellList> resultList = new ArrayList<T_Batch_SellList>();
		List products = (List)data.get("products");
		if (products == null || products.size() == 0) {
			return resultList;
		}
		T_Batch_SellList model = null;
		for (int i = 0; i < products.size(); i++) {
			Map product = (Map)products.get(i);
			Integer sel_pi_type = Integer.parseInt(StringUtil.trimString(product.get("sel_pi_type")));
			model = new T_Batch_SellList();
			model.setSel_pd_code(StringUtil.trimString(product.get("pd_code")));
			model.setSel_cr_code(StringUtil.trimString(product.get("cr_code")));
			model.setSel_sz_code(StringUtil.trimString(product.get("sz_code")));
			model.setSel_szg_code(StringUtil.trimString(product.get("pd_szg_code")));
			model.setSel_br_code(StringUtil.trimString(product.get("br_code")));
			model.setSel_sub_code(model.getSel_pd_code()+model.getSel_cr_code()+model.getSel_sz_code()+model.getSel_br_code());
			model.setSel_amount(Integer.parseInt(StringUtil.trimString(product.get("amount"))));
			if(sel_pi_type.intValue() == 1){//赠品
				model.setSel_unitprice(0d);
			}else {
				model.setSel_unitprice(Double.parseDouble(StringUtil.trimString(product.get("unitPrice"))));
			}
			model.setSel_retailprice(Double.parseDouble(StringUtil.trimString(product.get("retailPrice"))));
			model.setSel_costprice(Double.parseDouble(StringUtil.trimString(product.get("costPrice"))));
			model.setSel_rebateprice(Double.parseDouble(StringUtil.trimString(product.get("rebatePrice"))));
			model.setSel_remark("");
			model.setSel_pi_type(sel_pi_type);
			model.setSel_type(se_type);
			model.setSel_us_id(user.getUs_id());
			model.setCompanyid(user.getCompanyid());
			model.setOperate_type(StringUtil.trimString(product.get("operate_type")));
			resultList.add(model);
		}
		return resultList;
	}
	
	@SuppressWarnings("rawtypes")
	public static List<String[]> convertMap2Model_import(Map<String, Object> data,T_Sys_User user){
		List<String[]> resultList = new ArrayList<String[]>();
		List products = (List)data.get("datas");
		if (products == null || products.size() == 0) {
			return resultList;
		}
		String[] item = null;
		for (int i = 0; i < products.size(); i++) {
			Map product = (Map)products.get(i);
			item = new String[2];
			item[0] = StringUtil.trimString(product.get("barCode"));
			item[1] = StringUtil.trimString(product.get("amount"));
			resultList.add(item);
		}
		return resultList;
	}
	
	public static Map<String,Object> buildPrintJson(Map<String,Object> paramMap,Integer printMode){
		Map<String,Object> htmlMap = null;
		PrintVO.List2Map(paramMap);
		if(printMode != null){
			if(printMode.intValue() == 0){
				htmlMap = getPrintListHTML(paramMap);
			}
			if(printMode.intValue() == 1){
				htmlMap = getPrintSizeHTML(paramMap);
			}
			if(printMode.intValue() == 2){
				htmlMap = getPrintSumHTML(paramMap);
			}
		}
		return htmlMap;
	}
	
	@SuppressWarnings("unchecked")
	public static String[] getHeadFieldHTML(Map<String,Object> paramMap){
		StringBuffer headHtml = new StringBuffer(""),footHtml = new StringBuffer("");
		List<T_Sys_PrintField> printFields = (List<T_Sys_PrintField>)paramMap.get("fields");
		Map<String, Object> printSetMap = (Map<String, Object>)paramMap.get("printSetMap");
		T_Batch_Sell sell= (T_Batch_Sell)paramMap.get("sell");
		T_Batch_Client client = (T_Batch_Client)paramMap.get("client");
		T_Sys_User user = (T_Sys_User)paramMap.get("user");
		headHtml.append("<table cellpadding=\"0\" cellspacing=\"0\" class=\"head-print\">");
		headHtml.append("<tbody>");
		headHtml.append("<tr>");
		headHtml.append("<td colspan=\"3\" class=\"head-title\"><b class=\"head-font\">"+printSetMap.get(PrintUtil.PrintSet.PAGE_TITLE)+"&nbsp;</b></td>");
		headHtml.append("</tr>");
		int index = -1;
		double i = 1;
		for(T_Sys_PrintField printField:printFields){
			String colspan = "";
			if(printField.getSpf_position().intValue() == PrintUtil.FALSE){
				if(index != printField.getSpf_line()){
					if(index == -1){
						headHtml.append("<tr>");
					}else{
						i ++;
						headHtml.append("</tr><tr>");
					}
				}
				if(printField.getSpf_colspan() > 1){
					colspan = "colspan='"+printField.getSpf_colspan()+"'";
				}
				if(PrintUtil.PrintField.MAKEDATE.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(sell.getSe_make_date()).append("</td>");
				}
				if(PrintUtil.PrintField.MANAGER.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(sell.getSe_manager()).append("</td>");
				}
				if(PrintUtil.PrintField.NUMBER.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(sell.getSe_number()).append("</td>");
				}
				if(PrintUtil.PrintField.CLIENT_NAME.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(sell.getClient_name()).append("</td>");
				}
				if(PrintUtil.PrintField.TEL.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(client.getCi_tel()).append("</td>");
				}
				if(PrintUtil.PrintField.MOBILE.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(client.getCi_mobile()).append("</td>");
				}
				if(PrintUtil.PrintField.DEPOT_NAME.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(sell.getDepot_name()).append("</td>");
				}
				if(PrintUtil.PrintField.LINKMAN.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(client.getCi_man()).append("</td>");
				}
				if(PrintUtil.PrintField.HAND_NUMBER.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(sell.getSe_handnumber()).append("</td>");
				}
				if(PrintUtil.PrintField.AMOUNT.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(sell.getSe_amount()).append("</td>");
				}
				if(PrintUtil.PrintField.MONEY.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(PriceLimitUtil.checkPriceLimit(sell.getSe_money(),CommonUtil.PRICE_LIMIT_BATCH, user.getUs_limit())).append("</td>");
				}
				if(PrintUtil.PrintField.REBATE_MONEY.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(PriceLimitUtil.checkPriceLimit(sell.getSe_rebatemoney(),CommonUtil.PRICE_LIMIT_BATCH, user.getUs_limit())).append("</td>");
				}
				if(PrintUtil.PrintField.STREAM_MONEY.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(PriceLimitUtil.checkPriceLimit(sell.getSe_stream_money(),CommonUtil.PRICE_LIMIT_BATCH, user.getUs_limit())).append("</td>");
				}
				if(PrintUtil.PrintField.RECEIVABLE.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(PriceLimitUtil.checkPriceLimit(sell.getSe_receivable(),CommonUtil.PRICE_LIMIT_BATCH, user.getUs_limit())).append("</td>");
				}
				if(PrintUtil.PrintField.DEPT.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(client.getCi_receivable()-client.getCi_received()).append("</td>");
				}
				if(PrintUtil.PrintField.ADDRESS.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(client.getCi_addr()).append("</td>");
				}
				if(PrintUtil.PrintField.REMARK.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(sell.getSe_remark()).append("</td>");
				}
				index = printField.getSpf_line();
			}else if(printField.getSpf_position().intValue() == PrintUtil.TRUE){
				if(PrintUtil.PrintField.END_MAKER.equals(printField.getSpf_code())){
					footHtml.append("<span style='float:left;width:45mm;'>").append(printField.getSpf_name()).append("：").append(StringUtil.trimString(sell.getSe_maker())).append("</span>");
				}
				if(PrintUtil.PrintField.END_APPROVER.equals(printField.getSpf_code())){
					footHtml.append("<span style='float:left;width:45mm;'>").append(printField.getSpf_name()).append("：").append(StringUtil.trimString("TODO")).append("</span>");
				}
			}
		}
		headHtml.append("</tr>");
		//标题高度
		double titleheight = Double.parseDouble((String)printSetMap.get(PrintUtil.PrintSet.TITLE_HEIGHT));
		//表头每行高度
		double head_height = Double.parseDouble((String)printSetMap.get(PrintUtil.PrintSet.HEAD_HEIGHT));
		//表格数据与表头间距
		double table_top = Double.parseDouble((String)printSetMap.get(PrintUtil.PrintSet.TABLE_TOP));
		double page_top = Double.parseDouble((String)printSetMap.get(PrintUtil.PrintSet.PAGE_TOP));
		//总高度
		double sum_height = page_top + table_top + titleheight + (head_height * i);
		headHtml.append(" </tbody>");
		headHtml.append("</table>");
		String[] html = new String[3];
		html[0] = headHtml.toString();
		html[1] = footHtml.toString();
		html[2] = sum_height + "";
		return html;
	}
	
	public static boolean IsExistBras(List<T_Batch_SellList> sellLists){
		boolean flag = false;
		for(T_Batch_SellList item :sellLists){
			if(StringUtil.isNotEmpty(item.getSel_br_code())){
				flag = true;
				break;
			}
		}
		return flag;
	}
	private static String formatOdl_pi_type(Integer odl_pi_type){
		if(odl_pi_type == null){
			return "";
		}
		if(odl_pi_type.intValue() == 1){
			return "赠品";
		}
		return "";
	}
	
	@SuppressWarnings("unchecked")
	public static Map<String,Object> getPrintListHTML(Map<String,Object> paramMap){
		Map<String,Object> htmlMap = null;
		StringBuffer html = new StringBuffer("");
		try{
			T_Sys_Print print = (T_Sys_Print)paramMap.get("print");
			List<T_Sys_PrintData> printDatas = (List<T_Sys_PrintData>)(paramMap.get("datas"));
			Map<String, Object> printSetMap = (Map<String, Object>)paramMap.get("printSetMap");
			Map<String, Object> printDataMap = (Map<String, Object>)paramMap.get("printDataMap");
			List<T_Batch_SellList> sellList = (List<T_Batch_SellList>)paramMap.get("sellList");
			T_Sys_User user = (T_Sys_User)paramMap.get("user");
			
			//获取表头与表尾的数据
			String[] htmlArray = getHeadFieldHTML(paramMap);
			
			html.append("<table cellpadding=\"4\" cellspacing=\"0\" class=\"data-print\">");
			html.append("<thead>");
			html.append("<tr>");
			
			int dataSize = printDatas.size();
			boolean isHas = IsExistBras(sellList);
			for(T_Sys_PrintData printData:printDatas){
				if(PrintUtil.PrintData.BR_NAME.equals(printData.getSpd_code())){
					if(isHas){
						html.append("<th>");
						html.append("<div style=\"width='"+printData.getSpd_width()+"mm'\">");
						html.append(printData.getSpd_name());
						html.append("</div>");
						html.append("</th>");
					}
				}else{
					html.append("<th>");
					html.append("<div style=\"width='"+printData.getSpd_width()+"mm'\">");
					html.append(printData.getSpd_name());
					html.append("</div>");
					html.append("</th>");
				}
			}
			html.append("</tr>");
			html.append("</thead>");
			int i = 0,amount = 0;
			double unitMoney = 0d,retailMoney = 0d,costMoney = 0d,rebateMoney = 0d;
			html.append("<tbody>");			
			for(T_Batch_SellList item: sellList){
				html.append("<tr>");
				for (int j = 0; j < printDatas.size(); j++) {
					if(PrintUtil.PrintData.INDEX.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.INDEX)).append(">");
						html.append((i+1)+"</td>");
					}
					if(PrintUtil.PrintData.PD_NO.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_NO)).append(">");
						html.append(item.getPd_no()+"</td>");
					}
					if(PrintUtil.PrintData.PD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_NAME)).append(">");
						html.append(item.getPd_name()+"</td>");
					}
					if(PrintUtil.PrintData.BD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.BD_NAME)).append(">");
						html.append(item.getBd_name()+"</td>");
					}
					if(PrintUtil.PrintData.TP_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.TP_NAME)).append(">");
						html.append(item.getTp_name()+"</td>");
					}
					if(PrintUtil.PrintData.PD_SEASON.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_SEASON)).append(">");
						html.append(item.getPd_season()+"</td>");
					}
					if(PrintUtil.PrintData.PD_YEAR.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_YEAR)).append(">");
						html.append(item.getPd_year()+"</td>");
					}
					if(PrintUtil.PrintData.CR_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.CR_NAME)).append(">");
						html.append(item.getCr_name()+"</td>");
					}
					if(PrintUtil.PrintData.BR_NAME.equals(printDatas.get(j).getSpd_code())){
						if (isHas){
							html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.BR_NAME)).append(">");
							html.append(item.getBr_name()+"</td>");
						}
					}
					if(PrintUtil.PrintData.SZ_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.SZ_NAME)).append(">");
						html.append(item.getSz_name()+"</td>");
					}
					if(PrintUtil.PrintData.PD_UNIT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_UNIT)).append(">");
						html.append(item.getPd_unit()+"</td>");
					}
					if(PrintUtil.PrintData.AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.AMOUNT)).append(">");
						html.append(item.getSel_amount()+"</td>");
						amount += item.getSel_amount();
					}
					if(PrintUtil.PrintData.UNIT_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.UNIT_PRICE)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getSel_unitprice(),CommonUtil.PRICE_LIMIT_BATCH, user.getUs_limit())+"</td>");
					}
					if(PrintUtil.PrintData.UNIT_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.UNIT_MONEY)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getSel_unitmoney(),CommonUtil.PRICE_LIMIT_BATCH, user.getUs_limit())+"</td>");
						unitMoney += item.getSel_unitmoney();
					}
					if(PrintUtil.PrintData.RETAIL_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.RETAIL_PRICE)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getSel_retailprice(),CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit())+"</td>");
					}
					if(PrintUtil.PrintData.RETAIL_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.RETAIL_MONEY)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getSel_retailmoney(),CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit())+"</td>");
						retailMoney += item.getSel_retailmoney();
					}
					if(PrintUtil.PrintData.COST_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.COST_PRICE)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getSel_costprice(),CommonUtil.PRICE_LIMIT_COST, user.getUs_limit())+"</td>");
					}
					if(PrintUtil.PrintData.COST_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.COST_MONEY)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getSel_costmoney(),CommonUtil.PRICE_LIMIT_COST, user.getUs_limit())+"</td>");
						costMoney += item.getSel_costmoney();
					}
					if(PrintUtil.PrintData.REBATE_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.REBATE_PRICE)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getSel_rebateprice(),CommonUtil.PRICE_LIMIT_BATCH, user.getUs_limit())+"</td>");
					}
					if(PrintUtil.PrintData.REBATE_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.REBATE_MONEY)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getSel_rebatemoney(),CommonUtil.PRICE_LIMIT_BATCH, user.getUs_limit())+"</td>");
						rebateMoney += item.getSel_rebatemoney();
					}
					if(PrintUtil.PrintData.REBATE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.REBATE)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getSel_unitprice()/item.getSel_retailprice(), CommonUtil.PRICE_LIMIT_BATCH, CommonUtil.PRICE_LIMIT_RETAIL,user.getUs_limit()));
						html.append("</td>");
					}
					if(PrintUtil.PrintData.GIFT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.GIFT)).append(">");
						html.append(formatOdl_pi_type(item.getSel_pi_type())+"</td>");
					}
					if(PrintUtil.PrintData.REMARK.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.REMARK)).append(">");
						html.append(item.getSel_remark()+"</td>");
					}
				}
				html.append("</tr>");
				i++;
			}
			html.append("</tbody>");
			String subtotal = (String)printSetMap.get(PrintUtil.PrintSet.TABLE_SUBTOTAL);
			if(String.valueOf(PrintUtil.TRUE).equals(subtotal)){
				html.append("<tfoot>");
				//列表模式下当前页合计算输出
				html.append("<tr>");
				for (int j = 0; j < printDatas.size(); j++) {
					if(PrintUtil.PrintData.INDEX.equals(printDatas.get(j).getSpd_code())){
						html.append("<td>小计</td>");
					}
					if(PrintUtil.PrintData.PD_NO.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.BD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.TP_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_SEASON.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_YEAR.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.CR_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.BR_NAME.equals(printDatas.get(j).getSpd_code())){
						if (isHas){
							html.append("<td></td>");
						}
					}
					if(PrintUtil.PrintData.SZ_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_UNIT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;' tdata='subSum' format='#'>###</td>");
					}
					if(PrintUtil.PrintData.UNIT_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.UNIT_MONEY.equals(printDatas.get(j).getSpd_code())){
						if(PriceLimitUtil.hasPriceLimit(CommonUtil.PRICE_LIMIT_ENTER, user.getUs_limit())){
							html.append("<td style='text-align:right;' tdata='subSum' format='#,##0.00'>###</td>");
						}else {
							html.append("<td style='text-align:right;'>***</td>");
						}
					}
					if(PrintUtil.PrintData.RETAIL_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.RETAIL_MONEY.equals(printDatas.get(j).getSpd_code())){
						if(PriceLimitUtil.hasPriceLimit(CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit())){
							html.append("<td style='text-align:right;' tdata='subSum' format='#,##0.00'>###</td>");
						}else {
							html.append("<td style='text-align:right;'>***</td>");
						}
					}
					if(PrintUtil.PrintData.COST_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.COST_MONEY.equals(printDatas.get(j).getSpd_code())){
						if(PriceLimitUtil.hasPriceLimit(CommonUtil.PRICE_LIMIT_COST, user.getUs_limit())){
							html.append("<td style='text-align:right;' tdata='subSum' format='#,##0.00'>###</td>");
						}else {
							html.append("<td style='text-align:right;'>***</td>");
						}
					}
					if(PrintUtil.PrintData.REBATE_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.REBATE_MONEY.equals(printDatas.get(j).getSpd_code())){
						if(PriceLimitUtil.hasPriceLimit(CommonUtil.PRICE_LIMIT_BATCH, user.getUs_limit())){
							html.append("<td style='text-align:right;' tdata='subSum' format='#,##0.00'>###</td>");
						}else {
							html.append("<td style='text-align:right;'>***</td>");
						}
					}
					if(PrintUtil.PrintData.REBATE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.GIFT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.REMARK.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
				}
				html.append("</tr>");
			}
			
			String total = (String)printSetMap.get(PrintUtil.PrintSet.TABLE_TOTAL);
			if(String.valueOf(PrintUtil.TRUE).equals(total)){
				html.append("<tr>");
				for (int j = 0; j < printDatas.size(); j++) {
					if(PrintUtil.PrintData.INDEX.equals(printDatas.get(j).getSpd_code())){
						html.append("<td>总计</td>");
					}
					if(PrintUtil.PrintData.PD_NO.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.BD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.TP_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_SEASON.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_YEAR.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.CR_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.BR_NAME.equals(printDatas.get(j).getSpd_code())){
						if (isHas){
							html.append("<td></td>");
						}
					}
					if(PrintUtil.PrintData.SZ_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_UNIT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+amount+"</td>");
					}
					if(PrintUtil.PrintData.UNIT_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.UNIT_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+PriceLimitUtil.checkPriceLimit(unitMoney, CommonUtil.PRICE_LIMIT_BATCH, user.getUs_limit())+"</td>");
					}
					if(PrintUtil.PrintData.RETAIL_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.RETAIL_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+PriceLimitUtil.checkPriceLimit(retailMoney, CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit())+"</td>");
					}
					
					if(PrintUtil.PrintData.COST_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.COST_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+PriceLimitUtil.checkPriceLimit(costMoney, CommonUtil.PRICE_LIMIT_COST, user.getUs_limit())+"</td>");
					}
					if(PrintUtil.PrintData.REBATE_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.REBATE_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+PriceLimitUtil.checkPriceLimit(rebateMoney, CommonUtil.PRICE_LIMIT_BATCH, user.getUs_limit())+"</td>");
					}
					
					if(PrintUtil.PrintData.REBATE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.GIFT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.REMARK.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
				}
				html.append("</tr>");
			}
			html.append("<tr>");
			int colspan = 0;
			if(isHas){
				colspan = dataSize+1;
			}else{
				colspan = dataSize;
			}
			html.append("<td colspan='"+colspan+"'>");
			html.append(htmlArray[1]);
			html.append("<div style='float:right;'>");
			html.append("<span  tdata='pageNO' format=\"#\">第#页</span>");
			html.append("<b>/</b>");
			html.append("<span tdata='pageCount' format=\"#\">总#页</span>");
			html.append("</div>");
			String end_endshow = (String)printSetMap.get(PrintUtil.PrintSet.END_ENDSHOW);
			if(String.valueOf(PrintUtil.FALSE).equals(end_endshow)){
				if(StringUtil.isNotEmpty(print.getSp_remark())){
					html.append("<br/><div style='float:left;'>");
	                html.append(StringUtil.trimString(print.getSp_remark()));
	                html.append("</div>");
				}
            }
			html.append("</td>");
			html.append("</tfoot>");
			html.append("</table>");
			htmlMap = new HashMap<String, Object>(27);
			htmlMap.put("listHtml", html.toString());
			htmlMap.put("headHtml", htmlArray[0]);
			htmlMap.put("styleHtml", PrintVO.getPrintStyleHTML(paramMap));
			htmlMap.put("totalHeadHeight", htmlArray[2]);
			htmlMap.putAll(printSetMap);
		}catch(Exception e){
			e.printStackTrace();
		}
		return htmlMap;
	}
	
	@SuppressWarnings("unchecked")
	public static Map<String,Object> getPrintSizeHTML(Map<String,Object> paramMap){
		StringBuffer html = new StringBuffer("");
		Map<String,Object> htmlMap = null;
		try{
			T_Sys_Print print = (T_Sys_Print)paramMap.get("print");
			List<T_Sys_PrintData> printDatas = (List<T_Sys_PrintData>)(paramMap.get("datas"));
			Map<String, Object> printSetMap = (Map<String, Object>)paramMap.get("printSetMap");
			Map<String, Object> printDataMap = (Map<String, Object>)paramMap.get("printDataMap");
			List<T_Batch_SellList> sellList = (List<T_Batch_SellList>)paramMap.get("sellList");
			List<T_Base_SizeList> sizeGroupList = (List<T_Base_SizeList>)paramMap.get("sizeGroupList");
			T_Sys_User user = (T_Sys_User)paramMap.get("user");
			int[] maxColNumArray = new int[1];
			Map<String, List<String>> sizeMap = new HashMap<String, List<String>>();
			List<List<String>> listResult = SizeHorizontalVO.getSizeModeTitleInfoList(sizeMap, maxColNumArray, sizeGroupList);
			//获取表头与表尾的数据
			String[] htmlArray = getHeadFieldHTML(paramMap);
			List<T_Batch_SizeCommon> sizeCommonList = getSizeModeList(sellList, maxColNumArray[0], sizeMap);
			if(listResult.size()==0 || sizeCommonList.size()==0) {
				return null;
			}
			int[] totalSizeBySingle = new int[maxColNumArray[0]];
			boolean isHas = IsExistBras(sellList);
			
			//根据商家ID和货号查询出对应的尺码组信息在title输出
		    int rowSize = listResult.size()+1;
		    html.append("<table cellpadding=\"4\" cellspacing=\"0\" class=\"data-print\">");
		    html.append("<thead>");
		    html.append("<tr>");
			for(T_Sys_PrintData printData:printDatas){
				if(PrintUtil.PrintData.BR_NAME.equals(printData.getSpd_code())){
					if(isHas){
						html.append("<th rowspan=\""+rowSize+"\">");
						html.append(printData.getSpd_name());
						html.append("</th>");
					}
		    	}else if(PrintUtil.PrintData.SZ_NAME.equals(printData.getSpd_code())){
		    		html.append("<th colspan=\""+maxColNumArray[0]+"\">");
					html.append(printData.getSpd_name());
					html.append("</th>");
				}else if(!PrintUtil.PrintData.REMARK.equals(printData.getSpd_code())){
					html.append("<th rowspan=\""+(rowSize)+"\">");
					html.append(printData.getSpd_name());
					html.append("</th>");
				}
			}

		    html.append("</tr>"); 
		    if(printDataMap.containsKey(PrintUtil.PrintData.SZ_NAME)){
			    for(int i=0;i<listResult.size();i++){  
			    	html.append("<tr>");
			    	List<String> sizeList = (List<String>)listResult.get(i);
			    	for (int k = 0;k<maxColNumArray[0];k++){
			    		html.append("<th>");//入库单详细信息
			    		if (k < sizeList.size()){
			    			html.append(sizeList.get(k).split("_AND_")[1]);
			    		}else{
			    			html.append("-");
			    		}
			    		html.append("</th>");
			    	}
			    	html.append("</tr>");
			    }
		    }
		    html.append("</thead>");
		    int i = 0,amount = 0;
		    double unitMoney = 0d,retailMoney = 0d,costMoney = 0d,rebateMoney = 0d;
			for (T_Batch_SizeCommon item : sizeCommonList) {
				i++;
				html.append("<tr>");
				for (int j = 0; j < printDatas.size(); j++) {
					if(PrintUtil.PrintData.INDEX.equals(printDatas.get(j).getSpd_code())){
						html.append("<td nowrap").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.INDEX)).append(">");
						html.append((i+1)+"</td>");
					}
					if(PrintUtil.PrintData.PD_NO.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_NO)).append(">");
						html.append(item.getPd_no()+"</td>");
					}
					if(PrintUtil.PrintData.PD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_NAME)).append(">");
						html.append(item.getPd_name()+"</td>");
					}
					if(PrintUtil.PrintData.BD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.BD_NAME)).append(">");
						html.append(item.getBd_name()+"</td>");
					}
					if(PrintUtil.PrintData.TP_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.TP_NAME)).append(">");
						html.append(item.getTp_name()+"</td>");
					}
					if(PrintUtil.PrintData.PD_SEASON.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_SEASON)).append(">");
						html.append(item.getPd_season()+"</td>");
					}
					if(PrintUtil.PrintData.PD_YEAR.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_YEAR)).append(">");
						html.append(item.getPd_year()+"</td>");
					}
					if(PrintUtil.PrintData.CR_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.CR_NAME)).append(">");
						html.append(item.getCr_name()+"</td>");
					}
					if(PrintUtil.PrintData.BR_NAME.equals(printDatas.get(j).getSpd_code())){
						if (isHas){
							html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.BR_NAME)).append(">");
							html.append(item.getBr_name()+"</td>");
						}
					}
					if(PrintUtil.PrintData.SZ_NAME.equals(printDatas.get(j).getSpd_code())){
						
						int[] arr = item.getSingle_amount();
						for(int k = 0;k< maxColNumArray[0];k++){	
							html.append("<td align='center'>");
							if (k<arr.length){
								html.append(arr[k] == 0 ? "" : arr[k]);//单个尺码数量
								totalSizeBySingle[k] = totalSizeBySingle[k]+ arr[k];
							}
							html.append("</td>");
						}
					}
					if(PrintUtil.PrintData.PD_UNIT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_UNIT)).append(">");
						html.append(item.getPd_unit()+"</td>");
					}
					
					if(PrintUtil.PrintData.AMOUNT.equals(printDatas.get(j).getSpd_code())){//数量合计
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.AMOUNT)).append(">");
						html.append(item.getTotalamount()+"</td>");
						amount += item.getTotalamount();
					}
					
					if(PrintUtil.PrintData.UNIT_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.UNIT_PRICE)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getUnitprice(),CommonUtil.PRICE_LIMIT_ENTER, user.getUs_limit()));
						html.append("</td>");
					}
					if(PrintUtil.PrintData.UNIT_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.UNIT_MONEY)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getUnitmoney(),CommonUtil.PRICE_LIMIT_ENTER, user.getUs_limit()));
						html.append("</td>");
						unitMoney += item.getUnitmoney();
					}
					if(PrintUtil.PrintData.RETAIL_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.RETAIL_PRICE)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getRetailprice(),CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit()));
						html.append("</td>");
					}
					if(PrintUtil.PrintData.RETAIL_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.RETAIL_MONEY)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getRetailmoney(),CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit()));
						html.append("</td>");
						retailMoney += item.getRetailmoney();
					}
					if(PrintUtil.PrintData.COST_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.COST_PRICE)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getCostprice(),CommonUtil.PRICE_LIMIT_COST, user.getUs_limit()));
						html.append("</td>");
					}
					if(PrintUtil.PrintData.COST_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.COST_MONEY)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getCostmoney(),CommonUtil.PRICE_LIMIT_COST, user.getUs_limit()));
						html.append("</td>");
						costMoney += item.getCostmoney();
					}
					if(PrintUtil.PrintData.REBATE_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.REBATE_PRICE)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getRebateprice(),CommonUtil.PRICE_LIMIT_BATCH, user.getUs_limit()));
						html.append("</td>");
					}
					if(PrintUtil.PrintData.REBATE_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.REBATE_MONEY)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getRebatemoney(),CommonUtil.PRICE_LIMIT_BATCH, user.getUs_limit()));
						html.append("</td>");
						rebateMoney += item.getRebatemoney();
					}
					if(PrintUtil.PrintData.REBATE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.REBATE)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getUnitprice()/item.getRetailprice(),CommonUtil.PRICE_LIMIT_ENTER,CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit()));
						html.append("</td>");
					}
					if(PrintUtil.PrintData.GIFT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.GIFT)).append(">");
						html.append(formatOdl_pi_type(Integer.parseInt(item.getPd_type()))+"</td>");
					}
				}
				html.append("</tr>");
			}
			html.append("<tfoot>");
			boolean hasMaxSize = false;
			html.append("<tr>");
			for (int j = 0; j < printDatas.size(); j++) {
				if(PrintUtil.PrintData.INDEX.equals(printDatas.get(j).getSpd_code())){
					html.append("<td>总计</td>");
				}
				if(PrintUtil.PrintData.PD_NO.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.PD_NAME.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.BD_NAME.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.TP_NAME.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.PD_SEASON.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.PD_YEAR.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.CR_NAME.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.BR_NAME.equals(printDatas.get(j).getSpd_code())){
					if (isHas){
						html.append("<td></td>");
					}
				}
				if(PrintUtil.PrintData.SZ_NAME.equals(printDatas.get(j).getSpd_code())){
					for(int k = 0;k< maxColNumArray[0];k++){	
						hasMaxSize = true;
						html.append("<td style=\"text-align:center;\">");
						if(totalSizeBySingle[k] != 0){
							html.append(totalSizeBySingle[k]);
						}
						html.append("</td>");
					}	
					if (!hasMaxSize){
						html.append("<td>");html.append("</td>");
					} 
				}
				if(PrintUtil.PrintData.PD_UNIT.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.AMOUNT.equals(printDatas.get(j).getSpd_code())){//数量合计
					html.append("<td style=\"text-align:right;\">");
					html.append(amount+"</td>");
				}
				if(PrintUtil.PrintData.UNIT_PRICE.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.UNIT_MONEY.equals(printDatas.get(j).getSpd_code())){
					html.append("<td style=\"text-align:right;\"><span>"+PriceLimitUtil.checkPriceLimit(unitMoney,CommonUtil.PRICE_LIMIT_BATCH, user.getUs_limit())+"</span></td>");
				}
				if(PrintUtil.PrintData.RETAIL_PRICE.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.RETAIL_MONEY.equals(printDatas.get(j).getSpd_code())){
					html.append("<td style=\"text-align:right;\"><span>"+PriceLimitUtil.checkPriceLimit(retailMoney,CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit())+"</span></td>");
				}
				
				if(PrintUtil.PrintData.COST_PRICE.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.COST_MONEY.equals(printDatas.get(j).getSpd_code())){
					html.append("<td style=\"text-align:right;\"><span>"+PriceLimitUtil.checkPriceLimit(costMoney,CommonUtil.PRICE_LIMIT_COST, user.getUs_limit())+"</span></td>");
				}
				if(PrintUtil.PrintData.REBATE_PRICE.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.REBATE_MONEY.equals(printDatas.get(j).getSpd_code())){
					html.append("<td style=\"text-align:right;\"><span>"+PriceLimitUtil.checkPriceLimit(rebateMoney,CommonUtil.PRICE_LIMIT_BATCH, user.getUs_limit())+"</span></td>");
				}
				
				if(PrintUtil.PrintData.REBATE.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.GIFT.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
			}
			html.append("</tr>");
			
			int dataSize = printDatas.size()-1;
			if(printDataMap.containsKey(PrintUtil.PrintData.REMARK)){
				dataSize -= 1;
			}
			int colspan = 0;
			if(isHas){
				colspan = dataSize;
			}else if(printDataMap.containsKey(PrintUtil.PrintData.BR_NAME)){
				colspan = dataSize-1;
			}else{
				colspan = dataSize;
			}
			colspan += maxColNumArray[0];
			html.append("<td colspan='"+colspan+"'>");
			html.append(htmlArray[1]);
			html.append("<div style='float:right;'>");
			html.append("<span tdata='pageNO' format=\"#\">第#页</span>");
			html.append("<b>/</b>");
			html.append("<span tdata='pageCount' format=\"#\">总#页</span>");
			html.append("</div>");
			String end_endshow = (String)printSetMap.get(PrintUtil.PrintSet.END_ENDSHOW);
			if(String.valueOf(PrintUtil.FALSE).equals(end_endshow)){
				if(StringUtil.isNotEmpty(print.getSp_remark())){
					html.append("<br/><div style='float:left;'>");
	                html.append(StringUtil.trimString(print.getSp_remark()));
	                html.append("</div>");
				}
            }
			html.append("</td>");
			html.append("</tr>");
			html.append("</tfoot>");
			html.append("</table>");
			htmlMap = new HashMap<String, Object>(27);
			htmlMap.put("listHtml", html.toString());
			htmlMap.put("headHtml", htmlArray[0]);
			htmlMap.put("styleHtml", PrintVO.getPrintStyleHTML(paramMap));
			htmlMap.put("totalHeadHeight", htmlArray[2]);
			htmlMap.putAll(printSetMap);
		}catch(Exception e){
			e.printStackTrace();
		}
		return htmlMap;
	}
	
	@SuppressWarnings("unchecked")
	public static Map<String,Object> getPrintSumHTML(Map<String,Object> paramMap){
		Map<String,Object> htmlMap = null;
		StringBuffer html = new StringBuffer("");
		try{
			T_Sys_Print print = (T_Sys_Print)paramMap.get("print");
			List<T_Sys_PrintData> printDatas = (List<T_Sys_PrintData>)(paramMap.get("datas"));
			Map<String, Object> printSetMap = (Map<String, Object>)paramMap.get("printSetMap");
			Map<String, Object> printDataMap = (Map<String, Object>)paramMap.get("printDataMap");
			List<T_Batch_SellList> sellList = (List<T_Batch_SellList>)paramMap.get("sellList");
			T_Sys_User user = (T_Sys_User)paramMap.get("user");
			
			//获取表头与表尾的数据
			String[] htmlArray = getHeadFieldHTML(paramMap);
			
			html.append("<table cellpadding=\"4\" cellspacing=\"0\" class=\"data-print\">");
			html.append("<thead>");
			html.append("<tr>");
			
			int dataSize = printDatas.size();
			for(T_Sys_PrintData printData:printDatas){
				if(PrintUtil.PrintData.BR_NAME.equals(printData.getSpd_code())||
						PrintUtil.PrintData.CR_NAME.equals(printData.getSpd_code())||
						PrintUtil.PrintData.SZ_NAME.equals(printData.getSpd_code())){//汇总模式不显示尺码杯型颜色
				}else{
					html.append("<th>");
					html.append("<div style=\"width='"+printData.getSpd_width()+"mm'\">");
					html.append(printData.getSpd_name());
					html.append("</div>");
					html.append("</th>");
				}
			}
			html.append("</tr>");
			html.append("</thead>");
			int i = 0,amount = 0;
			double unitMoney = 0d,retailMoney = 0d,costMoney = 0d,rebateMoney = 0d;
			html.append("<tbody>");			
			for(T_Batch_SellList item: sellList){
				html.append("<tr>");
				for (int j = 0; j < printDatas.size(); j++) {
					if(PrintUtil.PrintData.INDEX.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.INDEX)).append(">");
						html.append((i+1)+"</td>");
					}
					if(PrintUtil.PrintData.PD_NO.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_NO)).append(">");
						html.append(item.getPd_no()+"</td>");
					}
					if(PrintUtil.PrintData.PD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_NAME)).append(">");
						html.append(item.getPd_name()+"</td>");
					}
					if(PrintUtil.PrintData.BD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.BD_NAME)).append(">");
						html.append(item.getBd_name()+"</td>");
					}
					if(PrintUtil.PrintData.TP_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.TP_NAME)).append(">");
						html.append(item.getTp_name()+"</td>");
					}
					if(PrintUtil.PrintData.PD_SEASON.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_SEASON)).append(">");
						html.append(item.getPd_season()+"</td>");
					}
					if(PrintUtil.PrintData.PD_YEAR.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_YEAR)).append(">");
						html.append(item.getPd_year()+"</td>");
					}
					if(PrintUtil.PrintData.PD_UNIT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_UNIT)).append(">");
						html.append(item.getPd_unit()+"</td>");
					}
					if(PrintUtil.PrintData.AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.AMOUNT)).append(">");
						html.append(item.getSel_amount()+"</td>");
						amount += item.getSel_amount();
					}
					if(PrintUtil.PrintData.UNIT_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.UNIT_PRICE)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getSel_unitprice(),CommonUtil.PRICE_LIMIT_BATCH, user.getUs_limit()));
						html.append("</td>");
					}
					if(PrintUtil.PrintData.UNIT_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.UNIT_MONEY)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getSel_unitmoney(),CommonUtil.PRICE_LIMIT_BATCH, user.getUs_limit()));
						html.append("</td>");
						unitMoney += item.getSel_unitmoney();
					}
					if(PrintUtil.PrintData.RETAIL_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.RETAIL_PRICE)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getSel_retailprice(),CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit()));
						html.append("</td>");
					}
					if(PrintUtil.PrintData.RETAIL_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.RETAIL_MONEY)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getSel_retailmoney(),CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit()));
						html.append("</td>");
						retailMoney += item.getSel_retailmoney();
					}
					if(PrintUtil.PrintData.COST_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.COST_PRICE)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getSel_costprice(),CommonUtil.PRICE_LIMIT_COST, user.getUs_limit()));
						html.append("</td>");
					}
					if(PrintUtil.PrintData.COST_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.COST_MONEY)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getSel_costmoney(),CommonUtil.PRICE_LIMIT_COST, user.getUs_limit()));
						html.append("</td>");
						costMoney += item.getSel_costmoney();
					}
					if(PrintUtil.PrintData.REBATE_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.REBATE_PRICE)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getSel_rebateprice(),CommonUtil.PRICE_LIMIT_BATCH, user.getUs_limit()));
						html.append("</td>");
					}
					if(PrintUtil.PrintData.REBATE_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.REBATE_MONEY)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getSel_rebatemoney(),CommonUtil.PRICE_LIMIT_BATCH, user.getUs_limit()));
						html.append("</td>");
						rebateMoney += item.getSel_rebatemoney();
					}
					if(PrintUtil.PrintData.REBATE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.REBATE)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getSel_unitprice()/item.getSel_retailprice(),CommonUtil.PRICE_LIMIT_ENTER,CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit()));
						html.append("</td>");
					}
					if(PrintUtil.PrintData.GIFT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.GIFT)).append(">");
						html.append(formatOdl_pi_type(item.getSel_pi_type())+"</td>");
					}
					if(PrintUtil.PrintData.REMARK.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.REMARK)).append(">");
						html.append(item.getSel_remark()+"</td>");
					}
				}
				html.append("</tr>");
				i++;
			}
			html.append("</tbody>");
			String subtotal = (String)printSetMap.get(PrintUtil.PrintSet.TABLE_SUBTOTAL);
			if(String.valueOf(PrintUtil.TRUE).equals(subtotal)){
				html.append("<tfoot>");
				//列表模式下当前页合计算输出
				html.append("<tr>");
				for (int j = 0; j < printDatas.size(); j++) {
					if(PrintUtil.PrintData.INDEX.equals(printDatas.get(j).getSpd_code())){
						html.append("<td>小计</td>");
					}
					if(PrintUtil.PrintData.PD_NO.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.BD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.TP_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_SEASON.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_YEAR.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_UNIT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;' tdata='subSum' format='#'>###</td>");
					}
					if(PrintUtil.PrintData.UNIT_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.UNIT_MONEY.equals(printDatas.get(j).getSpd_code())){
						if(PriceLimitUtil.hasPriceLimit(CommonUtil.PRICE_LIMIT_BATCH, user.getUs_limit())){
							html.append("<td style='text-align:right;' tdata='subSum' format='#,##0.00'>###</td>");
						}else {
							html.append("<td style='text-align:right;'>***</td>");
						}
					}
					if(PrintUtil.PrintData.RETAIL_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.RETAIL_MONEY.equals(printDatas.get(j).getSpd_code())){
						if(PriceLimitUtil.hasPriceLimit(CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit())){
							html.append("<td style='text-align:right;' tdata='subSum' format='#,##0.00'>###</td>");
						}else {
							html.append("<td style='text-align:right;'>***</td>");
						}
					}
					if(PrintUtil.PrintData.COST_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.COST_MONEY.equals(printDatas.get(j).getSpd_code())){
						if(PriceLimitUtil.hasPriceLimit(CommonUtil.PRICE_LIMIT_COST, user.getUs_limit())){
							html.append("<td style='text-align:right;' tdata='subSum' format='#,##0.00'>###</td>");
						}else {
							html.append("<td style='text-align:right;'>***</td>");
						}
					}
					if(PrintUtil.PrintData.REBATE_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.REBATE_MONEY.equals(printDatas.get(j).getSpd_code())){
						if(PriceLimitUtil.hasPriceLimit(CommonUtil.PRICE_LIMIT_BATCH, user.getUs_limit())){
							html.append("<td style='text-align:right;' tdata='subSum' format='#,##0.00'>###</td>");
						}else {
							html.append("<td style='text-align:right;'>***</td>");
						}
					}
					if(PrintUtil.PrintData.REBATE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.GIFT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.REMARK.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
				}
				html.append("</tr>");
			}
			
			String total = (String)printSetMap.get(PrintUtil.PrintSet.TABLE_TOTAL);
			if(String.valueOf(PrintUtil.TRUE).equals(total)){
				html.append("<tr>");
				for (int j = 0; j < printDatas.size(); j++) {
					if(PrintUtil.PrintData.INDEX.equals(printDatas.get(j).getSpd_code())){
						html.append("<td>总计</td>");
					}
					if(PrintUtil.PrintData.PD_NO.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.BD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.TP_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_SEASON.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_YEAR.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_UNIT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+amount+"</td>");
					}
					if(PrintUtil.PrintData.UNIT_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.UNIT_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+PriceLimitUtil.checkPriceLimit(unitMoney,CommonUtil.PRICE_LIMIT_ENTER, user.getUs_limit())+"</td>");
					}
					if(PrintUtil.PrintData.RETAIL_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.RETAIL_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+PriceLimitUtil.checkPriceLimit(retailMoney,CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit())+"</td>");
					}
					if(PrintUtil.PrintData.COST_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.COST_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+PriceLimitUtil.checkPriceLimit(costMoney,CommonUtil.PRICE_LIMIT_COST, user.getUs_limit())+"</td>");
					}
					if(PrintUtil.PrintData.REBATE_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.REBATE_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+PriceLimitUtil.checkPriceLimit(rebateMoney,CommonUtil.PRICE_LIMIT_BATCH, user.getUs_limit())+"</td>");
					}
					if(PrintUtil.PrintData.REBATE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.GIFT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.REMARK.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
				}
				html.append("</tr>");
			}
			html.append("<tr>");
			int colspan = 0;
			colspan = dataSize-2;
			html.append("<td colspan='"+colspan+"'>");
			html.append(htmlArray[1]);
			html.append("<div style='float:right;'>");
			html.append("<span  tdata='pageNO' format=\"#\">第#页</span>");
			html.append("<b>/</b>");
			html.append("<span tdata='pageCount' format=\"#\">总#页</span>");
			html.append("</div>");
			String end_endshow = (String)printSetMap.get(PrintUtil.PrintSet.END_ENDSHOW);
			if(String.valueOf(PrintUtil.FALSE).equals(end_endshow)){
				if(StringUtil.isNotEmpty(print.getSp_remark())){
					html.append("<br/><div style='float:left;'>");
	                html.append(StringUtil.trimString(print.getSp_remark()));
	                html.append("</div>");
				}
            }
			html.append("</td>");
			html.append("</tfoot>");
			html.append("</table>");
			htmlMap = new HashMap<String, Object>(27);
			htmlMap.put("listHtml", html.toString());
			htmlMap.put("headHtml", htmlArray[0]);
			htmlMap.put("styleHtml", PrintVO.getPrintStyleHTML(paramMap));
			htmlMap.put("totalHeadHeight", htmlArray[2]);
			htmlMap.putAll(printSetMap);
		}catch(Exception e){
			e.printStackTrace();
		}
		return htmlMap;
	}
	
}
