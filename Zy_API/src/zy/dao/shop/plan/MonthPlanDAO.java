package zy.dao.shop.plan;

import java.util.List;
import java.util.Map;

import zy.entity.shop.plan.T_Shop_MonthPlan;
import zy.entity.shop.plan.T_Shop_MonthPlan_Day;

public interface MonthPlanDAO {
	Integer count(Map<String,Object> params);
	List<T_Shop_MonthPlan> list(Map<String,Object> params);
	T_Shop_MonthPlan load(Integer mp_id);
	T_Shop_MonthPlan check(String shop_code, Integer year,Integer month, Integer companyid);
	T_Shop_MonthPlan check(String mp_number, Integer companyid);
	void save(T_Shop_MonthPlan plan, List<T_Shop_MonthPlan_Day> days);
	List<T_Shop_MonthPlan_Day> listDay(String number,Integer companyid);
	void updateApprove(T_Shop_MonthPlan plan);
	void del(String mp_number, Integer companyid);
}
