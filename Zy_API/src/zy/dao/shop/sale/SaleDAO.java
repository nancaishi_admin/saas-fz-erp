package zy.dao.shop.sale;

import java.util.List;
import java.util.Map;

import zy.entity.sell.cash.T_Sell_ShopList;
import zy.entity.shop.sale.T_Shop_Sale;

public interface SaleDAO {
	Integer count(Map<String,Object> param);
	List<T_Shop_Sale> list(Map<String,Object> param);
	List<T_Shop_Sale> list(Map<String,Object> param,int limit);
	Integer queryPriority(Map<String,Object> param);
	void save(Map<String,Object> param);
	Map<String, Object> load(Map<String, Object> param);
	T_Shop_Sale check(String code,Integer companyid);
	void updateApprove(T_Shop_Sale sale);
	void del(String ss_code,Integer companyid);
	Integer rateList_count(Map<String,Object> paramMap);
	List<T_Sell_ShopList> rateList_list(Map<String,Object> paramMap);
	Map<String, Object> rateList_sum(Map<String, Object> paramMap);
	List<T_Sell_ShopList> totalList_list(Map<String,Object> paramMap);
	Map<String, Object> totalList_sum(Map<String, Object> paramMap);
}
