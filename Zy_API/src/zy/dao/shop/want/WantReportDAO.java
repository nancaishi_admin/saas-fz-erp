package zy.dao.shop.want;

import java.util.List;
import java.util.Map;

import zy.dto.shop.want.WantDetailReportDto;
import zy.dto.shop.want.WantReportDto;

public interface WantReportDAO {
	Integer countWantReport(Map<String,Object> params);
	Map<String, Object> sumWantReport(Map<String, Object> params);
	List<WantReportDto> listWantReport(Map<String,Object> params);
	Integer countWantDetailReport(Map<String,Object> params);
	Map<String, Object> sumWantDetailReport(Map<String, Object> params);
	List<WantDetailReportDto> listWantDetailReport(Map<String,Object> params);
}
