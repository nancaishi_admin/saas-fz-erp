package zy.dao.shop.report.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.shop.report.ShopReportDAO;
import zy.entity.sell.day.T_Sell_Day;
import zy.util.CommonUtil;
import zy.util.NumberUtil;
import zy.util.StringUtil;
@Repository
public class ShopReportDAOImpl extends BaseDaoImpl implements ShopReportDAO{

	@Override
	public List<T_Sell_Day> flow(Map<String, Object> paramMap) {
		String shop_type = StringUtil.trimString(paramMap.get(CommonUtil.SHOP_TYPE));
		Object sh_shop_code = paramMap.get("sh_shop_code");
		Object begindate = paramMap.get("begindate");
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT COUNT(1) sh_count,HOUR(sh_sysdate) da_date,sh_id da_id,");
		sql.append("SUM(sh_amount) sh_amount,SUM(sh_money) sh_money");
		sql.append(" FROM t_sell_shop t");
		sql.append(" JOIN t_base_shop s");
		sql.append(" ON s.sp_code=t.sh_shop_code");
		sql.append(" AND s.companyid=t.companyid");
		if(StringUtil.isNotEmpty(sh_shop_code)){
			paramMap.put("shop_codes", StringUtil.parseList(StringUtil.trimString(sh_shop_code)));
			sql.append(" AND sp_code IN (:shop_codes)");
		}
		if("1".equals(shop_type) || "2".equals(shop_type)){//总公司、分公司
			sql.append(getShopSQL(shop_type, 1));
		}else{//自营、加盟、合伙
			sql.append(" AND sp_code = :shop_code");
		}
		sql.append(" WHERE 1=1");
		if(StringUtil.isNotEmpty(begindate)){
			sql.append(" AND t.sh_date BETWEEN :begindate AND :enddate");
		}
		sql.append(" AND t.companyid=:companyid");
		sql.append(" GROUP BY HOUR(sh_sysdate)");
		List<T_Sell_Day> list = namedParameterJdbcTemplate.query(sql.toString(), paramMap, 
				new BeanPropertyRowMapper<>(T_Sell_Day.class));
		sql.setLength(0);
		sql.append(" SELECT re_id da_id,HOUR(re_date) da_date,COUNT(1) da_come,re_type da_type");
		sql.append(" FROM t_sell_receive t");
		sql.append(" JOIN t_base_shop s");
		sql.append(" ON s.sp_code=t.re_shop_code");
		sql.append(" AND s.companyid=t.companyid");
		if(StringUtil.isNotEmpty(sh_shop_code)){
			paramMap.put("shop_codes", StringUtil.parseList(StringUtil.trimString(sh_shop_code)));
			sql.append(" AND sp_code IN (:shop_codes)");
		}
		if("1".equals(shop_type) || "2".equals(shop_type)){//总公司、分公司
			sql.append(getShopSQL(shop_type, 1));
		}else{//自营、加盟、合伙
			sql.append(" AND sp_code = :shop_code");
		}
		sql.append(" WHERE t.companyid=:companyid");
		if(StringUtil.isNotEmpty(begindate)){
			sql.append(" AND t.re_date BETWEEN :begindate AND :enddate");
		}
		sql.append(" GROUP BY HOUR(re_date),re_type");
		List<T_Sell_Day> comeList = namedParameterJdbcTemplate.query(sql.toString(), paramMap, 
				new BeanPropertyRowMapper<>(T_Sell_Day.class));
		sql.setLength(0);
		sql.append(" SELECT tr_id da_id,HOUR(tr_date) da_date,COUNT(1) da_try");
		sql.append(" FROM t_sell_try t");
		sql.append(" JOIN t_base_shop s");
		sql.append(" ON s.sp_code=t.tr_shop_code");
		sql.append(" AND s.companyid=t.companyid");
		if(StringUtil.isNotEmpty(sh_shop_code)){
			paramMap.put("shop_codes", StringUtil.parseList(StringUtil.trimString(sh_shop_code)));
			sql.append(" AND sp_code IN (:shop_codes)");
		}
		if("1".equals(shop_type) || "2".equals(shop_type)){//总公司、分公司
			sql.append(getShopSQL(shop_type, 1));
		}else{//自营、加盟、合伙
			sql.append(" AND sp_code = :shop_code");
		}
		sql.append(" WHERE t.companyid=:companyid");
		if(StringUtil.isNotEmpty(begindate)){
			sql.append(" AND t.tr_date BETWEEN :begindate AND :enddate");
		}
		sql.append(" GROUP BY HOUR(tr_date)");
		List<T_Sell_Day> tryList = namedParameterJdbcTemplate.query(sql.toString(), paramMap, 
				new BeanPropertyRowMapper<>(T_Sell_Day.class));
		buildList(list,comeList,tryList);
		return list;
	}
	
	
	private void buildList(List<T_Sell_Day> list,List<T_Sell_Day> comeList,List<T_Sell_Day> tryList){
		if(null != list && list.size() > 0){
			for(T_Sell_Day day:list){
				if(null != comeList && comeList.size() > 0){
					for(T_Sell_Day come:comeList){
						if(come.getDa_date().equals(day.getDa_date())){
							if(come.getDa_type().equals("1")){
								day.setDa_receive(NumberUtil.toInteger(come.getDa_come()));
							}else{
								day.setDa_come(NumberUtil.toInteger(come.getDa_come()));
							}
						}else{
							/*T_Sell_Day item = new T_Sell_Day();
							if(come.getDa_type().equals("1")){
								item.setDa_receive(NumberUtil.toInteger(come.getDa_come()));
							}else{
								item.setDa_come(NumberUtil.toInteger(come.getDa_come()));
							}
							item.setDa_date(come.getDa_date());
							item.setSh_amount(0);
							item.setSh_money(0d);
							item.setSh_count(0);
							item.setDa_id(come.getDa_id());
							list.add(item);*/
						}
					}
				}
				if(null != tryList && tryList.size() > 0){
					for(T_Sell_Day trys:tryList){
						if(trys.getDa_date().equals(day.getDa_date())){
							day.setDa_try(NumberUtil.toInteger(trys.getDa_try()));
						}else{
							/*T_Sell_Day item = new T_Sell_Day();
							day.setDa_try(trys.getDa_try());
							item.setDa_date(trys.getDa_date());
							item.setDa_come(0);
							item.setDa_receive(0);
							item.setSh_amount(0);
							item.setSh_money(0d);
							item.setSh_count(0);
							list.add(item);*/
						}
					}
				}
			}
		}
	}
}
