package zy.dao.shop.kpiassess;

import java.util.List;
import java.util.Map;

import zy.entity.base.emp.T_Base_EmpGroupList;
import zy.entity.shop.kpiassess.T_Shop_KpiAssess;
import zy.entity.shop.kpiassess.T_Shop_KpiAssessList;
import zy.entity.shop.kpiassess.T_Shop_KpiAssessReward;

public interface KpiAssessDAO {
	Integer count(Map<String,Object> params);
	List<T_Shop_KpiAssess> list(Map<String,Object> params);
	List<T_Shop_KpiAssess> list4cashier(Map<String, Object> params);
	T_Shop_KpiAssess load(Integer ka_id);
	T_Shop_KpiAssess load(String ka_number,Integer companyid);
	List<Map<String, Object>> loadHeader(String number,Integer companyid);
	List<T_Shop_KpiAssessList> loadDetail(String number,Integer companyid);
	List<T_Shop_KpiAssessList> statDetail(Map<String, Object> params);
	void save(T_Shop_KpiAssess kpiAssess,List<T_Shop_KpiAssessList> kpiAssessLists);
	void complete(T_Shop_KpiAssess kpiAssess);
	void completeDetail(List<T_Shop_KpiAssessList> kpiAssessLists);
	List<T_Base_EmpGroupList> loadEmpByGroup(List<String> eg_codes,Integer companyid);
	void saveReward(List<T_Shop_KpiAssessReward> rewards);
	void del(String ka_number, Integer companyid) ;
}
