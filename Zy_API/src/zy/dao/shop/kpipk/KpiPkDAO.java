package zy.dao.shop.kpipk;

import java.util.List;
import java.util.Map;

import zy.entity.base.emp.T_Base_EmpGroupList;
import zy.entity.shop.kpipk.T_Shop_KpiPk;
import zy.entity.shop.kpipk.T_Shop_KpiPkList;
import zy.entity.shop.kpipk.T_Shop_KpiPkReward;

public interface KpiPkDAO {
	Integer count(Map<String,Object> params);
	List<T_Shop_KpiPk> list(Map<String,Object> params);
	List<T_Shop_KpiPk> list4cashier(Map<String, Object> params);
	T_Shop_KpiPk load(Integer kp_id);
	T_Shop_KpiPk load(String kp_number,Integer companyid);
	List<Map<String, Object>> loadHeader(String number,Integer companyid);
	List<T_Shop_KpiPkList> loadDetail(String number,Integer companyid);
	List<T_Shop_KpiPkList> statDetail(Map<String, Object> params);
	void save(T_Shop_KpiPk kpiPk,List<T_Shop_KpiPkList> kpiPkLists,List<T_Shop_KpiPkReward> kpiPkRewards);
	void complete(T_Shop_KpiPk kpiPk);
	void completeDetail(List<T_Shop_KpiPkList> kpiPkLists);
	List<T_Base_EmpGroupList> loadEmpByGroup(List<String> eg_codes,Integer companyid);
	void deleteReward(String kp_number, Integer companyid);
	void saveReward(List<T_Shop_KpiPkReward> rewards);
	List<T_Shop_KpiPkReward> listReward(String number, Integer companyid);
	void del(String kp_number, Integer companyid);
}
