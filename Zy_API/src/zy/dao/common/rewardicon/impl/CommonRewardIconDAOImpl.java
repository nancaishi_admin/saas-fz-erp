package zy.dao.common.rewardicon.impl;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.common.rewardicon.CommonRewardIconDAO;
import zy.entity.common.rewardicon.Common_RewardIcon;

@Repository
public class CommonRewardIconDAOImpl extends BaseDaoImpl implements CommonRewardIconDAO{
	@Override
	public List<Common_RewardIcon> list() {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT ri_id,ri_icon,ri_style");
		sql.append(" FROM common_rewardicon");
		return namedParameterJdbcTemplate.query(sql.toString(),new MapSqlParameterSource(),
				new BeanPropertyRowMapper<>(Common_RewardIcon.class));
	}
}
