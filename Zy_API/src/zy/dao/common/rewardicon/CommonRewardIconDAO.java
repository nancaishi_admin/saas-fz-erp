package zy.dao.common.rewardicon;

import java.util.List;

import zy.entity.common.rewardicon.Common_RewardIcon;

public interface CommonRewardIconDAO {
	List<Common_RewardIcon> list();
}
