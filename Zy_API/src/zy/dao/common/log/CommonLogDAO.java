package zy.dao.common.log;

import java.util.List;
import java.util.Map;

import zy.entity.common.log.Common_Log;

public interface CommonLogDAO {
	Integer count(Map<String, Object> params);
	List<Common_Log> list(Map<String, Object> params);
	List<Common_Log> list();
}
