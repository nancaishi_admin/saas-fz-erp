package zy.dao.common.city.impl;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.common.city.CityDAO;
import zy.entity.common.city.Common_City;

@Repository
public class CityDAOImpl extends BaseDaoImpl implements CityDAO{

	@Override
	public List<Common_City> queryProvince() {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT cy_id,cy_name");
		sql.append(" FROM common_city t");
		sql.append(" WHERE 1 = 1");
		sql.append(" AND t.cy_upid=:id");
		return namedParameterJdbcTemplate.query(sql.toString(), new MapSqlParameterSource().addValue("id", 0),
				new BeanPropertyRowMapper<>(Common_City.class));
	}

	@Override
	public List<Common_City> queryCity(Integer id) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT cy_id,cy_name");
		sql.append(" FROM common_city t");
		sql.append(" WHERE 1 = 1");
		sql.append(" AND t.cy_upid=:id");
		return namedParameterJdbcTemplate.query(sql.toString(), new MapSqlParameterSource().addValue("id", id),
				new BeanPropertyRowMapper<>(Common_City.class));
	}

	@Override
	public List<Common_City> queryTown(Integer id) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT cy_id,cy_name");
		sql.append(" FROM common_city t");
		sql.append(" WHERE 1 = 1");
		sql.append(" AND t.cy_upid=:id");
		return namedParameterJdbcTemplate.query(sql.toString(), new MapSqlParameterSource().addValue("id", id),
				new BeanPropertyRowMapper<>(Common_City.class));
	}

}
