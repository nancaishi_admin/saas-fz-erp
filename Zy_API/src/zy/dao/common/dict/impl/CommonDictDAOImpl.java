package zy.dao.common.dict.impl;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.common.dict.CommonDictDAO;
import zy.entity.common.dict.Common_Dict;

@Repository
public class CommonDictDAOImpl extends BaseDaoImpl implements CommonDictDAO{

	@Override
	public List<Common_Dict> list(String dt_type) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT dt_id,dt_code,dt_name,dt_type");
		sql.append(" FROM common_dict");
		sql.append(" WHERE dt_type = :dt_type");
		return namedParameterJdbcTemplate.query(sql.toString(),new MapSqlParameterSource().addValue("dt_type", dt_type),
				new BeanPropertyRowMapper<>(Common_Dict.class));
	}

}
