package zy.dao.common.dict;

import java.util.List;

import zy.entity.common.dict.Common_Dict;

public interface CommonDictDAO {
	List<Common_Dict> list(String dt_type);
}
