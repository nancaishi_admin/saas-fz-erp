package zy.dao.common.type.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.common.type.CommonTypeDAO;
import zy.entity.common.type.Common_Type;
@Repository
public class CommonTypeDAOImpl extends BaseDaoImpl implements CommonTypeDAO{

	@Override
	public List<Common_Type> list(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" select ty_id,ty_name");
		sql.append(" from common_type t");
		sql.append(" where instr(ty_type,:shop_type)>0");
		sql.append(" and instr(ty_ver,:version)>0");
		List<Common_Type> list = namedParameterJdbcTemplate.query(sql.toString(), param, 
				new BeanPropertyRowMapper<>(Common_Type.class));
		return list;
	}

	@Override
	public List<Common_Type> sub_list(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" select ty_id,ty_name");
		sql.append(" from common_type t");
		sql.append(" where instr(ty_sub,:shop_type)>0");
		sql.append(" and instr(ty_ver,:version)>0");
		List<Common_Type> list = namedParameterJdbcTemplate.query(sql.toString(), param, 
				new BeanPropertyRowMapper<>(Common_Type.class));
		return list;
	}

}
