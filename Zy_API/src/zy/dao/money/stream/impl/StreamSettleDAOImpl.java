package zy.dao.money.stream.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.money.stream.StreamSettleDAO;
import zy.entity.base.stream.T_Base_Stream_Bill;
import zy.entity.money.stream.T_Money_StreamSettle;
import zy.entity.money.stream.T_Money_StreamSettleList;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;

@Repository
public class StreamSettleDAOImpl extends BaseDaoImpl implements StreamSettleDAO{
	@Override
	public Integer count(Map<String, Object> params) {
		Object st_ar_state = params.get("st_ar_state");
		Object begindate = params.get("begindate");
		Object enddate = params.get("enddate");
		Object st_stream_code = params.get("st_stream_code");
		Object st_manager = params.get("st_manager");
		Object st_number = params.get("st_number");
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT COUNT(1)");
		sql.append(" FROM t_money_streamsettle t");
		sql.append(" JOIN t_base_stream se ON se_code = st_stream_code AND t.companyid = se.companyid");
		sql.append(" WHERE 1=1");
		if (StringUtil.isNotEmpty(st_ar_state)) {
			sql.append(" AND st_ar_state = :st_ar_state ");
		}
		if (StringUtil.isNotEmpty(begindate)) {
			sql.append(" AND st_date >= :begindate ");
		}
		if (StringUtil.isNotEmpty(enddate)) {
			sql.append(" AND st_date <= :enddate ");
		}
		if (StringUtil.isNotEmpty(st_stream_code)) {
			sql.append(" AND st_stream_code = :st_stream_code ");
		}
		if (StringUtil.isNotEmpty(st_manager)) {
			sql.append(" AND st_manager = :st_manager ");
		}
		if (StringUtil.isNotEmpty(st_number)) {
			sql.append(" AND INSTR(st_number,:st_number) > 0 ");
		}
		sql.append(" AND se_sp_code = :shop_code ");//上级店铺编号
		sql.append(" AND t.companyid=:companyid");
		return namedParameterJdbcTemplate.queryForObject(sql.toString(), params, Integer.class);
	}

	@Override
	public List<T_Money_StreamSettle> list(Map<String, Object> params) {
		Object sidx = params.get(CommonUtil.SIDX);
		Object sord = params.get(CommonUtil.SORD);
		Object st_ar_state = params.get("st_ar_state");
		Object begindate = params.get("begindate");
		Object enddate = params.get("enddate");
		Object st_stream_code = params.get("st_stream_code");
		Object st_manager = params.get("st_manager");
		Object st_number = params.get("st_number");
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT st_id,st_number,st_date,st_stream_code,st_maker,st_manager,st_ba_code,st_discount_money,st_paid,st_remark,");
		sql.append(" st_ar_state,st_ar_date,st_sysdate,st_us_id,t.companyid,se_name AS stream_name,");
		sql.append(" (SELECT ba_name FROM t_money_bank ba WHERE ba_code = st_ba_code AND ba.companyid = t.companyid LIMIT 1) AS ba_name");
		sql.append(" FROM t_money_streamsettle t");
		sql.append(" JOIN t_base_stream se ON se_code = st_stream_code AND t.companyid = se.companyid");
		sql.append(" WHERE 1=1");
		if (StringUtil.isNotEmpty(st_ar_state)) {
			sql.append(" AND st_ar_state = :st_ar_state ");
		}
		if (StringUtil.isNotEmpty(begindate)) {
			sql.append(" AND st_date >= :begindate ");
		}
		if (StringUtil.isNotEmpty(enddate)) {
			sql.append(" AND st_date <= :enddate ");
		}
		if (StringUtil.isNotEmpty(st_stream_code)) {
			sql.append(" AND st_stream_code = :st_stream_code ");
		}
		if (StringUtil.isNotEmpty(st_manager)) {
			sql.append(" AND st_manager = :st_manager ");
		}
		if (StringUtil.isNotEmpty(st_number)) {
			sql.append(" AND INSTR(st_number,:st_number) > 0 ");
		}
		sql.append(" AND se_sp_code = :shop_code ");//上级店铺编号
		sql.append(" AND t.companyid=:companyid");
		if(sidx != null && !"".equals(sidx)){
			sql.append(" ORDER BY ").append(sidx).append(" ").append(sord);
		}else {
			sql.append(" ORDER BY st_id DESC");
		}
		sql.append(" LIMIT :start,:end");
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Money_StreamSettle.class));
	}

	@Override
	public T_Money_StreamSettle load(Integer st_id) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT st_id,st_number,st_date,st_stream_code,st_maker,st_manager,st_ba_code,st_discount_money,st_paid,st_remark,");
		sql.append(" st_ar_state,st_ar_date,st_sysdate,st_us_id,t.companyid,se_name AS stream_name,");
		sql.append(" (SELECT ba_name FROM t_money_bank ba WHERE ba_code = st_ba_code AND ba.companyid = t.companyid LIMIT 1) AS ba_name");
		sql.append(" FROM t_money_streamsettle t");
		sql.append(" JOIN t_base_stream se ON se_code = st_stream_code AND t.companyid = se.companyid");
		sql.append(" WHERE st_id = :st_id");
		sql.append(" LIMIT 1");
		try {
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), new MapSqlParameterSource().addValue("st_id", st_id),
					new BeanPropertyRowMapper<>(T_Money_StreamSettle.class));
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public T_Money_StreamSettle load(String number, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT st_id,st_number,st_date,st_stream_code,st_maker,st_manager,st_ba_code,st_discount_money,st_paid,st_remark,");
		sql.append(" st_ar_state,st_ar_date,st_sysdate,st_us_id,t.companyid,se_name AS stream_name,");
		sql.append(" (SELECT ba_name FROM t_money_bank ba WHERE ba_code = st_ba_code AND ba.companyid = t.companyid LIMIT 1) AS ba_name");
		sql.append(" FROM t_money_streamsettle t");
		sql.append(" JOIN t_base_stream se ON se_code = st_stream_code AND t.companyid = se.companyid");
		sql.append(" WHERE st_number = :st_number");
		sql.append(" AND t.companyid = :companyid");
		sql.append(" LIMIT 1");
		try {
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new MapSqlParameterSource().addValue("st_number", number).addValue("companyid", companyid),
					new BeanPropertyRowMapper<>(T_Money_StreamSettle.class));
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public T_Money_StreamSettle check(String number, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT st_id,st_number,st_date,st_stream_code,st_maker,st_manager,st_ba_code,st_discount_money,st_paid,st_remark,");
		sql.append(" st_ar_state,st_ar_date,st_sysdate,st_us_id,t.companyid");
		sql.append(" FROM t_money_streamsettle t");
		sql.append(" WHERE st_number = :st_number");
		sql.append(" AND companyid = :companyid");
		sql.append(" LIMIT 1");
		try {
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new MapSqlParameterSource().addValue("st_number", number).addValue("companyid", companyid),
					new BeanPropertyRowMapper<>(T_Money_StreamSettle.class));
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public T_Money_StreamSettle check_settle(String se_code, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT st_id");
		sql.append(" FROM t_money_streamsettle");
		sql.append(" WHERE st_ar_state != 1");
		sql.append(" AND st_stream_code = :se_code");
		sql.append(" AND companyid = :companyid");
		sql.append(" LIMIT 1");
		try {
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new MapSqlParameterSource().addValue("se_code", se_code).addValue("companyid", companyid),
					new BeanPropertyRowMapper<>(T_Money_StreamSettle.class));
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<T_Money_StreamSettleList> load_bill_forsavetemp(String se_code,Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT seb_money AS stl_payable,");
		sql.append(" seb_payabled AS stl_payabled,");
		sql.append(" seb_discount_money AS stl_discount_money_yet,");
		sql.append(" seb_payable AS stl_unpayable,");
		sql.append(" 0 AS stl_discount_money,");
		sql.append(" 0 AS stl_real_pay,");
		sql.append(" seb_number AS stl_bill_number");
		sql.append(" FROM t_base_stream_bill");
		sql.append(" WHERE 1=1");
		sql.append(" AND seb_se_code = :se_code");
		sql.append(" AND seb_pay_state != 2");
		sql.append(" AND companyid = :companyid");
		sql.append(" ORDER BY seb_id ASC");
		return namedParameterJdbcTemplate.query(sql.toString(), 
				new MapSqlParameterSource().addValue("se_code", se_code).addValue("companyid", companyid),
				new BeanPropertyRowMapper<>(T_Money_StreamSettleList.class));
	}
	
	@Override
	public List<T_Money_StreamSettleList> temp_list(Map<String, Object> params) {
		Object sidx = params.get(CommonUtil.SIDX);
		Object sord = params.get(CommonUtil.SORD);
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT stl_id,stl_payable,stl_payabled,stl_discount_money_yet,stl_unpayable,");
		sql.append(" stl_discount_money,stl_real_pay,stl_bill_number,stl_remark,stl_us_id,companyid");
		sql.append(" FROM t_money_streamsettlelist_temp t");
		sql.append(" WHERE 1=1");
		sql.append(" AND stl_us_id = :stl_us_id");
		sql.append(" AND t.companyid = :companyid");
		if(StringUtil.isNotEmpty(sidx)){
			sql.append(" ORDER BY ").append(sidx).append(" ").append(sord);
		}else {
			sql.append(" ORDER BY stl_id ASC");
		}
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Money_StreamSettleList.class));
	}
	
	@Override
	public List<T_Money_StreamSettleList> temp_list_forsave(Integer us_id,Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT stl_id,stl_payable,stl_payabled,stl_discount_money_yet,stl_unpayable,");
		sql.append(" stl_discount_money,stl_real_pay,stl_bill_number,stl_remark,stl_us_id,companyid");
		sql.append(" FROM t_money_streamsettlelist_temp t");
		sql.append(" WHERE 1=1");
		sql.append(" AND stl_discount_money+stl_real_pay != 0");
		sql.append(" AND stl_us_id = :stl_us_id");
		sql.append(" AND t.companyid = :companyid");
		sql.append(" ORDER BY stl_id ASC");
		return namedParameterJdbcTemplate.query(sql.toString(), 
				new MapSqlParameterSource().addValue("stl_us_id", us_id).addValue("companyid", companyid),
				new BeanPropertyRowMapper<>(T_Money_StreamSettleList.class));
	}
	
	@Override
	public void temp_save(List<T_Money_StreamSettleList> temps) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" INSERT INTO t_money_streamsettlelist_temp");
		sql.append(" (stl_payable,stl_payabled,stl_discount_money_yet,stl_unpayable,");
		sql.append(" stl_discount_money,stl_real_pay,stl_bill_number,stl_remark,stl_us_id,companyid)");
		sql.append(" VALUES ");
		sql.append(" (:stl_payable,:stl_payabled,:stl_discount_money_yet,:stl_unpayable,");
		sql.append(" :stl_discount_money,:stl_real_pay,:stl_bill_number,:stl_remark,:stl_us_id,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(temps.toArray()));
	}
	
	@Override
	public void temp_update(List<T_Money_StreamSettleList> temps) {
		StringBuffer sql = new StringBuffer("");
		sql.append("UPDATE t_money_streamsettlelist_temp");
		sql.append(" SET stl_discount_money = :stl_discount_money");
		sql.append(" ,stl_real_pay = :stl_real_pay");
		sql.append(" WHERE 1=1");
		sql.append(" AND stl_id = :stl_id");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(temps.toArray()));
	}

	@Override
	public void temp_clear(Integer us_id,Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append("DELETE FROM t_money_streamsettlelist_temp");
		sql.append(" WHERE 1=1");
		sql.append(" AND stl_us_id = :stl_us_id");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(),
				new MapSqlParameterSource().addValue("stl_us_id", us_id).addValue("companyid", companyid));
	}
	
	@Override
	public void temp_updateDiscountMoney(T_Money_StreamSettleList temp) {
		StringBuffer sql = new StringBuffer("");
		sql.append("UPDATE t_money_streamsettlelist_temp");
		sql.append(" SET stl_discount_money = :stl_discount_money");
		if(temp.getStl_real_pay() != null){
			sql.append(" ,stl_real_pay = :stl_real_pay");
		}
		sql.append(" WHERE 1=1");
		sql.append(" AND stl_id = :stl_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(temp));
	}

	@Override
	public void temp_updateRealMoney(T_Money_StreamSettleList temp) {
		StringBuffer sql = new StringBuffer("");
		sql.append("UPDATE t_money_streamsettlelist_temp");
		sql.append(" SET stl_real_pay = :stl_real_pay");
		sql.append(" WHERE 1=1");
		sql.append(" AND stl_id = :stl_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(temp));
	}

	@Override
	public void temp_updateRemark(T_Money_StreamSettleList temp) {
		StringBuffer sql = new StringBuffer("");
		sql.append("UPDATE t_money_streamsettlelist_temp");
		sql.append(" SET stl_remark = :stl_remark");
		sql.append(" WHERE 1=1");
		sql.append(" AND stl_id = :stl_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(temp));
	}
	
	@Override
	public List<T_Money_StreamSettleList> detail_list(String number,Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT stl_id,stl_number,stl_payable,stl_payabled,stl_discount_money_yet,stl_unpayable,");
		sql.append(" stl_discount_money,stl_real_pay,stl_bill_number,stl_remark,companyid");
		sql.append(" FROM t_money_streamsettlelist t");
		sql.append(" WHERE 1=1");
		sql.append(" AND stl_number = :stl_number");
		sql.append(" AND companyid = :companyid");
		sql.append(" ORDER BY stl_id ASC");
		return namedParameterJdbcTemplate.query(sql.toString(), 
				new MapSqlParameterSource().addValue("stl_number", number).addValue("companyid", companyid), 
				new BeanPropertyRowMapper<>(T_Money_StreamSettleList.class));
	}
	
	@Override
	public void save(T_Money_StreamSettle settle, List<T_Money_StreamSettleList> details) {
		String prefix = CommonUtil.NUMBER_PREFIX_WLJSD + DateUtil.getYearMonthDateYYYYMMDD();
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT CONCAT(:prefix,f_addnumber(MAX(st_number))) AS new_number");
		sql.append(" FROM t_money_streamsettle");
		sql.append(" WHERE 1=1");
		sql.append(" AND INSTR(st_number,:prefix) > 0");
		sql.append(" AND companyid = :companyid");
		String new_number = namedParameterJdbcTemplate.queryForObject(sql.toString(), 
				new MapSqlParameterSource().addValue("prefix", prefix).addValue("companyid", settle.getCompanyid()), String.class);
		settle.setSt_number(new_number);
		sql.setLength(0);
		sql.append("INSERT INTO t_money_streamsettle");
		sql.append(" (st_number,st_date,st_stream_code,st_maker,st_manager,st_ba_code,st_discount_money,st_paid,");
		sql.append(" st_remark,st_ar_state,st_ar_date,st_sysdate,st_us_id,companyid)");
		sql.append(" VALUES");
		sql.append(" (:st_number,:st_date,:st_stream_code,:st_maker,:st_manager,:st_ba_code,:st_discount_money,:st_paid,");
		sql.append(" :st_remark,:st_ar_state,:st_ar_date,:st_sysdate,:st_us_id,:companyid)");
		KeyHolder holder = new GeneratedKeyHolder(); 
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(settle),holder);
		settle.setSt_id(holder.getKey().intValue());
		for(T_Money_StreamSettleList item:details){
			item.setStl_number(settle.getSt_number());
		}
		sql.setLength(0);
		sql.append("INSERT INTO t_money_streamsettlelist");
		sql.append(" (stl_number,stl_payable,stl_payabled,stl_discount_money_yet,stl_unpayable,");
		sql.append(" stl_discount_money,stl_real_pay,stl_bill_number,stl_remark,companyid)");
		sql.append(" VALUES");
		sql.append(" (:stl_number,:stl_payable,:stl_payabled,:stl_discount_money_yet,:stl_unpayable,");
		sql.append(" :stl_discount_money,:stl_real_pay,:stl_bill_number,:stl_remark,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(details.toArray()));
	}
	
	@Override
	public void update(T_Money_StreamSettle settle, List<T_Money_StreamSettleList> details) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" UPDATE t_money_streamsettle");
		sql.append(" SET st_date=:st_date");
		sql.append(" ,st_maker=:st_maker");
		sql.append(" ,st_manager=:st_manager");
		sql.append(" ,st_ba_code=:st_ba_code");
		sql.append(" ,st_discount_money=:st_discount_money");
		sql.append(" ,st_paid=:st_paid");
		sql.append(" ,st_remark=:st_remark");
		sql.append(" ,st_ar_state=:st_ar_state");
		sql.append(" ,st_ar_date=:st_ar_date");
		sql.append(" ,st_us_id=:st_us_id");
		sql.append(" WHERE st_id=:st_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(settle));
		for (T_Money_StreamSettleList item : details) {
			item.setStl_number(settle.getSt_number());
		}
		sql.setLength(0);
		sql.append("INSERT INTO t_money_streamsettlelist");
		sql.append(" (stl_number,stl_payable,stl_payabled,stl_discount_money_yet,stl_unpayable,");
		sql.append(" stl_discount_money,stl_real_pay,stl_bill_number,stl_remark,companyid)");
		sql.append(" VALUES");
		sql.append(" (:stl_number,:stl_payable,:stl_payabled,:stl_discount_money_yet,:stl_unpayable,");
		sql.append(" :stl_discount_money,:stl_real_pay,:stl_bill_number,:stl_remark,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(details.toArray()));
	}
	
	@Override
	public void updateApprove(T_Money_StreamSettle settle) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" UPDATE t_money_streamsettle");
		sql.append(" SET st_ar_state=:st_ar_state");
		sql.append(" ,st_ar_date = :st_ar_date");
		sql.append(" WHERE st_id=:st_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(settle));
	}
	
	@Override
	public List<T_Base_Stream_Bill> listBillBySettle(String number,Integer companyid){
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT seb_id,seb_money,");
		sql.append(" seb_discount_money + stl_discount_money AS seb_discount_money,");
		sql.append(" seb_payabled + stl_real_pay AS seb_payabled,");
		sql.append(" seb_payable - (stl_discount_money+stl_real_pay) AS seb_payable");
		sql.append(" FROM t_money_streamsettlelist t");
		sql.append(" JOIN t_base_stream_bill seb ON stl_bill_number = seb_number AND seb.companyid = t.companyid");
		sql.append(" WHERE 1=1");
		sql.append(" AND stl_number = :stl_number");
		sql.append(" AND t.companyid = :companyid");
		return namedParameterJdbcTemplate.query(sql.toString(), 
				new MapSqlParameterSource().addValue("stl_number", number).addValue("companyid", companyid),
				new BeanPropertyRowMapper<>(T_Base_Stream_Bill.class));
	}
	
	@Override
	public List<T_Base_Stream_Bill> listBillBySettle_Reverse(String number,Integer companyid){
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT seb_id,seb_money,");
		sql.append(" seb_discount_money - stl_discount_money AS seb_discount_money,");
		sql.append(" seb_payabled - stl_real_pay AS seb_payabled,");
		sql.append(" seb_payable + (stl_discount_money+stl_real_pay) AS seb_payable");
		sql.append(" FROM t_money_streamsettlelist t");
		sql.append(" JOIN t_base_stream_bill seb ON stl_bill_number = seb_number AND seb.companyid = t.companyid");
		sql.append(" WHERE 1=1");
		sql.append(" AND stl_number = :stl_number");
		sql.append(" AND t.companyid = :companyid");
		return namedParameterJdbcTemplate.query(sql.toString(), 
				new MapSqlParameterSource().addValue("stl_number", number).addValue("companyid", companyid),
				new BeanPropertyRowMapper<>(T_Base_Stream_Bill.class));
	}

	@Override
	public void updateBillBySettle(List<T_Base_Stream_Bill> bills) {
		StringBuffer sql = new StringBuffer("");
		sql.append("UPDATE t_base_stream_bill");
		sql.append(" SET seb_discount_money = :seb_discount_money");
		sql.append(" ,seb_payabled = :seb_payabled");
		sql.append(" ,seb_payable = :seb_payable");
		sql.append(" ,seb_pay_state = :seb_pay_state");
		sql.append(" WHERE 1=1");
		sql.append(" AND seb_id = :seb_id");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(bills.toArray()));
	}
	
	@Override
	public void del(String st_number, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" DELETE FROM t_money_streamsettle");
		sql.append(" WHERE st_number=:st_number");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("st_number", st_number).addValue("companyid", companyid));
		sql.setLength(0);
		sql.append(" DELETE FROM t_money_streamsettlelist");
		sql.append(" WHERE stl_number=:stl_number");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("stl_number", st_number).addValue("companyid", companyid));
	}
	
	@Override
	public void deleteList(String st_number, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" DELETE FROM t_money_streamsettlelist");
		sql.append(" WHERE stl_number=:stl_number");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("stl_number", st_number).addValue("companyid", companyid));
	}
	
}
