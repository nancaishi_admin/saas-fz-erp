package zy.dao.money.stream;

import java.util.List;
import java.util.Map;

import zy.entity.base.stream.T_Base_Stream_Bill;
import zy.entity.money.stream.T_Money_StreamSettle;
import zy.entity.money.stream.T_Money_StreamSettleList;

public interface StreamSettleDAO {
	Integer count(Map<String,Object> params);
	List<T_Money_StreamSettle> list(Map<String,Object> params);
	T_Money_StreamSettle load(Integer st_id);
	T_Money_StreamSettle load(String number, Integer companyid);
	T_Money_StreamSettle check(String number,Integer companyid);
	T_Money_StreamSettle check_settle(String se_code, Integer companyid);
	List<T_Money_StreamSettleList> load_bill_forsavetemp(String se_code,Integer companyid);
	List<T_Money_StreamSettleList> temp_list(Map<String, Object> params);
	List<T_Money_StreamSettleList> temp_list_forsave(Integer us_id,Integer companyid);
	void temp_save(List<T_Money_StreamSettleList> temps);
	void temp_update(List<T_Money_StreamSettleList> temps);
	void temp_clear(Integer us_id,Integer companyid);
	void temp_updateDiscountMoney(T_Money_StreamSettleList temp);
	void temp_updateRealMoney(T_Money_StreamSettleList temp);
	void temp_updateRemark(T_Money_StreamSettleList temp);
	List<T_Money_StreamSettleList> detail_list(String number,Integer companyid);
	void save(T_Money_StreamSettle settle, List<T_Money_StreamSettleList> details);
	void update(T_Money_StreamSettle settle, List<T_Money_StreamSettleList> details);
	void updateApprove(T_Money_StreamSettle settle);
	List<T_Base_Stream_Bill> listBillBySettle(String number,Integer companyid);
	List<T_Base_Stream_Bill> listBillBySettle_Reverse(String number,Integer companyid);
	void updateBillBySettle(List<T_Base_Stream_Bill> bills);
	void del(String st_number, Integer companyid);
	void deleteList(String st_number, Integer companyid);
}
