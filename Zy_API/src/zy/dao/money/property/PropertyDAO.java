package zy.dao.money.property;

import java.util.List;
import java.util.Map;

import zy.entity.money.property.T_Money_Property;

public interface PropertyDAO {
	List<T_Money_Property> list(Map<String,Object> param);
	Integer queryByName(T_Money_Property property);;
	T_Money_Property queryByID(Integer pp_id);
	void save(T_Money_Property property);
	void update(T_Money_Property property);
	void del(Integer pp_id);
	Integer checkUse_Expense(String pp_code,Integer companyid);
	Integer checkUse_Income(String pp_code,Integer companyid);
}
