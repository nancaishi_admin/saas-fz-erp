package zy.dao.money.access;

import java.util.List;
import java.util.Map;

import zy.dto.money.access.AccessListDto;
import zy.entity.money.access.T_Money_Access;
import zy.entity.money.access.T_Money_AccessList;

public interface AccessDAO {
	Integer count(Map<String,Object> params);
	List<T_Money_Access> list(Map<String,Object> params);
	T_Money_Access load(Integer ac_id);
	T_Money_Access load(String number,Integer companyid);
	T_Money_Access check(String number,Integer companyid);
	List<T_Money_AccessList> temp_list(Map<String, Object> params);
	List<String> temp_check(Integer us_id,Integer companyid);
	List<String> temp_check_sell(Integer ca_id, Integer companyid);
	void temp_save(List<T_Money_AccessList> temps);
	void temp_updateMoney(T_Money_AccessList temp);
	void temp_updateType(T_Money_AccessList temp);
	void temp_updateRemark(T_Money_AccessList temp);
	void temp_del(Integer acl_id);
	void temp_clear(Map<String, Object> params);
	List<T_Money_AccessList> detail_list(Map<String, Object> params);
	List<T_Money_AccessList> detail_list_forsavetemp(String ac_number,Integer companyid);
	void save(T_Money_Access access, List<T_Money_AccessList> details);
	void update(T_Money_Access access, List<T_Money_AccessList> details);
	void updateApprove(T_Money_Access access);
	List<AccessListDto> listBank4Approve(String number,Integer companyid);
	void del(String ac_number, Integer companyid);
	void deleteList(String ac_number, Integer companyid);
}
