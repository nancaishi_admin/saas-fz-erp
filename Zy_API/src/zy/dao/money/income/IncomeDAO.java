package zy.dao.money.income;

import java.util.List;
import java.util.Map;

import zy.dto.money.income.IncomeReportDto;
import zy.entity.money.income.T_Money_Income;
import zy.entity.money.income.T_Money_IncomeList;

public interface IncomeDAO {
	Integer count(Map<String,Object> params);
	List<T_Money_Income> list(Map<String,Object> params);
	T_Money_Income load(Integer ic_id);
	T_Money_Income load(String number,Integer companyid);
	T_Money_Income check(String number,Integer companyid);
	List<T_Money_IncomeList> temp_list(Map<String, Object> params);
	List<String> temp_check(Integer us_id,Integer companyid);
	void temp_save(List<T_Money_IncomeList> temps);
	void temp_updateMoney(T_Money_IncomeList temp);
	void temp_updateRemark(T_Money_IncomeList temp);
	void temp_del(Integer icl_id);
	void temp_clear(Integer us_id,Integer companyid);
	List<T_Money_IncomeList> detail_list(Map<String, Object> params);
	List<T_Money_IncomeList> detail_list_forsavetemp(String ic_number,Integer companyid);
	void save(T_Money_Income income, List<T_Money_IncomeList> details);
	void update(T_Money_Income income, List<T_Money_IncomeList> details);
	void updateApprove(T_Money_Income income);
	void del(String ic_number, Integer companyid);
	void deleteList(String ic_number, Integer companyid);
	List<IncomeReportDto> listReport(Map<String,Object> params);
	List<T_Money_IncomeList> listReportDetail(Map<String, Object> params);
	List<T_Money_Income> monthMoney(Map<String,Object> params);
}
