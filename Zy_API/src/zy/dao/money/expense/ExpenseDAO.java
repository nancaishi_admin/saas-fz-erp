package zy.dao.money.expense;

import java.util.List;
import java.util.Map;

import zy.dto.money.expense.ExpenseReportDepartmentDto;
import zy.dto.money.expense.ExpenseReportDto;
import zy.dto.money.expense.ExpenseReportMonthDto;
import zy.entity.money.expense.T_Money_Expense;
import zy.entity.money.expense.T_Money_ExpenseList;

public interface ExpenseDAO {
	Integer count(Map<String,Object> params);
	List<T_Money_Expense> list(Map<String,Object> params);
	T_Money_Expense load(Integer ep_id);
	T_Money_Expense load(String number,Integer companyid);
	T_Money_Expense check(String number,Integer companyid);
	List<T_Money_Expense> check(List<String> numbers,Integer companyid);
	List<T_Money_ExpenseList> temp_list(Map<String, Object> params);
	List<String> temp_check(Integer us_id,Integer companyid);
	void temp_save(List<T_Money_ExpenseList> temps);
	void temp_updateMoney(T_Money_ExpenseList temp);
	void temp_updateSharemonth(T_Money_ExpenseList temp);
	void temp_updateRemark(T_Money_ExpenseList temp);
	void temp_del(Integer epl_id);
	void temp_clear(Integer us_id,Integer companyid);
	List<T_Money_ExpenseList> detail_list(Map<String, Object> params);
	List<T_Money_ExpenseList> detail_list_forsavetemp(String ep_number,Integer companyid);
	void save(T_Money_Expense expense, List<T_Money_ExpenseList> details);
	void update(T_Money_Expense expense, List<T_Money_ExpenseList> details);
	void updateApprove(T_Money_Expense expense);
	void updateApprove(List<T_Money_Expense> expenses);
	void del(List<String> numbers, Integer companyid);
	void deleteList(String ep_number, Integer companyid);
	List<ExpenseReportDto> listReport(Map<String,Object> params);
	List<T_Money_ExpenseList> listReportDetail(Map<String, Object> params);
	Map<String, ExpenseReportMonthDto> expense_head(Map<String, Object> params);
	Map<String, ExpenseReportMonthDto> expense_shop(Map<String, Object> params);
	List<ExpenseReportDepartmentDto> expense_department(Map<String,Object> params);
	List<T_Money_Expense> monthMoney(Map<String,Object> params);
}
