package zy.dao.money.bank;

import java.util.List;
import java.util.Map;

import zy.entity.money.bank.T_Money_Bank;

public interface BankDAO {
	List<T_Money_Bank> list(Map<String,Object> param);
	T_Money_Bank queryByID(Integer ba_id);
	T_Money_Bank queryByCode(String ba_code,Integer companyid);
	void save(T_Money_Bank bank);
	void update(T_Money_Bank bank);
	void updateBalanceById(T_Money_Bank bank);
	void updateBalanceById(List<T_Money_Bank> banks);
	void del(Integer ba_id);
	
	public List<T_Money_Bank> listShop(Map<String, Object> param);
}
