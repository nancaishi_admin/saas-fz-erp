package zy.dao.set.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.set.SellSetDAO;
import zy.entity.sell.cashier.T_Sell_Cashier_Set;
import zy.entity.sell.set.T_Sell_Print;
import zy.entity.sell.set.T_Sell_PrintData;
import zy.entity.sell.set.T_Sell_PrintField;
import zy.entity.sell.set.T_Sell_PrintSet;
import zy.entity.sell.set.T_Sell_Set;

@Repository
public class SellSetDAOImpl extends BaseDaoImpl implements SellSetDAO{

	@Override
	public List<T_Sell_Set> cashSet(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT st_key,st_value");
		sql.append(" FROM t_sell_set t");
		sql.append(" WHERE 1=1");
		sql.append(" AND st_shop_code = :shop_code");
		sql.append(" AND companyid = :companyid");
		return namedParameterJdbcTemplate.query(sql.toString(), param,
				new BeanPropertyRowMapper<>(T_Sell_Set.class));
	}

	@SuppressWarnings("unchecked")
	@Override
	public void updateCash(Map<String, Object> param) {
		List<T_Sell_Cashier_Set> setList = (List<T_Sell_Cashier_Set>)param.get("setlist"); 
		StringBuffer sql = new StringBuffer("");
		sql.append("UPDATE t_sell_set SET");
		sql.append(" st_value=:st_value");
		sql.append(" WHERE 1=1");
		sql.append(" AND st_key=:st_key");
		sql.append(" AND st_shop_code=:st_shop_code");
		sql.append(" AND companyid=:companyid");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(setList.toArray()));
	}

	@Override
	public Map<String, Object> print(Map<String, Object> param) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT sp_id,sp_code,sp_title,sp_qrcode,sp_remark,sp_print,sp_auto,sp_shop_code,companyid");
		sql.append(" FROM t_sell_print");
		sql.append(" WHERE 1=1");
		sql.append(" AND sp_shop_code=:shop_code");
		sql.append(" AND companyid=:companyid");
		sql.append(" LIMIT 1");
		T_Sell_Print print = null;
		try {
			print = namedParameterJdbcTemplate.queryForObject(sql.toString(), param, 
					new BeanPropertyRowMapper<>(T_Sell_Print.class));
		} catch (Exception e) {
			return resultMap;
		}
		if(null != print){
			sql.setLength(0);
			sql.append("SELECT spf_id,spf_sp_code,spf_code,spf_name,spf_namecustom,spf_colspan,spf_show,spf_line,spf_row,spf_position ");
			sql.append(" FROM t_sell_printfield");
			sql.append(" WHERE spf_sp_code=:sp_code");
//			sql.append(" AND spf_show=1");
			sql.append(" AND companyid=:companyid");
			sql.append(" order by spf_position,spf_line,spf_row");
			List<T_Sell_PrintField> fields = namedParameterJdbcTemplate.query(sql.toString(), 
					new BeanPropertySqlParameterSource(print), 
					new BeanPropertyRowMapper<>(T_Sell_PrintField.class));
			sql.setLength(0);
			sql.append("SELECT spd_id,spd_sp_code,spd_code,spd_name,spd_namecustom,spd_order,spd_show,spd_width,spd_align");
			sql.append(" FROM t_sell_printdata");
			sql.append(" WHERE spd_sp_code=:sp_code");
//			sql.append(" AND spd_show=1");
			sql.append(" AND companyid=:companyid");
			sql.append(" order by spd_order");
			List<T_Sell_PrintData> datas = namedParameterJdbcTemplate.query(sql.toString(), 
					new BeanPropertySqlParameterSource(print),  
					new BeanPropertyRowMapper<>(T_Sell_PrintData.class));
			sql.setLength(0);
			sql.append("SELECT sps_id,sps_sp_code,sps_key,sps_value ");
			sql.append(" FROM t_sell_printset");
			sql.append(" WHERE sps_sp_code=:sp_code");
			sql.append(" AND companyid=:companyid");
			List<T_Sell_PrintSet> sets = namedParameterJdbcTemplate.query(sql.toString(), 
					new BeanPropertySqlParameterSource(print),  
					new BeanPropertyRowMapper<>(T_Sell_PrintSet.class));
			resultMap.put("fields", fields);
			resultMap.put("datas", datas);
			resultMap.put("sets", buildMap(sets));
		}
		resultMap.put("print", print);
		return resultMap;
	}

	@Override
	public Map<String, Object> sysprint(Map<String, Object> param) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT ");
		sql.append(" ptf_id spf_id,ptf_code spf_code,ptf_name spf_name,");
		sql.append(" ptf_name spf_namecustom,ptf_colspan spf_colspan,");
		sql.append(" ptf_show spf_show,ptf_line spf_line,ptf_row spf_row,ptf_position spf_position");

		sql.append(" FROM common_printfield");
		sql.append(" WHERE ptf_pt_id = :pt_id");
		List<T_Sell_PrintField> fields = namedParameterJdbcTemplate.query(sql.toString(), 
				param, 
				new BeanPropertyRowMapper<>(T_Sell_PrintField.class));
		sql.setLength(0);
		sql.append("SELECT ");
		sql.append("ptd_id spd_id,");
		sql.append("ptd_code spd_code,");
		sql.append("ptd_name spd_name,");
		sql.append("ptd_name spd_namecustom,");
		sql.append("ptd_order spd_order,");
		sql.append("ptd_show spd_show,");
		sql.append("ptd_width spd_width,");
		sql.append("ptd_align spd_align");

		sql.append(" FROM common_printdata");
		sql.append(" WHERE ptd_pt_id = :pt_id");
		List<T_Sell_PrintData> datas = namedParameterJdbcTemplate.query(sql.toString(), 
				param,  
				new BeanPropertyRowMapper<>(T_Sell_PrintData.class));
		sql.setLength(0);
		sql.append("SELECT ");
		sql.append("pts_id sps_id,");
		sql.append("pts_code sps_key,");
		sql.append("pts_name sps_value");

		sql.append(" FROM common_printset");
		sql.append(" WHERE pts_pt_id = :pt_id");
		List<T_Sell_PrintSet> sets = namedParameterJdbcTemplate.query(sql.toString(), 
				param,  
				new BeanPropertyRowMapper<>(T_Sell_PrintSet.class));
		resultMap.put("fields", fields);
		resultMap.put("datas", datas);
		resultMap.put("sets", buildMap(sets));
		return resultMap;
	}
	private Map<String,Object> buildMap(List<T_Sell_PrintSet> sets){
		if(null != sets){
			Map<String,Object> map = new HashMap<String, Object>(sets.size());
			for(T_Sell_PrintSet set:sets){
				map.put(set.getSps_key(), set.getSps_value());
			}
			return map;
		}
		return null;
	}

	@Override
	public void savePrint(T_Sell_Print print,
			List<T_Sell_PrintField> printFields,
			List<T_Sell_PrintData> printDatas, List<T_Sell_PrintSet> printSets) {
		StringBuffer sql = new StringBuffer();
		
		sql.append(" select f_three_code(max(sp_code+0)) from t_sell_print ");
		sql.append(" where 1=1");
		sql.append(" and companyid=:companyid");
		String code = namedParameterJdbcTemplate.queryForObject(sql.toString(), new BeanPropertySqlParameterSource(print), String.class);
		print.setSp_code(code);
		sql.setLength(0);
		sql.append("INSERT INTO t_sell_print(sp_code,sp_title,sp_qrcode,sp_print,sp_auto,sp_remark,sp_shop_code,companyid)");
		sql.append(" VALUES(:sp_code,:sp_title,:sp_qrcode,:sp_print,:sp_auto,:sp_remark,:sp_shop_code,:companyid)");
		KeyHolder holder = new GeneratedKeyHolder(); 
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(print),holder);
		print.setSp_id(holder.getKey().intValue());
		for (T_Sell_PrintField field : printFields) {
			field.setSpf_sp_code(print.getSp_code());
		}
		for (T_Sell_PrintData data : printDatas) {
			data.setSpd_sp_code(print.getSp_code());
		}
		for (T_Sell_PrintSet set : printSets) {
			set.setSps_sp_code(print.getSp_code());
		}
		sql.setLength(0);
		sql.append("INSERT INTO t_sell_printfield");
		sql.append(" (spf_sp_code,spf_code,spf_name,spf_namecustom,spf_colspan,spf_show,spf_line,spf_row,spf_position,companyid)");
		sql.append(" VALUES");
		sql.append(" (:spf_sp_code,:spf_code,:spf_name,:spf_namecustom,:spf_colspan,:spf_show,:spf_line,:spf_row,:spf_position,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(printFields.toArray()));
		sql.setLength(0);
		sql.append("INSERT INTO t_sell_printdata");
		sql.append(" (spd_sp_code,spd_code,spd_name,spd_namecustom,spd_order,spd_show,spd_width,spd_align,companyid)");
		sql.append(" VALUES");
		sql.append(" (:spd_sp_code,:spd_code,:spd_name,:spd_namecustom,:spd_order,:spd_show,:spd_width,:spd_align,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(printDatas.toArray()));
		sql.setLength(0);
		sql.append("INSERT INTO t_sell_printset(sps_sp_code,sps_key,sps_value,companyid)");
		sql.append(" VALUES(:sps_sp_code,:sps_key,:sps_value,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(printSets.toArray()));
	}

	@Override
	public void updatePrint(T_Sell_Print print,
			List<T_Sell_PrintField> printFields,
			List<T_Sell_PrintData> printDatas, List<T_Sell_PrintSet> printSets) {
		StringBuffer sql = new StringBuffer();
		sql.append("UPDATE t_sell_print");
		sql.append(" SET sp_title = :sp_title,sp_remark = :sp_remark,");
		sql.append(" sp_qrcode=:sp_qrcode,sp_print=:sp_print,sp_auto=:sp_auto");
		sql.append(" WHERE sp_id = :sp_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(print));
		sql.setLength(0);
		sql.append("UPDATE t_sell_printfield");
		sql.append(" SET spf_namecustom = :spf_namecustom");
		sql.append(" ,spf_show = :spf_show");
		sql.append(" ,spf_colspan = :spf_colspan");
		sql.append(" ,spf_line = :spf_line");
		sql.append(" ,spf_row = :spf_row");
		sql.append(" WHERE spf_id = :spf_id");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(printFields.toArray()));
		sql.setLength(0);
		sql.append("UPDATE t_sell_printdata");
		sql.append(" SET spd_namecustom = :spd_namecustom");
		sql.append(" ,spd_order = :spd_order");
		sql.append(" ,spd_show = :spd_show");
		sql.append(" ,spd_width = :spd_width");
		sql.append(" ,spd_align = :spd_align");
		sql.append(" WHERE spd_id = :spd_id");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(printDatas.toArray()));
		sql.setLength(0);
		sql.append(" UPDATE t_sell_printset");
		sql.append(" SET sps_value=:sps_value");
		sql.append(" WHERE sps_key=:sps_key AND sps_sp_code=:sps_sp_code");
		sql.append(" AND companyid=:companyid");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(printSets.toArray()));
	}
	
	
}
