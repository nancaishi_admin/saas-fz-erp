package zy.dao.sell.shift.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.sell.shift.ShiftDAO;
import zy.entity.sell.shift.T_Sell_Shift;
import zy.util.CommonUtil;

@Repository
public class ShiftDAOImpl extends BaseDaoImpl implements ShiftDAO {

	@Override
	public Integer count(Map<String, Object> params) {
		Object searchContent = params.get("searchContent");
		Object shop_type = params.get(CommonUtil.SHOP_TYPE);
		Object sp_code = params.get("sp_code");//页面传递的店铺编号
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT COUNT(1)");
		sql.append(" FROM t_sell_shift t");
		sql.append(" JOIN t_base_shop sp ON sp_code = st_shop_code AND sp.companyid = t.companyid");
		sql.append(" WHERE 1 = 1");
		if(CommonUtil.ONE.equals(shop_type) || CommonUtil.TWO.equals(shop_type)){//总公司、分公司
			sql.append(" AND sp_upcode = :shop_code AND (sp_shop_type = "+CommonUtil.THREE+" OR sp_shop_type = "+CommonUtil.FIVE+")");
		}else if(CommonUtil.THREE.equals(shop_type) || CommonUtil.FOUR.equals(shop_type) || CommonUtil.FIVE.equals(shop_type)){//自营店、加盟店、合伙店
			sql.append(" AND sp_code = :shop_code");
		}
		if(null != searchContent && !"".equals(searchContent)){
        	sql.append(" AND (INSTR(st_code,:searchContent)>0 OR INSTR(st_name,:searchContent)>0)");
        }
		if(sp_code != null && !"".equals(sp_code)){
			sp_code = sp_code.toString().replace(",", "','");
			sql.append(" AND st_shop_code IN ('"+sp_code+"')");
		}
		sql.append(" AND t.companyid=:companyid");
		return namedParameterJdbcTemplate.queryForObject(sql.toString(), params, Integer.class);
	}

	@Override
	public List<T_Sell_Shift> listShop(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT st_id,st_code,st_name,st_begintime,st_endtime");
		sql.append(" FROM t_sell_shift t");
		sql.append(" WHERE 1 = 1");
		sql.append(" AND st_shop_code=:shop_code");
		sql.append(" AND t.companyid=:companyid");
		return namedParameterJdbcTemplate.query(sql.toString(), param, new BeanPropertyRowMapper<>(T_Sell_Shift.class));
	}

	@Override
	public List<T_Sell_Shift> list(Map<String, Object> params) {
		Object searchContent = params.get("searchContent");
		Object shop_type = params.get(CommonUtil.SHOP_TYPE);
		Object sp_code = params.get("sp_code");//页面传递的店铺编号
		Object sidx = params.get(CommonUtil.SIDX);
		Object sord = params.get(CommonUtil.SORD);
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT st_id,st_code,st_name,st_shop_code,st_begintime,st_endtime,t.companyid,sp_name AS shop_name");
		sql.append(" FROM t_sell_shift t");
		sql.append(" JOIN t_base_shop sp ON sp_code = st_shop_code AND sp.companyid = t.companyid");
		sql.append(" WHERE 1 = 1");
		if(CommonUtil.ONE.equals(shop_type) || CommonUtil.TWO.equals(shop_type)){//总公司、分公司
			sql.append(" AND sp_upcode = :shop_code AND (sp_shop_type = "+CommonUtil.THREE+" OR sp_shop_type = "+CommonUtil.FIVE+")");
		}else if(CommonUtil.THREE.equals(shop_type) || CommonUtil.FOUR.equals(shop_type) || CommonUtil.FIVE.equals(shop_type)){//自营店、加盟店、合伙店
			sql.append(" AND sp_code = :shop_code");
		}
		if(null != searchContent && !"".equals(searchContent)){
        	sql.append(" AND (INSTR(st_code,:searchContent)>0 OR INSTR(st_name,:searchContent)>0)");
        }
		if(sp_code != null && !"".equals(sp_code)){
			sp_code = sp_code.toString().replace(",", "','");
			sql.append(" AND st_shop_code IN ('"+sp_code+"')");
		}
		sql.append(" AND t.companyid=:companyid");
		if(sidx != null && !"".equals(sidx)){
			sql.append(" ORDER BY ").append(sidx).append(" ").append(sord);
		}else {
			sql.append(" ORDER BY st_id DESC");
		}
		sql.append(" LIMIT :start,:end");
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Sell_Shift.class));
	}

	@Override
	public T_Sell_Shift queryByID(Integer st_id) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT st_id,st_code,st_name,st_shop_code,st_begintime,st_endtime,companyid, ");
		sql.append(" (SELECT sp_name FROM t_base_shop sp WHERE sp_code = st_shop_code AND sp.companyid = t.companyid LIMIT 1) AS shop_name");
		sql.append(" FROM t_sell_shift t");
		sql.append(" WHERE st_id=:st_id");
		sql.append(" LIMIT 1");
		try{
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), new MapSqlParameterSource().addValue("st_id", st_id),
					new BeanPropertyRowMapper<>(T_Sell_Shift.class));
		}catch(Exception e){
			return null;
		}
	}

	@Override
	public String queryMaxStcode(Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT f_three_code(max(st_code+0)) from t_sell_shift ");
		sql.append(" WHERE 1=1");
		sql.append(" AND companyid=:companyid");
		String code = namedParameterJdbcTemplate.queryForObject(sql.toString(), new MapSqlParameterSource().addValue("companyid",companyid), String.class);
		return code;
	}

	@Override
	public void save(List<T_Sell_Shift> shifts) {
		StringBuffer sql = new StringBuffer("");
		sql.append("INSERT INTO t_sell_shift");
		sql.append(" (st_code,st_name,st_shop_code,st_begintime,st_endtime,companyid)");
		sql.append(" VALUES(:st_code,:st_name,:st_shop_code,:st_begintime,:st_endtime,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(shifts.toArray()));
	}

	@Override
	public void update(T_Sell_Shift shift) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" UPDATE t_sell_shift");
		sql.append(" SET st_name=:st_name");
		sql.append(" ,st_begintime=:st_begintime");
		sql.append(" ,st_endtime=:st_endtime");
		sql.append(" WHERE st_id=:st_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(shift));
	}

	@Override
	public void del(Integer st_id) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" DELETE FROM t_sell_shift");
		sql.append(" WHERE st_id=:st_id");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("st_id", st_id));
	}

}
