package zy.dao.sell.displayarea;

import java.util.List;
import java.util.Map;

import zy.entity.sell.displayarea.T_Sell_DisplayArea;
import zy.form.StringForm;

public interface DisplayAreaDAO {
	Integer count(Map<String,Object> params);
	List<T_Sell_DisplayArea> list(Map<String,Object> params);
	T_Sell_DisplayArea queryByID(Integer da_id);
	void save(T_Sell_DisplayArea displayArea);
	void update(T_Sell_DisplayArea displayArea);
	void del(Integer da_id);
	
	List<StringForm> listShop(Map<String,Object> param);
}
