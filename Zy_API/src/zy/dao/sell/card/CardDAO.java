package zy.dao.sell.card;

import java.util.List;
import java.util.Map;

import zy.entity.sell.card.T_Sell_Card;
import zy.entity.sell.card.T_Sell_CardList;

public interface CardDAO {
	Integer count(Map<String,Object> params);
	List<T_Sell_Card> list(Map<String,Object> params);
	Integer countDetail(Map<String,Object> params);
	List<T_Sell_CardList> listDetail(Map<String,Object> params);
	List<T_Sell_CardList> listDetail(String cd_code,Integer companyid);
	Integer countByCardCode(Map<String, Object> params);
	T_Sell_Card queryByID(Integer cd_id);
	Integer checkExistOperation(String cd_code, Integer companyid);
	T_Sell_CardList queryInitBank(String cd_code, Integer companyid);
	void save(List<T_Sell_Card> cards,List<T_Sell_CardList> cardDetails);
	void saveDetail(T_Sell_CardList cardList);
	void update(T_Sell_Card card);
	void updateMoney(T_Sell_Card card);
	void updateState(T_Sell_Card card);
	void del(Integer cd_id);
	void delList(String cd_code,Integer companyid);
	
	//------------------------前台------------------------
	List<T_Sell_Card> listShop(Map<String,Object> param);
	void saveShop(Map<String,Object> param);
	Integer idByCode(Map<String,Object> param);
	void editPass(Map<String,Object> paramMap);
	T_Sell_Card queryByCode(Map<String,Object> param);
	T_Sell_Card cardByMobile(Map<String,Object> param);
	void updateCard(Map<String,Object> param);
}
