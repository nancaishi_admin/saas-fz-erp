package zy.dao.sell.day;

import java.util.List;
import java.util.Map;

import zy.entity.sell.day.T_Sell_Day;
import zy.entity.sell.day.T_Sell_Weather;

public interface DayDAO {
	String cityByCode(Map<String, Object> param);
	T_Sell_Weather weather(Map<String, Object> param);
	T_Sell_Weather sysWeather(Map<String, Object> param);
	T_Sell_Weather tomorrow(Map<String, Object> param);
	void saveSysWeather(T_Sell_Weather weather);
	void saveWeather(T_Sell_Weather weather);
	
	T_Sell_Day queryDay(Map<String, Object> paramMap);
	void saveDay(T_Sell_Day day);
	void come(Map<String,Object> paramMap);
	void receive(Map<String,Object> paramMap);
	void comereceive(Map<String, Object> paramMap);
	void doTry(Map<String,Object> paramMap);
	Integer countreceive(Map<String, Object> params);
	List<T_Sell_Day> listByDate(Map<String,Object> paramMap);
	List<T_Sell_Day> listByShop(Map<String,Object> paramMap);
}
