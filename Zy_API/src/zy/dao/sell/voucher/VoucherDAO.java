package zy.dao.sell.voucher;

import java.util.List;
import java.util.Map;

import zy.entity.sell.voucher.T_Sell_Voucher;
import zy.entity.sell.voucher.T_Sell_VoucherList;

public interface VoucherDAO {
	Integer count(Map<String,Object> params);
	List<T_Sell_Voucher> list(Map<String,Object> params);
	Integer countDetail(Map<String,Object> params);
	List<T_Sell_VoucherList> listDetail(Map<String,Object> params);
	List<T_Sell_VoucherList> listDetail(String vc_code,Integer companyid);
	Integer countByCardCode(Map<String, Object> params);
	T_Sell_Voucher queryByID(Integer vc_id);
	String queryInitBank(String vc_code, Integer companyid);
	void save(List<T_Sell_Voucher> vouchers,List<T_Sell_VoucherList> voucherDetails);
	void update(T_Sell_Voucher voucher);
	void updateState(T_Sell_Voucher voucher);
	void del(Integer vc_id);
	void delList(String vc_code,Integer companyid);
	
	//----前台------------
	List<T_Sell_Voucher> listShop(Map<String,Object> params);
	void saveShop(Map<String, Object> params);
	Integer idByCardCode(Map<String, Object> param);
	T_Sell_Voucher queryByCode(Map<String,Object> param);
	void updateVoucher(Map<String,Object> param);
}
