package zy.dao.sell.voucher.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.sell.voucher.VoucherDAO;
import zy.entity.sell.voucher.T_Sell_Voucher;
import zy.entity.sell.voucher.T_Sell_VoucherList;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;

@Repository
public class VoucherDAOImpl extends BaseDaoImpl implements VoucherDAO{

	@Override
	public Integer count(Map<String, Object> params) {
		Object shop_type = params.get(CommonUtil.SHOP_TYPE);
		Object begindate = params.get("begindate");
		Object enddate = params.get("enddate");
		Object minmoney = params.get("minmoney");
		Object maxmoney = params.get("maxmoney");
		Object vc_shop_code = params.get("vc_shop_code");
		Object vc_cardcode = params.get("vc_cardcode");
		Object vc_name = params.get("vc_name");
		Object vc_mobile = params.get("vc_mobile");
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT COUNT(1)");
		sql.append(" FROM t_sell_voucher t");
		sql.append(" JOIN t_base_shop sp ON sp_code = vc_shop_code AND sp.companyid = t.companyid");
		if("1".equals(shop_type) || "2".equals(shop_type)){//总公司、分公司
			sql.append(getShopSQL(shop_type, 1));
		}else{//自营、加盟、合伙
			sql.append(" AND sp_code=:shop_code");
		}
		sql.append(" WHERE 1 = 1");
		if (StringUtil.isNotEmpty(begindate)) {
			sql.append(" AND vc_grantdate >= :begindate ");
		}
		if (StringUtil.isNotEmpty(enddate)) {
			sql.append(" AND vc_grantdate <= :enddate ");
		}
		if (StringUtil.isNotEmpty(vc_shop_code)) {
			params.put("shop_codes", StringUtil.parseList(StringUtil.trimString(vc_shop_code)));
			sql.append(" AND vc_shop_code IN (:shop_codes)");
		}
		if (StringUtil.isNotEmpty(vc_cardcode)) {
			sql.append(" AND INSTR(vc_cardcode, :vc_cardcode) > 0 ");
		}
		if (StringUtil.isNotEmpty(vc_name)) {
			sql.append(" AND INSTR(vc_name, :vc_name) > 0 ");
		}
		if (StringUtil.isNotEmpty(vc_mobile)) {
			sql.append(" AND INSTR(vc_mobile, :vc_mobile) > 0 ");
		}
		if (StringUtil.isNotEmpty(minmoney) && StringUtil.isNotEmpty(maxmoney)) {
			sql.append(" AND (vc_money-vc_used_money) BETWEEN :minmoney AND :maxmoney ");
		}
		/*if(CommonUtil.ONE.equals(shop_type) || CommonUtil.TWO.equals(shop_type)){//总公司、分公司
			sql.append(" AND sp_upcode = :shop_code AND (sp_shop_type = "+CommonUtil.THREE+" OR sp_shop_type = "+CommonUtil.FIVE+")");
		}else if(CommonUtil.THREE.equals(shop_type) || CommonUtil.FOUR.equals(shop_type) || CommonUtil.FIVE.equals(shop_type)){//自营店、加盟店、合伙店
			sql.append(" AND sp_code = :shop_code");
		}*/
		sql.append(" AND t.companyid=:companyid");
		Integer count = namedParameterJdbcTemplate.queryForObject(sql.toString(), params, Integer.class);
		return count;
	}

	@Override
	public List<T_Sell_Voucher> list(Map<String, Object> params) {
		Object shop_type = params.get(CommonUtil.SHOP_TYPE);
		Object sidx = params.get(CommonUtil.SIDX);
		Object sord = params.get(CommonUtil.SORD);
		Object begindate = params.get("begindate");
		Object enddate = params.get("enddate");
		Object minmoney = params.get("minmoney");
		Object maxmoney = params.get("maxmoney");
		Object vc_shop_code = params.get("vc_shop_code");
		Object vc_cardcode = params.get("vc_cardcode");
		Object vc_name = params.get("vc_name");
		Object vc_mobile = params.get("vc_mobile");
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT vc_id,vc_code,vc_cardcode,vc_shop_code,vc_grantdate,vc_enddate,vc_money,vc_used_money,");
		sql.append(" vc_realcash,vc_cashrate,vc_state,vc_remark,vc_name,vc_mobile,vc_manager,t.companyid,");
		sql.append(" sp_name AS shop_name");
		sql.append(" FROM t_sell_voucher t");
		sql.append(" JOIN t_base_shop sp ON sp_code = vc_shop_code AND sp.companyid = t.companyid");
		if("1".equals(shop_type) || "2".equals(shop_type)){//总公司、分公司
			sql.append(getShopSQL(shop_type, 1));
		}else{//自营、加盟、合伙
			sql.append(" AND sp_code=:shop_code");
		}
		sql.append(" WHERE 1=1");
		if (StringUtil.isNotEmpty(begindate)) {
			sql.append(" AND vc_grantdate >= :begindate ");
		}
		if (StringUtil.isNotEmpty(enddate)) {
			sql.append(" AND vc_grantdate <= :enddate ");
		}
		if (StringUtil.isNotEmpty(vc_shop_code)) {
			params.put("shop_codes", StringUtil.parseList(StringUtil.trimString(vc_shop_code)));
			sql.append(" AND vc_shop_code IN (:shop_codes)");
		}
		if (StringUtil.isNotEmpty(vc_cardcode)) {
			sql.append(" AND INSTR(vc_cardcode, :vc_cardcode) > 0 ");
		}
		if (StringUtil.isNotEmpty(vc_name)) {
			sql.append(" AND INSTR(vc_name, :vc_name) > 0 ");
		}
		if (StringUtil.isNotEmpty(vc_mobile)) {
			sql.append(" AND INSTR(vc_mobile, :vc_mobile) > 0 ");
		}
		if (StringUtil.isNotEmpty(minmoney) && StringUtil.isNotEmpty(maxmoney)) {
			sql.append(" AND (vc_money-vc_used_money) BETWEEN :minmoney AND :maxmoney ");
		}
		/*if(CommonUtil.ONE.equals(shop_type) || CommonUtil.TWO.equals(shop_type)){//总公司、分公司
			sql.append(" AND sp_upcode = :shop_code AND (sp_shop_type = "+CommonUtil.THREE+" OR sp_shop_type = "+CommonUtil.FIVE+")");
		}else if(CommonUtil.THREE.equals(shop_type) || CommonUtil.FOUR.equals(shop_type) || CommonUtil.FIVE.equals(shop_type)){//自营店、加盟店、合伙店
			sql.append(" AND sp_code = :shop_code");
		}*/
		sql.append(" AND t.companyid=:companyid");
		if(sidx != null && !"".equals(sidx)){
			sql.append(" ORDER BY ").append(sidx).append(" ").append(sord);
		}else {
			sql.append(" ORDER BY vc_id DESC");
		}
		sql.append(" LIMIT :start,:end");
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Sell_Voucher.class));
	}

	@Override
	public Integer countDetail(Map<String, Object> params) {
		Object begindate = params.get("begindate");
		Object enddate = params.get("enddate");
		Object vcl_shop_code = params.get("vcl_shop_code");
		Object vcl_cardcode = params.get("vcl_cardcode");
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT COUNT(1)");
		sql.append(" FROM t_sell_voucherlist t");
		sql.append(" WHERE 1 = 1");
		if (StringUtil.isNotEmpty(begindate)) {
			sql.append(" AND vcl_date >= :begindate ");
		}
		if (StringUtil.isNotEmpty(enddate)) {
			sql.append(" AND vcl_date <= :enddate ");
		}
		if (StringUtil.isNotEmpty(vcl_shop_code)) {
			sql.append(" AND vcl_shop_code = :vcl_shop_code ");
		}
		if (StringUtil.isNotEmpty(vcl_cardcode)) {
			sql.append(" AND INSTR(vcl_cardcode, :vcl_cardcode) > 0 ");
		}
		sql.append(" AND t.companyid=:companyid");
		return namedParameterJdbcTemplate.queryForObject(sql.toString(), params, Integer.class);
	}

	@Override
	public List<T_Sell_VoucherList> listDetail(Map<String, Object> params) {
		Object sidx = params.get(CommonUtil.SIDX);
		Object sord = params.get(CommonUtil.SORD);
		Object begindate = params.get("begindate");
		Object enddate = params.get("enddate");
		Object vcl_shop_code = params.get("vcl_shop_code");
		Object vcl_cardcode = params.get("vcl_cardcode");
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT vcl_id,vcl_vc_code,vcl_cardcode,vcl_money,vcl_cash,vcl_date,vcl_type,vcl_shop_code,vcl_ba_code,vcl_number,vcl_manager,companyid,");
		sql.append(" (SELECT sp_name FROM t_base_shop sp WHERE sp_code = vcl_shop_code AND sp.companyid = t.companyid LIMIT 1) AS shop_name,");
		sql.append(" (SELECT ba_name FROM t_money_bank ba WHERE ba_code = vcl_ba_code AND ba.companyid = t.companyid LIMIT 1) AS bank_name");
		sql.append(" FROM t_sell_voucherlist t");
		sql.append(" WHERE 1=1");
		if (StringUtil.isNotEmpty(begindate)) {
			sql.append(" AND vcl_date >= :begindate ");
		}
		if (StringUtil.isNotEmpty(enddate)) {
			sql.append(" AND vcl_date <= :enddate ");
		}
		if (StringUtil.isNotEmpty(vcl_shop_code)) {
			sql.append(" AND vcl_shop_code = :vcl_shop_code ");
		}
		if (StringUtil.isNotEmpty(vcl_cardcode)) {
			sql.append(" AND INSTR(vcl_cardcode, :vcl_cardcode) > 0 ");
		}
		sql.append(" AND companyid = :companyid");
		if(sidx != null && !"".equals(sidx)){
			sql.append(" ORDER BY ").append(sidx).append(" ").append(sord);
		}else {
			sql.append(" ORDER BY vcl_id DESC");
		}
		sql.append(" LIMIT :start,:end");
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Sell_VoucherList.class));
	}

	@Override
	public List<T_Sell_VoucherList> listDetail(String vc_code, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT vcl_id,vcl_vc_code,vcl_cardcode,vcl_money,vcl_cash,vcl_date,vcl_type,vcl_shop_code,vcl_ba_code,vcl_number,vcl_manager,companyid,");
		sql.append(" (SELECT sp_name FROM t_base_shop sp WHERE sp_code = vcl_shop_code AND sp.companyid = t.companyid LIMIT 1) AS shop_name,");
		sql.append(" (SELECT ba_name FROM t_money_bank ba WHERE ba_code = vcl_ba_code AND ba.companyid = t.companyid LIMIT 1) AS bank_name");
		sql.append(" FROM t_sell_voucherlist t");
		sql.append(" WHERE 1=1");
		sql.append(" AND vcl_vc_code = :vcl_vc_code");
		sql.append(" AND companyid = :companyid");
		return namedParameterJdbcTemplate.query(sql.toString(), new MapSqlParameterSource().addValue("vcl_vc_code", vc_code).addValue("companyid", companyid),
				new BeanPropertyRowMapper<>(T_Sell_VoucherList.class));
	}
	
	@Override
	public Integer countByCardCode(Map<String, Object> params) {
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT COUNT(1)");
		sql.append(" FROM t_sell_voucher");
		sql.append(" WHERE 1=1");
		sql.append(" AND vc_cardcode IN(:vc_cardcode)");
		sql.append(" AND companyid = :companyid");
		return namedParameterJdbcTemplate.queryForObject(sql.toString(), params, Integer.class);
	}
	
	@Override
	public T_Sell_Voucher queryByID(Integer vc_id) {
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT vc_id,vc_code,vc_cardcode,vc_shop_code,vc_grantdate,vc_enddate,vc_money,vc_used_money,");
		sql.append(" vc_realcash,vc_cashrate,vc_state,vc_remark,vc_name,vc_mobile,vc_manager,companyid,");
		sql.append(" (SELECT sp_name FROM t_base_shop sp WHERE sp_code = vc_shop_code AND sp.companyid = t.companyid LIMIT 1) AS shop_name");
		sql.append(" FROM t_sell_voucher t");
		sql.append(" WHERE vc_id=:vc_id");
		sql.append(" LIMIT 1");
		try{
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), new MapSqlParameterSource().addValue("vc_id", vc_id),
					new BeanPropertyRowMapper<>(T_Sell_Voucher.class));
		}catch(Exception e){
			return null;
		}
	}
	
	@Override
	public String queryInitBank(String vc_code, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT vcl_ba_code");
		sql.append(" FROM t_sell_voucherlist t");
		sql.append(" WHERE 1=1");
		sql.append(" AND vcl_type = 0");
		sql.append(" AND vcl_vc_code = :vcl_vc_code");
		sql.append(" AND companyid = :companyid");
		sql.append(" LIMIT 1");
		try{
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), new MapSqlParameterSource().addValue("vcl_vc_code", vc_code).addValue("companyid", companyid),
					String.class);
		}catch(Exception e){
			return null;
		}
	}
	
	@Override
	public void save(List<T_Sell_Voucher> vouchers, List<T_Sell_VoucherList> voucherDetails) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT f_three_code(max(vc_code+0)) FROM t_sell_voucher ");
		sql.append(" WHERE 1=1");
		sql.append(" AND companyid=:companyid");
		String code = namedParameterJdbcTemplate.queryForObject(sql.toString(), new BeanPropertySqlParameterSource(vouchers.get(0)), String.class);
		int beginCode = Integer.parseInt(code);
		for (int i = 0; i < vouchers.size(); i++) {
			vouchers.get(i).setVc_code(StringUtil.trimString(beginCode+i));
			voucherDetails.get(i).setVcl_vc_code(StringUtil.trimString(beginCode+i));
		}
		sql.setLength(0);
		sql.append(" INSERT INTO t_sell_voucher");
		sql.append(" (vc_code,vc_cardcode,vc_shop_code,vc_grantdate,vc_enddate,vc_money,vc_used_money,vc_realcash,vc_cashrate,");
		sql.append(" vc_state,vc_remark,vc_name,vc_mobile,vc_manager,companyid)");
		sql.append(" VALUES(:vc_code,:vc_cardcode,:vc_shop_code,:vc_grantdate,:vc_enddate,:vc_money,:vc_used_money,:vc_realcash,");
		sql.append(" :vc_cashrate,:vc_state,:vc_remark,:vc_name,:vc_mobile,:vc_manager,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(vouchers.toArray()));
		sql.setLength(0);
		sql.append(" INSERT INTO t_sell_voucherlist");
		sql.append(" (vcl_vc_code,vcl_cardcode,vcl_money,vcl_cash,vcl_date,vcl_type,vcl_shop_code,vcl_ba_code,vcl_number,vcl_manager,companyid)");
		sql.append(" VALUES(:vcl_vc_code,:vcl_cardcode,:vcl_money,:vcl_cash,:vcl_date,:vcl_type,:vcl_shop_code,:vcl_ba_code,:vcl_number,:vcl_manager,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(voucherDetails.toArray()));
	}
	
	@Override
	public void update(T_Sell_Voucher voucher) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" UPDATE t_sell_voucher");
		sql.append(" SET vc_name=:vc_name");
		sql.append(" ,vc_enddate=:vc_enddate");
		sql.append(" ,vc_mobile=:vc_mobile");
		sql.append(" WHERE vc_id=:vc_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(voucher));
	}

	@Override
	public void updateState(T_Sell_Voucher voucher) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" UPDATE t_sell_voucher");
		sql.append(" SET vc_state = :vc_state");
		sql.append(" WHERE vc_id=:vc_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(voucher));
	}

	@Override
	public void del(Integer vc_id) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" DELETE FROM t_sell_voucher");
		sql.append(" WHERE vc_id=:vc_id");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("vc_id", vc_id));
	}

	@Override
	public void delList(String vc_code, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" DELETE FROM t_sell_voucherlist");
		sql.append(" WHERE vcl_vc_code=:vc_code AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("vc_code", vc_code).addValue("companyid", companyid));
	}
	//--------------------------------------前台功能-------------------------------------
	/**
	 * 前台只能根据券号查询
	 * */
	@Override
	public List<T_Sell_Voucher> listShop(Map<String, Object> params) {
		Object shop_type = params.get(CommonUtil.SHOP_TYPE);
		Object shop_uptype = params.get(CommonUtil.SHOP_UPTYPE);
		Object vc_cardcode = params.get("vc_cardcode");
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT vc_id,vc_code,vc_cardcode,vc_grantdate,vc_enddate,vc_money,vc_used_money,");
		sql.append(" vc_realcash,vc_state,vc_name,vc_mobile,");
		sql.append(" sp_name AS shop_name");
		sql.append(" FROM t_sell_voucher t");
		sql.append(" JOIN t_base_shop sp ON sp_code = vc_shop_code AND sp.companyid = t.companyid");
		if(!CommonUtil.FOUR.equals(shop_type)){ // 如果是自营店，则下上级店铺相同所有会员
			sql.append(getSellShopSQL(shop_uptype, 0));
		}else{
			sql.append(" AND sp_code=:shop_code");
		} 
		sql.append(" WHERE 1 = 1");
		if (StringUtil.isNotEmpty(vc_cardcode)) {
			sql.append(" AND vc_cardcode=:vc_cardcode");
		}
		sql.append(" AND t.companyid=:companyid");
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Sell_Voucher.class));
	}

	@Override
	public Integer idByCardCode(Map<String, Object> param) {
		Object shop_type = param.get(CommonUtil.SHOP_TYPE);
		Object shop_uptype = param.get(CommonUtil.SHOP_UPTYPE);
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT vc_id");
		sql.append(" FROM t_sell_voucher t");
		sql.append(" JOIN t_base_shop s");
		sql.append(" ON s.sp_code=t.vc_shop_code");
		sql.append(" AND s.companyid=t.companyid");
		if(!CommonUtil.FOUR.equals(shop_type)){ // 如果登录的角色范围是自营店，则查询自营店下上级店铺相同所有自营店会员数据
			sql.append(getSellShopSQL(shop_uptype, 0));
		}else{
			sql.append(" AND sp_code=:shop_code");
		} 
		sql.append(" WHERE 1=1");
		sql.append(" AND vc_cardcode=:vc_cardcode");
		sql.append(" AND t.companyid=:companyid");
		try {
			Integer id = namedParameterJdbcTemplate.queryForObject(sql.toString(), param, Integer.class);
			return id;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void saveShop(Map<String, Object> params) {
		T_Sell_Voucher voucher = (T_Sell_Voucher)params.get("voucher");
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT f_autoincre_code(max(vc_code+0)) FROM t_sell_voucher ");
		sql.append(" WHERE 1=1");
		sql.append(" AND companyid=:companyid");
		String code = namedParameterJdbcTemplate.queryForObject(sql.toString(), new BeanPropertySqlParameterSource(voucher), String.class);
		voucher.setVc_code(code);
		sql.setLength(0);
		sql.append(" INSERT INTO t_sell_voucher");
		sql.append(" (vc_code,vc_cardcode,vc_shop_code,vc_grantdate,vc_enddate,vc_money,vc_used_money,vc_realcash,vc_cashrate,");
		sql.append(" vc_state,vc_remark,vc_name,vc_mobile,vc_manager,companyid)");
		sql.append(" VALUES(:vc_code,:vc_cardcode,:vc_shop_code,:vc_grantdate,:vc_enddate,:vc_money,:vc_used_money,:vc_realcash,");
		sql.append(" :vc_cashrate,:vc_state,:vc_remark,:vc_name,:vc_mobile,:vc_manager,:companyid)");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(voucher));
		sql.setLength(0);
		sql.append(" INSERT INTO t_sell_voucherlist");
		sql.append(" (vcl_vc_code,vcl_cardcode,vcl_money,vcl_cash,vcl_date,vcl_type,vcl_shop_code,vcl_ba_code,vcl_manager,companyid)");
		sql.append(" VALUES(:vc_code,:vc_cardcode,:vc_money,:vc_realcash,sysdate(),0,:vc_shop_code,:vcl_ba_code,:vc_manager,:companyid)");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(voucher));
	}

	@Override
	public T_Sell_Voucher queryByCode(Map<String, Object> param) {
		Object shop_type = param.get(CommonUtil.SHOP_TYPE);
		Object shop_uptype = param.get(CommonUtil.SHOP_UPTYPE);
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT vc_id,vc_code,vc_cardcode,vc_money-vc_used_money vc_money,vc_realcash,vc_cashrate");
		sql.append(" FROM t_sell_voucher t");
		sql.append(" JOIN t_base_shop sp ON sp_code = vc_shop_code AND sp.companyid = t.companyid");
		if(!"4".equals(shop_type)){ // 如果是自营店，则下上级店铺相同所有会员
			sql.append(getSellShopSQL(shop_uptype, 0));
		}else{
			sql.append(" AND sp_code=:shop_code");
		} 
		sql.append(" WHERE 1 = 1");
		sql.append(" AND vc_enddate >= sysdate()");
		sql.append(" AND vc_state=0");
		sql.append(" AND vc_cardcode=:vc_cardcode");
		sql.append(" AND t.companyid=:companyid");
		sql.append(" LIMIT 1");
		try {
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), param, 
					new BeanPropertyRowMapper<>(T_Sell_Voucher.class));
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void updateVoucher(Map<String, Object> param) {
		String[] vc_ids = (String[])param.get("vc_ids");
		String[] vc_money = (String[])param.get("vc_money");
		String number = (String)param.get("number");
		String emp_name = (String)param.get(CommonUtil.EMP_NAME);
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT vc_id,vc_code,vc_cardcode,vc_money,vc_used_money,vc_realcash,vc_cashrate");
		sql.append(",vc_shop_code,companyid");
		sql.append(" FROM t_sell_voucher t");
		sql.append(" WHERE 1 = 1");
		sql.append(" AND vc_id IN (:_vc_ids)");
		param.put("_vc_ids", Arrays.asList(vc_ids));
		List<T_Sell_Voucher> list = namedParameterJdbcTemplate.query(sql.toString(), param, 
				new BeanPropertyRowMapper<>(T_Sell_Voucher.class));
		if(null != list && list.size() > 0){
			List<T_Sell_VoucherList> vList = new ArrayList<T_Sell_VoucherList>();
			for(T_Sell_Voucher voucher:list){
				for(int i =0;i<vc_ids.length;i++){
					String id = vc_ids[i];
					Double money = Double.parseDouble(StringUtil.trimString(vc_money[i]));
					if((voucher.getVc_id()+"").equals(id)){
						voucher.setVc_used_money(voucher.getVc_used_money()+money);
						voucher.setVc_state(1);
						T_Sell_VoucherList vl = new T_Sell_VoucherList();
						vl.setCompanyid(voucher.getCompanyid());
						vl.setVcl_shop_code(voucher.getVc_shop_code());
						vl.setVcl_cardcode(voucher.getVc_cardcode());
						vl.setVcl_vc_code(voucher.getVc_code());
						vl.setVcl_date(DateUtil.getYearMonthDate());
						vl.setVcl_number(number);
						vl.setVcl_manager(emp_name);
						vl.setVcl_cash(StringUtil.double2Two(money*voucher.getVc_cashrate()));
						vl.setVcl_type(1);
						vl.setVcl_money(-Math.abs(money));
					}
				}
			}
			sql.setLength(0);
			sql.append(" UPDATE t_sell_voucher SET");
			sql.append(" vc_used_money=:vc_used_money");
			sql.append(" ,vc_state=:vc_state");
			sql.append(" WHERE 1=1 ");
			sql.append(" AND vc_id=:vc_id");
			namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(list.toArray()));
			sql.setLength(0);
			sql.append(" INSERT INTO t_sell_voucherlist(");
			sql.append(" vcl_vc_code,vcl_cardcode,vcl_money,vcl_cash,");
			sql.append(" vcl_date,vcl_type,vcl_shop_code,vcl_number,");
			sql.append(" vcl_number,vcl_manager,companyid");
			sql.append(" )VALUES(");
			sql.append(" :vcl_vc_code,:vcl_cardcode,:vcl_money,:vcl_cash,");
			sql.append(" :vcl_date,:vcl_type,:vcl_shop_code,:vcl_number,");
			sql.append(" :vcl_manager,:companyid");
			sql.append(" )");
			namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(vList.toArray()));
		}
	}
	
	
}
