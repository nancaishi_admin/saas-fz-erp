package zy.dao.sell.report;

import java.util.List;
import java.util.Map;

import zy.dto.sell.allocate.SellAllocateReportDto;
import zy.dto.sell.report.SellStockByShopDepotDto;
import zy.dto.sell.report.SellStockBySizeDto;
import zy.dto.vip.member.ConsumeSnapshotDto;
import zy.entity.base.size.T_Base_Size;
import zy.entity.sell.cash.T_Sell_Shop;
import zy.entity.sell.cash.T_Sell_ShopList;

public interface SellReportDAO {
	Map<String,Object> countShop(Map<String,Object> params);
	List<T_Sell_Shop> listShop(Map<String,Object> params);
	List<T_Sell_Shop> listShopByVip(Map<String, Object> params);
	List<T_Sell_ShopList> listShopList(String number,Integer companyid);
	ConsumeSnapshotDto loadConsumeSnapshot(String vm_code,Integer companyid);
	Map<String,Object> countRetailReport(Map<String,Object> params);
	List<T_Sell_ShopList> retailReportList(Map<String,Object> params);
	
	Map<String,Object> noRate_count(Map<String,Object> paramMap);
	List<T_Sell_ShopList> noRate_list(Map<String,Object> paramMap);
	List<T_Sell_ShopList> rank_list(Map<String,Object> paramMap);
	List<Map<String,Object>> rank_stock(Map<String,Object> paramMap);
	List<T_Sell_ShopList> empRank(Map<String,Object> paramMap);
	List<T_Sell_ShopList> buyRank(Map<String,Object> paramMap);
	Map<String,Object> noRank_count(Map<String,Object> paramMap);
	List<T_Sell_ShopList> noRank_list(Map<String,Object> paramMap);
	Map<String,Object> listRank_count(Map<String,Object> paramMap);
	List<T_Sell_ShopList> listRank_list(Map<String,Object> paramMap);
	Map<String,Object> styleRank_count(Map<String,Object> paramMap);
	List<T_Sell_ShopList> styleRank_list(Map<String,Object> paramMap);
	List<T_Sell_Shop> shopRank(Map<String,Object> paramMap);
	List<T_Sell_Shop> month(Map<String,Object> paramMap);
	List<T_Sell_Shop> year(Map<String,Object> paramMap);
	List<T_Sell_ShopList> shopSell(Map<String,Object> paramMap);
	Integer sell_allocate_count(Map<String,Object> params);
	List<SellAllocateReportDto> sell_allocate(Map<String,Object> params);
	Map<String, Object> sell_allocate_sum(Map<String, Object> params);
	String getSupply(Map<String, Object> params);
	List<SellStockByShopDepotDto> query_stock_detail(Map<String, Object> params);
	List<T_Base_Size> loadProductSize(String pd_code,Integer companyid);
	List<SellStockBySizeDto> query_stock_detail_size(Map<String, Object> params);
}
