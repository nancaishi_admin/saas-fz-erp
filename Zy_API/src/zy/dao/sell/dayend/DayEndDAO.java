package zy.dao.sell.dayend;

import java.util.List;
import java.util.Map;

import zy.entity.sell.dayend.T_Sell_DayEnd;

public interface DayEndDAO {
	public void save(Map<String, Object> param);
	public T_Sell_DayEnd query(Map<String,Object> param);
	void update(T_Sell_DayEnd dayend);
	Integer count(Map<String,Object> paramMap);
	List<T_Sell_DayEnd> list(Map<String,Object> paramMap);
	List<T_Sell_DayEnd> listShop(Map<String,Object> paramMap);
	public T_Sell_DayEnd endByID(Integer de_id);
}
