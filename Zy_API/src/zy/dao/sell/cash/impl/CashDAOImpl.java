package zy.dao.sell.cash.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.sell.cash.CashDAO;
import zy.entity.base.size.T_Base_Size;
import zy.entity.buy.order.T_Buy_Product;
import zy.entity.sell.cash.T_Sell_Pay_Temp;
import zy.entity.sell.cash.T_Sell_Shop;
import zy.entity.sell.cash.T_Sell_ShopList;
import zy.entity.sell.cash.T_Sell_Shop_Temp;
import zy.entity.shop.sale.T_Shop_Sale_Present;
import zy.entity.stock.data.T_Stock_Data;
import zy.entity.vip.member.T_Vip_Member;
import zy.form.NumberForm;
import zy.form.StringForm;
import zy.form.shop.SaleForm;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;
import zy.vo.sell.SaleVO;
@Repository
public class CashDAOImpl extends BaseDaoImpl implements CashDAO{
	@Override
	public List<StringForm> putUpList(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT sht_state as code,sht_putup_no as name");
		sql.append(" FROM t_sell_shop_temp");
		sql.append(" WHERE sht_isputup=:putup");
		sql.append(" AND sht_em_code=:emp_code");
		sql.append(" AND sht_shop_code=:shop_code");
		sql.append(" AND companyid=:companyid");
		sql.append(" GROUP BY sht_putup_no");
		try{
			return namedParameterJdbcTemplate.query(sql.toString(),param,new BeanPropertyRowMapper<>(StringForm.class));
		}catch(Exception e){
			return null;
		}
	}

	@Override
	public List<T_Sell_Shop_Temp> listTemp(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT sht_id,sht_pd_code,sht_sub_code,sht_state");
		sql.append(" ,sht_cr_code,sht_sz_code,sht_br_code,sht_bd_code,sht_tp_code");
//		sql.append(" ,p.pd_name,p.pd_no,(select cr_name from t_base_color c where cr_code=t.sht_cr_code and c.companyid=t.companyid LIMIT 1) as cr_name");
//		sql.append(" ,(select sz_name from t_base_size s where sz_code=t.sht_sz_code and s.companyid=t.companyid LIMIT 1) as sz_name");
//		sql.append(" ,(select br_name from t_base_bra b where br_code=t.sht_br_code and b.companyid=t.companyid LIMIT 1) as br_name");
		sql.append(" ,sht_da_code,sht_main,sht_slave,sht_vip_other");
//		sql.append(" ,(select em_name from t_base_emp b where em_code=t.sht_main and b.companyid=t.companyid LIMIT 1) as main_name");
//		sql.append(" ,(select em_name from t_base_emp b where em_code=t.sht_slave and b.companyid=t.companyid LIMIT 1) as slave_name");
		sql.append(" ,(select da_name from t_sell_displayarea b where da_code=t.sht_da_code and b.companyid=t.companyid LIMIT 1) as da_name");
		sql.append(" ,pd_no,pd_name,cr_name,sz_name,br_name,main_name,slave_name,em_name");//,vip_info
		sql.append(" ,sht_vip_code,sht_vip_money,sht_amount");
		sql.append(" ,sht_sell_price,sht_sign_price,sht_upcost_price");
		sql.append(" ,sht_cost_price,sht_price,sht_money");
		sql.append(" ,sht_sale_code,sht_sale_model,sht_sale_money");
		sql.append(" ,sht_sale_send_money,sht_hand_money,sht_vip_money");
		sql.append(" ,sht_ishand,sht_isvip,sht_ispoint");
		sql.append(" ,sht_isgift,sht_viptype,sht_vipvalue");
		sql.append(" ,sht_handtype,sht_handvalue,sht_remark");
		sql.append(" FROM t_sell_shop_temp t");
//		sql.append(" JOIN t_base_product p");
//		sql.append(" ON pd_code=t.sht_pd_code");
//		sql.append(" AND t.companyid=p.companyid");
		sql.append(" WHERE 1=1");
//		sql.append(" AND sht_state=:sell_type");
		sql.append(" AND sht_isputup=:putup");
		sql.append(" AND sht_em_code=:emp_code");
		sql.append(" AND sht_shop_code=:shop_code");
		sql.append(" AND t.companyid=:companyid");
		sql.append(" ORDER BY sht_id");
		List<T_Sell_Shop_Temp> list = namedParameterJdbcTemplate.query(sql.toString(), param
				, new BeanPropertyRowMapper<>(T_Sell_Shop_Temp.class));
		return list;
	}

	@Override
	public void updateEmp(Map<String, Object> param) {
		Object line = param.get("line");
		Object type = param.get("type");
		StringBuffer sql = new StringBuffer("");
		if(null != line){
			sql.append(" UPDATE t_sell_shop_temp SET");
			if(null != type && "main".equals(type)){
				sql.append(" sht_main=:em_code");
				sql.append(",main_name=:em_name");
			}else{
				sql.append(" sht_slave=:em_code");
				sql.append(",slave_name=:em_name");
			}
			if("1".equals(line)){
				sql.append(" WHERE 1=1");
				sql.append(" AND sht_isputup=:putup");
				sql.append(" AND sht_em_code=:emp_code");
				sql.append(" AND sht_shop_code=:shop_code");
				sql.append(" AND companyid=:companyid");
			}else{
				sql.append(" WHERE 1=1");
				sql.append(" AND sht_id=:sht_id");
			}
			namedParameterJdbcTemplate.update(sql.toString(), param);
		}
	}
	@Override
	public void updateArea(Map<String, Object> param) {
		Object line = param.get("line");
		StringBuffer sql = new StringBuffer("");
		if(null != line){
			sql.append(" UPDATE t_sell_shop_temp");
			sql.append(" SET sht_da_code=:da_code");
			if("1".equals(line)){
				sql.append(" WHERE 1=1");
				sql.append(" AND sht_isputup=:putup");
				sql.append(" AND sht_em_code=:emp_code");
				sql.append(" AND sht_shop_code=:shop_code");
				sql.append(" AND companyid=:companyid");
			}else{
				sql.append(" WHERE 1=1");
				sql.append(" AND sht_id=:sht_id");
			}
			namedParameterJdbcTemplate.update(sql.toString(), param);
		}
	}

	@Override
	public NumberForm countBack(Map<String, Object> param) {
		Object isnumber = param.get("isnumber");
		StringBuffer sql = new StringBuffer("");
		if(StringUtil.trimString(isnumber).equals("2")){//是零售单据
			sql.append("SELECT COUNT(1) as count,sum(shl_amount) as number");
			sql.append(" FROM t_sell_shoplist t");
			sql.append(" WHERE t.shl_number=:code");
			sql.append(" AND t.shl_state=0");
			sql.append(" AND t.shl_shop_code=:shop_code");
			sql.append(" AND t.companyid=:companyid");
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), param, 
					new BeanPropertyRowMapper<>(NumberForm.class));
		}else if(StringUtil.trimString(isnumber).equals("1")){
			NumberForm form = new NumberForm();
			form.setCount(1);
			form.setNumber(1d);
			return form;
		}else{
			return countSell(param);
		}
	}
	@Override
	public List<T_Sell_Shop_Temp> listBack(Map<String, Object> param) {
		Object isnumber = param.get("isnumber");
		StringBuffer sql = new StringBuffer("");
		if(StringUtil.trimString(isnumber).equals("2")){//是零售单据
			sql.append("SELECT ");
			sql.append(" shl_id sht_id,shl_pd_code sht_pd_code,shl_sub_code sht_sub_code,shl_cr_code sht_cr_code,shl_sz_code sht_sz_code,");
			sql.append(" pd_no,pd_name,cr_name,sz_name,br_name,main_name,slave_name,em_name,");//vip_info,
//			sql.append(" (select cr_name from t_base_color c where c.cr_code=t.shl_cr_code AND c.companyid=t.companyid LIMIT 1) AS cr_name,");
//			sql.append(" (select sz_name from t_base_size c where c.sz_code=t.shl_sz_code AND c.companyid=t.companyid LIMIT 1) AS sz_name,");
//			sql.append(" (select br_name from t_base_bra c where c.br_code=t.shl_br_code AND c.companyid=t.companyid LIMIT 1) AS br_name,");
			sql.append(" shl_br_code sht_br_code,shl_amount sht_amount,shl_sell_price sht_sell_price,shl_price sht_price,");
			sql.append(" shl_cost_price sht_cost_price,shl_upcost_price sht_upcost_price,shl_vip_code sht_vip_code,");
			sql.append(" pd_sale sht_ishand,pd_vip_sale sht_isvip,pd_point sht_ispoint,pd_gift sht_isgift,");
			sql.append(" pd_sign_price sht_sign_price");
			sql.append(" FROM t_sell_shoplist t");
//			sql.append(" JOIN t_base_product p");
//			sql.append(" ON p.pd_code=t.shl_pd_code");
//			sql.append(" AND p.companyid=t.companyid");
			sql.append(" WHERE shl_number=:code");
			sql.append(" AND shl_shop_code=:shop_code");
			sql.append(" AND t.companyid=:companyid");
			return namedParameterJdbcTemplate.query(sql.toString(), param, 
					new BeanPropertyRowMapper<>(T_Sell_Shop_Temp.class));
		}else if(StringUtil.trimString(isnumber).equals("1")){
			T_Sell_Shop_Temp temp = querySubCode(param);
			if(null != temp){
				List<T_Sell_Shop_Temp> list = new ArrayList<T_Sell_Shop_Temp>();
				list.add(temp);
				return list;
			}else{
				return null;
			}
		}else{
			return listSell(param);
		}
	}
	private T_Sell_Shop_Temp querySubCode(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT");
		sql.append(" bc_subcode sht_sub_code,");
		sql.append(" pd_no,");
		sql.append(" pd_name,");
		sql.append(" pd_unit,");
		sql.append(" pd_bd_code AS sht_bd_code,");
		sql.append(" pd_tp_code AS sht_tp_code,");
		sql.append(" pd_code AS sht_pd_code,");
		sql.append(" pd_sign_price sht_sign_price,");
		sql.append(" pd_cost_price sht_upcost_price,");
		sql.append(" pd_sale sht_ishand,");
		sql.append(" pd_vip_sale sht_isvip,");
		sql.append(" pd_point sht_ispoint,");
		sql.append(" pd_gift sht_isgift,");
		sql.append(" pd_cost_price AS sht_cost_price,");
		sql.append(" IFNULL(");
		sql.append(" pdp_sell_price,");
		sql.append(" pd_sell_price");
		sql.append(" ) AS sht_sell_price,");
		sql.append(" IFNULL(pdp_vip_price, pd_vip_price) AS sht_vip_price,");
		sql.append(" bc_color sht_cr_code,");
		sql.append(" bc_size sht_sz_code,");
		sql.append(" bc_bra sht_br_code,");
		sql.append(" (select cr_name from t_base_color c where c.cr_code=t.bc_color AND c.companyid=t.companyid LIMIT 1) AS cr_name,");
		sql.append(" (select sz_name from t_base_size c where c.sz_code=t.bc_size AND c.companyid=t.companyid LIMIT 1) AS sz_name,");
		sql.append(" (select br_name from t_base_bra c where c.br_code=t.bc_bra AND c.companyid=t.companyid LIMIT 1) AS br_name,");
		sql.append(" 1 sht_amount,");
		sql.append(" 0 AS sht_isputup,");
		sql.append(" :emp_code AS sht_em_code,");
		sql.append(" t.companyid,:shop_code AS sht_shop_code,");
		sql.append(" :sell_type sht_state");
		sql.append(" FROM t_base_barcode t");
		sql.append(" JOIN t_base_product p");
		sql.append(" ON p.pd_code=t.bc_pd_code");
		sql.append(" AND p.companyid=t.companyid");
		sql.append(" LEFT JOIN t_base_product_shop_price s ON s.pdp_pd_code = p.pd_code");
		sql.append(" AND s.companyid = p.companyid");
		sql.append(" AND pdp_shop_code=:shop_code");
		sql.append(" WHERE bc_barcode=:code");
		sql.append(" AND t.companyid=:companyid");
		try {
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), param, new BeanPropertyRowMapper<>(T_Sell_Shop_Temp.class));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	@Override
	public NumberForm countSell(Map<String, Object> param) {
		Object code = param.get("code");
		Object isall = param.get("isall");
		Object isquery = param.get("isquery");
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT count(1) as count,sum(sd_amount) as number");
		sql.append(" FROM t_stock_data t");
		sql.append(" JOIN t_base_product p");
		sql.append(" ON p.pd_code=t.sd_pd_code");
		sql.append(" AND p.companyid=t.companyid");
		sql.append(" WHERE 1=1");
		if(null == isall || "0".equals(isall)){//全部显示时不过滤数量
			sql.append(" AND sd_amount > 0");
		}
		if(null != code && !"".equals(code)){
			if(null != isquery && "1".equals(isquery)){
				sql.append(" AND (INSTR(pd_no,:code)>0 OR INSTR(pd_spell,:code)>0)");
			}else{
				sql.append(" AND pd_no=:code");
			}
		}
		sql.append(" AND pd_state IN (0,1)");//淘汰的商品不显示
		sql.append(" AND sd_dp_code=:dp_code");
		sql.append(" AND t.companyid=:companyid");
		return namedParameterJdbcTemplate.queryForObject(sql.toString(), param, 
				new BeanPropertyRowMapper<>(NumberForm.class));
	}

	@Override
	public List<T_Sell_Shop_Temp> listSell(Map<String, Object> param) {
		Object code = param.get("code");
		Object isall = param.get("isall");
		Object isquery = param.get("isquery");
		Object shop_type = param.get(CommonUtil.SHOP_TYPE);
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT sd_id sht_id,sd_code sht_sub_code,pd_no,pd_name,pd_unit,pd_bd_code as sht_bd_code,pd_tp_code as sht_tp_code,");
		sql.append(" pd_code as sht_pd_code,");
		sql.append(" pd_sign_price sht_sign_price,pd_cost_price sht_upcost_price,");
		sql.append(" pd_sale sht_ishand,pd_vip_sale sht_isvip,pd_point sht_ispoint,pd_gift sht_isgift,");
		if("3".equals(StringUtil.trimString(shop_type))){//自营店成本价就是总部成本价，自营店没有分店成本价
			sql.append(" pd_cost_price AS sht_cost_price,");
		}else{//加盟或合伙有自己成本价，若没有，就是总部配送价
			sql.append(" IFNULL(pdp_cost_price,pd_sort_price) AS sht_cost_price,");
		}
		sql.append(" IFNULL(pdp_sell_price,pd_sell_price) as sht_sell_price,");
		sql.append(" IFNULL(pdp_vip_price,pd_vip_price) AS sht_vip_price,");
		
		sql.append(" sd_cr_code sht_cr_code,sd_sz_code sht_sz_code,sd_br_code sht_br_code,sd_amount sht_amount,");
		sql.append(" (select cr_name from t_base_color c where c.cr_code=t.sd_cr_code AND c.companyid=t.companyid LIMIT 1) AS cr_name,");
		sql.append(" (select sz_name from t_base_size c where c.sz_code=t.sd_sz_code AND c.companyid=t.companyid LIMIT 1) AS sz_name,");
		sql.append(" (select br_name from t_base_bra c where c.br_code=t.sd_br_code AND c.companyid=t.companyid LIMIT 1) AS br_name");
		sql.append(" FROM t_stock_data t");
		sql.append(" JOIN t_base_product p");
		sql.append(" ON p.pd_code=t.sd_pd_code");
		sql.append(" AND p.companyid=t.companyid");		
		sql.append(" LEFT JOIN t_base_product_shop_price s");
		sql.append(" ON s.pdp_pd_code=p.pd_code");
		sql.append(" AND s.companyid=p.companyid");
		sql.append(" AND pdp_shop_code=:shop_code");
		sql.append(" WHERE 1=1");
		if(null == isall || "0".equals(isall)){//全部显示时不过滤数量
			sql.append(" AND sd_amount > 0");
		}
		if(null != code && !"".equals(code)){
			if(null != isquery && "1".equals(isquery)){
				sql.append(" AND (INSTR(pd_no,:code)>0 OR INSTR(pd_spell,:code)>0)");
			}else{
				sql.append(" AND pd_no=:code");
			}
		}
		sql.append(" AND pd_state IN (0,1)");//淘汰的商品不显示
		sql.append(" AND sd_dp_code=:dp_code");
		sql.append(" AND t.companyid=:companyid");
		sql.append(" ORDER BY sd_code");
		sql.append(" LIMIT :start,:end");
		return namedParameterJdbcTemplate.query(sql.toString(), param,
				new BeanPropertyRowMapper<>(T_Sell_Shop_Temp.class));
	}

	
	@Override
	public T_Sell_Shop_Temp product(Map<String, Object> param) {
		Object shop_type = param.get(CommonUtil.SHOP_TYPE);
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT pd_no,pd_name,pd_unit,pd_code AS sht_pd_code,pd_szg_code,");
		sql.append(" pd_bd_code AS sht_bd_code,pd_tp_code AS sht_tp_code,");
		sql.append(" pd_sign_price sht_sign_price,pd_cost_price sht_upcost_price,");
		sql.append(" pd_sale sht_ishand,pd_vip_sale sht_isvip,pd_point sht_ispoint,pd_gift sht_isgift,");
		if("3".equals(StringUtil.trimString(shop_type))){//自营店成本价就是总部成本价，自营店没有分店成本价
			sql.append(" pd_cost_price AS sht_cost_price,");
		}else{//加盟或合伙有自己成本价，若没有，就是总部配送价
			sql.append(" IFNULL(pdp_cost_price,pd_sort_price) AS sht_cost_price,");
		}
		sql.append(" IFNULL(pdp_sell_price,pd_sell_price) as sht_sell_price,");
		sql.append(" IFNULL(pdp_vip_price,pd_vip_price) AS sht_vip_price");
		sql.append(" FROM t_base_product p");
		sql.append(" LEFT JOIN t_base_product_shop_price s");
		sql.append(" ON s.pdp_pd_code=p.pd_code");
		sql.append(" AND s.companyid=p.companyid");
		sql.append(" AND pdp_shop_code=:shop_code");
		sql.append(" WHERE 1=1");
		sql.append(" AND pd_no=:pd_no");
		sql.append(" AND p.companyid=:companyid");
		sql.append(" LIMIT 1");
		try {
			T_Sell_Shop_Temp product = namedParameterJdbcTemplate.queryForObject(sql.toString(), param, new BeanPropertyRowMapper<>(T_Sell_Shop_Temp.class));
			return product;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public Map<String, Object> productSize(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT sz_code,sz_name");
		sql.append(" FROM t_base_sizelist szl");
		sql.append(" JOIN t_base_size sz ON szl_sz_code = sz_code AND szl.companyid = sz.companyid");
		sql.append(" WHERE 1=1");
		sql.append(" AND szl_szg_code = :pd_szg_code");
		sql.append(" AND szl.companyid = :companyid");
		sql.append(" ORDER BY szl_order");
		List<T_Base_Size> sizes = namedParameterJdbcTemplate.query(sql.toString(), 
				param,
				new BeanPropertyRowMapper<>(T_Base_Size.class));
		
		//2.获取颜色杯型信息
		sql.setLength(0);
		sql.append("SELECT DISTINCT cr_name,cr_code,IFNULL(br_code,'') AS br_code,IFNULL(br_name,'') AS br_name");
		sql.append(" FROM t_base_product_color pdc");
		sql.append(" JOIN t_base_color cr ON cr_code = pdc_cr_code AND pdc.companyid = pdc.companyid");
		sql.append(" LEFT JOIN t_base_product_br pdb ON pdb_pd_code = pdc_pd_code AND pdb.companyid = pdc.companyid");
		sql.append(" LEFT JOIN t_base_bra br ON br_code = pdb_br_code AND br.companyid = pdc.companyid");
		sql.append(" WHERE pdc_pd_code = :pd_code AND pdc.companyid = :companyid");
		sql.append(" ORDER BY cr_code,br_code");
		List<T_Buy_Product> inputs = namedParameterJdbcTemplate.query(sql.toString(), 
				param, 
				new BeanPropertyRowMapper<>(T_Buy_Product.class));
		sql.setLength(0);
		sql.append(" SELECT sd_code,sd_pd_code,sd_sz_code,sd_cr_code,sd_br_code,sd_amount");
		sql.append(" FROM t_stock_data sd");
		sql.append(" WHERE 1=1");
		sql.append(" AND sd_pd_code = :pd_code");
		sql.append(" AND sd_dp_code = :dp_code");
		sql.append(" AND sd.companyid = :companyid");
		List<T_Stock_Data> stocks = namedParameterJdbcTemplate.query(sql.toString(), 
				param, 
				new BeanPropertyRowMapper<>(T_Stock_Data.class));
		Map<String,Object> map = new HashMap<String, Object>(3);
		map.put("sizes", sizes);
		map.put("inputs", inputs);
		map.put("stocks", stocks);
		return map;
	}

	@Override
	public T_Sell_Shop_Temp subCode(Map<String, Object> param) {
		String sell_type = (String)param.get(CommonUtil.SELL_TYPE);
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT");
		sql.append(" bc_subcode sht_sub_code,");
		sql.append(" pd_no,");
		sql.append(" pd_name,");
		sql.append(" pd_unit,");
		sql.append(" (select cr_name from t_base_color c where c.cr_code=t.bc_color AND c.companyid=t.companyid LIMIT 1) AS cr_name,");
		sql.append(" (select sz_name from t_base_size c where c.sz_code=t.bc_size AND c.companyid=t.companyid LIMIT 1) AS sz_name,");
		sql.append(" (select br_name from t_base_bra c where c.br_code=t.bc_bra AND c.companyid=t.companyid LIMIT 1) AS br_name,");
		sql.append(" pd_bd_code AS sht_bd_code,");
		sql.append(" pd_tp_code AS sht_tp_code,");
		sql.append(" pd_code AS sht_pd_code,");
		sql.append(" pd_sign_price sht_sign_price,");
		sql.append(" pd_cost_price sht_upcost_price,");
		sql.append(" pd_sale sht_ishand,");
		sql.append(" pd_vip_sale sht_isvip,");
		sql.append(" pd_point sht_ispoint,");
		sql.append(" pd_gift sht_isgift,");
		sql.append(" pd_cost_price AS sht_cost_price,");
		sql.append(" IFNULL(");
		sql.append(" pdp_sell_price,");
		sql.append(" pd_sell_price");
		sql.append(" ) AS sht_sell_price,");
		sql.append(" IFNULL(pdp_vip_price, pd_vip_price) AS sht_vip_price,");
		sql.append(" bc_color sht_cr_code,");
		sql.append(" bc_size sht_sz_code,");
		sql.append(" bc_bra sht_br_code,");
		if(!"1".equals(sell_type)){
			sql.append(" 1 sht_amount,");
		}else{
			sql.append(" -1 sht_amount,");
		}
		sql.append(" 0 AS sht_isputup,");
		sql.append(" :emp_code AS sht_em_code,");
		sql.append(" :emp_name em_name,");
		sql.append(" t.companyid,:shop_code AS sht_shop_code,");
		sql.append(" :sell_type sht_state");
		sql.append(" FROM t_base_barcode t");
		sql.append(" JOIN t_base_product p");
		sql.append(" ON p.pd_code=t.bc_pd_code");
		sql.append(" AND p.companyid=t.companyid");
		sql.append(" LEFT JOIN t_base_product_shop_price s ON s.pdp_pd_code = p.pd_code");
		sql.append(" AND s.companyid = p.companyid");
		sql.append(" AND pdp_shop_code=:shop_code");
		sql.append(" WHERE bc_barcode=:code");
		sql.append(" AND t.companyid=:companyid");
		try {
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), param, new BeanPropertyRowMapper<>(T_Sell_Shop_Temp.class));
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public Integer queryNumber(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT sh_id ");
		sql.append(" FROM t_sell_shop t");
		sql.append(" WHERE sh_number=:code");
		sql.append(" AND sh_shop_code=:shop_code");
		sql.append(" AND t.companyid=:companyid");
		Integer id = null;
		try {
			id = namedParameterJdbcTemplate.queryForObject(sql.toString(), param,Integer.class);
		} catch (Exception e) {
		}
		return id;
	}

	@Override
	public void saveTemp(List<T_Sell_Shop_Temp> tempList) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" INSERT INTO t_sell_shop_temp (");
		sql.append(" pd_no,pd_name,cr_name,sz_name,br_name,main_name,slave_name,em_name,");//vip_info,
		sql.append(" sht_pd_code,sht_sub_code,sht_cr_code,sht_sz_code,sht_br_code,");
		sql.append(" sht_tp_code,sht_bd_code,sht_em_code,sht_vip_other,sht_vip_code,");
		sql.append(" sht_amount,sht_sell_price,sht_sign_price,sht_upcost_price,");
		sql.append(" sht_vip_price,sht_cost_price,sht_price,sht_money,sht_ishand,");
		sql.append(" sht_isvip,sht_ispoint,sht_isgift,sht_isputup,sht_state,");
		sql.append(" sht_handtype,sht_handvalue,sht_shop_code,companyid");
		sql.append(" )VALUES(");
		sql.append(" :pd_no,:pd_name,:cr_name,:sz_name,:br_name,:main_name,:slave_name,:em_name,");//:vip_info,
		sql.append(" :sht_pd_code,:sht_sub_code,:sht_cr_code,:sht_sz_code,:sht_br_code,");
		sql.append(" :sht_tp_code,:sht_bd_code,:sht_em_code,:sht_vip_other,:sht_vip_code,");
		sql.append(" :sht_amount,:sht_sell_price,:sht_sign_price,:sht_upcost_price,");
		sql.append(" :sht_vip_price,:sht_cost_price,:sht_price,:sht_money,:sht_ishand,");
		sql.append(" :sht_isvip,:sht_ispoint,:sht_isgift,:sht_isputup,:sht_state,");
		sql.append(" :sht_handtype,:sht_handvalue,:sht_shop_code,:companyid");
		sql.append(" )");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(tempList.toArray()));
	}
	
	@Override
	public void saveTemp(T_Sell_Shop_Temp temp) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" INSERT INTO t_sell_shop_temp (");
		sql.append(" pd_no,pd_name,cr_name,sz_name,br_name,main_name,slave_name,em_name,");//vip_info,
		sql.append(" sht_pd_code,sht_sub_code,sht_cr_code,sht_sz_code,sht_br_code,");
		sql.append(" sht_tp_code,sht_bd_code,sht_em_code,sht_vip_other,sht_vip_code,");
		sql.append(" sht_amount,sht_sell_price,sht_sign_price,sht_upcost_price,");
		sql.append(" sht_vip_price,sht_cost_price,sht_price,sht_money,sht_ishand,");
		sql.append(" sht_isvip,sht_ispoint,sht_isgift,sht_isputup,sht_state,");
		sql.append(" sht_handtype,sht_handvalue,sht_shop_code,companyid");
		sql.append(" )VALUES(");
		sql.append(" :pd_no,:pd_name,:cr_name,:sz_name,:br_name,:main_name,:slave_name,:em_name,");//:vip_info,
		sql.append(" :sht_pd_code,:sht_sub_code,:sht_cr_code,:sht_sz_code,:sht_br_code,");
		sql.append(" :sht_tp_code,:sht_bd_code,:sht_em_code,:sht_vip_other,:sht_vip_code,");
		sql.append(" :sht_amount,:sht_sell_price,:sht_sign_price,:sht_upcost_price,");
		sql.append(" :sht_vip_price,:sht_cost_price,:sht_price,:sht_money,:sht_ishand,");
		sql.append(" :sht_isvip,:sht_ispoint,:sht_isgift,:sht_isputup,:sht_state,");
		sql.append(" :sht_handtype,:sht_handvalue,:sht_shop_code,:companyid");
		sql.append(" )");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(temp));
	}

	@Override
	public void updateTemp(List<T_Sell_Shop_Temp> tempList) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" UPDATE t_sell_shop_temp SET");
		sql.append(" sht_amount=:sht_amount,sht_price=:sht_price,sht_money=:sht_money,");
		if(!StringUtil.isEmpty(tempList.get(0).getSht_vip_other())){
			sql.append("sht_vip_other=:sht_vip_other,");
		}
		sql.append(" pd_no=:pd_no,pd_name=:pd_name,cr_name=:cr_name,sz_name=:sz_name,br_name=:br_name,");
		sql.append(" main_name=:main_name,slave_name=:slave_name,em_name=:em_name,");//vip_info=:vip_info,
		sql.append(" sht_vip_code=:sht_vip_code,sht_sale_priority=:sht_sale_priority,");
		sql.append(" sht_sale_code=:sht_sale_code,sht_sale_model=:sht_sale_model,sht_sale_money=:sht_sale_money,");
		sql.append(" sht_hand_money=:sht_hand_money,sht_handtype=:sht_handtype,sht_handvalue=:sht_handvalue,");
		sql.append(" sht_viptype=:sht_viptype,sht_vipvalue=:sht_vipvalue,sht_vip_money=:sht_vip_money,");
		sql.append(" sht_ishand=:sht_ishand,sht_isvip=:sht_isvip,sht_ispoint=:sht_ispoint,sht_isgift=:sht_isgift,");
		sql.append(" sht_remark=:sht_remark");
		sql.append(" WHERE sht_id=:sht_id");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(tempList.toArray()));
	}

	@Override
	public void delTemp(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" DELETE FROM t_sell_shop_temp");
		sql.append(" WHERE sht_id=:sht_id");
		namedParameterJdbcTemplate.update(sql.toString(), param);
	}

	@Override
	public void clearTemp(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" DELETE FROM t_sell_shop_temp");
		sql.append(" WHERE 1=1");
		sql.append(" AND sht_isputup=:putup");
		sql.append(" AND sht_em_code=:emp_code");
		sql.append(" AND sht_shop_code=:shop_code");
		sql.append(" AND companyid=:companyid");
		namedParameterJdbcTemplate.update(sql.toString(), param);
	}

	@Override
	public void delGift(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" DELETE FROM t_sell_shop_temp");
		sql.append(" WHERE 1=1");
		sql.append(" AND sht_isgift=1");
		sql.append(" AND sht_sale_code<>''");
//		sql.append(" AND sht_sale_model<>'311'");
		sql.append(" AND sht_isputup=:putup");
		sql.append(" AND sht_em_code=:emp_code");
		sql.append(" AND sht_shop_code=:shop_code");
		sql.append(" AND companyid=:companyid");
		namedParameterJdbcTemplate.update(sql.toString(), param);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void saveGift(Map<String, Object> param) {
		List<T_Shop_Sale_Present> giftList = (List<T_Shop_Sale_Present>)param.get("addGift");
		if(null != giftList && giftList.size() > 0){
			Map<String, Object> map = SaleVO.getGift(giftList);
			param.putAll(map);
			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT pd_code AS sht_pd_code,");
			sql.append(" ssp_subcode as sht_subcode,");
			sql.append(" ssp_br_code as sht_cr_code,");
			sql.append(" ssp_sz_code as sht_sz_code,");
			sql.append(" ssp_br_code as sht_br_code,");
			sql.append(" ssp_count as sht_amount,");
			sql.append(" ssp_ss_code AS sht_sale_code,");
			sql.append(" :shop_code AS sht_shop_code,");
			sql.append(" :companyid AS companyid,");
			sql.append(" :emp_code AS sht_em_code,");
			sql.append(" :sell_type AS sht_state,");
			sql.append(" :putup AS sht_isputup,");
			sql.append(" pd_bd_code AS sht_bd_code,");
			sql.append(" pd_tp_code AS sht_tp_code,");
			sql.append(" pd_code AS sht_pd_code,");
			sql.append(" pd_sign_price sht_sign_price,");
			sql.append(" pd_cost_price sht_upcost_price,");
			sql.append(" pd_sale sht_ishand,");
			sql.append(" pd_vip_sale sht_isvip,");
			sql.append(" pd_point sht_ispoint,");
			sql.append(" pd_cost_price AS sht_cost_price,");
			sql.append(" IFNULL(pdp_sell_price, pd_sell_price) AS sht_sell_price,");
			sql.append(" IFNULL(pdp_vip_price, pd_vip_price) AS sht_vip_price");
			sql.append(" FROM t_shop_sale_present t");
			sql.append(" JOIN t_base_product p ");
			sql.append(" ON p.pd_code=t.ssp_pd_code");
			sql.append(" AND p.companyid=t.companyid");
			sql.append(" LEFT JOIN t_base_product_shop_price s ON s.pdp_pd_code = p.pd_code");
			sql.append(" AND s.companyid = p.companyid");
			sql.append(" AND pdp_shop_code =:shop_code");
			sql.append(" WHERE	1 = 1");
			sql.append(" AND p.ssp_subcode IN(:subcodes)");
			sql.append(" AND p.companyid =:companyid");
			sql.append(" GROUP BY t.ssp_ss_code,t.ssp_subcode");
			List<T_Sell_Shop_Temp> tempList = namedParameterJdbcTemplate.query(sql.toString(), param, 
					new BeanPropertyRowMapper<>(T_Sell_Shop_Temp.class));
			SaleVO.buildGifts(tempList,param);
			saveTemp(tempList);
		}
	}

	@Override
	public List<T_Sell_Shop_Temp> queryTemp(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT sht_id,sht_pd_code,sht_sub_code");
		sql.append(" ,sht_cr_code,sht_sz_code,sht_br_code,sht_bd_code,sht_tp_code");
		sql.append(" ,sht_da_code,sht_main,sht_slave,sht_vip_other");
		sql.append(" ,sht_vip_code,sht_amount");
		sql.append(" ,sht_sell_price,sht_sign_price,sht_upcost_price");
		sql.append(" ,sht_cost_price,sht_price,sht_money");
		sql.append(" ,sht_sale_code,sht_sale_model,sht_sale_money,sht_sale_priority");
		sql.append(" ,sht_sale_send_money,sht_hand_money,sht_vip_money");
		sql.append(" ,sht_ishand,sht_isvip,sht_ispoint");
		sql.append(" ,sht_isgift,sht_viptype,sht_vipvalue");
		sql.append(" ,sht_handtype,sht_handvalue");
		sql.append(" FROM t_sell_shop_temp t");
		sql.append(" WHERE 1=1");
//		sql.append(" AND sht_state=:sell_type");
		sql.append(" AND sht_isputup=:putup");
		sql.append(" AND sht_em_code=:emp_code");
		sql.append(" AND sht_shop_code=:shop_code");
		sql.append(" AND t.companyid=:companyid");
		try {
			List<T_Sell_Shop_Temp> list = namedParameterJdbcTemplate.query(sql.toString(), param
					, new BeanPropertyRowMapper<>(T_Sell_Shop_Temp.class));
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	
	@Override
	public T_Sell_Shop_Temp tempByID(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT sht_id,sht_pd_code,sht_sub_code");
		sql.append(" ,sht_cr_code,sht_sz_code,sht_br_code,sht_bd_code,sht_tp_code");
		sql.append(" ,sht_da_code,sht_main,sht_slave,sht_vip_other");
		sql.append(" ,sht_vip_code,sht_vip_money,sht_amount");
		sql.append(" ,sht_sell_price,sht_sign_price,sht_upcost_price");
		sql.append(" ,sht_cost_price,sht_price,sht_money");
		sql.append(" ,sht_sale_code,sht_sale_model,sht_sale_money");
		sql.append(" ,sht_sale_send_money,sht_hand_money,sht_vip_money");
		sql.append(" ,sht_ishand,sht_isvip,sht_ispoint");
		sql.append(" ,sht_isgift,sht_viptype,sht_vipvalue");
		sql.append(" ,sht_handtype,sht_handvalue");
		sql.append(" FROM t_sell_shop_temp t");
		sql.append(" WHERE 1=1");
		sql.append(" AND sht_id=:sht_id");
		T_Sell_Shop_Temp temp = namedParameterJdbcTemplate.queryForObject(sql.toString(), param
				, new BeanPropertyRowMapper<>(T_Sell_Shop_Temp.class));
		return temp;
	}
	@Override
	public T_Vip_Member queryVip(Map<String, Object> param) {
		Object vip_code = param.get("vip_code");
		Object shop_uptype = param.get(CommonUtil.SHOP_UPTYPE); 
		Object shop_type = param.get(CommonUtil.SHOP_TYPE);
		if (StringUtil.isEmpty(vip_code)) {
			return null;
		}
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT vm_id,vm_code,vm_mobile,vm_cardcode,vm_mt_code,vm_shop_code,vm_name,vm_sex,");
		sql.append(" vm_birthday_type,vm_birthday,vm_lunar_birth,vm_state,vm_date,vm_manager_code,");
		sql.append(" s.sp_name AS shop_name,vm_points,");
		sql.append(" mt_name,mt_discount");
		sql.append(" FROM t_vip_member t");
		sql.append(" JOIN t_vip_membertype mt");
		sql.append(" ON mt_code = vm_mt_code");
		sql.append(" AND mt.companyid=t.companyid");
		sql.append(" JOIN t_base_shop s");
		sql.append(" ON s.sp_code=t.vm_shop_code");
		sql.append(" AND s.companyid=t.companyid");
		if(!"4".equals(StringUtil.trimString(shop_type))){ // 如果是自营店，则下上级店铺相同所有会员
			sql.append(getSellShopSQL(shop_uptype, 0));
		}else{
			sql.append(" AND sp_code=:shop_code");
		} 
		sql.append(" WHERE 1 = 1");
		if (StringUtil.isNotEmpty(vip_code)) {
			sql.append(" AND (INSTR(vm_cardcode, :vip_code) > 0 OR INSTR(vm_mobile, :vip_code) > 0)");
		}
		sql.append(" AND t.companyid=:companyid");
		sql.append(" LIMIT 1");
		try{
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), param,new BeanPropertyRowMapper<>(T_Vip_Member.class));
		}catch(Exception e){
			return null;
		}
	}

	@Override
	public List<NumberForm> vipType(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT sht_tp_code as code,tr_rate as number");
		sql.append(" FROM t_vip_type_rate r");
		sql.append(" JOIN (");
		sql.append(" SELECT sht_pd_code,t.companyid,sht_tp_code");
		sql.append(" FROM t_sell_shop_temp t");
		sql.append(" WHERE 1=1");
		sql.append(" AND sht_isputup=:putup");
		sql.append(" AND sht_state=:sell_type");
		sql.append(" AND sht_em_code=:emp_code");
		sql.append(" AND sht_shop_code=:shop_code");
		sql.append(" AND t.companyid=:companyid");
		sql.append(" GROUP BY sht_tp_code) AS s");
		sql.append(" ON r.tr_tp_code=s.sht_tp_code");
		sql.append(" AND s.companyid=r.companyid");
		sql.append(" AND r.tr_mt_code=:mt_code");
		sql.append(" WHERE r.companyid=:companyid");
		try{
			return namedParameterJdbcTemplate.query(sql.toString(),param,new BeanPropertyRowMapper<>(NumberForm.class));
		}catch(Exception e){
			return null;
		}
	}
	@Override
	public List<NumberForm> vipBrand(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT sht_bd_code as code,br_rate as number");
		sql.append(" FROM t_vip_brand_rate r");
		sql.append(" JOIN (");
		sql.append(" SELECT sht_pd_code,t.companyid,sht_bd_code");
		sql.append(" FROM t_sell_shop_temp t");
		sql.append(" WHERE 1=1");
		sql.append(" AND sht_isputup=:putup");
		sql.append(" AND sht_state=:sell_type");
		sql.append(" AND sht_em_code=:emp_code");
		sql.append(" AND sht_shop_code=:shop_code");
		sql.append(" AND t.companyid=:companyid");
		sql.append(" GROUP BY sht_bd_code) AS s");
		sql.append(" ON r.br_bd_code=s.sht_bd_code");
		sql.append(" AND s.companyid=r.companyid");
		sql.append(" AND r.br_mt_code=:mt_code");
		sql.append(" WHERE r.companyid=:companyid");
		try{
			return namedParameterJdbcTemplate.query(sql.toString(),param,new BeanPropertyRowMapper<>(NumberForm.class));
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<SaleForm> querySale(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT ss_code,ss_mt_code,ss_point,ss_priority,ss_model,");//ssm_model,ssm_rule,ssm_scope,
		sql.append(" ssa_index,ssa_discount,ssa_buy_full,ssa_buy_number,ssa_donation_number,ssa_donation_amount,ssa_reduce_amount,ssa_increased_amount,");
		sql.append(" ssb_index,ssb_bd_code,ssb_discount,ssb_buy_full,ssb_buy_number,ssb_donation_amount,ssb_reduce_amount,ssb_increased_amount,");
		sql.append(" sst_index,sst_tp_code,sst_discount,sst_buy_full,sst_buy_number,sst_donation_amount,sst_reduce_amount,sst_increased_amount,");
		sql.append(" ssp_index,ssp_pd_code,ssp_discount,ssp_buy_full,ssp_buy_number,ssp_donation_amount,ssp_reduce_amount,ssp_increased_amount,ssp_special_offer");

		sql.append(" FROM t_shop_sale t");
		/*sql.append(" JOIN t_shop_sale_model m");
		sql.append(" ON m.ssm_code=t.ss_model");*/
		sql.append(" JOIN t_shop_sale_shop s");
		sql.append(" ON t.ss_code=s.sss_ss_code");
		sql.append(" AND t.companyid=s.companyid");
		sql.append(" AND s.sss_shop_code=:shop_code");
		sql.append(" LEFT JOIN t_shop_sale_all a");
		sql.append(" ON a.ssa_ss_code=t.ss_code");
		sql.append(" AND a.companyid=t.companyid");
		sql.append(" LEFT JOIN t_shop_sale_brand b");
		sql.append(" ON b.ssb_ss_code=t.ss_code");
		sql.append(" AND b.companyid=t.companyid");
		sql.append(" LEFT JOIN t_shop_sale_type y");
		sql.append(" ON y.sst_ss_code=t.ss_code");
		sql.append(" AND y.companyid=t.companyid");
		sql.append(" LEFT JOIN t_shop_sale_product p");
		sql.append(" ON p.ssp_ss_code=t.ss_code");
		sql.append(" AND p.companyid=t.companyid");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND :today BETWEEN ss_begin_date AND ss_end_date");
		sql.append(" AND INSTR(ss_week,:week)>0");
		sql.append(" AND ss_state = 1");
		sql.append(" AND t.companyid=:companyid");
		try {
			return namedParameterJdbcTemplate.query(sql.toString(), param,new BeanPropertyRowMapper<>(SaleForm.class));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<T_Shop_Sale_Present> queryPersent(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT ssp_subcode,ssp_count,ssp_pd_code,ssp_index ");
		sql.append(" FROM t_shop_sale_present");
		sql.append(" WHERE ssp_ss_code IN (:codes)");
		sql.append(" AND companyid=:companyid");
		try {
			return namedParameterJdbcTemplate.query(sql.toString(), param, 
					new BeanPropertyRowMapper<>(T_Shop_Sale_Present.class));
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public T_Sell_Shop_Temp putDown(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT sht_id,sht_vip_code,sht_state");
		sql.append(" FROM t_sell_shop_temp t");
		sql.append(" WHERE sht_isputup=1");
		sql.append(" AND sht_putup_no=:put_code");
		sql.append(" AND sht_em_code=:emp_code");
		sql.append(" AND sht_shop_code=:shop_code");
		sql.append(" AND t.companyid=:companyid");
		List<T_Sell_Shop_Temp> list = null;
		try {
			list = namedParameterJdbcTemplate.query(sql.toString(), param, new BeanPropertyRowMapper<>(T_Sell_Shop_Temp.class));
		} catch (Exception e) {
		}
		if(null != list && list.size() > 0){
			sql.setLength(0);
			sql.append(" UPDATE t_sell_shop_temp");
			sql.append(" SET sht_putup_no=''");
			sql.append(" ,sht_isputup=0");
			sql.append(" WHERE sht_id=:sht_id");
			namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(list.toArray()));
			return list.get(0);
		}
		return null;
	}

	@Override
	public void putUp(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" UPDATE t_sell_shop_temp");
		sql.append(" SET sht_putup_no=:put_code");
		sql.append(" ,sht_isputup=1");
		sql.append(" WHERE sht_isputup=:putup");//putup 默认是0
		sql.append(" AND sht_em_code=:emp_code");
		sql.append(" AND sht_shop_code=:shop_code");
		sql.append(" AND companyid=:companyid");
		namedParameterJdbcTemplate.update(sql.toString(), param);
	}

	@Override
	public void to_cash(Map<String, Object> param) {
		Integer ecu_flag = 0,cou_flag = 0;
		Object shop_uptype = param.get(CommonUtil.SHOP_UPTYPE); 
		Object shop_type = param.get(CommonUtil.SHOP_TYPE);
		T_Vip_Member vip = (T_Vip_Member)param.get(CommonUtil.KEY_VIP);
		if(null != vip){
			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT ecu_id");
			sql.append(" FROM t_sell_ecoupon_user t ");
			sql.append(" JOIN t_base_shop s");
			sql.append(" ON s.sp_code=t.ecu_shop_code");
			sql.append(" AND s.companyid=t.companyid");
			if(!"4".equals(StringUtil.trimString(shop_type))){ // 如果是自营店，则下上级店铺相同所有会员
				sql.append(getSellShopSQL(shop_uptype, 0));
			}else{
				sql.append(" AND sp_code=:shop_code");
			}
			sql.append(" WHERE 1=1");
			sql.append(" AND ecu_state=0");
			sql.append(" AND ecu_tel=:mobile");
			sql.append(" AND :today BETWEEN ecu_begindate AND ecu_enddate");
			sql.append(" AND t.companyid=:companyid");
			sql.append(" LIMIT 1");
			try {
				ecu_flag = namedParameterJdbcTemplate.queryForObject(sql.toString(), param,Integer.class);
			} catch (Exception e) {
			}
		}
		param.clear();
		param.put("ecu_flag", ecu_flag);//是否电子券
		param.put("cou_flag", cou_flag);//是否优惠券
	}
	/**
	 * 获取收银的最新编号
	 * */
	@Override
	public String cashNumber(Map<String, Object> param) {
		String sell_type = StringUtil.trimString(param.get(CommonUtil.SELL_TYPE));
		String shop_code = StringUtil.trimString(param.get(CommonUtil.SHOP_CODE));
		String emp_code = StringUtil.trimString(param.get(CommonUtil.EMP_CODE));
		Integer companyid = Integer.parseInt(StringUtil.trimString(param.get(CommonUtil.COMPANYID)));
		return cashNumber(sell_type, emp_code, shop_code, companyid);
	}

	@Override
	public T_Sell_Shop sumTemp(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT SUM(sht_amount) AS sh_amount,");
		sql.append(" SUM(sht_sell_price*sht_amount) AS sh_sell_money,");
		sql.append(" SUM(sht_cost_price*sht_amount) AS sh_cost_money,");
		sql.append(" SUM(sht_upcost_price*sht_amount) AS sh_upcost_money,");
		sql.append(" SUM(sht_money) AS sh_money");
		sql.append(" FROM t_sell_shop_temp");
		sql.append(" WHERE 1=1");
		sql.append(" AND sht_isputup=:putup");
		sql.append(" AND sht_state=:sell_type");
		sql.append(" AND sht_em_code=:emp_code");
		sql.append(" AND sht_shop_code=:shop_code");
		sql.append(" AND companyid=:companyid");
		try {
			T_Sell_Shop shop = namedParameterJdbcTemplate.queryForObject(sql.toString(), param, new BeanPropertyRowMapper<>(T_Sell_Shop.class));
			return shop;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void save(T_Sell_Shop sell) {
		StringBuffer sql = new StringBuffer();
		sql.append(" INSERT INTO t_sell_shop(");
		sql.append(" sh_number,sh_sysdate,sh_date,sh_em_code,sh_shop_code,");
		sql.append(" sh_vip_other,sh_vip_code,sh_amount,sh_money,");
		sql.append(" sh_sell_money,sh_cash,sh_cost_money,sh_upcost_money,");
		sql.append(" sh_cd_money,sh_state,companyid,");
		sql.append(" sh_cd_rate,sh_vc_money,sh_vc_rate,sh_point_money,");
		sql.append(" sh_ec_money,sh_sd_money,sh_bank_money,");
		sql.append(" sh_ali_money,sh_wx_money,");
		sql.append(" sh_mall_money,sh_lost_money,sh_st_code,sh_source,sh_remark");
		sql.append(" ) VALUES (");
		sql.append(" :sh_number,:sh_sysdate,:sh_date,:sh_em_code,:sh_shop_code,");
		sql.append(" :sh_vip_other,:sh_vip_code,:sh_amount,:sh_money,");
		sql.append(" :sh_sell_money,:sh_cash,:sh_cost_money,:sh_upcost_money,");
		sql.append(" :sh_cd_money,:sh_state,:companyid,");
		sql.append(" :sh_cd_rate,:sh_vc_money,:sh_vc_rate,:sh_point_money,");
		sql.append(" :sh_ec_money,:sh_sd_money,:sh_bank_money,");
		sql.append(" :sh_ali_money,:sh_wx_money,");
		sql.append(" :sh_mall_money,:sh_lost_money,:sh_st_code,:sh_source,:sh_remark");
		sql.append(" )");
		KeyHolder holder = new GeneratedKeyHolder(); 
		namedParameterJdbcTemplate.update(sql.toString(),new BeanPropertySqlParameterSource(sell),holder);
		sell.setSh_id(holder.getKey().intValue());
	}

	@Override
	public void saveList(Map<String, Object> param) {
		String number = StringUtil.trimString(param.get("number"));
		String dp_code = StringUtil.trimString(param.get("dp_code"));
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT sht_bd_code bd_code,sht_tp_code tp_code,");
		sql.append(" sht_pd_code shl_pd_code,sht_sub_code shl_sub_code,sht_cr_code shl_cr_code,");
		sql.append(" sht_sz_code shl_sz_code,sht_br_code shl_br_code,sht_vip_code shl_vip_code,");
		sql.append(" sht_amount shl_amount,sht_sell_price shl_sell_price,sht_upcost_price shl_upcost_price,");
		sql.append(" sht_cost_price shl_cost_price,sht_price shl_price,sht_money shl_money,");
		sql.append(" sht_main shl_main,sht_slave shl_slave,sht_da_code shl_da_code,sht_sale_code shl_sale_code,");
		sql.append(" '"+number).append("' AS shl_number,'"+dp_code+"' AS shl_dp_code,");
		sql.append("'"+DateUtil.getCurrentTime()+"' AS shl_sysdate,'"+DateUtil.getYearMonthDate()+"' AS shl_date,");
		sql.append(" sht_em_code shl_em_code,sht_state shl_state,sht_shop_code shl_shop_code,companyid");
		sql.append(" FROM t_sell_shop_temp t");
		sql.append(" WHERE 1=1");
		sql.append(" AND sht_isputup=:putup");
		sql.append(" AND sht_state=:sell_type");
		sql.append(" AND sht_em_code=:emp_code");
		sql.append(" AND sht_shop_code=:shop_code");
		sql.append(" AND companyid=:companyid");
		List<T_Sell_ShopList> list = namedParameterJdbcTemplate.query(sql.toString(), param, 
				new BeanPropertyRowMapper<>(T_Sell_ShopList.class));
		if(null != list && list.size() > 0){
			sql.setLength(0);
			sql.append("INSERT INTO t_sell_shoplist(");
			sql.append("shl_number,shl_date,shl_sysdate,shl_pd_code,");
			sql.append("shl_sub_code,shl_cr_code,shl_sz_code,shl_br_code,");
			sql.append("shl_em_code,shl_slave,shl_main,shl_da_code,");
			sql.append("shl_shop_code,shl_dp_code,shl_amount,shl_sell_price,");
			sql.append("shl_price,shl_cost_price,shl_upcost_price,");
			sql.append("shl_money,shl_state,companyid,shl_vip_code,shl_sale_code");
			sql.append(") VALUES (");
			sql.append(":shl_number,:shl_date,:shl_sysdate,:shl_pd_code,");
			sql.append(":shl_sub_code,:shl_cr_code,:shl_sz_code,:shl_br_code,");
			sql.append(":shl_em_code,:shl_slave,:shl_main,:shl_da_code,");
			sql.append(":shl_shop_code,:shl_dp_code,:shl_amount,:shl_sell_price,");
			sql.append(":shl_price,:shl_cost_price,:shl_upcost_price,");
			sql.append(":shl_money,:shl_state,:companyid,:shl_vip_code,:shl_sale_code");
			sql.append(")");
			namedParameterJdbcTemplate.batchUpdate(sql.toString(), 
					SqlParameterSourceUtils.createBatch(list.toArray()));
			param.put("sell_list", list);
		}
	}

	@Override
	public void updateDeposit(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer();
		sql.append(" UPDATE t_sell_deposit ");
		sql.append(" SET sd_state=1");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND sd_number=:sd_number");
		sql.append(" AND companyid=:companyid");
		namedParameterJdbcTemplate.update(sql.toString(), param);
	}

	@Override
	public void updateStock(Map<String, Object> param) {
		String dp_code = StringUtil.trimString(param.get("dp_code"));
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT sd_id,sd_code,sd_amount-s.sht_amount AS sd_amount");
		sql.append(" FROM t_stock_data d");
		sql.append(" JOIN (");
		sql.append(" SELECT SUM(sht_amount) sht_amount,sht_sub_code,t.companyid");
		sql.append(" FROM t_sell_shop_temp t");
		sql.append(" WHERE 1=1");
		sql.append(" AND t.sht_state=:sell_type");
		sql.append(" AND sht_em_code=:emp_code");
		sql.append(" AND sht_shop_code=:shop_code");
		sql.append(" AND sht_isputup=:putup");
		sql.append(" AND t.companyid=:companyid");
		sql.append(" GROUP BY sht_sub_code");
		sql.append(" ) s ");
		sql.append(" ON s.sht_sub_code=d.sd_code");
		sql.append(" AND s.companyid=d.companyid");
		sql.append(" WHERE sd_dp_code=:dp_code");
		sql.append(" AND d.companyid=:companyid");
		List<T_Stock_Data> list = namedParameterJdbcTemplate.query(sql.toString(), param, 
				new BeanPropertyRowMapper<>(T_Stock_Data.class));
		if(null != list && list.size() > 0){
			sql.setLength(0);
			sql.append("UPDATE t_stock_data SET");
			sql.append(" sd_amount=:sd_amount");
			sql.append(" WHERE sd_id=:sd_id");
			namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(list.toArray()));
		}
		sql.setLength(0);
		sql.append(" SELECT -SUM(sht_amount) sd_amount,sht_sub_code sd_code,t.companyid,");
		sql.append(" sht_pd_code sd_pd_code,sht_cr_code sd_cr_code,sht_sz_code sd_sz_code,");
		sql.append(" sht_br_code sd_br_code,'").append(dp_code).append("' AS sd_dp_code,");
		sql.append(" '"+DateUtil.getCurrentTime()+"' AS sd_date");
		sql.append(" FROM t_sell_shop_temp t");
		sql.append(" WHERE 1=1");
		sql.append(" AND t.sht_state=:sell_type");
		sql.append(" AND sht_em_code=:emp_code");
		sql.append(" AND sht_shop_code=:shop_code");
		sql.append(" AND sht_isputup=:putup");
		sql.append(" AND t.companyid=:companyid");
		sql.append(" GROUP BY sht_sub_code");
		List<T_Stock_Data> saveList = null;
		try {
			saveList = namedParameterJdbcTemplate.query(sql.toString(), param, 
					new BeanPropertyRowMapper<>(T_Stock_Data.class));
		} catch (Exception e) {
			saveList = null;
		}
		if(null != saveList && saveList.size() > 0){
			for(int i = saveList.size()-1;i >= 0;i--){//去除已经存在的数据
				for(T_Stock_Data data:list){
					System.out.println("i："+i+":"+data.getSd_code()+":"+saveList.get(i).getSd_code());
					if(data.getSd_code().equals(saveList.get(i).getSd_code())){
						saveList.remove(i);
						break;
					}
				}
			}
			if(saveList.size() > 0){//过滤后还有新增的数据
				sql.setLength(0);
				sql.append(" INSERT INTO t_stock_data(");
				sql.append(" sd_pd_code,sd_code,sd_cr_code,sd_sz_code,sd_br_code,");
				sql.append(" sd_dp_code,sd_amount,sd_date,companyid");
				sql.append(" )VALUES(");
				sql.append(" :sd_pd_code,:sd_code,:sd_cr_code,:sd_sz_code,:sd_br_code,");
				sql.append(" :sd_dp_code,:sd_amount,:sd_date,:companyid");
				sql.append(")");
				namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(saveList.toArray()));
			}
		}
	}

	@Override
	public void savePay(T_Sell_Pay_Temp pay) {
		StringBuffer sql = new StringBuffer();
		sql.append("INSERT INTO t_sell_pay_temp(");
		sql.append("companyid,pt_shop_code,pt_em_code,pt_state,");
		sql.append("pt_number,pt_money,pt_paycode");
		sql.append(") VALUES (");
		sql.append(":companyid,:pt_shop_code,:pt_em_code,0,:pt_number,:pt_money,:pt_paycode");
		sql.append(")");
		namedParameterJdbcTemplate.update(sql.toString(), 
				new BeanPropertySqlParameterSource(pay));
	}

	@Override
	public void delPay(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer();
		sql.append("DELETE FROM t_sell_pay_temp");
		sql.append(" WHERE pt_number=:pt_number");
		sql.append(" AND companyid=:companyid");
		namedParameterJdbcTemplate.update(sql.toString(), 
				param);
	}

	@Override
	public List<T_Sell_Pay_Temp> payList(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT pt_em_code,pt_state,");
		sql.append("pt_number,pt_money,pt_paycode");
		sql.append(" FROM t_sell_pay_temp");
		sql.append(" WHERE companyid=:companyid");
		sql.append(" AND pt_shop_code=:shop_code");
		return namedParameterJdbcTemplate.query(sql.toString(), param, 
				new BeanPropertyRowMapper<>(T_Sell_Pay_Temp.class));
	}
	
	
}
