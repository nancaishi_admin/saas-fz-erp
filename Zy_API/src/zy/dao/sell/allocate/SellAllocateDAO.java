package zy.dao.sell.allocate;

import java.util.List;
import java.util.Map;

import zy.dto.common.BarcodeImportDto;
import zy.dto.sell.allocate.SellAllocateReportDto;
import zy.entity.base.product.T_Base_Product;
import zy.entity.sell.allocate.T_Sell_Allocate;
import zy.entity.sell.allocate.T_Sell_AllocateList;
import zy.entity.stock.data.T_Stock_DataBill;

public interface SellAllocateDAO {
	Integer count(Map<String,Object> params);
	List<T_Sell_Allocate> list(Map<String,Object> params);
	T_Sell_Allocate load(Integer ac_id);
	T_Sell_Allocate load(String number,Integer companyid);
	T_Sell_Allocate check(String number,Integer companyid);
	List<T_Sell_AllocateList> detail_list_forsavetemp(String ac_number,Integer companyid);
	List<T_Sell_AllocateList> detail_list_forcopy(List<Long> ids,String sp_code);
	List<T_Sell_AllocateList> detail_list(Map<String, Object> params);
	List<T_Sell_AllocateList> detail_list_print(Map<String, Object> params);
	List<T_Sell_AllocateList> detail_sum(Map<String, Object> params);
	List<String> detail_szgcode(Map<String,Object> params);
	List<T_Sell_AllocateList> temp_list(Map<String, Object> params);
	List<T_Sell_AllocateList> temp_list_forimport(Integer us_id,Integer companyid);
	List<T_Sell_AllocateList> temp_list_forsave(String sp_code,Integer us_id,Integer companyid);
	List<T_Sell_AllocateList> temp_sum(Map<String, Object> params);
	List<String> temp_szgcode(Map<String,Object> params);
	T_Sell_AllocateList temp_loadBySubCode(String sub_code, Integer us_id, Integer companyid);
	Integer count_product(Map<String, Object> param);
	List<T_Base_Product> list_product(Map<String, Object> param);
	T_Base_Product load_product(String pd_code,String sp_code,Integer companyid);
	Map<String, Object> load_product_size(Map<String,Object> params);
	void temp_save(List<T_Sell_AllocateList> temps);
	void temp_save(T_Sell_AllocateList temp);
	void temp_update(List<T_Sell_AllocateList> temps);
	void temp_updateById(List<T_Sell_AllocateList> temps);
	void temp_update(T_Sell_AllocateList temp);
	void temp_updateRemarkById(T_Sell_AllocateList temp);
	void temp_updateRemarkByPdCode(T_Sell_AllocateList temp);
	void temp_del(List<T_Sell_AllocateList> temps);
	void temp_del(Integer acl_id);
	void temp_delByPiCode(T_Sell_AllocateList temp);
	void temp_clear(Integer us_id,Integer companyid);
	List<BarcodeImportDto> temp_listByImport(List<String> barCodes,String sp_code,Integer companyid);
	void save(T_Sell_Allocate allocate,List<T_Sell_AllocateList> details);
	void update(T_Sell_Allocate allocate, List<T_Sell_AllocateList> details);
	void updateSend(T_Sell_Allocate allocate);
	void updateReceive(T_Sell_Allocate allocate);
	void updateState(T_Sell_Allocate allocate);
	List<T_Stock_DataBill> listStock(String number,String dp_code, Integer companyid);
	void del(String ac_number, Integer companyid);
	void deleteList(String ac_number, Integer companyid);
	Integer countReport(Map<String,Object> params);
	List<SellAllocateReportDto> listReport(Map<String,Object> params);
}
