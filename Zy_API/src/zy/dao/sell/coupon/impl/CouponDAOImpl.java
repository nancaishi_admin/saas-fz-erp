package zy.dao.sell.coupon.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.sell.coupon.CouponDAO;
import zy.entity.sell.coupon.T_Sell_Coupon;
import zy.entity.sell.coupon.T_Sell_Coupon_Brand;
import zy.entity.sell.coupon.T_Sell_Coupon_Detail;
import zy.entity.sell.coupon.T_Sell_Coupon_Product;
import zy.entity.sell.coupon.T_Sell_Coupon_Type;
import zy.entity.sys.user.T_Sys_User;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;

@Repository
public class CouponDAOImpl extends BaseDaoImpl implements CouponDAO {

	@Override
	public Integer count(Map<String, Object> params) {
		Object shop_type = params.get(CommonUtil.SHOP_TYPE);
		Object begindate = params.get("begindate");
		Object enddate = params.get("enddate");
		Object sc_state = params.get("sc_state");
		Object sc_type = params.get("sc_type");
		Object sc_shop_code = params.get("sc_shop_code");
		Object sc_name = params.get("sc_name");
		Object sc_number = params.get("sc_number");
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT COUNT(1)");
		sql.append(" FROM t_sell_coupon t");
		sql.append(" JOIN t_base_shop sp ON sp.sp_code=t.sc_shop_code AND sp.companyid=t.companyid ");
		sql.append(" WHERE 1 = 1");
		if (StringUtil.isNotEmpty(begindate)) {
			sql.append(" AND DATE_FORMAT(t.sc_sysdate,'%Y-%m-%d') >= :begindate ");
		}
		if (StringUtil.isNotEmpty(enddate)) {
			sql.append(" AND DATE_FORMAT(t.sc_sysdate,'%Y-%m-%d') <= :enddate ");
		}
		if(StringUtil.isNotEmpty(sc_state)){
			sql.append(" AND sc_state = :sc_state ");
		}
		if(StringUtil.isNotEmpty(sc_shop_code)){
			sql.append(" AND sc_shop_code = :sc_shop_code ");
		}
		if(StringUtil.isNotEmpty(sc_type)){
			sql.append(" AND sc_type = :sc_type ");
		}
		if(StringUtil.isNotEmpty(sc_name)){
			sql.append(" AND INSTR(sc_name,:sc_name) > 0 ");
		}
		if(StringUtil.isNotEmpty(sc_number)){
			sql.append(" AND INSTR(sc_number,:sc_number) > 0 ");
		}
		if (shop_type == CommonUtil.ONE || shop_type == CommonUtil.TWO) {
			sql.append(" AND (sp.sp_upcode=:shop_code and (sp.sp_shop_type='"+CommonUtil.THREE+"' or sp.sp_shop_type='"+CommonUtil.FIVE+"'))");
		}else {
			sql.append(" and  t.sc_shop_code = :shop_code");
		}
		sql.append(" AND t.companyid=:companyid");
		return namedParameterJdbcTemplate.queryForObject(sql.toString(), params, Integer.class);
	}

	@Override
	public List<T_Sell_Coupon> list(Map<String, Object> params) {
		Object shop_type = params.get(CommonUtil.SHOP_TYPE);
		Object sidx = params.get(CommonUtil.SIDX);
		Object sord = params.get(CommonUtil.SORD);
		Object begindate = params.get("begindate");
		Object enddate = params.get("enddate");
		Object sc_state = params.get("sc_state");
		Object sc_type = params.get("sc_type");
		Object sc_shop_code = params.get("sc_shop_code");
		Object sc_name = params.get("sc_name");
		Object sc_number = params.get("sc_number");
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT sc_id,sc_number,sc_name,sc_shop_code,sc_full_money,sc_minus_money,sc_type,sc_amount,sc_receive_amount,sc_begindate,sc_enddate,");
		sql.append(" sc_state,sc_remark,sc_maker,sc_makedate,sc_us_id,t.companyid,sp.sp_name as sc_shop_name");
		sql.append(" FROM t_sell_coupon t");
		sql.append(" JOIN t_base_shop sp ON sp.sp_code=t.sc_shop_code AND sp.companyid=t.companyid ");
		sql.append(" WHERE 1 = 1");
		if (StringUtil.isNotEmpty(begindate)) {
			sql.append(" AND DATE_FORMAT(t.sc_sysdate,'%Y-%m-%d') >= :begindate ");
		}
		if (StringUtil.isNotEmpty(enddate)) {
			sql.append(" AND DATE_FORMAT(t.sc_sysdate,'%Y-%m-%d') <= :enddate ");
		}
		if(StringUtil.isNotEmpty(sc_state)){
			sql.append(" AND sc_state = :sc_state ");
		}
		if(StringUtil.isNotEmpty(sc_shop_code)){
			sql.append(" AND sc_shop_code = :sc_shop_code ");
		}
		if(StringUtil.isNotEmpty(sc_type)){
			sql.append(" AND sc_type = :sc_type ");
		}
		if(StringUtil.isNotEmpty(sc_name)){
			sql.append(" AND INSTR(sc_name,:sc_name) > 0 ");
		}
		if(StringUtil.isNotEmpty(sc_number)){
			sql.append(" AND INSTR(sc_number,:sc_number) > 0 ");
		}
		if (shop_type == CommonUtil.ONE || shop_type == CommonUtil.TWO) {
			sql.append(" AND (sp.sp_upcode=:shop_code and (sp.sp_shop_type='"+CommonUtil.THREE+"' or sp.sp_shop_type='"+CommonUtil.FIVE+"'))");
		}else {
			sql.append(" and  t.sc_shop_code = :shop_code");
		}
		sql.append(" AND t.companyid=:companyid");
		if(sidx != null && !"".equals(sidx)){
			sql.append(" ORDER BY ").append(sidx).append(" ").append(sord);
		}else {
			sql.append(" ORDER BY sc_id DESC");
		}
		sql.append(" LIMIT :start,:end");
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Sell_Coupon.class));
	}

	@Override
	public List<T_Sell_Coupon_Detail> listDetail(String sc_number,
			Integer sc_type, Integer companyid) {
		StringBuffer sql = new StringBuffer();
		if(sc_type == 1){//类别
			sql.append(" SELECT sct_id AS id,sct_tp_code AS code,tp_name AS name ");
			sql.append(" FROM t_sell_coupon_type t");
			sql.append(" JOIN t_base_type type ON t.sct_tp_code = type.tp_code AND t.companyid = type.companyid ");
			sql.append(" WHERE 1=1 ");
			sql.append(" AND t.sct_sc_number = :sc_number ");
			sql.append(" AND t.companyid = :companyid");
		}else if(sc_type == 2){//品牌
			sql.append(" SELECT scb_id AS id,scb_bd_code AS code,bd_name AS name ");
			sql.append(" FROM t_sell_coupon_brand t");
			sql.append(" JOIN t_base_brand brand ON t.scb_bd_code = brand.bd_code AND t.companyid = brand.companyid ");
			sql.append(" WHERE 1=1 ");
			sql.append(" AND t.scb_sc_number = :sc_number ");
			sql.append(" AND t.companyid = :companyid");
		}else if(sc_type == 3){//商品
			sql.append(" SELECT scp_id AS id,scp_pd_code AS code,pd_name AS name ");
			sql.append(" FROM t_sell_coupon_product t");
			sql.append(" JOIN t_base_product product ON t.scp_pd_code = product.pd_code AND t.companyid = product.companyid ");
			sql.append(" WHERE 1=1 ");
			sql.append(" AND t.scp_sc_number = :sc_number ");
			sql.append(" AND t.companyid = :companyid");
		}
		return namedParameterJdbcTemplate.query(sql.toString(), 
				new MapSqlParameterSource().addValue("sc_number", sc_number).addValue("companyid", companyid), 
				new BeanPropertyRowMapper<>(T_Sell_Coupon_Detail.class));
	}

	@Override
	public void save(Map<String, Object> params) {
		T_Sell_Coupon coupon = (T_Sell_Coupon)params.get("coupon");
		T_Sys_User user = (T_Sys_User)params.get("user");
		String[] shop_codes = coupon.getSc_shop_code().split(",");
		Integer sc_type = coupon.getSc_type();
		coupon.setSc_receive_amount(0);
		coupon.setSc_state(0);
		coupon.setSc_maker(user.getUs_name());
		coupon.setSc_makedate(DateUtil.getYearMonthDate());
		coupon.setSc_sysdate(DateUtil.getCurrentTime());
		coupon.setSc_us_id(user.getUs_id());
		StringBuffer sql = new StringBuffer("");
		for(int i=0;i<shop_codes.length;i++){
			String prefix = CommonUtil.NUMBER_PREFIX_ECOUPON + DateUtil.getYearMonthDateYYYYMMDD() + shop_codes[i];
			coupon.setSc_shop_code(shop_codes[i]);
			sql.setLength(0);
			sql.append("SELECT CONCAT(:prefix,f_addnumber(MAX(sc_number))) AS new_number");
			sql.append(" FROM t_sell_coupon");
			sql.append(" WHERE 1=1");
			sql.append(" AND INSTR(sc_number,:prefix) > 0");
			sql.append(" AND companyid = :companyid");
			String new_number = namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new MapSqlParameterSource().addValue("prefix", prefix).addValue("companyid", coupon.getCompanyid()), String.class);
			coupon.setSc_number(new_number);
			sql.setLength(0);
			sql.append("INSERT INTO t_sell_coupon");
			sql.append(" (sc_number,sc_name,sc_shop_code,sc_full_money,sc_minus_money,sc_type,sc_amount,sc_receive_amount,sc_begindate,sc_enddate,");
			sql.append(" sc_remark,sc_state,sc_maker,sc_makedate,sc_sysdate,sc_us_id,sc_show_chat,sc_show_sysinfo,companyid)");
			sql.append(" VALUES");
			sql.append(" (:sc_number,:sc_name,:sc_shop_code,:sc_full_money,:sc_minus_money,:sc_type,:sc_amount,:sc_receive_amount,:sc_begindate,:sc_enddate,");
			sql.append(" :sc_remark,:sc_state,:sc_maker,:sc_makedate,:sc_sysdate,:sc_us_id,:sc_show_chat,:sc_show_sysinfo,:companyid)");
			KeyHolder holder = new GeneratedKeyHolder(); 
			namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(coupon),holder);
			coupon.setSc_id(holder.getKey().intValue());
			
			if(sc_type == 1){//类别
				String[] codes = coupon.getCodes().split(",");
				List<T_Sell_Coupon_Type> typeList = new ArrayList<T_Sell_Coupon_Type>();
				T_Sell_Coupon_Type t_Sell_Coupon_Type = null;
				for(int j=0;j<codes.length;j++){
					t_Sell_Coupon_Type = new T_Sell_Coupon_Type();
					t_Sell_Coupon_Type.setSct_sc_number(new_number);
					t_Sell_Coupon_Type.setSct_tp_code(codes[j]);
					t_Sell_Coupon_Type.setCompanyid(coupon.getCompanyid());
					typeList.add(t_Sell_Coupon_Type);
				}
				sql.setLength(0);
				sql.append(" insert into t_sell_coupon_type ");
				sql.append(" (sct_sc_number,sct_tp_code,companyid)");
				sql.append(" VALUES ");
				sql.append(" (:sct_sc_number,:sct_tp_code,:companyid)");
				namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(typeList.toArray()));
			}
			
			if(sc_type == 2){//品牌
				String[] codes = coupon.getCodes().split(",");
				List<T_Sell_Coupon_Brand> brandList = new ArrayList<T_Sell_Coupon_Brand>();
				T_Sell_Coupon_Brand t_Sell_Coupon_Brand = null;
				for(int j=0;j<codes.length;j++){
					t_Sell_Coupon_Brand = new T_Sell_Coupon_Brand();
					t_Sell_Coupon_Brand.setScb_sc_number(new_number);
					t_Sell_Coupon_Brand.setScb_bd_code(codes[j]);
					t_Sell_Coupon_Brand.setCompanyid(coupon.getCompanyid());
					brandList.add(t_Sell_Coupon_Brand);
				}
				sql.setLength(0);
				sql.append(" insert into t_sell_coupon_brand ");
				sql.append(" (scb_sc_number,scb_bd_code,companyid)");
				sql.append(" VALUES ");
				sql.append(" (:scb_sc_number,:scb_bd_code,:companyid)");
				namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(brandList.toArray()));
			}
			
			if(sc_type == 3){//商品
				String[] codes = coupon.getCodes().split(",");
				List<T_Sell_Coupon_Product> productList = new ArrayList<T_Sell_Coupon_Product>();
				T_Sell_Coupon_Product t_Sell_Coupon_Product = null;
				for(int j=0;j<codes.length;j++){
					t_Sell_Coupon_Product = new T_Sell_Coupon_Product();
					t_Sell_Coupon_Product.setScp_sc_number(new_number);
					t_Sell_Coupon_Product.setScp_pd_code(codes[j]);
					t_Sell_Coupon_Product.setCompanyid(coupon.getCompanyid());
					productList.add(t_Sell_Coupon_Product);
				}
				sql.setLength(0);
				sql.append(" insert into t_sell_coupon_product ");
				sql.append(" (scp_sc_number,scp_pd_code,companyid)");
				sql.append(" VALUES ");
				sql.append(" (:scp_sc_number,:scp_pd_code,:companyid)");
				namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(productList.toArray()));
			}
		}
	}

	@Override
	public void update(Map<String, Object> params) {
		T_Sell_Coupon coupon = (T_Sell_Coupon)params.get("coupon");
		T_Sys_User user = (T_Sys_User)params.get("user");
		String sc_number = coupon.getSc_return_number();
		Integer companyid = user.getCompanyid();
		Integer sc_type_bak = coupon.getSc_type_bak();
		Integer sc_type = coupon.getSc_type();
		coupon.setSc_receive_amount(0);
		coupon.setSc_state(0);
		coupon.setSc_maker(user.getUs_name());
		coupon.setSc_makedate(DateUtil.getYearMonthDate());
		coupon.setSc_sysdate(DateUtil.getCurrentTime());
		coupon.setSc_us_id(user.getUs_id());
		coupon.setSc_number(sc_number);
		StringBuffer sql = new StringBuffer("");
		
		sql.append(" DELETE FROM t_sell_coupon");
		sql.append(" WHERE sc_number=:sc_number");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("sc_number", sc_number).addValue("companyid", companyid));
		if(sc_type_bak == 1){//类别
			sql.setLength(0);
			sql.append(" DELETE FROM t_sell_coupon_type");
			sql.append(" WHERE sct_sc_number=:sc_number");
			sql.append(" AND companyid = :companyid");
			namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("sc_number", sc_number).addValue("companyid", companyid));
		}
		if(sc_type_bak == 2){//品牌
			sql.setLength(0);
			sql.append(" DELETE FROM t_sell_coupon_brand");
			sql.append(" WHERE scb_sc_number=:sc_number");
			sql.append(" AND companyid = :companyid");
			namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("sc_number", sc_number).addValue("companyid", companyid));
		}
		if(sc_type_bak == 3){//商品
			sql.setLength(0);
			sql.append(" DELETE FROM t_sell_coupon_product");
			sql.append(" WHERE scp_sc_number=:sc_number");
			sql.append(" AND companyid = :companyid");
			namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("sc_number", sc_number).addValue("companyid", companyid));
		}
		
		sql.setLength(0);
		sql.append("INSERT INTO t_sell_coupon");
		sql.append(" (sc_number,sc_name,sc_shop_code,sc_full_money,sc_minus_money,sc_type,sc_amount,sc_receive_amount,sc_begindate,sc_enddate,");
		sql.append(" sc_remark,sc_state,sc_maker,sc_makedate,sc_sysdate,sc_us_id,sc_show_chat,sc_show_sysinfo,companyid)");
		sql.append(" VALUES");
		sql.append(" (:sc_number,:sc_name,:sc_shop_code,:sc_full_money,:sc_minus_money,:sc_type,:sc_amount,:sc_receive_amount,:sc_begindate,:sc_enddate,");
		sql.append(" :sc_remark,:sc_state,:sc_maker,:sc_makedate,:sc_sysdate,:sc_us_id,:sc_show_chat,:sc_show_sysinfo,:companyid)");
		KeyHolder holder = new GeneratedKeyHolder(); 
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(coupon),holder);
		coupon.setSc_id(holder.getKey().intValue());
			
		if(sc_type == 1){//类别
			String[] codes = coupon.getCodes().split(",");
			List<T_Sell_Coupon_Type> typeList = new ArrayList<T_Sell_Coupon_Type>();
			T_Sell_Coupon_Type t_Sell_Coupon_Type = null;
			for(int j=0;j<codes.length;j++){
				t_Sell_Coupon_Type = new T_Sell_Coupon_Type();
				t_Sell_Coupon_Type.setSct_sc_number(sc_number);
				t_Sell_Coupon_Type.setSct_tp_code(codes[j]);
				t_Sell_Coupon_Type.setCompanyid(coupon.getCompanyid());
				typeList.add(t_Sell_Coupon_Type);
			}
			sql.setLength(0);
			sql.append(" insert into t_sell_coupon_type ");
			sql.append(" (sct_sc_number,sct_tp_code,companyid)");
			sql.append(" VALUES ");
			sql.append(" (:sct_sc_number,:sct_tp_code,:companyid)");
			namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(typeList.toArray()));
		}
			
		if(sc_type == 2){//品牌
			String[] codes = coupon.getCodes().split(",");
			List<T_Sell_Coupon_Brand> brandList = new ArrayList<T_Sell_Coupon_Brand>();
			T_Sell_Coupon_Brand t_Sell_Coupon_Brand = null;
			for(int j=0;j<codes.length;j++){
				t_Sell_Coupon_Brand = new T_Sell_Coupon_Brand();
				t_Sell_Coupon_Brand.setScb_sc_number(sc_number);
				t_Sell_Coupon_Brand.setScb_bd_code(codes[j]);
				t_Sell_Coupon_Brand.setCompanyid(coupon.getCompanyid());
				brandList.add(t_Sell_Coupon_Brand);
			}
			sql.setLength(0);
			sql.append(" insert into t_sell_coupon_brand ");
			sql.append(" (scb_sc_number,scb_bd_code,companyid)");
			sql.append(" VALUES ");
			sql.append(" (:scb_sc_number,:scb_bd_code,:companyid)");
			namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(brandList.toArray()));
		}
			
		if(sc_type == 3){//商品
			String[] codes = coupon.getCodes().split(",");
			List<T_Sell_Coupon_Product> productList = new ArrayList<T_Sell_Coupon_Product>();
			T_Sell_Coupon_Product t_Sell_Coupon_Product = null;
			for(int j=0;j<codes.length;j++){
				t_Sell_Coupon_Product = new T_Sell_Coupon_Product();
				t_Sell_Coupon_Product.setScp_sc_number(sc_number);
				t_Sell_Coupon_Product.setScp_pd_code(codes[j]);
				t_Sell_Coupon_Product.setCompanyid(coupon.getCompanyid());
				productList.add(t_Sell_Coupon_Product);
			}
			sql.setLength(0);
			sql.append(" insert into t_sell_coupon_product ");
			sql.append(" (scp_sc_number,scp_pd_code,companyid)");
			sql.append(" VALUES ");
			sql.append(" (:scp_sc_number,:scp_pd_code,:companyid)");
			namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(productList.toArray()));
		}
	}

	@Override
	public T_Sell_Coupon check(String sc_number, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT sc_id,sc_number,sc_state,companyid");
		sql.append(" FROM t_sell_coupon t");
		sql.append(" WHERE sc_number = :sc_number");
		sql.append(" AND companyid = :companyid");
		sql.append(" LIMIT 1");
		try{
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new MapSqlParameterSource().addValue("sc_number", sc_number).addValue("companyid", companyid),
					new BeanPropertyRowMapper<>(T_Sell_Coupon.class));
		}catch(Exception e){
			return null;
		}
	}

	@Override
	public void del(String sc_number, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" DELETE FROM t_sell_coupon");
		sql.append(" WHERE sc_number=:sc_number");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("sc_number", sc_number).addValue("companyid", companyid));
		sql.setLength(0);
		sql.append(" DELETE FROM t_sell_coupon_brand");
		sql.append(" WHERE scb_sc_number=:sc_number");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("sc_number", sc_number).addValue("companyid", companyid));
		sql.setLength(0);
		sql.append(" DELETE FROM t_sell_coupon_product");
		sql.append(" WHERE scp_sc_number=:sc_number");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("sc_number", sc_number).addValue("companyid", companyid));
		sql.setLength(0);
		sql.append(" DELETE FROM t_sell_coupon_type");
		sql.append(" WHERE sct_sc_number=:sc_number");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("sc_number", sc_number).addValue("companyid", companyid));
	}

	@Override
	public T_Sell_Coupon load(Integer sc_id) {
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT sc_id,sc_number,sc_name,sc_shop_code,sc_full_money,sc_minus_money,sc_type,sc_amount,sc_receive_amount,sc_begindate,sc_enddate,");
		sql.append(" sc_remark,sc_state,sc_maker,sc_makedate,sc_sysdate,sc_us_id,sc_show_chat,sc_show_sysinfo,sp.sp_name as sc_shop_name");
		sql.append(" FROM t_sell_coupon t");
		sql.append(" JOIN t_base_shop sp ON sp.sp_code=t.sc_shop_code AND sp.companyid=t.companyid ");
		sql.append(" WHERE sc_id=:sc_id");
		sql.append(" LIMIT 1");
		return namedParameterJdbcTemplate.queryForObject(sql.toString(), new MapSqlParameterSource().addValue("sc_id", sc_id),
				new BeanPropertyRowMapper<>(T_Sell_Coupon.class));
	}

	@Override
	public void updateApprove(T_Sell_Coupon coupon) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" UPDATE t_sell_coupon");
		sql.append(" SET sc_state=:sc_state");
		sql.append(" WHERE sc_id=:sc_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(coupon));
	}

}
