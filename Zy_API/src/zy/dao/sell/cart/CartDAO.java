package zy.dao.sell.cart;

import java.util.List;

import zy.entity.sell.cart.T_Sell_Cart;
import zy.entity.sell.cart.T_Sell_CartList;

public interface CartDAO {
	List<T_Sell_CartList> temp_list(String scl_em_code,String scl_shop_code,Integer companyid);
	void updateAmount(Integer scl_amount,Long scl_id);
	T_Sell_CartList loadCartList(String sub_code,String em_code,String shop_code,Integer companyid);
	void saveCartList(T_Sell_CartList cartList);
	void delCartList(Long scl_id);
	void clearCartList(String em_code,String shop_code,Integer companyid);
	void updateCartListVip(String vm_code, String em_code, String shop_code, Integer companyid);
	void saveCart(T_Sell_Cart cart, List<T_Sell_CartList> cartLists);
}
