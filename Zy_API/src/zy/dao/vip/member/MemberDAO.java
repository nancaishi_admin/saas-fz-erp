package zy.dao.vip.member;

import java.util.List;
import java.util.Map;

import zy.entity.base.brand.T_Base_Brand;
import zy.entity.base.color.T_Base_Color;
import zy.entity.base.product.T_Base_Product;
import zy.entity.base.type.T_Base_Type;
import zy.entity.sell.cash.T_Sell_Shop;
import zy.entity.sell.cash.T_Sell_ShopList;
import zy.entity.sell.ecoupon.T_Sell_Ecoupon_User;
import zy.entity.sys.user.T_Sys_User;
import zy.entity.vip.member.T_Vip_ConsumeDetailList;
import zy.entity.vip.member.T_Vip_Member;
import zy.entity.vip.member.T_Vip_Member_Info;
import zy.entity.vip.member.T_Vip_PonitList;
import zy.entity.vip.member.T_Vip_Tag;
import zy.entity.vip.trylist.T_Vip_TryList;
import zy.entity.vip.visit.T_Vip_Visit;
import zy.form.NumberForm;

public interface MemberDAO {
	Integer count(Map<String,Object> params);
	List<T_Vip_Member> list(Map<String,Object> params);
	Integer birthday_count(Map<String,Object> params);
	List<T_Vip_Member> birthday_list(Map<String,Object> params);
	Map<String, Object> birthday_sum(Map<String, Object> params);
	Integer queryByCardCode(T_Vip_Member member,T_Sys_User user);
	Integer queryByMobile(T_Vip_Member member, T_Sys_User user);
	Integer queryByCardCode(T_Vip_Member member);
	Integer queryByMobile(T_Vip_Member member);
	T_Vip_Member queryByID(Integer vm_id);
	T_Vip_Member loadByCode(String vm_code,Integer companyid);
	T_Vip_Member_Info queryInfoByCode(String vm_code,Integer companyid);
	Integer queryInfoIdByCode(String vmi_code, Integer companyid);
	void save(T_Vip_Member member,T_Vip_Member_Info memberInfo);
	void update(T_Vip_Member member, T_Vip_Member_Info memberInfo);
	void updateImg(T_Vip_Member member);
	void del(Integer vm_id);
	void delInfo(Integer vmi_id);
	Integer assign_count(Map<String,Object> params);
	List<T_Vip_Member> assign_list(Map<String,Object> params);
	Map<String, Object> assign_sum(Map<String, Object> params);
	void update_assign_emp(Map<String, Object> params);
	void save_visit(T_Vip_Visit visit);
	void save_specialfocus(T_Vip_Visit visit);
	void save_reachshop(T_Vip_Visit visit);
	Map<String, List> getCheckImportMember(Map<String,Object> param);
	String queryMaxVmcode(Integer companyid);
	void saveMemberImport(List<T_Vip_Member> members);
	void saveMemberInfoImport(List<T_Vip_Member_Info> memberInfos);
	
	Map<String,Object> sell_count(Map<String,Object> paramMap);
	List<T_Vip_ConsumeDetailList> sell_list(Map<String,Object> paramMap);
	public List<T_Sell_Shop> shopList(Map<String, Object> paramMap);
	List<T_Sell_Ecoupon_User> ecouponList(Map<String,Object> paramMap);
	List<T_Vip_TryList> tryList(Map<String,Object> paramMap);
	List<T_Vip_Visit> backList(Map<String,Object> paramMap);
	List<NumberForm> timeList(Map<String,Object> paramMap);
	List<T_Base_Product> pushList(Map<String,Object> paramMap);
	List<T_Vip_Tag> query_vip_tag(Map<String,Object> params);
	void del_vip_tag(Map<String,Object> params);
	void del_member_tag(Map<String,Object> params);
	void set_member_tag(Map<String,Object> params);
	void save_vip_tag(T_Vip_Tag t_Vip_Tag);
	List<T_Base_Brand> queryVipOftenBrand(Map<String,Object> paramMap);
	List<T_Base_Type> queryVipOftenType(Map<String,Object> paramMap);
	List<T_Base_Color> queryVipOftenColor(Map<String,Object> paramMap);
	Map<String, Object> queryVipOftenPrice(Map<String,Object> paramMap);
	//---------------------------------前台--------------------
	Integer countByShop(Map<String,Object> params);
	List<T_Vip_Member> listByShop(Map<String,Object> params);
	public T_Vip_Member vipByCode(Map<String, Object> param);
	T_Vip_Member vipByMobile(Map<String, Object> param);
	T_Vip_Member vipByCode(String code,Integer companyid);
	T_Vip_Member vipById(Integer vm_id);
	void updatePoint(Map<String,Object> param);
	void updateBySell(Map<String,Object> param);
	List<T_Sell_ShopList> listSell(Map<String,Object> param);
	public List<T_Vip_PonitList> listPoint(Map<String, Object> param);
	//------------------------接口---------------------------
	List<T_Vip_Member> list_server(Map<String, Object> params);
	T_Vip_Member search(Map<String, Object> params);
}
