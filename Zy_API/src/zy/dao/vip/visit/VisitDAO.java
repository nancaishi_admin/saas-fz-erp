package zy.dao.vip.visit;

import java.util.List;
import java.util.Map;

import zy.entity.sell.ecoupon.T_Sell_Ecoupon_User;
import zy.entity.vip.member.T_Vip_Member;
import zy.entity.vip.visit.T_Vip_Visit;

public interface VisitDAO {
	Integer count(Map<String,Object> params);
	List<T_Vip_Member> list(Map<String,Object> params);
	Map<String, Object> sum(Map<String, Object> params);
	void save_ecoupon_user(T_Sell_Ecoupon_User ecouponUser);
	List<T_Vip_Member> list_birthday_visit(Map<String, Object> params);
	Integer detail_count(Map<String,Object> params);
	List<T_Vip_Visit> detail_list(Map<String,Object> params);
}
