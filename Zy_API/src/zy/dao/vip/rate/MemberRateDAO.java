package zy.dao.vip.rate;

import java.util.List;
import java.util.Map;

import zy.entity.vip.rate.T_Vip_Brand_Rate;
import zy.entity.vip.rate.T_Vip_Type_Rate;

public interface MemberRateDAO {
	public Integer count(Map<String,Object> param);
	public List<T_Vip_Brand_Rate> list(Map<String,Object> param);
	public Integer countType(Map<String,Object> param);
	public List<T_Vip_Type_Rate> listType(Map<String,Object> param);
	void saveBrand(Map<String,Object> param);
	void saveType(Map<String,Object> param);
	void delType(Map<String,Object> param);
	void delBrand(Map<String,Object> param);
	T_Vip_Brand_Rate brandByID(Integer br_id);
	T_Vip_Type_Rate typeByID(Integer tr_id);
	void updateBrand(Map<String,Object> param);
	void updateType(Map<String,Object> param);
}
