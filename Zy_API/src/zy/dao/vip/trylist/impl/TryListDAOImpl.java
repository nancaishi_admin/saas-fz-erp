package zy.dao.vip.trylist.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.vip.trylist.TryListDAO;
import zy.entity.vip.trylist.T_Vip_TryList;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Repository
public class TryListDAOImpl extends BaseDaoImpl implements TryListDAO{

	@Override
	public List<T_Vip_TryList> list(Map<String, Object> params) {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT tr_id,tr_sysdate,tr_pd_code,tr_sub_code,tr_cr_code,tr_sz_code,tr_br_code,tr_sell_price,");
		sql.append(" tr_vm_code,tr_em_code,tr_shop_code,t.companyid,pd_no,pd_name,pd_unit,pd_style,");
		sql.append(" (SELECT pdm_img_path FROM t_base_product_img pdm WHERE pdm_pd_code = pd_code AND pdm.companyid = pd.companyid LIMIT 1) AS pd_img_path,");
		sql.append(" (SELECT cr_name FROM t_base_color c WHERE c.cr_code=t.tr_cr_code AND c.companyid=t.companyid LIMIT 1) AS cr_name,");
		sql.append(" (SELECT sz_name FROM t_base_size s WHERE s.sz_code=t.tr_sz_code AND s.companyid=t.companyid LIMIT 1) AS sz_name,");
		sql.append(" (SELECT br_name FROM t_base_bra b WHERE b.br_code=t.tr_br_code AND b.companyid=t.companyid LIMIT 1) AS br_name,");
		sql.append(" (SELECT bd_name FROM t_base_brand bd WHERE bd_code = pd_bd_code AND bd.companyid = t.companyid LIMIT 1) AS bd_name,");
		sql.append(" (SELECT tp_name FROM t_base_type tp WHERE tp_code = pd_tp_code AND tp.companyid = t.companyid LIMIT 1) AS tp_name");
		sql.append(" FROM t_vip_trylist t");
		sql.append(" JOIN t_base_product pd ON pd_code = tr_pd_code AND pd.companyid = t.companyid");
		sql.append(" WHERE 1=1");
		sql.append(" AND tr_vm_code = :vm_code");
		sql.append(" AND t.companyid = :companyid");
		if(StringUtil.isNotEmpty(params.get(CommonUtil.SIDX))){
			sql.append(" ORDER BY ").append(params.get(CommonUtil.SIDX)).append(" ").append(params.get(CommonUtil.SORD));
		}else {
			sql.append(" ORDER BY tr_id DESC");
		}
		sql.append(" LIMIT :start,:end");
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Vip_TryList.class));
	}
	
	@Override
	public void save(T_Vip_TryList tryList) {
		StringBuffer sql = new StringBuffer("");
		sql.append("INSERT INTO t_vip_trylist");
		sql.append(" (tr_sysdate,tr_pd_code,tr_sub_code,tr_cr_code,tr_sz_code,tr_br_code,tr_sell_price,tr_vm_code,tr_em_code,tr_shop_code,companyid)");
		sql.append(" VALUES(:tr_sysdate,:tr_pd_code,:tr_sub_code,:tr_cr_code,:tr_sz_code,:tr_br_code,:tr_sell_price,:tr_vm_code,:tr_em_code,:tr_shop_code,:companyid)");
		KeyHolder holder = new GeneratedKeyHolder(); 
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(tryList),holder);
		tryList.setTr_id(holder.getKey().longValue());
	}

}
