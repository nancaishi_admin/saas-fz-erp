package zy.dao.batch.dealings.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.batch.dealings.BatchDealingsDAO;
import zy.entity.batch.dealings.T_Batch_Dealings;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Repository
public class BatchDealingsDAOImpl extends BaseDaoImpl implements BatchDealingsDAO{

	@Override
	public Integer count(Map<String, Object> params) {
		Object begindate = params.get("begindate");
		Object enddate = params.get("enddate");
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT COUNT(1)");
		sql.append(" FROM t_batch_dealings t");
		sql.append(" WHERE 1=1");
		if (StringUtil.isNotEmpty(begindate)) {
			sql.append(" AND dl_date >= :begindate ");
		}
		if (StringUtil.isNotEmpty(enddate)) {
			sql.append(" AND dl_date <= :enddate ");
		}
		sql.append(" AND dl_client_code = :dl_client_code");
		sql.append(" AND t.companyid=:companyid");
		return namedParameterJdbcTemplate.queryForObject(sql.toString(), params, Integer.class);
	}

	@Override
	public List<T_Batch_Dealings> list(Map<String, Object> params) {
		Object sidx = params.get(CommonUtil.SIDX);
		Object sord = params.get(CommonUtil.SORD);
		Object begindate = params.get("begindate");
		Object enddate = params.get("enddate");
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT dl_id,dl_client_code,dl_number,dl_type,dl_discount_money,dl_receivable,dl_received,dl_debt,dl_date,");
		sql.append(" dl_manager,dl_remark,dl_sysdate,dl_batchamount,companyid,");
		sql.append(" (SELECT ci_name FROM t_batch_client ci WHERE ci_code = dl_client_code AND ci.companyid = t.companyid LIMIT 1) AS client_name");
		sql.append(" FROM t_batch_dealings t");
		sql.append(" WHERE 1=1");
		if (StringUtil.isNotEmpty(begindate)) {
			sql.append(" AND dl_date >= :begindate ");
		}
		if (StringUtil.isNotEmpty(enddate)) {
			sql.append(" AND dl_date <= :enddate ");
		}
		sql.append(" AND dl_client_code = :dl_client_code");
		sql.append(" AND t.companyid=:companyid");
		if(sidx != null && !"".equals(sidx)){
			sql.append(" ORDER BY ").append(sidx).append(" ").append(sord);
		}else {
			sql.append(" ORDER BY dl_id ASC");
		}
		sql.append(" LIMIT :start,:end");
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Batch_Dealings.class));
	}
	
	@Override
	public void save(T_Batch_Dealings dealings) {
		StringBuffer sql = new StringBuffer("");
		sql.append("INSERT INTO t_batch_dealings");
		sql.append(" (dl_client_code,dl_number,dl_type,dl_discount_money,dl_receivable,dl_received,dl_debt,dl_date,dl_manager,dl_remark,dl_sysdate,dl_batchamount,companyid)");
		sql.append(" VALUES(:dl_client_code,:dl_number,:dl_type,:dl_discount_money,:dl_receivable,:dl_received,:dl_debt,:dl_date,:dl_manager,:dl_remark,:dl_sysdate,:dl_batchamount,:companyid)");
		KeyHolder holder = new GeneratedKeyHolder(); 
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(dealings),holder);
		dealings.setDl_id(holder.getKey().longValue());
	}

}
