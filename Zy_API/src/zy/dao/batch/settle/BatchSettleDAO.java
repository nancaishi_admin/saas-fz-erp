package zy.dao.batch.settle;

import java.util.List;
import java.util.Map;

import zy.entity.batch.fee.T_Batch_Fee;
import zy.entity.batch.sell.T_Batch_Sell;
import zy.entity.batch.settle.T_Batch_Settle;
import zy.entity.batch.settle.T_Batch_SettleList;

public interface BatchSettleDAO {
	Integer count(Map<String,Object> params);
	List<T_Batch_Settle> list(Map<String,Object> params);
	T_Batch_Settle load(Integer st_id);
	T_Batch_Settle load(String number, Integer companyid);
	T_Batch_Settle check(String number,Integer companyid);
	T_Batch_Settle check_settle(String ci_code, Integer companyid);
	List<T_Batch_SettleList> load_batch_forsavetemp(String ci_code,Integer companyid);
	List<T_Batch_SettleList> temp_list(Map<String, Object> params);
	List<T_Batch_SettleList> temp_list_forsave(Integer us_id,Integer companyid);
	void temp_save(List<T_Batch_SettleList> temps);
	void temp_update(List<T_Batch_SettleList> temps);
	void temp_clear(Integer us_id,Integer companyid);
	void temp_updateDiscountMoney(T_Batch_SettleList temp);
	void temp_updatePrepay(T_Batch_SettleList temp);
	void temp_updateRealMoney(T_Batch_SettleList temp);
	void temp_updateRemark(T_Batch_SettleList temp);
	void temp_updateJoin(String ids,String stl_join);
	List<T_Batch_SettleList> detail_list(String number,Integer companyid);
	void save(T_Batch_Settle settle, List<T_Batch_SettleList> details);
	void update(T_Batch_Settle settle, List<T_Batch_SettleList> details);
	void updateApprove(T_Batch_Settle settle);
	void updatePpNumber(T_Batch_Settle settle);
	List<T_Batch_Sell> listSellBySettle(String number,Integer companyid);
	List<T_Batch_Sell> listSellBySettle_Reverse(String number,Integer companyid);
	List<T_Batch_Fee> listFeeBySettle(String number,Integer companyid);
	List<T_Batch_Fee> listFeeBySettle_Reverse(String number,Integer companyid);
	void updateSellBySettle(List<T_Batch_Sell> sells);
	void updateFeeBySettle(List<T_Batch_Fee> fees);
	void del(String st_number, Integer companyid);
	void deleteList(String st_number, Integer companyid);
	T_Batch_SettleList check_settle_bill(String number, Integer companyid);
}
