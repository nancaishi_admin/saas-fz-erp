package zy.dao.batch.report;

import java.util.List;
import java.util.Map;

import zy.dto.batch.order.BatchOrderDetailReportDto;
import zy.dto.batch.sell.SellDetailReportDto;
import zy.dto.batch.sell.SellDetailSizeReportDto;
import zy.dto.batch.sell.SellReportCsbDto;
import zy.dto.batch.sell.SellReportDto;
import zy.dto.batch.sell.SellSummaryDto;
import zy.entity.base.type.T_Base_Type;

public interface BatchReportDAO {
	List<T_Base_Type> listTypeByUpCode(String tp_upcode,Integer companyid);
	List<T_Base_Type> listAllType(Integer companyid);
	Map<String, Object> countsumSellReport(Map<String, Object> params);
	List<SellReportDto> listSellReport(Map<String,Object> params);
	Map<String, Object> countsumSellDetailReport(Map<String, Object> params);
	List<SellDetailReportDto> listSellDetailReport(Map<String,Object> params);
	List<String> sell_size_szgcode(Map<String, Object> params);
	Integer countSellDetailSizeReport(Map<String, Object> params);
	List<SellDetailSizeReportDto> listSellDetailSizeReport(Map<String, Object> params);
	Map<String, Object> countsumOrderDetailReport(Map<String, Object> params);
	List<BatchOrderDetailReportDto> listOrderDetailReport(Map<String, Object> params);
	Integer countSellSummary(Map<String, Object> params);
	List<SellSummaryDto> listSellSummary(Map<String, Object> params);
	List<Map<String, Object>> type_level_sell(Map<String,Object> params);
	List<SellReportCsbDto> listSellReport_csb(Map<String, Object> params);
}
