package zy.dao.batch.client;

import java.util.List;
import java.util.Map;

import zy.dto.batch.money.ClientBackAnalysisDto;
import zy.dto.batch.money.ClientMoneyDetailsDto;
import zy.entity.batch.client.T_Batch_Client;
import zy.entity.batch.client.T_Batch_Client_Shop;

public interface ClientDAO {
	Integer count(Map<String,Object> params);
	List<T_Batch_Client> list(Map<String,Object> params);
	List<T_Batch_Client> list4dialog(Map<String, Object> params);
	T_Batch_Client load(Integer ci_id);
	T_Batch_Client load(String ci_code,Integer companyid);
	T_Batch_Client check(T_Batch_Client client);
	void save(T_Batch_Client client);
	void update(T_Batch_Client client);
	void del(Integer ci_id, String ci_code,Integer companyid);
	
	T_Batch_Client loadClient(String ci_code, Integer companyid);
	void updateReceivable(T_Batch_Client client);
	void updatePrepay(T_Batch_Client client);
	void updateSettle(T_Batch_Client client);
	
	Map<String, Object> countsumMoneyDetails(Map<String, Object> params);
	List<ClientMoneyDetailsDto> listMoneyDetails(Map<String,Object> params);
	List<ClientBackAnalysisDto> listBackAnalysis(Map<String,Object> params);
	
	List<T_Batch_Client_Shop> shop_list(Map<String,Object> params);
	T_Batch_Client_Shop check_client_shop(T_Batch_Client_Shop client_shop);
	void save_client_shop(T_Batch_Client_Shop client_shop);
	void del_client_shop(Map<String,Object> params);
	T_Batch_Client_Shop load_shop(Integer cis_id);
	void update_client_shop(T_Batch_Client_Shop client_Shop);
}
