package zy.dao.batch.fee;

import java.util.List;
import java.util.Map;

import zy.dto.batch.fee.BatchFeeReportDto;
import zy.entity.batch.fee.T_Batch_Fee;
import zy.entity.batch.fee.T_Batch_FeeList;

public interface BatchFeeDAO {
	Integer count(Map<String,Object> params);
	List<T_Batch_Fee> list(Map<String,Object> params);
	T_Batch_Fee load(Integer fe_id);
	T_Batch_Fee load(String number,Integer companyid);
	T_Batch_Fee check(String number,Integer companyid);
	List<T_Batch_FeeList> temp_list(Map<String, Object> params);
	List<String> temp_check(Integer us_id,Integer companyid);
	void temp_save(List<T_Batch_FeeList> temps);
	void temp_updateMoney(T_Batch_FeeList temp);
	void temp_updateRemark(T_Batch_FeeList temp);
	void temp_del(Integer fel_id);
	void temp_clear(Integer us_id,Integer companyid);
	List<T_Batch_FeeList> detail_list(Map<String, Object> params);
	List<T_Batch_FeeList> detail_list_forsavetemp(String fe_number,Integer companyid);
	void save(T_Batch_Fee fee, List<T_Batch_FeeList> details);
	void update(T_Batch_Fee fee, List<T_Batch_FeeList> details);
	void updateApprove(T_Batch_Fee fee);
	void del(String fe_number, Integer companyid);
	void deleteList(String fe_number, Integer companyid);
	List<BatchFeeReportDto> listReport(Map<String, Object> params);
	List<BatchFeeReportDto> listReportDetail(Map<String, Object> params);
}
