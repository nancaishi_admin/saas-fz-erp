package zy.dao.wx.user.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.wx.user.WxUserDAO;
import zy.entity.wx.my.T_Wx_Address;
import zy.entity.wx.order.T_Wx_Order;
import zy.entity.wx.user.T_Wx_User;
import zy.entity.wx.user.T_Wx_User_Account_Detail;
import zy.util.DateUtil;
import zy.util.MD5;
import zy.util.StringUtil;

@Repository
public class WxUserDAOImpl extends BaseDaoImpl implements WxUserDAO{
	@Override
	public T_Wx_User load(Integer wu_id) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT wu_id,wu_code,wu_mobile,wu_nickname,wu_password,wu_headimage,wu_state,wu_sysdate");
		sql.append(" FROM t_wx_user t");
		sql.append(" WHERE wu_id = :wu_id");
		sql.append(" LIMIT 1");
		try {
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), new MapSqlParameterSource().addValue("wu_id", wu_id),
					new BeanPropertyRowMapper<>(T_Wx_User.class));
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public T_Wx_User loadByMobile(String wu_mobile) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT wu_id,wu_code,wu_mobile,wu_nickname,wu_password,wu_headimage,wu_state,wu_sysdate");
		sql.append(" FROM t_wx_user t");
		sql.append(" WHERE wu_mobile = :wu_mobile");
		sql.append(" LIMIT 1");
		try {
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), new MapSqlParameterSource().addValue("wu_mobile", wu_mobile),
					new BeanPropertyRowMapper<>(T_Wx_User.class));
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public T_Wx_User login(String wu_mobile,String wu_password,String nickname,String avatarurl) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT wu_id,wu_code,wu_mobile,wu_nickname,wu_password,wu_headimage,wu_state,wu_sysdate");
		sql.append(" FROM t_wx_user t");
		sql.append(" WHERE wu_mobile = :wu_mobile ");
		sql.append(" AND wu_password = :wu_password");
		sql.append(" AND wu_state = 0");
		sql.append(" LIMIT 1");
		try {
			 T_Wx_User user = namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new MapSqlParameterSource().addValue("wu_mobile", wu_mobile).addValue("wu_password", wu_password),
					new BeanPropertyRowMapper<>(T_Wx_User.class));
			 if (user != null){
				 user.setWu_nickname(nickname);
				 user.setWu_headimage(avatarurl);
				 sql = new StringBuffer("");
				 sql.append(" UPDATE T_Wx_User SET wu_nickname= :wu_nickname, wu_headimage = :wu_headimage");
				 sql.append(" WHERE wu_id=:wu_id");
				 namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(user));
				
			 }
			return user;
//			return namedParameterJdbcTemplate.queryForObject(sql.toString(), 
//					new MapSqlParameterSource().addValue("wu_mobile", wu_mobile).addValue("wu_password", wu_password),
//					new BeanPropertyRowMapper<>(T_Wx_User.class));
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public T_Wx_User login(Map<String, Object> params) {
		String wu_mobile = StringUtil.trimString(params.get("mobile"));
		String wu_password = MD5.encryptMd5(StringUtil.trimString(params.get("pwd"))).toUpperCase();
//		String wu_nickname = StringUtil.trimString(params.get("nickname"));
//		String wu_headimage = StringUtil.trimString(params.get("avatarurl"));
		String wu_openid = StringUtil.trimString(params.get("openid"));
		StringBuffer sql = new StringBuffer("");
		

		sql.append(" SELECT wu_id,wu_code,wu_mobile,wu_nickname,wu_password,wu_headimage,wu_state,wu_sysdate,wu_openid,wu_totalmoney,wu_usedmoney,wu_balance");
		sql.append(" FROM t_wx_user t");
		sql.append(" WHERE wu_mobile = :wu_mobile ");
		sql.append(" AND wu_password = :wu_password");
		sql.append(" AND wu_state = 0");
		sql.append(" LIMIT 1");
		try {
			 T_Wx_User user = namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new MapSqlParameterSource().addValue("wu_mobile", wu_mobile).addValue("wu_password", wu_password),
					new BeanPropertyRowMapper<>(T_Wx_User.class));
			 if (user != null){
				 user.setWu_openid(wu_openid);
				 sql = new StringBuffer("");
				 sql.append(" UPDATE T_Wx_User SET wu_openid= :wu_openid");
				 sql.append(" WHERE wu_id=:wu_id");
				 namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(user));
				 
//				 user.setWu_nickname(wu_nickname);
//				 user.setWu_headimage(wu_headimage);
//				 sql = new StringBuffer("");
//				 sql.append(" UPDATE T_Wx_User SET wu_nickname= :wu_nickname, wu_headimage = :wu_headimage");
//				 sql.append(" WHERE wu_id=:wu_id");
//				 namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(user));
				
			 }
			return user;
//			return namedParameterJdbcTemplate.queryForObject(sql.toString(), 
//					new MapSqlParameterSource().addValue("wu_mobile", wu_mobile).addValue("wu_password", wu_password),
//					new BeanPropertyRowMapper<>(T_Wx_User.class));
		} catch (Exception e) {
			return null;
		}
		
		
	
		
	}
	
	@Override
	public T_Wx_User update(Map<String, Object> params) {
		String wu_code = StringUtil.trimString(params.get("wu_code"));
		String wu_nickname = StringUtil.trimString(params.get("nickname"));
		String wu_headimage = StringUtil.trimString(params.get("headimage"));
//		String wu_password = StringUtil.trimString(params.get("password"));
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT wu_id,wu_code,wu_mobile,wu_nickname,wu_password,wu_headimage,wu_state,wu_sysdate,wu_openid");
		sql.append(" FROM t_wx_user t");
		sql.append(" WHERE wu_code = :wu_code ");
		sql.append(" AND wu_state = 0");
		sql.append(" LIMIT 1");
		try {
			 T_Wx_User user = namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new MapSqlParameterSource().addValue("wu_code", wu_code),
					new BeanPropertyRowMapper<>(T_Wx_User.class));
			 if (user != null){
				 user.setWu_nickname(wu_nickname);
				 user.setWu_headimage(wu_headimage);
				 sql = new StringBuffer("");
				 sql.append(" UPDATE T_Wx_User SET wu_nickname= :wu_nickname, wu_headimage = :wu_headimage");
				 sql.append(" WHERE wu_id=:wu_id");
				 namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(user));
				 
//				 if (!"".equals(wu_password)){
//					 user.setWu_password(wu_password);
//					 sql = new StringBuffer("");
//					 sql.append(" UPDATE T_Wx_User SET wu_password= :wu_password");
//					 sql.append(" WHERE wu_id=:wu_id");
//					 namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(user));
//				 }
				
			 }
			return user;
		} catch (Exception e) {
			return null;
		}
		
		
	
		
	}
	
	@Override
	public T_Wx_User save(T_Wx_User wxUser) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT f_five_code(max(wu_code+0)) FROM t_wx_user ");
		String wu_code = namedParameterJdbcTemplate.queryForObject(sql.toString(), new BeanPropertySqlParameterSource(wxUser), String.class);
		wxUser.setWu_code(wu_code);
		sql.setLength(0);
		sql.append("INSERT INTO t_wx_user");
		sql.append(" (wu_code,wu_mobile,wu_nickname,wu_password,wu_headimage,wu_state,wu_sysdate)");
		sql.append(" VALUES(:wu_code,:wu_mobile,:wu_nickname,:wu_password,:wu_headimage,:wu_state,:wu_sysdate)");
		KeyHolder holder = new GeneratedKeyHolder(); 
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(wxUser),holder);
		wxUser.setWu_id(holder.getKey().intValue());
		
		return wxUser;
	}

	@Override
	public T_Wx_User updatePassword(String old_password, String new_password,String confirm_password) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" UPDATE t_wx_product");
		sql.append(" SET wp_state = :wp_state");
		sql.append(" WHERE wp_id=:wp_id");
		
		//namedParameterJdbcTemplate.update(sql.toString(), params);
		return null;
	}

	@Override
	public T_Wx_Address getAddress(String wu_code) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT *");
		sql.append(" FROM t_wx_address t");
		sql.append(" WHERE wd_user_code = :wu_code");
		sql.append(" ORDER BY wd_id DESC");
		sql.append(" LIMIT 1");
		try {
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), new MapSqlParameterSource().addValue("wu_code", wu_code),
					new BeanPropertyRowMapper<>(T_Wx_Address.class));
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<T_Wx_User_Account_Detail> getAccoutDetails(Map<String, Object> paramMap){
			
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT id,type,money,wu_code,createdate,remark,order_number ");
		sql.append(" FROM t_wx_user_account_detail t");
		sql.append(" WHERE 1=1");
		sql.append(" AND t.wu_code=:wu_code");
		sql.append(" ORDER BY id DESC");
		sql.append(" LIMIT :start,:end");
		return namedParameterJdbcTemplate.query(sql.toString(),paramMap, 
				new BeanPropertyRowMapper<>(T_Wx_User_Account_Detail.class));
	

	}
	
	@Override
	public T_Wx_User saveUserInfo(Map<String, Object> params) {
		String wu_code = StringUtil.trimString(params.get("wu_code"));
		String born = StringUtil.trimString(params.get("born"));
		String sex = StringUtil.trimString(params.get("sex"));
		String name = StringUtil.trimString(params.get("name"));
		String wxnumber = StringUtil.trimString(params.get("wxnumber"));
		String address = StringUtil.trimString(params.get("address"));

		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT wu_id,wu_code,wu_mobile,wu_nickname,wu_password,wu_headimage,wu_state,wu_sysdate,wu_openid");
		sql.append(" FROM t_wx_user t");
		sql.append(" WHERE wu_code = :wu_code ");
		sql.append(" AND wu_state = 0");
		sql.append(" LIMIT 1");
		try {
			 T_Wx_User user = namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new MapSqlParameterSource().addValue("wu_code", wu_code),
					new BeanPropertyRowMapper<>(T_Wx_User.class));
			 if (user != null){
				 user.setBorn(born);
				 user.setSex(sex);
				 user.setName(name);
				 user.setWxnumber(wxnumber);
				 user.setWu_headimage(address);
				 sql = new StringBuffer("");
				 sql.append(" UPDATE T_Wx_User SET name= :name, sex = :sex, born = :born, wxnumber = :wxnumber, address = :address");
				 sql.append(" WHERE wu_id=:wu_id");
				 namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(user));
				 

			 }
			return user;
		} catch (Exception e) {
			return null;
		}
		
		
	
		
	}
	
	@Override
	public T_Wx_User initUser(Map<String, Object> params) {
		String wu_openid = StringUtil.trimString(params.get("wu_openid"));
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT *");
		sql.append(" FROM t_wx_user t");
		sql.append(" WHERE wu_openid = :wu_openid ");
		sql.append(" AND wu_state = 0");
		sql.append(" LIMIT 1");
		try {
			 T_Wx_User user = namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new MapSqlParameterSource().addValue("wu_openid", wu_openid),
					new BeanPropertyRowMapper<>(T_Wx_User.class));
			 if (user == null){
				 user = new T_Wx_User();
				 user.setWu_openid(wu_openid);
				 user.setWu_totalmoney(0d);
				 user.setWu_usedmoney(0d);
				 user.setWu_balance(0d);
				 user.setWu_state(0);
				 user.setWu_sysdate(DateUtil.getCurrentTime());
			 	 //保存
				 save(user);
				 
				 return user;				
			 }
			return user;
		} catch (Exception e) {
			return null;
		}
		
		
	
		
	}
}
