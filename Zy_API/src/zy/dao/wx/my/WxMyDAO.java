package zy.dao.wx.my;

import java.util.List;
import java.util.Map;

import zy.entity.sell.ecoupon.T_Sell_Ecoupon_User;
import zy.entity.wx.my.T_Wx_Address;

public interface WxMyDAO {
	Map<String, Object> getMyValue(Map<String, Object> params);
	
	/**
	 * 根据用户编号获取当前密码
	 * @param wu_code 用户编号
	 * @return
	 */
	String getNowPasswordByWuCode(String wu_code);
	
	/**
	 * 根据用户编号修改密码
	 * @param wu_code
	 * @param new_password
	 */
	void updatePassword(String wu_code, String new_password);
	
	/**
	 * 根据用户编号查询地址列表
	 * @param wu_code
	 * @return
	 */
	List<T_Wx_Address> getAddrListByUserCode(String wu_code);
	
	/**
	 * 根据用户编号更新地址状态
	 * @param wu_code
	 */
	void updateAddressStateByUsCode(String wu_code);
	
	/**
	 * 增加用户地址
	 * @param t_Wx_Address
	 * @return
	 */
	T_Wx_Address saveAddress(T_Wx_Address t_Wx_Address);
	
	/**
	 * 根据地址id更新地址状态为默认
	 * @param wd_id
	 */
	void updateAddressStateById(Integer wd_id);
	
	/**
	 * 根据地址id获取地址信息
	 * @param wd_id
	 * @return
	 */
	T_Wx_Address getAddressInfoById(Integer wd_id);
	
	/**
	 * 根据用户编号获取默认地址信息
	 * @param wd_id
	 * @return
	 */
	T_Wx_Address getDefaultAddressInfoByCode(String wd_code);
	
	/**
	 * 根据地址id删除地址
	 * @param wd_id
	 */
	void deleteAddressById(Integer wd_id);
	
	/**
	 * 根据地址ID修改地址信息
	 * @param t_Wx_Address
	 */
	void updateAddressById(T_Wx_Address t_Wx_Address);
	
	/**
	 * 我的优惠券列表
	 * @param params
	 * @return
	 */
	List<T_Sell_Ecoupon_User> getMyCouponList(Map<String, Object> params);
}
