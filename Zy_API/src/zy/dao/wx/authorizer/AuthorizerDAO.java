package zy.dao.wx.authorizer;

import zy.entity.wx.authorizer.T_Wx_Authorizer;

public interface AuthorizerDAO {
	T_Wx_Authorizer load(String wa_appid);
	void save(T_Wx_Authorizer authorizer);
	void update(T_Wx_Authorizer authorizer);
}
