package zy.dao.wx.wechat.impl;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.wx.wechat.WechatInfoDAO;
import zy.entity.wx.wechat.T_Wx_WechatInfo;
import zy.util.StringUtil;

@Repository
public class WechatInfoDAOImpl extends BaseDaoImpl implements WechatInfoDAO{

	@Override
	public T_Wx_WechatInfo load(String shop_code, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT wx_id,wx_name,wx_originalid,wx_accountchat,wx_accounttype,wx_appid,wx_appsecret,wx_sysdate,wx_mt_code,wx_shop_code,");
		sql.append(" wx_enddate,wx_qrc_ticket,wx_mch_id,wx_mch_key,wx_mch_cer_path,wx_mch_pwd,companyid");
		sql.append(" FROM t_wx_wechatinfo");
		sql.append(" WHERE wx_shop_code = :wx_shop_code");
		sql.append(" AND companyid = :companyid");
		sql.append(" LIMIT 1");
		try {
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new MapSqlParameterSource().addValue("wx_shop_code", shop_code).addValue("companyid", companyid),
					new BeanPropertyRowMapper<>(T_Wx_WechatInfo.class));
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public void save(T_Wx_WechatInfo wechatInfo) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" INSERT INTO t_wx_wechatinfo");
		sql.append(" (wx_name,wx_originalid,wx_accountchat,wx_accounttype,wx_appid,wx_appsecret,wx_sysdate,wx_mt_code,wx_shop_code,wx_enddate,companyid,");
		sql.append(" wx_qrc_ticket,wx_mch_id,wx_mch_key,wx_mch_cer_path,wx_mch_pwd)");
		sql.append(" VALUES(:wx_name,:wx_originalid,:wx_accountchat,:wx_accounttype,:wx_appid,:wx_appsecret,:wx_sysdate,:wx_mt_code,:wx_shop_code,:wx_enddate,:companyid,");
		sql.append(" :wx_qrc_ticket,:wx_mch_id,:wx_mch_key,:wx_mch_cer_path,:wx_mch_pwd)");
		KeyHolder holder = new GeneratedKeyHolder(); 
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(wechatInfo),holder);
		wechatInfo.setWx_id(holder.getKey().intValue());
	}
	
	@Override
	public void update(T_Wx_WechatInfo wechatInfo) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" UPDATE t_wx_wechatinfo");
		sql.append(" SET wx_name = :wx_name");
		sql.append(" ,wx_originalid = :wx_originalid");
		sql.append(" ,wx_accountchat = :wx_accountchat");
		sql.append(" ,wx_appid = :wx_appid");
		sql.append(" ,wx_appsecret = :wx_appsecret");
		sql.append(" ,wx_mt_code = :wx_mt_code");
		sql.append(" ,wx_mch_id = :wx_mch_id");
		sql.append(" ,wx_mch_key = :wx_mch_key");
		if(StringUtil.isNotEmpty(wechatInfo.getWx_mch_cer_path())){
			sql.append(" ,wx_mch_cer_path = :wx_mch_cer_path");
		}
		if(StringUtil.isNotEmpty(wechatInfo.getWx_mch_pwd())){
			sql.append(" ,wx_mch_pwd = :wx_mch_pwd");
		}
		sql.append(" WHERE wx_id=:wx_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(wechatInfo));
	}

}
