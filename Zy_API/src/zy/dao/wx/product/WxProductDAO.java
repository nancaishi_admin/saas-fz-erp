package zy.dao.wx.product;

import java.util.List;
import java.util.Map;

import zy.entity.base.product.T_Base_Product;
import zy.entity.wx.product.T_Wx_Product;
import zy.entity.wx.product.T_Wx_ProductList;
import zy.entity.wx.product.T_Wx_ProductShop;

public interface WxProductDAO {
	Integer count(Map<String,Object> params);
	List<T_Wx_Product> list(Map<String,Object> params);
	T_Wx_Product load(Integer wp_id);
	T_Wx_Product load(String pd_code,String shop_code,Integer companyid);
	List<T_Wx_ProductShop> loadProductShop(String ps_number,Integer companyid);
	Integer count_product(Map<String, Object> params);
	List<T_Base_Product> list_product(Map<String, Object> params);
	Integer checkExist(Map<String, Object> params);
	public void save(List<T_Wx_Product> products,List<T_Wx_ProductList> productLists,List<T_Wx_ProductShop> productShops);
	void update(T_Wx_Product product);
	void updateState(Map<String, Object> params);
	void del(Map<String, Object> params);
	void saveProductShop(List<T_Wx_ProductShop> productShops);
	void delProductShop(List<String> shopCodes,String ps_number,Integer companyid);
	void saveProductList(List<T_Wx_ProductList> productLists);
	void delProductList(String pl_number,Integer pl_type,Integer companyid);
	List<T_Wx_Product> hot_sell(Map<String, Object> params);
	List<T_Wx_Product> listByType(Map<String, Object> params);
	List<String> listProductImgs(Map<String, Object> params);
}
