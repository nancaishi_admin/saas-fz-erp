package zy.dao.wx.product.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.wx.product.WxProductDAO;
import zy.entity.base.product.T_Base_Product;
import zy.entity.wx.product.T_Wx_Product;
import zy.entity.wx.product.T_Wx_ProductList;
import zy.entity.wx.product.T_Wx_ProductShop;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Repository
public class WxProductDAOImpl extends BaseDaoImpl implements WxProductDAO{

	@Override
	public Integer count(Map<String, Object> params) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT COUNT(1)");
		sql.append(" FROM t_wx_product t");
		sql.append(" JOIN t_base_product pd ON pd_code = wp_pd_code AND pd.companyid = t.companyid");
		sql.append(" WHERE 1=1");
		sql.append(" AND wp_shop_code = :shop_code");
		sql.append(" AND t.companyid = :companyid");
		return namedParameterJdbcTemplate.queryForObject(sql.toString(), params, Integer.class);
	}

	@Override
	public List<T_Wx_Product> list(Map<String, Object> params) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT wp_id,wp_number,wp_shop_code,wp_pd_code,wp_pd_name,wp_sell_price,wp_rate_price,wp_type,wp_state,wp_pt_code,wp_istry,wp_try_way,");
		sql.append(" wp_isbuy,wp_buy_way,wp_postfree,wp_subtitle,wp_weight,wp_sell_amount,wp_virtual_amount,wp_browse_count,wp_likecount,wp_sysdate,t.companyid,");
		sql.append(" pd_no,pd_season,pd_style,pd_year,pd_fabric,");
		sql.append(" (SELECT bd_name FROM t_base_brand bd WHERE bd_code = pd_bd_code AND bd.companyid = pd.companyid LIMIT 1) AS pd_bd_name,");
		sql.append(" (SELECT pt_name FROM t_wx_producttype pt WHERE pt_code = wp_pt_code AND pt_shop_code = wp_shop_code AND pt.companyid = t.companyid LIMIT 1) AS wp_pt_name,");
		sql.append(" (SELECT GROUP_CONCAT(dt_name) FROM t_wx_product_list pl ");
		sql.append(" JOIN common_dict dt ON dt_code = pl_code AND dt_type = 'PRODUCT_PROPERTY'");
		sql.append(" WHERE pl_type = 0 AND  pl_number = wp_number AND pl.companyid = t.companyid) AS wp_property_name,");
		sql.append(" (SELECT GROUP_CONCAT(dt_name) FROM t_wx_product_list pl ");
		sql.append(" JOIN common_dict dt ON dt_code = pl_code AND dt_type = 'PRODUCT_PROMISE'");
		sql.append(" WHERE pl_type = 1 AND  pl_number = wp_number AND pl.companyid = t.companyid) AS wp_promise_name");
		sql.append(" FROM t_wx_product t");
		sql.append(" JOIN t_base_product pd ON pd_code = wp_pd_code AND pd.companyid = t.companyid");
		sql.append(" WHERE 1=1");
		sql.append(" AND wp_shop_code = :shop_code");
		sql.append(" AND t.companyid = :companyid");
		if(StringUtil.isNotEmpty(params.get(CommonUtil.SIDX))){
			sql.append(" ORDER BY ").append(params.get(CommonUtil.SIDX)).append(" ").append(params.get(CommonUtil.SORD));
		}else {
			sql.append(" ORDER BY wp_id DESC");
		}
		sql.append(" LIMIT :start,:end");
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Wx_Product.class));
	}
	
	@Override
	public T_Wx_Product load(Integer wp_id) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT wp_id,wp_number,wp_shop_code,wp_pd_code,wp_pd_name,wp_sell_price,wp_rate_price,wp_type,wp_state,wp_pt_code,wp_istry,wp_try_way,");
		sql.append(" wp_isbuy,wp_buy_way,wp_postfree,wp_subtitle,wp_weight,wp_sell_amount,wp_virtual_amount,wp_browse_count,wp_likecount,wp_sysdate,t.companyid,");
		sql.append(" pd_no,pd_season,pd_style,pd_year,pd_fabric,");
		sql.append(" (SELECT bd_name FROM t_base_brand bd WHERE bd_code = pd_bd_code AND bd.companyid = pd.companyid LIMIT 1) AS pd_bd_name,");
		sql.append(" (SELECT pt_name FROM t_wx_producttype pt WHERE pt_code = wp_pt_code AND pt_shop_code = wp_shop_code AND pt.companyid = t.companyid LIMIT 1) AS wp_pt_name,");
		sql.append(" (SELECT GROUP_CONCAT(dt_name) FROM t_wx_product_list pl ");
		sql.append(" JOIN common_dict dt ON dt_code = pl_code AND dt_type = 'PRODUCT_PROPERTY'");
		sql.append(" WHERE pl_type = 0 AND  pl_number = wp_number AND pl.companyid = t.companyid) AS wp_property_name,");
		sql.append(" (SELECT GROUP_CONCAT(dt_name) FROM t_wx_product_list pl ");
		sql.append(" JOIN common_dict dt ON dt_code = pl_code AND dt_type = 'PRODUCT_PROMISE'");
		sql.append(" WHERE pl_type = 1 AND  pl_number = wp_number AND pl.companyid = t.companyid) AS wp_promise_name");
		sql.append(" FROM t_wx_product t");
		sql.append(" JOIN t_base_product pd ON pd_code = wp_pd_code AND pd.companyid = t.companyid");
		sql.append(" WHERE 1=1");
		sql.append(" AND wp_id = :wp_id");
		try {
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), new MapSqlParameterSource().addValue("wp_id", wp_id),
					new BeanPropertyRowMapper<>(T_Wx_Product.class));
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public T_Wx_Product load(String pd_code,String shop_code,Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT wp_id,wp_number,wp_shop_code,wp_pd_code,wp_pd_name,wp_sell_price,wp_rate_price,wp_type,wp_state,wp_pt_code,wp_istry,wp_try_way,");
		sql.append(" wp_isbuy,wp_buy_way,wp_postfree,wp_subtitle,wp_weight,wp_sell_amount,wp_virtual_amount,wp_browse_count,wp_likecount,wp_sysdate,t.companyid,");
		sql.append(" pd_no,pd_season,pd_style,pd_year,pd_fabric,");
		sql.append(" (SELECT bd_name FROM t_base_brand bd WHERE bd_code = pd_bd_code AND bd.companyid = pd.companyid LIMIT 1) AS pd_bd_name,");
		sql.append(" (SELECT pt_name FROM t_wx_producttype pt WHERE pt_code = wp_pt_code AND pt_shop_code = wp_shop_code AND pt.companyid = t.companyid LIMIT 1) AS wp_pt_name,");
		sql.append(" (SELECT GROUP_CONCAT(dt_name) FROM t_wx_product_list pl ");
		sql.append(" JOIN common_dict dt ON dt_code = pl_code AND dt_type = 'PRODUCT_PROPERTY'");
		sql.append(" WHERE pl_type = 0 AND  pl_number = wp_number AND pl.companyid = t.companyid) AS wp_property_name,");
		sql.append(" (SELECT GROUP_CONCAT(dt_name) FROM t_wx_product_list pl ");
		sql.append(" JOIN common_dict dt ON dt_code = pl_code AND dt_type = 'PRODUCT_PROMISE'");
		sql.append(" WHERE pl_type = 1 AND  pl_number = wp_number AND pl.companyid = t.companyid) AS wp_promise_name");
		sql.append(" FROM t_wx_product t");
		sql.append(" JOIN t_base_product pd ON pd_code = wp_pd_code AND pd.companyid = t.companyid");
		sql.append(" WHERE 1=1");
		sql.append(" AND wp_pd_code = :wp_pd_code");
		sql.append(" AND wp_shop_code = :wp_shop_code");
		sql.append(" AND t.companyid = :companyid");
		try {
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new MapSqlParameterSource().addValue("wp_shop_code", shop_code).addValue("wp_pd_code", pd_code).addValue("companyid", companyid),
					new BeanPropertyRowMapper<>(T_Wx_Product.class));
		} catch (Exception e) {
			return null;
		}
	}
	
	
	@Override
	public List<T_Wx_ProductShop> loadProductShop(String ps_number,Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT ps_id,ps_number,ps_shop_code,companyid");
		sql.append(" FROM t_wx_product_shop");
		sql.append(" WHERE ps_number = :ps_number");
		sql.append(" AND companyid = :companyid");
		return namedParameterJdbcTemplate.query(sql.toString(), 
				new MapSqlParameterSource().addValue("ps_number", ps_number).addValue("companyid", companyid), 
				new BeanPropertyRowMapper<>(T_Wx_ProductShop.class));
	}
	
	@Override
	public Integer count_product(Map<String, Object> param) {
		Object searchContent = param.get("searchContent");
		Object exactQuery = param.get("exactQuery");
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT Count(1)");
		sql.append(" FROM (");
		sql.append(" SELECT pd_id,");
		sql.append(" (SELECT SUM(sd_amount) FROM t_stock_data sd ");
		sql.append(" JOIN t_base_depot dp ON sd_dp_code = dp_code AND sd.companyid = dp.companyid");
		sql.append(" WHERE dp_shop_code = :shop_code AND sd_pd_code = pd_code AND sd.companyid = t.companyid");
		sql.append(" ) AS sd_amount");
		sql.append(" FROM t_base_product t");
		sql.append(" LEFT JOIN t_wx_product wp ON pd_code = wp_pd_code AND wp_shop_code = :shop_code AND wp.companyid = t.companyid");
		sql.append(" WHERE 1=1");
		if(StringUtil.isNotEmpty(searchContent)){
			if("1".equals(exactQuery)){
				sql.append(" AND (t.pd_name = :searchContent OR t.pd_no = :searchContent)");
			}else {
				sql.append(" AND (INSTR(t.pd_name,:searchContent)>0 OR INSTR(t.pd_spell,:searchContent)>0 OR INSTR(t.pd_no,:searchContent)>0)");
			}
        }
		sql.append(" AND wp_id IS NULL");
		sql.append(" AND t.companyid = :companyid");
		sql.append(" HAVING(sd_amount IS NOT NULL AND sd_amount > 0)");
		sql.append(" )t");
		return namedParameterJdbcTemplate.queryForObject(sql.toString(), param, Integer.class);
	}

	@Override
	public List<T_Base_Product> list_product(Map<String, Object> param) {
		Object searchContent = param.get("searchContent");
		Object exactQuery = param.get("exactQuery");
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT pd_id,pd_code,pd_no,pd_name,pd_sell_price,pd_season,pd_year,pd_style,pd_fabric,");
		sql.append(" (SELECT bd_name FROM t_base_brand bd WHERE bd_code = pd_bd_code AND bd.companyid = t.companyid LIMIT 1) AS pd_bd_name,");
		sql.append(" (SELECT SUM(sd_amount) FROM t_stock_data sd ");
		sql.append(" JOIN t_base_depot dp ON sd_dp_code = dp_code AND sd.companyid = dp.companyid");
		sql.append(" WHERE dp_shop_code = :shop_code AND sd_pd_code = pd_code AND sd.companyid = t.companyid");
		sql.append(" ) AS sd_amount");
		sql.append(" FROM t_base_product t");
		sql.append(" LEFT JOIN t_wx_product wp ON pd_code = wp_pd_code AND wp_shop_code = :shop_code AND wp.companyid = t.companyid");
		sql.append(" WHERE 1=1");
		if(StringUtil.isNotEmpty(searchContent)){
			if("1".equals(exactQuery)){
				sql.append(" AND (t.pd_name = :searchContent OR t.pd_no = :searchContent)");
			}else {
				sql.append(" AND (INSTR(t.pd_name,:searchContent)>0 OR INSTR(t.pd_spell,:searchContent)>0 OR INSTR(t.pd_no,:searchContent)>0)");
			}
        }
		sql.append(" AND wp_id IS NULL");
		sql.append(" AND t.companyid = :companyid");
		sql.append(" HAVING(sd_amount IS NOT NULL AND sd_amount > 0)");
		if(StringUtil.isNotEmpty(param.get(CommonUtil.SIDX))){
			sql.append(" ORDER BY ").append(param.get(CommonUtil.SIDX)).append(" ").append(param.get(CommonUtil.SORD));
		}else {
			sql.append(" ORDER BY pd_id DESC");
		}
		sql.append(" LIMIT :start,:end");
		return namedParameterJdbcTemplate.query(sql.toString(), param, new BeanPropertyRowMapper<>(T_Base_Product.class));
	}
	
	@Override
	public Integer checkExist(Map<String, Object> params) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT COUNT(1)");
		sql.append(" FROM t_wx_product t");
		sql.append(" WHERE 1=1");
		sql.append(" AND wp_pd_code IN(:pdCodes)");
		sql.append(" AND wp_shop_code = :shop_code");
		sql.append(" AND t.companyid = :companyid");
		return namedParameterJdbcTemplate.queryForObject(sql.toString(), params, Integer.class);
	}
	
	@Override
	public void save(List<T_Wx_Product> products,List<T_Wx_ProductList> productLists,List<T_Wx_ProductShop> productShops) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" INSERT INTO t_wx_product");
		sql.append(" (wp_number,wp_shop_code,wp_pd_code,wp_pd_name,wp_sell_price,wp_rate_price,wp_type,wp_state,wp_pt_code,wp_istry,wp_try_way,wp_isbuy,");
		sql.append(" wp_buy_way,wp_postfree,wp_subtitle,wp_weight,wp_sell_amount,wp_virtual_amount,wp_buy_limit,wp_browse_count,wp_likecount,wp_sysdate,companyid)");
		sql.append(" VALUES");
		sql.append(" (:wp_number,:wp_shop_code,:wp_pd_code,:wp_pd_name,:wp_sell_price,:wp_rate_price,:wp_type,:wp_state,:wp_pt_code,:wp_istry,:wp_try_way,:wp_isbuy,");
		sql.append(" :wp_buy_way,:wp_postfree,:wp_subtitle,:wp_weight,:wp_sell_amount,:wp_virtual_amount,:wp_buy_limit,:wp_browse_count,:wp_likecount,:wp_sysdate,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(products.toArray()));
		sql.setLength(0);
		sql.append(" INSERT INTO t_wx_product_list");
		sql.append(" (pl_number,pl_code,pl_type,companyid)");
		sql.append(" VALUES (:pl_number,:pl_code,:pl_type,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(productLists.toArray()));
		sql.setLength(0);
		sql.append(" INSERT INTO t_wx_product_shop");
		sql.append(" (ps_number,ps_shop_code,companyid)");
		sql.append(" VALUES (:ps_number,:ps_shop_code,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(productShops.toArray()));
	}
	
	@Override
	public void update(T_Wx_Product product) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" UPDATE t_wx_product");
		sql.append(" SET wp_pd_name = :wp_pd_name");
		sql.append(" ,wp_sell_price = :wp_sell_price");
		sql.append(" ,wp_rate_price = :wp_rate_price");
		sql.append(" ,wp_type = :wp_type");
		sql.append(" ,wp_pt_code = :wp_pt_code");
		sql.append(" ,wp_istry = :wp_istry");
		sql.append(" ,wp_try_way = :wp_try_way");
		sql.append(" ,wp_isbuy = :wp_isbuy");
		sql.append(" ,wp_buy_way = :wp_buy_way");
		sql.append(" ,wp_postfree = :wp_postfree");
		sql.append(" ,wp_subtitle = :wp_subtitle");
		sql.append(" ,wp_weight = :wp_weight");
		sql.append(" ,wp_virtual_amount = :wp_virtual_amount");
		sql.append(" ,wp_buy_limit = :wp_buy_limit");
		sql.append(" WHERE wp_id=:wp_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(product));
	}
	
	@Override
	public void updateState(Map<String, Object> params) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" UPDATE t_wx_product");
		sql.append(" SET wp_state = :wp_state");
		sql.append(" WHERE wp_id=:wp_id");
		namedParameterJdbcTemplate.update(sql.toString(), params);
	}
	
	@Override
	public void del(Map<String, Object> params) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" DELETE FROM t_wx_product");
		sql.append(" WHERE wp_id = :wp_id");
		namedParameterJdbcTemplate.update(sql.toString(), params);
		sql.setLength(0);
		sql.append(" DELETE FROM t_wx_product_list");
		sql.append(" WHERE pl_number = :wp_number");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), params);
		sql.setLength(0);
		sql.append(" DELETE FROM t_wx_product_shop");
		sql.append(" WHERE ps_number = :wp_number");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), params);
	}
	
	@Override
	public void saveProductShop(List<T_Wx_ProductShop> productShops) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" INSERT INTO t_wx_product_shop");
		sql.append(" (ps_number,ps_shop_code,companyid)");
		sql.append(" VALUES (:ps_number,:ps_shop_code,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(productShops.toArray()));
	}
	
	@Override
	public void delProductShop(List<String> shopCodes,String ps_number,Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" DELETE FROM t_wx_product_shop");
		sql.append(" WHERE ps_number = :ps_number");
		sql.append(" AND ps_shop_code IN (:shopCodes)");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(),
				new MapSqlParameterSource().addValue("shopCodes", shopCodes)
						.addValue("ps_number", ps_number)
						.addValue("companyid", companyid));
	}
	
	@Override
	public void saveProductList(List<T_Wx_ProductList> productLists) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" INSERT INTO t_wx_product_list");
		sql.append(" (pl_number,pl_code,pl_type,companyid)");
		sql.append(" VALUES (:pl_number,:pl_code,:pl_type,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(productLists.toArray()));
	}
	
	@Override
	public void delProductList(String pl_number,Integer pl_type,Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" DELETE FROM t_wx_product_list");
		sql.append(" WHERE pl_number = :pl_number");
		sql.append(" AND pl_type = :pl_type");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(),
				new MapSqlParameterSource().addValue("pl_number", pl_number)
						.addValue("pl_type", pl_type)
						.addValue("companyid", companyid));
	}
	
	@Override
	public List<T_Wx_Product> hot_sell(Map<String, Object> params) {
		String shop_type = StringUtil.trimString(params.get(CommonUtil.SHOP_TYPE));
		
		String productname = StringUtil.trimString(params.get("productname"));
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT wp_id,wp_number,wp_shop_code,wp_pd_code,wp_pd_name,wp_sell_price,wp_rate_price,wp_type,wp_state,wp_pt_code,wp_istry,wp_try_way,");
		sql.append(" wp_isbuy,wp_buy_way,wp_postfree,wp_subtitle,wp_weight,wp_sell_amount,wp_virtual_amount,wp_browse_count,wp_likecount,wp_sysdate,t.companyid,");
		sql.append(" pd_no,pd_season,pd_style,pd_year,pd_fabric,");
		sql.append(" (SELECT bd_name FROM t_base_brand bd WHERE bd_code = pd_bd_code AND bd.companyid = pd.companyid LIMIT 1) AS pd_bd_name,");
		sql.append(" (SELECT pt_name FROM t_wx_producttype pt WHERE pt_code = wp_pt_code AND pt_shop_code = wp_shop_code AND pt.companyid = t.companyid LIMIT 1) AS wp_pt_name,");
		sql.append(" (SELECT pdm_img_path FROM t_base_product_img pdm WHERE pdm_pd_code = t.wp_pd_code AND pdm.companyid = t.companyid LIMIT 1) AS pdm_img_path,");
		sql.append(" (SELECT GROUP_CONCAT(dt_name) FROM t_wx_product_list pl ");
		sql.append(" JOIN common_dict dt ON dt_code = pl_code AND dt_type = 'PRODUCT_PROPERTY'");
		sql.append(" WHERE pl_type = 0 AND  pl_number = wp_number AND pl.companyid = t.companyid) AS wp_property_name,");
		sql.append(" (SELECT GROUP_CONCAT(dt_name) FROM t_wx_product_list pl ");
		sql.append(" JOIN common_dict dt ON dt_code = pl_code AND dt_type = 'PRODUCT_PROMISE'");
		sql.append(" WHERE pl_type = 1 AND  pl_number = wp_number AND pl.companyid = t.companyid) AS wp_promise_name");
		sql.append(" FROM t_wx_product t");
		if(CommonUtil.THREE.equals(shop_type)){//自营店
			sql.append(" JOIN t_wx_product_shop ps ON ps_number = wp_number AND ps.companyid = t.companyid");
		}
		sql.append(" JOIN t_base_product pd ON pd_code = wp_pd_code AND pd.companyid = t.companyid");
		sql.append(" WHERE 1=1");
		if(CommonUtil.THREE.equals(shop_type)){//自营店
			sql.append(" AND ps_shop_code = :shop_code");
			sql.append(" AND wp_shop_code = :shop_upcode");
		}else {//总公司、分公司、加盟店、合伙店
			sql.append(" AND wp_shop_code = :shop_code");
		}
		
		//判断是否有模糊查询的数据存在
		if (!"".equals(productname)){
			sql.append(" AND t.wp_pd_name LIKE '%"+productname+"%'");
			
		}
		sql.append(" AND t.companyid = :companyid");
		sql.append(" ORDER BY wp_virtual_amount DESC");
		sql.append(" LIMIT :start,:end");
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Wx_Product.class));
	}
	
	@Override
	public List<T_Wx_Product> listByType(Map<String, Object> params) {
		String shop_type = StringUtil.trimString(params.get(CommonUtil.SHOP_TYPE));
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT wp_id,wp_number,wp_shop_code,wp_pd_code,wp_pd_name,wp_sell_price,wp_rate_price,wp_type,wp_state,wp_pt_code,wp_istry,wp_try_way,");
		sql.append(" wp_isbuy,wp_buy_way,wp_postfree,wp_subtitle,wp_weight,wp_sell_amount,wp_virtual_amount,wp_browse_count,wp_likecount,wp_sysdate,t.companyid,");
		sql.append(" pd_no,pd_season,pd_style,pd_year,pd_fabric,");
		sql.append(" (SELECT bd_name FROM t_base_brand bd WHERE bd_code = pd_bd_code AND bd.companyid = pd.companyid LIMIT 1) AS pd_bd_name,");
		sql.append(" (SELECT pt_name FROM t_wx_producttype pt WHERE pt_code = wp_pt_code AND pt_shop_code = wp_shop_code AND pt.companyid = t.companyid LIMIT 1) AS wp_pt_name,");
		sql.append(" (SELECT pdm_img_path FROM t_base_product_img pdm WHERE pdm_pd_code = t.wp_pd_code AND pdm.companyid = t.companyid LIMIT 1) AS pdm_img_path,");
		sql.append(" (SELECT GROUP_CONCAT(dt_name) FROM t_wx_product_list pl ");
		sql.append(" JOIN common_dict dt ON dt_code = pl_code AND dt_type = 'PRODUCT_PROPERTY'");
		sql.append(" WHERE pl_type = 0 AND  pl_number = wp_number AND pl.companyid = t.companyid) AS wp_property_name,");
		sql.append(" (SELECT GROUP_CONCAT(dt_name) FROM t_wx_product_list pl ");
		sql.append(" JOIN common_dict dt ON dt_code = pl_code AND dt_type = 'PRODUCT_PROMISE'");
		sql.append(" WHERE pl_type = 1 AND  pl_number = wp_number AND pl.companyid = t.companyid) AS wp_promise_name");
		sql.append(" FROM t_wx_product t");
		if(CommonUtil.THREE.equals(shop_type)){//自营店
			sql.append(" JOIN t_wx_product_shop ps ON ps_number = wp_number AND ps.companyid = t.companyid");
		}
		sql.append(" JOIN t_base_product pd ON pd_code = wp_pd_code AND pd.companyid = t.companyid");
		sql.append(" WHERE 1=1");
		if(CommonUtil.THREE.equals(shop_type)){//自营店
			sql.append(" AND ps_shop_code = :shop_code");
			sql.append(" AND wp_shop_code = :shop_upcode");
		}else {//总公司、分公司、加盟店、合伙店
			sql.append(" AND wp_shop_code = :shop_code");
		}
		sql.append(" AND wp_pt_code = :pt_code");
		sql.append(" AND t.companyid = :companyid");
		sql.append(" ORDER BY wp_id DESC");
		sql.append(" LIMIT :start,:end");
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Wx_Product.class));
	}
	
	@Override
	public List<String> listProductImgs(Map<String, Object> params) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT pdm_img_path");
		sql.append(" FROM t_base_product_img");
		sql.append(" WHERE pdm_pd_code = :pd_code");
		sql.append(" AND companyid = :companyid");
		return namedParameterJdbcTemplate.queryForList(sql.toString(), params, String.class);
	}
}
