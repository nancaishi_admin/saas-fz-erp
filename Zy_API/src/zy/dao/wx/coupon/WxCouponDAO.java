package zy.dao.wx.coupon;

import java.util.List;
import java.util.Map;

import zy.entity.sell.ecoupon.T_Sell_ECouponList;
import zy.entity.sell.ecoupon.T_Sell_Ecoupon_User;

public interface WxCouponDAO {
	/**
	 * 获取领券中心优惠券列表
	 * @param params
	 * @return
	 */
	List<T_Sell_ECouponList> getCouponCenterList(Map<String, Object> params);
	
	/**
	 * 根据优惠券编号查询优惠券信息
	 * @param ecl_code
	 * @param companyid
	 * @return
	 */
	T_Sell_ECouponList loadDetail(String ecl_code,Integer companyid);
	
	/**
	 * 查询优惠券已被领取的个数
	 * @param ecl_code
	 * @param companyid
	 * @return
	 */
	Integer countECouponReceived(String ecl_code,Integer companyid);
	
	/**
	 * 领取优惠券保存
	 * @param t_Sell_Ecoupon_User
	 */
	void save_ecoupon_user(T_Sell_Ecoupon_User t_Sell_Ecoupon_User);
}
