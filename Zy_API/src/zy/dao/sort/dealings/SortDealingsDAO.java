package zy.dao.sort.dealings;

import java.util.List;
import java.util.Map;

import zy.entity.sort.dealings.T_Sort_Dealings;

public interface SortDealingsDAO {
	Integer count(Map<String,Object> params);
	List<T_Sort_Dealings> list(Map<String,Object> params);
	void save(T_Sort_Dealings dealings);
}
