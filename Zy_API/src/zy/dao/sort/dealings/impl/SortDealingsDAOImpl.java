package zy.dao.sort.dealings.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.sort.dealings.SortDealingsDAO;
import zy.entity.sort.dealings.T_Sort_Dealings;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Repository
public class SortDealingsDAOImpl extends BaseDaoImpl implements SortDealingsDAO{
	
	@Override
	public Integer count(Map<String, Object> params) {
		Object begindate = params.get("begindate");
		Object enddate = params.get("enddate");
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT COUNT(1)");
		sql.append(" FROM t_sort_dealings t");
		sql.append(" WHERE 1=1");
		if (StringUtil.isNotEmpty(begindate)) {
			sql.append(" AND dl_date >= :begindate ");
		}
		if (StringUtil.isNotEmpty(enddate)) {
			sql.append(" AND dl_date <= :enddate ");
		}
		sql.append(" AND dl_shop_code = :dl_shop_code");
		sql.append(" AND t.companyid=:companyid");
		return namedParameterJdbcTemplate.queryForObject(sql.toString(), params, Integer.class);
	}

	@Override
	public List<T_Sort_Dealings> list(Map<String, Object> params) {
		Object sidx = params.get(CommonUtil.SIDX);
		Object sord = params.get(CommonUtil.SORD);
		Object begindate = params.get("begindate");
		Object enddate = params.get("enddate");
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT dl_id,dl_shop_code,dl_number,dl_type,dl_discount_money,dl_receivable,dl_received,dl_debt,dl_date,");
		sql.append(" dl_manager,dl_remark,dl_sysdate,dl_amount,companyid,");
		sql.append(" (SELECT sp_name FROM t_base_shop sp WHERE sp_code = dl_shop_code AND sp.companyid = t.companyid LIMIT 1) AS shop_name");
		sql.append(" FROM t_sort_dealings t");
		sql.append(" WHERE 1=1");
		if (StringUtil.isNotEmpty(begindate)) {
			sql.append(" AND dl_date >= :begindate ");
		}
		if (StringUtil.isNotEmpty(enddate)) {
			sql.append(" AND dl_date <= :enddate ");
		}
		sql.append(" AND dl_shop_code = :dl_shop_code");
		sql.append(" AND t.companyid=:companyid");
		if(sidx != null && !"".equals(sidx)){
			sql.append(" ORDER BY ").append(sidx).append(" ").append(sord);
		}else {
			sql.append(" ORDER BY dl_id ASC");
		}
		sql.append(" LIMIT :start,:end");
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Sort_Dealings.class));
	}
	
	@Override
	public void save(T_Sort_Dealings dealings) {
		StringBuffer sql = new StringBuffer("");
		sql.append("INSERT INTO t_sort_dealings");
		sql.append(" (dl_shop_code,dl_number,dl_type,dl_discount_money,dl_receivable,dl_received,dl_debt,dl_date,dl_manager,dl_remark,dl_sysdate,dl_amount,companyid)");
		sql.append(" VALUES(:dl_shop_code,:dl_number,:dl_type,:dl_discount_money,:dl_receivable,:dl_received,:dl_debt,:dl_date,:dl_manager,:dl_remark,:dl_sysdate,:dl_amount,:companyid)");
		KeyHolder holder = new GeneratedKeyHolder(); 
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(dealings),holder);
		dealings.setDl_id(holder.getKey().longValue());
	}

}
