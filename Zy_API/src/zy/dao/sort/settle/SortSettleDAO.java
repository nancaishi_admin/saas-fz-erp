package zy.dao.sort.settle;

import java.util.List;
import java.util.Map;

import zy.entity.sort.allot.T_Sort_Allot;
import zy.entity.sort.fee.T_Sort_Fee;
import zy.entity.sort.settle.T_Sort_Settle;
import zy.entity.sort.settle.T_Sort_SettleList;

public interface SortSettleDAO {
	Integer count(Map<String,Object> params);
	List<T_Sort_Settle> list(Map<String,Object> params);
	T_Sort_Settle load(Integer st_id);
	T_Sort_Settle load(String number,Integer companyid);
	T_Sort_Settle check(String number,Integer companyid);
	T_Sort_Settle check_settle(String sp_code, Integer companyid);
	List<T_Sort_SettleList> load_sort_forsavetemp(String sp_code,Integer companyid);
	List<T_Sort_SettleList> temp_list(Map<String, Object> params);
	List<T_Sort_SettleList> temp_list_forentire(Integer us_id,Integer companyid);
	List<T_Sort_SettleList> temp_list_forsave(Integer us_id,Integer companyid);
	void temp_save(List<T_Sort_SettleList> temps);
	void temp_update(List<T_Sort_SettleList> temps);
	void temp_clear(Integer us_id,Integer companyid);
	void temp_updateDiscountMoney(T_Sort_SettleList temp);
	void temp_updatePrepay(T_Sort_SettleList temp);
	void temp_updateRealMoney(T_Sort_SettleList temp);
	void temp_updateRemark(T_Sort_SettleList temp);
	void temp_updateJoin(String ids,String stl_join);
	List<T_Sort_SettleList> detail_list(String number,Integer companyid);
	void save(T_Sort_Settle settle, List<T_Sort_SettleList> details);
	void update(T_Sort_Settle settle, List<T_Sort_SettleList> details);
	void updateApprove(T_Sort_Settle settle);
	void updatePpNumber(T_Sort_Settle settle);
	List<T_Sort_Allot> listAllotBySettle(String number,Integer companyid);
	List<T_Sort_Allot> listAllotBySettle_Reverse(String number,Integer companyid);
	List<T_Sort_Fee> listFeeBySettle(String number,Integer companyid);
	List<T_Sort_Fee> listFeeBySettle_Reverse(String number,Integer companyid);
	void updateAllotBySettle(List<T_Sort_Allot> allots);
	void updateFeeBySettle(List<T_Sort_Fee> fees);
	void del(String st_number, Integer companyid);
	void deleteList(String st_number, Integer companyid);
	T_Sort_SettleList check_settle_bill(String number, Integer companyid);
}
