package zy.dao.stock.price;

import java.util.List;
import java.util.Map;

import zy.entity.stock.price.T_Stock_Price;
import zy.entity.stock.price.T_Stock_PriceList;

public interface StockPriceDAO {
	Integer count(Map<String,Object> params);
	List<T_Stock_Price> list(Map<String,Object> params);
	T_Stock_Price load(Integer pc_id);
	T_Stock_Price load(String number,Integer companyid);
	T_Stock_Price check(String number,Integer companyid);
	List<T_Stock_PriceList> detail_list(Map<String, Object> params);
	List<T_Stock_PriceList> detail_list_only(String pc_number,Integer companyid);
	List<T_Stock_PriceList> temp_list(Map<String, Object> params);
	List<T_Stock_PriceList> temp_list_forsave(Integer us_id,Integer companyid);
	List<String> temp_check(Integer us_id,Integer companyid);
	void temp_save(List<T_Stock_PriceList> temps);
	void temp_update(T_Stock_PriceList temp);
	void temp_del(Integer pcl_id);
	void temp_clear(Integer us_id,Integer companyid);
	void save(T_Stock_Price price, List<T_Stock_PriceList> details);
	void update(T_Stock_Price price, List<T_Stock_PriceList> details);
	void updateApprove(T_Stock_Price price);
	void approve_updateProductPrice(List<T_Stock_PriceList> details);
	void del(String pc_number, Integer companyid);
	void deleteList(String pc_number, Integer companyid);
}
