package zy.dao.stock.adjust;

import java.util.List;
import java.util.Map;

import zy.entity.base.product.T_Base_Product;
import zy.entity.stock.T_Stock_Import;
import zy.entity.stock.adjust.T_Stock_Adjust;
import zy.entity.stock.adjust.T_Stock_AdjustList;
import zy.entity.stock.data.T_Stock_DataBill;

public interface AdjustDAO {
	Integer count(Map<String,Object> params);
	List<T_Stock_Adjust> list(Map<String,Object> params);
	T_Stock_Adjust load(Integer aj_id);
	T_Stock_Adjust load(String number,Integer companyid);
	T_Stock_Adjust check(String number,Integer companyid);
	List<T_Stock_AdjustList> detail_list_forsavetemp(String aj_number,Integer companyid);
	List<T_Stock_AdjustList> detail_list(Map<String, Object> params);
	List<T_Stock_AdjustList> detail_list_print(Map<String, Object> params);
	List<T_Stock_AdjustList> detail_sum(Map<String, Object> params);
	List<String> detail_szgcode(Map<String,Object> params);
	List<T_Stock_AdjustList> temp_list(Map<String, Object> params);
	List<T_Stock_AdjustList> temp_list_forimport(Integer us_id,Integer companyid);
	List<T_Stock_AdjustList> temp_list_forsave(Integer us_id,Integer companyid);
	List<T_Stock_AdjustList> temp_sum(Map<String, Object> params);
	List<String> temp_szgcode(Map<String,Object> params);
	T_Stock_AdjustList temp_loadBySubCode(String sub_code, Integer us_id, Integer companyid);
	Integer count_product(Map<String, Object> param);
	List<T_Base_Product> list_product(Map<String, Object> param);
	T_Base_Product load_product(String pd_code,Integer companyid);
	Map<String, Object> load_product_size(Map<String,Object> params);
	Double temp_queryUnitPrice(String pd_code, Integer us_id, Integer companyid);
	void temp_save(List<T_Stock_AdjustList> temps);
	void temp_save(T_Stock_AdjustList temp);
	void temp_update(List<T_Stock_AdjustList> temps);
	void temp_updateById(List<T_Stock_AdjustList> temps);
	void temp_update(T_Stock_AdjustList temp);
	void temp_updateRemarkById(T_Stock_AdjustList temp);
	void temp_updateRemarkByPdCode(T_Stock_AdjustList temp);
	void temp_del(List<T_Stock_AdjustList> temps);
	void temp_del(Integer ajl_id);
	void temp_delByPiCode(T_Stock_AdjustList temp);
	void temp_clear(Integer us_id,Integer companyid);
	List<T_Stock_Import> temp_listByImport(List<String> barCodes,Integer companyid);
	void save(T_Stock_Adjust adjust,List<T_Stock_AdjustList> details);
	void update(T_Stock_Adjust adjust,List<T_Stock_AdjustList> details);
	void updateApprove(T_Stock_Adjust adjust);
	List<T_Stock_DataBill> listStock(String number,String dp_code, Integer companyid);
	void del(String aj_number, Integer companyid);
	void deleteList(String aj_number, Integer companyid);
}
