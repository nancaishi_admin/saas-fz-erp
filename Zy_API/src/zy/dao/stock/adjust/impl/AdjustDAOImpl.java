package zy.dao.stock.adjust.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.stock.adjust.AdjustDAO;
import zy.dto.common.ProductDto;
import zy.entity.base.product.T_Base_Product;
import zy.entity.base.size.T_Base_Size;
import zy.entity.stock.T_Stock_Import;
import zy.entity.stock.adjust.T_Stock_Adjust;
import zy.entity.stock.adjust.T_Stock_AdjustList;
import zy.entity.stock.data.T_Stock_DataBill;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;

@Repository
public class AdjustDAOImpl extends BaseDaoImpl implements AdjustDAO{

	@Override
	public Integer count(Map<String, Object> params) {
		Object aj_isdraft = params.get("aj_isdraft");
		Object aj_ar_state = params.get("aj_ar_state");
		Object begindate = params.get("begindate");
		Object enddate = params.get("enddate");
		Object aj_dp_code = params.get("aj_dp_code");
		Object aj_manager = params.get("aj_manager");
		Object aj_number = params.get("aj_number");
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT COUNT(1)");
		sql.append(" FROM t_stock_adjust t");
		sql.append(" WHERE 1=1");
		if (StringUtil.isNotEmpty(aj_isdraft)) {
			sql.append(" AND aj_isdraft = :aj_isdraft ");
		}
		if (StringUtil.isNotEmpty(aj_ar_state)) {
			sql.append(" AND aj_ar_state = :aj_ar_state ");
		}
		if (StringUtil.isNotEmpty(begindate)) {
			sql.append(" AND aj_date >= :begindate ");
		}
		if (StringUtil.isNotEmpty(enddate)) {
			sql.append(" AND aj_date <= :enddate ");
		}
		if (StringUtil.isNotEmpty(aj_dp_code)) {
			sql.append(" AND aj_dp_code = :aj_dp_code ");
		}
		if (StringUtil.isNotEmpty(aj_manager)) {
			sql.append(" AND aj_manager = :aj_manager ");
		}
		if (StringUtil.isNotEmpty(aj_number)) {
			sql.append(" AND INSTR(aj_number,:aj_number) > 0 ");
		}
		sql.append(" AND t.companyid=:companyid");
		return namedParameterJdbcTemplate.queryForObject(sql.toString(), params, Integer.class);
	}

	@Override
	public List<T_Stock_Adjust> list(Map<String, Object> params) {
		Object sidx = params.get(CommonUtil.SIDX);
		Object sord = params.get(CommonUtil.SORD);
		Object aj_isdraft = params.get("aj_isdraft");
		Object aj_ar_state = params.get("aj_ar_state");
		Object begindate = params.get("begindate");
		Object enddate = params.get("enddate");
		Object aj_dp_code = params.get("aj_dp_code");
		Object aj_manager = params.get("aj_manager");
		Object aj_number = params.get("aj_number");
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT aj_id,aj_number,aj_date,aj_dp_code,aj_manager,aj_amount,aj_money,aj_remark,aj_ar_state,aj_ar_date,aj_isdraft,");
		sql.append(" aj_sysdate,aj_us_id,companyid,");
		sql.append(" (SELECT dp_name FROM t_base_depot dp WHERE dp_code = aj_dp_code AND dp.companyid = t.companyid LIMIT 1) AS depot_name");
		sql.append(" FROM t_stock_adjust t");
		sql.append(" WHERE 1=1");
		if (StringUtil.isNotEmpty(aj_isdraft)) {
			sql.append(" AND aj_isdraft = :aj_isdraft ");
		}
		if (StringUtil.isNotEmpty(aj_ar_state)) {
			sql.append(" AND aj_ar_state = :aj_ar_state ");
		}
		if (StringUtil.isNotEmpty(begindate)) {
			sql.append(" AND aj_date >= :begindate ");
		}
		if (StringUtil.isNotEmpty(enddate)) {
			sql.append(" AND aj_date <= :enddate ");
		}
		if (StringUtil.isNotEmpty(aj_dp_code)) {
			sql.append(" AND aj_dp_code = :aj_dp_code ");
		}
		if (StringUtil.isNotEmpty(aj_manager)) {
			sql.append(" AND aj_manager = :aj_manager ");
		}
		if (StringUtil.isNotEmpty(aj_number)) {
			sql.append(" AND INSTR(aj_number,:aj_number) > 0 ");
		}
		sql.append(" AND t.companyid=:companyid");
		if(sidx != null && !"".equals(sidx)){
			sql.append(" ORDER BY ").append(sidx).append(" ").append(sord);
		}else {
			sql.append(" ORDER BY aj_id DESC");
		}
		sql.append(" LIMIT :start,:end");
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Stock_Adjust.class));
	}

	@Override
	public T_Stock_Adjust load(Integer aj_id) {
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT aj_id,aj_number,aj_date,aj_dp_code,aj_manager,aj_amount,aj_money,aj_remark,aj_ar_state,aj_ar_date,aj_isdraft,");
		sql.append(" aj_sysdate,aj_us_id,companyid,");
		sql.append(" (SELECT dp_name FROM t_base_depot dp WHERE dp_code = aj_dp_code AND dp.companyid = t.companyid LIMIT 1) AS depot_name");
		sql.append(" FROM t_stock_adjust t");
		sql.append(" WHERE aj_id = :aj_id");
		sql.append(" LIMIT 1");
		try {
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), new MapSqlParameterSource().addValue("aj_id", aj_id),
					new BeanPropertyRowMapper<>(T_Stock_Adjust.class));
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public T_Stock_Adjust load(String number,Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT aj_id,aj_number,aj_date,aj_dp_code,aj_manager,aj_amount,aj_money,aj_remark,aj_ar_state,aj_ar_date,aj_isdraft,");
		sql.append(" aj_sysdate,aj_us_id,companyid,");
		sql.append(" (SELECT dp_name FROM t_base_depot dp WHERE dp_code = aj_dp_code AND dp.companyid = t.companyid LIMIT 1) AS depot_name");
		sql.append(" FROM t_stock_adjust t");
		sql.append(" WHERE aj_number = :aj_number");
		sql.append(" AND companyid = :companyid");
		sql.append(" LIMIT 1");
		try {
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new MapSqlParameterSource().addValue("aj_number", number).addValue("companyid", companyid),
					new BeanPropertyRowMapper<>(T_Stock_Adjust.class));
		} catch (Exception e) {
			return null;
		}
	}
	
	
	@Override
	public T_Stock_Adjust check(String number,Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT aj_id,aj_number,aj_dp_code,aj_ar_state,aj_remark,companyid");
		sql.append(" FROM t_stock_adjust t");
		sql.append(" WHERE aj_number = :aj_number");
		sql.append(" AND companyid = :companyid");
		sql.append(" LIMIT 1");
		try {
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new MapSqlParameterSource().addValue("aj_number", number).addValue("companyid", companyid),
					new BeanPropertyRowMapper<>(T_Stock_Adjust.class));
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public List<T_Stock_AdjustList> detail_list_forsavetemp(String aj_number,Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT ajl_id,ajl_pd_code,ajl_sub_code,ajl_szg_code,ajl_sz_code,ajl_cr_code,ajl_br_code,ajl_amount,ajl_unitprice,ajl_remark,companyid");
		sql.append(" FROM t_stock_adjustlist t");
		sql.append(" WHERE 1=1");
		sql.append(" AND ajl_number = :ajl_number");
		sql.append(" AND t.companyid = :companyid");
		sql.append(" ORDER BY ajl_id ASC");
		return namedParameterJdbcTemplate.query(sql.toString(),
				new MapSqlParameterSource().addValue("ajl_number", aj_number).addValue("companyid", companyid),
				new BeanPropertyRowMapper<>(T_Stock_AdjustList.class));
	}
	
	@Override
	public List<T_Stock_AdjustList> detail_list(Map<String, Object> params) {
		Object sidx = params.get(CommonUtil.SIDX);
		Object sord = params.get(CommonUtil.SORD);
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT ajl_id,ajl_number,ajl_pd_code,ajl_sub_code,ajl_szg_code,ajl_sz_code,ajl_cr_code,ajl_br_code,ajl_amount,ajl_unitprice,ajl_remark,");
		sql.append(" t.companyid,pd_no,pd_name,pd_unit,");
		sql.append(" (SELECT cr_name FROM t_base_color cr WHERE cr_code = ajl_cr_code AND cr.companyid = t.companyid LIMIT 1) AS cr_name,");
		sql.append(" (SELECT sz_name FROM t_base_size sz WHERE sz_code = ajl_sz_code AND sz.companyid = t.companyid LIMIT 1) AS sz_name,");
		sql.append(" (SELECT br_name FROM t_base_bra br WHERE br_code = ajl_br_code AND br.companyid = t.companyid LIMIT 1) AS br_name");
		sql.append(" FROM t_stock_adjustlist t");
		sql.append(" JOIN t_base_product pd ON pd_code = t.ajl_pd_code AND pd.companyid = t.companyid");
		sql.append(" WHERE 1=1");
		sql.append(" AND ajl_number = :ajl_number");
		sql.append(" AND t.companyid = :companyid");
		if(StringUtil.isNotEmpty(sidx)){
			sql.append(" ORDER BY ").append(sidx).append(" ").append(sord);
		}else {
			sql.append(" ORDER BY ajl_id DESC");
		}
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Stock_AdjustList.class));
	}
	
	@Override
	public List<T_Stock_AdjustList> detail_list_print(Map<String, Object> params) {
		Object sidx = params.get(CommonUtil.SIDX);
		Object sord = params.get(CommonUtil.SORD);
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT ajl_id,ajl_number,ajl_pd_code,ajl_sub_code,ajl_szg_code,ajl_sz_code,ajl_cr_code,ajl_br_code,ajl_amount,ajl_unitprice,ajl_remark,");
		sql.append(" t.companyid,pd_no,pd_name,pd_unit,pd_season,pd_year,");
		sql.append(" (SELECT cr_name FROM t_base_color cr WHERE cr_code = ajl_cr_code AND cr.companyid = t.companyid LIMIT 1) AS cr_name,");
		sql.append(" (SELECT sz_name FROM t_base_size sz WHERE sz_code = ajl_sz_code AND sz.companyid = t.companyid LIMIT 1) AS sz_name,");
		sql.append(" (SELECT br_name FROM t_base_bra br WHERE br_code = ajl_br_code AND br.companyid = t.companyid LIMIT 1) AS br_name,");
		sql.append(" (SELECT bd_name FROM t_base_brand bd WHERE bd_code = pd_bd_code AND bd.companyid = t.companyid LIMIT 1) AS bd_name,");
		sql.append(" (SELECT tp_name FROM t_base_type tp WHERE tp_code = pd_tp_code AND tp.companyid = t.companyid LIMIT 1) AS tp_name");
		sql.append(" FROM t_stock_adjustlist t");
		sql.append(" JOIN t_base_product pd ON pd_code = t.ajl_pd_code AND pd.companyid = t.companyid");
		sql.append(" WHERE 1=1");
		sql.append(" AND ajl_number = :ajl_number");
		sql.append(" AND t.companyid = :companyid");
		if(StringUtil.isNotEmpty(sidx)){
			sql.append(" ORDER BY ").append(sidx).append(" ").append(sord);
		}else {
			sql.append(" ORDER BY ajl_id DESC");
		}
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Stock_AdjustList.class));
	}
	
	@Override
	public List<T_Stock_AdjustList> detail_sum(Map<String, Object> params) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT ajl_id,ajl_number,ajl_pd_code,ajl_szg_code,SUM(ABS(ajl_amount)) AS ajl_amount,ajl_unitprice,ajl_remark,");
		sql.append(" t.companyid,pd_no,pd_name,pd_unit,pd_season,pd_year,");
		sql.append(" (SELECT bd_name FROM t_base_brand bd WHERE bd_code = pd_bd_code AND bd.companyid = t.companyid LIMIT 1) AS bd_name,");
		sql.append(" (SELECT tp_name FROM t_base_type tp WHERE tp_code = pd_tp_code AND tp.companyid = t.companyid LIMIT 1) AS tp_name");
		sql.append(" FROM t_stock_adjustlist t");
		sql.append(" JOIN t_base_product pd ON pd_code = t.ajl_pd_code AND pd.companyid = t.companyid");
		sql.append(" WHERE 1=1");
		sql.append(" AND ajl_number = :ajl_number");
		sql.append(" AND t.companyid = :companyid");
		sql.append(" GROUP BY ajl_pd_code");
		sql.append(" ORDER BY ajl_pd_code ASC");
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Stock_AdjustList.class));
	}
	
	@Override
	public List<String> detail_szgcode(Map<String,Object> params) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT DISTINCT ajl_szg_code");
		sql.append(" FROM t_stock_adjustlist t ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND ajl_number = :ajl_number");
		sql.append(" AND t.companyid = :companyid");
		return namedParameterJdbcTemplate.queryForList(sql.toString(), params, String.class);
	}
	
	@Override
	public List<T_Stock_AdjustList> temp_list(Map<String, Object> params) {
		Object sidx = params.get(CommonUtil.SIDX);
		Object sord = params.get(CommonUtil.SORD);
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT ajl_id,ajl_pd_code,ajl_sub_code,ajl_szg_code,ajl_sz_code,ajl_cr_code,ajl_br_code,ajl_amount,ajl_unitprice,ajl_remark,");
		sql.append(" ajl_us_id,t.companyid,pd_no,pd_name,pd_unit,");
		sql.append(" (SELECT cr_name FROM t_base_color cr WHERE cr_code = ajl_cr_code AND cr.companyid = t.companyid LIMIT 1) AS cr_name,");
		sql.append(" (SELECT sz_name FROM t_base_size sz WHERE sz_code = ajl_sz_code AND sz.companyid = t.companyid LIMIT 1) AS sz_name,");
		sql.append(" (SELECT br_name FROM t_base_bra br WHERE br_code = ajl_br_code AND br.companyid = t.companyid LIMIT 1) AS br_name");
		sql.append(" FROM t_stock_adjustlist_temp t");
		sql.append(" JOIN t_base_product pd ON pd_code = t.ajl_pd_code AND pd.companyid = t.companyid");
		sql.append(" WHERE 1=1");
		sql.append(" AND ajl_us_id = :ajl_us_id");
		sql.append(" AND t.companyid = :companyid");
		if(StringUtil.isNotEmpty(sidx)){
			sql.append(" ORDER BY ").append(sidx).append(" ").append(sord);
		}else {
			sql.append(" ORDER BY ajl_id DESC");
		}
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Stock_AdjustList.class));
	}

	@Override
	public List<T_Stock_AdjustList> temp_list_forimport(Integer us_id,Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT ajl_id,ajl_pd_code,ajl_sub_code,ajl_szg_code,ajl_sz_code,ajl_cr_code,ajl_br_code,ajl_amount,ajl_unitprice,ajl_remark,");
		sql.append(" ajl_us_id,t.companyid");
		sql.append(" FROM t_stock_adjustlist_temp t");
		sql.append(" WHERE 1=1");
		sql.append(" AND ajl_us_id = :ajl_us_id");
		sql.append(" AND t.companyid = :companyid");
		return namedParameterJdbcTemplate.query(sql.toString(),
				new MapSqlParameterSource().addValue("ajl_us_id", us_id).addValue("companyid", companyid),
				new BeanPropertyRowMapper<>(T_Stock_AdjustList.class));
	}
	
	@Override
	public List<T_Stock_AdjustList> temp_list_forsave(Integer us_id,Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT ajl_id,ajl_pd_code,ajl_sub_code,ajl_szg_code,ajl_sz_code,ajl_cr_code,ajl_br_code,ajl_amount,ajl_unitprice,ajl_remark,");
		sql.append(" ajl_us_id,t.companyid");
		sql.append(" FROM t_stock_adjustlist_temp t");
		sql.append(" WHERE 1=1");
		sql.append(" AND ajl_us_id = :ajl_us_id");
		sql.append(" AND t.companyid = :companyid");
		sql.append(" ORDER BY ajl_id ASC");
		return namedParameterJdbcTemplate.query(sql.toString(),
				new MapSqlParameterSource().addValue("ajl_us_id", us_id).addValue("companyid", companyid),
				new BeanPropertyRowMapper<>(T_Stock_AdjustList.class));
	}
	
	@Override
	public List<T_Stock_AdjustList> temp_sum(Map<String, Object> params) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT ajl_id,ajl_pd_code,ajl_szg_code,SUM(ajl_amount) AS ajl_amount,ajl_unitprice,ajl_remark,");
		sql.append(" ajl_us_id,t.companyid,pd_no,pd_name,pd_unit,");
		sql.append(" (SELECT bd_name FROM t_base_brand bd WHERE bd_code = pd_bd_code AND bd.companyid = t.companyid LIMIT 1) AS bd_name,");
		sql.append(" (SELECT tp_name FROM t_base_type tp WHERE tp_code = pd_tp_code AND tp.companyid = t.companyid LIMIT 1) AS tp_name");
		sql.append(" FROM t_stock_adjustlist_temp t");
		sql.append(" JOIN t_base_product pd ON pd_code = t.ajl_pd_code AND pd.companyid = t.companyid");
		sql.append(" WHERE 1=1");
		sql.append(" AND ajl_us_id = :ajl_us_id");
		sql.append(" AND t.companyid = :companyid");
		sql.append(" GROUP BY ajl_pd_code");
		sql.append(" ORDER BY ajl_pd_code ASC");
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Stock_AdjustList.class));
	}

	@Override
	public List<String> temp_szgcode(Map<String, Object> params) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT DISTINCT ajl_szg_code");
		sql.append(" FROM t_stock_adjustlist_temp t ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND ajl_us_id = :ajl_us_id");
		sql.append(" AND t.companyid = :companyid");
		return namedParameterJdbcTemplate.queryForList(sql.toString(), params, String.class);
	}
	
	@Override
	public T_Stock_AdjustList temp_loadBySubCode(String sub_code, Integer us_id, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT ajl_id,ajl_pd_code,ajl_sub_code,ajl_szg_code,ajl_sz_code,ajl_cr_code,ajl_br_code,ajl_amount,ajl_unitprice,ajl_remark,");
		sql.append(" ajl_us_id,companyid");
		sql.append(" FROM t_stock_adjustlist_temp t");
		sql.append(" WHERE 1=1");
		sql.append(" AND ajl_sub_code = :ajl_sub_code");
		sql.append(" AND ajl_us_id = :ajl_us_id");
		sql.append(" AND companyid = :companyid");
		sql.append(" LIMIT 1");
		try{
			return namedParameterJdbcTemplate.queryForObject(sql.toString(),
					new MapSqlParameterSource().addValue("ajl_sub_code", sub_code).addValue("ajl_us_id", us_id).addValue("companyid", companyid),
					new BeanPropertyRowMapper<>(T_Stock_AdjustList.class));
		}catch(Exception e){
			return null;
		}
	}
	
	@Override
	public Integer count_product(Map<String, Object> param) {
		Object searchContent = param.get("searchContent");
		Object alreadyExist = param.get("alreadyExist");
		Object exactQuery = param.get("exactQuery");
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT count(1)");
		sql.append(" FROM (");
		sql.append(" SELECT 1 FROM t_base_product t");
		sql.append(" LEFT JOIN t_stock_adjustlist_temp ajl ON ajl_pd_code = pd_code AND ajl.companyid = t.companyid AND ajl_us_id = :us_id");
		sql.append(" where 1 = 1");
		if(StringUtil.isNotEmpty(searchContent)){
			if("1".equals(exactQuery)){
				sql.append(" AND (t.pd_name = :searchContent OR t.pd_no = :searchContent)");
			}else {
				sql.append(" AND (instr(t.pd_name,:searchContent)>0 OR instr(t.pd_spell,:searchContent)>0 OR instr(t.pd_no,:searchContent)>0)");
			}
        }
		if("1".equals(alreadyExist)){
			sql.append(" AND ajl_id IS NOT NULL ");
		}
		sql.append(" and t.companyid=:companyid");
		sql.append(" GROUP BY pd_code)t");
		return namedParameterJdbcTemplate.queryForObject(sql.toString(), param, Integer.class);
	}

	@Override
	public List<T_Base_Product> list_product(Map<String, Object> param) {
		Object sidx = param.get(CommonUtil.SIDX);
		Object sord = param.get(CommonUtil.SORD);
		Object searchContent = param.get("searchContent");
		Object alreadyExist = param.get("alreadyExist");
		Object exactQuery = param.get("exactQuery");
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT pd_id,pd_code,pd_no,pd_name,IF(ajl_id IS NULL,0,1) AS exist");
		sql.append(" FROM t_base_product t");
		sql.append(" LEFT JOIN t_stock_adjustlist_temp ajl ON ajl_pd_code = pd_code AND ajl.companyid = t.companyid AND ajl_us_id = :us_id");
		sql.append(" where 1 = 1");
		if(StringUtil.isNotEmpty(searchContent)){
			if("1".equals(exactQuery)){
				sql.append(" AND (t.pd_name = :searchContent OR t.pd_no = :searchContent)");
			}else {
				sql.append(" AND (instr(t.pd_name,:searchContent)>0 OR instr(t.pd_spell,:searchContent)>0 OR instr(t.pd_no,:searchContent)>0)");
			}
        }
		if("1".equals(alreadyExist)){
			sql.append(" AND ajl_id IS NOT NULL ");
		}
		sql.append(" and t.companyid=:companyid");
		sql.append(" GROUP BY pd_code");
		if(StringUtil.isNotEmpty(sidx)){
			sql.append(" ORDER BY ").append(sidx).append(" ").append(sord);
		}else {
			sql.append(" ORDER BY pd_id DESC");
		}
		sql.append(" LIMIT :start,:end");
		return namedParameterJdbcTemplate.query(sql.toString(), param, new BeanPropertyRowMapper<>(T_Base_Product.class));
	}
	
	@Override
	public T_Base_Product load_product(String pd_code,Integer companyid){
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT pd.pd_id,pd.pd_code,pd_no,pd_name,pd_szg_code,pd_unit,pd_year,pd_season,pd_sell_price,pd_buy_price,pd_cost_price,");
		sql.append(" (SELECT bd_name FROM t_base_brand bd WHERE bd_code = pd_bd_code AND bd.companyid = pd.companyid LIMIT 1) AS pd_bd_name,");
		sql.append(" (SELECT tp_name FROM t_base_type tp WHERE tp_code = pd_tp_code AND tp.companyid = pd.companyid LIMIT 1) AS pd_tp_name");
		sql.append(" ,(SELECT pdm_img_path FROM t_base_product_img pdm WHERE pdm_pd_code = pd.pd_code AND pdm.companyid = pd.companyid LIMIT 1) AS pdm_img_path");
		sql.append(" FROM t_base_product pd");
		sql.append(" WHERE 1=1");
		sql.append(" AND pd.pd_code = :pd_code");
		sql.append(" AND pd.companyid = :companyid");
		try{
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new MapSqlParameterSource().addValue("pd_code", pd_code).addValue("companyid", companyid),
					new BeanPropertyRowMapper<>(T_Base_Product.class));
		}catch(Exception e){
			return null;
		}
	}
	
	@Override
	public Map<String, Object> load_product_size(Map<String,Object> params) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		//1.查询尺码信息
		List<T_Base_Size> sizes = namedParameterJdbcTemplate.query(getSizeSQL(), params, new BeanPropertyRowMapper<>(T_Base_Size.class));
		resultMap.put("sizes",sizes);
		//2.获取颜色杯型信息
		List<ProductDto> inputs = namedParameterJdbcTemplate.query(getColorBraSQL(), params, new BeanPropertyRowMapper<>(ProductDto.class));
		resultMap.put("inputs",inputs);
		//3.已录入数量
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT ajl_sz_code AS sz_code,ajl_cr_code AS cr_code,ajl_br_code AS br_code,ajl_amount AS amount");
		sql.append(" FROM t_stock_adjustlist_temp");
		sql.append(" WHERE 1=1");
		sql.append(" AND ajl_pd_code = :pd_code");
		sql.append(" AND ajl_us_id = :us_id");
		sql.append(" AND companyid = :companyid");
		List<ProductDto> temps = namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(ProductDto.class));
		resultMap.put("temps",temps);
		//4.库存数量
		List<ProductDto> stocks = namedParameterJdbcTemplate.query(getStockSQL(), params, new BeanPropertyRowMapper<>(ProductDto.class));
		resultMap.put("stocks",stocks);
		return resultMap;
	}
	
	@Override
	public Double temp_queryUnitPrice(String pd_code, Integer us_id, Integer companyid){
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT ajl_unitprice");
		sql.append(" FROM t_stock_adjustlist_temp");
		sql.append(" WHERE 1=1");
		sql.append(" AND ajl_pd_code = :ajl_pd_code");
		sql.append(" AND ajl_us_id = :ajl_us_id");
		sql.append(" AND companyid = :companyid");
		sql.append(" LIMIT 1");
		try {
			return namedParameterJdbcTemplate.queryForObject(sql.toString(),
					new MapSqlParameterSource().addValue("ajl_pd_code", pd_code)
							.addValue("ajl_us_id", us_id)
							.addValue("companyid", companyid), Double.class);
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public void temp_save(List<T_Stock_AdjustList> temps) {
		StringBuffer sql = new StringBuffer("");
		sql.append("INSERT INTO t_stock_adjustlist_temp");
		sql.append(" (ajl_pd_code,ajl_sub_code,ajl_sz_code,ajl_szg_code,ajl_cr_code,ajl_br_code,ajl_amount,");
		sql.append(" ajl_unitprice,ajl_remark,ajl_us_id,companyid)");
		sql.append(" VALUES");
		sql.append(" (:ajl_pd_code,:ajl_sub_code,:ajl_sz_code,:ajl_szg_code,:ajl_cr_code,:ajl_br_code,:ajl_amount,");
		sql.append(" :ajl_unitprice,:ajl_remark,:ajl_us_id,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(temps.toArray()));
	}
	
	@Override
	public void temp_save(T_Stock_AdjustList temp) {
		StringBuffer sql = new StringBuffer("");
		sql.append("INSERT INTO t_stock_adjustlist_temp");
		sql.append(" (ajl_pd_code,ajl_sub_code,ajl_sz_code,ajl_szg_code,ajl_cr_code,ajl_br_code,ajl_amount,");
		sql.append(" ajl_unitprice,ajl_remark,ajl_us_id,companyid)");
		sql.append(" VALUES");
		sql.append(" (:ajl_pd_code,:ajl_sub_code,:ajl_sz_code,:ajl_szg_code,:ajl_cr_code,:ajl_br_code,:ajl_amount,");
		sql.append(" :ajl_unitprice,:ajl_remark,:ajl_us_id,:companyid)");
		KeyHolder holder = new GeneratedKeyHolder(); 
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(temp),holder);
		temp.setAjl_id(holder.getKey().intValue());
	}
	
	@Override
	public void temp_update(List<T_Stock_AdjustList> temps) {
		StringBuffer sql = new StringBuffer("");
		sql.append("UPDATE t_stock_adjustlist_temp");
		sql.append(" SET ajl_amount = :ajl_amount");
		sql.append(" WHERE 1=1");
		sql.append(" AND ajl_sub_code = :ajl_sub_code");
		sql.append(" AND ajl_us_id = :ajl_us_id");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(temps.toArray()));
	}
	
	@Override
	public void temp_updateById(List<T_Stock_AdjustList> temps) {
		StringBuffer sql = new StringBuffer("");
		sql.append("UPDATE t_stock_adjustlist_temp");
		sql.append(" SET ajl_amount = :ajl_amount");
		sql.append(" WHERE 1=1");
		sql.append(" AND ajl_id = :ajl_id");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(temps.toArray()));
	}
	
	@Override
	public void temp_update(T_Stock_AdjustList temp) {
		StringBuffer sql = new StringBuffer("");
		sql.append("UPDATE t_stock_adjustlist_temp");
		sql.append(" SET ajl_amount = :ajl_amount");
		sql.append(" WHERE 1=1");
		sql.append(" AND ajl_id = :ajl_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(temp));
	}
	
	@Override
	public void temp_updateRemarkById(T_Stock_AdjustList temp) {
		StringBuffer sql = new StringBuffer("");
		sql.append("UPDATE t_stock_adjustlist_temp");
		sql.append(" SET ajl_remark = :ajl_remark");
		sql.append(" WHERE 1=1");
		sql.append(" AND ajl_id = :ajl_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(temp));
	}
	
	@Override
	public void temp_updateRemarkByPdCode(T_Stock_AdjustList temp) {
		StringBuffer sql = new StringBuffer("");
		sql.append("UPDATE t_stock_adjustlist_temp");
		sql.append(" SET ajl_remark = :ajl_remark");
		sql.append(" WHERE 1=1");
		sql.append(" AND ajl_pd_code = :ajl_pd_code");
		sql.append(" AND ajl_us_id = :ajl_us_id");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(temp));
	}
	
	@Override
	public void temp_del(List<T_Stock_AdjustList> temps) {
		StringBuffer sql = new StringBuffer("");
		sql.append("DELETE FROM t_stock_adjustlist_temp");
		sql.append(" WHERE 1=1");
		sql.append(" AND ajl_sub_code = :ajl_sub_code");
		sql.append(" AND ajl_us_id = :ajl_us_id");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(temps.toArray()));
	}

	@Override
	public void temp_del(Integer ajl_id) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" DELETE FROM t_stock_adjustlist_temp");
		sql.append(" WHERE ajl_id=:ajl_id");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("ajl_id", ajl_id));
	}
	
	@Override
	public void temp_delByPiCode(T_Stock_AdjustList temp) {
		StringBuffer sql = new StringBuffer("");
		sql.append("DELETE FROM t_stock_adjustlist_temp");
		sql.append(" WHERE 1=1");
		sql.append(" AND ajl_pd_code = :ajl_pd_code");
		if(StringUtil.isNotEmpty(temp.getAjl_cr_code())){
			sql.append(" AND ajl_cr_code = :ajl_cr_code");
		}
		if(StringUtil.isNotEmpty(temp.getAjl_br_code())){
			sql.append(" AND ajl_br_code = :ajl_br_code");
		}
		sql.append(" AND ajl_us_id = :ajl_us_id");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(temp));
	}
	
	@Override
	public void temp_clear(Integer us_id,Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append("DELETE FROM t_stock_adjustlist_temp");
		sql.append(" WHERE 1=1");
		sql.append(" AND ajl_us_id = :ajl_us_id");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(),
				new MapSqlParameterSource().addValue("ajl_us_id", us_id).addValue("companyid", companyid));
	}
	
	@Override
	public List<T_Stock_Import> temp_listByImport(List<String> barCodes,Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT DISTINCT bc_pd_code,bc_subcode,bc_barcode,bc_size,pd_szg_code,bc_color,bc_bra,pd_cost_price AS unit_price");
		sql.append(" FROM t_base_barcode bc");
		sql.append(" JOIN t_base_product pd ON pd.pd_code = bc_pd_code AND pd.companyid = bc.companyid");
		sql.append(" WHERE 1=1");
		sql.append(" AND bc_barcode IN(:barcode)");
		sql.append(" AND bc.companyid = :companyid");
		return namedParameterJdbcTemplate.query(sql.toString(),
				new MapSqlParameterSource().addValue("barcode", barCodes).addValue("companyid", companyid),
				new BeanPropertyRowMapper<>(T_Stock_Import.class));
	}
	
	@Override
	public void save(T_Stock_Adjust adjust, List<T_Stock_AdjustList> details) {
		String prefix = CommonUtil.NUMBER_PREFIX_STOCK_ADJUST + DateUtil.getYearMonthDateYYYYMMDD();
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT CONCAT(:prefix,f_addnumber(MAX(aj_number))) AS new_number");
		sql.append(" FROM t_stock_adjust");
		sql.append(" WHERE 1=1");
		sql.append(" AND INSTR(aj_number,:prefix) > 0");
		sql.append(" AND companyid = :companyid");
		String new_number = namedParameterJdbcTemplate.queryForObject(sql.toString(), 
				new MapSqlParameterSource().addValue("prefix", prefix).addValue("companyid", adjust.getCompanyid()), String.class);
		adjust.setAj_number(new_number);
		sql.setLength(0);
		sql.append(" INSERT INTO t_stock_adjust");
		sql.append(" (aj_number,aj_date,aj_dp_code,aj_manager,aj_amount,aj_money,");
		sql.append(" aj_remark,aj_ar_state,aj_isdraft,aj_sysdate,aj_us_id,companyid)");
		sql.append(" VALUES");
		sql.append(" (:aj_number,:aj_date,:aj_dp_code,:aj_manager,:aj_amount,:aj_money,");
		sql.append(" :aj_remark,:aj_ar_state,:aj_isdraft,:aj_sysdate,:aj_us_id,:companyid)");
		KeyHolder holder = new GeneratedKeyHolder(); 
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(adjust),holder);
		adjust.setAj_id(holder.getKey().intValue());
		for(T_Stock_AdjustList item:details){
			item.setAjl_number(adjust.getAj_number());
		}
		sql.setLength(0);
		sql.append(" INSERT INTO t_stock_adjustlist");
		sql.append(" (ajl_number,ajl_pd_code,ajl_sub_code,ajl_szg_code,ajl_sz_code,ajl_cr_code,ajl_br_code,ajl_amount,ajl_unitprice,ajl_remark,companyid)");
		sql.append(" VALUES");
		sql.append(" (:ajl_number,:ajl_pd_code,:ajl_sub_code,:ajl_szg_code,:ajl_sz_code,:ajl_cr_code,:ajl_br_code,:ajl_amount,:ajl_unitprice,:ajl_remark,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(details.toArray()));
	}
	
	@Override
	public void update(T_Stock_Adjust adjust, List<T_Stock_AdjustList> details) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" UPDATE t_stock_adjust");
		sql.append(" SET aj_date=:aj_date");
		sql.append(" ,aj_dp_code=:aj_dp_code");
		sql.append(" ,aj_manager=:aj_manager");
		sql.append(" ,aj_amount=:aj_amount");
		sql.append(" ,aj_money=:aj_money");
		sql.append(" ,aj_remark=:aj_remark");
		sql.append(" ,aj_ar_state=:aj_ar_state");
		sql.append(" ,aj_us_id=:aj_us_id");
		sql.append(" WHERE aj_id=:aj_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(adjust));
		for(T_Stock_AdjustList item:details){
			item.setAjl_number(adjust.getAj_number());
		}
		sql.setLength(0);
		sql.append(" INSERT INTO t_stock_adjustlist");
		sql.append(" (ajl_number,ajl_pd_code,ajl_sub_code,ajl_szg_code,ajl_sz_code,ajl_cr_code,ajl_br_code,ajl_amount,ajl_unitprice,ajl_remark,companyid)");
		sql.append(" VALUES");
		sql.append(" (:ajl_number,:ajl_pd_code,:ajl_sub_code,:ajl_szg_code,:ajl_sz_code,:ajl_cr_code,:ajl_br_code,:ajl_amount,:ajl_unitprice,:ajl_remark,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(details.toArray()));
	}
	
	@Override
	public void updateApprove(T_Stock_Adjust adjust) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" UPDATE t_stock_adjust");
		sql.append(" SET aj_ar_state=:aj_ar_state");
		sql.append(" ,aj_ar_date = :aj_ar_date");
		sql.append(" WHERE aj_id=:aj_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(adjust));
	}
	
	@Override
	public List<T_Stock_DataBill> listStock(String number,String dp_code, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT ajl_pd_code AS sd_pd_code,ajl_sub_code AS sd_code,ajl_cr_code AS sd_cr_code,ajl_sz_code AS sd_sz_code,");
		sql.append(" ajl_br_code AS sd_br_code,SUM(ajl_amount) AS bill_amount,sd_id,sd_amount");
		sql.append(" FROM t_stock_adjustlist ajl");
		sql.append(" LEFT JOIN t_stock_data sd ON ajl_sub_code = sd_code AND ajl.companyid = sd.companyid AND sd.sd_dp_code = :dp_code");
		sql.append(" WHERE 1=1");
		sql.append(" AND ajl_number = :ajl_number");
		sql.append(" AND ajl.companyid = :companyid");
		sql.append(" GROUP BY ajl_sub_code");
		return namedParameterJdbcTemplate.query(sql.toString(),
				new MapSqlParameterSource().addValue("ajl_number", number).addValue("dp_code", dp_code).addValue("companyid", companyid),
				new BeanPropertyRowMapper<>(T_Stock_DataBill.class));
	}
	
	@Override
	public void del(String aj_number, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" DELETE FROM t_stock_adjust");
		sql.append(" WHERE aj_number=:aj_number");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("aj_number", aj_number).addValue("companyid", companyid));
		sql.setLength(0);
		sql.append(" DELETE FROM t_stock_adjustlist");
		sql.append(" WHERE ajl_number=:ajl_number");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("ajl_number", aj_number).addValue("companyid", companyid));
	}
	
	@Override
	public void deleteList(String aj_number, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" DELETE FROM t_stock_adjustlist");
		sql.append(" WHERE ajl_number=:ajl_number");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("ajl_number", aj_number).addValue("companyid", companyid));
	}
	
}
