package zy.dao.base.brand.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.base.brand.BrandDAO;
import zy.entity.base.brand.T_Base_Brand;

@Repository
public class BrandDAOImpl extends BaseDaoImpl implements BrandDAO{

	@Override
	public Integer count(Map<String, Object> param) {
		Object searchContent = param.get("searchContent");
		StringBuffer sql = new StringBuffer("");
		sql.append(" select count(1)");
		sql.append(" from t_base_brand t");
		sql.append(" where 1 = 1");
		if(null != searchContent && !"".equals(searchContent)){
        	sql.append(" AND (INSTR(bd_code,:searchContent)>0 OR INSTR(bd_name,:searchContent)>0 OR INSTR(bd_spell,:searchContent)>0)");
        }
		sql.append(" and t.companyid=:companyid");
		Integer count = namedParameterJdbcTemplate.queryForObject(sql.toString(), param, Integer.class);
		return count;
	}

	@Override
	public List<T_Base_Brand> list(Map<String, Object> param) {
		Object searchContent = param.get("searchContent");
		StringBuffer sql = new StringBuffer("");
		sql.append(" select bd_id,bd_code,bd_name,bd_spell,bd_state,companyid");
		sql.append(" from t_base_brand t");
		sql.append(" where 1 = 1");
		if(null != searchContent && !"".equals(searchContent)){
        	sql.append(" AND (INSTR(bd_code,:searchContent)>0 OR INSTR(bd_name,:searchContent)>0 OR INSTR(bd_spell,:searchContent)>0)");
        }
		sql.append(" and t.companyid=:companyid");
		sql.append(" order by bd_id desc");
		sql.append(" limit :start,:end");
		List<T_Base_Brand> list = namedParameterJdbcTemplate.query(sql.toString(), param, new BeanPropertyRowMapper<>(T_Base_Brand.class));
		return list;
	}

	@Override
	public Integer queryByName(T_Base_Brand brand) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" select bd_id from t_base_brand");
		sql.append(" where 1=1");
		if(null != brand.getBd_name() && !"".equals(brand.getBd_name())){
			sql.append(" and bd_name=:bd_name");
		}
		if(null != brand.getBd_id() && brand.getBd_id() > 0){
			sql.append(" and bd_id <> :bd_id");
		}
		sql.append(" and companyid=:companyid");
		sql.append(" limit 1");
		try{
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new BeanPropertySqlParameterSource(brand),Integer.class);
		}catch(Exception e){
			return null;
		}
	}

	@Override
	public T_Base_Brand queryByID(Integer bd_id) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" select bd_id,bd_code,bd_name,bd_spell,bd_state,companyid ");
		sql.append(" from t_base_brand");
		sql.append(" where bd_id=:bd_id");
		sql.append(" limit 1");
		try{
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), new MapSqlParameterSource().addValue("bd_id", bd_id),new BeanPropertyRowMapper<>(T_Base_Brand.class));
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void save(T_Base_Brand brand) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" select f_three_code(max(bd_code+0)) from t_base_brand ");
		sql.append(" where 1=1");
		sql.append(" and companyid=:companyid");
		String code = namedParameterJdbcTemplate.queryForObject(sql.toString(), new BeanPropertySqlParameterSource(brand), String.class);
		brand.setBd_code(code);
		sql.setLength(0);
		sql.append("INSERT INTO t_base_brand");
		sql.append(" (bd_code,bd_name,bd_spell,bd_state,companyid)");
		sql.append(" VALUES(:bd_code,:bd_name,:bd_spell,:bd_state,:companyid)");
		
		KeyHolder holder = new GeneratedKeyHolder(); 
		
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(brand),holder);
		int id = holder.getKey().intValue();
		brand.setBd_id(id);
	}

	@Override
	public void update(T_Base_Brand brand) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" update t_base_brand");
		sql.append(" set bd_name=:bd_name,bd_spell=:bd_spell,bd_state=:bd_state");
		sql.append(" where bd_id=:bd_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(brand));
	}

	@Override
	public void del(Integer bd_id) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" DELETE FROM t_base_brand");
		sql.append(" WHERE bd_id=:bd_id");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("bd_id", bd_id));
	}
	
	@Override
	public List<String> brandList(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" select bd_name ");
		sql.append(" from t_base_brand t");
		sql.append(" where 1 = 1");
		sql.append(" and t.companyid=:companyid");
		List<String> list = new ArrayList<String>();
		list = namedParameterJdbcTemplate.queryForList(sql.toString(), param, String.class);
		return list;
	}

	@Override
	public void save(List<T_Base_Brand> brands) {
		StringBuffer sql = new StringBuffer("");
		sql.append("INSERT INTO t_base_brand");
		sql.append(" (bd_code,bd_name,bd_spell,bd_state,companyid)");
		sql.append(" (SELECT f_three_code(max(bd_code+0))as bd_code,:bd_name,:bd_spell,:bd_state,:companyid from t_base_brand where 1=1 and companyid=:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(brands.toArray()));
	}
}
