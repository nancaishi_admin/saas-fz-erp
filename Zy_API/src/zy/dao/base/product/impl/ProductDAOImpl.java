package zy.dao.base.product.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.base.product.ProductDAO;
import zy.entity.base.bra.T_Base_Bra;
import zy.entity.base.brand.T_Base_Brand;
import zy.entity.base.color.T_Base_Color;
import zy.entity.base.dict.T_Base_DictList;
import zy.entity.base.product.T_Base_Barcode;
import zy.entity.base.product.T_Base_Barcode_Set;
import zy.entity.base.product.T_Base_Product;
import zy.entity.base.product.T_Base_Product_Assist;
import zy.entity.base.product.T_Base_Product_Br;
import zy.entity.base.product.T_Base_Product_Color;
import zy.entity.base.product.T_Base_Product_Img;
import zy.entity.base.product.T_Base_Product_Shop_Price;
import zy.entity.base.size.T_Base_SizeList;
import zy.entity.base.size.T_Base_Size_Group;
import zy.entity.base.type.T_Base_Type;
import zy.entity.buy.supply.T_Buy_Supply;
import zy.entity.sys.user.T_Sys_User;
import zy.util.CommonUtil;
import zy.util.StringUtil;
@Repository
public class ProductDAOImpl extends BaseDaoImpl implements ProductDAO {

	@Override
	public Integer count(Map<String, Object> param) {
		Object searchContent = param.get("searchContent");
		Object pd_no = param.get("pd_no");
		Object pd_name = param.get("pd_name");
		Object pd_spell = param.get("pd_spell");
		Object pd_year = param.get("pd_year");
		Object pd_season = param.get("pd_season");
		Object pd_tp_code = param.get("pd_tp_code");
		Object pd_bd_code = param.get("pd_bd_code");
		Object pd_sp_code = param.get("pd_sp_code");
		Object pd_number = param.get("pd_number");
		
		StringBuffer sql = new StringBuffer("");
		sql.append(" select count(1)");
		sql.append(" from t_base_product t");
		sql.append(" inner join t_base_product_info info on t.pd_code=info.pd_code and t.companyid=info.companyid");
		sql.append(" where 1 = 1");
        if(null != searchContent && !"".equals(searchContent)){
        	sql.append(" AND (instr(t.pd_name,:searchContent)>0 OR instr(t.pd_spell,:searchContent)>0 OR instr(t.pd_no,:searchContent)>0 OR instr(t.pd_number,:searchContent)>0)");
        	/*sql.append(" AND (instr(t.pd_name,:searchContent)>0 OR instr(t.pd_spell,:searchContent)>0 OR t.pd_code=:searchContent OR t.pd_no=:searchContent)");*/
        }
        if(null != pd_no && !"".equals(pd_no)){
			sql.append(" AND t.pd_no=:pd_no");
		}
		if(null != pd_name && !"".equals(pd_name)){
			sql.append(" AND instr(t.pd_name,:pd_name)>0");
		}
		if(null != pd_spell && !"".equals(pd_spell)){
			sql.append(" AND instr(t.pd_spell,:pd_spell)>0");
		}
		if(null != pd_number && !"".equals(pd_number)){
			sql.append(" AND instr(t.pd_number,:pd_number)>0");
		}
		if(null != pd_year && !"".equals(pd_year)){
			sql.append(" AND t.pd_year=:pd_year");
		}
		if(null != pd_season && !"".equals(pd_season)){
			sql.append(" AND t.pd_season=:pd_season");
		}
		if (pd_tp_code != null && !"".equals(pd_tp_code) && !"0".equals(pd_tp_code)) {
			pd_tp_code = pd_tp_code.toString().replace(",", "','");
			sql.append(" AND t.pd_tp_code IN ('"+pd_tp_code+"')");
		}
		if(null != pd_bd_code && !"".equals(pd_bd_code)){
			sql.append(" AND t.pd_bd_code=:pd_bd_code");
		}
		if(null != pd_sp_code && !"".equals(pd_sp_code)){
			sql.append(" AND t.pd_sp_code=:pd_sp_code");
		}
		sql.append(" and t.companyid=:companyid");
		Integer count = namedParameterJdbcTemplate.queryForObject(sql.toString(), param, Integer.class);
		return count;
	}

	@Override
	public List<T_Base_Product> list(Map<String, Object> param) {
		Object searchContent = param.get("searchContent");
		Object start = param.get("start");
		Object end = param.get("end");
		Object pd_id = param.get("pd_id");
		Object pd_no = param.get("pd_no");
		Object pd_name = param.get("pd_name");
		Object pd_spell = param.get("pd_spell");
		Object pd_year = param.get("pd_year");
		Object pd_season = param.get("pd_season");
		Object pd_tp_code = param.get("pd_tp_code");
		Object pd_bd_code = param.get("pd_bd_code");
		Object pd_sp_code = param.get("pd_sp_code");
		Object pd_number = param.get("pd_number");
		
		StringBuffer sql = new StringBuffer("");
		sql.append(" select t.pd_id,t.pd_code,pd_no,pd_number,pd_name,pd_spell,pd_tp_code,pd_bd_code,pd_szg_code,pd_cost_price,pd_batch_price,pd_buy_price,pd_sort_price,");
		sql.append("pd_vip_price,pd_sign_price,pd_sell_price,pd_style,pd_unit,pd_season,pd_year,pd_date,pd_fabric,pd_buy_cycle,pd_sell_cycle,pd_sp_code,pd_bra,");
		sql.append("pd_add_manager,pd_add_date,pd_mod_manager,pd_mod_date,");
		sql.append("(select bd_name from t_base_brand bd where bd.bd_code=t.pd_bd_code and bd.companyid=t.companyid LIMIT 1) as pd_bd_name,");//品牌
		sql.append("(select tp_name from t_base_type tp where tp.tp_code=t.pd_tp_code and tp.companyid=t.companyid LIMIT 1) as pd_tp_name,");//类别
		sql.append("(select szg_name from t_base_size_group szg where szg.szg_code=t.pd_szg_code and szg.companyid=t.companyid LIMIT 1) as pd_szg_name,");//尺码组
		sql.append("(select pdm_id from t_base_product_img pdm where pdm.pdm_pd_code=t.pd_code and pdm.companyid=t.companyid LIMIT 1) as pdm_id");
//		sql.append("(select sp_name from t_buy_supply sp where sp.sp_code=main.pdm_sp_code and sp.companyid=main.companyid LIMIT 1) as pdm_sp_name");//供货商
		sql.append(" from t_base_product t");
		sql.append(" inner join t_base_product_info info on t.pd_code=info.pd_code and t.companyid=info.companyid");
//		sql.append(" inner join t_base_product_main main on t.pd_code=main.pd_code and t.companyid=main.companyid");
//		sql.append(" inner join t_base_product_sell sell on t.pd_code=sell.pd_code and t.companyid=sell.companyid");
		sql.append(" where 1 = 1");
		if(null != searchContent && !"".equals(searchContent)){
			sql.append(" AND (instr(t.pd_name,:searchContent)>0 OR instr(t.pd_spell,:searchContent)>0 OR instr(t.pd_no,:searchContent)>0 OR instr(t.pd_number,:searchContent)>0)");
	    }
		if(null != pd_id && !"".equals(pd_id)){
			sql.append(" AND t.pd_id=:pd_id");
		}
		if(null != pd_no && !"".equals(pd_no)){
			sql.append(" AND t.pd_no=:pd_no");
		}
		if(null != pd_name && !"".equals(pd_name)){
			sql.append(" AND instr(t.pd_name,:pd_name)>0");
		}
		if(null != pd_spell && !"".equals(pd_spell)){
			sql.append(" AND instr(t.pd_spell,:pd_spell)>0");
		}
		if(null != pd_number && !"".equals(pd_number)){
			sql.append(" AND instr(t.pd_number,:pd_number)>0");
		}
		if(null != pd_year && !"".equals(pd_year)){
			sql.append(" AND t.pd_year=:pd_year");
		}
		if(null != pd_season && !"".equals(pd_season)){
			sql.append(" AND t.pd_season=:pd_season");
		}
		if (pd_tp_code != null && !"".equals(pd_tp_code) && !"0".equals(pd_tp_code)) {
			pd_tp_code = pd_tp_code.toString().replace(",", "','");
			sql.append(" AND t.pd_tp_code IN ('"+pd_tp_code+"')");
		}
		if(null != pd_bd_code && !"".equals(pd_bd_code)){
			sql.append(" AND t.pd_bd_code=:pd_bd_code");
		}
		if(null != pd_sp_code && !"".equals(pd_sp_code)){
			sql.append(" AND t.pd_sp_code=:pd_sp_code");
		}
		sql.append(" and t.companyid=:companyid");
		sql.append(" order by pd_id desc");
		if(null != start && !"".equals(start) && null != end && !"".equals(end)){
			sql.append(" limit :start,:end");
		}
		List<T_Base_Product> list = namedParameterJdbcTemplate.query(sql.toString(), param, new BeanPropertyRowMapper<>(T_Base_Product.class));
		return list;
	}

	@Override
	public Integer barcodeCount(Map<String, Object> param) {
		Object pd_code = param.get("pd_code");
		StringBuffer sql = new StringBuffer("");
		sql.append(" select count(1)");
		sql.append(" from t_base_barcode t");
		sql.append(" where 1 = 1");
        if(null != pd_code && !"".equals(pd_code)){
        	sql.append(" AND t.bc_pd_code=:pd_code");
        }
		sql.append(" and t.companyid=:companyid");
		Integer count = namedParameterJdbcTemplate.queryForObject(sql.toString(), param, Integer.class);
		return count;
	}

	@Override
	public List<T_Base_Barcode> barcodeList(Map<String, Object> param) {
		Object pd_code = param.get("pd_code");
		Object start = param.get("start");
		Object end = param.get("end");
		StringBuffer sql = new StringBuffer("");
		sql.append(" select bc_id,bc_pd_code,bc_pd_no,bc_size,bc_bra,bc_color,bc_subcode,bc_sys_barcode,bc_barcode,pd_sell_price,pd_vip_price,pd_sign_price,");
		sql.append("pd_name as bc_pd_name,pd_fill,pd_safe,pd_grade,pd_execute,pd_fabric,pd_in_fabric,pd_place,pd_wash_explain,");
		sql.append("(select bd_name from t_base_brand  bd where bd.bd_code=pd.pd_bd_code and bd.companyid=pd.companyid LIMIT 1) as pd_bd_name,");//品牌
		sql.append("(select tp_name from t_base_type tp where tp.tp_code=pd.pd_tp_code and tp.companyid=pd.companyid LIMIT 1) as pd_tp_name,");//类别
		sql.append("(select cr_name from t_base_color color where color.cr_code=t.bc_color and color.companyid=t.companyid LIMIT 1) as bc_colorname,");//颜色
		sql.append("(select sz_name from t_base_size size where size.sz_code=t.bc_size and size.companyid=t.companyid LIMIT 1) as bc_sizename,");//尺码
		sql.append("(select br_name from t_base_bra bra where bra.br_code=t.bc_bra and bra.companyid=t.companyid LIMIT 1) as bc_braname");//杯型
		sql.append(" from t_base_barcode t");
		sql.append(" LEFT JOIN t_base_product pd ON pd.pd_code = t.bc_pd_code AND pd.companyid = t.companyid ");
		sql.append(" JOIN t_base_product_info pdi ON pdi.pd_code = pd.pd_code AND pdi.companyid = pd.companyid");
		sql.append(" where 1 = 1");
		if(null != pd_code && !"".equals(pd_code)){
			sql.append(" AND t.bc_pd_code=:pd_code");
	    }
		sql.append(" and t.companyid=:companyid");
		sql.append(" order by bc_id desc ");
		if(null != start && !"".equals(start) && null != end && !"".equals(end)){
			sql.append(" limit :start,:end");
		}
		List<T_Base_Barcode> list = namedParameterJdbcTemplate.query(sql.toString(), param, new BeanPropertyRowMapper<>(T_Base_Barcode.class));
		return list;
	}
	
	@Override
	public Integer shop_price_listCount(Map<String, Object> param) {
		Object pd_code = param.get("pd_code");
		Object shop_type= param.get("shop_type");
		StringBuffer sql = new StringBuffer("");
		sql.append(" select count(1)");
		sql.append(" from t_base_product_shop_price t");
		sql.append(" JOIN t_base_shop sp ON sp_code = pdp_shop_code AND sp.companyid = t.companyid");
		if(CommonUtil.ONE.equals(shop_type) || CommonUtil.TWO.equals(shop_type)){//总公司、分公司
			sql.append(getShopSQL(shop_type, 0));
			sql.append(" WHERE 1 = 1");
		}else{//加盟店
			sql.append(" JOIN common_type on ty_id=sp_shop_type");
			sql.append(" WHERE 1 = 1");
			sql.append(" AND pdp_shop_code = :shop_code");
		}
        if(null != pd_code && !"".equals(pd_code)){
        	sql.append(" AND t.pdp_pd_code=:pd_code");
        }
		sql.append(" and t.companyid=:companyid");
		Integer count = namedParameterJdbcTemplate.queryForObject(sql.toString(), param, Integer.class);
		return count;
	}
	
	@Override
	public List<T_Base_Product_Shop_Price> shop_price_list(
			Map<String, Object> param) {
		Object pd_code = param.get("pd_code");
		Object shop_type= param.get("shop_type");
		Object start = param.get("start");
		Object end = param.get("end");
		StringBuffer sql = new StringBuffer("");
		sql.append(" select pdp_id,pdp_pd_code,pdp_pd_point,pdp_shop_code,pdp_cost_price,pdp_sell_price,");
		sql.append(" pdp_vip_price,pdp_sort_price,sp.sp_name as shop_name,ty_name as shop_type ");
		sql.append(" from t_base_product_shop_price t");
		sql.append(" JOIN t_base_shop sp ON sp_code = pdp_shop_code AND sp.companyid = t.companyid");
		if(CommonUtil.ONE.equals(shop_type) || CommonUtil.TWO.equals(shop_type)){//总公司、分公司
			sql.append(getShopSQL(shop_type, 0));
			sql.append(" WHERE 1 = 1");
		}else{//加盟店
			sql.append(" JOIN common_type on ty_id=sp_shop_type");
			sql.append(" WHERE 1 = 1");
			sql.append(" AND pdp_shop_code = :shop_code");
		}
        if(null != pd_code && !"".equals(pd_code)){
        	sql.append(" AND t.pdp_pd_code=:pd_code");
        }
		sql.append(" and t.companyid=:companyid");
		sql.append(" order by pdp_id desc ");
		if(null != start && !"".equals(start) && null != end && !"".equals(end)){
			sql.append(" limit :start,:end");
		}
		return namedParameterJdbcTemplate.query(sql.toString(), param, new BeanPropertyRowMapper<>(T_Base_Product_Shop_Price.class));
	}

	@Override
	public Integer barcodePageCount(Map<String, Object> param) {
		Object pd_no = param.get("pd_no");
		Object pd_name = param.get("pd_name");
		Object bc_barcode = param.get("bc_barcode");
		Object pd_tp_code = param.get("pd_tp_code");
		Object pd_bd_code = param.get("pd_bd_code");
		StringBuffer sql = new StringBuffer("");
		sql.append(" select count(1)");
		sql.append(" from t_base_barcode t");
		sql.append(" left join t_base_product pd on pd.pd_code = t.bc_pd_code and pd.companyid = t.companyid ");
		sql.append(" where 1 = 1");
		if(null != pd_no && !"".equals(pd_no)){
			sql.append(" AND instr(pd.pd_no,:pd_no)>0");
		}
		if(null != pd_name && !"".equals(pd_name)){
			sql.append(" AND instr(pd.pd_name,:pd_name)>0");
		}
		if(null != bc_barcode && !"".equals(bc_barcode)){
			sql.append(" AND t.bc_barcode=:bc_barcode");
		}
		if (pd_tp_code != null && !"".equals(pd_tp_code) && !"0".equals(pd_tp_code)) {
			pd_tp_code = pd_tp_code.toString().replace(",", "','");
			sql.append(" AND pd.pd_tp_code IN ('"+pd_tp_code+"')");
		}
		if(null != pd_bd_code && !"".equals(pd_bd_code)){
			sql.append(" AND pd.pd_bd_code=:pd_bd_code");
		}
		sql.append(" and t.companyid=:companyid");
		Integer count = namedParameterJdbcTemplate.queryForObject(sql.toString(), param, Integer.class);
		return count;
	}

	@Override
	public List<T_Base_Barcode> barcodePageList(Map<String, Object> param) {
		Object pd_no = param.get("pd_no");
		Object pd_name = param.get("pd_name");
		Object bc_barcode = param.get("bc_barcode");
		Object pd_tp_code = param.get("pd_tp_code");
		Object pd_bd_code = param.get("pd_bd_code");
		Object start = param.get("start");
		Object end = param.get("end");
		StringBuffer sql = new StringBuffer("");
		sql.append(" select bc_id,bc_pd_code,bc_pd_no,bc_size,bc_bra,bc_color,bc_subcode,bc_sys_barcode,bc_barcode,pd.pd_name as bc_pd_name,pd.pd_sell_price,");
		sql.append(" pd_vip_price,pd_sign_price,pd_grade,pd_safe,pd_fill,pd_execute,pd_place,pd_in_fabric,pd_fabric,pd_wash_explain,");
		sql.append("(select bd_name from t_base_brand bd where bd.bd_code=pd.pd_bd_code and bd.companyid=pd.companyid LIMIT 1) as pd_bd_name,");//品牌
		sql.append("(select tp_name from t_base_type tp where tp.tp_code=pd.pd_tp_code and tp.companyid=pd.companyid LIMIT 1) as pd_tp_name,");//类别
		sql.append("(select cr_name from t_base_color color where color.cr_code=t.bc_color and color.companyid=t.companyid LIMIT 1) as bc_colorname,");//颜色
		sql.append("(select sz_name from t_base_size size where size.sz_code=t.bc_size and size.companyid=t.companyid LIMIT 1) as bc_sizename,");//尺码
		sql.append("(select br_name from t_base_bra bra where bra.br_code=t.bc_bra and bra.companyid=t.companyid LIMIT 1) as bc_braname");//杯型
		sql.append(" from t_base_barcode t");
		sql.append(" left join t_base_product pd on pd.pd_code = t.bc_pd_code and pd.companyid = t.companyid ");
		sql.append(" join t_base_product_info info on info.pd_code = pd.pd_code and info.companyid = pd.companyid ");
		sql.append(" where 1 = 1");
		if(null != pd_no && !"".equals(pd_no)){
			sql.append(" AND instr(pd.pd_no,:pd_no)>0");
		}
		if(null != pd_name && !"".equals(pd_name)){
			sql.append(" AND instr(pd.pd_name,:pd_name)>0");
		}
		if(null != bc_barcode && !"".equals(bc_barcode)){
			sql.append(" AND t.bc_barcode=:bc_barcode");
		}
		if (pd_tp_code != null && !"".equals(pd_tp_code) && !"0".equals(pd_tp_code)) {
			pd_tp_code = pd_tp_code.toString().replace(",", "','");
			sql.append(" AND pd.pd_tp_code IN ('"+pd_tp_code+"')");
		}
		if(null != pd_bd_code && !"".equals(pd_bd_code)){
			sql.append(" AND pd.pd_bd_code=:pd_bd_code");
		}
		sql.append(" and t.companyid=:companyid");
		sql.append(" order by bc_id desc ");
		if(null != start && !"".equals(start) && null != end && !"".equals(end)){
			sql.append(" limit :start,:end");
		}
		List<T_Base_Barcode> list = namedParameterJdbcTemplate.query(sql.toString(), param, new BeanPropertyRowMapper<>(T_Base_Barcode.class));
		return list;
	}

	@Override
	public List<T_Base_Barcode_Set> barcodeplan(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer("");
		sql.append("select bs_id,bs_plan_name,bs_plan_state ");
		sql.append(" from t_base_barcode_set t ");
		sql.append(" where 1=1 ");
		sql.append(" and t.companyid=:companyid ");
		sql.append(" order by bs_id desc ");
		return namedParameterJdbcTemplate.query(sql.toString(), param, new BeanPropertyRowMapper<>(T_Base_Barcode_Set.class));
	}

	@Override
	public T_Base_Barcode_Set getPrintSetByBsid(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer("");
		sql.append("select * from t_base_barcode_set t ");
		sql.append(" where 1=1 ");
		sql.append(" and t.companyid=:companyid ");
		sql.append(" and t.bs_id=:bs_id");
		return namedParameterJdbcTemplate.queryForObject(sql.toString(), param ,new BeanPropertyRowMapper<>(T_Base_Barcode_Set.class));
	}
	
	@Override
	public T_Base_Barcode_Set getPrintSetByState(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer("");
		sql.append("select * from t_base_barcode_set t ");
		sql.append(" where 1=1 ");
		sql.append(" and t.companyid=:companyid ");
		sql.append(" and t.bs_plan_state=1");
		return namedParameterJdbcTemplate.queryForObject(sql.toString(), param ,new BeanPropertyRowMapper<>(T_Base_Barcode_Set.class));
	}

	@Override
	public void enable_plan(Map<String, Object> param) {
		Object companyid = param.get("companyid");
		Object bs_id = param.get("bs_id");
		StringBuffer sql = new StringBuffer("");
		sql.append(" UPDATE t_base_barcode_set SET bs_plan_state = 0 WHERE companyid=:companyid AND bs_plan_state = 1 ");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("companyid", companyid));
		
		sql.setLength(0);
		sql.append(" UPDATE t_base_barcode_set SET bs_plan_state = 1 WHERE companyid=:companyid AND bs_id = :bs_id ");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("bs_id", bs_id)
				.addValue("companyid", companyid));
	}

	@Override
	public void del_bar_plan(Map<String, Object> param) {
		Object companyid = param.get("companyid");
		Object bs_id = param.get("bs_id");
		StringBuffer sql = new StringBuffer("");
		sql.append(" DELETE FROM t_base_barcode_set WHERE companyid=:companyid AND bs_id = :bs_id ");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("companyid", companyid)
				.addValue("bs_id", bs_id));
	}

	@Override
	public void update_barcode_printset(T_Base_Barcode_Set barcode_Set) {
		StringBuffer sql = new StringBuffer();
		sql.append("UPDATE t_base_barcode_set SET ");
		sql.append(" bs_paper_width=:bs_paper_width,");
		sql.append(" bs_paper_height=:bs_paper_height,");
		sql.append(" bs_top_backgauge=:bs_top_backgauge,");
		sql.append(" bs_left_backgauge=:bs_left_backgauge,");
		sql.append(" bs_bartype=:bs_bartype,");
		sql.append(" bs_line=:bs_line,");
		sql.append(" bs_hight_spacing=:bs_hight_spacing,");
		sql.append(" bs_width_spacing=:bs_width_spacing,");
		sql.append(" bs_width=:bs_width,");
		sql.append(" bs_height=:bs_height,");
		sql.append(" bs_p_space=:bs_p_space,");
		sql.append(" bs_ps_space=:bs_ps_space,");
		sql.append(" bs_font_name=:bs_font_name,");
		sql.append(" bs_font_size=:bs_font_size,");
		sql.append(" bs_bold=:bs_bold,");
		sql.append(" bs_pdname_ifshow=:bs_pdname_ifshow,");
		sql.append(" bs_pdname_row=:bs_pdname_row,");
		sql.append(" bs_class_ifshow=:bs_class_ifshow,");
		sql.append(" bs_class_row=:bs_class_row,");
		sql.append(" bs_safestandard_ifshow=:bs_safestandard_ifshow,");
		sql.append(" bs_safestandard_row=:bs_safestandard_row,");
		sql.append(" bs_filling_ifshow=:bs_filling_ifshow,");
		sql.append(" bs_filling_row=:bs_filling_row,");
		sql.append(" bs_cr_ifshow=:bs_cr_ifshow,");
		sql.append(" bs_cr_row=:bs_cr_row,");
		sql.append(" bs_pdno_ifshow=:bs_pdno_ifshow,");
		sql.append(" bs_pdno_row=:bs_pdno_row,");
		sql.append(" bs_pdno_row=:bs_pdno_row,");
		sql.append(" bs_sz_ifshow=:bs_sz_ifshow,");
		sql.append(" bs_sz_row=:bs_sz_row,");
		sql.append(" bs_bs_ifshow=:bs_bs_ifshow,");
		sql.append(" bs_bs_row=:bs_bs_row,");
		sql.append(" bs_bd_ifshow=:bs_bd_ifshow,");
		sql.append(" bs_bd_row=:bs_bd_row,");
		sql.append(" bs_price_ifshow=:bs_price_ifshow,");
		sql.append(" bs_price_row=:bs_price_row,");
		sql.append(" bs_show_fiename=:bs_show_fiename,");
		sql.append(" bs_plan_name=:bs_plan_name,");
		sql.append(" bs_subcode_showup=:bs_subcode_showup,");
		
		sql.append(" bs_price_showdown=:bs_price_showdown,");
		sql.append(" bs_executionstandard_ifshow=:bs_executionstandard_ifshow,");
		sql.append(" bs_executionstandard_row=:bs_executionstandard_row,");
		sql.append(" bs_language=:bs_language,");
		sql.append(" bs_vipprice_ifshow=:bs_vipprice_ifshow,");
		sql.append(" bs_vipprice_row=:bs_vipprice_row,");
		sql.append(" bs_signprice_ifshow=:bs_signprice_ifshow,");
		sql.append(" bs_signprice_row=:bs_signprice_row,");
		sql.append(" bs_type_ifshow=:bs_type_ifshow,");
		sql.append(" bs_type_row=:bs_type_row,");
		sql.append(" bs_fabric_ifshow=:bs_fabric_ifshow,");
		sql.append(" bs_fabric_row=:bs_fabric_row,");
		sql.append(" bs_infabric_ifshow=:bs_infabric_ifshow,");
		sql.append(" bs_infabric_row=:bs_infabric_row,");
		sql.append(" bs_place_ifshow=:bs_place_ifshow,");
		sql.append(" bs_place_row=:bs_place_row,");
		sql.append(" bs_washexplain_ifshow=:bs_washexplain_ifshow,");
		sql.append(" bs_washexplain_row=:bs_washexplain_row");
		sql.append(" where bs_id=:bs_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(barcode_Set));
	}

	@Override
	public void save_barcode_printset(T_Base_Barcode_Set barcode_Set) {
		StringBuffer sql = new StringBuffer();
		sql.append(" UPDATE t_base_barcode_set SET bs_plan_state = 0 WHERE companyid=:companyid AND bs_plan_state = 1 ");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("companyid", barcode_Set.getCompanyid()));
		
		sql.setLength(0);
		sql.append("INSERT INTO t_base_barcode_set");
		sql.append("(bs_paper_width,bs_paper_height,bs_top_backgauge,bs_left_backgauge,bs_bartype,bs_line,bs_hight_spacing,bs_width_spacing,bs_width,");
		sql.append(" bs_height,bs_p_space,bs_ps_space,bs_font_name,bs_font_size,bs_bold,bs_pdname_ifshow,bs_pdname_row,bs_class_ifshow,bs_class_row,");
		sql.append(" bs_safestandard_ifshow,bs_safestandard_row,bs_filling_ifshow,bs_filling_row,bs_cr_ifshow,bs_cr_row,bs_pdno_ifshow,bs_pdno_row,bs_sz_ifshow,");
		sql.append(" bs_sz_row,bs_bs_ifshow,bs_bs_row,bs_bd_ifshow,bs_bd_row,bs_price_ifshow,bs_price_row,bs_show_fiename,bs_sysdate,bs_plan_name,bs_plan_state,");
		sql.append(" bs_subcode_showup,bs_price_showdown,bs_executionstandard_ifshow,bs_executionstandard_row,bs_language,bs_vipprice_ifshow,bs_vipprice_row,bs_signprice_ifshow,bs_signprice_row,");
		sql.append(" bs_type_ifshow,bs_type_row,bs_fabric_ifshow,bs_fabric_row,bs_infabric_ifshow,bs_infabric_row,bs_place_ifshow,bs_place_row,bs_washexplain_ifshow,bs_washexplain_row,");
		sql.append(" companyid)");
		sql.append(" VALUES");
		sql.append("(:bs_paper_width,:bs_paper_height,:bs_top_backgauge,:bs_left_backgauge,:bs_bartype,:bs_line,:bs_hight_spacing,:bs_width_spacing,:bs_width,");
		sql.append(" :bs_height,:bs_p_space,:bs_ps_space,:bs_font_name,:bs_font_size,:bs_bold,:bs_pdname_ifshow,:bs_pdname_row,:bs_class_ifshow,:bs_class_row,");
		sql.append(" :bs_safestandard_ifshow,:bs_safestandard_row,:bs_filling_ifshow,:bs_filling_row,:bs_cr_ifshow,:bs_cr_row,:bs_pdno_ifshow,:bs_pdno_row,:bs_sz_ifshow,");
		sql.append(" :bs_sz_row,:bs_bs_ifshow,:bs_bs_row,:bs_bd_ifshow,:bs_bd_row,:bs_price_ifshow,:bs_price_row,:bs_show_fiename,:bs_sysdate,:bs_plan_name,1,");
		sql.append(" :bs_subcode_showup,:bs_price_showdown,:bs_executionstandard_ifshow,:bs_executionstandard_row,:bs_language,:bs_vipprice_ifshow,:bs_vipprice_row,:bs_signprice_ifshow,:bs_signprice_row,");
		sql.append(" :bs_type_ifshow,:bs_type_row,:bs_fabric_ifshow,:bs_fabric_row,:bs_infabric_ifshow,:bs_infabric_row,:bs_place_ifshow,:bs_place_row,:bs_washexplain_ifshow,:bs_washexplain_row,");
		sql.append(" :companyid)");
		KeyHolder holder = new GeneratedKeyHolder(); 
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(barcode_Set),holder);
	}

	@Override
	public Integer queryByPdNo(T_Base_Product product) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT pd_id FROM t_base_product");
		sql.append(" WHERE 1=1");
		if(null != product.getPd_no() && !"".equals(product.getPd_no())){
			sql.append(" AND pd_no=:pd_no");
		}
		if(null != product.getPd_id() && product.getPd_id() > 0){
			sql.append(" AND pd_id <> :pd_id");
		}
		sql.append(" AND companyid=:companyid");
		sql.append(" LIMIT 1");
		try{
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new BeanPropertySqlParameterSource(product),Integer.class);
		}catch(Exception e){
			return null;
		}
	}

	@Override
	public void save(T_Base_Product product) {
		String br_codes = product.getPdb_br_codes();//杯型编号
		String cr_codes = product.getPdc_cr_codes();//颜色编号
		
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT f_autoincre_code(max(pd_code+0)) FROM t_base_product ");
		sql.append(" WHERE 1=1");
		sql.append(" AND companyid=:companyid");
		String code = namedParameterJdbcTemplate.queryForObject(sql.toString(), new BeanPropertySqlParameterSource(product), String.class);
		product.setPd_code(code);
		sql.setLength(0);
		sql.append("INSERT INTO t_base_product");
		sql.append("(pd_code,pd_no,pd_number,pd_name,pd_spell,pd_szg_code,pd_tp_code,pd_tp_upcode,pd_unit,");
		sql.append(" pd_bd_code,pd_season,pd_style,pd_year,pd_cost_price,pd_sell_cycle,pd_sell_price,pd_state,");
		sql.append(" pd_fabric,pd_bra,pd_change,pd_date,pd_returndate,pd_commission,pd_commission_rate,pd_present,pd_score,");
		sql.append(" pd_sp_code,pd_buy_cycle,pd_buy_price,pd_buy,pd_sort_price,pd_batch_price,pd_batch_price1,");
		sql.append(" pd_batch_price2,pd_batch_price3,pd_sign_price,pd_vip_price,pd_vip_sale,pd_gift,pd_point,pd_sale,");
		sql.append(" companyid)");
		sql.append(" VALUES");
		sql.append("(:pd_code,:pd_no,:pd_number,:pd_name,:pd_spell,:pd_szg_code,:pd_tp_code,:pd_tp_upcode,:pd_unit,");
		sql.append(" :pd_bd_code,:pd_season,:pd_style,:pd_year,:pd_cost_price,:pd_sell_cycle,:pd_sell_price,:pd_state,");
		sql.append(" :pd_fabric,:pd_bra,:pd_change,:pd_date,:pd_returndate,:pd_commission,:pd_commission_rate,:pd_present,:pd_score,");
		sql.append(" :pd_sp_code,:pd_buy_cycle,:pd_buy_price,:pd_buy,:pd_sort_price,:pd_batch_price,:pd_batch_price1,");
		sql.append(" :pd_batch_price2,:pd_batch_price3,:pd_sign_price,:pd_vip_price,:pd_vip_sale,:pd_gift,:pd_point,:pd_sale,");
		sql.append(" :companyid)");
		KeyHolder holder = new GeneratedKeyHolder(); 
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(product),holder);
		product.setPd_id(holder.getKey().intValue());
		sql.setLength(0);
		sql.append("INSERT INTO t_base_product_info");
		sql.append(" (pd_code,pd_sysdate,pd_fill,pd_color,pd_size,pd_place,pd_in_fabric,pd_model,pd_safe,pd_execute,pd_grade,");
		sql.append(" pd_add_manager,pd_add_date,pd_price_name,pd_salesman_comm,pd_salesman_commrate,pd_gbcode,pd_wash_explain,companyid)");
		sql.append(" VALUES");
		sql.append(" (:pd_code,:pd_sysdate,:pd_fill,:pd_color,:pd_size,:pd_place,:pd_in_fabric,:pd_model,:pd_safe,:pd_execute,:pd_grade,");
		sql.append(" :pd_add_manager,:pd_add_date,:pd_price_name,:pd_salesman_comm,:pd_salesman_commrate,:pd_gbcode,:pd_wash_explain,:companyid)");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(product));
		
		if(cr_codes != null && !"".equals(cr_codes)){
			String[] cr_arr = cr_codes.split(",");
			List<T_Base_Product_Color> colorList = new ArrayList<T_Base_Product_Color>();
			T_Base_Product_Color color = null;
			for(int i=0;i<cr_arr.length;i++){
				color = new T_Base_Product_Color();
				color.setPdc_pd_code(product.getPd_code());
				color.setPdc_cr_code(cr_arr[i]);
				color.setCompanyid(product.getCompanyid());
				colorList.add(color);
			}
			sql.setLength(0);
			sql.append("INSERT INTO t_base_product_color");
			sql.append(" (pdc_pd_code,pdc_cr_code,companyid)");
			sql.append(" VALUES(:pdc_pd_code,:pdc_cr_code,:companyid)");
			namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(colorList.toArray()));
		}
		
		if(br_codes != null && !"".equals(br_codes) 
				&& product.getPd_bra()==1){
			String[] br_arr = br_codes.split(",");
			List<T_Base_Product_Br> brList = new ArrayList<T_Base_Product_Br>();
			T_Base_Product_Br br = null;
			for(int i=0;i<br_arr.length;i++){
				br = new T_Base_Product_Br();
				br.setPdb_pd_code(product.getPd_code());
				br.setPdb_br_code(br_arr[i]);
				br.setCompanyid(product.getCompanyid());
				brList.add(br);
			}
			sql.setLength(0);
			sql.append("INSERT INTO t_base_product_br");
			sql.append(" (pdb_pd_code,pdb_br_code,companyid)");
			sql.append(" VALUES(:pdb_pd_code,:pdb_br_code,:companyid)");
			namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(brList.toArray()));
		}
	}

	@Override
	public void update(T_Base_Product product) {
		String br_codes = product.getPdb_br_codes();//杯型编号
		String br_codes_bak= product.getPdb_br_codes_bak();
		String cr_codes = product.getPdc_cr_codes();//颜色编号
		String cr_codes_bak = product.getPdc_cr_codes_bak();
		
		StringBuffer sql = new StringBuffer("");
		sql.append("UPDATE t_base_product SET ");
		sql.append(" pd_number=:pd_number,");
		sql.append(" pd_name=:pd_name,");
		sql.append(" pd_spell=:pd_spell,");
		sql.append(" pd_szg_code=:pd_szg_code,");
		sql.append(" pd_tp_code=:pd_tp_code,");
		sql.append(" pd_tp_upcode=:pd_tp_upcode,");
		sql.append(" pd_unit=:pd_unit,");
		sql.append(" pd_bd_code=:pd_bd_code,");
		sql.append(" pd_season=:pd_season,");
		sql.append(" pd_style=:pd_style,");
		sql.append(" pd_year=:pd_year,");
		sql.append(" pd_cost_price=:pd_cost_price,");
		sql.append(" pd_sell_cycle=:pd_sell_cycle,");
		sql.append(" pd_sell_price=:pd_sell_price,");
		sql.append(" pd_fabric=:pd_fabric,");
		sql.append(" pd_bra=:pd_bra,");
		sql.append(" pd_change=:pd_change,");
		sql.append(" pd_date=:pd_date,");
		sql.append(" pd_returndate=:pd_returndate,");
		sql.append(" pd_commission=:pd_commission,");
		sql.append(" pd_commission_rate=:pd_commission_rate,");
		sql.append(" pd_present=:pd_present,");
		sql.append(" pd_score=:pd_score,");
		sql.append(" pd_sign_price=:pd_sign_price,");
		sql.append(" pd_vip_price=:pd_vip_price,");
		sql.append(" pd_vip_sale=:pd_vip_sale,");
		sql.append(" pd_gift=:pd_gift,");
		sql.append(" pd_point=:pd_point,");
		sql.append(" pd_sale=:pd_sale,");
		sql.append(" pd_sp_code=:pd_sp_code,");
		sql.append(" pd_buy_cycle=:pd_buy_cycle,");
		sql.append(" pd_buy_price=:pd_buy_price,");
		sql.append(" pd_buy=:pd_buy,");
		sql.append(" pd_sort_price=:pd_sort_price,");
		sql.append(" pd_batch_price=:pd_batch_price,");
		sql.append(" pd_batch_price1=:pd_batch_price1,");
		sql.append(" pd_batch_price2=:pd_batch_price2,");
		sql.append(" pd_batch_price3=:pd_batch_price3");
		sql.append(" where pd_id=:pd_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(product));
		
		sql.setLength(0);
		sql.append("UPDATE t_base_product_info SET ");
		sql.append(" pd_fill=:pd_fill,");
		sql.append(" pd_color=:pd_color,");
		sql.append(" pd_size=:pd_size,");
		sql.append(" pd_place=:pd_place,");
		sql.append(" pd_in_fabric=:pd_in_fabric,");
		sql.append(" pd_model=:pd_model,");
		sql.append(" pd_safe=:pd_safe,");
		sql.append(" pd_execute=:pd_execute,");
		sql.append(" pd_mod_manager=:pd_mod_manager,");
		sql.append(" pd_mod_date=:pd_mod_date,");
		sql.append(" pd_grade=:pd_grade,");
		sql.append(" pd_price_name=:pd_price_name,");
		sql.append(" pd_salesman_comm=:pd_salesman_comm,");
		sql.append(" pd_salesman_commrate=:pd_salesman_commrate,");
		sql.append(" pd_gbcode=:pd_gbcode,");
		sql.append(" pd_wash_explain=:pd_wash_explain");
		sql.append(" where pd_code=:pd_code");
		sql.append(" and companyid=:companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(product));
		
		
		if(!cr_codes.equals(cr_codes_bak) &&
				cr_codes != null && !"".equals(cr_codes)){//颜色变更
			
			sql.setLength(0);
			sql.append(" DELETE FROM t_base_product_color");
			sql.append(" WHERE 1=1 ");
			sql.append(" AND pdc_pd_code=:pd_code ");
			sql.append(" AND companyid=:companyid ");
			namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(product));
			
			String[] cr_arr = cr_codes.split(",");
			List<T_Base_Product_Color> colorList = new ArrayList<T_Base_Product_Color>();
			T_Base_Product_Color color = null;
			for(int i=0;i<cr_arr.length;i++){
				color = new T_Base_Product_Color();
				color.setPdc_pd_code(product.getPd_code());
				color.setPdc_cr_code(cr_arr[i]);
				color.setCompanyid(product.getCompanyid());
				colorList.add(color);
			}
			sql.setLength(0);
			sql.append("INSERT INTO t_base_product_color");
			sql.append(" (pdc_pd_code,pdc_cr_code,companyid)");
			sql.append(" VALUES(:pdc_pd_code,:pdc_cr_code,:companyid)");
			namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(colorList.toArray()));
		}
		
		if(product.getPd_bra()==1){
			if(!br_codes.equals(br_codes_bak) 
					&& br_codes != null && !"".equals(br_codes)){//杯型变更
				
				sql.setLength(0);
				sql.append(" DELETE FROM t_base_product_br");
				sql.append(" WHERE 1=1 ");
				sql.append(" AND pdb_pd_code=:pd_code ");
				sql.append(" AND companyid=:companyid ");
				namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(product));
				
				String[] br_arr = br_codes.split(",");
				List<T_Base_Product_Br> brList = new ArrayList<T_Base_Product_Br>();
				T_Base_Product_Br br = null;
				for(int i=0;i<br_arr.length;i++){
					br = new T_Base_Product_Br();
					br.setPdb_pd_code(product.getPd_code());
					br.setPdb_br_code(br_arr[i]);
					br.setCompanyid(product.getCompanyid());
					brList.add(br);
				}
				sql.setLength(0);
				sql.append("INSERT INTO t_base_product_br");
				sql.append(" (pdb_pd_code,pdb_br_code,companyid)");
				sql.append(" VALUES(:pdb_pd_code,:pdb_br_code,:companyid)");
				namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(brList.toArray()));
			}
		} else {
			sql.setLength(0);
			sql.append(" DELETE FROM t_base_product_br");
			sql.append(" WHERE 1=1 ");
			sql.append(" AND pdb_pd_code=:pd_code ");
			sql.append(" AND companyid=:companyid ");
			namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(product));
		}
	}

	@Override
	public void del(Map<String, Object> param) {
		Object companyid = param.get("companyid");
		Object pd_id = param.get("pd_id");
		Object pd_code = param.get("pd_code");
		StringBuffer sql = new StringBuffer("");
		sql.append(" DELETE FROM t_base_product WHERE pd_id=:pd_id ");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("pd_id", pd_id));
		
		sql.setLength(0);
		sql.append(" DELETE FROM t_base_product_info");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND pd_code=:pd_code ");
		sql.append(" AND companyid=:companyid ");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("pd_code", pd_code)
				.addValue("companyid", companyid));
		
		sql.setLength(0);
		sql.append(" DELETE FROM t_base_product_color");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND pdc_pd_code=:pd_code ");
		sql.append(" AND companyid=:companyid ");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("pd_code", pd_code)
				.addValue("companyid", companyid));
		
		sql.setLength(0);
		sql.append(" DELETE FROM t_base_product_br");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND pdb_pd_code=:pd_code ");
		sql.append(" AND companyid=:companyid ");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("pd_code", pd_code)
				.addValue("companyid", companyid));
		
		sql.setLength(0);
		sql.append(" DELETE FROM t_base_product_shop_price");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND pdp_pd_code=:pd_code ");
		sql.append(" AND companyid=:companyid ");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("pd_code", pd_code)
				.addValue("companyid", companyid));
		
		sql.setLength(0);//条形码
		sql.append(" DELETE FROM t_base_barcode");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND bc_pd_code=:pd_code ");
		sql.append(" AND companyid=:companyid ");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("pd_code", pd_code)
				.addValue("companyid", companyid));
		
		sql.setLength(0);
		sql.append(" DELETE FROM t_base_product_img");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND pdm_pd_code=:pd_code ");
		sql.append(" AND companyid=:companyid ");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("pd_code", pd_code)
				.addValue("companyid", companyid));
	}

	@Override
	public String queryMaxPdcode(Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT f_autoincre_code(max(pd_code+0)) FROM t_base_product ");
		sql.append(" WHERE 1=1");
		sql.append(" AND companyid=:companyid");
		String code = namedParameterJdbcTemplate.queryForObject(sql.toString(), new MapSqlParameterSource().addValue("companyid", companyid), String.class);
		return code;
	}

	@Override
	public void saveProductInfo(List<T_Base_Product> productList) {
		StringBuffer sql = new StringBuffer("");
		sql.append("INSERT INTO t_base_product");
		sql.append("(pd_code,pd_no,pd_number,pd_name,pd_spell,pd_szg_code,pd_tp_code,pd_tp_upcode,pd_unit,");
		sql.append(" pd_bd_code,pd_season,pd_style,pd_year,pd_cost_price,pd_sell_cycle,pd_sell_price,pd_state,");
		sql.append(" pd_fabric,pd_bra,pd_change,pd_date,pd_returndate,pd_commission,pd_commission_rate,pd_present,pd_score,");
		sql.append(" pd_sp_code,pd_buy_cycle,pd_buy_price,pd_buy,pd_sort_price,pd_batch_price,pd_batch_price1,");
		sql.append(" pd_batch_price2,pd_batch_price3,pd_sign_price,pd_vip_price,pd_vip_sale,pd_gift,pd_point,pd_sale,");
		sql.append(" companyid)");
		sql.append(" VALUES");
		sql.append("(:pd_code,:pd_no,:pd_number,:pd_name,:pd_spell,:pd_szg_code,:pd_tp_code,:pd_tp_upcode,:pd_unit,");
		sql.append(" :pd_bd_code,:pd_season,:pd_style,:pd_year,:pd_cost_price,:pd_sell_cycle,:pd_sell_price,:pd_state,");
		sql.append(" :pd_fabric,:pd_bra,:pd_change,:pd_date,:pd_returndate,:pd_commission,:pd_commission_rate,:pd_present,:pd_score,");
		sql.append(" :pd_sp_code,:pd_buy_cycle,:pd_buy_price,:pd_buy,:pd_sort_price,:pd_batch_price,:pd_batch_price1,");
		sql.append(" :pd_batch_price2,:pd_batch_price3,:pd_sign_price,:pd_vip_price,:pd_vip_sale,:pd_gift,:pd_point,:pd_sale,");
		sql.append(" :companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(productList.toArray()));
		
		sql.setLength(0);
		sql.append("INSERT INTO t_base_product_info");
		sql.append(" (pd_code,pd_sysdate,pd_fill,pd_color,pd_size,pd_place,pd_in_fabric,pd_model,pd_safe,");
		sql.append(" pd_execute,pd_grade,pd_add_manager,pd_add_date,pd_price_name,pd_salesman_comm,pd_salesman_commrate,companyid)");
		sql.append(" VALUES");
		sql.append(" (:pd_code,:pd_sysdate,:pd_fill,:pd_color,:pd_size,:pd_place,:pd_in_fabric,:pd_model,:pd_safe,");
		sql.append(" :pd_execute,:pd_grade,:pd_add_manager,:pd_add_date,:pd_price_name,:pd_salesman_comm,:pd_salesman_commrate,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(productList.toArray()));
	}

	@Override
	public void saveProductColor(List<T_Base_Product_Color> productColorList) {
		StringBuffer sql = new StringBuffer("");
		sql.append("INSERT INTO t_base_product_color");
		sql.append(" (pdc_pd_code,pdc_cr_code,companyid)");
		sql.append(" VALUES(:pdc_pd_code,:pdc_cr_code,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(productColorList.toArray()));
	}

	@Override
	public void saveProductBr(List<T_Base_Product_Br> productBrList) {
		StringBuffer sql = new StringBuffer("");
		sql.append("INSERT INTO t_base_product_br");
		sql.append(" (pdb_pd_code,pdb_br_code,companyid)");
		sql.append(" VALUES(:pdb_pd_code,:pdb_br_code,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(productBrList.toArray()));
	}

	@Override
	public Boolean queryColorIfEmploy(Map<String, Object> param) {
		String codes = (String)param.get("codes");
		String[] code_arr = codes.split(",");
		boolean isHadValue = true;
		StringBuffer sql = new StringBuffer("");
		
		//库存
		sql.append(" select sd_id from t_stock_data where companyid=:companyid ");
		sql.append(" AND sd_cr_code IN (");
		for(int i=0;i<code_arr.length;i++){
			sql.append("'"+code_arr[i]+"'");
			if(i < code_arr.length-1){
				sql.append(",");
			}
		}
		sql.append(")");
		sql.append(" AND sd_pd_code = :pd_code limit 1 ");
		List sdidList = namedParameterJdbcTemplate.queryForList(sql.toString(), param);
		if(sdidList != null && sdidList.size() > 0){
			isHadValue = false;
			return isHadValue;
		}
		
		//进货
		sql.setLength(0);
		sql.append(" select odl_id from t_buy_orderlist where companyid=:companyid ");
		sql.append(" AND odl_cr_code IN (");
		for(int i=0;i<code_arr.length;i++){
			sql.append("'"+code_arr[i]+"'");
			if(i < code_arr.length-1){
				sql.append(",");
			}
		}
		sql.append(")");
		sql.append(" AND odl_pd_code = :pd_code limit 1 ");
		List odlidList = namedParameterJdbcTemplate.queryForList(sql.toString(), param);
		if(odlidList != null && odlidList.size() > 0){
			isHadValue = false;
			return isHadValue;
		}
		
		//进货临时表
		sql.setLength(0);
		sql.append(" select odl_id as odlt_id from t_buy_orderlist_temp where companyid=:companyid ");
		sql.append(" AND odl_cr_code IN (");
		for(int i=0;i<code_arr.length;i++){
			sql.append("'"+code_arr[i]+"'");
			if(i < code_arr.length-1){
				sql.append(",");
			}
		}
		sql.append(")");
		sql.append(" AND odl_pd_code = :pd_code limit 1 ");
		List odltidList = namedParameterJdbcTemplate.queryForList(sql.toString(), param);
		if(odltidList != null && odltidList.size() > 0){
			isHadValue = false;
			return isHadValue;
		}
		
		return isHadValue;
	}

	@Override
	public Boolean querySizeIfEmploy(Map<String, Object> param) {
		List<T_Base_SizeList> sizeList = (List<T_Base_SizeList>)param.get("sizeList");
		boolean isHadValue = true;
		if(sizeList == null || sizeList.size() <= 0){
			return isHadValue;
		}
		StringBuffer sql = new StringBuffer("");
		
		//库存
		sql.append(" select sd_id from t_stock_data where companyid=:companyid ");
		sql.append(" AND sd_sz_code IN (");
		for(int i=0;i<sizeList.size();i++){
			sql.append("'"+sizeList.get(i).getSzl_sz_code()+"'");
			if(i < sizeList.size()-1){
				sql.append(",");
			}
		}
		sql.append(")");
		sql.append(" AND sd_pd_code = :pd_code ");
		sql.append(" limit 1 ");
		List sdidList = namedParameterJdbcTemplate.queryForList(sql.toString(), param);
		if(sdidList != null && sdidList.size() > 0){
			isHadValue = false;
			return isHadValue;
		}
				
		//进货
		sql.setLength(0);
		sql.append(" select odl_id from t_buy_orderlist where companyid=:companyid ");
		sql.append(" AND odl_szg_code = :codes");
		sql.append(" AND odl_pd_code = :pd_code ");
		sql.append(" limit 1 ");
		List odlidList = namedParameterJdbcTemplate.queryForList(sql.toString(), param);
		if(odlidList != null && odlidList.size() > 0){
			isHadValue = false;
			return isHadValue;
		}
				
		//进货临时表
		sql.setLength(0);
		sql.append(" select odl_id as odlt_id from t_buy_orderlist_temp where companyid=:companyid ");
		sql.append(" AND odl_szg_code = :codes");
		sql.append(" AND odl_pd_code = :pd_code ");
		sql.append(" limit 1 ");
		List odltidList = namedParameterJdbcTemplate.queryForList(sql.toString(), param);
		if(odltidList != null && odltidList.size() > 0){
			isHadValue = false;
			return isHadValue;
		}
				
		return isHadValue;
	}

	@Override
	public Boolean queryBraIfEmploy(Map<String, Object> param) {
		String codes = (String)param.get("codes");
		String[] code_arr = codes.split(",");
		boolean isHadValue = true;
		StringBuffer sql = new StringBuffer("");
		
		//库存
		sql.append(" select sd_id from t_stock_data where companyid=:companyid ");
		sql.append(" AND sd_br_code IN (");
		for(int i=0;i<code_arr.length;i++){
			sql.append("'"+code_arr[i]+"'");
			if(i < code_arr.length-1){
				sql.append(",");
			}
		}
		sql.append(")");
		sql.append(" AND sd_pd_code = :pd_code limit 1 ");
		List sdidList = namedParameterJdbcTemplate.queryForList(sql.toString(), param);
		if(sdidList != null && sdidList.size() > 0){
			isHadValue = false;
			return isHadValue;
		}
		
		//进货
		sql.setLength(0);
		sql.append(" select odl_id from t_buy_orderlist where companyid=:companyid ");
		sql.append(" AND odl_br_code IN (");
		for(int i=0;i<code_arr.length;i++){
			sql.append("'"+code_arr[i]+"'");
			if(i < code_arr.length-1){
				sql.append(",");
			}
		}
		sql.append(")");
		sql.append(" AND odl_pd_code = :pd_code limit 1 ");
		List odlidList = namedParameterJdbcTemplate.queryForList(sql.toString(), param);
		if(odlidList != null && odlidList.size() > 0){
			isHadValue = false;
			return isHadValue;
		}
		
		//进货临时表
		sql.setLength(0);
		sql.append(" select odl_id as odlt_id from t_buy_orderlist_temp where companyid=:companyid ");
		sql.append(" AND odl_br_code IN (");
		for(int i=0;i<code_arr.length;i++){
			sql.append("'"+code_arr[i]+"'");
			if(i < code_arr.length-1){
				sql.append(",");
			}
		}
		sql.append(")");
		sql.append(" AND odl_pd_code = :pd_code limit 1 ");
		List odltidList = namedParameterJdbcTemplate.queryForList(sql.toString(), param);
		if(odltidList != null && odltidList.size() > 0){
			isHadValue = false;
			return isHadValue;
		}
		
		return isHadValue;
	}

	@Override
	public Boolean queryProductIfEmploy(Map<String, Object> param) {
		boolean isHadValue = true;
		StringBuffer sql = new StringBuffer("");
		//库存
		sql.append(" select sd_id from t_stock_data where companyid=:companyid ");
		sql.append(" AND sd_pd_code = :pd_code limit 1 ");
		List sdidList = namedParameterJdbcTemplate.queryForList(sql.toString(), param);
		if(sdidList != null && sdidList.size() > 0){
			isHadValue = false;
			return isHadValue;
		}
		//进货
		sql.setLength(0);
		sql.append(" select odl_id from t_buy_orderlist where companyid=:companyid ");
		sql.append(" AND odl_pd_code = :pd_code limit 1 ");
		List odlidList = namedParameterJdbcTemplate.queryForList(sql.toString(), param);
		if(odlidList != null && odlidList.size() > 0){
			isHadValue = false;
			return isHadValue;
		}
		//进货临时表
		sql.setLength(0);
		sql.append(" select odl_id as odlt_id from t_buy_orderlist_temp where companyid=:companyid ");
		sql.append(" AND odl_pd_code = :pd_code limit 1 ");
		List odltidList = namedParameterJdbcTemplate.queryForList(sql.toString(), param);
		if(odltidList != null && odltidList.size() > 0){
			isHadValue = false;
			return isHadValue;
		}
		return isHadValue;
	}

	@Override
	public T_Base_Product queryByID(Integer pd_id) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" select t.pd_id,t.pd_code,pd_number,pd_no,pd_name,pd_spell,pd_szg_code,pd_tp_code,pd_tp_upcode,pd_bd_code,pd_unit,pd_cost_price,pd_bd_code,pd_season,");
		sql.append(" pd_style,pd_year,pd_cost_price,pd_sell_cycle,pd_sell_price,pd_state,pd_fabric,pd_bra,pd_change,pd_date,pd_returndate,pd_commission,pd_commission_rate,");
		sql.append(" pd_present,pd_score,pd_sign_price,pd_vip_price,pd_vip_sale,pd_gift,pd_point,pd_sale,pd_sp_code,pd_buy_cycle,pd_buy_price,pd_buy_date,pd_buy,");
		sql.append(" pd_sort_price,pd_batch_price,pd_batch_price1,pd_batch_price2,pd_batch_price3,pd_sysdate,pd_fill,pd_color,pd_size,pd_place,pd_in_fabric,pd_model,");
		sql.append(" pd_safe,pd_execute,pd_grade,GROUP_CONCAT(pdb.pdb_br_code) AS pdb_br_codes,GROUP_CONCAT(br.br_name) AS pdb_br_name,pd_price_name,pd_salesman_comm,pd_salesman_commrate,pd_gbcode,pd_wash_explain,");
		sql.append("(select bd_name from t_base_brand bd where bd.bd_code=t.pd_bd_code and bd.companyid=t.companyid LIMIT 1) as pd_bd_name,");//品牌
		sql.append("(select tp_name from t_base_type tp where tp.tp_code=t.pd_tp_code and tp.companyid=t.companyid LIMIT 1) as pd_tp_name,");//类别
		sql.append("(select szg_name from t_base_size_group szg where szg.szg_code=t.pd_szg_code and szg.companyid=t.companyid LIMIT 1) as pd_szg_name,");//尺码组
		sql.append("(select sp_name from t_buy_supply sp where sp.sp_code=t.pd_sp_code and sp.companyid=t.companyid LIMIT 1) as pd_sp_name");//供货商
		sql.append(" from t_base_product t");
		sql.append(" JOIN t_base_product_info pdi ON pdi.pd_code=t.pd_code AND pdi.companyid=t.companyid");
		sql.append(" LEFT JOIN t_base_product_br pdb ON pdb.pdb_pd_code=t.pd_code AND pdb.companyid=t.companyid");
		sql.append(" LEFT JOIN t_base_bra br ON br.br_code=pdb.pdb_br_code AND br.companyid=pdb.companyid");
		sql.append(" where 1 = 1");
		sql.append(" and t.pd_id = :pd_id");
		sql.append(" LIMIT 1");
		try{
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), new MapSqlParameterSource().addValue("pd_id", pd_id),new BeanPropertyRowMapper<>(T_Base_Product.class));
		}catch(Exception e){
			return null;
		}
	}

	@Override
	public List<T_Base_Color> colorByProduct(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" select color.cr_code,color.cr_name ");
		sql.append(" from t_base_product_color t");
		sql.append(" left join t_base_color color on t.pdc_cr_code=color.cr_code and t.companyid=color.companyid");
		sql.append(" where 1 = 1");
		sql.append(" and t.companyid=:companyid");
		sql.append(" and t.pdc_pd_code=:pd_code");
		sql.append(" order by t.pdc_id desc ");
		List<T_Base_Color> list = namedParameterJdbcTemplate.query(sql.toString(), param, new BeanPropertyRowMapper<>(T_Base_Color.class));
		return list;
	}

	@Override
	public List<T_Base_Bra> braByProduct(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" select bra.br_code,bra.br_name ");
		sql.append(" from t_base_product_br t");
		sql.append(" left join t_base_bra bra on t.pdb_br_code=bra.br_code and t.companyid=bra.companyid");
		sql.append(" where 1 = 1");
		sql.append(" and t.companyid=:companyid");
		sql.append(" and t.pdb_pd_code=:pd_code");
		sql.append(" order by t.pdb_id desc ");
		List<T_Base_Bra> list = namedParameterJdbcTemplate.query(sql.toString(), param, new BeanPropertyRowMapper<>(T_Base_Bra.class));
		return list;
	}

	@Override
	public List<T_Base_Product_Img> productimglist(Map<String, Object> param) {
		Object cr_code = param.get("cr_code");
		StringBuffer sql = new StringBuffer("");
		sql.append(" select pdm_id,pdm_pd_code,pdm_cr_code,pdm_all_path,pdm_img_path,color.cr_name ");
		sql.append(" from t_base_product_img t");
		sql.append(" left join t_base_color color on t.pdm_cr_code=color.cr_code and t.companyid=color.companyid ");
		sql.append(" where 1 = 1");
		sql.append(" and t.companyid=:companyid");
		sql.append(" and t.pdm_state='0'");
		sql.append(" and t.pdm_pd_code=:pd_code");
		if(null != cr_code && !"".equals(cr_code)){
			sql.append(" AND t.pdm_cr_code=:cr_code");
	    }
		sql.append(" order by t.pdm_id desc ");
		List<T_Base_Product_Img> list = namedParameterJdbcTemplate.query(sql.toString(), param, new BeanPropertyRowMapper<>(T_Base_Product_Img.class));
		return list;
	}

	@Override
	public List<String> queryBarCodeIfExit(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		String bc_id = (String)param.get("bc_id");
		String bc_barcode = (String)param.get("bc_barcode");
		String[] bc_barcodes = null;
		String[] bc_ids = null;
		if(bc_barcode != null && !"".equals(bc_barcode)){
			bc_barcodes = bc_barcode.split(",");
		}
		if(bc_id != null && !"".equals(bc_id)){
			bc_ids = bc_id.split(",");
		}
		
		StringBuffer sql = new StringBuffer("");
		sql.append(" select bc_barcode from t_base_barcode t where companyid = '"+companyid+"'");
		if(bc_barcodes!=null){
			for(int i=0;i<bc_barcodes.length;i++){
				if(bc_id != null && !"".equals(bc_id) && bc_ids != null){
					if(i==0){
						sql.append(" AND ( (bc_barcode = '"+bc_barcodes[i]+"' AND bc_id != '"+bc_ids[i]+"')");
					}else {
						sql.append(" OR (bc_barcode = '"+bc_barcodes[i]+"' AND bc_id != '"+bc_ids[i]+"')");
					}
				} else {
					if(i==0){
						sql.append(" AND ( bc_barcode = '"+bc_barcodes[i]+"' ");
					}else {
						sql.append(" OR bc_barcode = '"+bc_barcodes[i]+"' ");
					}
				}
				if(i==bc_barcodes.length-1){
					sql.append(" )");
				}
			}
		}
		List<String> list  = namedParameterJdbcTemplate.getJdbcOperations().queryForList(sql.toString(),String.class);
		return list;
	}

	@Override
	public void saveOneBarcode(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer("");
		sql.append("INSERT INTO t_base_barcode");
		sql.append("(bc_pd_code,bc_pd_no,bc_size,bc_bra,bc_color,bc_subcode,bc_sys_barcode,bc_barcode,companyid)");
		sql.append(" VALUES ");
		sql.append("(:pd_code,:pd_no,:sz_code,:br_code,:cr_code,:bc_subcode,:bc_sys_barcode,:bc_barcode,:companyid)");
		namedParameterJdbcTemplate.update(sql.toString(), param);
	}

	@Override
	public void saveAllBarcode(Map<String, Object> param) {
		String bc_barcode = (String)param.get("bc_barcode");
		String pd_no = (String)param.get("pd_no");
		String pd_code = (String)param.get("pd_code");
		String cr_code = (String)param.get("cr_code");
		String sz_code = (String)param.get("sz_code");
		String br_code = (String)param.get("br_code");
		String bc_subcode = (String)param.get("bc_subcode");
		String bc_sys_barcode = (String)param.get("bc_sys_barcode");
		
		Integer companyid = (Integer)param.get("companyid");
		String[] bc_barcode_arr = bc_barcode.split(",");
		String[] pd_no_arr = pd_no.split(",");
		String[] pd_code_arr = pd_code.split(",");
		String[] cr_code_arr = cr_code.split(",");
		String[] sz_code_arr = sz_code.split(",");
		String[] br_code_arr = br_code.split(",");
		String[] bc_subcode_arr = bc_subcode.split(",");
		String[] bc_sys_barcode_arr = bc_sys_barcode.split(",");
		
		List<T_Base_Barcode> bcList = new ArrayList<T_Base_Barcode>();
		T_Base_Barcode bc = null;
		for(int i=0;i<bc_barcode_arr.length;i++){
			bc = new T_Base_Barcode();
			bc.setBc_pd_code(pd_code_arr[i]);
			bc.setBc_pd_no(pd_no_arr[i]);
			bc.setBc_size(sz_code_arr[i]);
			if("0".equals(br_code_arr[i])){
				bc.setBc_bra("");
			}else {
				bc.setBc_bra(br_code_arr[i]);
			}
			bc.setBc_color(cr_code_arr[i]);
			bc.setBc_subcode(bc_subcode_arr[i]);
			bc.setBc_sys_barcode(bc_sys_barcode_arr[i]);
			bc.setBc_barcode(bc_barcode_arr[i]);
			bc.setCompanyid(companyid);
			bcList.add(bc);
		}
		StringBuffer sql = new StringBuffer("");
		sql.append("INSERT INTO t_base_barcode");
		sql.append("(bc_pd_code,bc_pd_no,bc_size,bc_bra,bc_color,bc_subcode,bc_sys_barcode,bc_barcode,companyid)");
		sql.append(" VALUES ");
		sql.append("(:bc_pd_code,:bc_pd_no,:bc_size,:bc_bra,:bc_color,:bc_subcode,:bc_sys_barcode,:bc_barcode,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(bcList.toArray()));
	}

	@Override
	public void updateOneBarcode(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer("");
		sql.append("update t_base_barcode set bc_barcode=:bc_barcode ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND bc_id = :bc_id ");
		sql.append(" AND companyid = :companyid ");
		namedParameterJdbcTemplate.update(sql.toString(), param);
	}

	@Override
	public void updateAllBarcode(Map<String, Object> param) {
		String bc_id = (String)param.get("bc_id");
		String bc_barcode = (String)param.get("bc_barcode");
		Integer companyid = (Integer)param.get("companyid");
		String[] bc_id_arr = bc_id.split(",");
		String[] bc_barcode_arr = bc_barcode.split(",");
		List<T_Base_Barcode> bcList = new ArrayList<T_Base_Barcode>();
		T_Base_Barcode bc = null;
		for(int i=0;i<bc_id_arr.length;i++){
			bc = new T_Base_Barcode();
			bc.setBc_id(Integer.parseInt(bc_id_arr[i]));
			bc.setBc_barcode(bc_barcode_arr[i]);
			bc.setCompanyid(companyid);
			bcList.add(bc);
		}
		StringBuffer sql = new StringBuffer("");
		sql.append("update t_base_barcode set bc_barcode=:bc_barcode ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND bc_id = :bc_id ");
		sql.append(" AND companyid = :companyid ");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(bcList.toArray()));
	}

	@Override
	public void saveBarcode(List<T_Base_Barcode> t_Base_Barcodes) {
		StringBuffer sql = new StringBuffer("");
		sql.append("INSERT INTO t_base_barcode");
		sql.append("(bc_pd_code,bc_pd_no,bc_size,bc_bra,bc_color,bc_subcode,bc_sys_barcode,bc_barcode,companyid)");
		sql.append(" VALUES ");
		sql.append("(:bc_pd_code,:bc_pd_no,:bc_size,:bc_bra,:bc_color,:bc_subcode,:bc_sys_barcode,:bc_barcode,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(t_Base_Barcodes.toArray()));
	}

	@Override
	public void deleteBarcode(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer("");
		sql.append("DELETE FROM t_base_barcode ");
		sql.append(" WHERE bc_id = :bc_id ");
		sql.append(" AND companyid = :companyid ");
		namedParameterJdbcTemplate.update(sql.toString(), param);
	}

	@Override
	public void delBulkBarcode(List<T_Base_Barcode> t_Base_Barcodes) {
		StringBuffer sql = new StringBuffer("");
		sql.append("DELETE FROM t_base_barcode ");
		sql.append(" WHERE bc_id = :bc_id ");
		sql.append(" AND companyid = :companyid ");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(t_Base_Barcodes.toArray()));
	}

	@Override
	public void delAllBarcode(Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append("DELETE FROM t_base_barcode ");
		sql.append(" WHERE companyid = :companyid ");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("companyid", companyid));
	}

	@Override
	public T_Base_Barcode loadBarcode(String barcode, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT bc_id,bc_pd_code,bc_pd_no,bc_size,IFNULL(bc_bra,'') AS bc_bra,bc_color,bc_subcode,bc_barcode,companyid,");
		sql.append(" (SELECT cr_name FROM t_base_color cr WHERE cr_code = bc_color AND cr.companyid = t.companyid LIMIT 1) AS bc_colorname,");
		sql.append(" (SELECT sz_name FROM t_base_size sz WHERE sz_code = bc_size AND sz.companyid = t.companyid LIMIT 1) AS bc_sizename,");
		sql.append(" (SELECT br_name FROM t_base_bra br WHERE br_code = bc_bra AND br.companyid = t.companyid LIMIT 1) AS bc_braname");
		sql.append(" FROM t_base_barcode t");
		sql.append(" WHERE 1=1");
		sql.append(" AND bc_barcode = :bc_barcode"); 
		sql.append(" AND companyid = :companyid");
		sql.append(" LIMIT 1");
		try{
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new MapSqlParameterSource().addValue("bc_barcode", barcode).addValue("companyid", companyid),
					new BeanPropertyRowMapper<>(T_Base_Barcode.class));
		}catch(Exception e){
			return null;
		}
	}
	
	@Override
	public void saveimg(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer("");
		sql.append("INSERT INTO t_base_product_img");
		sql.append("(pdm_pd_code,pdm_cr_code,pdm_img_name,pdm_all_path,pdm_img_path,companyid)");
		sql.append(" VALUES ");
		sql.append("(:pd_code,:cr_code,:img_name,:all_path,:img_path,:companyid)");
		namedParameterJdbcTemplate.update(sql.toString(), param);
	}

	@Override
	public void deleteimg(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer("");
		sql.append("DELETE FROM t_base_product_img WHERE pdm_id = :pdm_id AND companyid = :companyid ");
		namedParameterJdbcTemplate.update(sql.toString(), param);
	}

	@Override
	public List<T_Base_Product> list_server(Map<String, Object> params) {
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT t.pd_id,t.pd_code,pd_no,pd_name,pd_spell,pd_tp_code,pd_bd_code,");
		sql.append("pd_sell_price,pd_unit,pd_season,pd_year,pd_style,");
		sql.append("(select bd_name from t_base_brand bd where bd.bd_code=t.pd_bd_code and bd.companyid=t.companyid LIMIT 1) as pd_bd_name,");//品牌
		sql.append("(select tp_name from t_base_type tp where tp.tp_code=t.pd_tp_code and tp.companyid=t.companyid LIMIT 1) as pd_tp_name,");//类别
		sql.append(" (SELECT pdm_img_path FROM t_base_product_img pdm WHERE pdm_pd_code = t.pd_code AND pdm.companyid = t.companyid LIMIT 1) AS pdm_img_path");
		sql.append(" FROM t_base_product t");
		sql.append(" WHERE 1 = 1");
		if (StringUtil.isNotEmpty(params.get("searchContent"))) {
			sql.append(" AND (INSTR(t.pd_name,:searchContent)>0 OR INSTR(t.pd_spell,:searchContent)>0 OR INSTR(t.pd_no,:searchContent)>0)");
	    }
		sql.append(" AND t.companyid=:companyid");
		sql.append(" ORDER BY pd_id DESC ");
		sql.append(" LIMIT :start,:end");
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Base_Product.class));
	}
	
	@Override
	public T_Base_Product loadByBarcode(String barcode, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT t.pd_id,t.pd_code,pd_no,pd_name,pd_spell,pd_tp_code,pd_bd_code,");
		sql.append("pd_sell_price,pd_unit,pd_season,pd_year,pd_style,");
		sql.append("(select bd_name from t_base_brand bd where bd.bd_code=t.pd_bd_code and bd.companyid=t.companyid LIMIT 1) as pd_bd_name,");//品牌
		sql.append("(select tp_name from t_base_type tp where tp.tp_code=t.pd_tp_code and tp.companyid=t.companyid LIMIT 1) as pd_tp_name,");//类别
		sql.append(" (SELECT pdm_img_path FROM t_base_product_img pdm WHERE pdm_pd_code = t.pd_code AND pdm.companyid = t.companyid LIMIT 1) AS pdm_img_path");
		sql.append(" FROM t_base_product t");
		sql.append(" JOIN t_base_barcode bc ON bc_pd_code = t.pd_code AND bc.companyid = t.companyid");
		sql.append(" WHERE 1 = 1");
		sql.append(" AND bc_barcode = :bc_barcode");
		sql.append(" AND t.companyid=:companyid");
		sql.append(" LIMIT 1");
		try{
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new MapSqlParameterSource().addValue("bc_barcode", barcode).addValue("companyid", companyid),
					new BeanPropertyRowMapper<>(T_Base_Product.class));
		}catch(Exception e){
			return null;
		}
	}

	@Override
	public Map<String, List> getCheckImportProduct(T_Sys_User user) {
		Integer companyid = user.getCompanyid();
		Map<String, List> checkImportProduct = new HashMap<String, List>();
		StringBuffer sql = new StringBuffer("");
		//查询当前商家的品牌信息
		List<T_Base_Brand> brandList = new ArrayList<T_Base_Brand>();
		sql.append("SELECT bd_code,bd_name FROM t_base_brand ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND companyid = :companyid");
		brandList = namedParameterJdbcTemplate.query(sql.toString(),new MapSqlParameterSource().addValue("companyid", companyid),
				new BeanPropertyRowMapper<>(T_Base_Brand.class));
		checkImportProduct.put("brandList", brandList);
		
		//查询当前商家供应商
		List<T_Buy_Supply> supplieList = new ArrayList<T_Buy_Supply>();
		sql.setLength(0);
		sql.append("SELECT sp_code,sp_name FROM t_buy_supply ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND companyid = :companyid");
		supplieList = namedParameterJdbcTemplate.query(sql.toString(),new MapSqlParameterSource().addValue("companyid", companyid),
				new BeanPropertyRowMapper<>(T_Buy_Supply.class));
		checkImportProduct.put("supplieList", supplieList);
		
		//查询当前商家商品分类
		List<T_Base_Type> typeList = new ArrayList<T_Base_Type>();
		sql.setLength(0);
		sql.append("SELECT tp_code,tp_upcode,tp_name FROM t_base_type ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND companyid = :companyid");
		typeList = namedParameterJdbcTemplate.query(sql.toString(),new MapSqlParameterSource().addValue("companyid", companyid),
				new BeanPropertyRowMapper<>(T_Base_Type.class));
		checkImportProduct.put("typeList", typeList);
		
		//查询基础字典表，包含 季节、单位、面料、款式、价格特性 等信息
		List<T_Base_DictList> dictLists = new ArrayList<T_Base_DictList>();
		List<T_Base_DictList> seasonList = new ArrayList<T_Base_DictList>();//季节
		List<T_Base_DictList> unitList = new ArrayList<T_Base_DictList>();//单位
		List<T_Base_DictList> stuffList = new ArrayList<T_Base_DictList>();//面料
		List<T_Base_DictList> styleList = new ArrayList<T_Base_DictList>();//款式
		List<T_Base_DictList> priceList = new ArrayList<T_Base_DictList>();//价格特性
		sql.setLength(0);
		sql.append("SELECT dtl_code,dtl_upcode,dtl_name FROM t_base_dictlist ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND companyid = :companyid");
		dictLists = namedParameterJdbcTemplate.query(sql.toString(),new MapSqlParameterSource().addValue("companyid", companyid),
				new BeanPropertyRowMapper<>(T_Base_DictList.class));
		if(dictLists!=null && dictLists.size()>0){
			for(int i=0;i<dictLists.size();i++){
				String dtl_upcode = dictLists.get(i).getDtl_upcode();
				if(CommonUtil.SEASON.equals(dtl_upcode)){//季节
					seasonList.add(dictLists.get(i));
					continue;
				}
				if(CommonUtil.UNITNAME.equals(dtl_upcode)){//单位
					unitList.add(dictLists.get(i));
					continue;
				}
				if(CommonUtil.STUFF.equals(dtl_upcode)){//面料
					stuffList.add(dictLists.get(i));
					continue;
				}
				if(CommonUtil.STYLE.equals(dtl_upcode)){//款式
					styleList.add(dictLists.get(i));
					continue;
				}
				if(CommonUtil.PRICE.equals(dtl_upcode)){//价格特性
					priceList.add(dictLists.get(i));
					continue;
				}
			}
		}
		checkImportProduct.put("seasonList", seasonList);
		checkImportProduct.put("unitList", unitList);
		checkImportProduct.put("stuffList", stuffList);
		checkImportProduct.put("styleList", styleList);
		checkImportProduct.put("priceList", priceList);
		
		//查询尺码组
		List<T_Base_Size_Group> sizeGroupList = new ArrayList<T_Base_Size_Group>();
		sql.setLength(0);
		sql.append("SELECT szg_code,szg_name FROM t_base_size_group ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND companyid = :companyid");
		sizeGroupList = namedParameterJdbcTemplate.query(sql.toString(),new MapSqlParameterSource().addValue("companyid", companyid),
				new BeanPropertyRowMapper<>(T_Base_Size_Group.class));
		checkImportProduct.put("sizeGroupList", sizeGroupList);
		
		//查询颜色
		List<T_Base_Color> colorList = new ArrayList<T_Base_Color>();
		sql.setLength(0);
		sql.append("SELECT cr_code,cr_name FROM t_base_color ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND companyid = :companyid");
		colorList = namedParameterJdbcTemplate.query(sql.toString(),new MapSqlParameterSource().addValue("companyid", companyid),
				new BeanPropertyRowMapper<>(T_Base_Color.class));
		checkImportProduct.put("colorList", colorList);
		
		//查询杯型
		List<T_Base_Bra> braList = new ArrayList<T_Base_Bra>();
		sql.setLength(0);
		sql.append("SELECT br_code,br_code FROM t_base_bra ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND companyid = :companyid");
		braList = namedParameterJdbcTemplate.query(sql.toString(),new MapSqlParameterSource().addValue("companyid", companyid),
				new BeanPropertyRowMapper<>(T_Base_Bra.class));
		checkImportProduct.put("braList", braList);
		
		//查询商品货号
		List<String> pdNoList = new ArrayList<String>();
		sql.setLength(0);
		sql.append("SELECT pd_no FROM t_base_product ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND companyid = :companyid");
		pdNoList = namedParameterJdbcTemplate.query(sql.toString(),new MapSqlParameterSource().addValue("companyid", companyid),
				new BeanPropertyRowMapper<>(String.class));
		checkImportProduct.put("pdNoList", pdNoList);
		
		return checkImportProduct;
	}

	@Override
	public Map<String, Object> getCheckImportBarcode(T_Sys_User user) {
		Integer companyid = user.getCompanyid();
		Map<String, Object> checkImportBarcode = new HashMap<String, Object>();
		StringBuffer sql = new StringBuffer("");
		
		//查询尺码组对应尺码
		List<T_Base_SizeList> sizeLists = new ArrayList<T_Base_SizeList>();
		sql.append(" SELECT szl_szg_code,szl_sz_code,sz_name ");
		sql.append(" FROM t_base_sizelist szl");
		sql.append(" JOIN t_base_size sz ");
		sql.append(" ON sz.sz_code=szl.szl_sz_code AND sz.companyid=szl.companyid");
		sql.append(" WHERE szl.companyid=:companyid ");
		sql.append(" ORDER BY szl_szg_code ");
		sizeLists = namedParameterJdbcTemplate.query(sql.toString(),new MapSqlParameterSource().addValue("companyid", companyid),
				new BeanPropertyRowMapper<>(T_Base_SizeList.class));
		Map<String,Object> sizeGroupMap=new HashMap<String, Object>();
		if(sizeLists != null && sizeLists.size()>0){
			for(int i=0;i<sizeLists.size();i++){
				Map<String,String> sizeMap=null;
				String szg_code = sizeLists.get(i).getSzl_szg_code();
				if (sizeGroupMap.get(szg_code)==null){
					sizeMap=new HashMap<String, String>();
					sizeGroupMap.put(szg_code,new HashMap<String, String>());
				}
				sizeMap=(Map<String,String>)sizeGroupMap.get(szg_code);
				sizeMap.put(sizeLists.get(i).getSz_name(), sizeLists.get(i).getSzl_sz_code());
			}
		}
		
		//查询商品货号
		List<T_Base_Product> products = new ArrayList<T_Base_Product>();
		sql.setLength(0);
		sql.append("SELECT pd_code,pd_no,pd_szg_code FROM t_base_product WHERE 1=1 ");
		sql.append(" AND companyid = :companyid ");
		products = namedParameterJdbcTemplate.query(sql.toString(),new MapSqlParameterSource().addValue("companyid", companyid),
				new BeanPropertyRowMapper<>(T_Base_Product.class));
		Map<String,Map<String,Object>> productMap = new HashMap<String, Map<String,Object>>();
		if(products != null && products.size()>0){
			for(int i=0;i<products.size();i++){
				Map<String,Object> info=new HashMap<String,Object>();
				info.put("pdCode", products.get(i).getPd_code());
				info.put("brMap",new HashMap<String,String>());
				info.put("colorMap",new HashMap<String,String>());
				String szg_code=products.get(i).getPd_szg_code();
				if (sizeGroupMap.get(szg_code)!=null){
					Map<String,String> sizeMap=(Map<String,String>)sizeGroupMap.get(szg_code);
					info.put("sizeMap",sizeMap);	
				}else{
					info.put("sizeMap",new HashMap<String,String>());	
				}
				productMap.put(products.get(i).getPd_no().toLowerCase(),info);	
			}
		}
		
		//查询杯型
		List<T_Base_Product_Br> product_brs = new ArrayList<T_Base_Product_Br>();
		sql.setLength(0);
		sql.append(" select pdb_pd_code,pd_no,pdb_br_code,br_name");
		sql.append(" from t_base_product_br pdb ");
		sql.append(" join t_base_bra br ");
		sql.append(" on br.br_code=pdb.pdb_br_code and br.companyid=pdb.companyid ");
		sql.append(" join t_base_product pd ");
		sql.append(" on pd.pd_code=pdb.pdb_pd_code and pd.companyid=pdb.companyid ");
		sql.append(" where pdb.companyid=:companyid");
		sql.append(" order by pdb_pd_code");
		product_brs = namedParameterJdbcTemplate.query(sql.toString(),new MapSqlParameterSource().addValue("companyid", companyid),
				new BeanPropertyRowMapper<>(T_Base_Product_Br.class));
		if(product_brs != null && product_brs.size()>0){
			for(int i=0;i<product_brs.size();i++){
				String pd_no = product_brs.get(i).getPd_no().toLowerCase();
				Map<String,Object> infoMap=productMap.get(pd_no);
				if(infoMap!=null){
					Map<String,String> brMap=(Map<String,String>)infoMap.get("brMap");
					if(brMap!=null){
						brMap.put(product_brs.get(i).getBr_name(), product_brs.get(i).getPdb_br_code());
					}
				}
			}
		}
		
		//查询颜色
		List<T_Base_Product_Color> product_colors = new ArrayList<T_Base_Product_Color>();
		sql.setLength(0);
		sql.append(" select pdc_pd_code,pd_no,pdc_cr_code,cr_name ");
		sql.append(" from t_base_product_color pdc");
		sql.append(" join t_base_color cr ");
		sql.append(" on cr.cr_code=pdc.pdc_cr_code and cr.companyid=pdc.companyid ");
		sql.append(" join t_base_product pd ");
		sql.append(" on pd.pd_code=pdc.pdc_pd_code and pd.companyid=pdc.companyid ");
		sql.append(" where pdc.companyid = :companyid ");
		sql.append(" order by pdc_pd_code");
		product_colors = namedParameterJdbcTemplate.query(sql.toString(),new MapSqlParameterSource().addValue("companyid", companyid),
				new BeanPropertyRowMapper<>(T_Base_Product_Color.class));
		if(product_colors != null && product_colors.size()>0){
			for(int i=0;i<product_colors.size();i++){
				String pd_no = product_colors.get(i).getPd_no().toLowerCase();
				Map<String,Object> infoMap=productMap.get(pd_no);
				if(infoMap!=null){
					Map<String,String> colorMap=(Map<String,String>)infoMap.get("colorMap");
					if(colorMap!=null){
						colorMap.put(product_colors.get(i).getCr_name(), product_colors.get(i).getPdc_cr_code());
					}
				}
			}
		}
		checkImportBarcode.put("productMap", productMap);
			
		//查询条码
		List<String> barcodes = new ArrayList<String>();
		sql.setLength(0);
		sql.append("SELECT bc_barcode FROM t_base_barcode WHERE 1=1 ");
		sql.append(" AND companyid = :companyid ");
		barcodes = namedParameterJdbcTemplate.queryForList(sql.toString(),new MapSqlParameterSource().addValue("companyid", companyid),String.class);
		List<Map<String, String>> barcodeList = new ArrayList<Map<String, String>>();
		Map<String, String> barcodeMap = new HashMap<String, String>();
		if(barcodes != null && barcodes.size()>0){
			for(int i=0;i<barcodes.size();i++){
				if(barcodes.get(i) != null && !"".equals(barcodes.get(i))){
					barcodeMap.put(barcodes.get(i).toUpperCase(),barcodes.get(i).toUpperCase());
				}
			}
		}
		barcodeList.add(0,barcodeMap);
		checkImportBarcode.put("barcodeList", barcodeList);
		return checkImportBarcode;
	}

	@Override
	public List<T_Base_Product_Assist> assist_template(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer("");
		sql.append("select pda_id,pda_name ");
		sql.append(" from t_base_product_assist t ");
		sql.append(" where 1=1 ");
		sql.append(" and t.companyid=:companyid ");
		sql.append(" order by pda_id desc ");
		return namedParameterJdbcTemplate.query(sql.toString(), param, new BeanPropertyRowMapper<>(T_Base_Product_Assist.class));
	}

	@Override
	public T_Base_Product_Assist get_assist_byid(Map<String, Object> param) {
		Object pda_id = param.get("pda_id");
		StringBuffer sql = new StringBuffer("");
		sql.append("select * from t_base_product_assist t ");
		sql.append(" where 1=1 ");
		sql.append(" and t.pda_id=:pda_id ");
		sql.append(" LIMIT 1");
		try{
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new MapSqlParameterSource().addValue("pda_id", pda_id),new BeanPropertyRowMapper<>(T_Base_Product_Assist.class));
		}catch(Exception e){
			return null;
		}
	}

	@Override
	public void save_product_assist(T_Base_Product_Assist product_Assist) {
		StringBuffer sql = new StringBuffer("");
		sql.append("INSERT INTO t_base_product_assist");
		sql.append("(pda_name,pda_fill,pda_color,pda_size,pda_place,pda_model,pda_safe,pda_execute,pda_grade,");
		sql.append(" pda_price_name,pda_salesman_comm,pda_salesman_commrate,pda_gbcode,pda_wash_explain,pda_sale,pda_change,pda_gift,");
		sql.append(" pda_vip_sale,pda_present,pda_buy,pda_point,pda_batch_price1,pda_batch_price2,pda_batch_price3,companyid)");
		sql.append(" VALUES");
		sql.append("(:pda_name,:pda_fill,:pda_color,:pda_size,:pda_place,:pda_model,:pda_safe,:pda_execute,:pda_grade,");
		sql.append(" :pda_price_name,:pda_salesman_comm,:pda_salesman_commrate,:pda_gbcode,:pda_wash_explain,:pda_sale,:pda_change,:pda_gift,");
		sql.append(" :pda_vip_sale,:pda_present,:pda_buy,:pda_point,:pda_batch_price1,:pda_batch_price2,:pda_batch_price3,:companyid)");
		KeyHolder holder = new GeneratedKeyHolder(); 
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(product_Assist),holder);
		product_Assist.setPda_id(holder.getKey().intValue());
	}

	@Override
	public void update_product_assist(T_Base_Product_Assist product_Assist) {
		StringBuffer sql = new StringBuffer("");
		sql.append("UPDATE t_base_product_assist SET ");
		sql.append(" pda_name=:pda_name,");
		sql.append(" pda_fill=:pda_fill,");
		sql.append(" pda_color=:pda_color,");
		sql.append(" pda_size=:pda_size,");
		sql.append(" pda_place=:pda_place,");
		sql.append(" pda_model=:pda_model,");
		sql.append(" pda_safe=:pda_safe,");
		sql.append(" pda_execute=:pda_execute,");
		sql.append(" pda_grade=:pda_grade,");
		sql.append(" pda_price_name=:pda_price_name,");
		sql.append(" pda_salesman_comm=:pda_salesman_comm,");
		sql.append(" pda_salesman_commrate=:pda_salesman_commrate,");
		sql.append(" pda_gbcode=:pda_gbcode,");
		sql.append(" pda_wash_explain=:pda_wash_explain,");
		sql.append(" pda_sale=:pda_sale,");
		sql.append(" pda_change=:pda_change,");
		sql.append(" pda_gift=:pda_gift,");
		sql.append(" pda_vip_sale=:pda_vip_sale,");
		sql.append(" pda_present=:pda_present,");
		sql.append(" pda_buy=:pda_buy,");
		sql.append(" pda_point=:pda_point,");
		sql.append(" pda_batch_price1=:pda_batch_price1,");
		sql.append(" pda_batch_price2=:pda_batch_price2,");
		sql.append(" pda_batch_price3=:pda_batch_price3");
		sql.append(" where pda_id=:pda_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(product_Assist));
	}

	@Override
	public void del_product_assist(Map<String, Object> param) {
		Object pda_id = param.get("pda_id");
		StringBuffer sql = new StringBuffer("");
		sql.append(" DELETE FROM t_base_product_assist WHERE pda_id=:pda_id ");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("pda_id", pda_id));
	}

}
