package zy.dao.base.department;

import java.util.List;
import java.util.Map;

import zy.entity.base.department.T_Base_Department;

public interface DepartmentDAO {
	Integer count(Map<String,Object> param);
	List<T_Base_Department> list(Map<String,Object> param);
	
	Integer queryByName(T_Base_Department model);
	T_Base_Department queryByID(Integer id);
	void update(T_Base_Department model);
	void save(T_Base_Department model);
	void del(Integer id);
}
