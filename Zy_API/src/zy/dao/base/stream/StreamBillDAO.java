package zy.dao.base.stream;

import java.util.List;
import java.util.Map;

import zy.entity.base.stream.T_Base_Stream_Bill;

public interface StreamBillDAO {
	Integer count(Map<String,Object> params);
	List<T_Base_Stream_Bill> list(Map<String,Object> params);
	T_Base_Stream_Bill loadByJoinNumber(String joinNumber,Integer companyid);
	void save(T_Base_Stream_Bill bill);
	void delete(T_Base_Stream_Bill bill);
	void delete(Integer seb_id);
}
