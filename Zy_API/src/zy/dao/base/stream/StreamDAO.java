package zy.dao.base.stream;

import java.util.List;
import java.util.Map;

import zy.entity.base.stream.T_Base_Stream;

public interface StreamDAO {
	Integer count(Map<String,Object> param);
	List<T_Base_Stream> list(Map<String,Object> param);
	T_Base_Stream check(T_Base_Stream stream);
	void save(T_Base_Stream model);
	T_Base_Stream queryByID(Integer id);
	void update(T_Base_Stream model);
	void del(Integer id);
	T_Base_Stream loadStream(String se_code, Integer companyid);
	void updateSettle(T_Base_Stream stream);
}
