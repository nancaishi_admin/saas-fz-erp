package zy.dao.base.color;

import java.util.List;
import java.util.Map;

import zy.entity.base.color.T_Base_Color;
import zy.entity.base.product.T_Base_Product_Color;

public interface ColorDAO {
	Integer count(Map<String,Object> param);
	List<T_Base_Color> list(Map<String,Object> param);
	Integer queryByName(T_Base_Color color);
	T_Base_Color queryByID(Integer cr_id);
	T_Base_Color loadByName(String cr_name,Integer companyid);
	void update(T_Base_Color color);
	void save(T_Base_Color color);
	void save(List<T_Base_Color> colors);
	void del(Integer cr_id);
	List<T_Base_Color> colorList(Map<String,Object> param);
	List<T_Base_Color> queryByPdCode(Map<String,Object> param);
	T_Base_Product_Color loadProductColor(String pd_code,String cr_code,Integer companyid);
	void saveProductColor(T_Base_Product_Color productColor);
} 
