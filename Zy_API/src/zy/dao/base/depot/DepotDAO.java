package zy.dao.base.depot;

import java.util.List;
import java.util.Map;

import zy.entity.base.depot.T_Base_Depot;

public interface DepotDAO {
	Integer count(Map<String,Object> param);
	List<T_Base_Depot> list(Map<String,Object> param);
	List<T_Base_Depot> list4buy(Map<String,Object> params);
	List<T_Base_Depot> list4batch(Map<String, Object> params);
	List<T_Base_Depot> list4stock(Map<String, Object> params);
	List<T_Base_Depot> list4want(Map<String, Object> params);
	List<T_Base_Depot> list4allot(Map<String, Object> params);
	List<T_Base_Depot> list4sellallocate(Map<String, Object> params);
	List<T_Base_Depot> listByShop(String shop_code,Integer companyid);
	List<String> listDepotByShop(String shop_code,Integer companyid);
	T_Base_Depot queryByID(Integer id);
	void update(T_Base_Depot model);
	void save(T_Base_Depot model);
	void updateDefault(T_Base_Depot model);
	void del(T_Base_Depot model);
	Integer useByCode(T_Base_Depot model);
	//----------------------前台--------------------
	public List<T_Base_Depot> ownDepot(Map<String, Object> param);
}
