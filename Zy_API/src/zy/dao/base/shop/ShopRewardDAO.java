package zy.dao.base.shop;

import java.util.List;
import java.util.Map;

import zy.entity.base.shop.T_Base_Shop_Reward;

public interface ShopRewardDAO {
	Integer count(Map<String, Object> params);
	List<T_Base_Shop_Reward> list(Map<String, Object> params);
	List<T_Base_Shop_Reward> statByShop(Map<String, Object> params);
	void save(T_Base_Shop_Reward shopReward);
	void save(List<T_Base_Shop_Reward> shopRewards);
}
