package zy.dao.base.shop;

import java.util.List;

import zy.entity.base.shop.T_Base_Shop_Line;

public interface ShopLineDAO {
	T_Base_Shop_Line load(String sp_code,Integer companyid);
	void save(T_Base_Shop_Line shopLine);
	void update(T_Base_Shop_Line shopLine);
	List<T_Base_Shop_Line> listByMobile(String mobile);
}
