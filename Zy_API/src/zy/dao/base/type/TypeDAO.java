package zy.dao.base.type;

import java.util.List;
import java.util.Map;

import zy.entity.base.type.T_Base_Type;

public interface TypeDAO {
	List<T_Base_Type> list(Map<String,Object> param);
	Integer queryByName(T_Base_Type type);
	void save(T_Base_Type type);
	void del(Integer tp_id);
	void update(T_Base_Type type);
	T_Base_Type queryByID(Integer tp_id);
}
