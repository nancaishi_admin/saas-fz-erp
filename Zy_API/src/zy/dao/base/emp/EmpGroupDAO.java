package zy.dao.base.emp;

import java.util.List;
import java.util.Map;

import zy.entity.base.emp.T_Base_Emp;
import zy.entity.base.emp.T_Base_EmpGroup;
import zy.entity.base.emp.T_Base_EmpGroupList;

public interface EmpGroupDAO {
	List<T_Base_EmpGroup> list(Map<String, Object> params);
	T_Base_EmpGroup load(Integer eg_id);
	List<T_Base_EmpGroupList> listDetails(Map<String, Object> params);
	List<T_Base_Emp> listEmps(Map<String, Object> params);
	void save(T_Base_EmpGroup empGroup, List<T_Base_EmpGroupList> groupLists);
	void update(T_Base_EmpGroup empGroup, List<T_Base_EmpGroupList> groupLists);
	void del(Integer eg_id,String eg_code,Integer companyid);
}
