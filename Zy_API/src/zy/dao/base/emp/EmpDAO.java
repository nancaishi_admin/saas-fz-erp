package zy.dao.base.emp;

import java.util.List;
import java.util.Map;

import zy.dto.base.emp.EmpLoginDto;
import zy.entity.base.emp.T_Base_Emp;
import zy.form.StringForm;

public interface EmpDAO {
	Integer count(Map<String,Object> param);
	List<T_Base_Emp> list(Map<String,Object> param);
	Integer countShop(Map<String,Object> param);
	void save(T_Base_Emp emp);
	T_Base_Emp queryByID(Integer em_id);
	void update(T_Base_Emp emp);
	void del(Integer em_id);
	
	List<T_Base_Emp> listCombo(Map<String, Object> param);
	List<T_Base_Emp> listShop(Map<String,Object> param);
	List<StringForm> _listShop(Map<String,Object> param);
	
	EmpLoginDto login(String em_code,String em_pass,String co_code);
	void updatePwd(T_Base_Emp emp);
}
