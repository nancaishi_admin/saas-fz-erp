package zy.dao.base.emp;

import java.util.List;
import java.util.Map;

import zy.entity.base.emp.T_Base_Emp_Reward;

public interface EmpRewardDAO {
	Integer count(Map<String, Object> params);
	List<T_Base_Emp_Reward> list(Map<String, Object> params);
	List<T_Base_Emp_Reward> statByEmp(Map<String, Object> params);
	void save(T_Base_Emp_Reward empReward);
	void save(List<T_Base_Emp_Reward> empRewards);
}
