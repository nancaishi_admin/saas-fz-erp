package zy.dao.base.bra.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.base.bra.BraDAO;
import zy.entity.base.bra.T_Base_Bra;
import zy.util.StringUtil;
@Repository
public class BraDAOImpl extends BaseDaoImpl implements BraDAO{
	@Override
	public Integer count(Map<String, Object> param) {
		Object name = param.get("name");
		StringBuffer sql = new StringBuffer("");
		sql.append(" select count(1)");
		sql.append(" from T_Base_Bra t");
		sql.append(" where 1 = 1");
        if(null != name && !"".equals(name)){
        	sql.append(" and instr(t.br_name,:name)>0");
        }
		sql.append(" and t.companyid=:companyid");
		Integer count = namedParameterJdbcTemplate.queryForObject(sql.toString(), param, Integer.class);
		return count;
	}

	@Override
	public List<T_Base_Bra> list(Map<String, Object> param) {
		Object name = param.get("name");
		StringBuffer sql = new StringBuffer("");
		sql.append(" select br_id,br_code,br_name");
		sql.append(" from T_Base_Bra t");
		sql.append(" where 1 = 1");
        if(null != name && !"".equals(name)){
        	sql.append(" and instr(t.br_name,:name)>0");
        }
		sql.append(" and t.companyid=:companyid");
		sql.append(" order by br_id asc");
		sql.append(" limit :start,:end");
		List<T_Base_Bra> list = namedParameterJdbcTemplate.query(sql.toString(), param, new BeanPropertyRowMapper<>(T_Base_Bra.class));
		return list;
	}

	@Override
	public Integer queryByName(T_Base_Bra model) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" select br_id from T_Base_Bra");
		sql.append(" where 1=1");
		if(null != model.getBr_name() && !"".equals(model.getBr_name())){
			sql.append(" and br_name=:br_name");
		}
		if(null != model.getBr_id() && model.getBr_id() > 0){
			sql.append(" and br_id <> :br_id");
		}
		sql.append(" and companyid=:companyid");
		sql.append(" limit 1");
		try{
			Integer id = namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new BeanPropertySqlParameterSource(model),Integer.class);
			return id;
		}catch(Exception e){
			//e.printStackTrace();
			return null;
		}
	}

	@Override
	public T_Base_Bra queryByID(Integer id) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" select br_id,br_code,br_name from T_Base_Bra");
		sql.append(" where br_id=:br_id");
		sql.append(" limit 1");
		try{
			T_Base_Bra data = namedParameterJdbcTemplate.queryForObject(sql.toString(), new MapSqlParameterSource().addValue("br_id", id),new BeanPropertyRowMapper<>(T_Base_Bra.class));
			return data;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void save(T_Base_Bra model) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" select f_two_code(max(br_code+0)) from T_Base_Bra ");
		sql.append(" where 1=1");
		sql.append(" and companyid=:companyid");
		String code = namedParameterJdbcTemplate.queryForObject(sql.toString(), new BeanPropertySqlParameterSource(model), String.class);
		model.setBr_code(code);
		sql.setLength(0);
		sql.append("INSERT INTO T_Base_Bra");
		sql.append(" (br_code,br_name,companyid)");
		sql.append(" VALUES(:br_code,:br_name,:companyid)");
		
		KeyHolder holder = new GeneratedKeyHolder(); 
		
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(model),holder);
		int id = holder.getKey().intValue();
		model.setBr_id(id);
	}

	@Override
	public void update(T_Base_Bra model) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" update T_Base_Bra");
		sql.append(" set br_name=:br_name");
		sql.append(" where br_id=:br_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(model));
	}

	@Override
	public void del(Integer id) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" DELETE FROM T_Base_Bra");
		sql.append(" WHERE br_id=:br_id");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("br_id", id));
	}

	@Override
	public List<T_Base_Bra> braList(Map<String, Object> param) {
		Object br_name = param.get("br_name"); 
		StringBuffer sql = new StringBuffer("");
		sql.append(" select br_id,br_code,br_name");
		sql.append(" from T_Base_Bra t");
		sql.append(" where 1 = 1");
		if(StringUtil.isNotEmpty(br_name)){
			sql.append(" AND INSTR(t.br_name,:br_name)>-1");
		}
		sql.append(" and t.companyid=:companyid");
		sql.append(" order by br_id asc");
		List<T_Base_Bra> list = namedParameterJdbcTemplate.query(sql.toString(), param, new BeanPropertyRowMapper<>(T_Base_Bra.class));
		return list;
	}

	@Override
	public List<T_Base_Bra> queryByPdCode(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT br_code,br_name ");
		sql.append(" FROM t_base_bra t");
		sql.append(" JOIN t_base_product_br b");
		sql.append(" ON b.pdb_br_code=t.br_code");
		sql.append(" AND b.companyid=t.companyid");
		sql.append(" WHERE b.pdb_pd_code=:pd_code");
		sql.append(" AND b.companyid=:companyid");
		List<T_Base_Bra> list = namedParameterJdbcTemplate.query(sql.toString(), param, new BeanPropertyRowMapper<>(T_Base_Bra.class));
		return list;
	}
	
}
