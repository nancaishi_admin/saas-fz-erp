package zy.dao.buy.dealings.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.buy.dealings.BuyDealingsDAO;
import zy.entity.buy.dealings.T_Buy_Dealings;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Repository
public class BuyDealingsDAOImpl extends BaseDaoImpl implements BuyDealingsDAO{

	@Override
	public Integer count(Map<String, Object> params) {
		Object begindate = params.get("begindate");
		Object enddate = params.get("enddate");
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT COUNT(1)");
		sql.append(" FROM t_buy_dealings t");
		sql.append(" WHERE 1=1");
		if (StringUtil.isNotEmpty(begindate)) {
			sql.append(" AND dl_date >= :begindate ");
		}
		if (StringUtil.isNotEmpty(enddate)) {
			sql.append(" AND dl_date <= :enddate ");
		}
		sql.append(" AND dl_supply_code = :dl_supply_code");
		sql.append(" AND t.companyid=:companyid");
		return namedParameterJdbcTemplate.queryForObject(sql.toString(), params, Integer.class);
	}

	@Override
	public List<T_Buy_Dealings> list(Map<String, Object> params) {
		Object sidx = params.get(CommonUtil.SIDX);
		Object sord = params.get(CommonUtil.SORD);
		Object begindate = params.get("begindate");
		Object enddate = params.get("enddate");
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT dl_id,dl_supply_code,dl_number,dl_type,dl_discount_money,dl_payable,dl_payabled,dl_debt,dl_date,");
		sql.append(" dl_manager,dl_remark,dl_sysdate,dl_buyamount,companyid,");
		sql.append(" (SELECT sp_name FROM t_buy_supply sp WHERE sp_code = dl_supply_code AND sp.companyid = t.companyid LIMIT 1) AS supply_name");
		sql.append(" FROM t_buy_dealings t");
		sql.append(" WHERE 1=1");
		if (StringUtil.isNotEmpty(begindate)) {
			sql.append(" AND dl_date >= :begindate ");
		}
		if (StringUtil.isNotEmpty(enddate)) {
			sql.append(" AND dl_date <= :enddate ");
		}
		sql.append(" AND dl_supply_code = :dl_supply_code");
		sql.append(" AND t.companyid=:companyid");
		if(sidx != null && !"".equals(sidx)){
			sql.append(" ORDER BY ").append(sidx).append(" ").append(sord);
		}else {
			sql.append(" ORDER BY dl_id ASC");
		}
		sql.append(" LIMIT :start,:end");
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Buy_Dealings.class));
	}
	
	
	@Override
	public void save(T_Buy_Dealings dealings) {
		StringBuffer sql = new StringBuffer("");
		sql.append("INSERT INTO t_buy_dealings");
		sql.append(" (dl_supply_code,dl_number,dl_type,dl_discount_money,dl_payable,dl_payabled,dl_debt,dl_date,dl_manager,dl_remark,dl_sysdate,dl_buyamount,companyid)");
		sql.append(" VALUES(:dl_supply_code,:dl_number,:dl_type,:dl_discount_money,:dl_payable,:dl_payabled,:dl_debt,:dl_date,:dl_manager,:dl_remark,:dl_sysdate,:dl_buyamount,:companyid)");
		KeyHolder holder = new GeneratedKeyHolder(); 
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(dealings),holder);
		dealings.setDl_id(holder.getKey().longValue());
	}

}
