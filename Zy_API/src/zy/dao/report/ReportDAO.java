package zy.dao.report;

import java.util.List;
import java.util.Map;

import zy.dto.report.kpi.KpiAnalysisDayDto;
import zy.dto.report.kpi.KpiAnalysisDto;
import zy.dto.report.kpi.KpiAnalysisMonthDto;
import zy.entity.base.shop.T_Base_Shop;
import zy.entity.sell.cash.T_Sell_ShopList;

public interface ReportDAO {
	List<T_Base_Shop> listShop(Map<String, Object> params);
	Map<String,Object> sell_max_count(Map<String,Object> paramMap);
	List<T_Sell_ShopList> sell_max_list(Map<String,Object> paramMap);
	Map<String,Object> selllist_max_count(Map<String,Object> paramMap);
	List<T_Sell_ShopList> selllist_max_list(Map<String,Object> paramMap);
	List<T_Sell_ShopList> sell_compare(Map<String,Object> paramMap);
	Integer sellStock_count(Map<String,Object> paramMap);
	List<T_Sell_ShopList> sellStock_list(Map<String,Object> paramMap);
	Map<String, KpiAnalysisDto> kpi_analysis(Map<String, Object> params);
	Map<String, KpiAnalysisMonthDto> kpi_analysis_month(Map<String, Object> params);
	Map<String, KpiAnalysisDayDto> kpi_analysis_day(Map<String, Object> params);
	List<Map<String,Object>> brand_run(Map<String,Object> param);
	Integer product_run_count(Map<String,Object> param);
	List<Map<String,Object>> product_run_list(Map<String,Object> param);
	
}
