package zy.dao.sys.print.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.sys.print.PrintDAO;
import zy.entity.common.print.Common_Print;
import zy.entity.sys.print.T_Sys_Print;
import zy.entity.sys.print.T_Sys_PrintData;
import zy.entity.sys.print.T_Sys_PrintField;
import zy.entity.sys.print.T_Sys_PrintSet;

@Repository
public class PrintDAOImpl extends BaseDaoImpl implements PrintDAO{
	
	@Override
	public List<Common_Print> listCommonPrint(List<Integer> pt_type) {
		MapSqlParameterSource params = new MapSqlParameterSource().addValue("pt_type", pt_type);
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT pt_id,pt_name,pt_type");
		sql.append(" FROM common_print");
		sql.append(" WHERE pt_type IN (:pt_type)");
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(Common_Print.class));
	}
	
	@Override
	public List<T_Sys_Print> list(Integer sp_type,String sp_shop_code,Integer companyid) {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT sp_id,sp_name,sp_type,sp_remark,sp_shop_code,sp_sysdate,sp_us_id,companyid");
		sql.append(" FROM t_sys_print");
		sql.append(" WHERE 1=1");
		sql.append(" AND sp_type = :sp_type");
		sql.append(" AND sp_shop_code = :sp_shop_code");
		sql.append(" AND companyid = :companyid");
		MapSqlParameterSource params = new MapSqlParameterSource().addValue("sp_type", sp_type).addValue("sp_shop_code", sp_shop_code).addValue("companyid", companyid);
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Sys_Print.class));
	}
	
	@Override
	public Map<String, Object> loadPrint(Integer sp_id) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		MapSqlParameterSource params = new MapSqlParameterSource().addValue("sp_id", sp_id);
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT sp_id,sp_name,sp_type,sp_remark,sp_shop_code,sp_sysdate,sp_us_id,companyid");
		sql.append(" FROM t_sys_print");
		sql.append(" WHERE 1=1");
		sql.append(" AND sp_id = :sp_id");
		sql.append(" LIMIT 1");
		T_Sys_Print print = null;
		try {
			print = namedParameterJdbcTemplate.queryForObject(sql.toString(), params, new BeanPropertyRowMapper<>(T_Sys_Print.class));
		} catch (Exception e) {
			return resultMap;
		}
		sql.setLength(0);
		sql.append("SELECT spf_id,spf_sp_id,spf_code,spf_name,spf_namecustom,spf_colspan,spf_show,spf_line,spf_row,spf_position,companyid ");
		sql.append(" FROM t_sys_printfield");
		sql.append(" WHERE spf_sp_id = :sp_id");
		List<T_Sys_PrintField> fields = namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Sys_PrintField.class));
		sql.setLength(0);
		sql.append("SELECT spd_id,spd_sp_id,spd_code,spd_name,spd_namecustom,spd_order,spd_show,spd_width,spd_align,companyid");
		sql.append(" FROM t_sys_printdata");
		sql.append(" WHERE spd_sp_id = :sp_id");
		List<T_Sys_PrintData> datas = namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Sys_PrintData.class));
		sql.setLength(0);
		sql.append("SELECT sps_id,sps_sp_id,sps_code,sps_name,companyid ");
		sql.append(" FROM t_sys_printset");
		sql.append(" WHERE sps_sp_id = :sp_id");
		List<T_Sys_PrintSet> sets = namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Sys_PrintSet.class));
		resultMap.put("print", print);
		resultMap.put("fields", fields);
		resultMap.put("datas", datas);
		resultMap.put("sets", sets);
		return resultMap;
	}
	
	@Override
	public Map<String, Object> loadPrint4Bill(Integer sp_id) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		MapSqlParameterSource params = new MapSqlParameterSource().addValue("sp_id", sp_id).addValue("show", 1);
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT sp_id,sp_name,sp_type,sp_remark,sp_shop_code,sp_sysdate,sp_us_id,companyid");
		sql.append(" FROM t_sys_print");
		sql.append(" WHERE 1=1");
		sql.append(" AND sp_id = :sp_id");
		sql.append(" LIMIT 1");
		T_Sys_Print print = null;
		try {
			print = namedParameterJdbcTemplate.queryForObject(sql.toString(), params, new BeanPropertyRowMapper<>(T_Sys_Print.class));
		} catch (Exception e) {
			return resultMap;
		}
		sql.setLength(0);
		sql.append("SELECT spf_id,spf_sp_id,spf_code,spf_name,spf_namecustom,spf_colspan,spf_show,spf_line,spf_row,spf_position,companyid ");
		sql.append(" FROM t_sys_printfield");
		sql.append(" WHERE spf_sp_id = :sp_id");
		sql.append(" AND spf_show = :show");
		sql.append(" ORDER BY spf_line,spf_row");
		List<T_Sys_PrintField> fields = namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Sys_PrintField.class));
		sql.setLength(0);
		sql.append("SELECT spd_id,spd_sp_id,spd_code,spd_name,spd_namecustom,spd_order,spd_show,spd_width,spd_align,companyid");
		sql.append(" FROM t_sys_printdata");
		sql.append(" WHERE spd_sp_id = :sp_id");
		sql.append(" AND spd_show = :show");
		sql.append(" ORDER BY spd_order");
		List<T_Sys_PrintData> datas = namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Sys_PrintData.class));
		sql.setLength(0);
		sql.append("SELECT sps_id,sps_sp_id,sps_code,sps_name,companyid ");
		sql.append(" FROM t_sys_printset");
		sql.append(" WHERE sps_sp_id = :sp_id");
		List<T_Sys_PrintSet> sets = namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Sys_PrintSet.class));
		resultMap.put("print", print);
		resultMap.put("fields", fields);
		resultMap.put("datas", datas);
		resultMap.put("sets", sets);
		return resultMap;
	}
	
	@Override
	public Map<String, Object> loadDefaultPrint(Integer pt_type) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		MapSqlParameterSource params = new MapSqlParameterSource().addValue("pt_type", pt_type);
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT pt_id,pt_name,pt_type");
		sql.append(" FROM common_print");
		sql.append(" WHERE pt_type = :pt_type");
		Common_Print print = null;
		try {
			print = namedParameterJdbcTemplate.queryForObject(sql.toString(), params, new BeanPropertyRowMapper<>(Common_Print.class));
		} catch (Exception e) {
			return resultMap;
		}
		params = new MapSqlParameterSource().addValue("pt_id", print.getPt_id());
		sql.setLength(0);
		sql.append("SELECT ptf_id AS spf_id,ptf_code AS spf_code,ptf_name AS spf_name,ptf_name AS spf_namecustom,");
		sql.append(" ptf_colspan AS spf_colspan,ptf_show AS spf_show,ptf_line AS spf_line,ptf_row AS spf_row,ptf_position AS spf_position");
		sql.append(" FROM common_printfield");
		sql.append(" WHERE  ptf_pt_id = :pt_id");
		List<T_Sys_PrintField> fields = namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Sys_PrintField.class));
		sql.setLength(0);
		sql.append("SELECT ptd_id AS spd_id,ptd_code AS spd_code,ptd_name AS spd_name,ptd_name AS spd_namecustom,");
		sql.append(" ptd_order AS spd_order,ptd_show AS spd_show,ptd_width AS spd_width,ptd_align AS spd_align");
		sql.append(" FROM common_printdata");
		sql.append(" WHERE ptd_pt_id = :pt_id");
		List<T_Sys_PrintData> datas = namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Sys_PrintData.class));
		sql.setLength(0);
		
		sql.append("SELECT pts_id AS sps_id,pts_code AS sps_code,pts_name AS sps_name");
		sql.append(" FROM common_printset");
		sql.append(" WHERE pts_pt_id = :pt_id");
		List<T_Sys_PrintSet> sets = namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Sys_PrintSet.class));
		
		resultMap.put("fields", fields);
		resultMap.put("datas", datas);
		resultMap.put("sets", sets);
		return resultMap;
	}

	@Override
	public void save(T_Sys_Print print, List<T_Sys_PrintField> printFields, List<T_Sys_PrintData> printDatas, List<T_Sys_PrintSet> printSets) {
		StringBuffer sql = new StringBuffer();
		sql.append("INSERT INTO t_sys_print(sp_name,sp_type,sp_remark,sp_shop_code,sp_sysdate,sp_us_id,companyid)");
		sql.append(" VALUES(:sp_name,:sp_type,:sp_remark,:sp_shop_code,:sp_sysdate,:sp_us_id,:companyid)");
		KeyHolder holder = new GeneratedKeyHolder(); 
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(print),holder);
		print.setSp_id(holder.getKey().intValue());
		for (T_Sys_PrintField field : printFields) {
			field.setSpf_sp_id(print.getSp_id());
		}
		for (T_Sys_PrintData data : printDatas) {
			data.setSpd_sp_id(print.getSp_id());
		}
		for (T_Sys_PrintSet set : printSets) {
			set.setSps_sp_id(print.getSp_id());
		}
		sql.setLength(0);
		sql.append("INSERT INTO t_sys_printfield");
		sql.append(" (spf_sp_id,spf_code,spf_name,spf_namecustom,spf_colspan,spf_show,spf_line,spf_row,spf_position,companyid)");
		sql.append(" VALUES");
		sql.append(" (:spf_sp_id,:spf_code,:spf_name,:spf_namecustom,:spf_colspan,:spf_show,:spf_line,:spf_row,:spf_position,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(printFields.toArray()));
		sql.setLength(0);
		sql.append("INSERT INTO t_sys_printdata");
		sql.append(" (spd_sp_id,spd_code,spd_name,spd_namecustom,spd_order,spd_show,spd_width,spd_align,companyid)");
		sql.append(" VALUES");
		sql.append(" (:spd_sp_id,:spd_code,:spd_name,:spd_namecustom,:spd_order,:spd_show,:spd_width,:spd_align,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(printDatas.toArray()));
		sql.setLength(0);
		sql.append("INSERT INTO t_sys_printset(sps_sp_id,sps_code,sps_name,companyid)");
		sql.append(" VALUES(:sps_sp_id,:sps_code,:sps_name,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(printSets.toArray()));
	}

	@Override
	public void update(T_Sys_Print print, List<T_Sys_PrintField> printFields, List<T_Sys_PrintData> printDatas, List<T_Sys_PrintSet> printSets) {
		StringBuffer sql = new StringBuffer();
		sql.append("UPDATE t_sys_print");
		sql.append(" SET sp_name = :sp_name,sp_remark = :sp_remark");
		sql.append(" WHERE sp_id = :sp_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(print));
		sql.setLength(0);
		sql.append("UPDATE t_sys_printfield");
		sql.append(" SET spf_namecustom = :spf_namecustom");
		sql.append(" ,spf_show = :spf_show");
		sql.append(" ,spf_colspan = :spf_colspan");
		sql.append(" ,spf_line = :spf_line");
		sql.append(" ,spf_row = :spf_row");
		sql.append(" WHERE spf_id = :spf_id");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(printFields.toArray()));
		sql.setLength(0);
		sql.append("UPDATE t_sys_printdata");
		sql.append(" SET spd_namecustom = :spd_namecustom");
		sql.append(" ,spd_order = :spd_order");
		sql.append(" ,spd_show = :spd_show");
		sql.append(" ,spd_width = :spd_width");
		sql.append(" ,spd_align = :spd_align");
		sql.append(" WHERE spd_id = :spd_id");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(printDatas.toArray()));
		sql.setLength(0);
		sql.append(" UPDATE t_sys_printset");
		sql.append(" SET sps_name = :sps_name");
		sql.append(" WHERE sps_code = :sps_code AND sps_sp_id = :sps_sp_id");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(printSets.toArray()));
	}

	@Override
	public void del(Integer sp_id) {
		MapSqlParameterSource params = new MapSqlParameterSource().addValue("sp_id", sp_id);
		StringBuffer sql = new StringBuffer();
		sql.append("DELETE FROM t_sys_print");
		sql.append(" WHERE sp_id = :sp_id");
		namedParameterJdbcTemplate.update(sql.toString(), params);
		sql.setLength(0);
		sql.append("DELETE FROM t_sys_printdata");
		sql.append(" WHERE spd_sp_id = :sp_id");
		namedParameterJdbcTemplate.update(sql.toString(), params);
		sql.setLength(0);
		sql.append("DELETE FROM t_sys_printfield");
		sql.append(" WHERE spf_sp_id = :sp_id");
		namedParameterJdbcTemplate.update(sql.toString(), params);
		sql.setLength(0);
		sql.append("DELETE FROM t_sys_printset");
		sql.append(" WHERE sps_sp_id = :sp_id");
		namedParameterJdbcTemplate.update(sql.toString(), params);
	}

}
