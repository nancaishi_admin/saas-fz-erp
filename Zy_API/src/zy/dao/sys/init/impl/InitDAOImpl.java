package zy.dao.sys.init.impl;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.sys.init.InitDAO;
import zy.entity.base.shop.T_Base_Shop;
import zy.entity.batch.client.T_Batch_Client;
import zy.entity.batch.sell.T_Batch_Sell;
import zy.entity.buy.enter.T_Buy_Enter;
import zy.entity.buy.supply.T_Buy_Supply;
import zy.entity.money.bank.T_Money_Bank;
import zy.entity.sort.allot.T_Sort_Allot;
import zy.util.CommonUtil;

@Repository
public class InitDAOImpl extends BaseDaoImpl implements InitDAO{
	
	@Override
	public T_Base_Shop checkShop(String shop_code,Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT sp_id,sp_code,sp_name,sp_init ");
		sql.append(" FROM t_base_shop t");
		sql.append(" WHERE 1=1");
		sql.append(" AND sp_code = :shop_code");
		sql.append(" AND companyid = :companyid");
		sql.append(" LIMIT 1");
		try{
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new MapSqlParameterSource().addValue("shop_code", shop_code).addValue("companyid", companyid),
					new BeanPropertyRowMapper<>(T_Base_Shop.class));
		}catch(Exception e){
			return null;
		}
	}
	
	@Override
	public void updateShopInit(String shop_code,String shop_type, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" UPDATE t_base_shop");
		sql.append(" SET sp_init = 1");
		sql.append(" WHERE 1 = 1");
		if(CommonUtil.ONE.equals(shop_type) || CommonUtil.TWO.equals(shop_type)){//总公司、分公司
			sql.append(" AND ((sp_upcode = :shop_code AND (sp_shop_type = "+CommonUtil.FOUR+" OR sp_shop_type = "+CommonUtil.FIVE+"))");
			sql.append(" OR sp_code = :shop_code)");
		}else{
			sql.append(" AND sp_code = :shop_code");
		}
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("shop_code", shop_code).addValue("companyid", companyid));
	}
	
	@Override
	public List<T_Batch_Client> listClient4Init(String shop_code,Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT ci_id,ci_code,ci_name,ci_init_debt,ci_receivable");
		sql.append(" FROM t_batch_client");
		sql.append(" WHERE 1=1");
		sql.append(" AND ci_init_debt <> 0");
		sql.append(" AND ci_sp_code = :shop_code");
		sql.append(" AND companyid = :companyid");
		return namedParameterJdbcTemplate.query(sql.toString(), 
				new MapSqlParameterSource().addValue("shop_code", shop_code).addValue("companyid", companyid), 
				new BeanPropertyRowMapper<>(T_Batch_Client.class));
	}
	
	@Override
	public List<T_Buy_Supply> listSupply4Init(Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT sp_id,sp_code,sp_name,sp_init_debt,IFNULL(sp_payable,0) AS sp_payable");
		sql.append(" FROM t_buy_supply t");
		sql.append(" WHERE 1=1");
		sql.append(" AND sp_init_debt <> 0");
		sql.append(" AND companyid = :companyid");
		return namedParameterJdbcTemplate.query(sql.toString(), 
				new MapSqlParameterSource().addValue("companyid", companyid), 
				new BeanPropertyRowMapper<>(T_Buy_Supply.class));
	}

	@Override
	public List<T_Base_Shop> listShop4Init(String shop_code,String shop_type, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT sp_id,sp_code,sp_name,sp_init_debt,sp_receivable ");
		sql.append(" FROM t_base_shop t");
		sql.append(" WHERE 1=1");
		sql.append(" AND sp_init_debt <> 0");
		if(CommonUtil.ONE.equals(shop_type)){//总公司(查询下级分公司、加盟店、合伙店)
			sql.append(" AND ((sp_upcode = :shop_code AND (sp_shop_type = "+CommonUtil.FOUR+" OR sp_shop_type = "+CommonUtil.FIVE+"))");
			sql.append(" OR sp_shop_type = "+CommonUtil.TWO+")");
		}else if(CommonUtil.TWO.equals(shop_type)){//分公司(查询下级加盟店、合伙店)
			sql.append(" AND sp_upcode = :shop_code AND (sp_shop_type = "+CommonUtil.FOUR+" OR sp_shop_type = "+CommonUtil.FIVE+")");
		}else{
			sql.append(" AND sp_code = :shop_code");
		}
		sql.append(" AND companyid = :companyid");
		return namedParameterJdbcTemplate.query(sql.toString(), 
				new MapSqlParameterSource().addValue("shop_code", shop_code).addValue("companyid", companyid), 
				new BeanPropertyRowMapper<>(T_Base_Shop.class));
	}
	
	@Override
	public List<T_Money_Bank> listBank4Init(String shop_code,Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT ba_id,ba_code,ba_name,ba_init_balance,ba_balance,ba_shop_code");
		sql.append(" FROM t_money_bank");
		sql.append(" WHERE 1=1");
		sql.append(" AND ba_init_balance <> 0");
		sql.append(" AND ba_shop_code = :shop_code");
		sql.append(" AND companyid = :companyid");
		return namedParameterJdbcTemplate.query(sql.toString(), 
				new MapSqlParameterSource().addValue("shop_code", shop_code).addValue("companyid", companyid), 
				new BeanPropertyRowMapper<>(T_Money_Bank.class));
	}
	
	@Override
	public void saveQcClient(List<T_Batch_Sell> sells) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" INSERT INTO t_batch_sell");
		sql.append(" (se_number,se_make_date,se_client_code,se_depot_code,se_maker,se_manager,se_handnumber,se_amount,");
		sql.append(" se_money,se_retailmoney,se_costmoney,se_rebatemoney,se_discount_money,se_ba_code,se_property,");
		sql.append(" se_remark,se_ar_state,se_ar_date,se_isdraft,se_pay_state,se_receivable,se_received,se_prepay,se_order_number,");
		sql.append(" se_stream_code,se_stream_money,se_lastdebt,se_sysdate,se_us_id,se_type,se_isprint,companyid)");
		sql.append(" VALUES");
		sql.append(" (:se_number,:se_make_date,:se_client_code,:se_depot_code,:se_maker,:se_manager,:se_handnumber,:se_amount,");
		sql.append(" :se_money,:se_retailmoney,:se_costmoney,:se_rebatemoney,:se_discount_money,:se_ba_code,:se_property,");
		sql.append(":se_remark,:se_ar_state,:se_ar_date,:se_isdraft,:se_pay_state,:se_receivable,:se_received,:se_prepay,:se_order_number,");
		sql.append(" :se_stream_code,:se_stream_money,:se_lastdebt,:se_sysdate,:se_us_id,:se_type,:se_isprint,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sells.toArray()));
	}
	
	@Override
	public void saveQcSupply(List<T_Buy_Enter> enters) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" INSERT INTO t_buy_enter");
		sql.append(" (et_number,et_make_date,et_supply_code,et_depot_code,et_maker,et_manager,et_handnumber,et_amount,et_money,et_retailmoney,");
		sql.append(" et_discount_money,et_ba_code,et_property,et_remark,et_ar_state,et_isdraft,et_pay_state,et_payable,et_payabled,et_prepay,");
		sql.append(" et_order_number,et_type,et_sysdate,et_us_id,companyid)");
		sql.append(" VALUES");
		sql.append(" (:et_number,:et_make_date,:et_supply_code,:et_depot_code,:et_maker,:et_manager,:et_handnumber,:et_amount,:et_money,");
		sql.append(" :et_retailmoney,:et_discount_money,:et_ba_code,:et_property,:et_remark,:et_ar_state,:et_isdraft,:et_pay_state,:et_payable,");
		sql.append(" :et_payabled,:et_prepay,:et_order_number,:et_type,:et_sysdate,:et_us_id,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(enters.toArray()));
	}
	
	@Override
	public void saveQcShop(List<T_Sort_Allot> allots) {
		StringBuffer sql = new StringBuffer("");
		sql.append("INSERT INTO t_sort_allot");
		sql.append(" (at_number,at_date,at_shop_code,at_outdp_code,at_indp_code,at_maker,at_manager,at_handnumber,at_applyamount,at_sendamount,");
		sql.append(" at_applymoney,at_sendmoney,at_sendcostmoney,at_sendsellmoney,at_discount_money,at_ba_code,at_property,at_remark,at_ar_state,at_ar_date,");
		sql.append(" at_pay_state,at_receivable,at_received,at_prepay,at_lastdebt,at_type,at_sysdate,at_us_id,at_isdraft,companyid)");
		sql.append(" VALUES");
		sql.append(" (:at_number,:at_date,:at_shop_code,:at_outdp_code,:at_indp_code,:at_maker,:at_manager,:at_handnumber,:at_applyamount,:at_sendamount,");
		sql.append(" :at_applymoney,:at_sendmoney,:at_sendcostmoney,:at_sendsellmoney,:at_discount_money,:at_ba_code,:at_property,:at_remark,:at_ar_state,:at_ar_date,");
		sql.append(" :at_pay_state,:at_receivable,:at_received,:at_prepay,:at_lastdebt,:at_type,:at_sysdate,:at_us_id,:at_isdraft,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(allots.toArray()));
	}
	
}
