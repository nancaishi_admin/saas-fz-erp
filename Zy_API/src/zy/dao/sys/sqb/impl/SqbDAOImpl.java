package zy.dao.sys.sqb.impl;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.sys.sqb.SqbDAO;
import zy.entity.sys.sqb.T_Sys_Sqb;

@Repository
public class SqbDAOImpl extends BaseDaoImpl implements SqbDAO{

	
	@Override
	public T_Sys_Sqb load(Integer sqb_id) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT sqb_id,sqb_device_id,sqb_device_name,sqb_terminal_sn,sqb_terminal_key,sqb_activate_time,sqb_checkin_time,sqb_shop_code,sqb_state,companyid");
		sql.append(" FROM t_sys_sqb t");
		sql.append(" WHERE sqb_id = :sqb_id");
		sql.append(" LIMIT 1");
		try {
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), new MapSqlParameterSource().addValue("sqb_id", sqb_id),
					new BeanPropertyRowMapper<>(T_Sys_Sqb.class));
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public T_Sys_Sqb check(String device_id) {
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT sqb_id,sqb_device_id,sqb_device_name");
		sql.append(" FROM t_sys_sqb t");
		sql.append(" WHERE 1 = 1");
		sql.append(" AND sqb_device_id = :sqb_device_id ");//设备全局唯一
		sql.append(" LIMIT 1");
		try{
			return namedParameterJdbcTemplate.queryForObject(sql.toString(),new MapSqlParameterSource().addValue("sqb_device_id", device_id),
					new BeanPropertyRowMapper<>(T_Sys_Sqb.class));
		}catch(Exception e){
			return null;
		}
	}
	
	@Override
	public void save(T_Sys_Sqb sqb) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" INSERT INTO t_sys_sqb");
		sql.append(" (sqb_device_id,sqb_device_name,sqb_terminal_sn,sqb_terminal_key,sqb_activate_time,sqb_checkin_time,sqb_shop_code,sqb_state,companyid)");
		sql.append(" VALUES");
		sql.append(" (:sqb_device_id,:sqb_device_name,:sqb_terminal_sn,:sqb_terminal_key,:sqb_activate_time,:sqb_checkin_time,:sqb_shop_code,:sqb_state,:companyid)");
		KeyHolder holder = new GeneratedKeyHolder(); 
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(sqb),holder);
		sqb.setSqb_id(holder.getKey().intValue());
	}
	
	@Override
	public void update_activate(T_Sys_Sqb sqb) {
		StringBuffer sql = new StringBuffer("");
		sql.append("UPDATE t_sys_sqb");
		sql.append(" SET sqb_terminal_sn=:sqb_terminal_sn");
		sql.append(" ,sqb_terminal_key=:sqb_terminal_key");
		sql.append(" ,sqb_activate_time=:sqb_activate_time");
		sql.append(" WHERE sqb_id=:sqb_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(sqb));
	}
	
	@Override
	public void update_checkin(T_Sys_Sqb sqb) {
		StringBuffer sql = new StringBuffer("");
		sql.append("UPDATE t_sys_sqb");
		sql.append(" SET sqb_terminal_key=:sqb_terminal_key");
		sql.append(" ,sqb_checkin_time=:sqb_checkin_time");
		sql.append(" WHERE sqb_id=:sqb_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(sqb));
	}
	
}
