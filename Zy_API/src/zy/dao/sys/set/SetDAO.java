package zy.dao.sys.set;

import zy.entity.sys.set.T_Sys_Set;

public interface SetDAO {
	T_Sys_Set load(Integer companyid);
	void save(T_Sys_Set set);
	void update(T_Sys_Set set);
	void updateUseable(T_Sys_Set set);
}
