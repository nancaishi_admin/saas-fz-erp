package zy.dao.sys.set.impl;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.sys.set.SetDAO;
import zy.entity.sys.set.T_Sys_Set;

@Repository
public class SetDAOImpl extends BaseDaoImpl implements SetDAO{

	@Override
	public T_Sys_Set load(Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT st_id,companyid,st_buy_calc_costprice,st_buy_os,st_batch_os,st_batch_showrebates,st_ar_print,st_blank,st_subcode_rule,st_subcode_isrule,");
		sql.append(" st_allocate_unitprice,st_check_showstock,st_check_showdiffer,st_force_checkstock,st_useable,st_use_upmoney ");
		sql.append(" FROM t_sys_set");
		sql.append(" WHERE companyid = :companyid");
		sql.append(" LIMIT 1");
		try{
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), new MapSqlParameterSource().addValue("companyid", companyid),
					new BeanPropertyRowMapper<>(T_Sys_Set.class));
		}catch(Exception e){
			return null;
		}
	}

	@Override
	public void save(T_Sys_Set set) {
		StringBuffer sql = new StringBuffer("");
		sql.append("INSERT INTO t_sys_set");
		sql.append(" (st_id,companyid,st_buy_calc_costprice,st_buy_os,st_batch_os,st_batch_showrebates,st_ar_print,st_blank,");
		sql.append(" st_subcode_rule,st_subcode_isrule,st_allocate_unitprice,st_check_showstock,st_check_showdiffer,st_force_checkstock,st_useable,st_use_upmoney)");
		sql.append(" VALUES(:st_id,:companyid,:st_buy_calc_costprice,:st_buy_os,:st_batch_os,:st_batch_showrebates,:st_ar_print,:st_blank,");
		sql.append(" :st_subcode_rule,:st_subcode_isrule,:st_allocate_unitprice,:st_check_showstock,:st_check_showdiffer,:st_force_checkstock,:st_useable,:st_use_upmoney)");
		KeyHolder holder = new GeneratedKeyHolder(); 
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(set),holder);
		int id = holder.getKey().intValue();
		set.setSt_id(id);
	}

	@Override
	public void update(T_Sys_Set set) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" UPDATE t_sys_set");
		sql.append(" SET st_buy_calc_costprice = :st_buy_calc_costprice,");
		sql.append(" st_buy_os = :st_buy_os,");
		sql.append(" st_batch_os = :st_batch_os,");
		sql.append(" st_batch_showrebates = :st_batch_showrebates,");
		sql.append(" st_ar_print = :st_ar_print,");
		sql.append(" st_blank = :st_blank,");
		sql.append(" st_subcode_rule = :st_subcode_rule,");
		sql.append(" st_subcode_isrule = :st_subcode_isrule,");
		sql.append(" st_allocate_unitprice = :st_allocate_unitprice,");
		sql.append(" st_check_showstock = :st_check_showstock,");
		sql.append(" st_check_showdiffer = :st_check_showdiffer,");
		sql.append(" st_force_checkstock = :st_force_checkstock,");
		sql.append(" st_use_upmoney = :st_use_upmoney");
		sql.append(" WHERE st_id = :st_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(set));
	}
	
	@Override
	public void updateUseable(T_Sys_Set set) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" UPDATE t_sys_set");
		sql.append(" SET st_useable = :st_useable");
		sql.append(" WHERE st_id = :st_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(set));
	}

}
