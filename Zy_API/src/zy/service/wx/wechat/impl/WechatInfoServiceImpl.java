package zy.service.wx.wechat.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.wx.wechat.WechatInfoDAO;
import zy.entity.sys.user.T_Sys_User;
import zy.entity.wx.wechat.T_Wx_WechatInfo;
import zy.service.wx.wechat.WechatInfoService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;
import zy.util.security.ThreeDESUtil;

@Service
public class WechatInfoServiceImpl implements WechatInfoService{
	@Resource
	private WechatInfoDAO wechatInfoDAO;

	@Override
	public T_Wx_WechatInfo load(String shop_code, Integer companyid) {
		T_Wx_WechatInfo wechatInfo = wechatInfoDAO.load(shop_code, companyid);
		if (StringUtil.isNotEmpty(wechatInfo.getWx_mch_id())) {
			try {
				String mchId = new String(ThreeDESUtil.des3DecodeCBC(wechatInfo.getWx_mch_id()), "UTF-8");
				wechatInfo.setWx_mch_id(mchId);
			} catch (Exception e) {
			}
		}
		return wechatInfo;
	}

	@Override
	@Transactional
	public void update(T_Wx_WechatInfo wechatInfo, T_Sys_User user) {
		if (wechatInfo.getWx_id() != null && wechatInfo.getWx_id() > 0) {
			wechatInfoDAO.update(wechatInfo);
		}else {
			if (CommonUtil.ONE.equals(user.getShoptype())
					|| CommonUtil.TWO.equals(user.getShoptype())
					|| CommonUtil.FOUR.equals(user.getShoptype())) {// 总公司、分公司、加盟
	        	wechatInfo.setWx_shop_code(user.getUs_shop_code());
			}else{//自营、合伙
				wechatInfo.setWx_shop_code(user.getShop_upcode());
			}
			wechatInfo.setWx_accounttype(1);
			wechatInfo.setWx_sysdate(DateUtil.getCurrentTime());
			wechatInfo.setCompanyid(user.getCompanyid());
			wechatInfoDAO.save(wechatInfo);
		}
	}
}
