package zy.service.wx.wechat;

import zy.entity.sys.user.T_Sys_User;
import zy.entity.wx.wechat.T_Wx_WechatInfo;

public interface WechatInfoService {
	T_Wx_WechatInfo load(String shop_code,Integer companyid);
	void update(T_Wx_WechatInfo wechatInfo, T_Sys_User user);
}
