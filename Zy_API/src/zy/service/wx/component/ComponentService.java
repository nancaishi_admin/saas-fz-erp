package zy.service.wx.component;

import zy.entity.wx.component.T_Wx_Component;

public interface ComponentService {
	void update(T_Wx_Component component);
	String getComponentAccessToken();
	String getPreAuthCode();
	String queryAuth(String authCode,String expiresIn);
}
