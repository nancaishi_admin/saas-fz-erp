package zy.service.wx.component.impl;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.wx.authorizer.AuthorizerDAO;
import zy.dao.wx.component.ComponentDAO;
import zy.entity.wx.authorizer.T_Wx_Authorizer;
import zy.entity.wx.component.T_Wx_Component;
import zy.service.wx.component.ComponentService;
import zy.util.DateUtil;
import zy.util.StringUtil;
import zy.util.WeixinUtil;

import com.alibaba.fastjson.JSONObject;

@Service
public class ComponentServiceImpl implements ComponentService{
	@Resource
	private ComponentDAO componentDAO;
	@Resource
	private AuthorizerDAO authorizerDAO;
	
	@Override
	@Transactional
	public void update(T_Wx_Component component) {
		T_Wx_Component existComponent = componentDAO.load(component.getWc_appid());
		if (existComponent != null) {
			existComponent.setWc_createtime(component.getWc_createtime());
			existComponent.setWc_verify_ticket(component.getWc_verify_ticket());
			componentDAO.update(existComponent);
		}
	}
	
	@Override
	public String getComponentAccessToken() {
		T_Wx_Component component = componentDAO.load(WeixinUtil.my_app_id);
		if (component == null) {
			return null;
		}
		//已存在且未过期，则直接返回
		if(StringUtil.isNotEmpty(component.getWc_access_token())&&StringUtil.isNotEmpty(component.getWc_expire_time())
				&&DateUtil.getSecondsMinusBetween(DateUtil.getCurrentTime(),component.getWc_expire_time())>0){
			return component.getWc_access_token();
		}
		//调用微信接口获取，并更新到数据库
		Map<String, Object> resultMap = WeixinUtil.getComponentAccessToken(component.getWc_verify_ticket());
		String component_access_token = StringUtil.trimString(resultMap.get("component_access_token"));
		String expires_in = StringUtil.trimString(resultMap.get("expires_in"));
		System.out.println("------------"+resultMap);
		if (StringUtil.isEmpty(component_access_token) || StringUtil.isEmpty(expires_in)) {
			return null;
		}
		component.setWc_access_token(component_access_token);
		component.setWc_expire_time(DateUtil.getTimeAddSeconds(Integer.parseInt(expires_in)));
		componentDAO.updateAccessToken(component);
		return component_access_token;
	}
	
	@Override
	public String getPreAuthCode() {
		T_Wx_Component component = componentDAO.load(WeixinUtil.my_app_id);
		if (component == null) {
			return null;
		}
		//已存在未过期，则直接返回
		if(StringUtil.isNotEmpty(component.getWc_pre_auth_code())&&StringUtil.isNotEmpty(component.getWc_expire_time_pre_auth())
				&&DateUtil.getSecondsMinusBetween(DateUtil.getCurrentTime(),component.getWc_expire_time_pre_auth())>0){
			return component.getWc_pre_auth_code();
		}
		//调用微信接口获取，并更新到数据库
		String component_access_token = getComponentAccessToken();
		Map<String, Object> resultMap = WeixinUtil.getPreAuthCode(component_access_token);
		String pre_auth_code = StringUtil.trimString(resultMap.get("pre_auth_code"));
		String expires_in = StringUtil.trimString(resultMap.get("expires_in"));
		System.out.println("------------"+resultMap);
		if (StringUtil.isEmpty(pre_auth_code) || StringUtil.isEmpty(expires_in)) {
			return null;
		}
		component.setWc_pre_auth_code(pre_auth_code);
		component.setWc_expire_time_pre_auth(DateUtil.getTimeAddSeconds(Integer.parseInt(expires_in)));
		componentDAO.updatePreAuth(component);
		return pre_auth_code;
	}
	
	@Override
	public String queryAuth(String authCode,String expiresIn) {
	    String componentAccessToken = getComponentAccessToken();//把之前保存的component_access_token取出来
	    JSONObject resultJson = WeixinUtil.queryAuth(componentAccessToken, authCode);
	    JSONObject infoJson = resultJson.getJSONObject("authorization_info");
	    T_Wx_Authorizer authorizer = new T_Wx_Authorizer();
	    authorizer.setWa_appid(infoJson.getString("authorizer_appid"));
	    authorizer.setWa_access_token(infoJson.getString("authorizer_access_token"));
	    authorizer.setWa_refresh_token(infoJson.getString("authorizer_refresh_token"));
	    authorizer.setWa_expire_time(DateUtil.getTimeAddSeconds(infoJson.getInteger("expires_in")));
	    T_Wx_Authorizer exist = authorizerDAO.load(authorizer.getWa_appid());
	    if(exist != null){
	    	authorizerDAO.update(authorizer);
	    }else {
			authorizerDAO.save(authorizer);
		}
	    return authorizer.getWa_appid();
	}
	
	
}
