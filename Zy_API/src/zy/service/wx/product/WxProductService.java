package zy.service.wx.product;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.base.product.T_Base_Product;
import zy.entity.sys.user.T_Sys_User;
import zy.entity.wx.product.T_Wx_Product;
import zy.entity.wx.product.T_Wx_ProductShop;

public interface WxProductService {
	PageData<T_Wx_Product> page(Map<String, Object> params);
	T_Wx_Product load(Integer wp_id);
	List<T_Wx_ProductShop> loadProductShop(String ps_number,Integer companyid);
	PageData<T_Base_Product> page_product(Map<String, Object> params);
	void save(List<T_Wx_Product> products, T_Sys_User user);
	void update(T_Wx_Product product, T_Sys_User user);
	void updateState(Map<String, Object> params);
	void updateShop(Map<String, Object> params);
	void del(Map<String, Object> params);
	List<T_Wx_Product> hot_sell(Map<String, Object> params);
	List<T_Wx_Product> listByType(Map<String, Object> params);
	T_Wx_Product load(String pd_code,String shop_code,Integer companyid);
	Map<String, Object> loadStock(Map<String, Object> paramMap);
	Map<String, Object> loadStock_Wx(Map<String, Object> params);
}
