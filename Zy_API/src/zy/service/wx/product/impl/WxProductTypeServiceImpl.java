package zy.service.wx.product.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import zy.dao.wx.product.WxProductTypeDAO;
import zy.entity.wx.product.T_Wx_Product;
import zy.entity.wx.product.T_Wx_ProductType;
import zy.service.wx.product.WxProductTypeService;
import zy.util.CommonUtil;

@Service
public class WxProductTypeServiceImpl implements WxProductTypeService{
	@Resource
	private WxProductTypeDAO wxProductTypeDAO;

	@Override
	public List<T_Wx_ProductType> list(Map<String, Object> params) {
		Assert.notNull(params.get(CommonUtil.COMPANYID), "连接超时，请重新登录!");
		return wxProductTypeDAO.list(params);
	}

	@Override
	public T_Wx_ProductType load(Integer pt_id) {
		return wxProductTypeDAO.load(pt_id);
	}

	@Transactional
	@Override
	public void save(T_Wx_ProductType productType) {
		T_Wx_ProductType exist = wxProductTypeDAO.check(productType);
		if (exist != null) {
			throw new RuntimeException("商品分类已存在!");
		}
		wxProductTypeDAO.save(productType);
	}

	@Transactional
	@Override
	public void update(T_Wx_ProductType productType) {
		T_Wx_ProductType exist = wxProductTypeDAO.check(productType);
		if (exist != null) {
			throw new RuntimeException("商品分类已存在!");
		}
		wxProductTypeDAO.update(productType);
	}

	@Transactional
	@Override
	public void del(Integer pt_id) {
		wxProductTypeDAO.del(pt_id);
	}
	

	@Override
	public List<T_Wx_ProductType> getProductType(Map<String, Object> params) {
		Assert.notNull(params.get(CommonUtil.COMPANYID), "连接超时，请重新登录!");
		return wxProductTypeDAO.getProductType(params);
	}
	
	
	@Override
	public List<T_Wx_Product> getProductByType(Map<String, Object> params) {
		Assert.notNull(params.get(CommonUtil.COMPANYID), "连接超时，请重新登录!");
		Assert.notNull(params.get(CommonUtil.PAGESIZE), "参数pageSize不能为空!");
		Assert.notNull(params.get(CommonUtil.PAGEINDEX), "参数pageIndex不能为空!");
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		return wxProductTypeDAO.getProductByType(params);
	}
	}
