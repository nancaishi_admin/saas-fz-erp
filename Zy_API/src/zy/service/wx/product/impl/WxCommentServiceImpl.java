package zy.service.wx.product.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import zy.dao.wx.product.WxCommentDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.wx.my.T_Wx_Comment;
import zy.service.wx.product.WxCommentService;
import zy.util.CommonUtil;
import zy.util.DateUtil;

@Service
public class WxCommentServiceImpl implements WxCommentService{
	@Resource
	private WxCommentDAO wxCommentDAO;
	
	
	@Override
	public PageData<T_Wx_Comment> page(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		
		Integer totalCount = wxCommentDAO.count(params);
		PageInfo pageInfo = new PageInfo(totalCount, pageSize, pageIndex);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		
		List<T_Wx_Comment> list = wxCommentDAO.list(params);
		PageData<T_Wx_Comment> pageData = new PageData<T_Wx_Comment>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}


	@Override
	public T_Wx_Comment saveComment(T_Wx_Comment t_Wx_Comment) {
		Assert.notNull(t_Wx_Comment.getContent(),"评价内容不能为空!");
		Assert.notNull(t_Wx_Comment.getStar(),"评价星级不能为空!");
		Assert.notNull(t_Wx_Comment.getNikname(),"评价用户昵称不能为空!");
		Assert.notNull(t_Wx_Comment.getWu_code(),"用户编码不能为空!");
		Assert.notNull(t_Wx_Comment.getPd_code(),"商品编码不能为空!");
		Assert.notNull(t_Wx_Comment.getShop_code(),"店铺编码不能为空!");
		Assert.notNull(t_Wx_Comment.getCompanyid(),"连接超时，请重新登录!");
		
		t_Wx_Comment.setCreatedate(DateUtil.getCurrentDate());
		t_Wx_Comment.setType("product");
		
		return wxCommentDAO.saveComment(t_Wx_Comment);
	}
}
