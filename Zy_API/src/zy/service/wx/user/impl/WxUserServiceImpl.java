package zy.service.wx.user.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import zy.dao.wx.user.WxUserDAO;
import zy.entity.wx.my.T_Wx_Address;
import zy.entity.wx.order.T_Wx_Order_Detail;
import zy.entity.wx.product.T_Wx_Product;
import zy.entity.wx.user.T_Wx_User;
import zy.entity.wx.user.T_Wx_User_Account_Detail;
import zy.service.wx.user.WxUserService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.MD5;

@Service
public class WxUserServiceImpl implements WxUserService{
	@Resource
	private WxUserDAO wxUserDAO;
	
	@Override
	public T_Wx_User loadByMobile(String wu_mobile) {
		return wxUserDAO.loadByMobile(wu_mobile);
	}
	
	@Override
	public T_Wx_User login(String wu_mobile,String wu_password,String nickname,String avatarurl) {
		return wxUserDAO.login(wu_mobile, wu_password, nickname, avatarurl);
	}
	
	@Override
	public T_Wx_User login(Map<String, Object> params) {
		Assert.notNull(params.get("mobile"), "手机不能为空");
		Assert.notNull(params.get("pwd"), "密码不能为空");


		return wxUserDAO.login(params);
	}
	
	@Override
	public T_Wx_User update(Map<String, Object> params) {

		return wxUserDAO.update(params);
	}

	
	@Override
	@Transactional
	public T_Wx_User save(T_Wx_User user) {
		Assert.notNull(user.getWu_mobile(),"手机号码不能为空!");
		Assert.notNull(user.getWu_nickname(),"昵称不能为空!");
		Assert.notNull(user.getWu_password(),"密码不能为空!");
		user.setWu_password(MD5.encryptMd5(user.getWu_password()));
		user.setWu_state(0);
		user.setWu_sysdate(DateUtil.getCurrentTime());
		wxUserDAO.save(user);
		return user;
	}

	@Override
	public T_Wx_User updatePassword(String old_password, String new_password,String confirm_password) {
		return wxUserDAO.updatePassword(old_password, new_password,confirm_password);
	}

	@Override
	public T_Wx_Address getAddress(String wu_code) {
		// TODO Auto-generated method stub
		return wxUserDAO.getAddress(wu_code);
	}
	@Override
	public Object getAccoutDetails(Map<String,Object> paramMap){
		Integer pageSize = (Integer)paramMap.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)paramMap.get(CommonUtil.PAGEINDEX);
		paramMap.put(CommonUtil.START, (pageIndex-1)*pageSize);
		paramMap.put(CommonUtil.END, pageSize);
		List<Map<String, Object>> orderDataList = new ArrayList<Map<String,Object>>();
		 List<T_Wx_User_Account_Detail> list = wxUserDAO.getAccoutDetails(paramMap);
		 for(T_Wx_User_Account_Detail order : list){
			 Map<String, Object> detailparamMap =  new HashMap<String,Object>();
			 detailparamMap.put("id", order.getId());
			 detailparamMap.put("type", order.getType());
			 detailparamMap.put("wu_code", order.getWu_code());

			 detailparamMap.put("money", order.getMoney());
			 detailparamMap.put("remark", order.getRemark());
			 detailparamMap.put("createdate", order.getCreatedate().toString());
			 detailparamMap.put("order_number", order.getOrder_number());
			 
			 
			 orderDataList.add(detailparamMap);
		 }
		 
		 return orderDataList;
	}
	
	@Override
	public T_Wx_User saveUserInfo(Map<String, Object> params) {

		return wxUserDAO.saveUserInfo(params);
	}

	@Override
	public T_Wx_User initUser(Map<String, Object> params) {
		// TODO Auto-generated method stub
		return  wxUserDAO.initUser(params);
	}
}
