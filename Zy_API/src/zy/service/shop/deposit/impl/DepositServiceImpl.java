package zy.service.shop.deposit.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.sell.cash.CashDAO;
import zy.dao.shop.deposit.DepositDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.sell.cash.T_Sell_Shop_Temp;
import zy.entity.sell.deposit.T_Sell_Deposit;
import zy.entity.sell.deposit.T_Sell_DepositList;
import zy.service.shop.deposit.DepositService;
import zy.util.CommonUtil;
import zy.vo.sell.SellVO;

@Service
public class DepositServiceImpl implements DepositService {
	@Resource
	private DepositDAO depositDAO;
	@Resource
	private CashDAO cashDAO;
	@Override
	public PageData<T_Sell_DepositList> page(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object pageSize = param.get(CommonUtil.PAGESIZE);
		Object pageIndex = param.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = depositDAO.count(param);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		param.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		param.put(CommonUtil.END, _pageSize);
		
		List<T_Sell_DepositList> list = depositDAO.list(param);
		PageData<T_Sell_DepositList> pageData = new PageData<T_Sell_DepositList>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	/**
	 * 1.查询临时表
	 * 2.插入到订金明细表
	 * 3.保存订金主表
	 * 4.删除临时表数据
	 * */
	@Transactional
	@Override
	public void payDeposit(Map<String, Object> param) {
		List<T_Sell_Shop_Temp> tempList = cashDAO.listTemp(param);
		depositDAO.queryNumber(param);
		List<T_Sell_DepositList> list = SellVO.buildDepositList(param,tempList);
		depositDAO.payDeposit(param,list);
		cashDAO.clearTemp(param);
	}
	@Override
	public List<T_Sell_Deposit> getDeposit(Map<String, Object> param) {
		cashDAO.clearTemp(param);
		return depositDAO.getDeposit(param);
	}
	@Override
	public void take(Map<String, Object> param) {
		depositDAO.take(param);
	}
	@Override
	public void back(Map<String, Object> param) {
		depositDAO.back(param);
	}

}
