package zy.service.shop.gift;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.base.product.T_Base_Product;
import zy.entity.shop.gift.T_Shop_Gift;
import zy.entity.shop.gift.T_Shop_GiftList;
import zy.entity.shop.gift.T_Shop_Gift_Run;
import zy.entity.shop.gift.T_Shop_Gift_Send;
import zy.entity.sys.user.T_Sys_User;

public interface GiftService {
	PageData<T_Shop_Gift> page(Map<String,Object> param);
	List<T_Shop_GiftList> temp_list(Map<String,Object> param);
	List<T_Shop_GiftList> temp_sum(String us_code,Integer companyid);
	Map<String, Object> temp_size(Map<String,Object> params);
	Map<String, Object> temp_size_title(Map<String, Object> params);
	void deleteTemp(Map<String,Object> param);
	List<String> queryDpCodeByShop(Map<String,Object> param);
	PageData<T_Base_Product> page_product(Map<String, Object> param);
	String temp_loadproduct(Map<String, Object> param);
	void temp_save(Map<String, Object> params);
	void temp_save_modify(Map<String, Object> params);
	void temp_del(Integer gil_id);
	void temp_delByPiCode(T_Shop_GiftList temp);
	void temp_updateAmountById(Map<String, Object> params);
	void temp_updatePointByPdcode(Map<String, Object> params);
	void save(T_Shop_Gift gift,T_Sys_User user);
	void update(T_Shop_Gift gift,T_Sys_User user);
	PageData<T_Shop_Gift_Run> list_run(Map<String,Object> param);
	void show_gift(Map<String, Object> params);
	void del(Map<String, Object> params);
	T_Shop_Gift load(Integer gi_id);
	void initGiftUpdate(Map<String, Object> params);
	void updateGiftListTempAddAmount(Map<String, Object> params);
	Integer getGiftTempSum(Map<String, Object> params);
	PageData<T_Shop_Gift_Send> report_list(Map<String, Object> params);
	/************前台***********/
	PageData<T_Shop_GiftList> sendList(Map<String, Object> params);
	void send(Map<String,Object> param);
}
