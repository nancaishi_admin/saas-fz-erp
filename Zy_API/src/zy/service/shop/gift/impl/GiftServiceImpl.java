package zy.service.shop.gift.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.base.size.SizeDAO;
import zy.dao.shop.gift.GiftDAO;
import zy.dao.vip.member.MemberDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.base.product.T_Base_Product;
import zy.entity.base.size.T_Base_Size;
import zy.entity.base.size.T_Base_SizeList;
import zy.entity.shop.gift.T_Shop_Gift;
import zy.entity.shop.gift.T_Shop_GiftList;
import zy.entity.shop.gift.T_Shop_Gift_Product;
import zy.entity.shop.gift.T_Shop_Gift_Run;
import zy.entity.shop.gift.T_Shop_Gift_Send;
import zy.entity.sys.user.T_Sys_User;
import zy.service.shop.gift.GiftService;
import zy.util.CommonUtil;
import zy.util.StringUtil;
import zy.vo.common.SizeHorizontalVO;
import zy.vo.shop.GiftVO;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
@Service
public class GiftServiceImpl implements GiftService{
	@Resource
	private GiftDAO giftDAO;
	
	@Resource
	private MemberDAO memberDAO;
	@Resource
	private SizeDAO sizeDAO;
	
	@Override
	public PageData<T_Shop_Gift> page(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object pageSize = param.get(CommonUtil.PAGESIZE);
		Object pageIndex = param.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = giftDAO.count(param);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		param.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		param.put(CommonUtil.END, _pageSize);
		
		List<T_Shop_Gift> list = giftDAO.list(param);
		PageData<T_Shop_Gift> pageData = new PageData<T_Shop_Gift>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}

	@Override
	public void deleteTemp(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object us_code = param.get("us_code");
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		if (us_code == null) {
			throw new IllegalArgumentException("参数us_code不能为null");
		}
		giftDAO.deleteTemp(param);
	}

	@Override
	public List<String> queryDpCodeByShop(Map<String, Object> param) {
		return giftDAO.queryDpCodeByShop(param);
	}

	@Override
	public PageData<T_Base_Product> page_product(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object pageSize = param.get(CommonUtil.PAGESIZE);
		Object pageIndex = param.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = giftDAO.count_product(param);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		param.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		param.put(CommonUtil.END, _pageSize);
		
		List<T_Base_Product> list = giftDAO.list_product(param);
		PageData<T_Base_Product> pageData = new PageData<T_Base_Product>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}

	@Override
	public String temp_loadproduct(Map<String, Object> param) {
		String fromJsp = (String)param.get("fromJsp");
		Map<String, Object> resultMap = giftDAO.temp_loadproduct(param);
		T_Base_Product product = (T_Base_Product)resultMap.get("product");
		List<T_Base_Size> sizes=(List<T_Base_Size>)resultMap.get("sizes");
		List<T_Shop_Gift_Product> braColors=(List<T_Shop_Gift_Product>)resultMap.get("braColors");
		List<T_Shop_Gift_Product> amountInputs=(List<T_Shop_Gift_Product>)resultMap.get("amountInputs");
		List<T_Shop_Gift_Product> amountStocks=(List<T_Shop_Gift_Product>)resultMap.get("amountStocks");
		T_Sys_User user = (T_Sys_User)param.get("user");
		Map<String,Object> usableStockMap=null;
		String json = "";
		if("modify".equals(fromJsp)){
			json = GiftVO.getJsonAddProductInfoForModify(product, braColors, sizes, amountInputs, amountStocks,usableStockMap,user);
		}else{
			json = GiftVO.getJsonAddProductInfo(product, braColors, sizes, amountInputs, amountStocks,usableStockMap,user);
		}
		return json;
	}

	@Override
	public List<T_Shop_GiftList> temp_list(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return giftDAO.temp_list(param);
	}

	@Override
	public List<T_Shop_GiftList> temp_sum(String us_code, Integer companyid) {
		return giftDAO.temp_sum(us_code, companyid);
	}

	@Override
	public Map<String, Object> temp_size(Map<String, Object> params) {
		Integer companyid = (Integer)params.get(CommonUtil.COMPANYID);
		List<String> szgCodes = giftDAO.temp_szgcode(params);
		if(szgCodes==null||szgCodes.size()==0){
			return new HashMap<String, Object>();
		}
		List<T_Base_SizeList> sizeGroupList = sizeDAO.listBySzg(szgCodes, companyid);
		params.put(CommonUtil.SIDX, "gil_pd_code,gil_cr_code,gil_bs_code");
		params.put(CommonUtil.SORD, "ASC");
		List<T_Shop_GiftList> temps = giftDAO.temp_list(params);
		return GiftVO.getJsonSizeData_Gift(sizeGroupList, temps);
	}

	@Override
	public Map<String, Object> temp_size_title(Map<String, Object> params) {
		Integer companyid = (Integer)params.get(CommonUtil.COMPANYID);
		List<String> szgCodes = giftDAO.temp_szgcode(params);
		if(szgCodes==null||szgCodes.size()==0){
			Map<String, Object> resultMap = new HashMap<String, Object>();
			resultMap.put("titles", new ArrayList<List<String>>());
			return resultMap;
		}
		List<T_Base_SizeList> sizeGroupList = sizeDAO.listBySzg(szgCodes, companyid);
		return SizeHorizontalVO.getJsonSizeTitles(sizeGroupList);
	}

	@Override
	@Transactional
	public void temp_save(Map<String, Object> params) {
		giftDAO.temp_save(params);
	}

	@Override
	@Transactional
	public void temp_save_modify(Map<String, Object> params) {
		giftDAO.temp_save_modify(params);
	}

	@Override
	@Transactional
	public void temp_del(Integer gil_id) {
		giftDAO.temp_del(gil_id);
	}

	@Override
	@Transactional
	public void temp_delByPiCode(T_Shop_GiftList temp) {
		if (temp.getCompanyid() == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		if (temp.getGil_user_code() == null) {
			throw new IllegalArgumentException("参数us_code不能为null");
		}
		giftDAO.temp_delByPiCode(temp);
	}

	@Override
	public void temp_updateAmountById(Map<String, Object> params) {
		giftDAO.temp_updateAmountById(params);
	}

	@Override
	public void temp_updatePointByPdcode(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		giftDAO.temp_updatePointByPdcode(params);
	}

	@Override
	@Transactional
	public void save(T_Shop_Gift gift, T_Sys_User user) {
		if(user == null){
			throw new IllegalArgumentException("登录超时，请重新登录！");
		}
		if(gift == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		if(StringUtil.isEmpty(gift.getGi_shop_code())){
			throw new IllegalArgumentException("店铺不能为空");
		}
		if(StringUtil.isEmpty(gift.getGi_begindate())){
			throw new IllegalArgumentException("兑换日期不能为空");
		}
		if(StringUtil.isEmpty(gift.getGi_enddate())){
			throw new IllegalArgumentException("兑换日期不能为空");
		}
		giftDAO.save(gift,user);
	}

	@Override
	@Transactional
	public void update(T_Shop_Gift gift, T_Sys_User user) {
		if(user == null){
			throw new IllegalArgumentException("登录超时，请重新登录！");
		}
		if(gift == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		if(StringUtil.isEmpty(gift.getGi_shop_code())){
			throw new IllegalArgumentException("店铺不能为空");
		}
		if(StringUtil.isEmpty(gift.getGi_begindate())){
			throw new IllegalArgumentException("兑换日期不能为空");
		}
		if(StringUtil.isEmpty(gift.getGi_enddate())){
			throw new IllegalArgumentException("兑换日期不能为空");
		}
		giftDAO.update(gift,user);
	}

	@Override
	public PageData<T_Shop_Gift_Run> list_run(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object pageSize = param.get(CommonUtil.PAGESIZE);
		Object pageIndex = param.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = giftDAO.count_run(param);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		param.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		param.put(CommonUtil.END, _pageSize);
		
		List<T_Shop_Gift_Run> list = giftDAO.list_run(param);
		PageData<T_Shop_Gift_Run> pageData = new PageData<T_Shop_Gift_Run>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}

	@Override
	public void show_gift(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		giftDAO.show_gift(params);
	}

	@Override
	@Transactional
	public void del(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		giftDAO.del(params);
	}

	@Override
	public T_Shop_Gift load(Integer gi_id) {
		return giftDAO.load(gi_id);
	}

	@Override
	@Transactional
	public void initGiftUpdate(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		giftDAO.initGiftUpdate(params);
	}

	@Override
	public void updateGiftListTempAddAmount(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		giftDAO.updateGiftListTempAddAmount(params);
	}

	@Override
	public Integer getGiftTempSum(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return giftDAO.getGiftTempSum(params);
	}

	@Override
	public PageData<T_Shop_GiftList> sendList(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object pageSize = param.get(CommonUtil.PAGESIZE);
		Object pageIndex = param.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录");
		}
		
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = giftDAO.sendCount(param);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		param.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		param.put(CommonUtil.END, _pageSize);
		
		List<T_Shop_GiftList> list = giftDAO.sendList(param);
		PageData<T_Shop_GiftList> pageData = new PageData<T_Shop_GiftList>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	@Transactional
	@Override
	public void send(Map<String, Object> param) {
		buildList(param);
		giftDAO.send(param);
		param.put("vpl_type", 8);
		param.put("vpl_remark", "");
		memberDAO.updatePoint(param);
	}
	
	private void buildList(Map<String,Object> param){
		String json = StringUtil.trimString(param.get("json"));
		List<T_Shop_GiftList> list = new ArrayList<T_Shop_GiftList>();
		List<String> ids = new ArrayList<String>();
		if(json.length() > 0){
			JSONArray arr  = JSONArray.parseArray(json);
			if(null != arr && arr.size() > 0){
				for(int i=0;i<arr.size();i++){
					JSONObject obj = arr.getJSONObject(i);
					T_Shop_GiftList gift = new T_Shop_GiftList();
					gift.setGil_id(obj.getInteger("gil_id"));
					gift.setGil_amount(obj.getInteger("gil_amount"));
					list.add(gift);
					ids.add(obj.getString("gil_id"));
				}
			}
		}
		param.put("ids", ids);
		param.put("list", list);
	}

	@Override
	public PageData<T_Shop_Gift_Send> report_list(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object pageSize = param.get(CommonUtil.PAGESIZE);
		Object pageIndex = param.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录");
		}
		
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = giftDAO.report_count(param);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		param.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		param.put(CommonUtil.END, _pageSize);
		
		List<T_Shop_Gift_Send> list = giftDAO.report_list(param);
		PageData<T_Shop_Gift_Send> pageData = new PageData<T_Shop_Gift_Send>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	
	
}
