package zy.service.shop.kpipk;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.shop.kpipk.T_Shop_KpiPk;
import zy.entity.shop.kpipk.T_Shop_KpiPkList;
import zy.entity.sys.user.T_Sys_User;

public interface KpiPkService {
	PageData<T_Shop_KpiPk> page(Map<String, Object> params);
	List<T_Shop_KpiPk> list4cashier(Map<String, Object> params);
	T_Shop_KpiPk load(Integer kp_id);
	Map<String, Object> loadDetail(Map<String, Object> params);
	Map<String, Object> statDetail(Map<String, Object> params);
	void save(T_Shop_KpiPk kpiPk,List<T_Shop_KpiPkList> kpiPkLists, T_Sys_User user);
	T_Shop_KpiPk complete(T_Shop_KpiPk kpiPk,List<T_Shop_KpiPkList> kpiPkLists);
	void del(Integer kp_id);
}
