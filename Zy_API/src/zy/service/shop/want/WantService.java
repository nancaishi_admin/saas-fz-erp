package zy.service.shop.want;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.approve.T_Approve_Record;
import zy.entity.base.product.T_Base_Product;
import zy.entity.shop.want.T_Shop_Want;
import zy.entity.shop.want.T_Shop_WantList;
import zy.entity.sys.user.T_Sys_User;

public interface WantService {
	Integer count(Map<String, Object> params);
	PageData<T_Shop_Want> page(Map<String,Object> params);
	T_Shop_Want load(Integer wt_id);
	List<T_Shop_WantList> detail_list(Map<String, Object> params);
	List<T_Shop_WantList> detail_sum(Map<String, Object> params);
	Map<String, Object> detail_size_title(Map<String, Object> params);
	Map<String, Object> detail_size(Map<String, Object> params);
	List<T_Shop_WantList> temp_list(Map<String, Object> params);
	List<T_Shop_WantList> temp_sum(Integer wt_type,Integer us_id,Integer companyid);
	Map<String, Object> temp_size_title(Map<String, Object> params);
	Map<String, Object> temp_size(Map<String, Object> params);
	PageData<T_Base_Product> page_product(Map<String, Object> param);
	Map<String, Object> temp_loadproduct(Map<String, Object> params);
	Map<String, Object> send_loadproduct(Map<String, Object> params);
	Map<String, Object> temp_save_bybarcode(Map<String, Object> params);
	Map<String, Object> detail_save_bybarcode(Map<String, Object> params);
	void temp_save(Map<String, Object> params);
	void send_save_detail(Map<String, Object> params);
	void temp_import(Map<String, Object> params);
	void temp_copy(Map<String, Object> params);
	void temp_updateAmount(T_Shop_WantList temp);
	void temp_updatePrice(T_Shop_WantList temp);
	void temp_updateRemarkById(T_Shop_WantList temp);
	void temp_updateRemarkByPdCode(T_Shop_WantList temp);
	void temp_del(Integer wtl_id);
	void temp_delByPiCode(T_Shop_WantList temp);
	void temp_clear(Integer wt_type,Integer us_id,Integer companyid);
	void detail_updateAmount(T_Shop_WantList temp);
	void detail_updatePrice(T_Shop_WantList temp);
	void detail_automatch(String number,Integer companyid);
	void detail_del(Integer wtl_id);
	void save(T_Shop_Want want, T_Sys_User user);
	void update(T_Shop_Want want, T_Sys_User user);
	T_Shop_Want approve(String number, T_Approve_Record record, T_Sys_User user);
	T_Shop_Want send(String number,String wt_outdp_code, T_Sys_User user);
	T_Shop_Want receive(Map<String, Object> params);
	T_Shop_Want reject(Map<String, Object> params);
	T_Shop_Want rejectconfirm(String number,T_Sys_User user);
	void initUpdate(String number,Integer wt_type, Integer us_id, Integer companyid);
	void del(String number, Integer companyid);
	Map<String, Object> loadPrintData(String number, Integer sp_id, Integer displayMode,T_Sys_User user);
}
