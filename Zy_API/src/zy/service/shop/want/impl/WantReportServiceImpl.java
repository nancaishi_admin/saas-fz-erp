package zy.service.shop.want.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.shop.want.WantReportDAO;
import zy.dto.shop.want.WantDetailReportDto;
import zy.dto.shop.want.WantReportDto;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.service.shop.want.WantReportService;
import zy.util.CommonUtil;

@Service
public class WantReportServiceImpl implements WantReportService{
	@Resource
	private WantReportDAO wantReportDAO;
	
	@Override
	public PageData<WantReportDto> pageWantReport(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		
		Integer totalCount = wantReportDAO.countWantReport(params);
		PageInfo pageInfo = new PageInfo(totalCount, pageSize, pageIndex);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		
		List<WantReportDto> list = wantReportDAO.listWantReport(params);
		PageData<WantReportDto> pageData = new PageData<WantReportDto>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		pageData.setData(wantReportDAO.sumWantReport(params));
		return pageData;
	}
	
	@Override
	public PageData<WantDetailReportDto> pageWantDetailReport(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		
		Integer totalCount = wantReportDAO.countWantDetailReport(params);
		PageInfo pageInfo = new PageInfo(totalCount, pageSize, pageIndex);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		
		List<WantDetailReportDto> list = wantReportDAO.listWantDetailReport(params);
		PageData<WantDetailReportDto> pageData = new PageData<WantDetailReportDto>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		pageData.setData(wantReportDAO.sumWantDetailReport(params));
		return pageData;
	}
	
}
