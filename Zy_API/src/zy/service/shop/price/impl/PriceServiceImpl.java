package zy.service.shop.price.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.approve.ApproveRecordDAO;
import zy.dao.shop.price.PriceDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.approve.T_Approve_Record;
import zy.entity.shop.price.T_Shop_Price;
import zy.entity.shop.price.T_Shop_PriceList;
import zy.entity.sys.user.T_Sys_User;
import zy.service.shop.price.PriceService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;

@Service
public class PriceServiceImpl implements PriceService{
	
	@Resource
	private PriceDAO priceDAO;
	
	@Resource
	private ApproveRecordDAO approveRecordDAO;
	
	@Override
	public PageData<T_Shop_Price> page(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object pageSize = param.get(CommonUtil.PAGESIZE);
		Object pageIndex = param.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = priceDAO.count(param);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		param.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		param.put(CommonUtil.END, _pageSize);
		
		List<T_Shop_Price> list = priceDAO.list(param);
		PageData<T_Shop_Price> pageData = new PageData<T_Shop_Price>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}

	@Override
	public List<T_Shop_PriceList> price_list(Map<String, Object> param) {
		return priceDAO.price_list(param);
	}

	@Override
	public List<T_Shop_PriceList> temp_list(Map<String, Object> param) {
		return priceDAO.temp_list(param);
	}

	@Override
	@Transactional
	public void save_temp_list(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		priceDAO.save_temp_list(params);
	}

	@Override
	@Transactional
	public String save_tempList_Enter(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("登录超时，请重新登录！");
		}
		return priceDAO.save_tempList_Enter(params);
	}

	@Override
	@Transactional
	public void shop_change_templist(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("登录超时，请重新登录！");
		}
		String shopCodeBefore = (String)params.get("shopCodeBefore");
		String sp_shop_code = (String)params.get("sp_shop_code");
		List<String> delShopCode = new ArrayList<String>();
		List<String> addShopCode = new ArrayList<String>();
		List<String> interShopcode = new ArrayList<String>();
		if(!StringUtil.trimString(shopCodeBefore).equals("")){
			delShopCode.addAll(Arrays.asList(shopCodeBefore.split(",")));
			interShopcode.addAll(Arrays.asList(shopCodeBefore.split(",")));
		}
		if(!StringUtil.trimString(sp_shop_code).equals("")){
			addShopCode.addAll(Arrays.asList(sp_shop_code.split(",")));
		}
		interShopcode.retainAll(addShopCode);//取交集
		delShopCode.removeAll(interShopcode);//去除交集得到待删除的店铺编号
		addShopCode.removeAll(interShopcode);//去除交集得到待增加的店铺编号
		
		params.put("delShopCode", delShopCode);
		params.put("addShopCode", addShopCode);
		priceDAO.shop_change_templist(params);
	}

	@Override
	public void update_templist_byPdCode(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		priceDAO.update_templist_byPdCode(params);
	}

	@Override
	public void savePrice_templist(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Object price_type = params.get("price_type");
		if (price_type == null) {
			throw new IllegalArgumentException("参数price_type不能为null");
		}
		Object prices = params.get("prices");
		if (prices == null) {
			throw new IllegalArgumentException("参数prices不能为null");
		}
		priceDAO.savePrice_templist(params);
	}

	@Override
	public void savePoint_templist(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		priceDAO.savePoint_templist(params);
	}

	@Override
	@Transactional
	public void save(T_Shop_Price shop_Price, T_Sys_User user) {
		if(shop_Price == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		if(StringUtil.isEmpty(shop_Price.getSp_shop_code())){
			throw new IllegalArgumentException("分店列表不能为空");
		}
		if(StringUtil.isEmpty(shop_Price.getSp_manager())){
			throw new IllegalArgumentException("经办人不能为空");
		}
		shop_Price.setSp_us_id(user.getUs_id());
		shop_Price.setSp_maker(user.getUs_name());//制单人
		shop_Price.setSp_sysdate(DateUtil.getCurrentTime());
		shop_Price.setSp_state(0);//状态 默认待审核
		shop_Price.setCompanyid(user.getCompanyid());
		shop_Price.setSp_manager(StringUtil.decodeString(shop_Price.getSp_manager()));
		shop_Price.setSp_remark(StringUtil.decodeString(shop_Price.getSp_remark()));
		
		//1.查临时表
		List<T_Shop_PriceList> temps = priceDAO.temp_list_forsave(user.getUs_id(), user.getCompanyid());
		if(temps == null || temps.size() == 0){
			throw new RuntimeException("订单已保存，请勿重复提交");
		}
		//2.保存主表
		priceDAO.save(shop_Price);
		//3.保存明细子表
		for (T_Shop_PriceList temp : temps) {
			temp.setSpl_number(shop_Price.getSp_number());
		}
		priceDAO.saveList(temps);
		//4.删除临时表
		priceDAO.temp_clear(user.getUs_id(), user.getCompanyid());
	}

	@Override
	@Transactional
	public void update(T_Shop_Price shop_Price, T_Sys_User user) {
		if(shop_Price == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		if(StringUtil.isEmpty(shop_Price.getSp_shop_code())){
			throw new IllegalArgumentException("分店列表不能为空");
		}
		if(StringUtil.isEmpty(shop_Price.getSp_manager())){
			throw new IllegalArgumentException("经办人不能为空");
		}
		shop_Price.setSp_us_id(user.getUs_id());
		shop_Price.setSp_maker(user.getUs_name());//制单人
		shop_Price.setSp_state(0);//状态 默认待审核
		shop_Price.setCompanyid(user.getCompanyid());
		shop_Price.setSp_manager(StringUtil.decodeString(shop_Price.getSp_manager()));
		shop_Price.setSp_remark(StringUtil.decodeString(shop_Price.getSp_remark()));
		
		//1.查临时表
		List<T_Shop_PriceList> temps = priceDAO.temp_list_forsave(user.getUs_id(), user.getCompanyid());
		if(temps == null || temps.size() == 0){
			throw new RuntimeException("订单已保存，请勿重复提交");
		}
		//2验证单据
		T_Shop_Price oldPrice = priceDAO.check(shop_Price.getSp_number(), shop_Price.getCompanyid());
		if (oldPrice == null || !CommonUtil.AR_STATE_FAIL.equals(oldPrice.getSp_state())) {
			throw new RuntimeException("订单已修改，请勿重复提交");
		}
		
		//3删除子表
		priceDAO.deleteList(shop_Price.getSp_number(), user.getCompanyid());
		
		//3.保存主表
		priceDAO.update(shop_Price);
		//4.保存明细子表
		for (T_Shop_PriceList temp : temps) {
			temp.setSpl_number(shop_Price.getSp_number());
		}
		priceDAO.saveList(temps);
		//5.删除临时表
		priceDAO.temp_clear(user.getUs_id(), user.getCompanyid());
	}

	@Override
	public void temp_clear(T_Sys_User user) {
		if(user == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		priceDAO.temp_clear(user.getUs_id(), user.getCompanyid());
	}

	@Override
	public void temp_del(Integer spl_id) {
		priceDAO.temp_del(spl_id);
	}

	@Override
	public T_Shop_Price load(Integer sp_id) {
		T_Shop_Price price = priceDAO.load(sp_id);
		if(price != null){
			T_Approve_Record approve_Record = approveRecordDAO.load(price.getSp_number(), price.getCompanyid());
			if(approve_Record != null){
				price.setAr_describe(approve_Record.getAr_describe());
			}
		}
		return price;
	}

	@Override
	@Transactional
	public T_Shop_Price approve(String number, T_Approve_Record record,
			T_Sys_User user) {
		if (number == null) {
			throw new IllegalArgumentException("参数number不能为null");
		}
		T_Shop_Price price = priceDAO.check(number, user.getCompanyid());
		if(price == null){
			throw new RuntimeException("单据不存在");
		}
		if(!CommonUtil.AR_STATE_NOTAPPROVE.equals(price.getSp_state())){
			throw new RuntimeException("单据已经审核");
		}
		//更新单据审核状态
		price.setSp_state(record.getAr_state());
		priceDAO.updateApprove(price);
		//保存审核记录表
		record.setAr_number(number);
		record.setAr_sysdate(DateUtil.getCurrentTime());
		record.setAr_us_name(user.getUs_name());
		record.setAr_type("t_shop_price");
		record.setCompanyid(user.getCompanyid());
		approveRecordDAO.save(record);
		//审核通过更新分店价格
		if(CommonUtil.AR_STATE_APPROVED.equals(record.getAr_state())){
			priceDAO.update_base_shopprice(price);
		}
		return price;
	}

	@Override
	public void del(String number, Integer companyid) {
		if (number == null) {
			throw new IllegalArgumentException("参数number不能为null");
		}
		T_Shop_Price price = priceDAO.check(number, companyid);
		if(price == null){
			throw new RuntimeException("单据不存在");
		}
		if(!CommonUtil.AR_STATE_NOTAPPROVE.equals(price.getSp_state()) && !CommonUtil.AR_STATE_FAIL.equals(price.getSp_state())){
			throw new RuntimeException("单据已审核通过不能删除！");
		}
		priceDAO.del(number, companyid);
	}

	@Override
	@Transactional
	public void initUpdate(String number, Integer us_id, Integer companyid) {
		List<T_Shop_PriceList> details = priceDAO.detail_list_forsavetemp(number,companyid);
		for(T_Shop_PriceList item:details){
			item.setSpl_us_id(us_id);
		}
		priceDAO.temp_clear(us_id, companyid);
		priceDAO.temp_save(details);
	}
}
