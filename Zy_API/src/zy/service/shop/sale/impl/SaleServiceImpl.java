package zy.service.shop.sale.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.approve.ApproveRecordDAO;
import zy.dao.shop.sale.SaleDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.approve.T_Approve_Record;
import zy.entity.sell.cash.T_Sell_ShopList;
import zy.entity.shop.sale.T_Shop_Sale;
import zy.entity.sys.user.T_Sys_User;
import zy.service.shop.sale.SaleService;
import zy.util.CommonUtil;
import zy.util.DateUtil;

@Service
public class SaleServiceImpl implements SaleService {
	@Resource
	private SaleDAO saleDAO;
	@Resource
	private ApproveRecordDAO approveRecordDAO;
	
	@Override
	public PageData<T_Shop_Sale> page(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object pageSize = param.get(CommonUtil.PAGESIZE);
		Object pageIndex = param.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = saleDAO.count(param);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		param.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		param.put(CommonUtil.END, _pageSize);
		
		List<T_Shop_Sale> list = saleDAO.list(param);
		PageData<T_Shop_Sale> pageData = new PageData<T_Shop_Sale>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}

	@Override
	public Integer queryPriority(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object ss_shop_code = param.get("ss_shop_code");
		Object ss_priority = param.get("ss_priority");
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		if (ss_shop_code == null) {
			throw new IllegalArgumentException("促销店铺不能为null");
		}
		if (ss_priority == null) {
			throw new IllegalArgumentException("促销等级不能为null");
		}
		return saleDAO.queryPriority(param);
	}

	@Override
	@Transactional
	public void save(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object us_name = param.get("us_name");
		Object ifUpdatePriority = param.get("ifUpdatePriority");
		Object ss_shop_codes = param.get("ss_shop_codes");
		T_Shop_Sale t_shop_sale = (T_Shop_Sale)param.get("t_shop_sale");
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		if (us_name == null) {
			throw new IllegalArgumentException("参数us_name不能为null");
		}
		if (ifUpdatePriority == null) {
			throw new IllegalArgumentException("参数ifUpdatePriority不能为null");
		}
		if (ss_shop_codes == null || "".equals(ss_shop_codes)) {
			throw new IllegalArgumentException("促销店铺不能为null");
		}
		if (t_shop_sale == null) {
			throw new IllegalArgumentException("参数t_shop_sale不能为null");
		}
		String ss_name = t_shop_sale.getSs_name();
		String ss_begin_date = t_shop_sale.getSs_begin_date();
		String ss_end_date = t_shop_sale.getSs_end_date();
		String ss_week = t_shop_sale.getSs_week();
		String ss_mt_code = t_shop_sale.getSs_mt_code();
		Integer ss_point = t_shop_sale.getSs_point();
		Integer ss_priority = t_shop_sale.getSs_priority();
		String ss_model = t_shop_sale.getSs_model();
		
		if(ss_name == null || "".equals(ss_name)){
			throw new IllegalArgumentException("促销方案名称不能为null");
		}
		if(ss_begin_date == null || "".equals(ss_begin_date)){
			throw new IllegalArgumentException("促销开始时间不能为null");
		}
		if(ss_end_date == null || "".equals(ss_end_date)){
			throw new IllegalArgumentException("促销结束时间不能为null");
		}
		if(ss_week == null || "".equals(ss_week)){
			throw new IllegalArgumentException("周几促销不能为null");
		}
		if(ss_mt_code == null || "".equals(ss_mt_code)){
			throw new IllegalArgumentException("会员类别不能为null");
		}
		if(ss_point == null){
			throw new IllegalArgumentException("是否积分不能为null");
		}
		if(ss_priority == null){
			throw new IllegalArgumentException("优先级不能为null");
		}
		if(ss_model == null || "".equals(ss_model)){
			throw new IllegalArgumentException("促销模式类型不能为null");
		}
		saleDAO.save(param);
	}

	@Override
	public Map<String, Object> load(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object ss_code = param.get("ss_code");
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		if (ss_code == null || "".equals(ss_code)) {
			throw new IllegalArgumentException("参数促销编号不能为null");
		}
		return saleDAO.load(param);
	}

	@Override
	@Transactional
	public T_Shop_Sale approve(String code, T_Approve_Record record,
			T_Sys_User user) {
		if (code == null) {
			throw new IllegalArgumentException("参数code不能为null");
		}
		T_Shop_Sale sale = saleDAO.check(code, user.getCompanyid());
		if(sale == null){
			throw new RuntimeException("促销方案已经不存在");
		}
		if(!CommonUtil.AR_STATE_NOTAPPROVE.equals(sale.getSs_state())){
			throw new RuntimeException("促销方案已经审核");
		}
		//更新促销方案审核状态
		sale.setSs_state(record.getAr_state());
		saleDAO.updateApprove(sale);
		//保存审核记录表
		record.setAr_number(code);
		record.setAr_sysdate(DateUtil.getCurrentTime());
		record.setAr_us_name(user.getUs_name());
		record.setAr_type("t_shop_sale");
		record.setCompanyid(user.getCompanyid());
		approveRecordDAO.save(record);
		return sale;
	}

	@Override
	public T_Shop_Sale stop(String code, String ec_stop_cause, T_Sys_User user) {
		if (code == null) {
			throw new IllegalArgumentException("参数code不能为null");
		}
		T_Shop_Sale sale = saleDAO.check(code, user.getCompanyid());
		if(sale == null){
			throw new RuntimeException("促销方案已经不存在");
		}
		if(!(CommonUtil.AR_STATE_APPROVED.equals(sale.getSs_state()))){
			throw new RuntimeException("促销方案未审核通过或已终止");
		}
		//更新电子券方案审核状态
		sale.setSs_state(CommonUtil.AR_STATE_STOP);
		saleDAO.updateApprove(sale);
		//保存审核记录表
		T_Approve_Record record = new T_Approve_Record();
		record.setAr_state(CommonUtil.AR_STATE_STOP);
		record.setAr_describe(ec_stop_cause);
		record.setAr_number(code);
		record.setAr_sysdate(DateUtil.getCurrentTime());
		record.setAr_us_name(user.getUs_name());
		record.setAr_type("t_shop_sale");
		record.setCompanyid(user.getCompanyid());
		approveRecordDAO.save(record);
		return sale;
	}

	@Override
	public void del(String ss_code, Integer companyid) {
		if (ss_code == null) {
			throw new IllegalArgumentException("参数ss_code不能为null");
		}
		T_Shop_Sale sale = saleDAO.check(ss_code, companyid);
		if(sale == null){
			throw new RuntimeException("单据不存在");
		}
		if(!CommonUtil.AR_STATE_NOTAPPROVE.equals(sale.getSs_state()) && !CommonUtil.AR_STATE_FAIL.equals(sale.getSs_state())){
			throw new RuntimeException("单据已审核通过不能删除！");
		}
		saleDAO.del(ss_code, companyid);
	}

	@Override
	public List<T_Shop_Sale> list(Map<String, Object> param) {
		return saleDAO.list(param,20);
	}

	@Override
	public PageData<T_Sell_ShopList> rateList(Map<String, Object> paramMap) {
		Object companyid = paramMap.get(CommonUtil.COMPANYID);
		Object pageSize = paramMap.get(CommonUtil.PAGESIZE);
		Object pageIndex = paramMap.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = saleDAO.rateList_count(paramMap);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		paramMap.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		paramMap.put(CommonUtil.END, _pageSize);
		
		List<T_Sell_ShopList> list = saleDAO.rateList_list(paramMap);
		PageData<T_Sell_ShopList> pageData = new PageData<T_Sell_ShopList>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		pageData.setData(saleDAO.rateList_sum(paramMap));
		return pageData;
	}

	@Override
	public PageData<T_Sell_ShopList> totalList(Map<String, Object> paramMap) {
		Object companyid = paramMap.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		List<T_Sell_ShopList> list = saleDAO.totalList_list(paramMap);
		PageData<T_Sell_ShopList> pageData = new PageData<T_Sell_ShopList>();
		pageData.setList(list);
		pageData.setData(saleDAO.totalList_sum(paramMap));
		return pageData;
	}

}
