package zy.service.shop.complaint;

import java.util.Map;

import zy.entity.PageData;
import zy.entity.shop.complaint.T_Shop_Complaint;
import zy.entity.sys.user.T_Sys_User;

public interface ComplaintService {
	PageData<T_Shop_Complaint> page(Map<String,Object> param);
	T_Shop_Complaint load(Integer sc_id);
	void update(T_Shop_Complaint complaint,T_Sys_User user);
}
