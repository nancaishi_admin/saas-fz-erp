package zy.service.shop.complaint.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.shop.complaint.ComplaintDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.shop.complaint.T_Shop_Complaint;
import zy.entity.sys.user.T_Sys_User;
import zy.service.shop.complaint.ComplaintService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;

@Service
public class ComplaintServiceImpl implements ComplaintService {

	@Resource
	private ComplaintDAO complaintDAO;
	
	@Override
	public T_Shop_Complaint load(Integer sc_id) {
		return complaintDAO.load(sc_id);
	}

	@Override
	public PageData<T_Shop_Complaint> page(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object pageSize = param.get(CommonUtil.PAGESIZE);
		Object pageIndex = param.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = complaintDAO.count(param);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		param.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		param.put(CommonUtil.END, _pageSize);
		
		List<T_Shop_Complaint> list = complaintDAO.list(param);
		PageData<T_Shop_Complaint> pageData = new PageData<T_Shop_Complaint>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}

	@Override
	public void update(T_Shop_Complaint complaint, T_Sys_User user) {
		if(complaint == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		if(StringUtil.isEmpty(complaint.getSc_id())){
			throw new IllegalArgumentException("sc_id不能为空!");
		}
		complaint.setSc_processname(user.getUs_name());
		complaint.setSc_acceptdate(DateUtil.getCurrentTime());
		complaint.setSc_processinfo(StringUtil.decodeString(complaint.getSc_processinfo()));
		complaint.setSc_state(1);
		complaintDAO.update(complaint);
	}

}
