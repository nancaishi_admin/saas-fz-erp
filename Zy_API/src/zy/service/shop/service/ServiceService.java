package zy.service.shop.service;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.entity.shop.service.T_Shop_Service;
import zy.entity.shop.service.T_Shop_Service_Item;
import zy.entity.sys.user.T_Sys_User;

public interface ServiceService {
	PageData<T_Shop_Service> page(Map<String,Object> param);
	void save(T_Shop_Service service,T_Sys_User user);
	void save(T_Shop_Service service,T_Sell_Cashier cashier);
	void update(T_Shop_Service service,T_Sys_User user);
	void update(T_Shop_Service service,T_Sell_Cashier cashier);
	void del(Integer ss_id);
	T_Shop_Service load(Integer ss_id);
	PageData<T_Shop_Service_Item> list_serviceitem(Map<String,Object> param);
	List<T_Shop_Service_Item> list_serviceitem_combo(Map<String,Object> param);
	Integer queryItemByName(T_Shop_Service_Item service_Item);
	void save_serviceitem(T_Shop_Service_Item service_Item);
	void update_serviceitem(T_Shop_Service_Item service_Item);
	T_Shop_Service_Item queryServiceItemByID(Integer ssi_id);
	void del_serviceitem(Integer ssi_id);
}
