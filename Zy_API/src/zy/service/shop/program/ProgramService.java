package zy.service.shop.program;

import java.util.Map;

import zy.entity.PageData;
import zy.entity.shop.program.T_Shop_Program;
import zy.entity.sys.user.T_Sys_User;

public interface ProgramService {
	PageData<T_Shop_Program> page(Map<String,Object> param);
	void save(T_Shop_Program program,T_Sys_User user);
	void del(Integer sp_id,Integer sps_id);
	T_Shop_Program load(Map<String,Object> param);
	T_Shop_Program load_cash(Map<String,Object> param);
}
