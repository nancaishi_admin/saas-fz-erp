package zy.service.shop.kpiassess;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.shop.kpiassess.T_Shop_KpiAssess;
import zy.entity.shop.kpiassess.T_Shop_KpiAssessList;
import zy.entity.sys.user.T_Sys_User;

public interface KpiAssessService {
	PageData<T_Shop_KpiAssess> page(Map<String, Object> params);
	List<T_Shop_KpiAssess> list4cashier(Map<String, Object> params);
	T_Shop_KpiAssess load(Integer ka_id);
	Map<String, Object> loadDetail(Map<String, Object> params);
	Map<String, Object> statDetail(Map<String, Object> params);
	void save(T_Shop_KpiAssess kpiAssess,List<T_Shop_KpiAssessList> kpiAssessLists, T_Sys_User user);
	T_Shop_KpiAssess complete(T_Shop_KpiAssess kpiAssess,List<T_Shop_KpiAssessList> kpiAssessLists);
	void del(Integer ka_id);
}
