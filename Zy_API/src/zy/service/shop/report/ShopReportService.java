package zy.service.shop.report;

import java.util.Map;

import zy.entity.PageData;
import zy.entity.sell.day.T_Sell_Day;

public interface ShopReportService {
	PageData<T_Sell_Day> flow(Map<String,Object> paramMap);
}
