package zy.service.shop.report.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.shop.report.ShopReportDAO;
import zy.entity.PageData;
import zy.entity.sell.day.T_Sell_Day;
import zy.service.shop.report.ShopReportService;
import zy.util.NumberUtil;
@Service
public class ShopReportServiceImpl implements ShopReportService{
	@Resource
	private ShopReportDAO shopReportDAO;
	@Override
	public PageData<T_Sell_Day> flow(Map<String, Object> paramMap) {
		List<T_Sell_Day> list = shopReportDAO.flow(paramMap);
		PageData<T_Sell_Day> pageData = new PageData<T_Sell_Day>();
		pageData.setList(list);
		pageData.setData(buildData(list));
		return pageData;
	}
	private Map<String,Object> buildData(List<T_Sell_Day> list){
		Integer come = 0,receive = 0,trys = 0,sh_amount = 0,sh_count = 0;
		Double sh_money = 0d;
		if(null != list && list.size() > 0){
			for(T_Sell_Day day:list){
				come += NumberUtil.toInteger(day.getDa_come());
				receive += NumberUtil.toInteger(day.getDa_receive());
				trys += NumberUtil.toInteger(day.getDa_try());
				sh_amount += NumberUtil.toInteger(day.getSh_amount());
				sh_count += NumberUtil.toInteger(day.getSh_count());
				sh_money += NumberUtil.toDouble(day.getSh_money());
			}
		}
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("da_come", come);
		map.put("da_receive", receive);
		map.put("da_try", trys);
		map.put("sh_amount", sh_amount);
		map.put("sh_count", sh_count);
		map.put("sh_money", sh_money);
		return map;
	}
}
