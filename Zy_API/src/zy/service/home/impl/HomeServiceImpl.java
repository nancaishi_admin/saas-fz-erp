package zy.service.home.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.home.HomeDAO;
import zy.entity.sell.day.T_Sell_Day;
import zy.service.home.HomeService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;

@Service
public class HomeServiceImpl implements HomeService{
	@Resource
	private HomeDAO homeDAO;

	@Override
	public Map<String, Object> loadHomeInfo(Map<String, Object> params) {
		Object shop_type = params.get(CommonUtil.SHOP_TYPE);
		String shop_code = (String)params.get(CommonUtil.SHOP_CODE);//登录用户所属店铺
		List<String> shopCodes = new ArrayList<String>();
		if(CommonUtil.ONE.equals(shop_type) || CommonUtil.TWO.equals(shop_type)){//总公司、分公司
			shopCodes.addAll(homeDAO.listAllShop(params));
		}else {
			shopCodes.add(shop_code);
		}
		params.put("shopCodes", shopCodes);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		//业务简报
		resultMap.put("businessInfo", homeDAO.loadBusinessInfo(params));
		//本月
		params.put("begindate", DateUtil.monthStartDay(DateUtil.getCurrentTime()));
		params.put("enddate", DateUtil.monthEndDay(DateUtil.getCurrentTime()));
		resultMap.put("thisMonth", homeDAO.loadHomeInfo(params));
		//同期
		params.put("begindate", DateUtil.monthStartDay(DateUtil.getDateAddYears(-1)));
		params.put("enddate", DateUtil.monthEndDay(DateUtil.getDateAddYears(-1)));
		resultMap.put("lastMonth", homeDAO.loadHomeInfo(params));
		return resultMap;
	}
	@Override
	public Map<String, Object> statSellByDay(Map<String, Object> params) {
		return homeDAO.statSellByDay(params);
	}
	
	@Override
	public Map<String, Object> statDayKpi(Map<String, Object> params) {
		Object shop_type = params.get(CommonUtil.SHOP_TYPE);
		String shop_code = (String)params.get(CommonUtil.SHOP_CODE);//登录用户所属店铺
		String shopCode = (String)params.get("shopCode");//页面传递
		List<String> shopCodes = new ArrayList<String>();
		if(CommonUtil.ONE.equals(shop_type) || CommonUtil.TWO.equals(shop_type)){//总公司、分公司
			if(StringUtil.isNotEmpty(shopCode)){
				shopCodes.addAll(Arrays.asList(shopCode.split(",")));
			}else {
				shopCodes.addAll(homeDAO.listAllShop(params));
			}
		}else {
			shopCodes.add(shop_code);
		}
		params.put("shopCodes", shopCodes);
		return homeDAO.statDayKpi(params);
	}
	@Override
	public List<T_Sell_Day> statByHour(Map<String, Object> params) {
		Object shop_type = params.get(CommonUtil.SHOP_TYPE);
		String shop_code = (String)params.get(CommonUtil.SHOP_CODE);//登录用户所属店铺
		String shopCode = (String)params.get("shopCode");//页面传递
		List<String> shopCodes = new ArrayList<String>();
		if(CommonUtil.ONE.equals(shop_type) || CommonUtil.TWO.equals(shop_type)){//总公司、分公司
			if(StringUtil.isNotEmpty(shopCode)){
				shopCodes.addAll(Arrays.asList(shopCode.split(",")));
			}else {
				shopCodes.addAll(homeDAO.listAllShop(params));
			}
		}else {
			shopCodes.add(shop_code);
		}
		params.put("shopCodes", shopCodes);
		Map<String, T_Sell_Day> resultMap = homeDAO.statByHour(params);
		
		List<T_Sell_Day> resultList = new ArrayList<T_Sell_Day>();
		T_Sell_Day item = null;
		for (int i = 0; i < 24; i++) {
			if(resultMap.containsKey(String.valueOf(i))){
				item = resultMap.get(String.valueOf(i));
			}else {
				item = new T_Sell_Day();
				item.setDa_come(0);
				item.setSh_money(0d);
			}
			item.setDa_date(String.valueOf(i));
			resultList.add(item);
		}
		return resultList;
	}
	
	@Override
	public Map<String, Object> loadWeather(Map<String, Object> params) {
		return homeDAO.loadWeather(params);
	}
	
}
