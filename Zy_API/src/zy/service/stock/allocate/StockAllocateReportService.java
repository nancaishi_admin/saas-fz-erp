package zy.service.stock.allocate;

import java.util.Map;

import zy.dto.stock.allocate.StockAllocateDetailReportDto;
import zy.dto.stock.allocate.StockAllocateReportDto;
import zy.entity.PageData;

public interface StockAllocateReportService {
	PageData<StockAllocateReportDto> pageAllocateReport(Map<String, Object> params);
	PageData<StockAllocateDetailReportDto> pageAllocateDetailReport(Map<String, Object> params);
}
