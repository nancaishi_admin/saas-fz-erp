package zy.service.stock.check;

import java.util.List;
import java.util.Map;

import zy.dto.stock.check.CheckDiffDto;
import zy.entity.PageData;
import zy.entity.approve.T_Approve_Record;
import zy.entity.base.product.T_Base_Product;
import zy.entity.stock.check.T_Stock_Batch;
import zy.entity.stock.check.T_Stock_Check;
import zy.entity.stock.check.T_Stock_CheckList;
import zy.entity.stock.data.T_Stock_DataView;
import zy.entity.sys.user.T_Sys_User;

public interface CheckService {
	PageData<T_Stock_Batch> batch_page(Map<String,Object> params);
	T_Stock_Batch batch_load(Integer ba_id);
	void batch_save(T_Stock_Batch batch,T_Sys_User user);
	T_Stock_Batch batch_approve(String number, T_Approve_Record record, T_Sys_User user);
	void batch_delete(String number, Integer companyid);
	
	List<T_Stock_Check> check_list4exec(String ba_number,Integer companyid);
	T_Stock_Check check_load(String ck_number,Integer companyid);
	List<T_Stock_CheckList> check_detail_list_all(Map<String, Object> params);
	List<T_Stock_CheckList> check_detail_sum_all(Map<String, Object> params);
	Map<String, Object> check_detail_size_title_all(Map<String, Object> params);
	Map<String, Object> check_detail_size_all(Map<String, Object> params);
	List<T_Stock_CheckList> check_detail_list(Map<String, Object> params);
	List<T_Stock_CheckList> check_detail_sum(Map<String, Object> params);
	Map<String, Object> check_detail_size_title(Map<String, Object> params);
	Map<String, Object> check_detail_size(Map<String, Object> params);
	List<T_Stock_CheckList> check_temp_list(Map<String, Object> params);
	List<T_Stock_CheckList> check_temp_sum(Map<String, Object> params);
	Map<String, Object> check_temp_size_title(Map<String, Object> params);
	Map<String, Object> check_temp_size(Map<String, Object> params);
	Map<String, Object> check_temp_save_bybarcode(Map<String, Object> params);
	PageData<T_Base_Product> page_product(Map<String, Object> param);
	Map<String, Object> temp_loadproduct(Map<String, Object> params);
	void check_temp_save(Map<String, Object> params);
	void check_temp_import(Map<String, Object> params);
	void check_temp_repair(Map<String, Object> params);
	void check_temp_updateAmount(T_Stock_CheckList temp);
	void check_temp_updateRemarkById(T_Stock_CheckList temp);
	void check_temp_updateRemarkByPdCode(T_Stock_CheckList temp);
	void check_temp_del(Integer ckl_id);
	void check_temp_delByPiCode(T_Stock_CheckList temp);
	void check_temp_clear(String ba_number,Integer ckl_isrepair,Integer us_id,Integer companyid);
	void check_temp_clear_all(String ba_number,Integer us_id,Integer companyid);
	void check_save(T_Stock_Check check, T_Sys_User user);
	void check_recheck(String ck_number,T_Sys_User user);
	List<T_Stock_DataView> check_missing(Map<String, Object> params);
	void check_exec(String ba_number,Integer ba_isexec,T_Sys_User user);
	
	PageData<CheckDiffDto> check_diff_list(Map<String,Object> params);
	PageData<CheckDiffDto> check_diff_sum(Map<String,Object> params);
}
