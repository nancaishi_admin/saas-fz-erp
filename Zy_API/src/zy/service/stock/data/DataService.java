package zy.service.stock.data;

import java.util.List;
import java.util.Map;

import jxl.Workbook;
import jxl.write.WritableWorkbook;
import zy.dto.stock.data.ProductSingleTrackDto;
import zy.dto.stock.data.StockAnalysisDto;
import zy.dto.stock.data.StockDataByDepotDto;
import zy.dto.stock.data.StockReportByShopDto;
import zy.dto.stock.data.StockShopDto;
import zy.entity.PageData;
import zy.entity.base.size.T_Base_SizeList;
import zy.entity.stock.data.T_Stock_Data;
import zy.entity.stock.data.T_Stock_DataSizeView;
import zy.entity.stock.data.T_Stock_DataView;
import zy.entity.sys.user.T_Sys_User;

public interface DataService {
	PageData<T_Stock_DataView> page(Map<String, Object> params);
	Map<String, List<T_Base_SizeList>> size_title(Map<String, Object> params);
	PageData<T_Stock_DataSizeView> size_data(Map<String, Object> params);
	PageData<StockDataByDepotDto> page_bydepot(Map<String, Object> params);
	PageData<StockReportByShopDto> page_byshop(Map<String, Object> params);
	List<StockAnalysisDto> list_analysis(Map<String, Object> params);
	List<StockShopDto> stock_shop(Map<String, Object> params);
	List<Map<String, Object>> type_level_stock(Map<String, Object> params);
	PageData<ProductSingleTrackDto> single_track(Map<String, Object> params);
	PageData<T_Stock_DataView> data_list_sale(Map<String, Object> param);
	PageData<Map<String,Object>> otherList(Map<String, Object> param);
	Map<String, Object> parseSavingStockExcel(Workbook soureWorkBook, WritableWorkbook errsheet, T_Sys_User user, String depot_code) throws Exception;
	void save_import_stock(Map<String,Object> param);
	List<T_Stock_Data> listAPI(Map<String,Object> paramMap);
	T_Stock_Data loadByBarcodeAPI(Map<String, Object> paramMap);
	Map<String,Object> queryAPI(Map<String,Object> paramMap);
	List<T_Stock_DataView> otherAPI(Map<String,Object> param);
}
