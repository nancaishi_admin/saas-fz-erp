package zy.service.stock.useable.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.stock.useable.UseableDAO;
import zy.dao.sys.set.SetDAO;
import zy.entity.stock.useable.T_Stock_Useable;
import zy.entity.sys.set.T_Sys_Set;
import zy.entity.sys.user.T_Sys_User;
import zy.service.stock.useable.UseableService;

@Service
public class UseableServiceImpl implements UseableService{
	@Resource
	private SetDAO setDAO;
	@Resource
	private UseableDAO useableDAO;

	@Override
	public List<T_Stock_Useable> list(T_Sys_User user) {
		return useableDAO.list(user);
	}

	@Override
	@Transactional
	public void update(List<T_Stock_Useable> datas, T_Sys_Set set) {
		if (set.getSt_id() == null) {
			setDAO.save(set);
		} else {
			setDAO.updateUseable(set);
		}
		List<T_Stock_Useable> addList = new ArrayList<T_Stock_Useable>();
		List<T_Stock_Useable> updateList = new ArrayList<T_Stock_Useable>();
		for (T_Stock_Useable item : datas) {
			if (item.getUa_id() == null) {
				addList.add(item);
			} else {
				updateList.add(item);
			}
		}
		if (addList.size() > 0) {
			useableDAO.save(addList);
		}
		if (updateList.size() > 0) {
			useableDAO.update(updateList);
		}
	}
}
