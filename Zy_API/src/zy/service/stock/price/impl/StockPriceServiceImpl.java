package zy.service.stock.price.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.approve.ApproveRecordDAO;
import zy.dao.stock.price.StockPriceDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.approve.T_Approve_Record;
import zy.entity.stock.price.T_Stock_Price;
import zy.entity.stock.price.T_Stock_PriceList;
import zy.entity.sys.user.T_Sys_User;
import zy.service.stock.price.StockPriceService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;
import zy.vo.stock.StockPriceVO;

@Service
public class StockPriceServiceImpl implements StockPriceService{
	@Resource
	private StockPriceDAO stockPriceDAO;
	@Resource
	private ApproveRecordDAO approveRecordDAO;
	
	@Override
	public PageData<T_Stock_Price> page(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		Object pageSize = params.get(CommonUtil.PAGESIZE);
		Object pageIndex = params.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = stockPriceDAO.count(params);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		params.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		params.put(CommonUtil.END, _pageSize);
		
		List<T_Stock_Price> list = stockPriceDAO.list(params);
		PageData<T_Stock_Price> pageData = new PageData<T_Stock_Price>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}

	@Override
	public T_Stock_Price load(Integer pc_id) {
		T_Stock_Price price = stockPriceDAO.load(pc_id);
		if(price != null){
			T_Approve_Record approve_Record = approveRecordDAO.load(price.getPc_number(), price.getCompanyid());
			if(approve_Record != null){
				price.setAr_describe(approve_Record.getAr_describe());
			}
		}
		return price;
	}
	
	@Override
	public List<T_Stock_PriceList> detail_list(Map<String, Object> params) {
		return stockPriceDAO.detail_list(params);
	}
	
	@Override
	public List<T_Stock_PriceList> temp_list(Map<String, Object> params) {
		return stockPriceDAO.temp_list(params);
	}
	
	@Override
	@Transactional
	public void temp_save(List<T_Stock_PriceList> products,T_Sys_User user) {
		List<String> temps = stockPriceDAO.temp_check(user.getUs_id(), user.getCompanyid());
		List<T_Stock_PriceList> temp_add = new ArrayList<T_Stock_PriceList>();
		for (T_Stock_PriceList temp : products) {
			if(temps.contains(temp.getPcl_pd_code())){
				continue;
			}
			temp.setPcl_us_id(user.getUs_id());
			temp.setCompanyid(user.getCompanyid());
			temp_add.add(temp);
		}
		if(temp_add.size() == 0){
			throw new RuntimeException("所选商品已在临时表中，请重新选择！");
		}
		stockPriceDAO.temp_save(temp_add);
	}
	
	@Override
	@Transactional
	public void temp_import_draft(Map<String, Object> params) {
		String pc_number = (String)params.get("pc_number");
		T_Sys_User user = (T_Sys_User)params.get("user");
		List<T_Stock_PriceList> details = stockPriceDAO.detail_list_only(pc_number,user.getCompanyid());
		if (details == null || details.size() == 0) {
			throw new RuntimeException("草稿不存在");
		}
		for (T_Stock_PriceList item : details) {
			item.setPcl_us_id(user.getUs_id());
		}
		stockPriceDAO.temp_clear(user.getUs_id(), user.getCompanyid());
		stockPriceDAO.temp_save(details);
		stockPriceDAO.del(pc_number, user.getCompanyid());
	}

	@Override
	@Transactional
	public void temp_update(T_Stock_PriceList temp) {
		stockPriceDAO.temp_update(temp);
	}

	@Override
	@Transactional
	public void temp_del(Integer pcl_id) {
		stockPriceDAO.temp_del(pcl_id);
	}

	@Override
	@Transactional
	public void temp_clear(Integer us_id, Integer companyid) {
		stockPriceDAO.temp_clear(us_id, companyid);
	}
	
	@Override
	@Transactional
	public void save(T_Stock_Price price, T_Sys_User user) {
		if(price == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		if(StringUtil.isEmpty(price.getPc_manager())){
			throw new IllegalArgumentException("经办人不能为空");
		}
		price.setCompanyid(user.getCompanyid());
		price.setPc_us_id(user.getUs_id());
		price.setPc_ar_state(CommonUtil.AR_STATE_NOTAPPROVE);
		price.setPc_sysdate(DateUtil.getCurrentTime());
		//1.查临时表
		List<T_Stock_PriceList> temps = stockPriceDAO.temp_list_forsave(user.getUs_id(), user.getCompanyid());
		if(temps == null || temps.size() == 0){
			throw new RuntimeException("单据已保存，请勿重复提交");
		}
		//2.保存单据
		stockPriceDAO.save(price, temps);
		//3.删除临时表
		stockPriceDAO.temp_clear(user.getUs_id(), user.getCompanyid());
	}
	
	@Override
	@Transactional
	public void update(T_Stock_Price price, T_Sys_User user) {
		if(price == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		if(StringUtil.isEmpty(price.getPc_manager())){
			throw new IllegalArgumentException("经办人不能为空");
		}
		price.setCompanyid(user.getCompanyid());
		price.setPc_us_id(user.getUs_id());
		price.setPc_ar_state(CommonUtil.AR_STATE_NOTAPPROVE);
		price.setPc_sysdate(DateUtil.getCurrentTime());
		//1.1查临时表
		List<T_Stock_PriceList> temps = stockPriceDAO.temp_list_forsave(user.getUs_id(), user.getCompanyid());
		if(temps == null || temps.size() == 0){
			throw new RuntimeException("单据已保存，请勿重复提交");
		}
		//1.2验证单据
		T_Stock_Price oldPrice = stockPriceDAO.check(price.getPc_number(), user.getCompanyid());
		if (oldPrice == null || !CommonUtil.AR_STATE_FAIL.equals(oldPrice.getPc_ar_state())) {
			throw new RuntimeException("单据已修改，请勿重复提交");
		}
		//1.3删除子表
		stockPriceDAO.deleteList(price.getPc_number(), user.getCompanyid());
		//2.保存主表
		stockPriceDAO.update(price, temps);
		//3.删除临时表
		stockPriceDAO.temp_clear(user.getUs_id(), user.getCompanyid());
	}
	
	@Override
	@Transactional
	public T_Stock_Price approve(String number, T_Approve_Record record, T_Sys_User user) {
		if (number == null) {
			throw new IllegalArgumentException("参数number不能为null");
		}
		T_Stock_Price price = stockPriceDAO.check(number, user.getCompanyid());
		if(price == null){
			throw new RuntimeException("单据不存在");
		}
		if(!CommonUtil.AR_STATE_NOTAPPROVE.equals(price.getPc_ar_state())){
			throw new RuntimeException("单据已经审核");
		}
		//1.更新单据审核状态
		price.setPc_ar_state(record.getAr_state());
		price.setPc_ar_date(DateUtil.getCurrentTime());
		stockPriceDAO.updateApprove(price);
		//2.保存审核记录表
		record.setAr_number(number);
		record.setAr_sysdate(DateUtil.getCurrentTime());
		record.setAr_us_name(user.getUs_name());
		record.setAr_type("t_stock_price");
		record.setCompanyid(user.getCompanyid());
		approveRecordDAO.save(record);
		if(!CommonUtil.AR_STATE_APPROVED.equals(price.getPc_ar_state())){//审核不通过，则直接返回
			return price;
		}
		//3.审核通过,更新商品资料成本价
		List<T_Stock_PriceList> details = stockPriceDAO.detail_list_only(number,user.getCompanyid());
		stockPriceDAO.approve_updateProductPrice(details);
		return price;
	}
	
	@Override
	@Transactional
	public void initUpdate(String number, Integer us_id, Integer companyid) {
		List<T_Stock_PriceList> details = stockPriceDAO.detail_list_only(number,companyid);
		for (T_Stock_PriceList item : details) {
			item.setPcl_us_id(us_id);
		}
		stockPriceDAO.temp_clear(us_id, companyid);
		stockPriceDAO.temp_save(details);
	}
	
	@Override
	@Transactional
	public void del(String number, Integer companyid) {
		if (number == null) {
			throw new IllegalArgumentException("参数number不能为null");
		}
		T_Stock_Price price = stockPriceDAO.check(number, companyid);
		if(price == null){
			throw new RuntimeException("单据不存在");
		}
		if(!CommonUtil.AR_STATE_NOTAPPROVE.equals(price.getPc_ar_state()) && !CommonUtil.AR_STATE_FAIL.equals(price.getPc_ar_state())){
			throw new RuntimeException("单据已审核通过不能删除！");
		}
		stockPriceDAO.del(number, companyid);
	}
	
	@Override
	public Map<String, Object> print(String number, T_Sys_User user) {
		if (number == null) {
			throw new IllegalArgumentException("参数number不能为null");
		}
		T_Stock_Price price = stockPriceDAO.load(number, user.getCompanyid());
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("pcl_number", number);
		params.put("companyid", user.getCompanyid());
		List<T_Stock_PriceList> details = stockPriceDAO.detail_list(params);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("header", StockPriceVO.getPrintHeaderHtml(price));
		resultMap.put("body", StockPriceVO.getPrintListHTML(details, user));
		return resultMap;
	}
	
}
