package zy.service.stock.price;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.approve.T_Approve_Record;
import zy.entity.stock.price.T_Stock_Price;
import zy.entity.stock.price.T_Stock_PriceList;
import zy.entity.sys.user.T_Sys_User;

public interface StockPriceService {
	PageData<T_Stock_Price> page(Map<String,Object> params);
	T_Stock_Price load(Integer pc_id);
	List<T_Stock_PriceList> detail_list(Map<String, Object> params);
	List<T_Stock_PriceList> temp_list(Map<String, Object> params);
	void temp_save(List<T_Stock_PriceList> products,T_Sys_User user);
	void temp_import_draft(Map<String, Object> params);
	void temp_update(T_Stock_PriceList temp);
	void temp_del(Integer pcl_id);
	void temp_clear(Integer us_id,Integer companyid);
	void save(T_Stock_Price price, T_Sys_User user);
	void update(T_Stock_Price price, T_Sys_User user);
	T_Stock_Price approve(String number, T_Approve_Record record, T_Sys_User user);
	void initUpdate(String number, Integer us_id, Integer companyid);
	void del(String number, Integer companyid);
	Map<String, Object> print(String number, T_Sys_User user);
}
