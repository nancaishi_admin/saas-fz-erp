package zy.service.stock.adjust.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.approve.ApproveRecordDAO;
import zy.dao.base.product.ProductDAO;
import zy.dao.base.size.SizeDAO;
import zy.dao.stock.adjust.AdjustDAO;
import zy.dao.stock.data.DataDAO;
import zy.dao.stock.useable.UseableDAO;
import zy.dao.sys.print.PrintDAO;
import zy.dto.common.ProductDto;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.approve.T_Approve_Record;
import zy.entity.base.product.T_Base_Barcode;
import zy.entity.base.product.T_Base_Product;
import zy.entity.base.size.T_Base_Size;
import zy.entity.base.size.T_Base_SizeList;
import zy.entity.stock.T_Stock_Import;
import zy.entity.stock.adjust.T_Stock_Adjust;
import zy.entity.stock.adjust.T_Stock_AdjustList;
import zy.entity.stock.data.T_Stock_DataBill;
import zy.entity.sys.set.T_Sys_Set;
import zy.entity.sys.user.T_Sys_User;
import zy.service.stock.adjust.AdjustService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;
import zy.vo.common.SizeHorizontalVO;
import zy.vo.stock.AdjustVO;

@Service
public class AdjustServiceImpl implements AdjustService{
	@Resource
	private AdjustDAO adjustDAO;
	
	@Resource
	private SizeDAO sizeDAO;
	
	@Resource
	private ApproveRecordDAO approveRecordDAO;

	@Resource
	private DataDAO dataDAO;
	
	@Resource
	private ProductDAO productDAO;
	
	@Resource
	private PrintDAO printDAO;
	
	@Resource
	private UseableDAO useableDAO;
	
	@Override
	public PageData<T_Stock_Adjust> page(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		Object pageSize = params.get(CommonUtil.PAGESIZE);
		Object pageIndex = params.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = adjustDAO.count(params);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		params.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		params.put(CommonUtil.END, _pageSize);
		
		List<T_Stock_Adjust> list = adjustDAO.list(params);
		PageData<T_Stock_Adjust> pageData = new PageData<T_Stock_Adjust>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}

	@Override
	public T_Stock_Adjust load(Integer aj_id) {
		T_Stock_Adjust adjust = adjustDAO.load(aj_id);
		if(adjust != null){
			T_Approve_Record approve_Record = approveRecordDAO.load(adjust.getAj_number(), adjust.getCompanyid());
			if(approve_Record != null){
				adjust.setAr_describe(approve_Record.getAr_describe());
			}
		}
		return adjust;
	}
	
	@Override
	public List<T_Stock_AdjustList> detail_list(Map<String, Object> params) {
		return adjustDAO.detail_list(params);
	}
	
	@Override
	public List<T_Stock_AdjustList> detail_sum(Map<String, Object> params) {
		return adjustDAO.detail_sum(params);
	}
	
	@Override
	public Map<String, Object> detail_size_title(Map<String, Object> params) {
		Integer companyid = (Integer)params.get(CommonUtil.COMPANYID);
		List<String> szgCodes = adjustDAO.detail_szgcode(params);
		if(szgCodes==null||szgCodes.size()==0){
			Map<String, Object> resultMap = new HashMap<String, Object>();
			resultMap.put("titles", new ArrayList<List<String>>());
			return resultMap;
		}
		List<T_Base_SizeList> sizeGroupList = sizeDAO.listBySzg(szgCodes, companyid);
		return SizeHorizontalVO.getJsonSizeTitles(sizeGroupList);
	}
	
	@Override
	public Map<String, Object> detail_size(Map<String, Object> params) {
		Integer companyid = (Integer)params.get(CommonUtil.COMPANYID);
		List<String> szgCodes = adjustDAO.detail_szgcode(params);
		if(szgCodes==null||szgCodes.size()==0){
			return new HashMap<String, Object>();
		}
		List<T_Base_SizeList> sizeGroupList = sizeDAO.listBySzg(szgCodes, companyid);
		params.put(CommonUtil.SIDX, "ajl_pd_code,ajl_cr_code,ajl_br_code");
		params.put(CommonUtil.SORD, "ASC");
		List<T_Stock_AdjustList> temps = adjustDAO.detail_list(params);
		return AdjustVO.getJsonSizeData(sizeGroupList, temps);
	}
	
	@Override
	public List<T_Stock_AdjustList> temp_list(Map<String, Object> params) {
		return adjustDAO.temp_list(params);
	}

	@Override
	public List<T_Stock_AdjustList> temp_sum(Map<String, Object> params) {
		return adjustDAO.temp_sum(params);
	}

	@Override
	public Map<String, Object> temp_size_title(Map<String, Object> params) {
		Integer companyid = (Integer)params.get(CommonUtil.COMPANYID);
		List<String> szgCodes = adjustDAO.temp_szgcode(params);
		if(szgCodes==null||szgCodes.size()==0){
			Map<String, Object> resultMap = new HashMap<String, Object>();
			resultMap.put("titles", new ArrayList<List<String>>());
			return resultMap;
		}
		List<T_Base_SizeList> sizeGroupList = sizeDAO.listBySzg(szgCodes, companyid);
		return SizeHorizontalVO.getJsonSizeTitles(sizeGroupList);
	}

	@Override
	public Map<String, Object> temp_size(Map<String, Object> params) {
		Integer companyid = (Integer)params.get(CommonUtil.COMPANYID);
		List<String> szgCodes = adjustDAO.temp_szgcode(params);
		if(szgCodes==null||szgCodes.size()==0){
			return new HashMap<String, Object>();
		}
		List<T_Base_SizeList> sizeGroupList = sizeDAO.listBySzg(szgCodes, companyid);
		params.put(CommonUtil.SIDX, "ajl_pd_code,ajl_cr_code,ajl_br_code");
		params.put(CommonUtil.SORD, "ASC");
		List<T_Stock_AdjustList> temps = adjustDAO.temp_list(params);
		return AdjustVO.getJsonSizeData(sizeGroupList, temps);
	}
	
	@Override
	@Transactional
	public Map<String, Object> temp_save_bybarcode(Map<String, Object> params) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String barcode = (String)params.get("barcode");
		Integer amount = (Integer)params.get("amount");
		T_Sys_User user = (T_Sys_User)params.get("user");
		T_Base_Barcode base_Barcode = productDAO.loadBarcode(barcode, user.getCompanyid());
		if(base_Barcode == null){
			resultMap.put("result", 3);//条码不存在
			return resultMap;
		}
		T_Stock_AdjustList temp = adjustDAO.temp_loadBySubCode(base_Barcode.getBc_subcode(), user.getUs_id(), user.getCompanyid());
		if (temp != null) {//临时表存在则直接更新数量
			temp.setAjl_amount(temp.getAjl_amount()+amount);
			adjustDAO.temp_update(temp);
			resultMap.put("result", 1);//update
			resultMap.put("temp", temp);
			return resultMap;
		}
		T_Base_Product base_Product = adjustDAO.load_product(base_Barcode.getBc_pd_code(), user.getCompanyid());
		Double unitPrice = adjustDAO.temp_queryUnitPrice(base_Barcode.getBc_pd_code(), user.getUs_id(), user.getCompanyid());;
		if(unitPrice == null){
			unitPrice = base_Product.getPd_cost_price();
		}
		temp = new T_Stock_AdjustList();
		temp.setAjl_pd_code(base_Barcode.getBc_pd_code());
		temp.setAjl_cr_code(base_Barcode.getBc_color());
		temp.setAjl_sz_code(base_Barcode.getBc_size());
		temp.setAjl_szg_code(base_Product.getPd_szg_code());
		temp.setAjl_br_code(base_Barcode.getBc_bra());
		temp.setAjl_sub_code(temp.getAjl_pd_code()+temp.getAjl_cr_code()+temp.getAjl_sz_code()+temp.getAjl_br_code());
		temp.setAjl_amount(amount);
		temp.setAjl_unitprice(unitPrice);
		temp.setAjl_remark("");
		temp.setAjl_us_id(user.getUs_id());
		temp.setCompanyid(user.getCompanyid());
		adjustDAO.temp_save(temp);
		temp.setPd_no(base_Product.getPd_no());
		temp.setPd_name(base_Product.getPd_name());
		temp.setPd_unit(base_Product.getPd_unit());
		temp.setBd_name(base_Product.getPd_bd_name());
		temp.setTp_name(base_Product.getPd_tp_name());
		temp.setCr_name(base_Barcode.getBc_colorname());
		temp.setSz_name(base_Barcode.getBc_sizename());
		temp.setBr_name(base_Barcode.getBc_braname());
		resultMap.put("result", 2);//add
		resultMap.put("temp", temp);
		return resultMap;
	}
	
	@Override
	public PageData<T_Base_Product> page_product(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object pageSize = param.get(CommonUtil.PAGESIZE);
		Object pageIndex = param.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = adjustDAO.count_product(param);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		param.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		param.put(CommonUtil.END, _pageSize);
		
		List<T_Base_Product> list = adjustDAO.list_product(param);
		PageData<T_Base_Product> pageData = new PageData<T_Base_Product>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> temp_loadproduct(Map<String, Object> params) {
		Integer companyid = (Integer)params.get(CommonUtil.COMPANYID);
		T_Sys_Set sysSet = (T_Sys_Set)params.get(CommonUtil.KEY_SYSSET);
		Integer us_id = (Integer)params.get("us_id");
		String pd_code = (String)params.get("pd_code");
		String dp_code = (String)params.get("dp_code");
		String exist = (String)params.get("exist");
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Map<String, Object> resultMap = new HashMap<String, Object>();
		T_Base_Product base_Product = adjustDAO.load_product(pd_code, companyid);
		Map<String, Object> product = new HashMap<String, Object>();
		product.put("pd_id", base_Product.getPd_id());
		product.put("pd_code", base_Product.getPd_code());
		product.put("pd_no", base_Product.getPd_no());
		product.put("pd_name", base_Product.getPd_name());
		product.put("pd_szg_code", base_Product.getPd_szg_code());
		product.put("pd_unit", base_Product.getPd_unit());
		product.put("pd_year", base_Product.getPd_year());
		product.put("pd_season", base_Product.getPd_season());
		product.put("pd_sell_price", base_Product.getPd_sell_price());
		product.put("pdm_buy_price", base_Product.getPd_buy_price());
		product.put("pdm_cost_price", base_Product.getPd_cost_price());
		product.put("pd_bd_name", base_Product.getPd_bd_name());
		product.put("pd_tp_name", base_Product.getPd_tp_name());
		product.put("pdm_img_path", StringUtil.trimString(base_Product.getPdm_img_path()));
		Double unitPrice = null;
		if("1".equals(exist)){//已经录入从临时表中查询单价
			unitPrice = adjustDAO.temp_queryUnitPrice(pd_code, us_id, companyid);
			product.put("temp_unitPrice", StringUtil.trimString(unitPrice));
		}
		if(unitPrice == null){
			unitPrice = base_Product.getPd_cost_price();
		}
		product.put("unitPrice", unitPrice);
		resultMap.put("product", product);
		params.put("szg_code", base_Product.getPd_szg_code());
		Map<String, Object> productMap = adjustDAO.load_product_size(params);
		List<T_Base_Size> sizes=(List<T_Base_Size>)productMap.get("sizes");
		List<ProductDto> inputs=(List<ProductDto>)productMap.get("inputs");
		List<ProductDto> temps=(List<ProductDto>)productMap.get("temps");
		List<ProductDto> stocks=(List<ProductDto>)productMap.get("stocks");
		Map<String,Object> usableStockMap = null;
		if (sysSet.getSt_useable() != null && sysSet.getSt_useable().intValue() == 1) {
			usableStockMap = useableDAO.loadUseableStock(pd_code, dp_code, companyid);
		}
		resultMap.putAll(SizeHorizontalVO.buildJsonProductInput(pd_code, sizes, inputs, stocks, temps, usableStockMap));
		return resultMap;
	}
	
	@Override
	@Transactional
	public void temp_save(Map<String, Object> params) {
		List<T_Stock_AdjustList> temps = (List<T_Stock_AdjustList>)params.get("temps");
		if (temps != null && temps.size() > 0) {
			List<T_Stock_AdjustList> temps_add = new ArrayList<T_Stock_AdjustList>();
			List<T_Stock_AdjustList> temps_update = new ArrayList<T_Stock_AdjustList>();
			List<T_Stock_AdjustList> temps_del = new ArrayList<T_Stock_AdjustList>();
			for (T_Stock_AdjustList item : temps) {
				if ("add".equals(item.getOperate_type())){
					if (item.getAjl_amount() != 0) {
						temps_add.add(item);
					}
				}else if("update".equals(item.getOperate_type())){
					if(!item.getAjl_amount().equals(0)){
						temps_update.add(item);
					}else {
						temps_del.add(item);
					}
				}
			}
			if (temps_add.size() > 0) {
				adjustDAO.temp_save(temps_add);
			}
			if (temps_update.size() > 0) {
				adjustDAO.temp_update(temps_update);
			}
			if (temps_del.size() > 0) {
				adjustDAO.temp_del(temps_del);
			}
		}
	}
	
	@Override
	@Transactional
	public void temp_import(Map<String, Object> params) {
		List<String[]> datas = (List<String[]>)params.get("datas");
		T_Sys_User user = (T_Sys_User)params.get("user");
		List<String> barcodes = new ArrayList<String>();
		Map<String, Integer> data_amount = new HashMap<String, Integer>();
		for (String[] data : datas) {
			barcodes.add(data[0]);
			data_amount.put(data[0], Integer.parseInt(data[1]));
		}
		List<T_Stock_Import> imports = adjustDAO.temp_listByImport(barcodes, user.getCompanyid());
		if (imports == null || imports.size() == 0) {
			throw new RuntimeException("无数据可导入");
		}
		Map<String, Double> unitPriceMap = new HashMap<String, Double>();
		Map<String, T_Stock_AdjustList> tempsMap = new HashMap<String, T_Stock_AdjustList>();
		List<T_Stock_AdjustList> temps = adjustDAO.temp_list_forimport(user.getUs_id(), user.getCompanyid());
		for (T_Stock_AdjustList temp : temps) {
			if(!unitPriceMap.containsKey(temp.getAjl_pd_code())){
				unitPriceMap.put(temp.getAjl_pd_code(), temp.getAjl_unitprice());
			}
			tempsMap.put(temp.getAjl_sub_code(), temp);
		}
		List<T_Stock_AdjustList> temps_add = new ArrayList<T_Stock_AdjustList>();
		List<T_Stock_AdjustList> temps_update = new ArrayList<T_Stock_AdjustList>();
		for (T_Stock_Import item : imports) {
			if(tempsMap.containsKey(item.getBc_subcode())){//临时表已存在，更新数量
				T_Stock_AdjustList temp = tempsMap.get(item.getBc_subcode());
				temp.setAjl_amount(temp.getAjl_amount()+data_amount.get(item.getBc_barcode()));
				temps_update.add(temp);
			}else {//临时表不存在，新增数据
				T_Stock_AdjustList temp = new T_Stock_AdjustList();
				temp.setAjl_pd_code(item.getBc_pd_code());
				temp.setAjl_sub_code(item.getBc_subcode());
				temp.setAjl_sz_code(item.getBc_size());
				temp.setAjl_szg_code(item.getPd_szg_code());
				temp.setAjl_cr_code(item.getBc_color());
				temp.setAjl_br_code(item.getBc_bra());
				temp.setAjl_amount(data_amount.get(item.getBc_barcode()));
				if(unitPriceMap.containsKey(item.getBc_pd_code())){//临时表已存在此货号，则使用临时表价格
					temp.setAjl_unitprice(unitPriceMap.get(item.getBc_pd_code()));
				}else{
					temp.setAjl_unitprice(item.getUnit_price());
				}
				temp.setAjl_remark("");
				temp.setAjl_us_id(user.getUs_id());
				temp.setCompanyid(user.getCompanyid());
				temps_add.add(temp);
			}
		}
		if (temps_add.size() > 0) {
			adjustDAO.temp_save(temps_add);
		}
		if (temps_update.size() > 0) {
			adjustDAO.temp_updateById(temps_update);
		}
	}
	
	@Override
	@Transactional
	public void temp_import_draft(Map<String, Object> params) {
		String aj_number = (String)params.get("aj_number");
		T_Sys_User user = (T_Sys_User)params.get("user");
		List<T_Stock_AdjustList> details = adjustDAO.detail_list_forsavetemp(aj_number,user.getCompanyid());
		if (details == null || details.size() == 0) {
			throw new RuntimeException("草稿不存在");
		}
		for(T_Stock_AdjustList item:details){
			item.setAjl_us_id(user.getUs_id());
		}
		adjustDAO.temp_clear(user.getUs_id(), user.getCompanyid());
		adjustDAO.temp_save(details);
		adjustDAO.del(aj_number, user.getCompanyid());
	}
	
	@Override
	@Transactional
	public void temp_updateAmount(T_Stock_AdjustList temp) {
		adjustDAO.temp_update(temp);
	}
	
	@Override
	@Transactional
	public void temp_updateRemarkById(T_Stock_AdjustList temp) {
		adjustDAO.temp_updateRemarkById(temp);
	}
	
	@Override
	@Transactional
	public void temp_updateRemarkByPdCode(T_Stock_AdjustList temp) {
		adjustDAO.temp_updateRemarkByPdCode(temp);
	}
	
	@Override
	@Transactional
	public void temp_del(Integer ajl_id) {
		adjustDAO.temp_del(ajl_id);
	}
	
	@Override
	@Transactional
	public void temp_delByPiCode(T_Stock_AdjustList temp) {
		if (temp.getCompanyid() == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		if (temp.getAjl_us_id() == null) {
			throw new IllegalArgumentException("参数us_id不能为null");
		}
		adjustDAO.temp_delByPiCode(temp);
	}
	
	@Override
	@Transactional
	public void temp_clear(Integer us_id,Integer companyid) {
		adjustDAO.temp_clear(us_id, companyid);;
	}
	
	@Override
	@Transactional
	public void save(T_Stock_Adjust adjust, T_Sys_User user) {
		if(adjust == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		if(StringUtil.isEmpty(adjust.getAj_dp_code())){
			throw new IllegalArgumentException("仓库不能为空");
		}
		if(StringUtil.isEmpty(adjust.getAj_manager())){
			throw new IllegalArgumentException("经办人不能为空");
		}
		adjust.setCompanyid(user.getCompanyid());
		adjust.setAj_us_id(user.getUs_id());
		adjust.setAj_ar_state(CommonUtil.AR_STATE_NOTAPPROVE);
		adjust.setAj_sysdate(DateUtil.getCurrentTime());
		//1.查临时表
		List<T_Stock_AdjustList> temps = adjustDAO.temp_list_forsave(user.getUs_id(), user.getCompanyid());
		if(temps == null || temps.size() == 0){
			throw new RuntimeException("单据已保存，请勿重复提交");
		}
		//2.保存单据
		int aj_amount = 0;
		double aj_money = 0d;
		for (T_Stock_AdjustList temp : temps) {
			aj_amount += temp.getAjl_amount();
			aj_money += temp.getAjl_amount() * temp.getAjl_unitprice();
		}
		adjust.setAj_amount(aj_amount);
		adjust.setAj_money(aj_money);
		adjustDAO.save(adjust, temps);
		//3.删除临时表
		adjustDAO.temp_clear(user.getUs_id(), user.getCompanyid());
	}
	
	@Override
	@Transactional
	public void update(T_Stock_Adjust adjust, T_Sys_User user) {
		if(adjust == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		if(StringUtil.isEmpty(adjust.getAj_dp_code())){
			throw new IllegalArgumentException("仓库不能为空");
		}
		if(StringUtil.isEmpty(adjust.getAj_manager())){
			throw new IllegalArgumentException("经办人不能为空");
		}
		adjust.setCompanyid(user.getCompanyid());
		adjust.setAj_us_id(user.getUs_id());
		adjust.setAj_ar_state(CommonUtil.AR_STATE_NOTAPPROVE);
		adjust.setAj_sysdate(DateUtil.getCurrentTime());
		//1.1查临时表
		List<T_Stock_AdjustList> temps = adjustDAO.temp_list_forsave(user.getUs_id(), user.getCompanyid());
		if(temps == null || temps.size() == 0){
			throw new RuntimeException("单据已保存，请勿重复提交");
		}
		
		//1.2验证单据
		T_Stock_Adjust oldAdjust = adjustDAO.check(adjust.getAj_number(), user.getCompanyid());
		if (oldAdjust == null || !CommonUtil.AR_STATE_FAIL.equals(oldAdjust.getAj_ar_state())) {
			throw new RuntimeException("单据已修改，请勿重复提交");
		}
		//1.3删除子表
		adjustDAO.deleteList(adjust.getAj_number(), user.getCompanyid());
		//2.保存主表
		int aj_amount = 0;
		double aj_money = 0d;
		for (T_Stock_AdjustList temp : temps) {
			aj_amount += temp.getAjl_amount();
			aj_money += temp.getAjl_amount() * temp.getAjl_unitprice();
		}
		adjust.setAj_amount(aj_amount);
		adjust.setAj_money(aj_money);
		adjustDAO.update(adjust, temps);
		//3.删除临时表
		adjustDAO.temp_clear(user.getUs_id(), user.getCompanyid());
	}
	
	@Override
	@Transactional
	public T_Stock_Adjust approve(String number, T_Approve_Record record, T_Sys_User user) {
		if (number == null) {
			throw new IllegalArgumentException("参数number不能为null");
		}
		T_Stock_Adjust adjust = adjustDAO.check(number, user.getCompanyid());
		if(adjust == null){
			throw new RuntimeException("单据不存在");
		}
		if(!CommonUtil.AR_STATE_NOTAPPROVE.equals(adjust.getAj_ar_state())){
			throw new RuntimeException("单据已经审核");
		}
		//1.更新单据审核状态
		adjust.setAj_ar_state(record.getAr_state());
		adjust.setAj_ar_date(DateUtil.getCurrentTime());
		adjustDAO.updateApprove(adjust);
		//2.保存审核记录表
		record.setAr_number(number);
		record.setAr_sysdate(DateUtil.getCurrentTime());
		record.setAr_us_name(user.getUs_name());
		record.setAr_type("t_stock_adjust");
		record.setCompanyid(user.getCompanyid());
		approveRecordDAO.save(record);
		if(!CommonUtil.AR_STATE_APPROVED.equals(adjust.getAj_ar_state())){//审核不通过，则直接返回
			return adjust;
		}
		//3.审核通过,更新库存
		List<T_Stock_DataBill> stocks = adjustDAO.listStock(number, adjust.getAj_dp_code(), user.getCompanyid());
		List<T_Stock_DataBill> add_stocks = new ArrayList<T_Stock_DataBill>();
		List<T_Stock_DataBill> update_stocks = new ArrayList<T_Stock_DataBill>();
		for (T_Stock_DataBill stock : stocks) {
			if(stock.getSd_id() == null){
				stock.setSd_dp_code(adjust.getAj_dp_code());
				stock.setSd_init(0);
				stock.setSd_date(DateUtil.getCurrentTime());
				stock.setCompanyid(user.getCompanyid());
				stock.setSd_amount(stock.getBill_amount());
				add_stocks.add(stock);
			}else {
				stock.setSd_amount(stock.getSd_amount() + stock.getBill_amount());
				update_stocks.add(stock);
			}
		}
		if (add_stocks.size() > 0) {
			dataDAO.save(add_stocks);
		}
		if (update_stocks.size() > 0) {
			dataDAO.update(update_stocks);
		}
		return adjust;
	}
	
	@Override
	@Transactional
	public T_Stock_Adjust reverse(String number, T_Sys_User user) {
		if (number == null) {
			throw new IllegalArgumentException("参数number不能为null");
		}
		T_Stock_Adjust adjust = adjustDAO.check(number, user.getCompanyid());
		if(adjust == null){
			throw new RuntimeException("单据不存在");
		}
		if(!CommonUtil.AR_STATE_APPROVED.equals(adjust.getAj_ar_state())){
			throw new RuntimeException("单据未审核或审核未通过");
		}
		//1.更新单据审核状态
		adjust.setAj_ar_state(CommonUtil.AR_STATE_NOTAPPROVE);
		adjust.setAj_ar_date(DateUtil.getCurrentTime());
		adjustDAO.updateApprove(adjust);
		//2.保存审核记录表
		T_Approve_Record record = new T_Approve_Record();
		record.setAr_state(CommonUtil.AR_STATE_REVERSE_APPROVE);
		record.setAr_describe(user.getUs_name()+"反审核单据");
		record.setAr_number(number);
		record.setAr_sysdate(DateUtil.getCurrentTime());
		record.setAr_us_name(user.getUs_name());
		record.setAr_type("t_stock_adjust");
		record.setCompanyid(user.getCompanyid());
		approveRecordDAO.save(record);
		//3更新库存
		List<T_Stock_DataBill> stocks = adjustDAO.listStock(number, adjust.getAj_dp_code(), user.getCompanyid());
		List<T_Stock_DataBill> add_stocks = new ArrayList<T_Stock_DataBill>();
		List<T_Stock_DataBill> update_stocks = new ArrayList<T_Stock_DataBill>();
		for (T_Stock_DataBill stock : stocks) {
			if(stock.getSd_id() == null){
				stock.setSd_dp_code(adjust.getAj_dp_code());
				stock.setSd_init(0);
				stock.setSd_date(DateUtil.getCurrentTime());
				stock.setCompanyid(user.getCompanyid());
				stock.setSd_amount(-stock.getBill_amount());
				add_stocks.add(stock);
			}else {
				stock.setSd_amount(stock.getSd_amount() - stock.getBill_amount());
				update_stocks.add(stock);
			}
		}
		if (add_stocks.size() > 0) {
			dataDAO.save(add_stocks);
		}
		if (update_stocks.size() > 0) {
			dataDAO.update(update_stocks);
		}
		return adjust;
	}
	
	@Override
	@Transactional
	public void initUpdate(String number, Integer us_id, Integer companyid) {
		List<T_Stock_AdjustList> details = adjustDAO.detail_list_forsavetemp(number,companyid);
		for(T_Stock_AdjustList item:details){
			item.setAjl_us_id(us_id);
		}
		adjustDAO.temp_clear(us_id, companyid);
		adjustDAO.temp_save(details);
	}
	
	@Override
	@Transactional
	public void del(String number, Integer companyid) {
		if (number == null) {
			throw new IllegalArgumentException("参数number不能为null");
		}
		T_Stock_Adjust adjust = adjustDAO.check(number, companyid);
		if(adjust == null){
			throw new RuntimeException("单据不存在");
		}
		if(!CommonUtil.AR_STATE_NOTAPPROVE.equals(adjust.getAj_ar_state()) && !CommonUtil.AR_STATE_FAIL.equals(adjust.getAj_ar_state())){
			throw new RuntimeException("单据已审核通过不能删除！");
		}
		adjustDAO.del(number, companyid);
	}
	
	@Override
	public Map<String, Object> loadPrintData(String number, Integer sp_id, Integer displayMode, T_Sys_User user) {
		Map<String, Object> resultMap = printDAO.loadPrint4Bill(sp_id);
		T_Stock_Adjust adjust = adjustDAO.load(number,user.getCompanyid());
		List<T_Stock_AdjustList> adjustList = null;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("ajl_number", number);
		params.put("companyid", user.getCompanyid());
		if(displayMode.intValue() == 2){//汇总模式
			adjustList = adjustDAO.detail_sum(params);
		}else {
			adjustList = adjustDAO.detail_list_print(params);
		}
		resultMap.put("adjust", adjust);
		resultMap.put("adjustList", adjustList);
		resultMap.put("approveRecord", approveRecordDAO.load(number,user.getCompanyid()));
		if(displayMode.intValue() == 1){//尺码模式查询
			List<String> szgCodes = new ArrayList<String>();
			for (T_Stock_AdjustList item : adjustList) {
				if(!szgCodes.contains(item.getAjl_szg_code())){
					szgCodes.add(item.getAjl_szg_code());
				}
			}
			List<T_Base_SizeList> sizeGroupList = sizeDAO.listBySzg(szgCodes, user.getCompanyid());
			resultMap.put("sizeGroupList", sizeGroupList);
		}
		return resultMap;
	}
	
}
