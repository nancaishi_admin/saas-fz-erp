package zy.service.set.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.set.SellSetDAO;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.entity.sell.set.T_Sell_Print;
import zy.entity.sell.set.T_Sell_PrintData;
import zy.entity.sell.set.T_Sell_PrintField;
import zy.entity.sell.set.T_Sell_PrintSet;
import zy.entity.sell.set.T_Sell_Set;
import zy.service.set.SellSetService;

@Service
public class SellSetServiceImpl implements SellSetService{
	@Resource
	private SellSetDAO sellSetDAO;
	@Override
	public void cashSet(Map<String, Object> param) {
		List<T_Sell_Set> setList = sellSetDAO.cashSet(param);
		if(null != setList && setList.size() > 0){
			for(T_Sell_Set set:setList){
				param.put(set.getSt_key(), set.getSt_value());
			}
		}
	}
	@Override
	public void updateCash(Map<String, Object> param) {
		sellSetDAO.updateCash(param);
	}
	@Override
	public Map<String, Object> print(Map<String, Object> param) {
		Map<String, Object> map = sellSetDAO.print(param);
		if(!map.containsKey("print")){
			map = sellSetDAO.sysprint(param);
		}
		return map;
	}
	@Override
	public void savePrint(T_Sell_Print print,
			List<T_Sell_PrintField> printFields,
			List<T_Sell_PrintData> printDatas, List<T_Sell_PrintSet> printSets,
			T_Sell_Cashier user) {
		if (printFields == null || printFields.size() == 0) {
			throw new IllegalArgumentException("参数printFields不能为null");
		}
		if (printDatas == null || printDatas.size() == 0) {
			throw new IllegalArgumentException("参数printDatas不能为null");
		}
		if (printSets == null || printSets.size() == 0) {
			throw new IllegalArgumentException("参数printSets不能为null");
		}
		print.setCompanyid(user.getCompanyid());
		print.setSp_shop_code(user.getCa_shop_code());
		for (T_Sell_PrintField field : printFields) {
			field.setCompanyid(user.getCompanyid());
		}
		for (T_Sell_PrintData data : printDatas) {
			data.setCompanyid(user.getCompanyid());
		}
		for (T_Sell_PrintSet set : printSets) {
			set.setCompanyid(user.getCompanyid());
		}
		sellSetDAO.savePrint(print, printFields, printDatas, printSets);
	}
	@Override
	public void updatePrint(T_Sell_Print print,
			List<T_Sell_PrintField> printFields,
			List<T_Sell_PrintData> printDatas, 
			List<T_Sell_PrintSet> printSets,
			T_Sell_Cashier user) {
		if (printFields == null || printFields.size() == 0) {
			throw new IllegalArgumentException("参数printFields不能为null");
		}
		if (printDatas == null || printDatas.size() == 0) {
			throw new IllegalArgumentException("参数printDatas不能为null");
		}
		if (printSets == null || printSets.size() == 0) {
			throw new IllegalArgumentException("参数printSets不能为null");
		}
		for (T_Sell_PrintSet set : printSets) {
			set.setSps_sp_code(print.getSp_code());
			set.setCompanyid(user.getCompanyid());
		}
		sellSetDAO.updatePrint(print, printFields, printDatas, printSets);
	}

}
