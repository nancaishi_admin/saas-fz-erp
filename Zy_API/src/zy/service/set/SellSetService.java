package zy.service.set;

import java.util.List;
import java.util.Map;

import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.entity.sell.set.T_Sell_Print;
import zy.entity.sell.set.T_Sell_PrintData;
import zy.entity.sell.set.T_Sell_PrintField;
import zy.entity.sell.set.T_Sell_PrintSet;

public interface SellSetService {
	public void cashSet(Map<String,Object> param);
	public void updateCash(Map<String,Object> param);
	Map<String,Object> print(Map<String,Object> param);
	void savePrint(T_Sell_Print print, List<T_Sell_PrintField> printFields,
			List<T_Sell_PrintData> printDatas, List<T_Sell_PrintSet> printSets
			,T_Sell_Cashier user);
	void updatePrint(T_Sell_Print print, List<T_Sell_PrintField> printFields, 
			List<T_Sell_PrintData> printDatas, List<T_Sell_PrintSet> printSets
			,T_Sell_Cashier user);
}
