package zy.service.sort.fee.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.approve.ApproveRecordDAO;
import zy.dao.base.shop.ShopDAO;
import zy.dao.sort.dealings.SortDealingsDAO;
import zy.dao.sort.fee.SortFeeDAO;
import zy.dao.sort.settle.SortSettleDAO;
import zy.dao.sys.print.PrintDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.approve.T_Approve_Record;
import zy.entity.base.shop.T_Base_Shop;
import zy.entity.sort.dealings.T_Sort_Dealings;
import zy.entity.sort.fee.T_Sort_Fee;
import zy.entity.sort.fee.T_Sort_FeeList;
import zy.entity.sort.settle.T_Sort_SettleList;
import zy.entity.sys.user.T_Sys_User;
import zy.service.sort.fee.SortFeeService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;

@Service
public class SortFeeServiceImpl implements SortFeeService{
	@Resource
	private SortFeeDAO sortFeeDAO;
	@Resource
	private ApproveRecordDAO approveRecordDAO;
	@Resource
	private SortDealingsDAO sortDealingsDAO;
	@Resource
	private SortSettleDAO sortSettleDAO;
	@Resource
	private ShopDAO shopDAO;
	@Resource
	private PrintDAO printDAO;
	
	@Override
	public PageData<T_Sort_Fee> page(Map<String, Object> params) {
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		
		Integer totalCount = sortFeeDAO.count(params);
		PageInfo pageInfo = new PageInfo(totalCount, pageSize, pageIndex);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		
		List<T_Sort_Fee> list = sortFeeDAO.list(params);
		PageData<T_Sort_Fee> pageData = new PageData<T_Sort_Fee>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	
	@Override
	public T_Sort_Fee load(Integer fe_id) {
		T_Sort_Fee fee = sortFeeDAO.load(fe_id);
		if(fee != null){
			T_Approve_Record approve_Record = approveRecordDAO.load(fee.getFe_number(), fee.getCompanyid());
			if(approve_Record != null){
				fee.setAr_describe(approve_Record.getAr_describe());
			}
		}
		return fee;
	}
	
	@Override
	public T_Sort_Fee load(String number, Integer companyid) {
		return sortFeeDAO.load(number, companyid);
	}
	
	@Override
	public List<T_Sort_FeeList> temp_list(Map<String, Object> params) {
		return sortFeeDAO.temp_list(params);
	}
	
	@Override
	@Transactional
	public void temp_save(List<T_Sort_FeeList> temps,T_Sys_User user) {
		if (temps == null || temps.size() == 0) {
			throw new RuntimeException("请选择费用类型");
		}
		List<String> existCode = sortFeeDAO.temp_check(user.getUs_id(),user.getCompanyid());
		List<T_Sort_FeeList> temps_add = new ArrayList<T_Sort_FeeList>();
		for (T_Sort_FeeList temp : temps) {
			if(!existCode.contains(temp.getFel_mp_code())){
				temp.setFel_us_id(user.getUs_id());
				temp.setFel_remark("");
				temp.setCompanyid(user.getCompanyid());
				temps_add.add(temp);
			}
		}
		if (temps_add.size() > 0) {
			sortFeeDAO.temp_save(temps_add);
		}
	}
	
	@Override
	@Transactional
	public void temp_updateMoney(T_Sort_FeeList temp) {
		sortFeeDAO.temp_updateMoney(temp);
	}
	
	@Override
	@Transactional
	public void temp_updateRemark(T_Sort_FeeList temp) {
		sortFeeDAO.temp_updateRemark(temp);
	}
	
	@Override
	@Transactional
	public void temp_del(Integer fel_id) {
		sortFeeDAO.temp_del(fel_id);
	}
	
	@Override
	@Transactional
	public void temp_clear(Integer us_id,Integer companyid) {
		sortFeeDAO.temp_clear(us_id, companyid);;
	}
	
	@Override
	public List<T_Sort_FeeList> detail_list(Map<String, Object> params) {
		return sortFeeDAO.detail_list(params);
	}
	
	@Override
	@Transactional
	public void save(T_Sort_Fee fee, T_Sys_User user) {
		if(fee == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		if(StringUtil.isEmpty(fee.getFe_shop_code())){
			throw new IllegalArgumentException("店铺不能为空");
		}
		if(StringUtil.isEmpty(fee.getFe_manager())){
			throw new IllegalArgumentException("经办人不能为空");
		}
		fee.setCompanyid(user.getCompanyid());
		fee.setFe_us_id(user.getUs_id());
		fee.setFe_maker(user.getUs_name());
		fee.setFe_ar_state(CommonUtil.AR_STATE_NOTAPPROVE);
		fee.setFe_sysdate(DateUtil.getCurrentTime());
		fee.setFe_pay_state(0);
		//1.查临时表
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("fel_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		List<T_Sort_FeeList> temps = sortFeeDAO.temp_list(params);
		if(temps == null || temps.size() == 0){
			throw new RuntimeException("单据已保存，请勿重复提交");
		}
		//2.保存单据
		double fe_money = 0d;
		for (T_Sort_FeeList temp : temps) {
			if(temp.getFel_money().doubleValue() == 0d){
				throw new RuntimeException("明细数据存在为0的费用类型，请修改");
			}
			fe_money += temp.getFel_money();
		}
		fee.setFe_money(fe_money);
		fee.setFe_discount_money(0d);
		fee.setFe_receivable(fe_money);
		fee.setFe_received(0d);
		fee.setFe_prepay(0d);
		sortFeeDAO.save(fee, temps);
		//3.删除临时表
		sortFeeDAO.temp_clear(user.getUs_id(), user.getCompanyid());
	}
	
	@Override
	@Transactional
	public void update(T_Sort_Fee fee, T_Sys_User user) {
		if(fee == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		if(StringUtil.isEmpty(fee.getFe_shop_code())){
			throw new IllegalArgumentException("店铺不能为空");
		}
		if(StringUtil.isEmpty(fee.getFe_manager())){
			throw new IllegalArgumentException("经办人不能为空");
		}
		fee.setCompanyid(user.getCompanyid());
		fee.setFe_us_id(user.getUs_id());
		fee.setFe_maker(user.getUs_name());
		fee.setFe_ar_state(CommonUtil.AR_STATE_NOTAPPROVE);
		fee.setFe_sysdate(DateUtil.getCurrentTime());
		fee.setFe_pay_state(0);
		//1.1查临时表
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("fel_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		List<T_Sort_FeeList> temps = sortFeeDAO.temp_list(params);
		if(temps == null || temps.size() == 0){
			throw new RuntimeException("单据已保存，请勿重复提交");
		}
		//1.2验证单据
		T_Sort_Fee oldFee = sortFeeDAO.check(fee.getFe_number(), user.getCompanyid());
		if (oldFee == null || !CommonUtil.AR_STATE_FAIL.equals(oldFee.getFe_ar_state())) {
			throw new RuntimeException("单据已修改，请勿重复提交");
		}
		//2.保存单据
		double fe_money = 0d;
		for (T_Sort_FeeList temp : temps) {
			if(temp.getFel_money().doubleValue() == 0d){
				throw new RuntimeException("明细数据存在为0的费用类型，请修改");
			}
			fe_money += temp.getFel_money();
		}
		fee.setFe_money(fe_money);
		fee.setFe_discount_money(0d);
		fee.setFe_receivable(fe_money);
		fee.setFe_received(0d);
		fee.setFe_prepay(0d);
		sortFeeDAO.deleteList(fee.getFe_number(), user.getCompanyid());
		sortFeeDAO.update(fee, temps);
		//3.删除临时表
		sortFeeDAO.temp_clear(user.getUs_id(), user.getCompanyid());
	}
	
	@Override
	@Transactional
	public T_Sort_Fee approve(String number, T_Approve_Record record, T_Sys_User user) {
		if (number == null) {
			throw new IllegalArgumentException("参数number不能为null");
		}
		T_Sort_Fee fee = sortFeeDAO.check(number, user.getCompanyid());
		if(fee == null){
			throw new RuntimeException("单据不存在");
		}
		if(!CommonUtil.AR_STATE_NOTAPPROVE.equals(fee.getFe_ar_state())){
			throw new RuntimeException("单据已经审核");
		}
		//更新单据审核状态
		fee.setFe_ar_state(record.getAr_state());
		fee.setFe_ar_date(DateUtil.getCurrentTime());
		sortFeeDAO.updateApprove(fee);
		//保存审核记录表
		record.setAr_number(number);
		record.setAr_sysdate(DateUtil.getCurrentTime());
		record.setAr_us_name(user.getUs_name());
		record.setAr_type("t_sort_fee");
		record.setCompanyid(user.getCompanyid());
		approveRecordDAO.save(record);
		if(!CommonUtil.AR_STATE_APPROVED.equals(fee.getFe_ar_state())){//审核不通过，则直接返回
			return fee;
		}
		//3.审核通过
		//3.1更新店铺财务&生成往来明细账
		T_Base_Shop shop = shopDAO.loadShop(fee.getFe_shop_code(), user.getCompanyid());
		if(shop != null){
			shop.setSp_receivable(shop.getSp_receivable()+fee.getFe_receivable());
			shopDAO.updateReceivable(shop);
			T_Sort_Dealings dealings = new T_Sort_Dealings();
			dealings.setDl_shop_code(fee.getFe_shop_code());
			dealings.setDl_number(fee.getFe_number());
			dealings.setDl_type(CommonUtil.SORTDEALINGS_TYPE_FY);
			dealings.setDl_discount_money(fee.getFe_discount_money());
			dealings.setDl_receivable(fee.getFe_receivable());
			dealings.setDl_received(0d);
			dealings.setDl_debt(shop.getSp_receivable()-shop.getSp_received()-shop.getSp_prepay());
			dealings.setDl_date(DateUtil.getCurrentTime());
			dealings.setDl_manager(user.getUs_name());
			dealings.setDl_remark(fee.getFe_remark());
			dealings.setDl_sysdate(DateUtil.getCurrentTime());
			dealings.setCompanyid(user.getCompanyid());
			sortDealingsDAO.save(dealings);
		}
		return fee;
	}
	
	@Override
	@Transactional
	public T_Sort_Fee reverse(String number, T_Sys_User user) {
		if (number == null) {
			throw new IllegalArgumentException("参数number不能为null");
		}
		T_Sort_Fee fee = sortFeeDAO.check(number, user.getCompanyid());
		if(fee == null){
			throw new RuntimeException("单据不存在");
		}
		if(!CommonUtil.AR_STATE_APPROVED.equals(fee.getFe_ar_state())){
			throw new RuntimeException("单据未审核或审核未通过");
		}
		if(!CommonUtil.PAY_STATE_UNPAY.equals(fee.getFe_pay_state())){
			throw new RuntimeException("该单据已经结算，不能反审核");
		}
		T_Sort_SettleList existSettle = sortSettleDAO.check_settle_bill(fee.getFe_number(), user.getCompanyid());
		if(existSettle != null){
			throw new RuntimeException("该单据已经结算，结算单据["+existSettle.getStl_number()+"]，不能反审核");
		}
		//1.更新单据审核状态
		fee.setFe_ar_state(CommonUtil.AR_STATE_NOTAPPROVE);
		fee.setFe_ar_date(DateUtil.getCurrentTime());
		sortFeeDAO.updateApprove(fee);
		//2.保存审核记录表
		T_Approve_Record record = new T_Approve_Record();
		record.setAr_state(CommonUtil.AR_STATE_REVERSE_APPROVE);
		record.setAr_describe(user.getUs_name()+"反审核单据");
		record.setAr_number(number);
		record.setAr_sysdate(DateUtil.getCurrentTime());
		record.setAr_us_name(user.getUs_name());
		record.setAr_type("t_sort_fee");
		record.setCompanyid(user.getCompanyid());
		approveRecordDAO.save(record);
		
		//3更新店铺财务&删除往来明细账
		T_Base_Shop shop = shopDAO.loadShop(fee.getFe_shop_code(), user.getCompanyid());
		if(shop != null){
			shop.setSp_receivable(shop.getSp_receivable()-fee.getFe_receivable());
			shopDAO.updateReceivable(shop);
			T_Sort_Dealings dealings = new T_Sort_Dealings();
			dealings.setDl_shop_code(fee.getFe_shop_code());
			dealings.setDl_number(fee.getFe_number());
			dealings.setDl_type(CommonUtil.SORTDEALINGS_TYPE_FY);
			dealings.setDl_discount_money(-fee.getFe_discount_money());
			dealings.setDl_receivable(-fee.getFe_receivable());
			dealings.setDl_received(0d);
			dealings.setDl_debt(shop.getSp_receivable()-shop.getSp_received()-shop.getSp_prepay());
			dealings.setDl_date(DateUtil.getCurrentTime());
			dealings.setDl_manager(user.getUs_name());
			dealings.setDl_remark("反审核恢复账目往来明细");
			dealings.setDl_sysdate(DateUtil.getCurrentTime());
			dealings.setCompanyid(user.getCompanyid());
			sortDealingsDAO.save(dealings);
		}
		return fee;
	}
	
	@Override
	@Transactional
	public void initUpdate(String number, Integer us_id, Integer companyid) {
		List<T_Sort_FeeList> details = sortFeeDAO.detail_list_forsavetemp(number, companyid);;
		for(T_Sort_FeeList item:details){
			item.setFel_us_id(us_id);
		}
		sortFeeDAO.temp_clear(us_id, companyid);
		sortFeeDAO.temp_save(details);
	}
	
	@Override
	@Transactional
	public void del(String number, Integer companyid) {
		if (number == null) {
			throw new IllegalArgumentException("参数number不能为null");
		}
		T_Sort_Fee fee = sortFeeDAO.check(number, companyid);
		if(fee == null){
			throw new RuntimeException("单据不存在");
		}
		if(!CommonUtil.AR_STATE_NOTAPPROVE.equals(fee.getFe_ar_state()) && !CommonUtil.AR_STATE_FAIL.equals(fee.getFe_ar_state())){
			throw new RuntimeException("单据已审核通过不能删除！");
		}
		sortFeeDAO.del(number, companyid);
	}
	
	@Override
	public Map<String, Object> loadPrintData(String number, Integer sp_id, T_Sys_User user) {
		Map<String, Object> resultMap = printDAO.loadPrint4Bill(sp_id);
		T_Sort_Fee fee = sortFeeDAO.load(number, user.getCompanyid());
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("fel_number", number);
		params.put("companyid", user.getCompanyid());
		List<T_Sort_FeeList> feeList = sortFeeDAO.detail_list(params);
		resultMap.put("fee", fee);
		resultMap.put("feeList", feeList);
		resultMap.put("shop", shopDAO.load(fee.getFe_shop_code(), user.getCompanyid()));
		return resultMap;
	}
	
}
