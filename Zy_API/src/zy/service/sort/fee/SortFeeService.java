package zy.service.sort.fee;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.approve.T_Approve_Record;
import zy.entity.sort.fee.T_Sort_Fee;
import zy.entity.sort.fee.T_Sort_FeeList;
import zy.entity.sys.user.T_Sys_User;

public interface SortFeeService {
	PageData<T_Sort_Fee> page(Map<String, Object> params);
	T_Sort_Fee load(Integer fe_id);
	T_Sort_Fee load(String number, Integer companyid);
	List<T_Sort_FeeList> temp_list(Map<String, Object> params);
	void temp_save(List<T_Sort_FeeList> temps,T_Sys_User user);
	void temp_updateMoney(T_Sort_FeeList temp);
	void temp_updateRemark(T_Sort_FeeList temp);
	void temp_del(Integer fel_id);
	void temp_clear(Integer us_id,Integer companyid);
	List<T_Sort_FeeList> detail_list(Map<String, Object> params);
	void save(T_Sort_Fee fee, T_Sys_User user);
	void update(T_Sort_Fee fee, T_Sys_User user);
	T_Sort_Fee approve(String number, T_Approve_Record record, T_Sys_User user);
	T_Sort_Fee reverse(String number, T_Sys_User user);
	void initUpdate(String number, Integer us_id, Integer companyid);
	void del(String number, Integer companyid);
	Map<String, Object> loadPrintData(String number, Integer sp_id, T_Sys_User user);
}
