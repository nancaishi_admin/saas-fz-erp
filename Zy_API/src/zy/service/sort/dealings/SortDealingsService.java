package zy.service.sort.dealings;

import java.util.Map;

import zy.entity.PageData;
import zy.entity.sort.dealings.T_Sort_Dealings;

public interface SortDealingsService {
	PageData<T_Sort_Dealings> page(Map<String, Object> params);
}
