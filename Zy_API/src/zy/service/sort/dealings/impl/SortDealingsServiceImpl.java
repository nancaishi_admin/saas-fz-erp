package zy.service.sort.dealings.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.sort.dealings.SortDealingsDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.sort.dealings.T_Sort_Dealings;
import zy.service.sort.dealings.SortDealingsService;
import zy.util.CommonUtil;

@Service
public class SortDealingsServiceImpl implements SortDealingsService{
	@Resource
	private SortDealingsDAO sortDealingsDAO;
	
	@Override
	public PageData<T_Sort_Dealings> page(Map<String, Object> params) {
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		
		Integer totalCount = sortDealingsDAO.count(params);
		PageInfo pageInfo = new PageInfo(totalCount, pageSize, pageIndex);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		
		List<T_Sort_Dealings> list = sortDealingsDAO.list(params);
		PageData<T_Sort_Dealings> pageData = new PageData<T_Sort_Dealings>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	
}
