package zy.service.sort.settle;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.approve.T_Approve_Record;
import zy.entity.base.shop.T_Base_Shop;
import zy.entity.sort.settle.T_Sort_Settle;
import zy.entity.sort.settle.T_Sort_SettleList;
import zy.entity.sys.user.T_Sys_User;

public interface SortSettleService {
	PageData<T_Sort_Settle> page(Map<String, Object> params);
	T_Sort_Settle load(Integer st_id);
	T_Sort_Settle load(String number,Integer companyid);
	T_Base_Shop loadShop(String sp_code,Integer companyid);
	List<T_Sort_SettleList> temp_list(Map<String, Object> params);
	void temp_save(String sp_code,T_Sys_User user);
	void temp_updateDiscountMoney(T_Sort_SettleList temp);
	void temp_updatePrepay(T_Sort_SettleList temp);
	void temp_updateRealMoney(T_Sort_SettleList temp);
	void temp_updateRemark(T_Sort_SettleList temp);
	void temp_updateJoin(String ids,String stl_join);
	Map<String, Object> temp_entire(Map<String, Object> params);
	List<T_Sort_SettleList> detail_list(String number,Integer companyid);
	void save(T_Sort_Settle settle, T_Sys_User user);
	void update(T_Sort_Settle settle, T_Sys_User user);
	T_Sort_Settle approve(String number, T_Approve_Record record, T_Sys_User user);
	T_Sort_Settle reverse(String number, T_Sys_User user);
	void initUpdate(String number, Integer us_id, Integer companyid);
	void del(String number, Integer companyid);
	Map<String, Object> loadPrintData(String number, Integer sp_id, T_Sys_User user);
}
