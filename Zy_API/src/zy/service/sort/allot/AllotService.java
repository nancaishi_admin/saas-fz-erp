package zy.service.sort.allot;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.approve.T_Approve_Record;
import zy.entity.base.product.T_Base_Product;
import zy.entity.sort.allot.T_Sort_Allot;
import zy.entity.sort.allot.T_Sort_AllotList;
import zy.entity.sys.user.T_Sys_User;

public interface AllotService {
	PageData<T_Sort_Allot> page(Map<String,Object> params);
	List<T_Sort_Allot> listExport(Map<String, Object> params);
	T_Sort_Allot load(Integer at_id);
	T_Sort_Allot load(String number, Integer companyid);
	List<T_Sort_AllotList> detail_list(Map<String, Object> params);
	List<T_Sort_AllotList> detail_sum(Map<String, Object> params);
	Map<String, Object> detail_size_title(Map<String, Object> params);
	Map<String, Object> detail_size(Map<String, Object> params);
	List<T_Sort_AllotList> temp_list(Map<String, Object> params);
	List<T_Sort_AllotList> temp_sum(Integer at_type,Integer atl_up,Integer us_id,Integer companyid);
	Map<String, Object> temp_size_title(Map<String, Object> params);
	Map<String, Object> temp_size(Map<String, Object> params);
	PageData<T_Base_Product> page_product(Map<String, Object> param);
	Map<String, Object> temp_loadproduct(Map<String, Object> params);
	Map<String, Object> send_loadproduct(Map<String, Object> params);
	Map<String, Object> temp_save_bybarcode(Map<String, Object> params);
	Map<String, Object> detail_save_bybarcode(Map<String, Object> params);
	void temp_save(Map<String, Object> params);
	void send_save_detail(Map<String, Object> params);
	void temp_import(Map<String, Object> params);
	void temp_copy(Map<String, Object> params);
	void temp_updateAmount(T_Sort_AllotList temp);
	void temp_updatePrice(T_Sort_AllotList temp);
	void temp_updateRemarkById(T_Sort_AllotList temp);
	void temp_updateRemarkByPdCode(T_Sort_AllotList temp);
	void temp_del(Integer atl_id);
	void temp_delByPiCode(T_Sort_AllotList temp);
	void temp_clear(Integer at_type,Integer atl_up,Integer us_id,Integer companyid);
	void detail_updateAmount(T_Sort_AllotList temp);
	void detail_automatch(String number,Integer companyid);
	void detail_del(Integer atl_id);
	void save(T_Sort_Allot allot, T_Sys_User user,Integer atl_up);
	void update(T_Sort_Allot allot, T_Sys_User user,Integer atl_up);
	T_Sort_Allot approve(String number, T_Approve_Record record, T_Sys_User user);
	T_Sort_Allot send(String number, String at_outdp_code, T_Sys_User user);
	T_Sort_Allot receive(String number, String at_indp_code, T_Sys_User user);
	T_Sort_Allot reject(String number, T_Sys_User user);
	T_Sort_Allot rejectconfirm(String number, T_Sys_User user);
	void initUpdate(String number,Integer at_type,Integer atl_up, Integer us_id, Integer companyid);
	void del(String number, Integer companyid);
	Map<String, Object> loadPrintData(String number, Integer sp_id, Integer displayMode,T_Sys_User user);
}
