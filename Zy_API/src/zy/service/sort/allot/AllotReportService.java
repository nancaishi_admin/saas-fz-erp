package zy.service.sort.allot;

import java.util.Map;

import zy.dto.sort.allot.AllotDetailReportDto;
import zy.dto.sort.allot.AllotReportDto;
import zy.entity.PageData;

public interface AllotReportService {
	PageData<AllotReportDto> pageAllotReport(Map<String, Object> params);
	PageData<AllotDetailReportDto> pageAllotDetailReport(Map<String, Object> params);
}
