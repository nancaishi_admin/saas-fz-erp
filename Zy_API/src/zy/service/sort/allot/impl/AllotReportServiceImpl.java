package zy.service.sort.allot.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.sort.allot.AllotReportDAO;
import zy.dto.sort.allot.AllotDetailReportDto;
import zy.dto.sort.allot.AllotReportDto;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.service.sort.allot.AllotReportService;
import zy.util.CommonUtil;

@Service
public class AllotReportServiceImpl implements AllotReportService{
	@Resource
	private AllotReportDAO allotReportDAO;
	
	@Override
	public PageData<AllotReportDto> pageAllotReport(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		
		Integer totalCount = allotReportDAO.countAllotReport(params);
		PageInfo pageInfo = new PageInfo(totalCount, pageSize, pageIndex);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		
		List<AllotReportDto> list = allotReportDAO.listAllotReport(params);
		PageData<AllotReportDto> pageData = new PageData<AllotReportDto>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		pageData.setData(allotReportDAO.sumAllotReport(params));
		return pageData;
	}
	
	@Override
	public PageData<AllotDetailReportDto> pageAllotDetailReport(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		
		Integer totalCount = allotReportDAO.countAllotDetailReport(params);
		PageInfo pageInfo = new PageInfo(totalCount, pageSize, pageIndex);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		
		List<AllotDetailReportDto> list = allotReportDAO.listAllotDetailReport(params);
		PageData<AllotDetailReportDto> pageData = new PageData<AllotDetailReportDto>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		pageData.setData(allotReportDAO.sumAllotDetailReport(params));
		return pageData;
	}
	
}
