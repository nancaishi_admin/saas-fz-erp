package zy.service.sell.cash;

import java.util.List;
import java.util.Map;

import zy.dto.sqb.PayResp;
import zy.entity.PageData;
import zy.entity.sell.cash.T_Sell_Pay_Temp;
import zy.entity.sell.cash.T_Sell_Shop_Temp;
import zy.entity.vip.member.T_Vip_Member;
import zy.form.StringForm;

public interface CashService {
	List<T_Sell_Shop_Temp> listTemp(Map<String,Object> param);
	void updateEmp(Map<String,Object> param);
	public void updateArea(Map<String, Object> param);
	//---------------商品查询方法
	PageData<T_Sell_Shop_Temp> pageSell(Map<String, Object> param);
	PageData<T_Sell_Shop_Temp> pageBack(Map<String, Object> param);
	int subCode(Map<String, Object> param);
	int queryNumber(Map<String, Object> param);
	Map<String,Object> productSize(Map<String, Object> param);
	T_Sell_Shop_Temp product(Map<String, Object> param);
	void saveTemp(Map<String, Object> param);
	void saveBackTemp(Map<String, Object> param);
	void delTemp(Map<String, Object> param);
	void clearTemp(Map<String, Object> param);
	T_Vip_Member queryVip(Map<String, Object> param);
	void vipRate(Map<String, Object> param);
	void handRate(Map<String, Object> param);
	void allRate(Map<String, Object> param);
	List<StringForm> putUpList(Map<String,Object> param);
	void updateTemp(Map<String,Object> param);
	T_Vip_Member putDown(Map<String,Object> param);
	void putUp(Map<String,Object> param);
	void to_cash(Map<String,Object> param);
	Map<String,Object> sellCashOut(Map<String,Object> param);
	PayResp doPay(Map<String,Object> param);
	PayResp payTemp(Map<String,Object> param);
	List<T_Sell_Pay_Temp> payList(Map<String,Object> param);
}
