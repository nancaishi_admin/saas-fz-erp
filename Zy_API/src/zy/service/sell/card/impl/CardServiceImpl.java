package zy.service.sell.card.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.money.bank.BankDAO;
import zy.dao.money.bank.BankRunDAO;
import zy.dao.sell.card.CardDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.money.bank.T_Money_Bank;
import zy.entity.money.bank.T_Money_BankRun;
import zy.entity.sell.card.T_Sell_Card;
import zy.entity.sell.card.T_Sell_CardList;
import zy.service.sell.card.CardService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.MD5;
import zy.util.NumberUtil;
import zy.util.StringUtil;

@Service
public class CardServiceImpl implements CardService{
	@Resource
	private CardDAO cardDAO;
	@Resource
	private BankDAO bankDAO;
	@Resource
	private BankRunDAO bankRunDAO;

	@Override
	public PageData<T_Sell_Card> page(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		Object pageSize = params.get(CommonUtil.PAGESIZE);
		Object pageIndex = params.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = cardDAO.count(params);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		params.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		params.put(CommonUtil.END, _pageSize);
		
		List<T_Sell_Card> list = cardDAO.list(params);
		PageData<T_Sell_Card> pageData = new PageData<T_Sell_Card>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	
	@Override
	public PageData<T_Sell_CardList> pageDetail(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		Object pageSize = params.get(CommonUtil.PAGESIZE);
		Object pageIndex = params.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = cardDAO.countDetail(params);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		params.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		params.put(CommonUtil.END, _pageSize);
		
		List<T_Sell_CardList> list = cardDAO.listDetail(params);
		PageData<T_Sell_CardList> pageData = new PageData<T_Sell_CardList>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}

	@Override
	public T_Sell_Card queryByID(Integer cd_id) {
		return cardDAO.queryByID(cd_id);
	}

	@Override
	public List<T_Sell_CardList> listDetail(String cd_code, Integer companyid) {
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		if (cd_code == null) {
			throw new IllegalArgumentException("参数cd_code不能为null");
		}
		return cardDAO.listDetail(cd_code, companyid);
	}

	@Override
	@Transactional
	public void save(Map<String, Object> params) {
		if(params == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		T_Sell_Card card = (T_Sell_Card)params.get("card");
		if (card.getCd_realcash() + card.getCd_bankmoney() > card.getCd_money()) {
			throw new IllegalArgumentException("实收现金和实收刷卡之和必须小于初始金额");
		}
		String start_no = (String)params.get("start_no");
		String mark = (String)params.get("mark");
		String cdl_ba_code = (String)params.get("cdl_ba_code");
		String cdl_bank_code = (String)params.get("cdl_bank_code");
		Integer add_count = (Integer)params.get("add_count");
		Integer prefix = (Integer)params.get("prefix");
		List<T_Sell_Card> cards = new ArrayList<T_Sell_Card>();
		List<T_Sell_CardList> cardDetails = new ArrayList<T_Sell_CardList>();
		String beginCode = StringUtils.stripStart(start_no, "0");//去掉前置0
		long beginIndex = 0;
		if (!"".equals(beginCode)) {
			beginIndex = Long.parseLong(beginCode);
		}
		long endIndex = beginIndex + add_count - 1;
		int length = Math.max(start_no.length(), StringUtil.trimString(endIndex).length());
		String format = "%0" + length + "d";
		String cardcode = null;
		List<String> cd_cardcode = new ArrayList<String>();
		for (int i = 0; i < add_count; i++) {
			if(prefix.equals(0)){
				cardcode = mark+String.format(format, beginIndex + i);
			}else {
				cardcode = String.format(format, beginIndex + i)+mark;
			}
			T_Sell_Card item = null;
			T_Sell_CardList itemDetail = null;
			try {
				item = (T_Sell_Card)card.clone();
				item.setCd_cardcode(cardcode);
				itemDetail = new T_Sell_CardList();
				itemDetail.setCompanyid(item.getCompanyid());
				itemDetail.setCdl_cardcode(cardcode);
				itemDetail.setCdl_ba_code(cdl_ba_code);
				itemDetail.setCdl_bank_code(cdl_bank_code);
				itemDetail.setCdl_realcash(item.getCd_realcash());
				itemDetail.setCdl_bankmoney(item.getCd_bankmoney());
				itemDetail.setCdl_money(item.getCd_money());
				itemDetail.setCdl_manager(item.getCd_manager());
				itemDetail.setCdl_shop_code(item.getCd_shop_code());
				itemDetail.setCdl_type(0);
				itemDetail.setCdl_date(DateUtil.getCurrentTime());
			} catch (CloneNotSupportedException e) {
				throw new IllegalArgumentException("对象深拷贝异常");
			}
			cards.add(item);
			cardDetails.add(itemDetail);
			cd_cardcode.add(cardcode);
		}
		//1.检测储值卡号是否已经存在
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("cd_cardcode", cd_cardcode);
		paramMap.put("companyid", card.getCompanyid());
		int existCount = cardDAO.countByCardCode(paramMap);
		if (existCount > 0) {
			throw new IllegalArgumentException("储值卡号已经存在");
		}
		//2.保存代金券主表和明细表
		cardDAO.save(cards, cardDetails);
		//3.更新银行帐目和银行流水
		if(cdl_ba_code.equals(cdl_bank_code)){
			updateBankAndRun4SameBank(cdl_ba_code, add_count, card, cards);
		}else {
			updateBankAndRun4DiffBank(cdl_ba_code, cdl_bank_code, add_count, card, cards);
		}
	}
	
	private void updateBankAndRun4SameBank(String cdl_ba_code,Integer add_count,T_Sell_Card card,List<T_Sell_Card> cards){
		T_Money_Bank bank = bankDAO.queryByCode(cdl_ba_code, card.getCompanyid());
		Double beginBalance = bank.getBa_balance() == null ? 0d : bank.getBa_balance();
		bank.setBa_balance(beginBalance + (card.getCd_realcash()+card.getCd_bankmoney()) * add_count);
		bankDAO.updateBalanceById(bank);
		List<T_Money_BankRun> bankRuns = new ArrayList<T_Money_BankRun>();
		T_Money_BankRun run = null;
		for (int i=0;i<cards.size();i++) {
			T_Sell_Card item =cards.get(i);
			run = new T_Money_BankRun();
			run.setBr_ba_code(cdl_ba_code);
			run.setBr_balance(beginBalance + (i + 1) * (item.getCd_realcash()+card.getCd_bankmoney()));
			run.setBr_bt_code(CommonUtil.BANKRUN_CARD_GRANT);
			run.setBr_date(item.getCd_grantdate());
			run.setBr_enter(item.getCd_realcash()+card.getCd_bankmoney());
			run.setBr_manager(item.getCd_manager());
			run.setBr_number(item.getCd_code());
			run.setBr_remark("");
			run.setBr_shop_code(bank.getBa_shop_code());
			run.setBr_sysdate(DateUtil.getCurrentTime());
			run.setCompanyid(item.getCompanyid());
			bankRuns.add(run);
		}
		bankRunDAO.save(bankRuns);
	}
	private void updateBankAndRun4DiffBank(String cdl_ba_code,String cdl_bank_code,Integer add_count,T_Sell_Card card,List<T_Sell_Card> cards){
		//3.1现金账户
		T_Money_Bank bank = bankDAO.queryByCode(cdl_ba_code, card.getCompanyid());
		Double beginBalance = bank.getBa_balance() == null ? 0d : bank.getBa_balance();
		bank.setBa_balance(beginBalance + card.getCd_realcash() * add_count);
		bankDAO.updateBalanceById(bank);
		List<T_Money_BankRun> bankRuns = new ArrayList<T_Money_BankRun>();
		T_Money_BankRun run = null;
		for (int i=0;i<cards.size();i++) {
			T_Sell_Card item =cards.get(i);
			run = new T_Money_BankRun();
			run.setBr_ba_code(cdl_ba_code);
			run.setBr_balance(beginBalance + (i + 1) * item.getCd_realcash());
			run.setBr_bt_code(CommonUtil.BANKRUN_CARD_GRANT);
			run.setBr_date(item.getCd_grantdate());
			run.setBr_enter(item.getCd_realcash());
			run.setBr_manager(item.getCd_manager());
			run.setBr_number(item.getCd_code());
			run.setBr_remark("");
			run.setBr_shop_code(bank.getBa_shop_code());
			run.setBr_sysdate(DateUtil.getCurrentTime());
			run.setCompanyid(item.getCompanyid());
			bankRuns.add(run);
		}
		bankRunDAO.save(bankRuns);
		//3.2刷卡账户
		T_Money_Bank bank_SK = bankDAO.queryByCode(cdl_bank_code, card.getCompanyid());
		Double beginBalance_SK = bank_SK.getBa_balance() == null ? 0d : bank_SK.getBa_balance();
		bank_SK.setBa_balance(beginBalance_SK + card.getCd_bankmoney() * add_count);
		bankDAO.updateBalanceById(bank_SK);
		List<T_Money_BankRun> bankRuns_SK = new ArrayList<T_Money_BankRun>();
		for (int i=0;i<cards.size();i++) {
			T_Sell_Card item =cards.get(i);
			run = new T_Money_BankRun();
			run.setBr_ba_code(cdl_bank_code);
			run.setBr_balance(beginBalance_SK + (i + 1) * item.getCd_bankmoney());
			run.setBr_bt_code(CommonUtil.BANKRUN_CARD_GRANT);
			run.setBr_date(item.getCd_grantdate());
			run.setBr_enter(item.getCd_bankmoney());
			run.setBr_manager(item.getCd_manager());
			run.setBr_number(item.getCd_code());
			run.setBr_remark("");
			run.setBr_shop_code(bank_SK.getBa_shop_code());
			run.setBr_sysdate(DateUtil.getCurrentTime());
			run.setCompanyid(item.getCompanyid());
			bankRuns_SK.add(run);
		}
		bankRunDAO.save(bankRuns_SK);
	}

	@Override
	@Transactional
	public void update(T_Sell_Card card) {
		if (card == null || card.getCd_id() == null) {
			throw new IllegalArgumentException("参数cd_id不能为null");
		}
		cardDAO.update(card);
	}
	
	@Override
	@Transactional
	public T_Sell_Card recharge(T_Sell_CardList cardList, Integer cd_id) {
		if(cd_id == null){
			throw new IllegalArgumentException("参数cd_id不能为null");
		}
		T_Sell_Card card = cardDAO.queryByID(cd_id);
		if (card == null) {
			throw new RuntimeException("储值卡不存在或已删除！");
		}
		cardList.setCdl_cd_code(card.getCd_code());
		if (cardList.getCdl_type().equals(2)) {// 减值
			if (card.getCd_money() < cardList.getCdl_money()) {
				throw new IllegalArgumentException("减值金额不能超过储值卡剩余金额");
			}
			cardList.setCdl_money(-cardList.getCdl_money());
			cardList.setCdl_realcash(-cardList.getCdl_realcash());
			cardList.setCdl_bankmoney(-cardList.getCdl_bankmoney());
		}
		card.setCd_money(card.getCd_money() + cardList.getCdl_money());
		card.setCd_realcash(card.getCd_realcash() + cardList.getCdl_realcash());
		card.setCd_bankmoney(card.getCd_bankmoney() + cardList.getCdl_bankmoney());
		if(card.getCd_money().doubleValue() != 0){
			card.setCd_cashrate((card.getCd_realcash()+card.getCd_bankmoney())/(card.getCd_money()+card.getCd_used_money()));
		}else {
			card.setCd_cashrate(0d);
		}
		cardDAO.updateMoney(card);
		cardDAO.saveDetail(cardList);
		if(cardList.getCdl_ba_code().equals(cardList.getCdl_bank_code())){
			updateBankAndRun4SameBank(cardList.getCdl_ba_code(), card, cardList);
		}else {
			updateBankAndRun4DiffBank(cardList.getCdl_ba_code(), cardList.getCdl_bank_code(), card, cardList);
		}
		return card;
	}

	private void updateBankAndRun4SameBank(String cdl_ba_code,T_Sell_Card card,T_Sell_CardList cardList){
		if (cardList.getCdl_realcash().doubleValue() == 0d && cardList.getCdl_bankmoney().doubleValue() == 0d) {
			return;
		}
		T_Money_Bank bank = bankDAO.queryByCode(cdl_ba_code, card.getCompanyid());
		Double beginBalance = bank.getBa_balance() == null ? 0d : bank.getBa_balance();
		bank.setBa_balance(beginBalance + cardList.getCdl_realcash()+cardList.getCdl_bankmoney());
		bankDAO.updateBalanceById(bank);
		T_Money_BankRun run = new T_Money_BankRun();
		run.setBr_ba_code(cdl_ba_code);
		run.setBr_balance(bank.getBa_balance());
		if (cardList.getCdl_type().equals(1)) {// 充值
			run.setBr_bt_code(CommonUtil.BANKRUN_CARD_PLUS);
			run.setBr_enter(cardList.getCdl_realcash()+cardList.getCdl_bankmoney());
		}else if (cardList.getCdl_type().equals(2)) {// 减值
			run.setBr_bt_code(CommonUtil.BANKRUN_CARD_MINUS);
			run.setBr_out(Math.abs(cardList.getCdl_realcash()+cardList.getCdl_bankmoney()));
		}
		run.setBr_date(cardList.getCdl_date());
		run.setBr_manager(cardList.getCdl_manager());
		run.setBr_number(card.getCd_code());
		run.setBr_remark("");
		run.setBr_shop_code(bank.getBa_shop_code());
		run.setBr_sysdate(DateUtil.getCurrentTime());
		run.setCompanyid(card.getCompanyid());
		bankRunDAO.save(run);
	}
	
	private void updateBankAndRun4DiffBank(String cdl_ba_code,String cdl_bank_code,T_Sell_Card card,T_Sell_CardList cardList){
		if (cardList.getCdl_realcash().doubleValue() != 0d) {
			T_Money_Bank bank = bankDAO.queryByCode(cdl_ba_code, card.getCompanyid());
			Double beginBalance = bank.getBa_balance() == null ? 0d : bank.getBa_balance();
			bank.setBa_balance(beginBalance + cardList.getCdl_realcash());
			bankDAO.updateBalanceById(bank);
			T_Money_BankRun run = new T_Money_BankRun();
			run.setBr_ba_code(cdl_ba_code);
			run.setBr_balance(bank.getBa_balance());
			if (cardList.getCdl_type().equals(1)) {// 充值
				run.setBr_bt_code(CommonUtil.BANKRUN_CARD_PLUS);
				run.setBr_enter(cardList.getCdl_realcash());
			}else if (cardList.getCdl_type().equals(2)) {// 减值
				run.setBr_bt_code(CommonUtil.BANKRUN_CARD_MINUS);
				run.setBr_out(Math.abs(cardList.getCdl_realcash()));
			}
			run.setBr_date(cardList.getCdl_date());
			run.setBr_manager(cardList.getCdl_manager());
			run.setBr_number(card.getCd_code());
			run.setBr_remark("");
			run.setBr_shop_code(bank.getBa_shop_code());
			run.setBr_sysdate(DateUtil.getCurrentTime());
			run.setCompanyid(card.getCompanyid());
			bankRunDAO.save(run);
		}
		if(cardList.getCdl_bankmoney().doubleValue() != 0d){
			T_Money_Bank bank_SK = bankDAO.queryByCode(cdl_bank_code, card.getCompanyid());
			Double beginBalance_SK = bank_SK.getBa_balance() == null ? 0d : bank_SK.getBa_balance();
			bank_SK.setBa_balance(beginBalance_SK + cardList.getCdl_bankmoney());
			bankDAO.updateBalanceById(bank_SK);
			T_Money_BankRun run = new T_Money_BankRun();
			run.setBr_ba_code(cdl_bank_code);
			run.setBr_balance(bank_SK.getBa_balance());
			if (cardList.getCdl_type().equals(1)) {// 充值
				run.setBr_bt_code(CommonUtil.BANKRUN_CARD_PLUS);
				run.setBr_enter(cardList.getCdl_bankmoney());
			}else if (cardList.getCdl_type().equals(2)) {// 减值
				run.setBr_bt_code(CommonUtil.BANKRUN_CARD_MINUS);
				run.setBr_out(Math.abs(cardList.getCdl_bankmoney()));
			}
			run.setBr_date(cardList.getCdl_date());
			run.setBr_manager(cardList.getCdl_manager());
			run.setBr_number(card.getCd_code());
			run.setBr_remark("");
			run.setBr_shop_code(bank_SK.getBa_shop_code());
			run.setBr_sysdate(DateUtil.getCurrentTime());
			run.setCompanyid(card.getCompanyid());
			bankRunDAO.save(run);
		}
	}
	
	@Override
	@Transactional
	public void updateState(T_Sell_Card card) {
		if (card == null || card.getCd_id() == null) {
			throw new IllegalArgumentException("参数cd_id不能为null");
		}
		cardDAO.updateState(card);
	}

	@Override
	@Transactional
	public void del(Integer cd_id) {
		if (cd_id == null) {
			throw new IllegalArgumentException("参数cd_id不能为null");
		}
		T_Sell_Card card = cardDAO.queryByID(cd_id);
		if (card == null) {
			throw new RuntimeException("储值卡不存在或已删除！");
		}
		if (card.getCd_used_money() > 0) {
			throw new RuntimeException("储值卡已使用，不能被删除！");
		}
		Integer cdl_id = cardDAO.checkExistOperation(card.getCd_code(), card.getCompanyid());
		if (cdl_id != null && cdl_id > 0) {
			throw new RuntimeException("储值卡已存在充值、减值、消费等操作，不能被删除！");
		}
		//1.更新银行帐目，产生账目回流银行流水
		T_Sell_CardList cardDetail = cardDAO.queryInitBank(card.getCd_code(), card.getCompanyid());
		if(cardDetail != null){
			if(cardDetail.getCdl_ba_code().equals(cardDetail.getCdl_bank_code())){
				backBankAndRun4SameBank(cardDetail.getCdl_ba_code(), card);
			}else {
				backBankAndRun4DiffBank(cardDetail.getCdl_ba_code(), cardDetail.getCdl_bank_code(), card);
			}
		}
		//2.删除储值卡主表和子表数据
		cardDAO.del(cd_id);
		cardDAO.delList(card.getCd_code(), card.getCompanyid());
	}
	
	private void backBankAndRun4SameBank(String cdl_ba_code,T_Sell_Card card){
		T_Money_Bank bank = bankDAO.queryByCode(cdl_ba_code, card.getCompanyid());
		Double beginBalance = bank.getBa_balance() == null ? 0d : bank.getBa_balance();
		bank.setBa_balance(beginBalance - card.getCd_realcash()-card.getCd_bankmoney());
		bankDAO.updateBalanceById(bank);
		T_Money_BankRun run = new T_Money_BankRun();
		run.setBr_ba_code(cdl_ba_code);
		run.setBr_balance(bank.getBa_balance());
		run.setBr_bt_code(CommonUtil.BANKRUN_CARD_GRANT);
		run.setBr_date(DateUtil.getCurrentTime());
		run.setBr_out(card.getCd_realcash()+card.getCd_bankmoney());
		run.setBr_manager(card.getCd_manager());
		run.setBr_number(card.getCd_code());
		run.setBr_remark("删除储值卡["+card.getCd_cardcode()+"]，银行帐目回流。");
		run.setBr_shop_code(bank.getBa_shop_code());
		run.setBr_sysdate(DateUtil.getCurrentTime());
		run.setCompanyid(card.getCompanyid());
		bankRunDAO.save(run);
	}
	private void backBankAndRun4DiffBank(String cdl_ba_code,String cdl_bank_code,T_Sell_Card card){
		//1现金账户
		T_Money_Bank bank = bankDAO.queryByCode(cdl_ba_code, card.getCompanyid());
		Double beginBalance = bank.getBa_balance() == null ? 0d : bank.getBa_balance();
		bank.setBa_balance(beginBalance - card.getCd_realcash());
		bankDAO.updateBalanceById(bank);
		T_Money_BankRun run = new T_Money_BankRun();
		run.setBr_ba_code(cdl_ba_code);
		run.setBr_balance(bank.getBa_balance());
		run.setBr_bt_code(CommonUtil.BANKRUN_CARD_GRANT);
		run.setBr_date(DateUtil.getCurrentTime());
		run.setBr_out(card.getCd_realcash());
		run.setBr_manager(card.getCd_manager());
		run.setBr_number(card.getCd_code());
		run.setBr_remark("删除储值卡["+card.getCd_cardcode()+"]，银行帐目回流。");
		run.setBr_shop_code(bank.getBa_shop_code());
		run.setBr_sysdate(DateUtil.getCurrentTime());
		run.setCompanyid(card.getCompanyid());
		bankRunDAO.save(run);
		//2刷卡账户
		T_Money_Bank bank_SK = bankDAO.queryByCode(cdl_bank_code, card.getCompanyid());
		Double beginBalance_SK = bank_SK.getBa_balance() == null ? 0d : bank_SK.getBa_balance();
		bank_SK.setBa_balance(beginBalance_SK - card.getCd_bankmoney());
		bankDAO.updateBalanceById(bank_SK);
		T_Money_BankRun run_SK = new T_Money_BankRun();
		run_SK.setBr_ba_code(cdl_bank_code);
		run_SK.setBr_balance(bank_SK.getBa_balance());
		run_SK.setBr_bt_code(CommonUtil.BANKRUN_CARD_GRANT);
		run_SK.setBr_date(DateUtil.getCurrentTime());
		run_SK.setBr_out(card.getCd_bankmoney());
		run_SK.setBr_manager(card.getCd_manager());
		run_SK.setBr_number(card.getCd_code());
		run_SK.setBr_remark("删除储值卡["+card.getCd_cardcode()+"]，银行帐目回流。");
		run_SK.setBr_shop_code(bank_SK.getBa_shop_code());
		run_SK.setBr_sysdate(DateUtil.getCurrentTime());
		run_SK.setCompanyid(card.getCompanyid());
		bankRunDAO.save(run_SK);
	}

	@Override
	public List<T_Sell_Card> listShop(Map<String, Object> param) {
		return cardDAO.listShop(param);
	}
	@Transactional
	@Override
	public void saveShop(Map<String, Object> param) {
		T_Sell_Card card = (T_Sell_Card)param.get("card");
		card.setCd_state(0);
		card.setCd_used_money(0d);
		card.setCd_cashrate((card.getCd_realcash()+card.getCd_bankmoney())/card.getCd_money());
		card.setCd_pass(MD5.encryptMd5(card.getCd_pass()));
		card.setCompanyid(NumberUtil.toInteger(param.get(CommonUtil.COMPANYID)));
		param.put("cd_cardcode", card.getCd_cardcode());
		Integer id = cardDAO.idByCode(param);
		if(null != id && id > 0){
			throw new IllegalArgumentException("卡号已存在");
		}
		cardDAO.saveShop(param);
		if(card.getCd_realcash() > 0){
			T_Money_Bank bank = bankDAO.queryByCode(card.getCdl_ba_code(), card.getCompanyid());
			Double beginBalance = bank.getBa_balance() == null ? 0d : bank.getBa_balance();
			bank.setBa_balance(beginBalance + card.getCd_realcash());
			bankDAO.updateBalanceById(bank);
			T_Money_BankRun run = new T_Money_BankRun();
			run.setBr_ba_code(card.getCdl_ba_code());
			run.setBr_balance(beginBalance + card.getCd_realcash());
			run.setBr_bt_code(CommonUtil.BANKRUN_VOUCHER);
			run.setBr_date(card.getCd_grantdate());
			run.setBr_enter(card.getCd_realcash());
			run.setBr_manager(card.getCd_manager());
			run.setBr_number(card.getCd_cardcode());
			run.setBr_shop_code(bank.getBa_shop_code());
			run.setBr_sysdate(DateUtil.getCurrentTime());
			run.setCompanyid(card.getCompanyid());
			bankRunDAO.save(run);
		}
		if(card.getCd_bankmoney() > 0){
			T_Money_Bank bank = bankDAO.queryByCode(card.getCdl_bank_code(), card.getCompanyid());
			Double beginBalance = bank.getBa_balance() == null ? 0d : bank.getBa_balance();
			bank.setBa_balance(beginBalance + card.getCd_bankmoney());
			bankDAO.updateBalanceById(bank);
			T_Money_BankRun run = new T_Money_BankRun();
			run.setBr_ba_code(card.getCdl_ba_code());
			run.setBr_balance(beginBalance + card.getCd_bankmoney());
			run.setBr_bt_code(CommonUtil.BANKRUN_VOUCHER);
			run.setBr_date(card.getCd_grantdate());
			run.setBr_enter(card.getCd_bankmoney());
			run.setBr_manager(card.getCd_manager());
			run.setBr_number(card.getCd_cardcode());
			run.setBr_shop_code(bank.getBa_shop_code());
			run.setBr_sysdate(DateUtil.getCurrentTime());
			run.setCompanyid(card.getCompanyid());
			bankRunDAO.save(run);
		}
	}

	@Override
	public void editPass(Map<String, Object> param) {
		cardDAO.editPass(param);
	}

	@Override
	public T_Sell_Card queryByCode(Map<String, Object> param) {
		return cardDAO.queryByCode(param);
	}

	@Override
	public T_Sell_Card cardByMobile(Map<String, Object> param) {
		return cardDAO.cardByMobile(param);
	}

}
