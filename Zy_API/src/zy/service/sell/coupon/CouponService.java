package zy.service.sell.coupon;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.approve.T_Approve_Record;
import zy.entity.sell.coupon.T_Sell_Coupon;
import zy.entity.sell.coupon.T_Sell_Coupon_Detail;
import zy.entity.sys.user.T_Sys_User;

public interface CouponService {
	PageData<T_Sell_Coupon> page(Map<String,Object> params);
	List<T_Sell_Coupon_Detail> listDetail(String sc_number,Integer sc_type,Integer companyid);
	void save(Map<String,Object> params);
	void update(Map<String,Object> params);
	void del(String sc_number, Integer companyid);
	T_Sell_Coupon load(Integer sc_id);
	T_Sell_Coupon approve(String number,T_Approve_Record record,T_Sys_User user);
	T_Sell_Coupon stop(String number,T_Approve_Record record,T_Sys_User user);
}
