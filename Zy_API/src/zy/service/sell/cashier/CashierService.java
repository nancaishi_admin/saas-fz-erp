package zy.service.sell.cashier;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.form.StringForm;

public interface CashierService {
	PageData<T_Sell_Cashier> page(Map<String,Object> param);
	T_Sell_Cashier queryByID(Integer id);
	void save(T_Sell_Cashier model);
	void update(T_Sell_Cashier model);
	void del(Integer id);
	Map<String,Object> limitByCode(Map<String,Object> param);
	void updateLimit(Map<String,Object> param);
	List<StringForm> listCash(Map<String,Object> param);
	void reset(Integer id);
}
