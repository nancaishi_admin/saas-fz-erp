package zy.service.sell.cashier.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.sell.cashier.CashierDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.entity.sell.cashier.T_Sell_Cashier_Set;
import zy.form.StringForm;
import zy.service.sell.cashier.CashierService;
import zy.util.CommonUtil;
import zy.util.MD5;

@Service
public class CashierServiceImpl implements CashierService{
	@Resource
	private CashierDAO cashierDAO;
	@Override
	public PageData<T_Sell_Cashier> page(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		Object pageSize = params.get(CommonUtil.PAGESIZE);
		Object pageIndex = params.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = cashierDAO.count(params);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		params.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		params.put(CommonUtil.END, _pageSize);
		
		List<T_Sell_Cashier> list = cashierDAO.list(params);
		PageData<T_Sell_Cashier> pageData = new PageData<T_Sell_Cashier>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}

	@Override
	public T_Sell_Cashier queryByID(Integer id) {
		return cashierDAO.queryByID(id);
	}
	@Transactional
	@Override
	public void save(T_Sell_Cashier model) {
		Integer id = cashierDAO.idByCode(model);
		if(null != id && id > 0){
			throw new IllegalArgumentException("该人员已经存在");
		}
		cashierDAO.save(model);
		cashierDAO.saveLimit(model);
	}

	@Override
	public void update(T_Sell_Cashier model) {
		cashierDAO.update(model);
	}

	@Override
	public void del(Integer id) {
		cashierDAO.del(id);
	}

	@Override
	public Map<String, Object> limitByCode(Map<String, Object> param) {
		List<T_Sell_Cashier_Set> list = cashierDAO.limitByCode(param);
		Map<String,Object> map = new HashMap<String, Object>(list.size());
		if(null != list && list.size() > 0){
			for(T_Sell_Cashier_Set set:list){
				map.put(set.getCs_key(), set.getCs_value());
			}
		}
		return map;
	}
	@Transactional
	@Override
	public void updateLimit(Map<String, Object> param) {
		cashierDAO.updateLimit(param);
	}

	@Override
	public List<StringForm> listCash(Map<String, Object> param) {
		return cashierDAO.listCash(param);
	}

	@Override
	public void reset(Integer id) {
		if (id == null || id <= 0) {
			throw new IllegalArgumentException("ID不能为空或小于等于0");
		}
		T_Sell_Cashier t_Sell_Cashier = new T_Sell_Cashier();
		t_Sell_Cashier.setCa_id(id);
		t_Sell_Cashier.setCa_pass(MD5.encryptMd5(CommonUtil.INIT_PWD));
		cashierDAO.updatePwd(t_Sell_Cashier);
	}
}
