package zy.service.sell.allocate.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.base.product.ProductDAO;
import zy.dao.base.size.SizeDAO;
import zy.dao.sell.allocate.SellAllocateDAO;
import zy.dao.stock.data.DataDAO;
import zy.dao.stock.useable.UseableDAO;
import zy.dao.sys.print.PrintDAO;
import zy.dto.common.BarcodeImportDto;
import zy.dto.common.ProductDto;
import zy.dto.sell.allocate.SellAllocateReportDto;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.base.product.T_Base_Barcode;
import zy.entity.base.product.T_Base_Product;
import zy.entity.base.size.T_Base_Size;
import zy.entity.base.size.T_Base_SizeList;
import zy.entity.sell.allocate.T_Sell_Allocate;
import zy.entity.sell.allocate.T_Sell_AllocateList;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.entity.stock.data.T_Stock_DataBill;
import zy.service.sell.allocate.SellAllocateService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;
import zy.vo.common.SizeHorizontalVO;
import zy.vo.sell.SellAllocateVO;

@Service
public class SellAllocateServiceImpl implements SellAllocateService{
	
	@Resource
	private SellAllocateDAO sellAllocateDAO;
	
	@Resource
	private SizeDAO sizeDAO;
	
	@Resource
	private DataDAO dataDAO;
	
	@Resource
	private ProductDAO productDAO;
	
	@Resource
	private UseableDAO useableDAO;
	
	@Resource
	private PrintDAO printDAO;
	
	@Override
	public PageData<T_Sell_Allocate> page(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		Object pageSize = params.get(CommonUtil.PAGESIZE);
		Object pageIndex = params.get(CommonUtil.PAGEINDEX);
		String shop_code = (String)params.get(CommonUtil.SHOP_CODE);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = sellAllocateDAO.count(params);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		params.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		params.put(CommonUtil.END, _pageSize);
		List<T_Sell_Allocate> list = sellAllocateDAO.list(params);
		for (T_Sell_Allocate allocate : list) {
			if (shop_code.equals(allocate.getAc_in_shop())) {//本店铺为调入店铺
				allocate.setAc_sell_money(allocate.getAc_in_sell_money());
			}
		}
		PageData<T_Sell_Allocate> pageData = new PageData<T_Sell_Allocate>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}

	@Override
	public T_Sell_Allocate load(Integer ac_id) {
		return sellAllocateDAO.load(ac_id);
	}

	@Override
	public List<T_Sell_AllocateList> detail_list(Map<String, Object> params) {
		return sellAllocateDAO.detail_list(params);
	}
	
	@Override
	public List<T_Sell_AllocateList> detail_sum(Map<String, Object> params) {
		return sellAllocateDAO.detail_sum(params);
	}
	
	@Override
	public Map<String, Object> detail_size_title(Map<String, Object> params) {
		Integer companyid = (Integer)params.get(CommonUtil.COMPANYID);
		List<String> szgCodes = sellAllocateDAO.detail_szgcode(params);
		if(szgCodes==null||szgCodes.size()==0){
			Map<String, Object> resultMap = new HashMap<String, Object>();
			resultMap.put("titles", new ArrayList<List<String>>());
			return resultMap;
		}
		List<T_Base_SizeList> sizeGroupList = sizeDAO.listBySzg(szgCodes, companyid);
		return SizeHorizontalVO.getJsonSizeTitles(sizeGroupList);
	}
	
	@Override
	public Map<String, Object> detail_size(Map<String, Object> params) {
		Integer companyid = (Integer)params.get(CommonUtil.COMPANYID);
		T_Sell_Cashier cashier = (T_Sell_Cashier)params.get("cashier");
		String ac_out_shop = (String)params.get("ac_out_shop");
		List<String> szgCodes = sellAllocateDAO.detail_szgcode(params);
		if(szgCodes==null||szgCodes.size()==0){
			return new HashMap<String, Object>();
		}
		List<T_Base_SizeList> sizeGroupList = sizeDAO.listBySzg(szgCodes, companyid);
		params.put(CommonUtil.SIDX, "acl_pd_code,acl_cr_code,acl_br_code");
		params.put(CommonUtil.SORD, "ASC");
		List<T_Sell_AllocateList> list = sellAllocateDAO.detail_list(params);
		if(!ac_out_shop.equals(cashier.getCa_shop_code())){
			for (T_Sell_AllocateList item : list) {
				item.setAcl_sell_price(item.getAcl_in_sell_price());
			}
		}
		return SellAllocateVO.getJsonSizeData(sizeGroupList, list);
	}
	
	@Override
	public List<T_Sell_AllocateList> temp_list(Map<String, Object> params) {
		return sellAllocateDAO.temp_list(params);
	}

	@Override
	public List<T_Sell_AllocateList> temp_sum(Map<String, Object> params) {
		return sellAllocateDAO.temp_sum(params);
	}

	@Override
	public Map<String, Object> temp_size_title(Map<String, Object> params) {
		Integer companyid = (Integer)params.get(CommonUtil.COMPANYID);
		List<String> szgCodes = sellAllocateDAO.temp_szgcode(params);
		if(szgCodes==null||szgCodes.size()==0){
			Map<String, Object> resultMap = new HashMap<String, Object>();
			resultMap.put("titles", new ArrayList<List<String>>());
			return resultMap;
		}
		List<T_Base_SizeList> sizeGroupList = sizeDAO.listBySzg(szgCodes, companyid);
		return SizeHorizontalVO.getJsonSizeTitles(sizeGroupList);
	}

	@Override
	public Map<String, Object> temp_size(Map<String, Object> params) {
		Integer companyid = (Integer)params.get(CommonUtil.COMPANYID);
		List<String> szgCodes = sellAllocateDAO.temp_szgcode(params);
		if(szgCodes==null||szgCodes.size()==0){
			return new HashMap<String, Object>();
		}
		List<T_Base_SizeList> sizeGroupList = sizeDAO.listBySzg(szgCodes, companyid);
		params.put(CommonUtil.SIDX, "acl_pd_code,acl_cr_code,acl_br_code");
		params.put(CommonUtil.SORD, "ASC");
		List<T_Sell_AllocateList> temps = sellAllocateDAO.temp_list(params);
		return SellAllocateVO.getJsonSizeData(sizeGroupList, temps);
	}
	
	@Override
	@Transactional
	public Map<String, Object> temp_save_bybarcode(Map<String, Object> params) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String barcode = (String)params.get("barcode");
		Integer amount = (Integer)params.get("amount");
		String sp_code = (String)params.get("sp_code");
		T_Sell_Cashier cashier = (T_Sell_Cashier)params.get("cashier");
		T_Base_Barcode base_Barcode = productDAO.loadBarcode(barcode, cashier.getCompanyid());
		if(base_Barcode == null){
			resultMap.put("result", 3);//条码不存在
			return resultMap;
		}
		T_Sell_AllocateList temp = sellAllocateDAO.temp_loadBySubCode(base_Barcode.getBc_subcode(), cashier.getCa_id(), cashier.getCompanyid());
		if (temp != null) {//临时表存在则直接更新数量
			temp.setAcl_amount(temp.getAcl_amount()+amount);
			sellAllocateDAO.temp_update(temp);
			resultMap.put("result", 1);//update
			resultMap.put("temp", temp);
			return resultMap;
		}
		T_Base_Product base_Product = sellAllocateDAO.load_product(base_Barcode.getBc_pd_code(),sp_code, cashier.getCompanyid());
		
		temp = new T_Sell_AllocateList();
		temp.setAcl_pd_code(base_Barcode.getBc_pd_code());
		temp.setAcl_cr_code(base_Barcode.getBc_color());
		temp.setAcl_sz_code(base_Barcode.getBc_size());
		temp.setAcl_szg_code(base_Product.getPd_szg_code());
		temp.setAcl_br_code(base_Barcode.getBc_bra());
		temp.setAcl_sub_code(temp.getAcl_pd_code()+temp.getAcl_cr_code()+temp.getAcl_sz_code()+temp.getAcl_br_code());
		temp.setAcl_amount(amount);
		temp.setAcl_sell_price(base_Product.getPd_sell_price());
		temp.setAcl_cost_price(base_Product.getPd_cost_price());
		temp.setAcl_remark("");
		temp.setAcl_us_id(cashier.getCa_id());
		temp.setCompanyid(cashier.getCompanyid());
		sellAllocateDAO.temp_save(temp);
		temp.setPd_no(base_Product.getPd_no());
		temp.setPd_name(base_Product.getPd_name());
		temp.setPd_unit(base_Product.getPd_unit());
		temp.setBd_name(base_Product.getPd_bd_name());
		temp.setTp_name(base_Product.getPd_tp_name());
		temp.setCr_name(base_Barcode.getBc_colorname());
		temp.setSz_name(base_Barcode.getBc_sizename());
		temp.setBr_name(base_Barcode.getBc_braname());
		resultMap.put("result", 2);//add
		resultMap.put("temp", temp);
		return resultMap;
	}
	
	@Override
	public PageData<T_Base_Product> page_product(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object pageSize = param.get(CommonUtil.PAGESIZE);
		Object pageIndex = param.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = sellAllocateDAO.count_product(param);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		param.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		param.put(CommonUtil.END, _pageSize);
		
		List<T_Base_Product> list = sellAllocateDAO.list_product(param);
		PageData<T_Base_Product> pageData = new PageData<T_Base_Product>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> temp_loadproduct(Map<String, Object> params) {
		Integer companyid = (Integer)params.get(CommonUtil.COMPANYID);
		String pd_code = (String)params.get("pd_code");
		String sp_code = (String)params.get("sp_code");
		String dp_code = (String)params.get("dp_code");
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Map<String, Object> resultMap = new HashMap<String, Object>();
		T_Base_Product base_Product = sellAllocateDAO.load_product(pd_code,sp_code, companyid);
		Map<String, Object> product = new HashMap<String, Object>();
		product.put("pd_id", base_Product.getPd_id());
		product.put("pd_code", base_Product.getPd_code());
		product.put("pd_no", base_Product.getPd_no());
		product.put("pd_name", base_Product.getPd_name());
		product.put("pd_szg_code", base_Product.getPd_szg_code());
		product.put("pd_unit", base_Product.getPd_unit());
		product.put("pd_year", base_Product.getPd_year());
		product.put("pd_season", base_Product.getPd_season());
		product.put("pd_sell_price", base_Product.getPd_sell_price());
		product.put("pd_cost_price", base_Product.getPd_cost_price());
		product.put("pd_bd_name", base_Product.getPd_bd_name());
		product.put("pd_tp_name", base_Product.getPd_tp_name());
		product.put("pdm_img_path", StringUtil.trimString(base_Product.getPdm_img_path()));
		resultMap.put("product", product);
		params.put("szg_code", base_Product.getPd_szg_code());
		Map<String, Object> productMap = sellAllocateDAO.load_product_size(params);
		List<T_Base_Size> sizes=(List<T_Base_Size>)productMap.get("sizes");
		List<ProductDto> inputs=(List<ProductDto>)productMap.get("inputs");
		List<ProductDto> temps=(List<ProductDto>)productMap.get("temps");
		List<ProductDto> stocks=(List<ProductDto>)productMap.get("stocks");
		Map<String,Object> usableStockMap = useableDAO.loadUseableStock(pd_code, dp_code, companyid);
		resultMap.putAll(SizeHorizontalVO.buildJsonProductInput(pd_code, sizes, inputs, stocks, temps, usableStockMap));
		return resultMap;
	}
	
	@Override
	@Transactional
	public void temp_save(List<T_Sell_AllocateList> temps) {
		if (temps != null && temps.size() > 0) {
			List<T_Sell_AllocateList> temps_add = new ArrayList<T_Sell_AllocateList>();
			List<T_Sell_AllocateList> temps_update = new ArrayList<T_Sell_AllocateList>();
			List<T_Sell_AllocateList> temps_del = new ArrayList<T_Sell_AllocateList>();
			for (T_Sell_AllocateList item : temps) {
				if ("add".equals(item.getOperate_type())){
					if (item.getAcl_amount() > 0) {
						temps_add.add(item);
					}
				}else if("update".equals(item.getOperate_type())){
					if(!item.getAcl_amount().equals(0)){
						temps_update.add(item);
					}else {
						temps_del.add(item);
					}
				}
			}
			if (temps_add.size() > 0) {
				sellAllocateDAO.temp_save(temps_add);
			}
			if (temps_update.size() > 0) {
				sellAllocateDAO.temp_update(temps_update);
			}
			if (temps_del.size() > 0) {
				sellAllocateDAO.temp_del(temps_del);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public void temp_import(Map<String, Object> params) {
		List<String[]> datas = (List<String[]>)params.get("datas");
		T_Sell_Cashier cashier = (T_Sell_Cashier)params.get("cashier");
		String sp_code = (String)params.get("sp_code");
		List<String> barcodes = new ArrayList<String>();
		Map<String, Integer> data_amount = new HashMap<String, Integer>();
		for (String[] data : datas) {
			barcodes.add(data[0]);
			data_amount.put(data[0], Integer.parseInt(data[1]));
		}
		List<BarcodeImportDto> imports = sellAllocateDAO.temp_listByImport(barcodes,sp_code, cashier.getCompanyid());
		if (imports == null || imports.size() == 0) {
			throw new RuntimeException("无数据可导入");
		}
		Map<String, T_Sell_AllocateList> tempsMap = new HashMap<String, T_Sell_AllocateList>();
		List<T_Sell_AllocateList> temps = sellAllocateDAO.temp_list_forimport(cashier.getCa_id(), cashier.getCompanyid());
		for (T_Sell_AllocateList temp : temps) {
			tempsMap.put(temp.getAcl_sub_code(), temp);
		}
		List<T_Sell_AllocateList> temps_add = new ArrayList<T_Sell_AllocateList>();
		List<T_Sell_AllocateList> temps_update = new ArrayList<T_Sell_AllocateList>();
		for (BarcodeImportDto item : imports) {
			if(tempsMap.containsKey(item.getBc_subcode())){//临时表已存在，更新数量
				T_Sell_AllocateList temp = tempsMap.get(item.getBc_subcode());
				temp.setAcl_amount(temp.getAcl_amount()+data_amount.get(item.getBc_barcode()));
				temps_update.add(temp);
			}else {//临时表不存在，新增数据
				T_Sell_AllocateList temp = new T_Sell_AllocateList();
				temp.setAcl_pd_code(item.getBc_pd_code());
				temp.setAcl_sub_code(item.getBc_subcode());
				temp.setAcl_sz_code(item.getBc_size());
				temp.setAcl_szg_code(item.getPd_szg_code());
				temp.setAcl_cr_code(item.getBc_color());
				temp.setAcl_br_code(item.getBc_bra());
				temp.setAcl_amount(data_amount.get(item.getBc_barcode()));
				temp.setAcl_sell_price(item.getSell_price());
				temp.setAcl_cost_price(item.getCost_price());
				temp.setAcl_remark("");
				temp.setAcl_us_id(cashier.getCa_id());
				temp.setCompanyid(cashier.getCompanyid());
				temps_add.add(temp);
			}
		}
		if (temps_add.size() > 0) {
			sellAllocateDAO.temp_save(temps_add);
		}
		if (temps_update.size() > 0) {
			sellAllocateDAO.temp_updateById(temps_update);
		}
	}
	
	@Override
	@Transactional
	public void temp_copy(Map<String, Object> params) {
		String ids = (String)params.get("ids");
		String sp_code = (String)params.get("sp_code");
		T_Sell_Cashier cashier = (T_Sell_Cashier)params.get("cashier");
		if(StringUtil.isEmpty(ids)){
			throw new IllegalArgumentException("参数ids不能为null");
		}
		List<Long> acl_ids = new ArrayList<Long>();
		String[] idArr = ids.split(",");
		for (String id : idArr) {
			acl_ids.add(Long.parseLong(id));
		}
		List<T_Sell_AllocateList> details = sellAllocateDAO.detail_list_forcopy(acl_ids,sp_code);
		if (details == null || details.size() == 0) {
			throw new RuntimeException("单据明细不存在");
		}
		for(T_Sell_AllocateList item:details){
			item.setAcl_us_id(cashier.getCa_id());
		}
		sellAllocateDAO.temp_clear(cashier.getCa_id(), cashier.getCompanyid());
		sellAllocateDAO.temp_save(details);
	}
	
	@Override
	@Transactional
	public void temp_updateAmount(T_Sell_AllocateList temp) {
		sellAllocateDAO.temp_update(temp);
	}
	
	@Override
	@Transactional
	public void temp_updateRemarkById(T_Sell_AllocateList temp) {
		sellAllocateDAO.temp_updateRemarkById(temp);
	}
	
	@Override
	@Transactional
	public void temp_updateRemarkByPdCode(T_Sell_AllocateList temp) {
		sellAllocateDAO.temp_updateRemarkByPdCode(temp);
	}
	
	@Override
	@Transactional
	public void temp_del(Integer acl_id) {
		sellAllocateDAO.temp_del(acl_id);
	}
	
	@Override
	@Transactional
	public void temp_delByPiCode(T_Sell_AllocateList temp) {
		if (temp.getCompanyid() == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		if (temp.getAcl_us_id() == null) {
			throw new IllegalArgumentException("参数us_id不能为null");
		}
		sellAllocateDAO.temp_delByPiCode(temp);
	}
	
	@Override
	@Transactional
	public void temp_clear(Integer us_id,Integer companyid) {
		sellAllocateDAO.temp_clear(us_id, companyid);;
	}
	
	@Override
	@Transactional
	public void save(T_Sell_Allocate allocate, T_Sell_Cashier cashier) {
		if(allocate == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		if(StringUtil.isEmpty(allocate.getAc_out_shop())){
			throw new IllegalArgumentException("调出店铺不能为空");
		}
		if(StringUtil.isEmpty(allocate.getAc_in_shop())){
			throw new IllegalArgumentException("调入店铺不能为空");
		}
		if(StringUtil.isEmpty(allocate.getAc_out_dp())){
			throw new IllegalArgumentException("发货仓库不能为空");
		}
		if(StringUtil.isEmpty(allocate.getAc_man())){
			throw new IllegalArgumentException("经办人不能为空");
		}
		if(StringUtil.isEmpty(allocate.getAc_state())){
			throw new IllegalArgumentException("单据状态不能为空");
		}
		allocate.setCompanyid(cashier.getCompanyid());
		allocate.setAc_sysdate(DateUtil.getCurrentTime());
		//1.查临时表
		List<T_Sell_AllocateList> temps = sellAllocateDAO.temp_list_forsave(allocate.getAc_in_shop(),cashier.getCa_id(), cashier.getCompanyid());
		if(temps == null || temps.size() == 0){
			throw new RuntimeException("单据已保存，请勿重复提交");
		}
		//2.保存单据
		int ac_amount = 0;
		double ac_sell_money = 0d;
		double ac_cost_money = 0d;
		double ac_in_sell_money = 0d;
		double ac_in_cost_money = 0d;
		for (T_Sell_AllocateList temp : temps) {
			ac_amount += temp.getAcl_amount();
			ac_sell_money += temp.getAcl_amount() * temp.getAcl_sell_price();
			ac_cost_money += temp.getAcl_amount() * temp.getAcl_cost_price();
			ac_in_sell_money += temp.getAcl_amount() * temp.getAcl_in_sell_price();
			ac_in_cost_money += temp.getAcl_amount() * temp.getAcl_in_cost_price();
		}
		allocate.setAc_amount(ac_amount);
		allocate.setAc_sell_money(ac_sell_money);
		allocate.setAc_cost_money(ac_cost_money);
		allocate.setAc_in_sell_money(ac_in_sell_money);
		allocate.setAc_in_cost_money(ac_in_cost_money);
		sellAllocateDAO.save(allocate, temps);
		//3.删除临时表
		sellAllocateDAO.temp_clear(cashier.getCa_id(), cashier.getCompanyid());
		//4.调出
		if(CommonUtil.ALLOCATE_STATE_SEND.intValue() == allocate.getAc_state().intValue()){
			List<T_Stock_DataBill> stocks = sellAllocateDAO.listStock(allocate.getAc_number(), allocate.getAc_out_dp(), cashier.getCompanyid());
			List<T_Stock_DataBill> add_stocks = new ArrayList<T_Stock_DataBill>();
			List<T_Stock_DataBill> update_stocks = new ArrayList<T_Stock_DataBill>();
			for (T_Stock_DataBill stock : stocks) {
				if(stock.getSd_id() == null){
					stock.setSd_dp_code(allocate.getAc_out_dp());
					stock.setSd_init(0);
					stock.setSd_date(DateUtil.getCurrentTime());
					stock.setCompanyid(cashier.getCompanyid());
					stock.setSd_amount(-stock.getBill_amount());
					add_stocks.add(stock);
				}else {
					stock.setSd_amount(stock.getSd_amount() - stock.getBill_amount());
					update_stocks.add(stock);
				}
			}
			if (add_stocks.size() > 0) {
				dataDAO.save(add_stocks);
			}
			if (update_stocks.size() > 0) {
				dataDAO.update(update_stocks);
			}
		}
	}
	
	@Override
	@Transactional
	public void update(T_Sell_Allocate allocate, T_Sell_Cashier cashier) {
		if(allocate == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		if(StringUtil.isEmpty(allocate.getAc_out_shop())){
			throw new IllegalArgumentException("调出店铺不能为空");
		}
		if(StringUtil.isEmpty(allocate.getAc_in_shop())){
			throw new IllegalArgumentException("调入店铺不能为空");
		}
		if(StringUtil.isEmpty(allocate.getAc_out_dp())){
			throw new IllegalArgumentException("发货仓库不能为空");
		}
		if(StringUtil.isEmpty(allocate.getAc_man())){
			throw new IllegalArgumentException("经办人不能为空");
		}
		if(StringUtil.isEmpty(allocate.getAc_state())){
			throw new IllegalArgumentException("单据状态不能为空");
		}
		allocate.setCompanyid(cashier.getCompanyid());
		allocate.setAc_sysdate(DateUtil.getCurrentTime());
		
		//1.1查临时表
		List<T_Sell_AllocateList> temps = sellAllocateDAO.temp_list_forsave(allocate.getAc_in_shop(),cashier.getCa_id(), cashier.getCompanyid());
		if(temps == null || temps.size() == 0){
			throw new RuntimeException("单据已保存，请勿重复提交");
		}
		
		//1.2验证单据
		T_Sell_Allocate oldaAllocate = sellAllocateDAO.check(allocate.getAc_number(), cashier.getCompanyid());
		if (oldaAllocate == null || !CommonUtil.AR_STATE_NOTAPPROVE.equals(oldaAllocate.getAc_state())) {
			throw new RuntimeException("单据已修改，请勿重复提交");
		}
		//1.3删除子表
		sellAllocateDAO.deleteList(allocate.getAc_number(), cashier.getCompanyid());
		//2.保存主表
		int ac_amount = 0;
		double ac_sell_money = 0d;
		double ac_cost_money = 0d;
		double ac_in_sell_money = 0d;
		double ac_in_cost_money = 0d;
		for (T_Sell_AllocateList temp : temps) {
			ac_amount += temp.getAcl_amount();
			ac_sell_money += temp.getAcl_amount() * temp.getAcl_sell_price();
			ac_cost_money += temp.getAcl_amount() * temp.getAcl_cost_price();
			ac_in_sell_money += temp.getAcl_amount() * temp.getAcl_in_sell_price();
			ac_in_cost_money += temp.getAcl_amount() * temp.getAcl_in_cost_price();
		}
		allocate.setAc_amount(ac_amount);
		allocate.setAc_sell_money(ac_sell_money);
		allocate.setAc_cost_money(ac_cost_money);
		allocate.setAc_in_sell_money(ac_in_sell_money);
		allocate.setAc_in_cost_money(ac_in_cost_money);
		sellAllocateDAO.update(allocate, temps);
		//3.删除临时表
		sellAllocateDAO.temp_clear(cashier.getCa_id(), cashier.getCompanyid());
		//4.调出
		if(CommonUtil.ALLOCATE_STATE_SEND.intValue() == allocate.getAc_state().intValue()){
			List<T_Stock_DataBill> stocks = sellAllocateDAO.listStock(allocate.getAc_number(), allocate.getAc_out_dp(), cashier.getCompanyid());
			List<T_Stock_DataBill> add_stocks = new ArrayList<T_Stock_DataBill>();
			List<T_Stock_DataBill> update_stocks = new ArrayList<T_Stock_DataBill>();
			for (T_Stock_DataBill stock : stocks) {
				if(stock.getSd_id() == null){
					stock.setSd_dp_code(allocate.getAc_out_dp());
					stock.setSd_init(0);
					stock.setSd_date(DateUtil.getCurrentTime());
					stock.setCompanyid(cashier.getCompanyid());
					stock.setSd_amount(-stock.getBill_amount());
					add_stocks.add(stock);
				}else {
					stock.setSd_amount(stock.getSd_amount() - stock.getBill_amount());
					update_stocks.add(stock);
				}
			}
			if (add_stocks.size() > 0) {
				dataDAO.save(add_stocks);
			}
			if (update_stocks.size() > 0) {
				dataDAO.update(update_stocks);
			}
		}
	}
	
	@Override
	@Transactional
	public T_Sell_Allocate send(String number,String ac_out_dp, T_Sell_Cashier cashier) {
		if (number == null) {
			throw new IllegalArgumentException("参数number不能为null");
		}
		T_Sell_Allocate allocate = sellAllocateDAO.check(number, cashier.getCompanyid());
		if(allocate == null){
			throw new RuntimeException("单据不存在");
		}
		if(!CommonUtil.ALLOCATE_STATE_NOTSEND.equals(allocate.getAc_state())){
			throw new RuntimeException("单据已发货");
		}
		allocate.setAc_out_dp(ac_out_dp);
		allocate.setAc_state(CommonUtil.ALLOCATE_STATE_SEND);
		if(StringUtil.isEmpty(allocate.getAc_out_dp())){
			throw new RuntimeException("发货仓库不能为空");
		}
		sellAllocateDAO.updateSend(allocate);
		
		//3.发货
		List<T_Stock_DataBill> stocks = sellAllocateDAO.listStock(number, allocate.getAc_out_dp(), cashier.getCompanyid());
		List<T_Stock_DataBill> add_stocks = new ArrayList<T_Stock_DataBill>();
		List<T_Stock_DataBill> update_stocks = new ArrayList<T_Stock_DataBill>();
		for (T_Stock_DataBill stock : stocks) {
			if(stock.getSd_id() == null){
				stock.setSd_dp_code(allocate.getAc_out_dp());
				stock.setSd_init(0);
				stock.setSd_date(DateUtil.getCurrentTime());
				stock.setCompanyid(cashier.getCompanyid());
				stock.setSd_amount(-stock.getBill_amount());
				add_stocks.add(stock);
			}else {
				stock.setSd_amount(stock.getSd_amount() - stock.getBill_amount());
				update_stocks.add(stock);
			}
		}
		if (add_stocks.size() > 0) {
			dataDAO.save(add_stocks);
		}
		if (update_stocks.size() > 0) {
			dataDAO.update(update_stocks);
		}
		return allocate;
	}
	
	@Override
	@Transactional
	public T_Sell_Allocate receive(String number,String ac_in_dp, T_Sell_Cashier cashier) {
		if (number == null) {
			throw new IllegalArgumentException("参数number不能为null");
		}
		T_Sell_Allocate allocate = sellAllocateDAO.check(number, cashier.getCompanyid());
		if(allocate == null){
			throw new RuntimeException("单据不存在");
		}
		if(!CommonUtil.ALLOCATE_STATE_SEND.equals(allocate.getAc_state())){
			throw new RuntimeException("单据未发货或已完成");
		}
		allocate.setAc_in_dp(ac_in_dp);
		allocate.setAc_state(CommonUtil.ALLOCATE_STATE_FINISH);
		if(StringUtil.isEmpty(allocate.getAc_in_dp())){
			throw new RuntimeException("收货仓库不能为空");
		}
		allocate.setAc_recedate(DateUtil.getCurrentTime());
		allocate.setAc_receiver(cashier.getEm_name());
		sellAllocateDAO.updateReceive(allocate);
		
		//3.收货
		List<T_Stock_DataBill> stocks = sellAllocateDAO.listStock(number, allocate.getAc_in_dp(), cashier.getCompanyid());
		List<T_Stock_DataBill> add_stocks = new ArrayList<T_Stock_DataBill>();
		List<T_Stock_DataBill> update_stocks = new ArrayList<T_Stock_DataBill>();
		for (T_Stock_DataBill stock : stocks) {
			if(stock.getSd_id() == null){
				stock.setSd_dp_code(allocate.getAc_in_dp());
				stock.setSd_init(0);
				stock.setSd_date(DateUtil.getCurrentTime());
				stock.setCompanyid(cashier.getCompanyid());
				stock.setSd_amount(stock.getBill_amount());
				add_stocks.add(stock);
			}else {
				stock.setSd_amount(stock.getSd_amount() + stock.getBill_amount());
				update_stocks.add(stock);
			}
		}
		if (add_stocks.size() > 0) {
			dataDAO.save(add_stocks);
		}
		if (update_stocks.size() > 0) {
			dataDAO.update(update_stocks);
		}
		return allocate;
	}
	
	@Override
	@Transactional
	public T_Sell_Allocate reject(String number,T_Sell_Cashier cashier) {
		if (number == null) {
			throw new IllegalArgumentException("参数number不能为null");
		}
		T_Sell_Allocate allocate = sellAllocateDAO.check(number, cashier.getCompanyid());
		if(allocate == null){
			throw new RuntimeException("单据不存在");
		}
		if(!CommonUtil.ALLOCATE_STATE_SEND.equals(allocate.getAc_state())){
			throw new RuntimeException("单据未发货或已完成");
		}
		
		allocate.setAc_state(CommonUtil.ALLOCATE_STATE_REJECT);
		sellAllocateDAO.updateState(allocate);
		return allocate;
	}
	
	@Override
	@Transactional
	public T_Sell_Allocate rejectconfirm(String number,T_Sell_Cashier cashier) {
		if (number == null) {
			throw new IllegalArgumentException("参数number不能为null");
		}
		T_Sell_Allocate allocate = sellAllocateDAO.check(number, cashier.getCompanyid());
		if(allocate == null){
			throw new RuntimeException("单据不存在");
		}
		if(!CommonUtil.ALLOCATE_STATE_REJECT.equals(allocate.getAc_state())){
			throw new RuntimeException("单据未被拒收，不能确认！");
		}
		allocate.setAc_state(CommonUtil.ALLOCATE_STATE_NOTSEND);
		sellAllocateDAO.updateState(allocate);
		//3.恢复库存
		List<T_Stock_DataBill> stocks = sellAllocateDAO.listStock(number, allocate.getAc_out_dp(), cashier.getCompanyid());
		List<T_Stock_DataBill> add_stocks = new ArrayList<T_Stock_DataBill>();
		List<T_Stock_DataBill> update_stocks = new ArrayList<T_Stock_DataBill>();
		for (T_Stock_DataBill stock : stocks) {
			if(stock.getSd_id() == null){
				stock.setSd_dp_code(allocate.getAc_out_dp());
				stock.setSd_init(0);
				stock.setSd_date(DateUtil.getCurrentTime());
				stock.setCompanyid(cashier.getCompanyid());
				stock.setSd_amount(stock.getBill_amount());
				add_stocks.add(stock);
			}else {
				stock.setSd_amount(stock.getSd_amount() + stock.getBill_amount());
				update_stocks.add(stock);
			}
		}
		if (add_stocks.size() > 0) {
			dataDAO.save(add_stocks);
		}
		if (update_stocks.size() > 0) {
			dataDAO.update(update_stocks);
		}
		return allocate;
	}
	
	@Override
	@Transactional
	public void initUpdate(String number, Integer us_id, Integer companyid) {
		List<T_Sell_AllocateList> details = sellAllocateDAO.detail_list_forsavetemp(number,companyid);
		for(T_Sell_AllocateList item:details){
			item.setAcl_us_id(us_id);
		}
		sellAllocateDAO.temp_clear(us_id, companyid);
		sellAllocateDAO.temp_save(details);
	}

	@Override
	@Transactional
	public void del(String number, Integer companyid) {
		if (number == null) {
			throw new IllegalArgumentException("参数number不能为null");
		}
		T_Sell_Allocate allocate = sellAllocateDAO.check(number, companyid);
		if(allocate == null){
			throw new RuntimeException("单据不存在");
		}
		if(!CommonUtil.ALLOCATE_STATE_NOTSEND.equals(allocate.getAc_state())){
			throw new RuntimeException("单据已发货通过不能删除！");
		}
		sellAllocateDAO.del(number, companyid);
	}
	
	@Override
	public Map<String, Object> loadPrintData(String number, Integer sp_id, Integer displayMode, T_Sell_Cashier cashier) {
		Map<String, Object> resultMap = printDAO.loadPrint4Bill(sp_id);
		T_Sell_Allocate allocate = sellAllocateDAO.load(number,cashier.getCompanyid());
		List<T_Sell_AllocateList> allocateList = null;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("acl_number", number);
		params.put("companyid", cashier.getCompanyid());
		if(displayMode.intValue() == 2){//汇总模式
			allocateList = sellAllocateDAO.detail_sum(params);
		}else {
			allocateList = sellAllocateDAO.detail_list_print(params);
		}
		if(!allocate.getAc_out_shop().equals(cashier.getCa_shop_code())){
			allocate.setAc_sell_money(allocate.getAc_in_sell_money());
			for (T_Sell_AllocateList item : allocateList) {
				item.setAcl_sell_price(item.getAcl_in_sell_price());
			}
		}
		resultMap.put("allocate", allocate);
		resultMap.put("allocateList", allocateList);
		if(displayMode.intValue() == 1){//尺码模式查询
			List<String> szgCodes = new ArrayList<String>();
			for (T_Sell_AllocateList item : allocateList) {
				if(!szgCodes.contains(item.getAcl_szg_code())){
					szgCodes.add(item.getAcl_szg_code());
				}
			}
			List<T_Base_SizeList> sizeGroupList = sizeDAO.listBySzg(szgCodes, cashier.getCompanyid());
			resultMap.put("sizeGroupList", sizeGroupList);
		}
		return resultMap;
	}
	
	@Override
	public PageData<SellAllocateReportDto> pageReport(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		Object pageSize = params.get(CommonUtil.PAGESIZE);
		Object pageIndex = params.get(CommonUtil.PAGEINDEX);
		String shop_code = (String)params.get(CommonUtil.SHOP_CODE);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = sellAllocateDAO.countReport(params);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		params.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		params.put(CommonUtil.END, _pageSize);
		
		List<SellAllocateReportDto> list = sellAllocateDAO.listReport(params);
		for (SellAllocateReportDto allocate : list) {
			if (shop_code.equals(allocate.getAc_in_shop())) {//本店铺为调入店铺,则显示调入价格
				allocate.setAcl_sell_price(allocate.getAcl_in_sell_price());
			}
		}
		PageData<SellAllocateReportDto> pageData = new PageData<SellAllocateReportDto>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	
}
