package zy.service.sell.shift;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.sell.shift.T_Sell_Shift;

public interface ShiftService {
	PageData<T_Sell_Shift> page(Map<String,Object> params);
	List<T_Sell_Shift> listShop(Map<String,Object> param);
	T_Sell_Shift queryByID(Integer st_id);
	void save(T_Sell_Shift shift);
	void update(T_Sell_Shift shift);
	void del(Integer st_id);
}
