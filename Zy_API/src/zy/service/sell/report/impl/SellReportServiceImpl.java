package zy.service.sell.report.impl;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import zy.dao.base.depot.DepotDAO;
import zy.dao.base.shop.ShopDAO;
import zy.dao.money.expense.ExpenseDAO;
import zy.dao.money.income.IncomeDAO;
import zy.dao.sell.day.DayDAO;
import zy.dao.sell.report.SellReportDAO;
import zy.dao.stock.data.DataDAO;
import zy.dto.sell.allocate.SellAllocateReportDto;
import zy.dto.sell.report.SellStockByShopDepotDto;
import zy.dto.sell.report.SellStockBySizeDto;
import zy.dto.vip.member.ConsumeSnapshotDto;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.base.shop.T_Base_Shop;
import zy.entity.base.size.T_Base_Size;
import zy.entity.money.expense.T_Money_Expense;
import zy.entity.money.income.T_Money_Income;
import zy.entity.sell.cash.T_Sell_Shop;
import zy.entity.sell.cash.T_Sell_ShopList;
import zy.entity.sell.day.T_Sell_Day;
import zy.entity.stock.data.T_Stock_Data;
import zy.service.sell.report.SellReportService;
import zy.util.CommonUtil;
import zy.util.NumberUtil;
import zy.util.StringUtil;
import zy.vo.sell.SellVO;

@Service
public class SellReportServiceImpl implements SellReportService{
	@Resource
	private SellReportDAO sellReportDAO;
	@Resource
	private DayDAO dayDAO;
	@Resource
	private ShopDAO shopDAO;
	@Resource
	private DataDAO dataDAO;
	@Resource
	private IncomeDAO incomeDAO;
	@Resource
	private ExpenseDAO expenseDAO;
	@Resource
	private DepotDAO depotDAO;
	@Override
	public PageData<T_Sell_Shop> pageShop(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		
		Map<String,Object> map = sellReportDAO.countShop(params);
		PageInfo pageInfo = new PageInfo(NumberUtil.toInteger(map.get("count")), pageSize, pageIndex);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		
		List<T_Sell_Shop> list = sellReportDAO.listShop(params);
		PageData<T_Sell_Shop> pageData = new PageData<T_Sell_Shop>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		pageData.setData(map);
		return pageData;
	}

	@Override
	public List<T_Sell_Shop> listShop(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		return sellReportDAO.listShop(params);
	}
	
	@Override
	public List<T_Sell_Shop> listShopByVip(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		return sellReportDAO.listShopByVip(params);
	}
	
	@Override
	public List<T_Sell_ShopList> listShopList(String number,Integer companyid) {
		return sellReportDAO.listShopList(number, companyid);
	}
	
	@Override
	public PageData<T_Sell_ShopList> retailReport(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		
		Map<String,Object> map = sellReportDAO.countRetailReport(params);
		PageInfo pageInfo = new PageInfo(NumberUtil.toInteger(map.get("count")), pageSize, pageIndex);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		
		List<T_Sell_ShopList> list = sellReportDAO.retailReportList(params);
		PageData<T_Sell_ShopList> pageData = new PageData<T_Sell_ShopList>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		pageData.setData(map);
		return pageData;
	}
	
	@Override
	public ConsumeSnapshotDto loadConsumeSnapshot(String vm_code, Integer companyid) {
		ConsumeSnapshotDto  consumeSnapshotDto = sellReportDAO.loadConsumeSnapshot(vm_code, companyid);
		return consumeSnapshotDto;
	}

	@Override
	public PageData<T_Sell_ShopList> noRate(Map<String, Object> paramMap) {
		Object companyid = paramMap.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer pageSize = (Integer)paramMap.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)paramMap.get(CommonUtil.PAGEINDEX);
		
		Map<String,Object> map = sellReportDAO.noRate_count(paramMap);
		PageInfo pageInfo = new PageInfo(NumberUtil.toInteger(map.get("count")), pageSize, pageIndex);
		paramMap.put(CommonUtil.START, (pageIndex-1)*pageSize);
		paramMap.put(CommonUtil.END, pageSize);
		
		List<T_Sell_ShopList> list = sellReportDAO.noRate_list(paramMap);
		PageData<T_Sell_ShopList> pageData = new PageData<T_Sell_ShopList>();
		pageData.setPageInfo(pageInfo);
		pageData.setData(map);
		pageData.setList(list);
		return pageData;
	}

	@Override
	public PageData<T_Sell_ShopList> rank(Map<String, Object> paramMap) {
		Object companyid = paramMap.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		
		List<T_Sell_ShopList> list = sellReportDAO.rank_list(paramMap);
		List<Map<String,Object>> stockList = sellReportDAO.rank_stock(paramMap);
		buildStock(list,stockList);
		PageData<T_Sell_ShopList> pageData = new PageData<T_Sell_ShopList>();
		pageData.setList(list);
		pageData.setData(buildType(list));
		return pageData;
	}
	private void buildStock(List<T_Sell_ShopList> list,List<Map<String,Object>> stockList){
		if(null != list && list.size() > 0){
			if(null != stockList && stockList.size() > 0){
				for(T_Sell_ShopList item:list){
					for(Map<String,Object> map:stockList){
						if(item.getBd_code().equals(StringUtil.trimString(map.get("bd_code")))){
							item.setSd_amount(NumberUtil.toInteger(map.get("sd_amount")));
							item.setSd_money(NumberUtil.toDouble(map.get("sd_money")));
						}
					}
				}
			}
		}
	}
	private Map<String,Object> buildType(List<T_Sell_ShopList> list){
		Map<String,Object> map = new HashMap<String, Object>(5);
		if(null != list && list.size() > 0){
			double sell_money = 0d,money = 0d,profit = 0d,cost = 0d,sd_money = 0d;
			Integer amount = 0,count = 0,trys = 0,recive = 0,vips = 0;
			for(T_Sell_ShopList item:list){
				sell_money += item.getShl_sell_money();
				sd_money += item.getSd_money();
				money += item.getShl_money();
				profit += item.getShl_profit();
				amount += item.getShl_amount();
				cost += item.getShl_cost_money();
				count += NumberUtil.toInteger(item.getShl_count());
				recive += NumberUtil.toInteger(item.getDa_receive());
				trys += NumberUtil.toInteger(item.getDa_try());
				vips += NumberUtil.toInteger(item.getShl_vips());
			}
			for(T_Sell_ShopList item:list){
				item.setShl_rate(StringUtil.double2Two(item.getShl_money()/money*100));
				item.setShl_profit_rate(StringUtil.double2Two(item.getShl_profit()/item.getShl_money()*100));
				item.setShl_profit_ratio(StringUtil.double2Two((item.getShl_profit()/profit)*100));
				item.setSd_rate(StringUtil.double2Two((item.getSd_money()/sd_money)*100));
			}
			map.put("shl_sell_money", sell_money);
			map.put("shl_money", money);
			map.put("sd_money", sd_money);
			map.put("shl_profit", profit);
			map.put("shl_amount", amount);
			map.put("shl_cost_money", cost);
			map.put("shl_count", count);
			map.put("da_try", trys);
			map.put("da_receive", recive);
			map.put("shl_vips", vips);
		}
		return map;
	}
	
	private Map<String,Object> buildMap(List<T_Sell_ShopList> list){
		Map<String,Object> map = new HashMap<String, Object>(5);
		if(null != list && list.size() > 0){
			double sell_money = 0d,money = 0d,profit = 0d,cost = 0d;
			Integer amount = 0,count = 0,trys = 0,recive = 0,vips = 0;
			for(T_Sell_ShopList item:list){
				sell_money += item.getShl_sell_money();
				money += item.getShl_money();
				profit += item.getShl_profit();
				amount += item.getShl_amount();
				cost += item.getShl_cost_money();
				count += NumberUtil.toInteger(item.getShl_count());
				recive += NumberUtil.toInteger(item.getDa_receive());
				trys += NumberUtil.toInteger(item.getDa_try());
				vips += NumberUtil.toInteger(item.getShl_vips());
			}
			for(T_Sell_ShopList item:list){
				item.setShl_rate(StringUtil.double2Two(item.getShl_money()/money*100));
				item.setShl_profit_rate(StringUtil.double2Two(item.getShl_profit()/item.getShl_money()*100));
				item.setShl_profit_ratio(StringUtil.double2Two((item.getShl_profit()/profit)*100));
			}
			map.put("shl_sell_money", sell_money);
			map.put("shl_money", money);
			map.put("shl_profit", profit);
			map.put("shl_amount", amount);
			map.put("shl_cost_money", cost);
			map.put("shl_count", count);
			map.put("da_try", trys);
			map.put("da_receive", recive);
			map.put("shl_vips", vips);
		}
		return map;
	}
	private Map<String,Object> buildSell(List<T_Sell_Shop> list){
		Map<String,Object> map = new HashMap<String, Object>(5);
		if(null != list && list.size() > 0){
			double sell_money = 0d,money = 0d,profit = 0d,amount = 0d,cost = 0d,income = 0,expense = 0,vc_after_money = 0d;
			for(T_Sell_Shop item:list){
				sell_money += item.getSh_sell_money();
				money += item.getSh_money();
				profit += item.getSh_profit();
				amount += item.getSh_amount();
				cost += item.getSh_cost_money();
				income += NumberUtil.toDouble(item.getIncome_money());
				expense += NumberUtil.toDouble(item.getExpense_money());
				if(item.getSh_vc_after_money() != null){
					vc_after_money += item.getSh_vc_after_money();
				}
			}
			for(T_Sell_Shop item:list){
				item.setSh_rate(StringUtil.double2Two(item.getSh_money()/money*100));
				item.setSh_profit_rate(StringUtil.double2Two(item.getSh_profit()/item.getSh_money()*100));//是折后金额还是零售金额？
				item.setSh_profit_ratio(StringUtil.double2Two((item.getSh_profit()/profit)*100));
			}
			map.put("sh_sell_money", sell_money);
			map.put("sh_money", money);
			map.put("sh_profit", profit);
			map.put("sh_amount", amount);
			map.put("sh_cost_money", cost);
			map.put("income", income);
			map.put("expense", expense);
			map.put("vc_after_money", vc_after_money);
		}
		return map;
	}
	@Override
	public PageData<T_Sell_ShopList> empRank(Map<String, Object> paramMap) {
		Object companyid = paramMap.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		
		List<T_Sell_ShopList> list = sellReportDAO.empRank(paramMap);
		PageData<T_Sell_ShopList> pageData = new PageData<T_Sell_ShopList>();
		pageData.setList(list);
		pageData.setData(buildMap(list));
		return pageData;
	}
	@Override
	public PageData<T_Sell_ShopList> buyRank(Map<String, Object> paramMap) {
		Object companyid = paramMap.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		List<T_Sell_ShopList> list = sellReportDAO.buyRank(paramMap);
		PageData<T_Sell_ShopList> pageData = new PageData<T_Sell_ShopList>();
		pageData.setList(list);
		pageData.setData(buildSupply(list));
		return pageData;
	}
	private Map<String,Object> buildSupply(List<T_Sell_ShopList> list){
		Map<String,Object> map = new HashMap<String, Object>(5);
		if(null != list && list.size() > 0){
			double sell_money = 0d,money = 0d,profit = 0d,cost = 0d;
			Integer amount = 0;
			for(T_Sell_ShopList item:list){
				sell_money += item.getShl_sell_money();
				money += item.getShl_money();
				profit += item.getShl_profit();
				amount += item.getShl_amount();
				cost += item.getShl_cost_money();
			}
			for(T_Sell_ShopList item:list){
				item.setShl_rate(StringUtil.double2Two(item.getShl_money()/money*100));
				item.setShl_profit_rate(StringUtil.double2Two(item.getShl_profit()/item.getShl_sell_money()*100));
				item.setShl_profit_ratio(StringUtil.double2Two((item.getShl_profit()/profit)*100));
			}
			map.put("shl_sell_money", sell_money);
			map.put("shl_money", money);
			map.put("shl_profit", profit);
			map.put("shl_amount", amount);
			map.put("shl_cost_money", cost);
		}
		return map;
	}
	@Override
	public PageData<T_Sell_ShopList> noRank(Map<String, Object> paramMap) {
		Object companyid = paramMap.get(CommonUtil.COMPANYID);
		Integer pageSize = (Integer)paramMap.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)paramMap.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Map<String,Object> map = sellReportDAO.noRank_count(paramMap);
		PageInfo pageInfo = new PageInfo(NumberUtil.toInteger(map.get("count")), pageSize, pageIndex);
		paramMap.put(CommonUtil.START, (pageIndex-1)*pageSize);
		paramMap.put(CommonUtil.END, pageSize);
		List<T_Sell_ShopList> list = sellReportDAO.noRank_list(paramMap);
		paramMap.put("pd_codes", SellVO.listToString(list));
		List<T_Stock_Data> data = dataDAO.listByPdCode(paramMap);
		SellVO.buildStock(list,data);//合并库存
		buildMap(list, map);//计算占比
		PageData<T_Sell_ShopList> pageData = new PageData<T_Sell_ShopList>();
		pageData.setList(list);
		pageData.setPageInfo(pageInfo);
		pageData.setData(map);
		return pageData;
	}
	
	@Override
	public PageData<T_Sell_ShopList> styleRank(Map<String, Object> paramMap) {
		Object companyid = paramMap.get(CommonUtil.COMPANYID);
		Integer pageSize = (Integer)paramMap.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)paramMap.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Map<String,Object> map = sellReportDAO.styleRank_count(paramMap);
		PageInfo pageInfo = new PageInfo(NumberUtil.toInteger(map.get("count")), pageSize, pageIndex);
		paramMap.put(CommonUtil.START, (pageIndex-1)*pageSize);
		paramMap.put(CommonUtil.END, pageSize);
		List<T_Sell_ShopList> list = sellReportDAO.styleRank_list(paramMap);
		buildMap(list, map);//计算占比
		PageData<T_Sell_ShopList> pageData = new PageData<T_Sell_ShopList>();
		pageData.setList(list);
		pageData.setPageInfo(pageInfo);
		pageData.setData(map);
		return pageData;
	}

	@Override
	public PageData<T_Sell_ShopList> listRank(Map<String, Object> paramMap) {
		Object companyid = paramMap.get(CommonUtil.COMPANYID);
		Integer pageSize = (Integer)paramMap.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)paramMap.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Map<String,Object> map = sellReportDAO.listRank_count(paramMap);
		PageInfo pageInfo = new PageInfo(NumberUtil.toInteger(map.get("count")), pageSize, pageIndex);
		paramMap.put(CommonUtil.START, (pageIndex-1)*pageSize);
		paramMap.put(CommonUtil.END, pageSize);
		List<T_Sell_ShopList> list = sellReportDAO.listRank_list(paramMap);
		buildMap(list, map);//计算占比
		PageData<T_Sell_ShopList> pageData = new PageData<T_Sell_ShopList>();
		pageData.setList(list);
		pageData.setPageInfo(pageInfo);
		pageData.setData(map);
		return pageData;
	}

	@Override
	public PageData<T_Sell_Shop> shopRank(Map<String, Object> paramMap) {
		Object companyid = paramMap.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		
		List<T_Sell_Shop> list = sellReportDAO.shopRank(paramMap);
		List<T_Sell_Day> dayList = dayDAO.listByShop(paramMap);
		if(null != list && list.size() > 0){
			if(null != dayList && dayList.size() > 0){
				for(T_Sell_Shop item:list){
					for(T_Sell_Day day:dayList){
						if(day.getDa_shop_code().equals(item.getSh_shop_code())){
							item.setDa_come(day.getDa_come());
							item.setDa_receive(day.getDa_receive());
							item.setDa_try(day.getDa_try());
						}
					}
				}
			}
		}
		PageData<T_Sell_Shop> pageData = new PageData<T_Sell_Shop>();
		pageData.setList(list);
		pageData.setData(buildSell(list));
		return pageData;
	}
	@Override
	public PageData<T_Sell_Shop> month(Map<String, Object> paramMap) {
		Object companyid = paramMap.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		
		List<T_Sell_Shop> list = sellReportDAO.month(paramMap);
		
		List<T_Sell_Day> dayList = dayDAO.listByDate(paramMap);
		
		List<T_Money_Income> inList = incomeDAO.monthMoney(paramMap);
		
		List<T_Money_Expense> exList = expenseDAO.monthMoney(paramMap);
		if(null != list && list.size() > 0){
			for(T_Sell_Shop item:list){
				double real_money = NumberUtil.toDouble(item.getSh_money()-item.getSh_cd_money()-
									item.getSh_vc_money()-item.getSh_ec_money()-
									item.getSh_point_money());
				item.setSh_real_money(real_money);
				item.setSh_profit(real_money-item.getSh_cost_money());
				if(null != dayList && dayList.size() > 0){
					for(T_Sell_Day day:dayList){
						if(day.getDa_date().equals(item.getSh_date())){
							item.setDa_come(day.getDa_come());
							item.setDa_receive(day.getDa_receive());
							item.setDa_try(day.getDa_try());
						}
					}
				}
				if(null != inList && inList.size() > 0){
					for(T_Money_Income in:inList){
						if(in.getIc_date().equals(item.getSh_date())){
							item.setIncome_money(in.getIc_money());
						}
					}
				}
				if(null != exList && exList.size() > 0){
					for(T_Money_Expense ex:exList){
						if(ex.getEp_date().equals(item.getSh_date())){
							item.setIncome_money(ex.getEp_money());
						}
					}
				}
			}
		}
		PageData<T_Sell_Shop> pageData = new PageData<T_Sell_Shop>();
		pageData.setList(list);
		pageData.setData(buildSell(list));
		return pageData;
	}
	
	@Override
	public PageData<T_Sell_Shop> year(Map<String, Object> paramMap) {
		Object companyid = paramMap.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		
		List<T_Sell_Shop> list = sellReportDAO.year(paramMap);
		
		if(null != list && list.size() > 0){
			for(T_Sell_Shop item:list){
				double real_money = NumberUtil.toDouble(item.getSh_money()-item.getSh_cd_money()-
									item.getSh_vc_money()-item.getSh_ec_money()-
									item.getSh_point_money());
				item.setSh_real_money(real_money);
				item.setSh_profit(real_money-item.getSh_cost_money());
			}
		}
		PageData<T_Sell_Shop> pageData = new PageData<T_Sell_Shop>();
		pageData.setList(list);
		pageData.setData(buildSell(list));
		return pageData;
	}
	@Override
	public PageData<Map<String, Object>> shopSell(Map<String, Object> paramMap) {
		List<T_Base_Shop> shops = shopDAO.sub_list(paramMap);
		List<T_Sell_ShopList> list = sellReportDAO.shopSell(paramMap);
		return buildSellMap(list, shops);
	}

	
	private PageData<Map<String, Object>> buildSellMap(List<T_Sell_ShopList> list,List<T_Base_Shop> shops){
		int[] sumAmount = new int[shops.size()+1];
		double[] sumMoney = new double[shops.size()+1];
		PageData<Map<String, Object>> pageData = new PageData<Map<String, Object>>();
		if(null != list && list.size() > 0){
			List<Map<String,Object>> _list = new ArrayList<Map<String,Object>>();
			Map<String,Object> amountMap,moneyMap;
			for(T_Sell_ShopList item:list){
				int flag = 0;
				for(Map<String,Object> map:_list){
					if(StringUtil.trimString(map.get("bd_code")).equals((item.getBd_code()))){
						flag = 1;
						double money = 0d;
						int index = 0,amount = 0;
						for(T_Base_Shop shop:shops){
							if(item.getShl_shop_code().equals(shop.getSp_code())){
								if(StringUtil.trimString(map.get("shl_type")).equals("数量")){
									map.put("s"+shop.getSp_code(), item.getShl_amount());
									amount += item.getShl_amount();
									sumAmount[index] += item.getShl_amount();
								}else{
									map.put("s"+shop.getSp_code(), item.getShl_money());
									money += item.getShl_money();
									sumMoney[index] += NumberUtil.toDouble(item.getShl_money());
								}
							}
							index ++;
						}
						if(StringUtil.trimString(map.get("shl_type")).equals("数量")){
							map.put("shl_total", NumberUtil.toInteger(map.get("shl_total"))+amount);
						}else{
							map.put("shl_total", NumberUtil.toDouble(map.get("shl_total"))+money);
						}
					}
				}
				if(flag == 0){
					amountMap = new HashMap<String, Object>();
					moneyMap = new HashMap<String, Object>();
					amountMap.put("shl_id", item.getShl_id());
					moneyMap.put("shl_id", item.getShl_id()+"1");
					amountMap.put("bd_code", item.getBd_code());
					moneyMap.put("bd_code", item.getBd_code());
					amountMap.put("bd_name", item.getBd_name());
					moneyMap.put("bd_name", item.getBd_name());
					amountMap.put("shl_type", "数量");
					moneyMap.put("shl_type", "金额");
					double money = 0d;
					int index = 0,amount = 0;
					for(T_Base_Shop shop:shops){
						if(item.getShl_shop_code().equals(shop.getSp_code())){
							amountMap.put("s"+shop.getSp_code(), item.getShl_amount());
							moneyMap.put("s"+shop.getSp_code(), item.getShl_money());
							money += item.getShl_money();
							amount += item.getShl_amount();
							sumAmount[index] += item.getShl_amount();
							sumMoney[index] += NumberUtil.toDouble(item.getShl_money());
						}else{
							amountMap.put("s"+shop.getSp_code(), 0);
							moneyMap.put("s"+shop.getSp_code(), 0d);
						}
						index ++;
					}
					amountMap.put("shl_total", amount);
					moneyMap.put("shl_total", money);
					_list.add(amountMap);
					_list.add(moneyMap);
				}
			}
			final Collator collator = Collator.getInstance(java.util.Locale.CHINA);
            Collections.sort(_list, new Comparator<Map<String, Object>>() {
                @Override
                public int compare(Map<String, Object> o1, Map<String, Object> o2) {
                    int c = 0;
                    Object val1 = o1.get("bd_code");
                    Object val2 = o2.get("bd_code");
                    c = collator.compare(val1, val2);
                    return c;
                }
            });
            pageData.setList(_list);
            Map<String, Object> userData = new HashMap<String, Object>();
            userData.put("bd_name", "合计:");
            for (int i = 0; i < shops.size(); i++) {//计算小计的合计
            	sumAmount[shops.size()] += sumAmount[i];
            	sumMoney[shops.size()] += sumMoney[i];
            	userData.put("s"+shops.get(i).getSp_code(), sumAmount[i]+"<br/>"+StringUtil.double2Two(sumMoney[i]));
			}
            userData.put("shl_total", sumAmount[shops.size()]+"<br/>"+StringUtil.double2Two(sumMoney[shops.size()]));
            pageData.setData(userData);
		}
		return pageData;
	}
	private Map<String,Object> buildMap(List<T_Sell_ShopList> list,Map<String,Object> map){
		if(null != list && list.size() > 0){
			double money = NumberUtil.toDouble(map.get("shl_money")),profit = NumberUtil.toDouble(map.get("shl_profit"));
			for(T_Sell_ShopList item:list){
				item.setShl_rate(StringUtil.double2Two(item.getShl_money()/money*100));
				item.setShl_profit_rate(StringUtil.double2Two(item.getShl_profit()/item.getShl_sell_money()*100));
				item.setShl_profit_ratio(StringUtil.double2Two((item.getShl_profit()/profit)*100));
			}
		}
		return map;
	}

	@Override
	public PageData<SellAllocateReportDto> sell_allocate(
			Map<String, Object> params) {
		Object shop_type = params.get(CommonUtil.SHOP_TYPE);
		Object shop_code = params.get(CommonUtil.SHOP_CODE);
		Object pageSize = params.get(CommonUtil.PAGESIZE);
		Object pageIndex = params.get(CommonUtil.PAGEINDEX);
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		String in_shop_code = (String)params.get("in_shop_code");
		String out_shop_code = (String)params.get("out_shop_code");
		
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		List<String> in_shop_codes = new ArrayList<String>();
		List<String> out_shop_codes = new ArrayList<String>();
		//获取店铺code
		List<String> shop_codes = shopDAO.queryShopCode(params);
		
		if (!StringUtil.trimString(in_shop_code).equals("")) {
			in_shop_codes.add(in_shop_code);
		} else {
			if(CommonUtil.ONE.equals(shop_type) || CommonUtil.TWO.equals(shop_type)){//总公司、分公司
				in_shop_codes.addAll(shop_codes);
			}else if (CommonUtil.THREE.equals(shop_type) || CommonUtil.FIVE.equals(shop_type)) {// 自营店或合伙店
				in_shop_codes.add((String)shop_code);
			}
		}
		if (!StringUtil.trimString(out_shop_code).equals("")) {
			out_shop_codes.add(out_shop_code);
		} else {
			if(CommonUtil.ONE.equals(shop_type) || CommonUtil.TWO.equals(shop_type)){//总公司、分公司
				out_shop_codes.addAll(shop_codes);
			}else if (CommonUtil.THREE.equals(shop_type) || CommonUtil.FIVE.equals(shop_type)) {// 自营店或合伙店
				out_shop_codes.add((String)shop_code);
			}
		}
		params.put("in_shop_codes", in_shop_codes);
		params.put("out_shop_codes", out_shop_codes);
		
		Integer totalCount = sellReportDAO.sell_allocate_count(params);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		params.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		params.put(CommonUtil.END, _pageSize);
		
		List<SellAllocateReportDto> list = sellReportDAO.sell_allocate(params);
		PageData<SellAllocateReportDto> pageData = new PageData<SellAllocateReportDto>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		pageData.setData(sellReportDAO.sell_allocate_sum(params));
		return pageData;
	}

	@Override
	public String getSupply(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return sellReportDAO.getSupply(params);
	}
	
	@Override
	public List<SellStockByShopDepotDto> query_stock_detail(Map<String, Object> params) {
		String pd_code = (String)params.get("pd_code");
		Assert.notNull(params.get(CommonUtil.COMPANYID), "连接超时，请重新登录!");
		Assert.hasText(pd_code,"pd_code不能为空");
		
		List<SellStockByShopDepotDto> dtos = sellReportDAO.query_stock_detail(params);
		for (SellStockByShopDepotDto dto : dtos) {
			List<String> shopCodes = new ArrayList<String>();
			for (String key : dto.getShop_SellAmountMap().keySet()) {
				if(!shopCodes.contains(key)){
					shopCodes.add(key);
				}
			}
			for (String key : dto.getShop_StockAmountMap().keySet()) {
				if(!shopCodes.contains(key)){
					shopCodes.add(key);
				}
			}
			for (String key : dto.getShop_OnWayAmountMap().keySet()) {
				if(!shopCodes.contains(key)){
					shopCodes.add(key);
				}
			}
			Map<String, String> shop_AmountMap = new HashMap<String, String>();
			int sellAmount,stockAmount,onwayAmount;
			for (String shopCode : shopCodes) {
				sellAmount = 0;
				stockAmount = 0;
				onwayAmount = 0;
				if(dto.getShop_SellAmountMap().containsKey(shopCode)){
					sellAmount = dto.getShop_SellAmountMap().get(shopCode);
				}
				if(dto.getShop_StockAmountMap().containsKey(shopCode)){
					stockAmount = dto.getShop_StockAmountMap().get(shopCode);
				}
				if(dto.getShop_OnWayAmountMap().containsKey(shopCode)){
					onwayAmount = dto.getShop_OnWayAmountMap().get(shopCode);
				}
				shop_AmountMap.put(shopCode, sellAmount+"/"+stockAmount+"/"+onwayAmount);
			}
			dto.setShop_AmountMap(shop_AmountMap);
		}
		return dtos;
	}

	@Override
	public List<T_Base_Size> loadProductSize(String pd_code,Integer companyid){
		return sellReportDAO.loadProductSize(pd_code, companyid);
	}
	
	@Override
	public List<SellStockBySizeDto> query_stock_detail_size(Map<String, Object> params) {
		String pd_code = (String)params.get("pd_code");
		Assert.notNull(params.get(CommonUtil.COMPANYID), "连接超时，请重新登录!");
		Assert.hasText(pd_code,"pd_code不能为空");
		
		List<SellStockBySizeDto> dtos = sellReportDAO.query_stock_detail_size(params);
		for (SellStockBySizeDto dto : dtos) {
			List<String> sizeCodes = new ArrayList<String>();
			for (String key : dto.getSellAmountMap().keySet()) {
				if(!sizeCodes.contains(key)){
					sizeCodes.add(key);
				}
			}
			for (String key : dto.getStockAmountMap().keySet()) {
				if(!sizeCodes.contains(key)){
					sizeCodes.add(key);
				}
			}
			Map<String, String> amountMap = new HashMap<String, String>();
			int sellAmount,stockAmount;
			for (String sizeCode : sizeCodes) {
				sellAmount = 0;
				stockAmount = 0;
				if(dto.getSellAmountMap().containsKey(sizeCode)){
					sellAmount = dto.getSellAmountMap().get(sizeCode);
				}
				if(dto.getStockAmountMap().containsKey(sizeCode)){
					stockAmount = dto.getStockAmountMap().get(sizeCode);
				}
				amountMap.put(sizeCode, sellAmount+"/"+stockAmount);
			}
			dto.setAmountMap(amountMap);
		}
		return dtos;
	}
	
}
