package zy.service.sell.report;

import java.util.List;
import java.util.Map;

import zy.dto.sell.allocate.SellAllocateReportDto;
import zy.dto.sell.report.SellStockByShopDepotDto;
import zy.dto.sell.report.SellStockBySizeDto;
import zy.dto.vip.member.ConsumeSnapshotDto;
import zy.entity.PageData;
import zy.entity.base.size.T_Base_Size;
import zy.entity.sell.cash.T_Sell_Shop;
import zy.entity.sell.cash.T_Sell_ShopList;

public interface SellReportService {
	PageData<T_Sell_Shop> pageShop(Map<String,Object> params);
	List<T_Sell_Shop> listShop(Map<String,Object> params);
	List<T_Sell_Shop> listShopByVip(Map<String, Object> params);
	List<T_Sell_ShopList> listShopList(String number,Integer companyid);
	ConsumeSnapshotDto loadConsumeSnapshot(String vm_code,Integer companyid);
	PageData<T_Sell_ShopList> retailReport(Map<String,Object> params);
	PageData<T_Sell_ShopList> noRate(Map<String,Object> paramMap);
	PageData<T_Sell_ShopList> rank(Map<String,Object> paramMap);
	PageData<T_Sell_ShopList> empRank(Map<String,Object> paramMap);
	PageData<T_Sell_ShopList> noRank(Map<String,Object> paramMap);
	PageData<T_Sell_ShopList> listRank(Map<String,Object> paramMap);
	PageData<T_Sell_ShopList> buyRank(Map<String,Object> paramMap);
	PageData<T_Sell_ShopList> styleRank(Map<String,Object> paramMap);
	PageData<T_Sell_Shop> shopRank(Map<String,Object> paramMap);
	PageData<T_Sell_Shop> month(Map<String,Object> paramMap);
	PageData<T_Sell_Shop> year(Map<String,Object> paramMap);
	PageData<Map<String,Object>> shopSell(Map<String,Object> paramMap);
	PageData<SellAllocateReportDto> sell_allocate(Map<String,Object> params);
	String getSupply(Map<String,Object> params);
	List<SellStockByShopDepotDto> query_stock_detail(Map<String, Object> params);
	List<T_Base_Size> loadProductSize(String pd_code,Integer companyid);
	List<SellStockBySizeDto> query_stock_detail_size(Map<String, Object> params);
}
