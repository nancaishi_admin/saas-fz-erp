package zy.service.sell.voucher;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.sell.voucher.T_Sell_Voucher;
import zy.entity.sell.voucher.T_Sell_VoucherList;

public interface VoucherService {
	PageData<T_Sell_Voucher> page(Map<String,Object> params);
	PageData<T_Sell_VoucherList> pageDetail(Map<String,Object> params);
	T_Sell_Voucher queryByID(Integer vc_id);
	List<T_Sell_VoucherList> listDetail(String vc_code,Integer companyid);
	void save(Map<String, Object> params);
	void update(T_Sell_Voucher voucher);
	void updateState(T_Sell_Voucher voucher);
	void del(Integer vc_id);
	//-----------前台功能------------------
	List<T_Sell_Voucher> listShop(Map<String,Object> params);
	void saveShop(Map<String, Object> params);
	T_Sell_Voucher queryByCode(Map<String,Object> param);
}
