package zy.service.sell.sell.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.base.emp.EmpDAO;
import zy.dao.base.shop.ShopDAO;
import zy.dao.sell.sell.SellDAO;
import zy.dao.set.SellSetDAO;
import zy.dao.vip.member.MemberDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.base.emp.T_Base_Emp;
import zy.entity.base.shop.T_Base_Shop;
import zy.entity.sell.cash.T_Sell_Shop;
import zy.entity.sell.cash.T_Sell_ShopList;
import zy.entity.vip.member.T_Vip_Member;
import zy.service.sell.sell.SellService;
import zy.util.CommonUtil;
import zy.util.NumberUtil;
import zy.util.StringUtil;
import zy.vo.sell.SellVO;
@Service
public class SellServiceImpl implements SellService{
	@Resource
	private EmpDAO empDAO;
	@Resource
	private SellDAO sellDAO;
	@Resource
	private SellSetDAO sellSetDAO;
	@Resource
	private ShopDAO shopDAO;
	@Resource
	private MemberDAO memberDAO;
	@Override
	public PageData<T_Sell_Shop> pageShop(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object pageSize = param.get(CommonUtil.PAGESIZE);
		Object pageIndex = param.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Map<String,Object> map = sellDAO.countShop(param);
		PageInfo pageInfo = new PageInfo(NumberUtil.toInteger(map.get("count")), _pageSize, _pageIndex);
		param.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		param.put(CommonUtil.END, _pageSize);
		
		List<T_Sell_Shop> list = sellDAO.listShop(param);
		PageData<T_Sell_Shop> pageData = new PageData<T_Sell_Shop>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		pageData.setData(map);
		return pageData;
	}
	@Override
	public PageData<T_Sell_ShopList> pageShopList(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object pageSize = param.get(CommonUtil.PAGESIZE);
		Object pageIndex = param.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = sellDAO.countShopList(param);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		param.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		param.put(CommonUtil.END, _pageSize);
		
		List<T_Sell_ShopList> list = sellDAO.listShopList(param);
		List<T_Base_Emp> empList = empDAO.listCombo(param);
		handList(empList, list);
		PageData<T_Sell_ShopList> pageData = new PageData<T_Sell_ShopList>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}

	private void handList(List<T_Base_Emp> empList,List<T_Sell_ShopList> list){
		try {
			if(null != list && list.size() > 0){
				if(null != empList && empList.size() > 0){
					for(T_Sell_ShopList shop:list){
						for(T_Base_Emp emp:empList){
							if(StringUtil.trimString(shop.getShl_em_code()).equals(emp.getEm_code())){
								shop.setEm_name(emp.getEm_name());
							}
							if(StringUtil.trimString(shop.getShl_main()).equals(emp.getEm_code())){
								shop.setMain_name(emp.getEm_name());
							}
							if(StringUtil.trimString(shop.getShl_slave()).equals(emp.getEm_code())){
								shop.setSlave_name(emp.getEm_name());
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public Map<String, Object> print(Map<String, Object> param) {
		Map<String,Object> printData = sellSetDAO.print(param);
		T_Sell_Shop sell = sellDAO.queryByID(param);
		T_Vip_Member vip = null;
		if(null != sell){
			sell.setSh_change_money(NumberUtil.toDouble(param.get("change_money")));
			List<T_Sell_ShopList> list = sellDAO.listSell(sell);
			T_Base_Shop shop = shopDAO.load((String)param.get(CommonUtil.SHOP_CODE), (Integer)param.get(CommonUtil.COMPANYID));
			if(!StringUtil.isEmpty(sell.getSh_vip_code())){
				vip = memberDAO.vipByCode(sell.getSh_vip_code(), (Integer)param.get(CommonUtil.COMPANYID));
			}
			return SellVO.buildPrint(list,sell,printData,shop,vip);
		}
		return null;
	}
	
}
