package zy.service.vip.trylist.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.vip.trylist.TryListDAO;
import zy.entity.vip.trylist.T_Vip_TryList;
import zy.service.vip.trylist.TryListService;
import zy.util.CommonUtil;

@Service
public class TryListServiceImpl implements TryListService{
	@Resource
	private TryListDAO tryListDAO;

	@Override
	public List<T_Vip_TryList> list(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		return tryListDAO.list(params);
	}
	
	@Override
	@Transactional
	public void save(T_Vip_TryList tryList) {
		tryListDAO.save(tryList);
	}
}
