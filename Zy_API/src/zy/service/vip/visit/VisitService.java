package zy.service.vip.visit;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.sell.ecoupon.T_Sell_Ecoupon_User;
import zy.entity.vip.member.T_Vip_Member;
import zy.entity.vip.visit.T_Vip_Visit;

public interface VisitService {
	PageData<T_Vip_Member> page(Map<String,Object> params);
	List<T_Vip_Member> list(Map<String, Object> params);
	List<T_Vip_Member> list_birthday_visit(Map<String, Object> params);
	void save_visit_ecoupon(T_Vip_Visit visit,T_Sell_Ecoupon_User ecouponUser);
	PageData<T_Vip_Visit> detail_page(Map<String,Object> params);
}
