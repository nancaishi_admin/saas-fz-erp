package zy.service.vip.rate;

import java.util.Map;

import zy.entity.PageData;
import zy.entity.vip.rate.T_Vip_Brand_Rate;
import zy.entity.vip.rate.T_Vip_Type_Rate;

public interface MemberRateService {
	PageData<T_Vip_Brand_Rate> page(Map<String,Object> param);
	PageData<T_Vip_Type_Rate> pageType(Map<String,Object> param);
	void saveBrand(Map<String,Object> param);
	void saveType(Map<String,Object> param);
	void delType(Map<String,Object> param);
	void delBrand(Map<String,Object> param);
	T_Vip_Brand_Rate brandByID(Integer tr_id);
	T_Vip_Type_Rate typeByID(Integer tr_id);
	
	void updateBrand(Map<String,Object> param);
	void updateType(Map<String,Object> param);
}
