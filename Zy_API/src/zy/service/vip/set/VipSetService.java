package zy.service.vip.set;

import java.util.List;
import java.util.Map;

import zy.entity.vip.set.T_Vip_AgeGroupSetUp;
import zy.entity.vip.set.T_Vip_BirthdaySms;
import zy.entity.vip.set.T_Vip_Grade;
import zy.entity.vip.set.T_Vip_ReturnSetUp;
import zy.entity.vip.set.T_Vip_Setup;

public interface VipSetService {
	T_Vip_BirthdaySms loadBirthdaySms(Map<String,Object> params);
	List<T_Vip_ReturnSetUp> loadReturnSetUp(Map<String,Object> params);
	List<T_Vip_AgeGroupSetUp> loadAgeGroupSetUp(Map<String,Object> params);
	T_Vip_AgeGroupSetUp loadAgeGroupSetUp(Integer ags_id);
	List<T_Vip_ReturnSetUp> loadReturnSetUp(String shop_code,Integer companyid);
	List<T_Vip_Grade> loadGrade(Map<String,Object> params);
	T_Vip_Setup loadSetUp(Map<String,Object> params);
	void update(Map<String,Object> params);
	void delReturnSet(Map<String,Object> params);
	void delAgeGroupSet(Map<String,Object> params);
	void saveReturnSet(Map<String,Object> params);
	void saveAgeGroupSet(Map<String,Object> params);
	void updateAgeGroupSet(Map<String,Object> params);
}
