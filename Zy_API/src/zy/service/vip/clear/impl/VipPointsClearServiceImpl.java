package zy.service.vip.clear.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.vip.clear.VipPointsClearDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.vip.member.T_Vip_Member;
import zy.service.vip.clear.VipPointsClearService;
import zy.util.CommonUtil;

@Service
public class VipPointsClearServiceImpl implements VipPointsClearService {

	@Resource
	private VipPointsClearDAO vipPointsClearDAO;
	@Override
	public PageData<T_Vip_Member> page(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		Object pageSize = params.get(CommonUtil.PAGESIZE);
		Object pageIndex = params.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = vipPointsClearDAO.count(params);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		params.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		params.put(CommonUtil.END, _pageSize);
		
		List<T_Vip_Member> list = vipPointsClearDAO.list(params);
		PageData<T_Vip_Member> pageData = new PageData<T_Vip_Member>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		pageData.setData(vipPointsClearDAO.sum(params));
		return pageData;
	}
	
	@Override
	public void clear_points(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		List<T_Vip_Member> memberList = vipPointsClearDAO.getMemberByIds(params);
		if(memberList != null && memberList.size()>0){
			params.put("memberList", memberList);
			vipPointsClearDAO.clear_points(params);
		}
	}
}
