package zy.service.vip.clear;

import java.util.Map;

import zy.entity.PageData;
import zy.entity.vip.member.T_Vip_Member;

public interface VipPointsClearService {
	PageData<T_Vip_Member> page(Map<String,Object> params);
	void clear_points(Map<String,Object> params);
}
