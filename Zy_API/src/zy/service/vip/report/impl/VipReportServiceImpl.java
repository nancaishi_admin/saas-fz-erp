package zy.service.vip.report.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.vip.report.VipReportDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.vip.member.T_Vip_ActivevipAnalysis;
import zy.entity.vip.member.T_Vip_BrandAnalysis;
import zy.entity.vip.member.T_Vip_ConsumeDetailList;
import zy.entity.vip.member.T_Vip_ConsumeMonthCompare;
import zy.entity.vip.member.T_Vip_Member;
import zy.entity.vip.member.T_Vip_NoShopAnalysis;
import zy.entity.vip.member.T_Vip_QuotaAnalysis;
import zy.entity.vip.member.T_Vip_TypeAnalysis;
import zy.entity.vip.set.T_Vip_AgeGroupSetUp;
import zy.service.vip.report.VipReportService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;
import zy.vo.vip.VipVO;

@Service
public class VipReportServiceImpl implements VipReportService{
	
	@Resource
	private VipReportDAO vipReportDAO;
	@Override
	public PageData<T_Vip_Member> point_list(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		Object pageSize = params.get(CommonUtil.PAGESIZE);
		Object pageIndex = params.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = vipReportDAO.point_count(params);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		params.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		params.put(CommonUtil.END, _pageSize);
		
		List<T_Vip_Member> list = vipReportDAO.point_list(params);
		PageData<T_Vip_Member> pageData = new PageData<T_Vip_Member>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	
	@Override
	public PageData<T_Vip_TypeAnalysis> type_analysis_list(
			Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		List<T_Vip_TypeAnalysis> list = vipReportDAO.type_analysis_list(params);
		Map<String, Object> sumMap = new HashMap<String, Object>();
		Integer vip_count_all = 0;
		Integer consumer_count_all = 0;
		Double rebate_monery_sum_all = 0.0d;
		Double cost_monery_sum_all = 0.0d;
		Double profits_sum_all = 0.0d;
		if(list != null && list.size()>0){
			for(int i=0;i<list.size();i++){
				vip_count_all += list.get(i).getVip_count();
				consumer_count_all += list.get(i).getConsumer_count();
				rebate_monery_sum_all += list.get(i).getRebate_monery_sum();
				cost_monery_sum_all += list.get(i).getCost_monery_sum();
				profits_sum_all += list.get(i).getProfits_sum();
			}
			
			for(int i=0;i<list.size();i++){
				list.get(i).setVip_count_rate((double)list.get(i).getVip_count()/vip_count_all);
				list.get(i).setConsumer_vip_rate((double)list.get(i).getConsumer_count()/consumer_count_all);
				list.get(i).setMonery_rate((double)list.get(i).getRebate_monery_sum()/rebate_monery_sum_all);
				list.get(i).setProfits_rate((double)list.get(i).getProfits_sum()/profits_sum_all);
			}
		}
		sumMap.put("vip_count", vip_count_all);
		sumMap.put("consumer_count", consumer_count_all);
		sumMap.put("rebate_monery_sum", rebate_monery_sum_all);
		sumMap.put("cost_monery_sum", cost_monery_sum_all);
		sumMap.put("profits_sum", profits_sum_all);
		
		
		
		PageData<T_Vip_TypeAnalysis> pageData = new PageData<T_Vip_TypeAnalysis>();
		pageData.setList(list);
		pageData.setData(sumMap);
		return pageData;
	}

	@Override
	public PageData<T_Vip_ConsumeDetailList> consume_detail_list(
			Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		Object pageSize = params.get(CommonUtil.PAGESIZE);
		Object pageIndex = params.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = vipReportDAO.consume_detail_count(params);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		params.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		params.put(CommonUtil.END, _pageSize);
		
		List<T_Vip_ConsumeDetailList> list = vipReportDAO.consume_detail_list(params);
		PageData<T_Vip_ConsumeDetailList> pageData = new PageData<T_Vip_ConsumeDetailList>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		pageData.setData(vipReportDAO.consume_detail_sum(params));
		return pageData;
	}

	@Override
	public PageData<T_Vip_ActivevipAnalysis> activevip_analysis_list(
			Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		Object pageSize = params.get(CommonUtil.PAGESIZE);
		Object pageIndex = params.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = vipReportDAO.activevip_analysis_count(params);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		params.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		params.put(CommonUtil.END, _pageSize);
		
		List<T_Vip_ActivevipAnalysis> list = vipReportDAO.activevip_analysis_list(params);
		PageData<T_Vip_ActivevipAnalysis> pageData = new PageData<T_Vip_ActivevipAnalysis>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		pageData.setData(vipReportDAO.activevip_analysis_sum(params));
		return pageData;
	}

	@Override
	public PageData<T_Vip_NoShopAnalysis> noshopvip_analysis_list(
			Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		Object pageSize = params.get(CommonUtil.PAGESIZE);
		Object pageIndex = params.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = vipReportDAO.noshopvip_analysis_count(params);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		params.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		params.put(CommonUtil.END, _pageSize);
		
		List<T_Vip_NoShopAnalysis> list = vipReportDAO.noshopvip_analysis_list(params);
		PageData<T_Vip_NoShopAnalysis> pageData = new PageData<T_Vip_NoShopAnalysis>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		pageData.setData(vipReportDAO.noshopvip_analysis_sum(params));
		return pageData;
	}

	@Override
	public Map<String, Object> member_lose_data(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer vs_loss_day = (Integer)params.get("vs_loss_day");
		if (StringUtil.isEmpty(vs_loss_day) ){//默认180天 
			vs_loss_day = 180;
		}
		
		Calendar cl = Calendar.getInstance();
		cl.add(Calendar.DATE, -vs_loss_day);
		String loss_date = DateUtil.getYearMonthDate(cl.getTime());//获取流失会员天水对应日期
		String now_date = DateUtil.getYearMonthDate();
		params.put("loss_date", loss_date);
		params.put("now_date", now_date);
		return vipReportDAO.member_lose_data(params);
	}

	@Override
	public PageData<T_Vip_QuotaAnalysis> vipquota_analysis_list(
			Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		Object pageSize = params.get(CommonUtil.PAGESIZE);
		Object pageIndex = params.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		
		Integer vs_loss_day = (Integer)params.get("vs_loss_day");
		if (StringUtil.isEmpty(vs_loss_day) ){//默认180天 
			vs_loss_day = 180;
		}
		Calendar cl = Calendar.getInstance();
		cl.add(Calendar.DATE, -vs_loss_day);
		String loss_date = DateUtil.getYearMonthDate(cl.getTime());//获取流失会员天水对应日期
		String now_date = DateUtil.getYearMonthDate();
		params.put("loss_date", loss_date);
		params.put("now_date", now_date);
		
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = vipReportDAO.vipquota_analysis_count(params);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		params.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		params.put(CommonUtil.END, _pageSize);
		
		List<T_Vip_QuotaAnalysis> list = vipReportDAO.vipquota_analysis_list(params);
		PageData<T_Vip_QuotaAnalysis> pageData = new PageData<T_Vip_QuotaAnalysis>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}

	@Override
	public PageData<T_Vip_BrandAnalysis> brand_analysis_list(
			Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		List<T_Vip_BrandAnalysis> list = vipReportDAO.brand_analysis_list(params);
		Map<String, Object> sumMap = new HashMap<String, Object>();
		Integer count_sum_all = 0;
		Double money_sum_all = 0.0d;
		Double cost_monery_sum_all = 0.0d;
		Double profits_sum_all = 0.0d;
		if(list != null && list.size()>0){
			for(int i=0;i<list.size();i++){
				count_sum_all += list.get(i).getCount_sum();
				money_sum_all += list.get(i).getMoney_sum();
				cost_monery_sum_all += list.get(i).getCost_monery_sum();
				profits_sum_all += list.get(i).getProfits_sum();
			}
			
			for(int i=0;i<list.size();i++){
				list.get(i).setCount_sum_rate((double)list.get(i).getCount_sum()/count_sum_all);
				list.get(i).setMoney_sum_rate((double)list.get(i).getMoney_sum()/money_sum_all);
				list.get(i).setProfits_sum_rate((double)list.get(i).getProfits_sum()/profits_sum_all);
			}
		}
		sumMap.put("count_sum", count_sum_all);
		sumMap.put("money_sum", money_sum_all);
		sumMap.put("cost_monery_sum", cost_monery_sum_all);
		sumMap.put("profits_sum", profits_sum_all);
		
		PageData<T_Vip_BrandAnalysis> pageData = new PageData<T_Vip_BrandAnalysis>();
		pageData.setList(list);
		pageData.setData(sumMap);
		return pageData;
	}

	@Override
	public PageData<Map<String, Object>> vipconsume_month_compare(
			Map<String, Object> params) {
		List<T_Vip_ConsumeMonthCompare> monthCompareList = null;
		List<T_Vip_ConsumeMonthCompare> monthCompareLastList = null;
		Double sum_money = 0.0d;//本期累计销售
		Double sum_last_money = 0.0d;//同期累计销售
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		String vm_year = (String)params.get("vm_year");
		if(StringUtil.isEmpty(vm_year)){
			vm_year = DateUtil.getYear()+"";
		}
		int year = Integer.parseInt(vm_year);
		List<String> monthsList = DateUtil.get12MonthsInCurrentYear(year);
		monthCompareList = vipReportDAO.vipconsumeMonthCompareList(params);
		
		year = year-1;
		params.put("vm_year", year);
		List<String> lastMonthsList = DateUtil.get12MonthsInCurrentYear(year);
		monthCompareLastList = vipReportDAO.vipconsumeMonthCompareList(params);
		
		for(int i=0;i<monthCompareList.size();i++){
			//本期累计销售额
			sum_money += monthCompareList.get(i).getSh_money();
		}
		for(int i=0;i<monthCompareLastList.size();i++){
			//同期累计销售额
			sum_last_money += monthCompareLastList.get(i).getSh_money();
		}
		
		List<Map<String, Object>> values = new ArrayList<Map<String, Object>>();
		HashMap<String, Object> entry = null;
		for(int i=0; i<monthsList.size(); i++){
			String month = monthsList.get(i);//本期月份
			String lastmonth = lastMonthsList.get(i);//同期月份
			
			//同期
			Double vm_last_money = 0.0d;
			Double vm_last_money_rate = 0.0d;
			entry = new HashMap<String, Object>();
			for(int j=0;j<monthCompareLastList.size();j++){
				if(lastmonth.equals(monthCompareLastList.get(j).getVm_month())){
					vm_last_money = monthCompareLastList.get(j).getSh_money();
					break;
				}
			}
			if(vm_last_money>0 && sum_last_money>0){
				vm_last_money_rate = vm_last_money/sum_last_money;
			}
			entry.put("vm_month", i+1);
    		entry.put("vm_item", "同期消费");
    		entry.put("vm_money", StringUtil.doubleFixed(vm_last_money, 0));
    		entry.put("vm_rate", StringUtil.doubleFixed(vm_last_money_rate*100, 0)+"%");
    		values.add(entry);
    		
			//本期
    		Double vm_this_money = 0.0d;
    		Double vm_this_money_rate = 0.0d;
			entry = new HashMap<String, Object>();
			for(int j=0;j<monthCompareList.size();j++){
				if(month.equals(monthCompareList.get(j).getVm_month())){
					vm_this_money = monthCompareList.get(j).getSh_money();
					break;
				}
			}
			if(vm_this_money>0 && sum_money>0 ){
				vm_this_money_rate = vm_this_money/sum_money;
			}
			entry.put("vm_month", i+1);
    		entry.put("vm_item", "本期消费");
    		entry.put("vm_money", StringUtil.doubleFixed(vm_this_money, 0));
    		entry.put("vm_rate", StringUtil.doubleFixed(vm_this_money_rate*100, 0)+"%");
    		values.add(entry);
    		
    		//趋势
    		entry = new HashMap<String, Object>();
    		entry.put("vm_month", i+1);
    		entry.put("vm_item", "趋势");
    		entry.put("vm_money", vm_last_money == vm_this_money ? "→" : (vm_this_money > vm_last_money ? "↑" : "↓"));
    		entry.put("vm_rate", vm_last_money_rate == vm_this_money_rate ? "→" : (vm_this_money_rate > vm_last_money_rate ? "↑" : "↓"));
    		values.add(entry);
		}
		//总计
		HashMap<String, Object> stats = new HashMap<String, Object>();
		stats.put("vm_month", "总计");
        stats.put("vm_item", "同期消费"+"<br/>"+"本期消费");
        stats.put("vm_money", StringUtil.doubleFixed(sum_last_money, 0)+"<br/>"+
        		              StringUtil.doubleFixed(sum_money, 0)+"<br/>" );
        PageData<Map<String, Object>> pageData = new PageData<Map<String, Object>>();
		pageData.setList(values);
		pageData.setData(stats);
		return pageData;
	}

	@Override
	public String vipconsume_month_compare_column(Map<String, Object> params) {
		List<T_Vip_ConsumeMonthCompare> monthCompareList = null;
		List<T_Vip_ConsumeMonthCompare> monthCompareLastList = null;
		Double sum_money = 0.0d;//本期累计销售
		Double sum_last_money = 0.0d;//同期累计销售
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		String vm_year = (String)params.get("vm_year");
		if(StringUtil.isEmpty(vm_year)){
			vm_year = DateUtil.getYear()+"";
		}
		int year = Integer.parseInt(vm_year);
		List<String> monthsList = DateUtil.get12MonthsInCurrentYear(year);
		monthCompareList = vipReportDAO.vipconsumeMonthCompareList(params);
		
		year = year-1;
		params.put("vm_year", year);
		List<String> lastMonthsList = DateUtil.get12MonthsInCurrentYear(year);
		monthCompareLastList = vipReportDAO.vipconsumeMonthCompareList(params);
		
		List<Double> list = null;
        Map<String, List<Double>> listMap = new HashMap<String, List<Double>>();;
        
		for(int i=0; i<monthsList.size(); i++){
			String month = monthsList.get(i);//本期月份
			String lastmonth = lastMonthsList.get(i);//同期月份
			
			list = new ArrayList<Double>();
			
			//同期
			Double vm_last_money = 0.0d;
			for(int j=0;j<monthCompareLastList.size();j++){
				if(lastmonth.equals(monthCompareLastList.get(j).getVm_month())){
					vm_last_money = monthCompareLastList.get(j).getSh_money();
					break;
				}
			}
    		
			//本期
    		Double vm_this_money = 0.0d;
			for(int j=0;j<monthCompareList.size();j++){
				if(month.equals(monthCompareList.get(j).getVm_month())){
					vm_this_money = monthCompareList.get(j).getSh_money();
					break;
				}
			}
			list.add(vm_last_money);
			list.add(vm_this_money);
			listMap.put( i+1+"", list);
		}
		return VipVO.getMonthCompareColumnVO(listMap);
	}

	@Override
	public String vipage_analysis_column(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		List<T_Vip_AgeGroupSetUp> age_group_list = null;
		age_group_list = vipReportDAO.vipage_analysis_column(params);
		return VipVO.getVipageAnalysisColumnVO(age_group_list);
	}

	@Override
	public PageData<T_Vip_Member> vipage_analysis_list(
			Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		Object pageSize = params.get(CommonUtil.PAGESIZE);
		Object pageIndex = params.get(CommonUtil.PAGEINDEX);
		Object ags_beg_age = params.get("ags_beg_age");
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		String monthBegDate = DateUtil.getYearMonthDate(DateUtil.getMonthBeg());
		String monthEndDate = DateUtil.getYearMonthDate(DateUtil.getMonthEnd());
    	params.put("monthBegDate", monthBegDate);
    	params.put("monthEndDate", monthEndDate);
		List<T_Vip_AgeGroupSetUp> age_group_list = vipReportDAO.vipage_analysis_sell_column(params);
		if(age_group_list == null || age_group_list.size()<=0){
			throw new IllegalArgumentException("会员年龄段为空，请设置年龄段!");
		}
		if(StringUtil.isEmpty(ags_beg_age)){
			params.put("ags_beg_age", age_group_list.get(0).getAgs_beg_age());
			params.put("ags_end_age", age_group_list.get(0).getAgs_end_age());
		}
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = vipReportDAO.vipage_analysis_count(params);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		params.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		params.put(CommonUtil.END, _pageSize);
    	
		List<T_Vip_Member> list = vipReportDAO.vipage_analysis_list(params);
		PageData<T_Vip_Member> pageData = new PageData<T_Vip_Member>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		pageData.setData(vipReportDAO.vipage_analysis_sum(params));
		pageData.setHeadData(VipVO.getVipageAnalysisListVO(age_group_list));
		return pageData;
	}
	
}
