package zy.service.approve.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.approve.ApproveRecordDAO;
import zy.entity.approve.T_Approve_Record;
import zy.service.approve.ApproveRecordService;

@Service
public class ApproveRecordServiceImpl implements ApproveRecordService{
	@Resource
	private ApproveRecordDAO approveRecordDAO;

	@Override
	public List<T_Approve_Record> list(String number, String ar_type, Integer companyid) {
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return approveRecordDAO.list(number, ar_type, companyid);
	}
	
	
	
}
