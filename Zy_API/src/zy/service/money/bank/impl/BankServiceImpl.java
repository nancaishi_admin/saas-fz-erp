package zy.service.money.bank.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import zy.dao.money.bank.BankDAO;
import zy.dao.money.bank.BankRunDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.money.bank.T_Money_Bank;
import zy.entity.money.bank.T_Money_BankRun;
import zy.service.money.bank.BankService;
import zy.util.CommonUtil;

@Service
public class BankServiceImpl implements BankService{
	
	@Resource
	private BankDAO bankDAO;
	@Resource
	private BankRunDAO bankRunDAO;
	
	@Override
	public List<T_Money_Bank> list(Map<String, Object> param) {
		Assert.notNull(param.get(CommonUtil.COMPANYID),"连接超时，请重新登录!");
		return bankDAO.list(param);
	}

	@Override
	public List<T_Money_Bank> listShop(Map<String, Object> param) {
		return bankDAO.listShop(param);
	}

	@Override
	public PageData<T_Money_BankRun> pageRun(Map<String, Object> params) {
		Assert.notNull(params.get(CommonUtil.COMPANYID),"连接超时，请重新登录!");
		Integer _pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer _pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		
		Integer totalCount = bankRunDAO.count(params);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		params.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		params.put(CommonUtil.END, _pageSize);
		
		List<T_Money_BankRun> list = bankRunDAO.list(params);
		PageData<T_Money_BankRun> pageData = new PageData<T_Money_BankRun>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	
	@Override
	public T_Money_Bank queryByID(Integer ba_id) {
		return bankDAO.queryByID(ba_id);
	}

	@Override
	public void save(T_Money_Bank bank) {
		bankDAO.save(bank);
	}

	@Override
	public void update(T_Money_Bank bank) {
		if (bank == null || bank.getBa_id() == null) {
			throw new IllegalArgumentException("参数ba_id不能为null");
		}
		bankDAO.update(bank);
	}

	@Override
	public void del(Integer ba_id) {
		bankDAO.del(ba_id);
	}
	
	@Override
	public List<T_Money_BankRun> report_sum(Map<String, Object> params) {
		Assert.notNull(params.get(CommonUtil.COMPANYID),"连接超时，请重新登录!");
		return bankRunDAO.report_sum(params);
	}

}
