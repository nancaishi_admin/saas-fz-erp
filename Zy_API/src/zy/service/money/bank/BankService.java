package zy.service.money.bank;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.money.bank.T_Money_Bank;
import zy.entity.money.bank.T_Money_BankRun;

public interface BankService {
	List<T_Money_Bank> list(Map<String,Object> param);
	PageData<T_Money_BankRun> pageRun(Map<String,Object> params);
	T_Money_Bank queryByID(Integer ba_id);
	void save(T_Money_Bank bank);
	void update(T_Money_Bank bank);
	void del(Integer ba_id);
	List<T_Money_BankRun> report_sum(Map<String, Object> params);
	
	//前台
	List<T_Money_Bank> listShop(Map<String,Object> param);
}
