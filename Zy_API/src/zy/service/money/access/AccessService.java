package zy.service.money.access;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.approve.T_Approve_Record;
import zy.entity.money.access.T_Money_Access;
import zy.entity.money.access.T_Money_AccessList;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.entity.sys.user.T_Sys_User;

public interface AccessService {
	PageData<T_Money_Access> page(Map<String, Object> params);
	T_Money_Access load(Integer ac_id);
	T_Money_Access load(String number,Integer companyid);
	List<T_Money_AccessList> temp_list(Map<String, Object> params);
	void temp_save(List<T_Money_AccessList> temps,T_Sys_User user);
	void temp_save(List<T_Money_AccessList> temps, T_Sell_Cashier cashier);
	void temp_updateMoney(T_Money_AccessList temp);
	void temp_updateType(T_Money_AccessList temp);
	void temp_updateRemark(T_Money_AccessList temp);
	void temp_del(Integer acl_id);
	void temp_clear(Map<String, Object> params);
	List<T_Money_AccessList> detail_list(Map<String, Object> params);
	void save(T_Money_Access access, T_Sys_User user);
	void save(T_Money_Access access, T_Sell_Cashier cashier);
	void update(T_Money_Access access, T_Sys_User user);
	void update(T_Money_Access access, T_Sell_Cashier cashier);
	T_Money_Access approve(String number, T_Approve_Record record, T_Sys_User user);
	T_Money_Access reverse(String number, T_Sys_User user);
	void initUpdate(String number, Integer us_id, Integer companyid);
	void initUpdate_Sell(String number, Integer ca_id, Integer companyid);
	void del(String number, Integer companyid);
	Map<String, Object> loadPrintData(String number, Integer sp_id, T_Sys_User user);
}
