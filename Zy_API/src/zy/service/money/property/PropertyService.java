package zy.service.money.property;

import java.util.List;
import java.util.Map;

import zy.entity.money.property.T_Money_Property;

public interface PropertyService {
	List<T_Money_Property> list(Map<String,Object> param);
	T_Money_Property queryByID(Integer pp_id);
	void save(T_Money_Property property);
	void update(T_Money_Property property);
	void del(Integer pp_id);
}
