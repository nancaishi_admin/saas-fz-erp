package zy.service.money.stream;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.approve.T_Approve_Record;
import zy.entity.base.stream.T_Base_Stream;
import zy.entity.base.stream.T_Base_Stream_Bill;
import zy.entity.money.stream.T_Money_StreamSettle;
import zy.entity.money.stream.T_Money_StreamSettleList;
import zy.entity.sys.user.T_Sys_User;

public interface StreamSettleService {
	PageData<T_Money_StreamSettle> page(Map<String, Object> params);
	PageData<T_Base_Stream_Bill> pageBill(Map<String, Object> params);
	T_Money_StreamSettle load(Integer st_id);
	T_Money_StreamSettle load(String number, Integer companyid);
	T_Base_Stream loadStream(String se_code,Integer companyid);
	List<T_Money_StreamSettleList> temp_list(Map<String, Object> params);
	void temp_save(String sp_code,T_Sys_User user);
	void temp_updateDiscountMoney(T_Money_StreamSettleList temp);
	void temp_updateRealMoney(T_Money_StreamSettleList temp);
	void temp_updateRemark(T_Money_StreamSettleList temp);
	void temp_entire(Map<String, Object> params);
	List<T_Money_StreamSettleList> detail_list(String number,Integer companyid);
	void save(T_Money_StreamSettle settle, T_Sys_User user);
	void update(T_Money_StreamSettle settle, T_Sys_User user);
	T_Money_StreamSettle approve(String number, T_Approve_Record record, T_Sys_User user);
	T_Money_StreamSettle reverse(String number, T_Sys_User user);
	void initUpdate(String number, Integer us_id, Integer companyid);
	void del(String number, Integer companyid);
}
