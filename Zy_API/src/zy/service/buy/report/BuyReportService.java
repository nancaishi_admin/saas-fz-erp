package zy.service.buy.report;

import java.util.List;
import java.util.Map;

import zy.dto.buy.enter.EnterDetailReportDto;
import zy.dto.buy.enter.EnterDetailsDto;
import zy.dto.buy.enter.EnterRankDto;
import zy.dto.buy.enter.EnterReportCsbDto;
import zy.dto.buy.enter.EnterReportDto;
import zy.entity.PageData;

public interface BuyReportService {
	PageData<EnterReportDto> pageEnterReport(Map<String, Object> params);
	List<EnterReportCsbDto> listEnterReport_csb(Map<String, Object> params);
	PageData<EnterDetailReportDto> pageEnterDetailReport(Map<String, Object> params);
	Map<String, Object> loadEnterDetailSizeReport(Map<String, Object> params);
	PageData<EnterDetailsDto> pageEnterDetails(Map<String, Object> params);
	PageData<EnterRankDto> pageEnterRank(Map<String, Object> params);
	List<Map<String, Object>> type_level_enter(Map<String, Object> params);
}
