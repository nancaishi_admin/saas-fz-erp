package zy.service.buy.dealings;

import java.util.Map;

import zy.entity.PageData;
import zy.entity.buy.dealings.T_Buy_Dealings;

public interface BuyDealingsService {
	PageData<T_Buy_Dealings> page(Map<String, Object> params);
}
