package zy.service.buy.supply.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.buy.supply.SupplyDAO;
import zy.dto.buy.money.SupplyMoneyDetailsDto;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.buy.supply.T_Buy_Supply;
import zy.service.buy.supply.SupplyService;
import zy.util.CommonUtil;

@Service
public class SupplyServiceImpl implements SupplyService{
	@Resource
	private SupplyDAO supplyDAO;
	
	@Override
	public List<T_Buy_Supply> list(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return supplyDAO.list(param);
	}

	@Override
	public void save(T_Buy_Supply supply) {
		supplyDAO.save(supply);
	}

	@Override
	public void del(Integer sp_id, String sp_code,Integer companyid) {
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		if (sp_code == null || "".equals(sp_code)) {
			throw new IllegalArgumentException("参数sp_code不能为null");
		}
		supplyDAO.del(sp_id,sp_code,companyid);
	}

	@Override
	public T_Buy_Supply queryByID(Integer sp_id) {
		return supplyDAO.queryByID(sp_id);
	}

	@Override
	public void update(T_Buy_Supply supply) {
		supplyDAO.update(supply);
	}
	
	@Override
	public PageData<SupplyMoneyDetailsDto> pageMoneyDetails(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		
		Map<String, Object> sumMap = supplyDAO.countsumMoneyDetails(params);
		int totalCount = Integer.parseInt(sumMap.get("totalCount").toString());
		PageInfo pageInfo = new PageInfo(totalCount, pageSize, pageIndex);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		
		List<SupplyMoneyDetailsDto> list = supplyDAO.listMoneyDetails(params);
		PageData<SupplyMoneyDetailsDto> pageData = new PageData<SupplyMoneyDetailsDto>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		pageData.setData(sumMap);
		return pageData;
	}
	
}
