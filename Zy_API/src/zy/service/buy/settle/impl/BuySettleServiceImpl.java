package zy.service.buy.settle.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.approve.ApproveRecordDAO;
import zy.dao.buy.dealings.BuyDealingsDAO;
import zy.dao.buy.prepay.BuyPrepayDAO;
import zy.dao.buy.settle.BuySettleDAO;
import zy.dao.buy.supply.SupplyDAO;
import zy.dao.money.bank.BankDAO;
import zy.dao.money.bank.BankRunDAO;
import zy.dao.sys.print.PrintDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.approve.T_Approve_Record;
import zy.entity.buy.dealings.T_Buy_Dealings;
import zy.entity.buy.enter.T_Buy_Enter;
import zy.entity.buy.fee.T_Buy_Fee;
import zy.entity.buy.prepay.T_Buy_Prepay;
import zy.entity.buy.settle.T_Buy_Settle;
import zy.entity.buy.settle.T_Buy_SettleList;
import zy.entity.buy.supply.T_Buy_Supply;
import zy.entity.money.bank.T_Money_Bank;
import zy.entity.money.bank.T_Money_BankRun;
import zy.entity.sys.user.T_Sys_User;
import zy.service.buy.settle.BuySettleService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;

@Service
public class BuySettleServiceImpl implements BuySettleService{
	@Resource
	private BuySettleDAO buySettleDAO;
	@Resource
	private ApproveRecordDAO approveRecordDAO;
	@Resource
	private BuyDealingsDAO buyDealingsDAO;
	@Resource
	private BuyPrepayDAO buyPrepayDAO;
	@Resource
	private BankDAO bankDAO;
	@Resource
	private BankRunDAO bankRunDAO;
	@Resource
	private SupplyDAO supplyDAO;
	@Resource
	private PrintDAO printDAO;
	
	@Override
	public PageData<T_Buy_Settle> page(Map<String, Object> params) {
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		
		Integer totalCount = buySettleDAO.count(params);
		PageInfo pageInfo = new PageInfo(totalCount, pageSize, pageIndex);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		
		List<T_Buy_Settle> list = buySettleDAO.list(params);
		PageData<T_Buy_Settle> pageData = new PageData<T_Buy_Settle>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	
	@Override
	public T_Buy_Settle load(Integer st_id) {
		T_Buy_Settle settle = buySettleDAO.load(st_id);
		if(settle != null){
			T_Approve_Record approve_Record = approveRecordDAO.load(settle.getSt_number(), settle.getCompanyid());
			if(approve_Record != null){
				settle.setAr_describe(approve_Record.getAr_describe());
			}
		}
		return settle;
	}
	
	@Override
	public T_Buy_Settle load(String number, Integer companyid) {
		return buySettleDAO.load(number, companyid);
	}
	
	@Override
	public T_Buy_Supply loadSupply(String sp_code,Integer companyid) {
		return supplyDAO.loadSupply(sp_code, companyid);
	}
	
	@Override
	public List<T_Buy_SettleList> temp_list(Map<String, Object> params) {
		return buySettleDAO.temp_list(params);
	}

	@Override
	@Transactional
	public void temp_save(String sp_code, T_Sys_User user) {
		if (StringUtil.isEmpty(sp_code)) {
			throw new RuntimeException("请选择供货厂商");
		}
		T_Buy_Settle existSettle = buySettleDAO.check_settle(sp_code, user.getCompanyid());
		if(existSettle != null){
			throw new RuntimeException("该供货厂商有单据未审核，不能再结算!");
		}
		buySettleDAO.temp_clear(user.getUs_id(), user.getCompanyid());
		List<T_Buy_SettleList> temps = buySettleDAO.load_buy_forsavetemp(sp_code, user.getCompanyid());
		for (T_Buy_SettleList temp : temps) {
			temp.setStl_join(1);
			temp.setStl_us_id(user.getUs_id());
			temp.setCompanyid(user.getCompanyid());
		}
		buySettleDAO.temp_save(temps);
	}
	
	@Override
	@Transactional
	public void temp_updateDiscountMoney(T_Buy_SettleList temp) {
		buySettleDAO.temp_updateDiscountMoney(temp);
	}

	@Override
	@Transactional
	public void temp_updatePrepay(T_Buy_SettleList temp) {
		buySettleDAO.temp_updatePrepay(temp);
	}

	@Override
	@Transactional
	public void temp_updateRealMoney(T_Buy_SettleList temp) {
		buySettleDAO.temp_updateRealMoney(temp);
	}

	@Override
	@Transactional
	public void temp_updateRemark(T_Buy_SettleList temp) {
		buySettleDAO.temp_updateRemark(temp);
	}

	@Override
	@Transactional
	public void temp_updateJoin(String ids, String stl_join) {
		buySettleDAO.temp_updateJoin(ids, stl_join);
	}
	
	@Override
	@Transactional
	public Map<String, Object> temp_entire(Map<String, Object> params) {
		Double discount_money = (Double)params.get("discount_money");
		Double prepay = (Double)params.get("prepay");
		Double paid = (Double)params.get("paid");
		T_Sys_User user = (T_Sys_User)params.get("user");
		if(discount_money == null || prepay == null || paid == null){
			throw new RuntimeException("整单结算金额不能为空");
		}
		if (discount_money.doubleValue() == 0d && prepay.doubleValue() == 0d
				&& paid.doubleValue() == 0d) {
			throw new RuntimeException("整单结算金额不能为0");
		}
		params.put("stl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		List<T_Buy_SettleList> temps = buySettleDAO.temp_list(params);
		
		double remained_discount_money = discount_money;
		double remained_prepay = prepay;
		double remained_paid = paid;
		for (T_Buy_SettleList temp : temps) {
			if (CommonUtil.BUYDEALINGS_TYPE_TH.equals(temp.getStl_type())//退货单
					|| (CommonUtil.BUYDEALINGS_TYPE_QC.equals(temp.getStl_type()) && temp.getStl_unpayable() < 0)//负的期初单
					|| (CommonUtil.BUYDEALINGS_TYPE_FY.equals(temp.getStl_type()) && temp.getStl_unpayable() < 0)) {//负的费用单
				double tempMoney = temp.getStl_unpayable();
				temp.setStl_discount_money(0d);
				temp.setStl_prepay(0d);
				temp.setStl_real_pay(tempMoney);
				temp.setStl_join(1);
				remained_paid += Math.abs(tempMoney);
				continue;
			}
			
			Double temp_discount_money = null;
			Double temp_prepay = null;
			Double temp_paid = null;
			
			// 使用优惠金额
			if (remained_discount_money > 0) {
				temp_discount_money = Math.min(remained_discount_money,temp.getStl_unpayable());
				remained_discount_money -= temp_discount_money;
			}else{
				temp_discount_money = 0d;
			}
			temp.setStl_discount_money(temp_discount_money);//设置本次使用优惠金额
			//使用预付款金额
			if(remained_prepay > 0){
				temp_prepay = Math.min(remained_prepay, temp.getStl_unpayable()-temp.getStl_discount_money());
				remained_prepay -= temp_prepay;
			}else{
				temp_prepay = 0d;
			}
			temp.setStl_prepay(temp_prepay);//设置本次使用预收款金额
			
			//使用实付金额
			if(remained_paid > 0){
				temp_paid = Math.min(remained_paid, temp.getStl_unpayable()-temp.getStl_discount_money()-temp.getStl_prepay());
				remained_paid -= temp_paid;
			}else{
				temp_paid = 0d;
			}
			temp.setStl_real_pay(temp_paid);
			if (temp_discount_money != 0 || temp_prepay != 0 || temp_paid != 0) {
				temp.setStl_join(1);
			}else {
				if (temp.getStl_unpayable() == 0) {
					temp.setStl_join(1);
				}else {
					temp.setStl_join(0);
				}
			}
		}
		buySettleDAO.temp_update(temps);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("st_paidmore", remained_discount_money+remained_prepay+remained_paid);
		return resultMap;
	}
	
	@Override
	public List<T_Buy_SettleList> detail_list(String number,Integer companyid) {
		return buySettleDAO.detail_list(number,companyid);
	}
	
	@Override
	@Transactional
	public void save(T_Buy_Settle settle, T_Sys_User user) {
		if(settle == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		if(StringUtil.isEmpty(settle.getSt_supply_code())){
			throw new IllegalArgumentException("供货厂商不能为空");
		}
		if(StringUtil.isEmpty(settle.getSt_ba_code())){
			throw new IllegalArgumentException("银行账户不能为空");
		}
		if(StringUtil.isEmpty(settle.getSt_manager())){
			throw new IllegalArgumentException("经办人不能为空");
		}
		settle.setCompanyid(user.getCompanyid());
		settle.setSt_us_id(user.getUs_id());
		settle.setSt_maker(user.getUs_name());
		settle.setSt_ar_state(CommonUtil.AR_STATE_NOTAPPROVE);
		settle.setSt_sysdate(DateUtil.getCurrentTime());
		//1.查临时表
		List<T_Buy_SettleList> temps = buySettleDAO.temp_list_forsave(user.getUs_id(),user.getCompanyid());
		if(temps == null || temps.size() == 0){
			throw new RuntimeException("单据已保存，请勿重复提交");
		}
		//2.保存单据
		double st_discount_money = 0d;
		double st_prepay = 0d;
		double st_paid = 0d;
		for (T_Buy_SettleList temp : temps) {
			st_discount_money += temp.getStl_discount_money();
			st_prepay += temp.getStl_prepay();
			st_paid += temp.getStl_real_pay();
		}
		settle.setSt_discount_money(st_discount_money);
		settle.setSt_prepay(st_prepay);
		settle.setSt_paid(st_paid);
		if (settle.getSt_entire().intValue() == 1) {
			if (settle.getSt_paidmore() == null || settle.getSt_paidmore() < 0) {
				settle.setSt_paidmore(0d);
			}
		} else {
			settle.setSt_paidmore(0d);
		}
		buySettleDAO.save(settle, temps);
		//3.删除临时表
		buySettleDAO.temp_clear(user.getUs_id(), user.getCompanyid());
	}
	
	@Override
	@Transactional
	public void update(T_Buy_Settle settle, T_Sys_User user) {
		if(settle == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		if(StringUtil.isEmpty(settle.getSt_supply_code())){
			throw new IllegalArgumentException("供货厂商不能为空");
		}
		if(StringUtil.isEmpty(settle.getSt_ba_code())){
			throw new IllegalArgumentException("银行账户不能为空");
		}
		if(StringUtil.isEmpty(settle.getSt_manager())){
			throw new IllegalArgumentException("经办人不能为空");
		}
		
		settle.setCompanyid(user.getCompanyid());
		settle.setSt_us_id(user.getUs_id());
		settle.setSt_maker(user.getUs_name());
		settle.setSt_ar_state(CommonUtil.AR_STATE_NOTAPPROVE);
		settle.setSt_ar_date(null);
		settle.setSt_sysdate(DateUtil.getCurrentTime());
		//1.1查临时表
		List<T_Buy_SettleList> temps = buySettleDAO.temp_list_forsave(user.getUs_id(),user.getCompanyid());
		if(temps == null || temps.size() == 0){
			throw new RuntimeException("单据已保存，请勿重复提交");
		}
		//1.2验证单据
		T_Buy_Settle oldSettle = buySettleDAO.check(settle.getSt_number(), user.getCompanyid());
		if (oldSettle == null || !CommonUtil.AR_STATE_FAIL.equals(oldSettle.getSt_ar_state())) {
			throw new RuntimeException("单据已修改，请勿重复提交");
		}
		//2.保存单据
		double st_discount_money = 0d;
		double st_prepay = 0d;
		double st_paid = 0d;
		for (T_Buy_SettleList temp : temps) {
			st_discount_money += temp.getStl_discount_money();
			st_prepay += temp.getStl_prepay();
			st_paid += temp.getStl_real_pay();
		}
		settle.setSt_discount_money(st_discount_money);
		settle.setSt_prepay(st_prepay);
		settle.setSt_paid(st_paid);
		if (settle.getSt_entire().intValue() == 1) {
			if (settle.getSt_paidmore() == null || settle.getSt_paidmore() < 0) {
				settle.setSt_paidmore(0d);
			}
		} else {
			settle.setSt_paidmore(0d);
		}
		buySettleDAO.deleteList(settle.getSt_number(), user.getCompanyid());
		buySettleDAO.update(settle, temps);
		//3.删除临时表
		buySettleDAO.temp_clear(user.getUs_id(), user.getCompanyid());
	}
	
	@Override
	@Transactional
	public T_Buy_Settle approve(String number, T_Approve_Record record, T_Sys_User user) {
		if (number == null) {
			throw new IllegalArgumentException("参数number不能为null");
		}
		T_Buy_Settle settle = buySettleDAO.check(number, user.getCompanyid());
		if(settle == null){
			throw new RuntimeException("单据不存在");
		}
		if(!CommonUtil.AR_STATE_NOTAPPROVE.equals(settle.getSt_ar_state())){
			throw new RuntimeException("单据已经审核");
		}
		//更新单据审核状态
		settle.setSt_ar_state(record.getAr_state());
		settle.setSt_ar_date(DateUtil.getCurrentTime());
		buySettleDAO.updateApprove(settle);
		//保存审核记录表
		record.setAr_number(number);
		record.setAr_sysdate(DateUtil.getCurrentTime());
		record.setAr_us_name(user.getUs_name());
		record.setAr_type("t_buy_settle");
		record.setCompanyid(user.getCompanyid());
		approveRecordDAO.save(record);
		if(!CommonUtil.AR_STATE_APPROVED.equals(settle.getSt_ar_state())){//审核不通过，则直接返回
			return settle;
		}
		//3.审核通过
		//3.1更新供应商财务&生成往来明细账
		T_Buy_Supply supply = supplyDAO.loadSupply(settle.getSt_supply_code(), user.getCompanyid());
		supply.setSp_payable(supply.getSp_payable()-settle.getSt_discount_money());
		supply.setSp_payabled(supply.getSp_payabled()+settle.getSt_prepay()+settle.getSt_paid());
		supply.setSp_prepay(supply.getSp_prepay()-settle.getSt_prepay()+settle.getSt_paidmore());
		supplyDAO.updateSettle(supply);
		if (settle.getSt_paidmore() > 0) {//生成预付款单
			T_Buy_Prepay prepay = new T_Buy_Prepay();
			prepay.setPp_date(DateUtil.getCurrentTime());
			prepay.setPp_supply_code(settle.getSt_supply_code());
			prepay.setPp_type(0);
			prepay.setPp_money(settle.getSt_paidmore());
			prepay.setPp_maker(user.getUs_name());
			prepay.setPp_manager(settle.getSt_manager());
			prepay.setPp_ba_code(settle.getSt_ba_code());
			prepay.setPp_remark("供应商结算单["+number+"]整单结算剩余金额转入本单据。");
			prepay.setPp_ar_state(CommonUtil.AR_STATE_APPROVED);
			prepay.setPp_ar_date(DateUtil.getCurrentTime());
			prepay.setPp_sysdate(DateUtil.getCurrentTime());
			prepay.setPp_us_id(user.getUs_id());
			prepay.setCompanyid(user.getCompanyid());
			buyPrepayDAO.save(prepay);;
			settle.setSt_pp_number(prepay.getPp_number());
			buySettleDAO.updatePpNumber(settle);
		}
		T_Buy_Dealings dealings = new T_Buy_Dealings();
		dealings.setDl_supply_code(settle.getSt_supply_code());
		dealings.setDl_number(settle.getSt_number());
		dealings.setDl_type(CommonUtil.BUYDEALINGS_TYPE_JS);
		dealings.setDl_discount_money(settle.getSt_discount_money());
		dealings.setDl_payable(0d);
		dealings.setDl_payabled(settle.getSt_paid());
		dealings.setDl_debt(supply.getSp_payable()-supply.getSp_payabled()-supply.getSp_prepay()+settle.getSt_paidmore());
		dealings.setDl_date(DateUtil.getCurrentTime());
		dealings.setDl_manager(user.getUs_name());
		String dl_remark = settle.getSt_remark();
		if(settle.getSt_prepay() != 0){
			dl_remark = "(使用预收款金额："+String.format("%.2f", settle.getSt_prepay())+")";
		}
		dealings.setDl_remark(dl_remark);
		dealings.setDl_sysdate(DateUtil.getCurrentTime());
		dealings.setCompanyid(user.getCompanyid());
		buyDealingsDAO.save(dealings);
		if(StringUtil.isNotEmpty(settle.getSt_pp_number())){//生成预收款单明细账
			dealings = new T_Buy_Dealings();
			dealings.setDl_supply_code(settle.getSt_supply_code());
			dealings.setDl_number(settle.getSt_pp_number());
			dealings.setDl_type(CommonUtil.BUYDEALINGS_TYPE_YFK);
			dealings.setDl_discount_money(0d);
			dealings.setDl_payable(0d);
			dealings.setDl_payabled(settle.getSt_paidmore());
			dealings.setDl_debt(supply.getSp_payable()-supply.getSp_payabled()-supply.getSp_prepay());
			dealings.setDl_date(DateUtil.getCurrentTime());
			dealings.setDl_manager(user.getUs_name());
			dealings.setDl_remark("");
			dealings.setDl_sysdate(DateUtil.getCurrentTime());
			dealings.setCompanyid(user.getCompanyid());
			buyDealingsDAO.save(dealings);
		}
		//更改批发销售单金额数据
		List<T_Buy_Enter> enters = buySettleDAO.listEnterBySettle(number, user.getCompanyid());
		if (enters.size() > 0) {
			for (T_Buy_Enter enter : enters) {
				double sum = Math.abs(enter.getEt_discount_money()+enter.getEt_prepay()+enter.getEt_payabled());
				if (sum == 0) {
					enter.setEt_pay_state(CommonUtil.PAY_STATE_UNPAY);
				} else if (sum < Math.abs(enter.getEt_money())) {
					enter.setEt_pay_state(CommonUtil.PAY_STATE_PART);
				} else {
					enter.setEt_pay_state(CommonUtil.PAY_STATE_FINISH);
				}
			}
			buySettleDAO.updateEnterBySettle(enters);
		}
		//更新客户费用单金额数据
		List<T_Buy_Fee> fees = buySettleDAO.listFeeBySettle(number, user.getCompanyid());
		if (fees.size() > 0) {
			for (T_Buy_Fee fee : fees) {
				double sum = Math.abs(fee.getFe_discount_money()+fee.getFe_prepay()+fee.getFe_payabled());
				if (sum == 0) {
					fee.setFe_pay_state(CommonUtil.PAY_STATE_UNPAY);
				} else if (sum < Math.abs(fee.getFe_money())) {
					fee.setFe_pay_state(CommonUtil.PAY_STATE_PART);
				} else {
					fee.setFe_pay_state(CommonUtil.PAY_STATE_FINISH);
				}
			}
			buySettleDAO.updateFeeBySettle(fees);
		}
		if(settle.getSt_paid() != 0 || settle.getSt_paidmore() > 0){//更改银行帐户余额
			T_Money_Bank bank = bankDAO.queryByCode(settle.getSt_ba_code(), user.getCompanyid());
			bank.setBa_balance(bank.getBa_balance()-settle.getSt_paid()-settle.getSt_paidmore());
			bankDAO.updateBalanceById(bank);
			if(settle.getSt_paid() != 0){//供应商结算单银行流水账
				T_Money_BankRun bankRun = new T_Money_BankRun();
				bankRun.setBr_ba_code(settle.getSt_ba_code());
				bankRun.setBr_balance(bank.getBa_balance()+settle.getSt_paidmore());
				bankRun.setBr_bt_code(CommonUtil.BANKRUN_BUYSETTLE);
				bankRun.setBr_date(DateUtil.getCurrentTime());
				if(settle.getSt_paid()>0){
					bankRun.setBr_out(Math.abs(settle.getSt_paid()));
				}else {
					bankRun.setBr_enter(Math.abs(settle.getSt_paid()));
				}
				bankRun.setBr_manager(settle.getSt_manager());
				bankRun.setBr_number(number);
				bankRun.setBr_remark("供应商结算单");
				bankRun.setBr_shop_code(bank.getBa_shop_code());
				bankRun.setBr_sysdate(DateUtil.getCurrentTime());
				bankRun.setCompanyid(user.getCompanyid());
				bankRunDAO.save(bankRun);
			}
			if(settle.getSt_paidmore() > 0){//预收款银行流水账
				T_Money_BankRun bankRun = new T_Money_BankRun();
				bankRun.setBr_ba_code(settle.getSt_ba_code());
				bankRun.setBr_balance(bank.getBa_balance());
				bankRun.setBr_bt_code(CommonUtil.BANKRUN_BUYPREPAY);
				bankRun.setBr_date(DateUtil.getCurrentTime());
				bankRun.setBr_out(settle.getSt_paidmore());
				bankRun.setBr_manager(settle.getSt_manager());
				bankRun.setBr_number(settle.getSt_pp_number());
				bankRun.setBr_remark("供应商预付款");
				bankRun.setBr_shop_code(bank.getBa_shop_code());
				bankRun.setBr_sysdate(DateUtil.getCurrentTime());
				bankRun.setCompanyid(user.getCompanyid());
				bankRunDAO.save(bankRun);
			}
		}
		
		return settle;
	}
	
	@Override
	@Transactional
	public T_Buy_Settle reverse(String number, T_Sys_User user) {
		if (number == null) {
			throw new IllegalArgumentException("参数number不能为null");
		}
		T_Buy_Settle settle = buySettleDAO.check(number, user.getCompanyid());
		if(settle == null){
			throw new RuntimeException("单据不存在");
		}
		if(!CommonUtil.AR_STATE_APPROVED.equals(settle.getSt_ar_state())){
			throw new RuntimeException("单据未审核或审核未通过");
		}
		
		//1.更新单据审核状态
		settle.setSt_ar_state(CommonUtil.AR_STATE_NOTAPPROVE);
		settle.setSt_ar_date(DateUtil.getCurrentTime());
		buySettleDAO.updateApprove(settle);
		//2.保存审核记录表
		T_Approve_Record record = new T_Approve_Record();
		record.setAr_state(CommonUtil.AR_STATE_REVERSE_APPROVE);
		record.setAr_describe(user.getUs_name()+"反审核单据");
		record.setAr_number(number);
		record.setAr_sysdate(DateUtil.getCurrentTime());
		record.setAr_us_name(user.getUs_name());
		record.setAr_type("t_buy_settle");
		record.setCompanyid(user.getCompanyid());
		approveRecordDAO.save(record);
		
		//3更新供货厂商财务&往来明细账
		T_Buy_Supply supply = supplyDAO.loadSupply(settle.getSt_supply_code(), user.getCompanyid());
		supply.setSp_payable(supply.getSp_payable()+settle.getSt_discount_money());
		supply.setSp_payabled(supply.getSp_payabled()-settle.getSt_prepay()-settle.getSt_paid());
		supply.setSp_prepay(supply.getSp_prepay()+settle.getSt_prepay()-settle.getSt_paidmore());
		supplyDAO.updateSettle(supply);
		T_Buy_Dealings dealings = new T_Buy_Dealings();
		dealings.setDl_supply_code(settle.getSt_supply_code());
		dealings.setDl_number(settle.getSt_number());
		dealings.setDl_type(CommonUtil.BUYDEALINGS_TYPE_JS);
		dealings.setDl_discount_money(-settle.getSt_discount_money());
		dealings.setDl_payable(0d);
		dealings.setDl_payabled(-settle.getSt_paid());
		dealings.setDl_debt(supply.getSp_payable()-supply.getSp_payabled()-supply.getSp_prepay()-settle.getSt_paidmore());
		dealings.setDl_date(DateUtil.getCurrentTime());
		dealings.setDl_manager(user.getUs_name());
		dealings.setDl_remark("反审核恢复账目往来明细");
		dealings.setDl_sysdate(DateUtil.getCurrentTime());
		dealings.setCompanyid(user.getCompanyid());
		buyDealingsDAO.save(dealings);
		if(StringUtil.isNotEmpty(settle.getSt_pp_number())){//预收款单及其往来明细
			buyPrepayDAO.del(settle.getSt_pp_number(), user.getCompanyid());
			dealings = new T_Buy_Dealings();
			dealings.setDl_supply_code(settle.getSt_supply_code());
			dealings.setDl_number(settle.getSt_pp_number());
			dealings.setDl_type(CommonUtil.BUYDEALINGS_TYPE_YFK);
			dealings.setDl_discount_money(0d);
			dealings.setDl_payable(0d);
			dealings.setDl_payabled(-settle.getSt_paidmore());
			dealings.setDl_debt(supply.getSp_payable()-supply.getSp_payabled()-supply.getSp_prepay());
			dealings.setDl_date(DateUtil.getCurrentTime());
			dealings.setDl_manager(user.getUs_name());
			dealings.setDl_remark("反审核恢复账目往来明细");
			dealings.setDl_sysdate(DateUtil.getCurrentTime());
			dealings.setCompanyid(user.getCompanyid());
			buyDealingsDAO.save(dealings);
		}
		
		//更改批发销售单金额数据
		List<T_Buy_Enter> enters = buySettleDAO.listEnterBySettle_Reverse(number, user.getCompanyid());
		if (enters.size() > 0) {
			for (T_Buy_Enter enter : enters) {
				double sum = Math.abs(enter.getEt_discount_money()+enter.getEt_prepay()+enter.getEt_payabled());
				if (sum == 0) {
					enter.setEt_pay_state(CommonUtil.PAY_STATE_UNPAY);
				} else if (sum < Math.abs(enter.getEt_money())) {
					enter.setEt_pay_state(CommonUtil.PAY_STATE_PART);
				} else {
					enter.setEt_pay_state(CommonUtil.PAY_STATE_FINISH);
				}
			}
			buySettleDAO.updateEnterBySettle(enters);
		}
		//更新客户费用单金额数据
		List<T_Buy_Fee> fees = buySettleDAO.listFeeBySettle_Reverse(number, user.getCompanyid());
		if (fees.size() > 0) {
			for (T_Buy_Fee fee : fees) {
				double sum = Math.abs(fee.getFe_discount_money()+fee.getFe_prepay()+fee.getFe_payabled());
				if (sum == 0) {
					fee.setFe_pay_state(CommonUtil.PAY_STATE_UNPAY);
				} else if (sum < Math.abs(fee.getFe_money())) {
					fee.setFe_pay_state(CommonUtil.PAY_STATE_PART);
				} else {
					fee.setFe_pay_state(CommonUtil.PAY_STATE_FINISH);
				}
			}
			buySettleDAO.updateFeeBySettle(fees);
		}
		if(settle.getSt_paid() != 0 || settle.getSt_paidmore() > 0){//更改银行帐户余额
			T_Money_Bank bank = bankDAO.queryByCode(settle.getSt_ba_code(), user.getCompanyid());
			bank.setBa_balance(bank.getBa_balance()+settle.getSt_paid()+settle.getSt_paidmore());
			bankDAO.updateBalanceById(bank);
			if(settle.getSt_paid() != 0){//供应商结算单银行流水账
				T_Money_BankRun bankRun = new T_Money_BankRun();
				bankRun.setBr_ba_code(settle.getSt_ba_code());
				bankRun.setBr_balance(bank.getBa_balance()-settle.getSt_paidmore());
				bankRun.setBr_bt_code(CommonUtil.BANKRUN_BUYSETTLE);
				bankRun.setBr_date(DateUtil.getCurrentTime());
				if(settle.getSt_paid()>0){
					bankRun.setBr_enter(Math.abs(settle.getSt_paid()));
				}else {
					bankRun.setBr_out(Math.abs(settle.getSt_paid()));
				}
				bankRun.setBr_manager(settle.getSt_manager());
				bankRun.setBr_number(number);
				bankRun.setBr_remark("反审核恢复账目");
				bankRun.setBr_shop_code(bank.getBa_shop_code());
				bankRun.setBr_sysdate(DateUtil.getCurrentTime());
				bankRun.setCompanyid(user.getCompanyid());
				bankRunDAO.save(bankRun);
			}
			if(settle.getSt_paidmore() > 0){//预付款银行流水账
				T_Money_BankRun bankRun = new T_Money_BankRun();
				bankRun.setBr_ba_code(settle.getSt_ba_code());
				bankRun.setBr_balance(bank.getBa_balance());
				bankRun.setBr_bt_code(CommonUtil.BANKRUN_BUYPREPAY);
				bankRun.setBr_date(DateUtil.getCurrentTime());
				bankRun.setBr_enter(settle.getSt_paidmore());
				bankRun.setBr_manager(settle.getSt_manager());
				bankRun.setBr_number(settle.getSt_pp_number());
				bankRun.setBr_remark("反审核恢复账目");
				bankRun.setBr_shop_code(bank.getBa_shop_code());
				bankRun.setBr_sysdate(DateUtil.getCurrentTime());
				bankRun.setCompanyid(user.getCompanyid());
				bankRunDAO.save(bankRun);
			}
		}
		return settle;
	}
	
	@Override
	@Transactional
	public void initUpdate(String number, Integer us_id, Integer companyid) {
		List<T_Buy_SettleList> details = buySettleDAO.detail_list(number, companyid);
		for(T_Buy_SettleList item:details){
			item.setStl_us_id(us_id);
			item.setStl_join(1);
		}
		buySettleDAO.temp_clear(us_id, companyid);
		buySettleDAO.temp_save(details);
	}
	
	@Override
	@Transactional
	public void del(String number, Integer companyid) {
		if (number == null) {
			throw new IllegalArgumentException("参数number不能为null");
		}
		T_Buy_Settle settle = buySettleDAO.check(number, companyid);
		if(settle == null){
			throw new RuntimeException("单据不存在");
		}
		if(!CommonUtil.AR_STATE_NOTAPPROVE.equals(settle.getSt_ar_state()) && !CommonUtil.AR_STATE_FAIL.equals(settle.getSt_ar_state())){
			throw new RuntimeException("单据已审核通过不能删除！");
		}
		buySettleDAO.del(number, companyid);
	}
	
	@Override
	public Map<String, Object> loadPrintData(String number, Integer sp_id, T_Sys_User user) {
		Map<String, Object> resultMap = printDAO.loadPrint4Bill(sp_id);
		T_Buy_Settle settle = buySettleDAO.load(number, user.getCompanyid());
		List<T_Buy_SettleList> settleList = buySettleDAO.detail_list(number, user.getCompanyid());
		resultMap.put("settle", settle);
		resultMap.put("settleList", settleList);
		resultMap.put("supply", supplyDAO.load(settle.getSt_supply_code(), user.getCompanyid()));
		return resultMap;
	}
	
}
