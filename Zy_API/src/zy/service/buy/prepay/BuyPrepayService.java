package zy.service.buy.prepay;

import java.util.Map;

import zy.entity.PageData;
import zy.entity.approve.T_Approve_Record;
import zy.entity.buy.prepay.T_Buy_Prepay;
import zy.entity.buy.supply.T_Buy_Supply;
import zy.entity.money.bank.T_Money_Bank;
import zy.entity.sys.user.T_Sys_User;

public interface BuyPrepayService {
	PageData<T_Buy_Prepay> page(Map<String, Object> params);
	T_Buy_Prepay load(Integer pp_id);
	T_Buy_Prepay load(String number,Integer companyid);
	T_Buy_Supply loadSupply(String sp_code,Integer companyid);
	T_Money_Bank loadBank(String ba_code,Integer companyid);
	void save(T_Buy_Prepay prepay, T_Sys_User user);
	void update(T_Buy_Prepay prepay, T_Sys_User user);
	T_Buy_Prepay approve(String number, T_Approve_Record record, T_Sys_User user);
	T_Buy_Prepay reverse(String number, T_Sys_User user);
	void del(String number, Integer companyid);
}
