package zy.service.buy.prepay.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.approve.ApproveRecordDAO;
import zy.dao.buy.dealings.BuyDealingsDAO;
import zy.dao.buy.prepay.BuyPrepayDAO;
import zy.dao.buy.supply.SupplyDAO;
import zy.dao.money.bank.BankDAO;
import zy.dao.money.bank.BankRunDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.approve.T_Approve_Record;
import zy.entity.buy.dealings.T_Buy_Dealings;
import zy.entity.buy.prepay.T_Buy_Prepay;
import zy.entity.buy.supply.T_Buy_Supply;
import zy.entity.money.bank.T_Money_Bank;
import zy.entity.money.bank.T_Money_BankRun;
import zy.entity.sys.user.T_Sys_User;
import zy.service.buy.prepay.BuyPrepayService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;

@Service
public class BuyPrepayServiceImpl implements BuyPrepayService{
	@Resource
	private BuyPrepayDAO buyPrepayDAO;
	@Resource
	private ApproveRecordDAO approveRecordDAO;
	@Resource
	private BuyDealingsDAO buyDealingsDAO;
	@Resource
	private BankDAO bankDAO;
	@Resource
	private BankRunDAO bankRunDAO;
	@Resource
	private SupplyDAO supplyDAO;
	
	@Override
	public PageData<T_Buy_Prepay> page(Map<String, Object> params) {
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		
		Integer totalCount = buyPrepayDAO.count(params);
		PageInfo pageInfo = new PageInfo(totalCount, pageSize, pageIndex);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		
		List<T_Buy_Prepay> list = buyPrepayDAO.list(params);
		PageData<T_Buy_Prepay> pageData = new PageData<T_Buy_Prepay>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	
	@Override
	public T_Buy_Prepay load(Integer pp_id) {
		T_Buy_Prepay prepay = buyPrepayDAO.load(pp_id);
		if(prepay != null){
			T_Approve_Record approve_Record = approveRecordDAO.load(prepay.getPp_number(), prepay.getCompanyid());
			if(approve_Record != null){
				prepay.setAr_describe(approve_Record.getAr_describe());
			}
		}
		return prepay;
	}
	
	@Override
	public T_Buy_Prepay load(String number,Integer companyid) {
		return buyPrepayDAO.load(number,companyid);
	}
	
	@Override
	public T_Buy_Supply loadSupply(String sp_code,Integer companyid) {
		return supplyDAO.loadSupply(sp_code, companyid);
	}
	
	@Override
	public T_Money_Bank loadBank(String ba_code,Integer companyid) {
		return bankDAO.queryByCode(ba_code, companyid);
	}
	
	@Override
	@Transactional
	public void save(T_Buy_Prepay prepay, T_Sys_User user) {
		if(prepay == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		if(StringUtil.isEmpty(prepay.getPp_supply_code())){
			throw new IllegalArgumentException("供货厂商不能为空");
		}
		if(StringUtil.isEmpty(prepay.getPp_ba_code())){
			throw new IllegalArgumentException("银行账户不能为空");
		}
		if(StringUtil.isEmpty(prepay.getPp_manager())){
			throw new IllegalArgumentException("经办人不能为空");
		}
		if (prepay.getPp_money() == null || prepay.getPp_money().doubleValue() == 0d) {
			throw new IllegalArgumentException("预收金额不能为0");
		}
		if (prepay.getPp_money() > 0) {
			prepay.setPp_type(0);
		}else {
			prepay.setPp_type(1);
		}
		prepay.setCompanyid(user.getCompanyid());
		prepay.setPp_us_id(user.getUs_id());
		prepay.setPp_maker(user.getUs_name());
		prepay.setPp_ar_state(CommonUtil.AR_STATE_NOTAPPROVE);
		prepay.setPp_sysdate(DateUtil.getCurrentTime());
		buyPrepayDAO.save(prepay);
	}
	
	@Override
	@Transactional
	public void update(T_Buy_Prepay prepay, T_Sys_User user) {
		if(prepay == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		if(StringUtil.isEmpty(prepay.getPp_supply_code())){
			throw new IllegalArgumentException("供货厂商不能为空");
		}
		if(StringUtil.isEmpty(prepay.getPp_ba_code())){
			throw new IllegalArgumentException("银行账户不能为空");
		}
		if(StringUtil.isEmpty(prepay.getPp_manager())){
			throw new IllegalArgumentException("经办人不能为空");
		}
		if (prepay.getPp_money() == null || prepay.getPp_money().doubleValue() == 0d) {
			throw new IllegalArgumentException("预收金额不能为0");
		}
		T_Buy_Prepay oldPrepay = buyPrepayDAO.check(prepay.getPp_number(), user.getCompanyid());
		if(oldPrepay == null || !CommonUtil.AR_STATE_FAIL.equals(oldPrepay.getPp_ar_state())){
			throw new RuntimeException("单据已修改，请勿重复提交");
		}
		if (prepay.getPp_money() > 0) {
			prepay.setPp_type(0);
		}else {
			prepay.setPp_type(1);
		}
		prepay.setCompanyid(user.getCompanyid());
		prepay.setPp_us_id(user.getUs_id());
		prepay.setPp_maker(user.getUs_name());
		prepay.setPp_ar_state(CommonUtil.AR_STATE_NOTAPPROVE);
		prepay.setPp_ar_date(null);
		prepay.setPp_sysdate(DateUtil.getCurrentTime());
		buyPrepayDAO.update(prepay);
	}
	
	@Override
	@Transactional
	public T_Buy_Prepay approve(String number, T_Approve_Record record, T_Sys_User user) {
		if (number == null) {
			throw new IllegalArgumentException("参数number不能为null");
		}
		T_Buy_Prepay prepay = buyPrepayDAO.check(number, user.getCompanyid());
		if(prepay == null){
			throw new RuntimeException("单据不存在");
		}
		if(!CommonUtil.AR_STATE_NOTAPPROVE.equals(prepay.getPp_ar_state())){
			throw new RuntimeException("单据已经审核");
		}
		T_Buy_Supply supply = supplyDAO.loadSupply(prepay.getPp_supply_code(), user.getCompanyid());
		supply.setSp_prepay(supply.getSp_prepay() + prepay.getPp_money());
		if (CommonUtil.AR_STATE_APPROVED.equals(record.getAr_state())
				&& prepay.getPp_money() < 0 && supply.getSp_prepay() < 0) {// 退款则验证供应商预付款余额是否足够
			throw new RuntimeException("供应商预付款余额不足");
		}
		//更新单据审核状态
		prepay.setPp_ar_state(record.getAr_state());
		prepay.setPp_ar_date(DateUtil.getCurrentTime());
		buyPrepayDAO.updateApprove(prepay);
		//保存审核记录表
		record.setAr_number(number);
		record.setAr_sysdate(DateUtil.getCurrentTime());
		record.setAr_us_name(user.getUs_name());
		record.setAr_type("t_buy_prepay");
		record.setCompanyid(user.getCompanyid());
		approveRecordDAO.save(record);
		if(!CommonUtil.AR_STATE_APPROVED.equals(prepay.getPp_ar_state())){//审核不通过，则直接返回
			return prepay;
		}
		//3.审核通过
		//更新供应商财务&生成往来明细账
		supplyDAO.updatePrepay(supply);
		T_Buy_Dealings dealings = new T_Buy_Dealings();
		dealings.setDl_supply_code(prepay.getPp_supply_code());
		dealings.setDl_number(prepay.getPp_number());
		if(prepay.getPp_money()>0){
			dealings.setDl_type(CommonUtil.BUYDEALINGS_TYPE_YFK);
		}else {
			dealings.setDl_type(CommonUtil.BUYDEALINGS_TYPE_YFTK);
		}
		dealings.setDl_discount_money(0d);
		dealings.setDl_payable(0d);
		dealings.setDl_payabled(prepay.getPp_money());
		dealings.setDl_debt(supply.getSp_payable()-supply.getSp_payabled()-supply.getSp_prepay());
		dealings.setDl_date(DateUtil.getCurrentTime());
		dealings.setDl_manager(user.getUs_name());
		dealings.setDl_remark(prepay.getPp_remark());
		dealings.setDl_sysdate(DateUtil.getCurrentTime());
		dealings.setCompanyid(user.getCompanyid());
		buyDealingsDAO.save(dealings);
		//银行账目及流水
		T_Money_Bank bank = bankDAO.queryByCode(prepay.getPp_ba_code(), user.getCompanyid());
		if(bank != null){
			bank.setBa_balance(bank.getBa_balance() - prepay.getPp_money());
			bankDAO.updateBalanceById(bank);
			T_Money_BankRun bankRun = new T_Money_BankRun();
			bankRun.setBr_ba_code(prepay.getPp_ba_code());
			bankRun.setBr_balance(bank.getBa_balance());
			bankRun.setBr_bt_code(CommonUtil.BANKRUN_BUYPREPAY);
			bankRun.setBr_date(prepay.getPp_date());
			if(prepay.getPp_money()>0){
				bankRun.setBr_out(Math.abs(prepay.getPp_money()));
			}else {
				bankRun.setBr_enter(Math.abs(prepay.getPp_money()));
			}
			bankRun.setBr_manager(prepay.getPp_manager());
			bankRun.setBr_number(prepay.getPp_number());
			bankRun.setBr_remark(prepay.getPp_remark());
			bankRun.setBr_shop_code(bank.getBa_shop_code());
			bankRun.setBr_sysdate(DateUtil.getCurrentTime());
			bankRun.setCompanyid(user.getCompanyid());
			bankRunDAO.save(bankRun);
		}
		return prepay;
	}
	
	@Override
	@Transactional
	public T_Buy_Prepay reverse(String number, T_Sys_User user) {
		if (number == null) {
			throw new IllegalArgumentException("参数number不能为null");
		}
		T_Buy_Prepay prepay = buyPrepayDAO.check(number, user.getCompanyid());
		if(prepay == null){
			throw new RuntimeException("单据不存在");
		}
		if(!CommonUtil.AR_STATE_APPROVED.equals(prepay.getPp_ar_state())){
			throw new RuntimeException("单据未审核或审核未通过");
		}
		T_Buy_Supply supply = supplyDAO.loadSupply(prepay.getPp_supply_code(), user.getCompanyid());
		supply.setSp_prepay(supply.getSp_prepay() - prepay.getPp_money());
		if (prepay.getPp_money() > 0 && supply.getSp_prepay() < 0) {// 收款则验证供应商预付款余额是否足够
			throw new RuntimeException("供应商预付款余额不足");
		}
		//更新单据审核状态
		prepay.setPp_ar_state(CommonUtil.AR_STATE_NOTAPPROVE);
		prepay.setPp_ar_date(DateUtil.getCurrentTime());
		buyPrepayDAO.updateApprove(prepay);
		//保存审核记录表
		T_Approve_Record record = new T_Approve_Record();
		record.setAr_state(CommonUtil.AR_STATE_REVERSE_APPROVE);
		record.setAr_describe(user.getUs_name()+"反审核单据");
		record.setAr_number(number);
		record.setAr_sysdate(DateUtil.getCurrentTime());
		record.setAr_us_name(user.getUs_name());
		record.setAr_type("t_buy_prepay");
		record.setCompanyid(user.getCompanyid());
		approveRecordDAO.save(record);
		//更新供应商财务&往来明细账
		supplyDAO.updatePrepay(supply);
		T_Buy_Dealings dealings = new T_Buy_Dealings();
		dealings.setDl_supply_code(prepay.getPp_supply_code());
		dealings.setDl_number(prepay.getPp_number());
		if(prepay.getPp_money()>0){
			dealings.setDl_type(CommonUtil.BUYDEALINGS_TYPE_YFK);
		}else {
			dealings.setDl_type(CommonUtil.BUYDEALINGS_TYPE_YFTK);
		}
		dealings.setDl_discount_money(0d);
		dealings.setDl_payable(0d);
		dealings.setDl_payabled(-prepay.getPp_money());
		dealings.setDl_debt(supply.getSp_payable()-supply.getSp_payabled()-supply.getSp_prepay());
		dealings.setDl_date(DateUtil.getCurrentTime());
		dealings.setDl_manager(user.getUs_name());
		dealings.setDl_remark("反审核恢复账目往来明细");
		dealings.setDl_sysdate(DateUtil.getCurrentTime());
		dealings.setCompanyid(user.getCompanyid());
		buyDealingsDAO.save(dealings);
		//银行账目及流水
		T_Money_Bank bank = bankDAO.queryByCode(prepay.getPp_ba_code(), user.getCompanyid());
		if(bank != null){
			bank.setBa_balance(bank.getBa_balance() + prepay.getPp_money());
			bankDAO.updateBalanceById(bank);
			T_Money_BankRun bankRun = new T_Money_BankRun();
			bankRun.setBr_ba_code(prepay.getPp_ba_code());
			bankRun.setBr_balance(bank.getBa_balance());
			bankRun.setBr_bt_code(CommonUtil.BANKRUN_BUYPREPAY);
			bankRun.setBr_date(prepay.getPp_date());
			if(prepay.getPp_money()>0){
				bankRun.setBr_enter(Math.abs(prepay.getPp_money()));
			}else {
				bankRun.setBr_out(Math.abs(prepay.getPp_money()));
			}
			bankRun.setBr_manager(user.getUs_name());
			bankRun.setBr_number(prepay.getPp_number());
			bankRun.setBr_remark("反审核恢复账目");
			bankRun.setBr_shop_code(bank.getBa_shop_code());
			bankRun.setBr_sysdate(DateUtil.getCurrentTime());
			bankRun.setCompanyid(user.getCompanyid());
			bankRunDAO.save(bankRun);
		}
		return prepay;
	}
	
	@Override
	@Transactional
	public void del(String number, Integer companyid) {
		T_Buy_Prepay prepay = buyPrepayDAO.check(number, companyid);
		if(prepay == null){
			throw new RuntimeException("单据不存在");
		}
		if(!CommonUtil.AR_STATE_NOTAPPROVE.equals(prepay.getPp_ar_state()) && !CommonUtil.AR_STATE_FAIL.equals(prepay.getPp_ar_state())){
			throw new RuntimeException("单据已审核通过不能删除！");
		}
		buyPrepayDAO.del(number, companyid);
	}
	
}
