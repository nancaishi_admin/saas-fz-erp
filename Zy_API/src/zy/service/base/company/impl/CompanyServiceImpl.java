package zy.service.base.company.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.base.company.CompanyDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.sys.company.T_Sys_Company;
import zy.entity.sys.company.T_Zhjr_Unit;
import zy.service.base.company.CompanyService;
import zy.util.CommonUtil;

@Service
public class CompanyServiceImpl implements CompanyService{
	@Resource
	private CompanyDAO companyDAO;
	@Override
	public PageData<T_Sys_Company> company_list(Map<String, Object> param) {
		Object pageSize = param.get(CommonUtil.PAGESIZE);
		Object pageIndex = param.get(CommonUtil.PAGEINDEX);
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = companyDAO.company_count(param);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		param.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		param.put(CommonUtil.END, _pageSize);
		
		List<T_Sys_Company> list = companyDAO.company_list(param);
		PageData<T_Sys_Company> pageData = new PageData<T_Sys_Company>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	@Override
	public Integer queryByCode(String code) {
		return companyDAO.queryByCode(code);
	}
	@Transactional
	@Override
	public void save(T_Sys_Company company) {
		companyDAO.save(company);
	}
	@Override
	public List<T_Zhjr_Unit> queryUnit() {
		return companyDAO.queryUnit();
	}

}
