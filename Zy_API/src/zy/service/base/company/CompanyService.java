package zy.service.base.company;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.sys.company.T_Sys_Company;
import zy.entity.sys.company.T_Zhjr_Unit;

public interface CompanyService {
	PageData<T_Sys_Company> company_list(Map<String,Object> param);
	Integer queryByCode(String code);
	void save(T_Sys_Company company);
	List<T_Zhjr_Unit> queryUnit();
}
