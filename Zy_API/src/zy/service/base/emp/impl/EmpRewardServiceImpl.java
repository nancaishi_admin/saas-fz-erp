package zy.service.base.emp.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.base.emp.EmpRewardDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.base.emp.T_Base_Emp_Reward;
import zy.service.base.emp.EmpRewardService;
import zy.util.CommonUtil;

@Service
public class EmpRewardServiceImpl implements EmpRewardService{
	@Resource
	private EmpRewardDAO empRewardDAO;

	@Override
	public List<T_Base_Emp_Reward> statByEmp(Map<String, Object> params) {
		return empRewardDAO.statByEmp(params);
	}
	
	@Override
	public PageData<T_Base_Emp_Reward> page(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		Object pageSize = params.get(CommonUtil.PAGESIZE);
		Object pageIndex = params.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = empRewardDAO.count(params);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		params.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		params.put(CommonUtil.END, _pageSize);
		
		List<T_Base_Emp_Reward> list = empRewardDAO.list(params);
		PageData<T_Base_Emp_Reward> pageData = new PageData<T_Base_Emp_Reward>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	
}
