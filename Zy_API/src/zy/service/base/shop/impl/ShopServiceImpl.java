package zy.service.base.shop.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.base.shop.ShopDAO;
import zy.dto.base.shop.ShopMoneyDetailsDto;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.base.shop.T_Base_Shop;
import zy.service.base.shop.ShopService;
import zy.util.CommonUtil;

@Service
public class ShopServiceImpl implements ShopService {
	@Resource
	private ShopDAO shopDAO;

	@Override
	public List<T_Base_Shop> all_list(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return shopDAO.all_list(param);
	}

	@Override
	public List<T_Base_Shop> sub_list(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return shopDAO.sub_list(param);
	}
	@Override
	public List<T_Base_Shop> sub_list_jmd(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return shopDAO.sub_list_jmd(param);
	}
	@Override
	public List<T_Base_Shop> selectList(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return shopDAO.selectList(param);
	}
	@Override
	public List<T_Base_Shop> price_list(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return shopDAO.price_list(param);
	}
	
	@Override
	public List<T_Base_Shop> want_list(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return shopDAO.want_list(param);
	}
	
	@Override
	public List<T_Base_Shop> allot_list(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return shopDAO.allot_list(param);
	}
	
	@Override
	public List<T_Base_Shop> money_list(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return shopDAO.money_list(param);
	}

	@Override
	public List<T_Base_Shop> up_sub_list(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return shopDAO.up_sub_list(param);
	}

	@Override
	public List<T_Base_Shop> sub_tree(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录");
		}
		return shopDAO.sub_tree(param);
	}

	@Override
	public PageData<T_Base_Shop> page(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object pageSize = param.get(CommonUtil.PAGESIZE);
		Object pageIndex = param.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}

		Integer _pageSize = (Integer) pageSize;
		Integer _pageIndex = (Integer) pageIndex;

		Integer totalCount = shopDAO.count(param);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		param.put(CommonUtil.START, (_pageIndex - 1) * _pageSize);
		param.put(CommonUtil.END, _pageSize);

		List<T_Base_Shop> list = shopDAO.list(param);
		PageData<T_Base_Shop> pageData = new PageData<T_Base_Shop>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}

	@Override
	public PageData<T_Base_Shop> pageZone(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object pageSize = param.get(CommonUtil.PAGESIZE);
		Object pageIndex = param.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}

		Integer _pageSize = (Integer) pageSize;
		Integer _pageIndex = (Integer) pageIndex;

		Integer totalCount = shopDAO.countZone(param);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		param.put(CommonUtil.START, (_pageIndex - 1) * _pageSize);
		param.put(CommonUtil.END, _pageSize);

		List<T_Base_Shop> list = shopDAO.listZone(param);
		PageData<T_Base_Shop> pageData = new PageData<T_Base_Shop>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}

	@Override
	public List<T_Base_Shop> listByEmp(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return shopDAO.listByEmp(param);
	}

	@Override
	public T_Base_Shop queryByID(Integer id) {
		return shopDAO.queryByID(id);
	}

	@Transactional
	@Override
	public void update(T_Base_Shop model) {
		shopDAO.update(model);
	}

	@Transactional
	@Override
	public void save(T_Base_Shop model) {
		shopDAO.save(model);
		shopDAO.saveSellSet(model);
	}

	@Override
	public void saveZone(T_Base_Shop model) {
		shopDAO.saveZone(model);
	}

	@Transactional
	@Override
	public void del(Integer id) {
		shopDAO.del(id);
	}

	@Override
	public void updateState(T_Base_Shop model) {
		shopDAO.updateState(model);
	}

	@Override
	public PageData<ShopMoneyDetailsDto> pageMoneyDetails(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		
		Map<String, Object> sumMap = shopDAO.countsumMoneyDetails(params);
		int totalCount = Integer.parseInt(sumMap.get("totalCount").toString());
		PageInfo pageInfo = new PageInfo(totalCount, pageSize, pageIndex);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		
		List<ShopMoneyDetailsDto> list = shopDAO.listMoneyDetails(params);
		PageData<ShopMoneyDetailsDto> pageData = new PageData<ShopMoneyDetailsDto>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		pageData.setData(sumMap);
		return pageData;
	}
	
}
