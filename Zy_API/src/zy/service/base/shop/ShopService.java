package zy.service.base.shop;

import java.util.List;
import java.util.Map;

import zy.dto.base.shop.ShopMoneyDetailsDto;
import zy.entity.PageData;
import zy.entity.base.shop.T_Base_Shop;

public interface ShopService {
	List<T_Base_Shop> all_list(Map<String,Object> param);
	List<T_Base_Shop> sub_list(Map<String, Object> param);
	List<T_Base_Shop> sub_list_jmd(Map<String, Object> param);
	List<T_Base_Shop> selectList(Map<String, Object> param);
	List<T_Base_Shop> price_list(Map<String, Object> param);
	List<T_Base_Shop> want_list(Map<String, Object> param);
	List<T_Base_Shop> allot_list(Map<String, Object> param);
	List<T_Base_Shop> money_list(Map<String, Object> param);
	List<T_Base_Shop> sub_tree(Map<String, Object> param);
	List<T_Base_Shop> listByEmp(Map<String, Object> param);
	PageData<T_Base_Shop> page(Map<String,Object> param);
	PageData<T_Base_Shop> pageZone(Map<String,Object> param);
	T_Base_Shop queryByID(Integer id);
	void update(T_Base_Shop model);
	void save(T_Base_Shop model);
	void saveZone(T_Base_Shop model);
	void del(Integer id);
	void updateState(T_Base_Shop model);
	//查询上级和下级的店铺
	public List<T_Base_Shop> up_sub_list(Map<String, Object> param);
	PageData<ShopMoneyDetailsDto> pageMoneyDetails(Map<String, Object> params);
}
