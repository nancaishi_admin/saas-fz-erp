package zy.service.base.shop;

import java.util.List;

import zy.entity.base.shop.T_Base_Shop_Line;
import zy.entity.sys.user.T_Sys_User;

public interface ShopLineService {
	T_Base_Shop_Line load(String shop_code, Integer companyid);
	void save(T_Base_Shop_Line shopLine, T_Sys_User user);
	List<T_Base_Shop_Line> listByMobile(String mobile);
}
