package zy.service.base.department;

import java.util.List;
import java.util.Map;

import zy.entity.base.department.T_Base_Department;

public interface DepartmentService {
	List<T_Base_Department> list(Map<String,Object> param);
	Integer queryByName(T_Base_Department model);
	T_Base_Department queryByID(Integer id);
	void save(T_Base_Department model);
	void update(T_Base_Department model);
	void del(Integer id);
}
