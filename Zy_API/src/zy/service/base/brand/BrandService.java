package zy.service.base.brand;

import java.util.Map;

import jxl.Workbook;
import jxl.write.WritableWorkbook;
import zy.entity.PageData;
import zy.entity.base.brand.T_Base_Brand;

public interface BrandService {
	PageData<T_Base_Brand> page(Map<String,Object> param);
	Integer queryByName(T_Base_Brand brand);
	T_Base_Brand queryByID(Integer bd_id);
	void save(T_Base_Brand brand);
	void save(Map<String,Object> param);
	void update(T_Base_Brand brand);
	void del(Integer bd_id);
	Map<String, Object> parseSavingBrandExcel(Workbook soureWorkBook, WritableWorkbook errsheet, int companyid) throws Exception;
}
