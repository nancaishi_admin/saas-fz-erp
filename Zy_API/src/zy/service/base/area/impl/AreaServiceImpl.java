package zy.service.base.area.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.base.area.AreaDAO;
import zy.entity.base.area.T_Base_Area;
import zy.service.base.area.AreaService;
import zy.util.CommonUtil;

@Service
public class AreaServiceImpl implements AreaService{
	
	@Resource
	private AreaDAO areaDAO;

	@Override
	public List<T_Base_Area> list(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return areaDAO.list(param);
	}

	@Override
	public Integer queryByName(T_Base_Area area) {
		return areaDAO.queryByName(area);
	}

	@Override
	public T_Base_Area queryByID(Integer ar_id) {
		return areaDAO.queryByID(ar_id);
	}

	@Override
	public void save(T_Base_Area area) {
		areaDAO.save(area);
	}

	@Override
	public void update(T_Base_Area area) {
		areaDAO.update(area);
	}

	@Override
	public void del(Integer ar_id) {
		areaDAO.del(ar_id);
	}

}
