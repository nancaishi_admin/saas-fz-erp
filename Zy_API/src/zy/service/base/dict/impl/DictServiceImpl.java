package zy.service.base.dict.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.base.dict.DictDAO;
import zy.entity.base.dict.T_Base_Dict;
import zy.entity.base.dict.T_Base_DictList;
import zy.service.base.dict.DictService;

@Service
public class DictServiceImpl implements DictService{
	@Resource
	private DictDAO dictDAO;
	
	@Override
	public List<T_Base_Dict> uplist(Map<String, Object> param) {
		return dictDAO.uplist(param);
	}

	@Override
	public List<T_Base_DictList> list(Map<String, Object> param) {
		return dictDAO.list(param);
	}

	@Override
	public Integer queryByName(T_Base_DictList model) {
		return dictDAO.queryByName(model);
	}

	@Override
	public T_Base_DictList queryByID(Integer id) {
		return dictDAO.queryByID(id);
	}

	@Override
	public void save(T_Base_DictList model) {
		dictDAO.save(model);
	}

	@Override
	public void update(T_Base_DictList model) {
		dictDAO.update(model);
	}

	@Override
	public void del(Integer id) {
		dictDAO.del(id);
	}

}
