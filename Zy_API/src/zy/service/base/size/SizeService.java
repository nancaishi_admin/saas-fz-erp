package zy.service.base.size;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.base.size.T_Base_Size;
import zy.entity.base.size.T_Base_SizeList;
import zy.entity.base.size.T_Base_Size_Group;

public interface SizeService {
	PageData<T_Base_Size> page(Map<String,Object> param);
	List<T_Base_Size> list(Map<String,Object> param);
	Integer queryByName(T_Base_Size model);
	T_Base_Size queryByID(Integer id);
	void save(T_Base_Size model);
	void update(T_Base_Size model);
	void del(Integer id);
	
	
	//------组的操作
	List<T_Base_Size_Group> listGroup(Map<String,Object> param);
	T_Base_Size_Group queryGroupByID(Integer id);
	void saveGroup(Map<String,Object> map);
	void updateGroup(Map<String,Object> map);
	void delGroup(Integer id);
	
	//-----以下是子表操作方法
	List<T_Base_SizeList> listByID(Map<String,Object> param);
}
