package zy.service.base.stream.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.base.stream.StreamDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.base.stream.T_Base_Stream;
import zy.service.base.stream.StreamService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Service
public class StreamServiceImpl implements StreamService {
	@Resource
	private StreamDAO streamDAO;
	@Override
	public PageData<T_Base_Stream> page(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object pageSize = param.get(CommonUtil.PAGESIZE);
		Object pageIndex = param.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = streamDAO.count(param);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		param.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		param.put(CommonUtil.END, _pageSize);
		
		List<T_Base_Stream> list = streamDAO.list(param);
		PageData<T_Base_Stream> pageData = new PageData<T_Base_Stream>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	@Override
	public T_Base_Stream queryByID(Integer id) {
		return streamDAO.queryByID(id);
	}
	@Transactional
	@Override
	public void save(T_Base_Stream model) {
		if(StringUtil.isEmpty(model.getSe_name())){
			throw new IllegalArgumentException("物流公司名称不能为空");
		}
		T_Base_Stream existStream = streamDAO.check(model);
		if (existStream != null) {
			throw new RuntimeException("物流公司已存在!");
		}
		if(model.getSe_init_debt() == null){
			model.setSe_init_debt(0d);
		}
		model.setSe_payable(0d);
		model.setSe_payabled(0d);
		streamDAO.save(model);
	}
	@Transactional
	@Override
	public void update(T_Base_Stream model) {
		if(StringUtil.isEmpty(model.getSe_name())){
			throw new IllegalArgumentException("物流公司名称不能为空");
		}
		T_Base_Stream existStream = streamDAO.check(model);
		if (existStream != null) {
			throw new RuntimeException("物流公司已存在!");
		}
		streamDAO.update(model);
	}
	@Transactional
	@Override
	public void del(Integer id) {
		streamDAO.del(id);
	}
}
