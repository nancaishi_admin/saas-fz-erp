package zy.service.base.affiliate;

import java.util.List;
import java.util.Map;

import zy.entity.base.affiliate.T_Base_Affiliate;

public interface AffiliateService {
	List<T_Base_Affiliate> list(Map<String,Object> param);
	Integer queryByName(T_Base_Affiliate affiliate);
	T_Base_Affiliate queryByID(Integer af_id);
	void save(T_Base_Affiliate affiliate);
	void update(T_Base_Affiliate affiliate);
	void del(Integer af_id);
}
