package zy.service.report.impl;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.base.shop.ShopDAO;
import zy.dao.report.ReportDAO;
import zy.dao.stock.data.DataDAO;
import zy.dto.report.kpi.KpiAnalysisDayDto;
import zy.dto.report.kpi.KpiAnalysisDto;
import zy.dto.report.kpi.KpiAnalysisMonthDto;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.base.shop.T_Base_Shop;
import zy.entity.sell.cash.T_Sell_ShopList;
import zy.entity.stock.data.T_Stock_Data;
import zy.service.report.ReportService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.ErrorUtil;
import zy.util.NumberUtil;
import zy.util.StringUtil;
import zy.vo.sell.SellVO;
@Service
public class ReportServiceImpl implements ReportService{
	@Resource
	private ReportDAO reportDAO;
	@Resource
	private ShopDAO shopDAO;
	@Resource
	private DataDAO dataDAO;
	@Override
	public PageData<T_Sell_ShopList> sell_max(Map<String, Object> paramMap) {
		Object companyid = paramMap.get(CommonUtil.COMPANYID);
		String chkss_day = (String)paramMap.get("chkss_day");
		Integer ss_day = (Integer) paramMap.get("ss_day");
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer pageSize = (Integer)paramMap.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)paramMap.get(CommonUtil.PAGEINDEX);
		
		String pd_date = "";
		if(StringUtil.isNotEmpty(chkss_day) && "0".equals(chkss_day)){//根据天数计算出需要的上市时间
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -ss_day);
			pd_date = DateUtil.format(cal, "yyyy-MM-dd");
		}
		paramMap.put("pd_date", pd_date);
		
		Map<String,Object> map = reportDAO.sell_max_count(paramMap);
		PageInfo pageInfo = new PageInfo(NumberUtil.toInteger(map.get("sh_count")), pageSize, pageIndex);
		paramMap.put(CommonUtil.START, (pageIndex-1)*pageSize);
		paramMap.put(CommonUtil.END, pageSize);
		List<T_Sell_ShopList> list = reportDAO.sell_max_list(paramMap);
		if(list!=null && list.size()>0){
			paramMap.put("pd_codes", SellVO.listToString(list));
			List<T_Stock_Data> data = dataDAO.listByPdCode(paramMap);
			SellVO.buildStock(list,data);
		}
		PageData<T_Sell_ShopList> pageData = new PageData<T_Sell_ShopList>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		pageData.setData(map);
		return pageData;
	}
	
	@Override
	public PageData<T_Sell_ShopList> sell_max_list(Map<String, Object> paramMap) {
		Object companyid = paramMap.get(CommonUtil.COMPANYID);
		String chkss_day = (String)paramMap.get("chkss_day");
		Integer ss_day = (Integer) paramMap.get("ss_day");
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer pageSize = (Integer)paramMap.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)paramMap.get(CommonUtil.PAGEINDEX);
		
		String pd_date = "";
		if(StringUtil.isNotEmpty(chkss_day) && "0".equals(chkss_day)){//根据天数计算出需要的上市时间
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -ss_day);
			pd_date = DateUtil.format(cal, "yyyy-MM-dd");
		}
		paramMap.put("pd_date", pd_date);
		
		Map<String,Object> map = reportDAO.selllist_max_count(paramMap);
		PageInfo pageInfo = new PageInfo(NumberUtil.toInteger(map.get("sh_count")), pageSize, pageIndex);
		paramMap.put(CommonUtil.START, (pageIndex-1)*pageSize);
		paramMap.put(CommonUtil.END, pageSize);
		List<T_Sell_ShopList> list = reportDAO.selllist_max_list(paramMap);
		if(list!=null && list.size()>0){
			paramMap.put("sub_codes", SellVO.listToSubString(list));
			List<T_Stock_Data> data = dataDAO.listBySubCode(paramMap);
			SellVO.buildSubStock(list,data);
		}
		PageData<T_Sell_ShopList> pageData = new PageData<T_Sell_ShopList>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		pageData.setData(map);
		return pageData;
	}

	@Override
	public List<Map<String,Object>> sell_compare(Map<String, Object> paramMap) {
		List<T_Sell_ShopList> list = reportDAO.sell_compare(paramMap);
		paramMap.put("begindate", DateUtil.getDateAddYears(StringUtil.trimString(paramMap.get("begindate")),-1));
		paramMap.put("enddate", DateUtil.getDateAddYears(StringUtil.trimString(paramMap.get("enddate")),-1));
		List<T_Sell_ShopList> _list = reportDAO.sell_compare(paramMap);
		
		Map<String,Map<String,Object>> thisYear = build2Map(list);
		Map<String,Map<String,Object>> lastYear = build2Map(_list);
		return build1Map(thisYear, lastYear);
	}
	
	private Map<String,Map<String,Object>> build2Map(List<T_Sell_ShopList> list){
		Map<String, Map<String, Object>> maps = new HashMap<String, Map<String,Object>>();
		if(null != list && list.size() > 0){
			for(T_Sell_ShopList item:list){
				if(maps.containsKey(item.getBd_code())){
					maps.get(item.getBd_code()).put("shl_amount"+item.getShl_date(), item.getShl_amount());
					maps.get(item.getBd_code()).put("shl_money"+item.getShl_date(), item.getShl_money());
					maps.get(item.getBd_code()).put("shl_profit"+item.getShl_date(), item.getShl_profit());
				}else {
					Map<String,Object> map = new HashMap<String, Object>();
					map.put("shl_id", item.getShl_id());
					map.put("bd_code", item.getBd_code());
					map.put("bd_name", item.getBd_name());
					map.put("shl_amount"+item.getShl_date(), item.getShl_amount());
					map.put("shl_money"+item.getShl_date(), item.getShl_money());
					map.put("shl_profit"+item.getShl_date(), item.getShl_profit());
					maps.put(item.getBd_code(), map);
				}
			}
		}
		return maps;
	}
	
	private List<Map<String, Object>> build1Map(Map<String,Map<String,Object>> thisYear,Map<String,Map<String,Object>> lastYear){
		List<String> allCode = new ArrayList<String>();
		List<String> allName = new ArrayList<String>();
		List<Map<String, Object>> results = new ArrayList<Map<String,Object>>();
		//取并集
		for(String key:thisYear.keySet()){
			Map<String, Object> item = thisYear.get(key);
			if(!allCode.contains(StringUtil.trimString(item.get("bd_code")))){
				allCode.add(StringUtil.trimString(item.get("bd_code")));
				allName.add(StringUtil.trimString(item.get("bd_name")));
			}
		}
		for(String key:lastYear.keySet()){
			Map<String, Object> item = lastYear.get(key);
			if(!allCode.contains(StringUtil.trimString(item.get("bd_code")))){
				allCode.add(StringUtil.trimString(item.get("bd_code")));
				allName.add(StringUtil.trimString(item.get("bd_name")));
			}
		}
		//构造结果集
		Map<String, Object> item = null;
		for (int i=0;i<allCode.size();i++) {
			String code = allCode.get(i);
			String name = allName.get(i);
			//同期数据
			item = null;
			if(lastYear.containsKey(code)){
				item = lastYear.get(code);
				item.put("shl_id", code+"_0");
				item.put("shl_type", "同期");
			}else {
				item = new HashMap<String, Object>();
				item.put("shl_id", code+"_0");
				item.put("shl_type", "同期");
				item.put("bd_code", code);
				item.put("bd_name", name);
			}
			results.add(item);
			//本期数据
			item = null;
			if(thisYear.containsKey(code)){
				item = thisYear.get(code);
				item.put("shl_id", code+"_1");
				item.put("shl_type", "本期");
			}else {
				item = new HashMap<String, Object>();
				item.put("shl_id", code+"_1");
				item.put("shl_type", "本期");
				item.put("bd_code", code);
				item.put("bd_name", name);
			}
			results.add(item);
		}
		//排序
		final Collator collator = Collator.getInstance(java.util.Locale.CHINA);
        Collections.sort(results, new Comparator<Map<String, Object>>() {
            @Override
            public int compare(Map<String, Object> o1, Map<String, Object> o2) {
                int c = 0;
                Object val1 = o1.get("bd_code");
                Object val2 = o2.get("bd_code");
                c = collator.compare(val1, val2);
                return c;
            }
        });
		return results;
	}
	@Override
	public PageData<Map<String,Object>> sellStock(Map<String, Object> paramMap) {
		Object companyid = paramMap.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException(ErrorUtil.TIME_OUT);
		}
		Integer pageSize = (Integer)paramMap.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)paramMap.get(CommonUtil.PAGEINDEX);
		
		Integer count = reportDAO.sellStock_count(paramMap);
		PageInfo pageInfo = new PageInfo(count, pageSize, pageIndex);
		paramMap.put(CommonUtil.START, (pageIndex-1)*pageSize);
		paramMap.put(CommonUtil.END, pageSize);
		paramMap.put("sp_code", paramMap.get("shl_shop_code"));
//		List<T_Base_Shop> shops = shopDAO.selectList(paramMap);
		List<T_Sell_ShopList> list = reportDAO.sellStock_list(paramMap);
		paramMap.put("sub_codes", SellVO.listToSubString(list));
		List<T_Stock_Data> data = dataDAO.listBySubCode(paramMap);
		List<T_Stock_Data> upData = dataDAO.upListByCode(paramMap);
		SellVO.buildSubStock(list,data);
		List<Map<String,Object>> maps = buildMap(list,upData);
		PageData<Map<String,Object>> pageData = new PageData<Map<String,Object>>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(maps);
		return pageData;
	}
	private List<Map<String,Object>> buildMap(List<T_Sell_ShopList> list,List<T_Stock_Data> upData){
		List<Map<String,Object>> maps = new ArrayList<Map<String,Object>>();
		if(null != list && list.size() > 0){
			Map<String,Object> map = null;
			for(T_Sell_ShopList item:list){
				int flag = 0,amount = 0,total_stock = 0;
				for(Map<String,Object> _map:maps){
					System.out.println(_map.get("shl_sub_code")+":"+item.getShl_sub_code());
					if(StringUtil.trimString(_map.get("shl_sub_code")).equals(item.getShl_sub_code())){
						flag = 1;
//						for(T_Base_Shop shop:shops){
//							if(!shop.getSp_code().equals(item.getShl_shop_code())){
								_map.put("so_"+item.getShl_shop_code(), item.getSd_amount()+"/"+item.getShl_amount());
								_map.put("shl_"+item.getShl_shop_code(), item.getSd_date());
								_map.put("sum_amount", NumberUtil.toInteger(_map.get("sum_amount")) + item.getShl_amount());
								_map.put("total_stock", NumberUtil.toInteger(_map.get("total_stock")) + item.getSd_amount());
//							}
//						}
					}
				}
				if(flag == 0){
					map = new HashMap<String, Object>();
					map.put("shl_id", item.getShl_id());
					map.put("shl_sub_code", item.getShl_sub_code());
					map.put("pd_no", item.getPd_no());
					map.put("pd_name", item.getPd_name());
					map.put("cr_name", item.getCr_name());
					map.put("sz_name", item.getSz_name());
					map.put("br_name", item.getBr_name());
					map.put("shl_sell_price", item.getShl_sell_price());
//					for(T_Base_Shop shop:shops){
//						if(shop.getSp_code().equals(item.getShl_shop_code())){
							map.put("so_"+item.getShl_shop_code(), item.getSd_amount()+"/"+item.getShl_amount());
							map.put("shl_"+item.getShl_shop_code(), item.getSd_date());
							amount += item.getShl_amount();
							total_stock += item.getSd_amount();
//						}
//					}
					if(null != upData && upData.size() > 0){
						for(T_Stock_Data dta:upData){
							if(item.getShl_sub_code().equals(dta.getSd_code())){
								map.put("sum_stock", dta.getSd_amount());
								map.put("sum_date", dta.getSd_date());
								total_stock += dta.getSd_amount();
								map.put("total_stock", total_stock);
							}
						}
					}
					map.put("sum_amount", amount);
					maps.add(map);
				}
			}
			if(maps.size() > 0){
				for(Map<String,Object> _map:maps){
					double amount = NumberUtil.toDouble(_map.get("sum_amount"));
					double total_stock = NumberUtil.toDouble(_map.get("total_stock"));
					if(amount > 0){
						if(total_stock > 0){
							_map.put("sell_rate", StringUtil.double2Two(amount/total_stock*100));
						}else{
							_map.put("sell_rate", "100");
						}
					}else{
						_map.put("sell_rate", "0");
					}
				}
			}
		}
		return maps;
	}

	@Override
	public Map<String, Object> kpi_analysis(Map<String, Object> params) {
		List<T_Base_Shop> shops = reportDAO.listShop(params);
		List<String> shopCodes = new ArrayList<String>();
		for (T_Base_Shop shop : shops) {
			shopCodes.add(shop.getSp_code());
		}
		params.put("shopCodes", shopCodes);
		Map<String, KpiAnalysisDto> thisData = reportDAO.kpi_analysis(params);
		params.put("begindate", DateUtil.getDateAddYears(StringUtil.trimString(params.get("begindate")),-1));
		params.put("enddate", DateUtil.getDateAddYears(StringUtil.trimString(params.get("enddate")),-1)+" 23:59:59");
		Map<String, KpiAnalysisDto> lastData = reportDAO.kpi_analysis(params);
		List<KpiAnalysisDto> list = new ArrayList<KpiAnalysisDto>();
		KpiAnalysisDto item;
		KpiAnalysisDto thisSum = new KpiAnalysisDto();
		KpiAnalysisDto lastSum = new KpiAnalysisDto();
		for (T_Base_Shop shop : shops) {
			//本期数据
			if(thisData.containsKey(shop.getSp_code())){
				item = thisData.get(shop.getSp_code());
			}else {
				item = new KpiAnalysisDto();
			}
			item.setId(shop.getSp_code()+"0");
			item.setShop_code(shop.getSp_code());
			item.setShop_name(shop.getSp_name());
			item.setProject_name("本期");
			list.add(item);
			thisSum.setComeamount(thisSum.getComeamount() + item.getComeamount());
			thisSum.setReceiveamount(thisSum.getReceiveamount() + item.getReceiveamount());
			thisSum.setTryamount(thisSum.getTryamount() + item.getTryamount());
			thisSum.setDealcount(thisSum.getDealcount() + item.getDealcount());
			thisSum.setVipcount(thisSum.getVipcount() + item.getVipcount());
			thisSum.setSellamount(thisSum.getSellamount() + item.getSellamount());
			thisSum.setSellmoney(thisSum.getSellmoney()+item.getSellmoney());
			thisSum.setRetailmoney(thisSum.getRetailmoney()+item.getRetailmoney());
			thisSum.setCostmoney(thisSum.getCostmoney()+item.getCostmoney());
			thisSum.setProfits(thisSum.getProfits()+item.getProfits());
			thisSum.setSellmoney_vip(thisSum.getSellmoney_vip()+item.getSellmoney_vip());
			thisSum.setCardcount(thisSum.getCardcount()+item.getCardcount());
			//同期数据
			if(lastData.containsKey(shop.getSp_code())){
				item = lastData.get(shop.getSp_code());
			}else{
				item = new KpiAnalysisDto();
			}
			item.setId(shop.getSp_code()+"1");
			item.setShop_code(shop.getSp_code());
			item.setShop_name(shop.getSp_name());
			item.setProject_name("同期");
			list.add(item);
			lastSum.setComeamount(lastSum.getComeamount() + item.getComeamount());
			lastSum.setReceiveamount(lastSum.getReceiveamount() + item.getReceiveamount());
			lastSum.setTryamount(lastSum.getTryamount() + item.getTryamount());
			lastSum.setDealcount(lastSum.getDealcount() + item.getDealcount());
			lastSum.setVipcount(lastSum.getVipcount() + item.getVipcount());
			lastSum.setSellamount(lastSum.getSellamount() + item.getSellamount());
			lastSum.setSellmoney(lastSum.getSellmoney()+item.getSellmoney());
			lastSum.setRetailmoney(lastSum.getRetailmoney()+item.getRetailmoney());
			lastSum.setCostmoney(lastSum.getCostmoney()+item.getCostmoney());
			lastSum.setProfits(lastSum.getProfits()+item.getProfits());
			lastSum.setSellmoney_vip(lastSum.getSellmoney_vip()+item.getSellmoney_vip());
			lastSum.setCardcount(lastSum.getCardcount()+item.getCardcount());
		}
		calcOtherKpi(thisSum);
		calcOtherKpi(lastSum);
		Map<String, Object> userData = new HashMap<String, Object>();
		userData.put("shop_name", "合计");
		userData.put("project_name", "本期<br/>同期");
//		userData.put("comeamount", thisSum.getComeamount()+"<br/>"+lastSum.getComeamount());
//		userData.put("receiveamount", thisSum.getReceiveamount()+"<br/>"+lastSum.getReceiveamount());
//		userData.put("tryamount", thisSum.getTryamount()+"<br/>"+lastSum.getTryamount());
//		userData.put("dealcount", thisSum.getDealcount()+"<br/>"+lastSum.getDealcount());
//		userData.put("vipcount", thisSum.getVipcount()+"<br/>"+lastSum.getVipcount());
//		userData.put("sellamount", thisSum.getSellamount()+"<br/>"+lastSum.getSellamount());
//		userData.put("sellmoney", String.format("%.2f", thisSum.getSellmoney())+"<br/>"+String.format("%.2f", lastSum.getSellmoney()));
//		userData.put("retailmoney", String.format("%.2f", thisSum.getRetailmoney())+"<br/>"+String.format("%.2f", lastSum.getRetailmoney()));
//		userData.put("costmoney", String.format("%.2f", thisSum.getCostmoney())+"<br/>"+String.format("%.2f", lastSum.getCostmoney()));
//		userData.put("profits", String.format("%.2f", thisSum.getProfits())+"<br/>"+String.format("%.2f", lastSum.getProfits()));
//		userData.put("avg_discount", String.format("%.2f", thisSum.getAvg_discount())+"<br/>"+String.format("%.2f", lastSum.getAvg_discount()));
//		userData.put("joint_rate", String.format("%.2f", thisSum.getJoint_rate())+"<br/>"+String.format("%.2f", lastSum.getJoint_rate()));
//		userData.put("sellmoney_vip", String.format("%.2f", thisSum.getSellmoney_vip())+"<br/>"+String.format("%.2f", lastSum.getSellmoney_vip()));
//		userData.put("sellmoney_vip_proportion", String.format("%.2f", thisSum.getSellmoney_vip_proportion())+"<br/>"+String.format("%.2f", lastSum.getSellmoney_vip_proportion()));
//		userData.put("avg_price", String.format("%.2f", thisSum.getAvg_price())+"<br/>"+String.format("%.2f", lastSum.getAvg_price()));
//		userData.put("avg_sellprice", String.format("%.2f", thisSum.getAvg_sellprice())+"<br/>"+String.format("%.2f", lastSum.getAvg_sellprice()));
		
		userData.put("comeamount", formatColor(thisSum.getComeamount(), lastSum.getComeamount()));
		userData.put("receiveamount", formatColor(thisSum.getReceiveamount(), lastSum.getReceiveamount()));
		userData.put("tryamount", formatColor(thisSum.getTryamount(), lastSum.getTryamount()));
		userData.put("dealcount", formatColor(thisSum.getDealcount(), lastSum.getDealcount()));
		userData.put("vipcount", formatColor(thisSum.getVipcount(), lastSum.getVipcount()));
		userData.put("sellamount", formatColor(thisSum.getSellamount(), lastSum.getSellamount()));
		userData.put("sellmoney", formatColor(thisSum.getSellmoney(), lastSum.getSellmoney()));
		userData.put("retailmoney", formatColor(thisSum.getRetailmoney(), lastSum.getRetailmoney()));
		userData.put("costmoney", formatColor(thisSum.getCostmoney(), lastSum.getCostmoney()));
		userData.put("profits", formatColor(thisSum.getProfits(), lastSum.getProfits()));
		userData.put("avg_discount", formatColor(thisSum.getAvg_discount(), lastSum.getAvg_discount()));
		userData.put("joint_rate", formatColor(thisSum.getJoint_rate(), lastSum.getJoint_rate()));
		userData.put("sellmoney_vip", formatColor(thisSum.getSellmoney_vip(), lastSum.getSellmoney_vip()));
		userData.put("sellmoney_vip_proportion", formatColor(thisSum.getSellmoney_vip_proportion(), lastSum.getSellmoney_vip_proportion()));
		userData.put("avg_price", formatColor(thisSum.getAvg_price(), lastSum.getAvg_price()));
		userData.put("avg_sellprice", formatColor(thisSum.getAvg_sellprice(), lastSum.getAvg_sellprice()));
		userData.put("cardcount", formatColor(thisSum.getCardcount(), lastSum.getCardcount()));
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("list", list);
		resultMap.put("userData", userData);
		return resultMap;
	}
	
	private String formatColor(int thisData,int lastData){
		if(thisData > lastData){
			return "<span style='color:blue;'>"+thisData+"<br/>"+lastData+"</span>";
		}else if(thisData < lastData){
			return "<span style='color:red;'>"+thisData+"<br/>"+lastData+"</span>";
		}else{
			return thisData+"<br/>"+lastData;
		}
	}
	
	private String formatColor(double thisData,double lastData){
		if(thisData > lastData){
			return "<span style='color:blue;'>"+String.format("%.2f", thisData)+"<br/>"+String.format("%.2f", lastData)+"</span>";
		}else if(thisData < lastData){
			return "<span style='color:red;'>"+String.format("%.2f", thisData)+"<br/>"+String.format("%.2f", lastData)+"</span>";
		}else{
			return thisData+"<br/>"+lastData;
		}
	}
	
	private void calcOtherKpi(KpiAnalysisDto dto){
		if(dto.getDealcount() != 0){
			dto.setJoint_rate(dto.getSellamount() / (double) dto.getDealcount());
			dto.setAvg_sellprice(dto.getSellmoney() / (double) dto.getDealcount());
		}else {
			dto.setJoint_rate(0d);
			dto.setAvg_sellprice(0d);
		}
		if(dto.getSellmoney() != 0){
			dto.setSellmoney_vip_proportion(100 * dto.getSellmoney_vip() / dto.getSellmoney());
		}else {
			dto.setSellmoney_vip_proportion(0d);
		}
		if(dto.getSellamount() != 0){
			dto.setAvg_price(dto.getSellmoney()/dto.getSellamount());
		}else {
			dto.setAvg_price(0d);
		}
		if(dto.getRetailmoney() != 0){
			dto.setAvg_discount(dto.getSellmoney()/dto.getRetailmoney());
		}else {
			dto.setAvg_discount(0d);
		}
	}
	
	@Override
	public List<KpiAnalysisMonthDto> kpi_analysis_month(Map<String, Object> params) {
		Integer year = (Integer)params.get("year");
		List<KpiAnalysisMonthDto> resultDtos = new ArrayList<KpiAnalysisMonthDto>();
		List<T_Base_Shop> shops = reportDAO.listShop(params);
		List<String> shopCodes = new ArrayList<String>();
		for (T_Base_Shop shop : shops) {
			shopCodes.add(shop.getSp_code());
		}
		params.put("shopCodes", shopCodes);
		Map<String, KpiAnalysisMonthDto> resultMap = reportDAO.kpi_analysis_month(params);
		KpiAnalysisMonthDto dto;
		for (int i = 1; i <= 12; i++) {
			if(resultMap.containsKey(String.valueOf(i))){
				dto = resultMap.get(String.valueOf(i));
			}else {
				dto = new KpiAnalysisMonthDto();
			}
			dto.setId(String.valueOf(i));
			dto.setMonth(String.format("%02d", i));
			resultDtos.add(dto);
		}
		return resultDtos;
	}
	
	@Override
	public List<KpiAnalysisDayDto> kpi_analysis_day(Map<String, Object> params) {
		Integer year = (Integer)params.get("year");
		Integer month = (Integer)params.get("month");
		String ym = year + "-" + String.format("%02d", month);
		List<KpiAnalysisDayDto> resultDtos = new ArrayList<KpiAnalysisDayDto>();
		List<T_Base_Shop> shops = reportDAO.listShop(params);
		List<String> shopCodes = new ArrayList<String>();
		for (T_Base_Shop shop : shops) {
			shopCodes.add(shop.getSp_code());
		}
		params.put("shopCodes", shopCodes);
		Map<String, KpiAnalysisDayDto> resultMap = reportDAO.kpi_analysis_day(params);
		KpiAnalysisDayDto dto;
		int days = (int)DateUtil.getMonthDays(params.get("begindate").toString(),DateUtil.FORMAT_YEAR_MON_DAY);
		for (int i = 1; i <= days; i++) {
			if(resultMap.containsKey(String.valueOf(i))){
				dto = resultMap.get(String.valueOf(i));
			}else {
				dto = new KpiAnalysisDayDto();
			}
			dto.setId(String.valueOf(i));
			dto.setDay(ym + "-" + String.format("%02d", i));
			dto.setWeekday(formatWeekDay(dto.getDay()));
			resultDtos.add(dto);
		}
		return resultDtos;
	}
	
	@Override
	public List<Map<String, Object>> brand_run(Map<String, Object> param) {
		Object depot_code = param.get("depot_code");
		if (depot_code == null || depot_code == "") {
			throw new IllegalArgumentException("请选择仓库!");
		}
		List<Map<String, Object>> list =  reportDAO.brand_run(param);
		return list;
	}
	
	@Override
	public PageData<Map<String, Object>> product_run(Map<String, Object> paramMap) {
		Object companyid = paramMap.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer pageSize = (Integer)paramMap.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)paramMap.get(CommonUtil.PAGEINDEX);
		
		Integer count = reportDAO.product_run_count(paramMap);
		PageInfo pageInfo = new PageInfo(count, pageSize, pageIndex);
		paramMap.put(CommonUtil.START, (pageIndex-1)*pageSize);
		paramMap.put(CommonUtil.END, pageSize);
		List<Map<String, Object>> list = reportDAO.product_run_list(paramMap);
		PageData<Map<String, Object>> pageData = new PageData<Map<String, Object>>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	private String formatWeekDay(String date){
		String week = DateUtil.getWeek(date);
		if("1".equals(week)){
			return "星期一";
		}else if("2".equals(week)){
			return "星期二";
		}else if("3".equals(week)){
			return "星期三";
		}else if("4".equals(week)){
			return "星期四";
		}else if("5".equals(week)){
			return "星期五";
		}else if("6".equals(week)){
			return "星期六";
		}else if("7".equals(week)){
			return "星期日";
		}
		return "";
	}
	
}

