package zy.service.report.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.report.EmpReportDAO;
import zy.dto.base.emp.EmpPerformanceDto;
import zy.dto.base.emp.EmpSortDto;
import zy.service.report.EmpReportService;
import zy.util.StringUtil;

@Service
public class EmpReportServiceImpl implements EmpReportService{
	@Resource
	private EmpReportDAO empReportDAO;
	
	@Override
	public Double loadEmpSellMoney(Map<String, Object> params){
		Map<String, Object> empSellMap = empReportDAO.querySellByEmp(params);
		double money = 0d;
		if (empSellMap != null && !empSellMap.isEmpty()) {
			if(StringUtil.isNotEmpty(empSellMap.get("shl_money"))){
				money = Double.parseDouble(empSellMap.get("shl_money").toString());
			}
		}
		return money;
	}
	
	@Override
	public List<EmpPerformanceDto> loadPerformanceByEmp(Map<String, Object> params) {
		Integer empSort = empReportDAO.queryEmpSort(params);
		int comeAmount = empReportDAO.countCome(params);
		Map<String, Object> empSellMap = empReportDAO.querySellByEmp(params);
		int amount = 0;
		double money = 0d;
		double sellMoney = 0d;
		if (empSellMap != null && !empSellMap.isEmpty()) {
			if(StringUtil.isNotEmpty(empSellMap.get("shl_amount"))){
				amount = Integer.parseInt(empSellMap.get("shl_amount").toString());
			}
			if(StringUtil.isNotEmpty(empSellMap.get("shl_money"))){
				money = Double.parseDouble(empSellMap.get("shl_money").toString());
			}
			if(StringUtil.isNotEmpty(empSellMap.get("shl_sellmoney"))){
				sellMoney = Double.parseDouble(empSellMap.get("shl_sellmoney").toString());
			}
		}
		int recevieAmount = empReportDAO.countReceiveByEmp(params);
		int tryAmount = empReportDAO.countTryByEmp(params);
		int sellDealCount = 0;
		int backDealCount = 0;
		Map<String, Object> dealCountMap = empReportDAO.queryDealCountByEmp(params);
		if (dealCountMap != null && !dealCountMap.isEmpty()) {
			if(StringUtil.isNotEmpty(dealCountMap.get("sell_count"))){
				sellDealCount = Integer.parseInt(dealCountMap.get("sell_count").toString());
			}
			if(StringUtil.isNotEmpty(dealCountMap.get("return_count"))){
				backDealCount = Integer.parseInt(dealCountMap.get("return_count").toString());
			}
		}
		int dealCount = sellDealCount - backDealCount;
		int vipCount = empReportDAO.countVipByEmp(params);
		
		List<EmpPerformanceDto> performanceDtos = new ArrayList<EmpPerformanceDto>();
		performanceDtos.add(new EmpPerformanceDto("业绩排名", String.valueOf(empSort)));
		performanceDtos.add(new EmpPerformanceDto("进店量", String.valueOf(comeAmount)));
		performanceDtos.add(new EmpPerformanceDto("销售数量", String.valueOf(amount)));
		performanceDtos.add(new EmpPerformanceDto("销售额", StringUtil.doubleRtTwo(money)));
		performanceDtos.add(new EmpPerformanceDto("新增会员", String.valueOf(vipCount)));
		if(comeAmount != 0){//接待率=接待人数/进店量；试穿率=试穿人数/进店量
			performanceDtos.add(new EmpPerformanceDto("接待率", StringUtil.doubleRtTwo((double)recevieAmount/comeAmount*100)+"%"));
			performanceDtos.add(new EmpPerformanceDto("试穿率", StringUtil.doubleRtTwo((double)tryAmount/comeAmount*100)+"%"));
		}else{
			performanceDtos.add(new EmpPerformanceDto("接待率", "0.00%"));
			performanceDtos.add(new EmpPerformanceDto("试穿率", "0.00%"));
		}
		if(recevieAmount != 0){//成交率=成交单数/接待人数
			performanceDtos.add(new EmpPerformanceDto("成交率", StringUtil.doubleRtTwo((double)dealCount/recevieAmount*100)+"%"));
		}else{
			performanceDtos.add(new EmpPerformanceDto("成交率", "0.00%"));
		}
		if(dealCount != 0){//连带率=销售数量/成交单数
			performanceDtos.add(new EmpPerformanceDto("连带率", StringUtil.doubleRtTwo((double)amount/dealCount)));
		}else{
			performanceDtos.add(new EmpPerformanceDto("连带率", "0.00"));
		}
		if(sellMoney != 0){//平均折扣率=销售金额/零售金额
			performanceDtos.add(new EmpPerformanceDto("平均折扣率", StringUtil.doubleRtTwo(money/sellMoney*100)+"%"));
		}else{
			performanceDtos.add(new EmpPerformanceDto("平均折扣率", "0.00%"));
		}
		if (sellDealCount + backDealCount != 0) {//退单率
			performanceDtos.add(new EmpPerformanceDto("退单率", StringUtil.doubleRtTwo((double)backDealCount / (sellDealCount + backDealCount)*100)+"%"));
		} else {
			performanceDtos.add(new EmpPerformanceDto("退单率", "0.00%"));
		}
		if(amount != 0){//平均单价
			performanceDtos.add(new EmpPerformanceDto("平均单价", StringUtil.doubleRtTwo(money/amount)));
		}else{
			performanceDtos.add(new EmpPerformanceDto("平均单价", "0.00"));
		}
		if(dealCount != 0){//客单价
			performanceDtos.add(new EmpPerformanceDto("客单价", StringUtil.doubleRtTwo(money/dealCount)));
		}else{
			performanceDtos.add(new EmpPerformanceDto("客单价", "0.00"));
		}
		return performanceDtos;
	}
	
	@Override
	public List<EmpSortDto> listEmpSort(Map<String, Object> params) {
		List<EmpSortDto> sortDtos = empReportDAO.listEmpSort(params);
		List<EmpSortDto> dealCountDtos = empReportDAO.listEmpDealCount(params);
		Map<String, Integer> dealCountMap = new HashMap<String, Integer>();
		for (EmpSortDto item : dealCountDtos) {
			dealCountMap.put(item.getEm_code(), item.getDealcount());
		}
		for (EmpSortDto item : sortDtos) {
			if(dealCountMap.containsKey(item.getEm_code())){
				item.setDealcount(dealCountMap.get(item.getEm_code()));
			}else {
				item.setDealcount(0);
			}
		}
		return sortDtos;
	}
	
}
