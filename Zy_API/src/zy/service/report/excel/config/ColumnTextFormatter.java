package zy.service.report.excel.config;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;

public abstract class ColumnTextFormatter<T> implements ColumnCallback<T> {

	@Override
	public boolean canProcess(int row, T t, Row dataRow, int column, Object value, String name, Cell dataCell) {
		if (name.equalsIgnoreCase(column())) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void process(int row, T t, Row dataRow, int column, Object value, String name, Cell dataCell) {
		Object _value = value(value);
		setValue(row, t, dataRow, column, _value, name, dataCell);
	}

	public abstract String column();

	public abstract Object value(Object value);

	public void setValue(int row, T t, Row dataRow, int column, Object value, String name, Cell dataCell) {
		if (value != null) {
			dataCell.setCellType(CellType.STRING);
			dataCell.setCellValue(value.toString());
		}
	}
}