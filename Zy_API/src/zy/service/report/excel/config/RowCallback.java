package zy.service.report.excel.config;

import org.apache.poi.ss.usermodel.Row;

public interface RowCallback<T> {
	public boolean canProcess(int row, T t, Row dataRow);

	public void process(int row, T t, Row dataRow);
}