package zy.service.report.excel.config;

import org.apache.poi.ss.usermodel.Workbook;

public interface ReportConfigInterface<T> {
	public T getDatas();

	public void write(Workbook workbook);
}