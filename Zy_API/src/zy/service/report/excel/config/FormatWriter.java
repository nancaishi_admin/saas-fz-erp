package zy.service.report.excel.config;

import java.util.List;

import org.apache.poi.ss.usermodel.Sheet;

public interface FormatWriter<T> {
	public void writeTitle(Sheet sheet, String title, String[] label, String[] name, List<T> data);

	public void writeHead(Sheet sheet, String title, String[] label, String[] name, List<T> data);

	public void writeData(Sheet sheet, String title, String[] label, String[] name, List<T> data);
	
	public FormatWriter<T> addRowCallback(RowCallback<T> rowCallback);
	
	public FormatWriter<T> addColumnCallback(ColumnCallback<T> columnCallback);
}