package zy.service.report.excel.config;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

public interface ColumnCallback<T> {
	public boolean canProcess(int row, T t, Row dataRow, int column, Object value, String name, Cell dataCell);

	public void process(int row, T t, Row dataRow, int column, Object value, String name, Cell dataCell);
}