package zy.service.report.excel.reporter;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import zy.entity.batch.sell.T_Batch_Sell;
import zy.service.batch.sell.BatchSellService;
import zy.service.report.excel.config.BeanWriter;
import zy.service.report.excel.config.ColumnTextFormatter;
import zy.service.report.excel.config.ReportConfigInterface;
import zy.service.report.excel.config.SingleSheetBeanReportConfig;

/**
 * @author: miaosai
 * @date:2018年4月30日 下午7:05:26
 * 
 */
@Component
@SuppressWarnings("unchecked")
@Scope("prototype")
public class BatchSellReporter extends AbstractFDAReport<List<T_Batch_Sell>>{
	@Resource
	private BatchSellService batchSellService;

	@Override
	protected String setFileName() {
		return "批发单" + time;
	}

	@Override
	public ReportConfigInterface<List<T_Batch_Sell>> getReportConfigInterface() {
		BeanWriter<T_Batch_Sell> beanWriter = new BeanWriter<T_Batch_Sell>(T_Batch_Sell.class);
		
		beanWriter.addColumnCallback(new ColumnTextFormatter<T_Batch_Sell>() {
			@Override
			public String column() {
				return "se_ar_state";
			}
			@Override
			public Object value(Object value) {
				if ("0".equals(value.toString())) {
					return "未审核";
				} else if ("1".equals(value.toString())) {
					return "已审核";
				} else if ("2".equals(value.toString())) {
					return "已退回";
				} else {
					return "";
				}
			}
		});

		return new SingleSheetBeanReportConfig<T_Batch_Sell>(beanWriter) {
			@Override
			public String getSheet() {
				return "批发单";
			}

			@Override
			public String getTitle() {
				return "批发单";
			}
			

			@Override
			public String[] getLabel() {
				return new String[] { "单据编号", "订单编号", "批发客户", "客户店铺", "仓库", "经办人", "总计数量", "批发金额", "优惠金额", "返点金额", "物流金额", "应收金额", 
						"零售金额", "审核状态", "审核日期","制单日期","制单人"};
			}
			
			
			@Override
			public String[] getName() {
				return new String[] { "se_number", "se_order_number", "client_name", "client_shop_name", "depot_name", "se_manager", "se_amount", 
						"se_money", "se_discount_money", "se_rebatemoney", "se_stream_money", "se_receivable", "se_retailmoney", "se_ar_state",
						"se_ar_date","se_make_date","se_maker"};
			}

			@Override
			public List<T_Batch_Sell> getData() {
				return batchSellService.listExport(map);
			}
		};
	}
}
