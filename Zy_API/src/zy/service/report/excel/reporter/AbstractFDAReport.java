package zy.service.report.excel.reporter;

import javax.annotation.Resource;

import zy.dao.BaseDao;
import zy.service.report.excel.config.AbstractConfigReport;

public abstract class AbstractFDAReport<T> extends AbstractConfigReport<T> {
	@Resource
	protected BaseDao baseDaoImpl;

	@Override
	protected void initSelf() {
		super.initSelf();
	}
}