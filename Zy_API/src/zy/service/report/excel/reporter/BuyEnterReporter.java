package zy.service.report.excel.reporter;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import zy.entity.buy.enter.T_Buy_Enter;
import zy.service.buy.enter.EnterService;
import zy.service.report.excel.config.BeanWriter;
import zy.service.report.excel.config.ColumnTextFormatter;
import zy.service.report.excel.config.ReportConfigInterface;
import zy.service.report.excel.config.SingleSheetBeanReportConfig;

/**
 * @author: miaosai
 * @date:2018年5月3日 下午9:42:11
 * 
 */
@Component
@SuppressWarnings("unchecked")
@Scope("prototype")
public class BuyEnterReporter extends AbstractFDAReport<List<T_Buy_Enter>>{
	@Resource
	private EnterService enterService;

	@Override
	protected String setFileName() {
		return "采购单" + time;
	}

	@Override
	public ReportConfigInterface<List<T_Buy_Enter>> getReportConfigInterface() {
		BeanWriter<T_Buy_Enter> beanWriter = new BeanWriter<T_Buy_Enter>(T_Buy_Enter.class);
		
		beanWriter.addColumnCallback(new ColumnTextFormatter<T_Buy_Enter>() {
			@Override
			public String column() {
				return "et_ar_state";
			}
			@Override
			public Object value(Object value) {
				if ("0".equals(value.toString())) {
					return "未审核";
				} else if ("1".equals(value.toString())) {
					return "已审核";
				} else if ("2".equals(value.toString())) {
					return "已退回";
				} else {
					return "";
				}
			}
		});

		return new SingleSheetBeanReportConfig<T_Buy_Enter>(beanWriter) {
			@Override
			public String getSheet() {
				return "采购单";
			}

			@Override
			public String getTitle() {
				return "采购单";
			}

			@Override
			public String[] getLabel() {
				return new String[] { "单据编号", "订单编号", "供货厂商", "仓库", "经办人", "总计数量", "进货金额", "零售金额", "审核状态","审核人", "审核日期","制单日期","制单人"};
			}
			
			
			@Override
			public String[] getName() {
				return new String[] { "et_number", "et_order_number", "supply_name", "depot_name", "et_manager", "et_amount", 
						"et_money", "et_retailmoney", "et_ar_state", "et_ar_name", "et_ar_date", "et_make_date", "et_maker"};
			}

			@Override
			public List<T_Buy_Enter> getData() {
				return enterService.listExport(map);
			}
		};
	}
}
