package zy.service.report.excel;

import java.io.File;

public interface ReportFileInterface {
	public File getFile();
}