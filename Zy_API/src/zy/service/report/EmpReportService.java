package zy.service.report;

import java.util.List;
import java.util.Map;

import zy.dto.base.emp.EmpPerformanceDto;
import zy.dto.base.emp.EmpSortDto;

public interface EmpReportService {
	Double loadEmpSellMoney(Map<String, Object> params);
	List<EmpPerformanceDto> loadPerformanceByEmp(Map<String, Object> params);
	List<EmpSortDto> listEmpSort(Map<String, Object> params);
}
