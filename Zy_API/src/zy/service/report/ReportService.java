package zy.service.report;

import java.util.List;
import java.util.Map;

import zy.dto.report.kpi.KpiAnalysisDayDto;
import zy.dto.report.kpi.KpiAnalysisMonthDto;
import zy.entity.PageData;
import zy.entity.sell.cash.T_Sell_ShopList;

public interface ReportService {
	PageData<T_Sell_ShopList> sell_max(Map<String,Object> paramMap);
	PageData<T_Sell_ShopList> sell_max_list(Map<String,Object> paramMap);
	List<Map<String,Object>> sell_compare(Map<String,Object> paramMap);
	PageData<Map<String,Object>> sellStock(Map<String,Object> paramMap);
	Map<String, Object> kpi_analysis(Map<String, Object> params);
	List<KpiAnalysisMonthDto> kpi_analysis_month(Map<String, Object> params);
	List<KpiAnalysisDayDto> kpi_analysis_day(Map<String, Object> params);
	List<Map<String,Object>> brand_run(Map<String,Object> param);
	PageData<Map<String,Object>> product_run(Map<String,Object> param);
	
}
