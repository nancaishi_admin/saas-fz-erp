package zy.service.common.log;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.common.log.Common_Log;

public interface CommonLogService {
	PageData<Common_Log> page(Map<String, Object> params);
	List<Common_Log> list();
}
