package zy.service.common.log.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.common.log.CommonLogDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.common.log.Common_Log;
import zy.service.common.log.CommonLogService;
import zy.util.CommonUtil;

@Service
public class CommonLogServiceImpl implements CommonLogService{
	@Resource
	private CommonLogDAO commonLogDAO;
	
	@Override
	public PageData<Common_Log> page(Map<String, Object> params) {
		Object pageSize = params.get(CommonUtil.PAGESIZE);
		Object pageIndex = params.get(CommonUtil.PAGEINDEX);
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = commonLogDAO.count(params);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		params.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		params.put(CommonUtil.END, _pageSize);
		
		List<Common_Log> list = commonLogDAO.list(params);
		PageData<Common_Log> pageData = new PageData<Common_Log>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	
	@Override
	public List<Common_Log> list() {
		return commonLogDAO.list();
	}
	
}
