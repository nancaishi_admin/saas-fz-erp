package zy.service.common.dict;

import java.util.List;

import zy.entity.common.dict.Common_Dict;

public interface CommonDictService {
	List<Common_Dict> list(String dt_type);
}
