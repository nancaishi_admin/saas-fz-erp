package zy.service.batch.dealings;

import java.util.Map;

import zy.entity.PageData;
import zy.entity.batch.dealings.T_Batch_Dealings;

public interface BatchDealingsService {
	PageData<T_Batch_Dealings> page(Map<String, Object> params);
}
