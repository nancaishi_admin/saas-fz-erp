package zy.service.batch.dealings.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.batch.dealings.BatchDealingsDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.batch.dealings.T_Batch_Dealings;
import zy.service.batch.dealings.BatchDealingsService;
import zy.util.CommonUtil;

@Service
public class BatchDealingsServiceImpl implements BatchDealingsService{
	@Resource
	private BatchDealingsDAO batchDealingsDAO;
	
	@Override
	public PageData<T_Batch_Dealings> page(Map<String, Object> params) {
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		
		Integer totalCount = batchDealingsDAO.count(params);
		PageInfo pageInfo = new PageInfo(totalCount, pageSize, pageIndex);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		
		List<T_Batch_Dealings> list = batchDealingsDAO.list(params);
		PageData<T_Batch_Dealings> pageData = new PageData<T_Batch_Dealings>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	
}
