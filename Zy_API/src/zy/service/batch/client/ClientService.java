package zy.service.batch.client;

import java.util.List;
import java.util.Map;

import zy.dto.batch.money.ClientBackAnalysisDto;
import zy.dto.batch.money.ClientMoneyDetailsDto;
import zy.entity.PageData;
import zy.entity.batch.client.T_Batch_Client;
import zy.entity.batch.client.T_Batch_Client_Shop;
import zy.entity.sys.user.T_Sys_User;

public interface ClientService {
	PageData<T_Batch_Client> page(Map<String,Object> params);
	PageData<T_Batch_Client> page4dialog(Map<String, Object> params);
	T_Batch_Client load(Integer ci_id);
	void save(T_Batch_Client client, T_Sys_User user);
	void update(T_Batch_Client client, T_Sys_User user);
	void del(Integer ci_id, String ci_code,Integer companyid);
	PageData<ClientMoneyDetailsDto> pageMoneyDetails(Map<String, Object> params);
	List<ClientBackAnalysisDto> listBackAnalysis(Map<String, Object> params);
	List<T_Batch_Client_Shop> shop_list(Map<String,Object> params);
	void save_client_shop(T_Batch_Client_Shop client_Shop, T_Sys_User user);
	void del_client_shop(Map<String,Object> params);
	T_Batch_Client_Shop load_shop(Integer cis_id);
	void update_client_shop(T_Batch_Client_Shop client_Shop, T_Sys_User user);
}
