package zy.service.batch.settle;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.approve.T_Approve_Record;
import zy.entity.batch.client.T_Batch_Client;
import zy.entity.batch.settle.T_Batch_Settle;
import zy.entity.batch.settle.T_Batch_SettleList;
import zy.entity.sys.user.T_Sys_User;

public interface BatchSettleService {
	PageData<T_Batch_Settle> page(Map<String, Object> params);
	T_Batch_Settle load(Integer st_id);
	T_Batch_Settle load(String number, Integer companyid);
	T_Batch_Client loadClient(String ci_code,Integer companyid);
	List<T_Batch_SettleList> temp_list(Map<String, Object> params);
	void temp_save(String ci_code,T_Sys_User user);
	void temp_updateDiscountMoney(T_Batch_SettleList temp);
	void temp_updatePrepay(T_Batch_SettleList temp);
	void temp_updateRealMoney(T_Batch_SettleList temp);
	void temp_updateRemark(T_Batch_SettleList temp);
	void temp_updateJoin(String ids,String stl_join);
	Map<String, Object> temp_entire(Map<String, Object> params);
	List<T_Batch_SettleList> detail_list(String number,Integer companyid);
	void save(T_Batch_Settle settle, T_Sys_User user);
	void update(T_Batch_Settle settle, T_Sys_User user);
	T_Batch_Settle approve(String number, T_Approve_Record record, T_Sys_User user);
	T_Batch_Settle reverse(String number, T_Sys_User user);
	void initUpdate(String number, Integer us_id, Integer companyid);
	void del(String number, Integer companyid);
	Map<String, Object> loadPrintData(String number, Integer sp_id, T_Sys_User user);
}
