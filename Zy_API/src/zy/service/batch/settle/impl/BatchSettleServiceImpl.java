package zy.service.batch.settle.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import zy.dao.approve.ApproveRecordDAO;
import zy.dao.batch.client.ClientDAO;
import zy.dao.batch.dealings.BatchDealingsDAO;
import zy.dao.batch.prepay.BatchPrepayDAO;
import zy.dao.batch.settle.BatchSettleDAO;
import zy.dao.money.bank.BankDAO;
import zy.dao.money.bank.BankRunDAO;
import zy.dao.sys.print.PrintDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.approve.T_Approve_Record;
import zy.entity.batch.client.T_Batch_Client;
import zy.entity.batch.dealings.T_Batch_Dealings;
import zy.entity.batch.fee.T_Batch_Fee;
import zy.entity.batch.prepay.T_Batch_Prepay;
import zy.entity.batch.sell.T_Batch_Sell;
import zy.entity.batch.settle.T_Batch_Settle;
import zy.entity.batch.settle.T_Batch_SettleList;
import zy.entity.money.bank.T_Money_Bank;
import zy.entity.money.bank.T_Money_BankRun;
import zy.entity.sys.user.T_Sys_User;
import zy.service.batch.settle.BatchSettleService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;

@Service
public class BatchSettleServiceImpl implements BatchSettleService{
	@Resource
	private BatchSettleDAO batchSettleDAO;
	@Resource
	private ApproveRecordDAO approveRecordDAO;
	@Resource
	private BatchDealingsDAO batchDealingsDAO;
	@Resource
	private BatchPrepayDAO batchPrepayDAO;
	@Resource
	private ClientDAO clientDAO;
	@Resource
	private BankDAO bankDAO;
	@Resource
	private BankRunDAO bankRunDAO;
	@Resource
	private PrintDAO printDAO;
	
	
	@Override
	public PageData<T_Batch_Settle> page(Map<String, Object> params) {
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		
		Integer totalCount = batchSettleDAO.count(params);
		PageInfo pageInfo = new PageInfo(totalCount, pageSize, pageIndex);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		
		List<T_Batch_Settle> list = batchSettleDAO.list(params);
		PageData<T_Batch_Settle> pageData = new PageData<T_Batch_Settle>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	
	@Override
	public T_Batch_Settle load(Integer st_id) {
		T_Batch_Settle settle = batchSettleDAO.load(st_id);
		if(settle != null){
			T_Approve_Record approve_Record = approveRecordDAO.load(settle.getSt_number(), settle.getCompanyid());
			if(approve_Record != null){
				settle.setAr_describe(approve_Record.getAr_describe());
			}
		}
		return settle;
	}
	
	@Override
	public T_Batch_Settle load(String number, Integer companyid) {
		return batchSettleDAO.load(number, companyid);
	}

	@Override
	public T_Batch_Client loadClient(String ci_code,Integer companyid) {
		return clientDAO.loadClient(ci_code, companyid);
	}
	
	@Override
	public List<T_Batch_SettleList> temp_list(Map<String, Object> params) {
		return batchSettleDAO.temp_list(params);
	}

	@Override
	@Transactional
	public void temp_save(String ci_code, T_Sys_User user) {
		Assert.hasText(ci_code,"请选择客户");
		T_Batch_Settle existSettle = batchSettleDAO.check_settle(ci_code, user.getCompanyid());
		if(existSettle != null){
			throw new RuntimeException("该客户有单据未审核，不能再结算!");
		}
		batchSettleDAO.temp_clear(user.getUs_id(), user.getCompanyid());
		List<T_Batch_SettleList> temps = batchSettleDAO.load_batch_forsavetemp(ci_code, user.getCompanyid());
		for (T_Batch_SettleList temp : temps) {
			temp.setStl_join(1);
			temp.setStl_us_id(user.getUs_id());
			temp.setCompanyid(user.getCompanyid());
		}
		batchSettleDAO.temp_save(temps);
	}

	@Override
	@Transactional
	public void temp_updateDiscountMoney(T_Batch_SettleList temp) {
		batchSettleDAO.temp_updateDiscountMoney(temp);
	}

	@Override
	@Transactional
	public void temp_updatePrepay(T_Batch_SettleList temp) {
		batchSettleDAO.temp_updatePrepay(temp);
	}

	@Override
	@Transactional
	public void temp_updateRealMoney(T_Batch_SettleList temp) {
		batchSettleDAO.temp_updateRealMoney(temp);
	}

	@Override
	@Transactional
	public void temp_updateRemark(T_Batch_SettleList temp) {
		batchSettleDAO.temp_updateRemark(temp);
	}

	@Override
	@Transactional
	public void temp_updateJoin(String ids, String stl_join) {
		batchSettleDAO.temp_updateJoin(ids, stl_join);
	}
	
	@Override
	@Transactional
	public Map<String, Object> temp_entire(Map<String, Object> params) {
		Double discount_money = (Double)params.get("discount_money");
		Double prepay = (Double)params.get("prepay");
		Double received = (Double)params.get("received");
		T_Sys_User user = (T_Sys_User)params.get("user");
		if(discount_money == null || prepay == null || received == null){
			throw new RuntimeException("整单结算金额不能为空");
		}
		if (discount_money.doubleValue() == 0d && prepay.doubleValue() == 0d
				&& received.doubleValue() == 0d) {
			throw new RuntimeException("整单结算金额不能为0");
		}
		params.put("stl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		List<T_Batch_SettleList> temps = batchSettleDAO.temp_list(params);
		
		double remained_discount_money = discount_money;
		double remained_prepay = prepay;
		double remained_received = received;
		for (T_Batch_SettleList temp : temps) {
			if (CommonUtil.BATCHDEALINGS_TYPE_TH.equals(temp.getStl_type())//退货单
					|| (CommonUtil.BATCHDEALINGS_TYPE_QC.equals(temp.getStl_type()) && temp.getStl_unreceivable() < 0)//负的期初单
					|| (CommonUtil.BATCHDEALINGS_TYPE_FY.equals(temp.getStl_type()) && temp.getStl_unreceivable() < 0)) {//负的费用单
				double tempMoney = temp.getStl_unreceivable();
				temp.setStl_discount_money(0d);
				temp.setStl_prepay(0d);
				temp.setStl_real_received(tempMoney);
				temp.setStl_join(1);
				remained_received += Math.abs(tempMoney);
				continue;
			}
			
			Double temp_discount_money = null;
			Double temp_prepay = null;
			Double temp_received = null;
			
			// 使用优惠金额
			if (remained_discount_money > 0) {
				temp_discount_money = Math.min(remained_discount_money,temp.getStl_unreceivable());
				remained_discount_money -= temp_discount_money;
			}else{
				temp_discount_money = 0d;
			}
			temp.setStl_discount_money(temp_discount_money);//设置本次使用优惠金额
			//使用预付款金额
			if(remained_prepay > 0){
				temp_prepay = Math.min(remained_prepay, temp.getStl_unreceivable()-temp.getStl_discount_money());
				remained_prepay -= temp_prepay;
			}else{
				temp_prepay = 0d;
			}
			temp.setStl_prepay(temp_prepay);//设置本次使用预收款金额
			
			//使用实收金额
			if(remained_received > 0){
				temp_received = Math.min(remained_received, temp.getStl_unreceivable()-temp.getStl_discount_money()-temp.getStl_prepay());
				remained_received -= temp_received;
			}else{
				temp_received = 0d;
			}
			temp.setStl_real_received(temp_received);
			if (temp_discount_money != 0 || temp_prepay != 0 || temp_received != 0) {
				temp.setStl_join(1);
			}else {
				if (temp.getStl_unreceivable() == 0) {
					temp.setStl_join(1);
				}else {
					temp.setStl_join(0);
				}
			}
		}
		batchSettleDAO.temp_update(temps);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("st_receivedmore", remained_discount_money+remained_prepay+remained_received);
		return resultMap;
	}
	
	@Override
	public List<T_Batch_SettleList> detail_list(String number,Integer companyid) {
		return batchSettleDAO.detail_list(number,companyid);
	}
	
	@Override
	@Transactional
	public void save(T_Batch_Settle settle, T_Sys_User user) {
		if(settle == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		if(StringUtil.isEmpty(settle.getSt_client_code())){
			throw new IllegalArgumentException("批发客户不能为空");
		}
		if(StringUtil.isEmpty(settle.getSt_ba_code())){
			throw new IllegalArgumentException("银行账户不能为空");
		}
		if(StringUtil.isEmpty(settle.getSt_manager())){
			throw new IllegalArgumentException("经办人不能为空");
		}
		settle.setCompanyid(user.getCompanyid());
		settle.setSt_us_id(user.getUs_id());
		settle.setSt_maker(user.getUs_name());
		settle.setSt_ar_state(CommonUtil.AR_STATE_NOTAPPROVE);
		settle.setSt_sysdate(DateUtil.getCurrentTime());
		//1.查临时表
		List<T_Batch_SettleList> temps = batchSettleDAO.temp_list_forsave(user.getUs_id(),user.getCompanyid());
		if(temps == null || temps.size() == 0){
			throw new RuntimeException("单据已保存，请勿重复提交");
		}
		//2.保存单据
		double st_discount_money = 0d;
		double st_prepay = 0d;
		double st_received = 0d;
		for (T_Batch_SettleList temp : temps) {
			st_discount_money += temp.getStl_discount_money();
			st_prepay += temp.getStl_prepay();
			st_received += temp.getStl_real_received();
		}
		settle.setSt_discount_money(st_discount_money);
		settle.setSt_prepay(st_prepay);
		settle.setSt_received(st_received);
		if (settle.getSt_entire().intValue() == 1) {
			if (settle.getSt_receivedmore() == null || settle.getSt_receivedmore() < 0) {
				settle.setSt_receivedmore(0d);
			}
		} else {
			settle.setSt_receivedmore(0d);
		}
		batchSettleDAO.save(settle, temps);
		//3.删除临时表
		batchSettleDAO.temp_clear(user.getUs_id(), user.getCompanyid());
	}
	
	@Override
	@Transactional
	public void update(T_Batch_Settle settle, T_Sys_User user) {
		if(settle == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		if(StringUtil.isEmpty(settle.getSt_client_code())){
			throw new IllegalArgumentException("批发客户不能为空");
		}
		if(StringUtil.isEmpty(settle.getSt_ba_code())){
			throw new IllegalArgumentException("银行账户不能为空");
		}
		if(StringUtil.isEmpty(settle.getSt_manager())){
			throw new IllegalArgumentException("经办人不能为空");
		}
		
		settle.setCompanyid(user.getCompanyid());
		settle.setSt_us_id(user.getUs_id());
		settle.setSt_maker(user.getUs_name());
		settle.setSt_ar_state(CommonUtil.AR_STATE_NOTAPPROVE);
		settle.setSt_ar_date(null);
		settle.setSt_sysdate(DateUtil.getCurrentTime());
		//1.1查临时表
		List<T_Batch_SettleList> temps = batchSettleDAO.temp_list_forsave(user.getUs_id(),user.getCompanyid());
		if(temps == null || temps.size() == 0){
			throw new RuntimeException("单据已保存，请勿重复提交");
		}
		//1.2验证单据
		T_Batch_Settle oldSettle = batchSettleDAO.check(settle.getSt_number(), user.getCompanyid());
		if (oldSettle == null || !CommonUtil.AR_STATE_FAIL.equals(oldSettle.getSt_ar_state())) {
			throw new RuntimeException("单据已修改，请勿重复提交");
		}
		//2.保存单据
		double st_discount_money = 0d;
		double st_prepay = 0d;
		double st_received = 0d;
		for (T_Batch_SettleList temp : temps) {
			st_discount_money += temp.getStl_discount_money();
			st_prepay += temp.getStl_prepay();
			st_received += temp.getStl_real_received();
		}
		settle.setSt_discount_money(st_discount_money);
		settle.setSt_prepay(st_prepay);
		settle.setSt_received(st_received);
		if (settle.getSt_entire().intValue() == 1) {
			if (settle.getSt_receivedmore() == null || settle.getSt_receivedmore() < 0) {
				settle.setSt_receivedmore(0d);
			}
		} else {
			settle.setSt_receivedmore(0d);
		}
		batchSettleDAO.deleteList(settle.getSt_number(), user.getCompanyid());
		batchSettleDAO.update(settle, temps);
		//3.删除临时表
		batchSettleDAO.temp_clear(user.getUs_id(), user.getCompanyid());
	}
	
	@Override
	@Transactional
	public T_Batch_Settle approve(String number, T_Approve_Record record, T_Sys_User user) {
		if (number == null) {
			throw new IllegalArgumentException("参数number不能为null");
		}
		T_Batch_Settle settle = batchSettleDAO.check(number, user.getCompanyid());
		if(settle == null){
			throw new RuntimeException("单据不存在");
		}
		if(!CommonUtil.AR_STATE_NOTAPPROVE.equals(settle.getSt_ar_state())){
			throw new RuntimeException("单据已经审核");
		}
		//更新单据审核状态
		settle.setSt_ar_state(record.getAr_state());
		settle.setSt_ar_date(DateUtil.getCurrentTime());
		batchSettleDAO.updateApprove(settle);
		//保存审核记录表
		record.setAr_number(number);
		record.setAr_sysdate(DateUtil.getCurrentTime());
		record.setAr_us_name(user.getUs_name());
		record.setAr_type("t_batch_settle");
		record.setCompanyid(user.getCompanyid());
		approveRecordDAO.save(record);
		if(!CommonUtil.AR_STATE_APPROVED.equals(settle.getSt_ar_state())){//审核不通过，则直接返回
			return settle;
		}
		//3.审核通过
		//3.1更新批发客户财务&生成往来明细账
		T_Batch_Client client = clientDAO.loadClient(settle.getSt_client_code(), user.getCompanyid());
		client.setCi_receivable(client.getCi_receivable()-settle.getSt_discount_money());
		client.setCi_received(client.getCi_received()+settle.getSt_prepay()+settle.getSt_received());
		client.setCi_prepay(client.getCi_prepay()-settle.getSt_prepay()+settle.getSt_receivedmore());
		clientDAO.updateSettle(client);
		if (settle.getSt_receivedmore() > 0) {//生成预收款单
			T_Batch_Prepay prepay = new T_Batch_Prepay();
			prepay.setPp_date(DateUtil.getCurrentTime());
			prepay.setPp_client_code(settle.getSt_client_code());
			prepay.setPp_type(0);
			prepay.setPp_money(settle.getSt_receivedmore());
			prepay.setPp_maker(user.getUs_name());
			prepay.setPp_manager(settle.getSt_manager());
			prepay.setPp_ba_code(settle.getSt_ba_code());
			prepay.setPp_remark("客户结算单["+number+"]整单结算剩余金额转入本单据。");
			prepay.setPp_ar_state(CommonUtil.AR_STATE_APPROVED);
			prepay.setPp_ar_date(DateUtil.getCurrentTime());
			prepay.setPp_sysdate(DateUtil.getCurrentTime());
			prepay.setPp_us_id(user.getUs_id());
			prepay.setCompanyid(user.getCompanyid());
			batchPrepayDAO.save(prepay);;
			settle.setSt_pp_number(prepay.getPp_number());
			batchSettleDAO.updatePpNumber(settle);
		}
		T_Batch_Dealings dealings = new T_Batch_Dealings();
		dealings.setDl_client_code(settle.getSt_client_code());
		dealings.setDl_number(settle.getSt_number());
		dealings.setDl_type(CommonUtil.BATCHDEALINGS_TYPE_JS);
		dealings.setDl_discount_money(settle.getSt_discount_money());
		dealings.setDl_receivable(0d);
		dealings.setDl_received(settle.getSt_received());
		dealings.setDl_debt(client.getCi_receivable()-client.getCi_received()-client.getCi_prepay()+settle.getSt_receivedmore());
		dealings.setDl_date(DateUtil.getCurrentTime());
		dealings.setDl_manager(user.getUs_name());
		String dl_remark = settle.getSt_remark();
		if(settle.getSt_prepay() != 0){
			dl_remark = "(使用预收款金额："+String.format("%.2f", settle.getSt_prepay())+")";
		}
		dealings.setDl_remark(dl_remark);
		dealings.setDl_sysdate(DateUtil.getCurrentTime());
		dealings.setCompanyid(user.getCompanyid());
		batchDealingsDAO.save(dealings);
		if(StringUtil.isNotEmpty(settle.getSt_pp_number())){//生成预收款单明细账
			dealings = new T_Batch_Dealings();
			dealings.setDl_client_code(settle.getSt_client_code());
			dealings.setDl_number(settle.getSt_pp_number());
			dealings.setDl_type(CommonUtil.BATCHDEALINGS_TYPE_YSK);
			dealings.setDl_discount_money(0d);
			dealings.setDl_receivable(0d);
			dealings.setDl_received(settle.getSt_receivedmore());
			dealings.setDl_debt(client.getCi_receivable()-client.getCi_received()-client.getCi_prepay());
			dealings.setDl_date(DateUtil.getCurrentTime());
			dealings.setDl_manager(user.getUs_name());
			dealings.setDl_remark("");
			dealings.setDl_sysdate(DateUtil.getCurrentTime());
			dealings.setCompanyid(user.getCompanyid());
			batchDealingsDAO.save(dealings);
		}
		//更改批发销售单金额数据
		List<T_Batch_Sell> sells = batchSettleDAO.listSellBySettle(number, user.getCompanyid());
		if (sells.size() > 0) {
			for (T_Batch_Sell sell : sells) {
				double sum = Math.abs(sell.getSe_discount_money()+sell.getSe_prepay()+sell.getSe_received());
				if (sum == 0) {
					sell.setSe_pay_state(CommonUtil.PAY_STATE_UNPAY);
				} else if (sum < Math.abs(sell.getSe_money() - sell.getSe_rebatemoney() + sell.getSe_stream_money())) {
					sell.setSe_pay_state(CommonUtil.PAY_STATE_PART);
				} else {
					sell.setSe_pay_state(CommonUtil.PAY_STATE_FINISH);
				}
			}
			batchSettleDAO.updateSellBySettle(sells);
		}
		//更新客户费用单金额数据
		List<T_Batch_Fee> fees = batchSettleDAO.listFeeBySettle(number, user.getCompanyid());
		if (fees.size() > 0) {
			for (T_Batch_Fee fee : fees) {
				double sum = Math.abs(fee.getFe_discount_money()+fee.getFe_prepay()+fee.getFe_received());
				if (sum == 0) {
					fee.setFe_pay_state(CommonUtil.PAY_STATE_UNPAY);
				} else if (sum < Math.abs(fee.getFe_money())) {
					fee.setFe_pay_state(CommonUtil.PAY_STATE_PART);
				} else {
					fee.setFe_pay_state(CommonUtil.PAY_STATE_FINISH);
				}
			}
			batchSettleDAO.updateFeeBySettle(fees);
		}
		if(settle.getSt_received() != 0 || settle.getSt_receivedmore() > 0){//更改银行帐户余额
			T_Money_Bank bank = bankDAO.queryByCode(settle.getSt_ba_code(), user.getCompanyid());
			bank.setBa_balance(bank.getBa_balance()+settle.getSt_received()+settle.getSt_receivedmore());
			bankDAO.updateBalanceById(bank);
			if(settle.getSt_received() != 0){//客户结算单银行流水账
				T_Money_BankRun bankRun = new T_Money_BankRun();
				bankRun.setBr_ba_code(settle.getSt_ba_code());
				bankRun.setBr_balance(bank.getBa_balance()-settle.getSt_receivedmore());
				bankRun.setBr_bt_code(CommonUtil.BANKRUN_BATCHSETTLE);
				bankRun.setBr_date(DateUtil.getCurrentTime());
				if(settle.getSt_received()>0){
					bankRun.setBr_enter(Math.abs(settle.getSt_received()));
				}else {
					bankRun.setBr_out(Math.abs(settle.getSt_received()));
				}
				bankRun.setBr_manager(settle.getSt_manager());
				bankRun.setBr_number(number);
				bankRun.setBr_remark("客户结算单");
				bankRun.setBr_shop_code(bank.getBa_shop_code());
				bankRun.setBr_sysdate(DateUtil.getCurrentTime());
				bankRun.setCompanyid(user.getCompanyid());
				bankRunDAO.save(bankRun);
			}
			if(settle.getSt_receivedmore() > 0){//预收款银行流水账
				T_Money_BankRun bankRun = new T_Money_BankRun();
				bankRun.setBr_ba_code(settle.getSt_ba_code());
				bankRun.setBr_balance(bank.getBa_balance());
				bankRun.setBr_bt_code(CommonUtil.BANKRUN_BATCHPREPAY);
				bankRun.setBr_date(DateUtil.getCurrentTime());
				bankRun.setBr_enter(settle.getSt_receivedmore());
				bankRun.setBr_manager(settle.getSt_manager());
				bankRun.setBr_number(settle.getSt_pp_number());
				bankRun.setBr_remark("客户预收款");
				bankRun.setBr_shop_code(bank.getBa_shop_code());
				bankRun.setBr_sysdate(DateUtil.getCurrentTime());
				bankRun.setCompanyid(user.getCompanyid());
				bankRunDAO.save(bankRun);
			}
		}
		
		return settle;
	}
	
	@Override
	@Transactional
	public T_Batch_Settle reverse(String number, T_Sys_User user) {
		Assert.hasText(number, "参数number不能为null");
		T_Batch_Settle settle = batchSettleDAO.check(number, user.getCompanyid());
		if(settle == null){
			throw new RuntimeException("单据不存在");
		}
		if(!CommonUtil.AR_STATE_APPROVED.equals(settle.getSt_ar_state())){
			throw new RuntimeException("单据未审核或审核未通过");
		}
		
		//1.更新单据审核状态
		settle.setSt_ar_state(CommonUtil.AR_STATE_NOTAPPROVE);
		settle.setSt_ar_date(DateUtil.getCurrentTime());
		batchSettleDAO.updateApprove(settle);
		//2.保存审核记录表
		T_Approve_Record record = new T_Approve_Record();
		record.setAr_state(CommonUtil.AR_STATE_REVERSE_APPROVE);
		record.setAr_describe(user.getUs_name()+"反审核单据");
		record.setAr_number(number);
		record.setAr_sysdate(DateUtil.getCurrentTime());
		record.setAr_us_name(user.getUs_name());
		record.setAr_type("t_batch_settle");
		record.setCompanyid(user.getCompanyid());
		approveRecordDAO.save(record);
		
		//3更新批发客户财务&删除往来明细账
		T_Batch_Client client = clientDAO.loadClient(settle.getSt_client_code(), user.getCompanyid());
		client.setCi_receivable(client.getCi_receivable()+settle.getSt_discount_money());
		client.setCi_received(client.getCi_received()-settle.getSt_prepay()-settle.getSt_received());
		client.setCi_prepay(client.getCi_prepay()+settle.getSt_prepay()-settle.getSt_receivedmore());
		clientDAO.updateSettle(client);
		
		T_Batch_Dealings dealings = new T_Batch_Dealings();
		dealings.setDl_client_code(settle.getSt_client_code());
		dealings.setDl_number(settle.getSt_number());
		dealings.setDl_type(CommonUtil.BATCHDEALINGS_TYPE_JS);
		dealings.setDl_discount_money(-settle.getSt_discount_money());
		dealings.setDl_receivable(0d);
		dealings.setDl_received(-settle.getSt_received());
		dealings.setDl_debt(client.getCi_receivable()-client.getCi_received()-client.getCi_prepay()-settle.getSt_receivedmore());
		dealings.setDl_date(DateUtil.getCurrentTime());
		dealings.setDl_manager(user.getUs_name());
		dealings.setDl_remark("反审核恢复账目往来明细");
		dealings.setDl_sysdate(DateUtil.getCurrentTime());
		dealings.setCompanyid(user.getCompanyid());
		batchDealingsDAO.save(dealings);
		
		if(StringUtil.isNotEmpty(settle.getSt_pp_number())){//删除预收款单及恢复往来明细
			batchPrepayDAO.del(settle.getSt_pp_number(), user.getCompanyid());
			dealings = new T_Batch_Dealings();
			dealings.setDl_client_code(settle.getSt_client_code());
			dealings.setDl_number(settle.getSt_pp_number());
			dealings.setDl_type(CommonUtil.BATCHDEALINGS_TYPE_YSK);
			dealings.setDl_discount_money(0d);
			dealings.setDl_receivable(0d);
			dealings.setDl_received(-settle.getSt_receivedmore());
			dealings.setDl_debt(client.getCi_receivable()-client.getCi_received()-client.getCi_prepay());
			dealings.setDl_date(DateUtil.getCurrentTime());
			dealings.setDl_manager(user.getUs_name());
			dealings.setDl_remark("反审核恢复账目往来明细");
			dealings.setDl_sysdate(DateUtil.getCurrentTime());
			dealings.setCompanyid(user.getCompanyid());
			batchDealingsDAO.save(dealings);
		}
		
		//更改批发销售单金额数据
		List<T_Batch_Sell> sells = batchSettleDAO.listSellBySettle_Reverse(number, user.getCompanyid());
		if (sells.size() > 0) {
			for (T_Batch_Sell sell : sells) {
				double sum = Math.abs(sell.getSe_discount_money()+sell.getSe_prepay()+sell.getSe_received());
				if (sum == 0) {
					sell.setSe_pay_state(CommonUtil.PAY_STATE_UNPAY);
				} else if (sum < Math.abs(sell.getSe_money() - sell.getSe_rebatemoney() + sell.getSe_stream_money())) {
					sell.setSe_pay_state(CommonUtil.PAY_STATE_PART);
				} else {
					sell.setSe_pay_state(CommonUtil.PAY_STATE_FINISH);
				}
			}
			batchSettleDAO.updateSellBySettle(sells);
		}
		//更新客户费用单金额数据
		List<T_Batch_Fee> fees = batchSettleDAO.listFeeBySettle_Reverse(number, user.getCompanyid());
		if (fees.size() > 0) {
			for (T_Batch_Fee fee : fees) {
				double sum = Math.abs(fee.getFe_discount_money()+fee.getFe_prepay()+fee.getFe_received());
				if (sum == 0) {
					fee.setFe_pay_state(CommonUtil.PAY_STATE_UNPAY);
				} else if (sum < Math.abs(fee.getFe_money())) {
					fee.setFe_pay_state(CommonUtil.PAY_STATE_PART);
				} else {
					fee.setFe_pay_state(CommonUtil.PAY_STATE_FINISH);
				}
			}
			batchSettleDAO.updateFeeBySettle(fees);
		}
		
		if(settle.getSt_received() != 0 || settle.getSt_receivedmore() > 0){//更改银行帐户余额
			T_Money_Bank bank = bankDAO.queryByCode(settle.getSt_ba_code(), user.getCompanyid());
			bank.setBa_balance(bank.getBa_balance()-settle.getSt_received()-settle.getSt_receivedmore());
			bankDAO.updateBalanceById(bank);
			if(settle.getSt_received() != 0){//客户结算单银行流水账
				T_Money_BankRun bankRun = new T_Money_BankRun();
				bankRun.setBr_ba_code(settle.getSt_ba_code());
				bankRun.setBr_balance(bank.getBa_balance()+settle.getSt_receivedmore());
				bankRun.setBr_bt_code(CommonUtil.BANKRUN_BATCHSETTLE);
				bankRun.setBr_date(DateUtil.getCurrentTime());
				if(settle.getSt_received()>0){
					bankRun.setBr_out(Math.abs(settle.getSt_received()));
				}else {
					bankRun.setBr_enter(Math.abs(settle.getSt_received()));
				}
				bankRun.setBr_manager(settle.getSt_manager());
				bankRun.setBr_number(number);
				bankRun.setBr_remark("反审核恢复账目");
				bankRun.setBr_shop_code(bank.getBa_shop_code());
				bankRun.setBr_sysdate(DateUtil.getCurrentTime());
				bankRun.setCompanyid(user.getCompanyid());
				bankRunDAO.save(bankRun);
			}
			if(settle.getSt_receivedmore() > 0){//预收款银行流水账
				T_Money_BankRun bankRun = new T_Money_BankRun();
				bankRun.setBr_ba_code(settle.getSt_ba_code());
				bankRun.setBr_balance(bank.getBa_balance());
				bankRun.setBr_bt_code(CommonUtil.BANKRUN_BATCHPREPAY);
				bankRun.setBr_date(DateUtil.getCurrentTime());
				bankRun.setBr_out(settle.getSt_receivedmore());
				bankRun.setBr_manager(settle.getSt_manager());
				bankRun.setBr_number(settle.getSt_pp_number());
				bankRun.setBr_remark("反审核恢复账目");
				bankRun.setBr_shop_code(bank.getBa_shop_code());
				bankRun.setBr_sysdate(DateUtil.getCurrentTime());
				bankRun.setCompanyid(user.getCompanyid());
				bankRunDAO.save(bankRun);
			}
		}
		return settle;
	}
	
	@Override
	@Transactional
	public void initUpdate(String number, Integer us_id, Integer companyid) {
		List<T_Batch_SettleList> details = batchSettleDAO.detail_list(number, companyid);
		for(T_Batch_SettleList item:details){
			item.setStl_us_id(us_id);
			item.setStl_join(1);
		}
		batchSettleDAO.temp_clear(us_id, companyid);
		batchSettleDAO.temp_save(details);
	}
	
	@Override
	@Transactional
	public void del(String number, Integer companyid) {
		Assert.hasText(number, "参数number不能为null");
		T_Batch_Settle settle = batchSettleDAO.check(number, companyid);
		if(settle == null){
			throw new RuntimeException("单据不存在");
		}
		if(!CommonUtil.AR_STATE_NOTAPPROVE.equals(settle.getSt_ar_state()) && !CommonUtil.AR_STATE_FAIL.equals(settle.getSt_ar_state())){
			throw new RuntimeException("单据已审核通过不能删除！");
		}
		batchSettleDAO.del(number, companyid);
	}
	
	@Override
	public Map<String, Object> loadPrintData(String number, Integer sp_id, T_Sys_User user) {
		Map<String, Object> resultMap = printDAO.loadPrint4Bill(sp_id);
		T_Batch_Settle settle = batchSettleDAO.load(number, user.getCompanyid());
		List<T_Batch_SettleList> settleList = batchSettleDAO.detail_list(number, user.getCompanyid());
		resultMap.put("settle", settle);
		resultMap.put("settleList", settleList);
		resultMap.put("client", clientDAO.load(settle.getSt_client_code(), user.getCompanyid()));
		return resultMap;
	}
	
}
