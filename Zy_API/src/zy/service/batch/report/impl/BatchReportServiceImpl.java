package zy.service.batch.report.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import zy.dao.base.size.SizeDAO;
import zy.dao.batch.report.BatchReportDAO;
import zy.dto.batch.order.BatchOrderDetailReportDto;
import zy.dto.batch.sell.SellDetailReportDto;
import zy.dto.batch.sell.SellDetailSizeReportDto;
import zy.dto.batch.sell.SellReportCsbDto;
import zy.dto.batch.sell.SellReportDto;
import zy.dto.batch.sell.SellSummaryDto;
import zy.dto.common.TypeLevelDto;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.base.size.T_Base_SizeList;
import zy.entity.base.type.T_Base_Type;
import zy.service.batch.report.BatchReportService;
import zy.util.CommonUtil;
import zy.util.StringUtil;
import zy.vo.base.TypeVO;
import zy.vo.common.SizeHorizontalVO;

@Service
public class BatchReportServiceImpl implements BatchReportService{
	@Resource
	private BatchReportDAO batchReportDAO;
	@Resource
	private SizeDAO sizeDAO;
	
	@Override
	public PageData<SellReportDto> pageSellReport(Map<String, Object> params) {
		Assert.notNull(params.get(CommonUtil.COMPANYID),"连接超时，请重新登录!");
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		
		Map<String, Object> sumMap = batchReportDAO.countsumSellReport(params);
		int totalCount = Integer.parseInt(sumMap.get("totalCount").toString());
		PageInfo pageInfo = new PageInfo(totalCount, pageSize, pageIndex);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		
		List<SellReportDto> list = batchReportDAO.listSellReport(params);
		PageData<SellReportDto> pageData = new PageData<SellReportDto>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		pageData.setData(sumMap);
		return pageData;
	}
	
	@Override
	public PageData<SellDetailReportDto> pageSellDetailReport(Map<String, Object> params) {
		Assert.notNull(params.get(CommonUtil.COMPANYID),"连接超时，请重新登录!");
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		
		Map<String, Object> sumMap = batchReportDAO.countsumSellDetailReport(params);
		int totalCount = Integer.parseInt(sumMap.get("totalCount").toString());
		PageInfo pageInfo = new PageInfo(totalCount, pageSize, pageIndex);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		
		List<SellDetailReportDto> list = batchReportDAO.listSellDetailReport(params);
		PageData<SellDetailReportDto> pageData = new PageData<SellDetailReportDto>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		pageData.setData(sumMap);
		return pageData;
	}
	
	@Override
	public Map<String, Object> sell_detail_size_title(Map<String, Object> params) {
		Integer companyid = (Integer)params.get(CommonUtil.COMPANYID);
		List<String> szgCodes = batchReportDAO.sell_size_szgcode(params);
		if(szgCodes==null||szgCodes.size()==0){
			Map<String, Object> resultMap = new HashMap<String, Object>();
			resultMap.put("titles", new ArrayList<List<String>>());
			return resultMap;
		}
		List<T_Base_SizeList> sizeGroupList = sizeDAO.listBySzg(szgCodes, companyid);
		return SizeHorizontalVO.getJsonSizeTitles(sizeGroupList);
	}
	
	@Override
	public PageData<SellDetailSizeReportDto> pageSellDetailSizeReport(Map<String, Object> params) {
		Assert.notNull(params.get(CommonUtil.COMPANYID),"连接超时，请重新登录!");
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		
		int totalCount = batchReportDAO.countSellDetailSizeReport(params);
		PageInfo pageInfo = new PageInfo(totalCount, pageSize, pageIndex);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		
		List<SellDetailSizeReportDto> list = batchReportDAO.listSellDetailSizeReport(params);
		for (SellDetailSizeReportDto dto : list) {
			String size_amount = dto.getSize_amount();
			if(StringUtil.isEmpty(size_amount)){
				continue;
			}
			String[] sizeAmounts = size_amount.split(",");
			for (String sizeAmount : sizeAmounts) {
				String[] temps = sizeAmount.split(":");
				if(temps.length != 2){
					continue;
				}
				if(dto.getAmountMap().containsKey(temps[0])){
					dto.getAmountMap().put(temps[0], dto.getAmountMap().get(temps[0]) + Integer.parseInt(temps[1]));
				}else {
					dto.getAmountMap().put(temps[0], Integer.parseInt(temps[1]));
				}
			}
		}
		
		PageData<SellDetailSizeReportDto> pageData = new PageData<SellDetailSizeReportDto>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
//		pageData.setData(sumMap);
		return pageData;
	}
	
	@Override
	public PageData<BatchOrderDetailReportDto> pageOrderDetailReport(Map<String, Object> params) {
		Assert.notNull(params.get(CommonUtil.COMPANYID),"连接超时，请重新登录!");
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		
		Map<String, Object> sumMap = batchReportDAO.countsumOrderDetailReport(params);
		int totalCount = Integer.parseInt(sumMap.get("totalCount").toString());
		PageInfo pageInfo = new PageInfo(totalCount, pageSize, pageIndex);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		
		List<BatchOrderDetailReportDto> list = batchReportDAO.listOrderDetailReport(params);
		PageData<BatchOrderDetailReportDto> pageData = new PageData<BatchOrderDetailReportDto>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		pageData.setData(sumMap);
		return pageData;
	}
	
	@Override
	public PageData<SellSummaryDto> pageSellSummary(Map<String, Object> params) {
		Assert.notNull(params.get(CommonUtil.COMPANYID),"连接超时，请重新登录!");
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		
		int totalCount = batchReportDAO.countSellSummary(params);
		PageInfo pageInfo = new PageInfo(totalCount, pageSize, pageIndex);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		
		List<SellSummaryDto> list = batchReportDAO.listSellSummary(params);
		
		for (SellSummaryDto dto : list) {
			String month_amountmoney = dto.getMonth_amountmoney();//3:54_3042.00,6:-12_-673.80
			if(StringUtil.isEmpty(month_amountmoney)){
				continue;
			}
			String[] month_amountmoneys = month_amountmoney.split(",");
			for (String item : month_amountmoneys) {//3:54_3042.00
				String[] temps = item.split(":");
				if(temps.length != 2){
					continue;
				}
				String[] amount_money = temps[1].split("_");
				if(amount_money.length != 2){
					continue;
				}
				if(dto.getAmountMap().containsKey(temps[0])){
					dto.getAmountMap().put(temps[0], dto.getAmountMap().get(temps[0]) + Integer.parseInt(amount_money[0]));
				}else {
					dto.getAmountMap().put(temps[0], Integer.parseInt(amount_money[0]));
				}
				if(dto.getMoneyMap().containsKey(temps[0])){
					dto.getMoneyMap().put(temps[0], dto.getMoneyMap().get(temps[0]) + Double.parseDouble(amount_money[1]));
				}else {
					dto.getMoneyMap().put(temps[0], Double.parseDouble(amount_money[1]));
				}
			}
			dto.setTotalamount(0);
			dto.setTotalmoney(0d);
			for (String key : dto.getAmountMap().keySet()) {
				dto.setTotalamount(dto.getTotalamount() + dto.getAmountMap().get(key));
			}
			for(String key:dto.getMoneyMap().keySet()){
				dto.setTotalmoney(dto.getTotalmoney() + dto.getMoneyMap().get(key));
			}
		}
		
		PageData<SellSummaryDto> pageData = new PageData<SellSummaryDto>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	
	@Override
	public List<Map<String, Object>> type_level_sell(Map<String, Object> params) {
		Assert.notNull(params.get(CommonUtil.COMPANYID),"连接超时，请重新登录!");
		Integer companyid = (Integer)params.get(CommonUtil.COMPANYID);
		String tp_upcode = StringUtil.trimString(params.get("tp_upcode"));
		List<T_Base_Type> childTypes = batchReportDAO.listTypeByUpCode(tp_upcode, companyid);
		Map<String, String> childTypeMap = new HashMap<String, String>();
		for (T_Base_Type type : childTypes) {
			childTypeMap.put(type.getTp_code(), type.getTp_name());
		}
		List<T_Base_Type> allTypes = batchReportDAO.listAllType(companyid);
		//根据父类编号的直接子类和所有类别，生成直接子类和其所有子类的映射关系
		Map<String, String> tpCode_RootCode = TypeVO.Convert(allTypes, Arrays.asList(childTypeMap.keySet().toArray()));
		List<Map<String, Object>> datas = batchReportDAO.type_level_sell(params);
		Map<String, TypeLevelDto> dtosMap = new HashMap<String, TypeLevelDto>();
		for (T_Base_Type type : childTypes) {
			TypeLevelDto dto = new TypeLevelDto();
			dto.setTp_code(type.getTp_code());
			dto.setTp_name(type.getTp_name());
			dtosMap.put(type.getTp_code(), dto);
		}
		for (Map<String, Object> item : datas) {
			String tp_code = StringUtil.trimString(item.get("tp_code"));
			String month = StringUtil.trimString(item.get("month"));
			Integer amount = Integer.parseInt(item.get("amount").toString());
			Double money = Double.parseDouble(item.get("money").toString());
			if (!childTypeMap.containsKey(tp_code) && tpCode_RootCode.containsKey(tp_code)) {
				tp_code = tpCode_RootCode.get(tp_code);
			}
			TypeLevelDto dto = dtosMap.get(tp_code);
			if (dto == null) {
				continue;
			}
			if(dto.getAmountMap().containsKey(month)){
				dto.getAmountMap().put(month, dto.getAmountMap().get(month)+amount);
			}else {
				dto.getAmountMap().put(month, amount);
			}
			if(dto.getMoneyMap().containsKey(month)){
				dto.getMoneyMap().put(month, dto.getMoneyMap().get(month)+money);
			}else {
				dto.getMoneyMap().put(month, money);
			}
			if(!dto.getChild_type().contains(StringUtil.trimString(item.get("tp_code")))){
				dto.getChild_type().add(StringUtil.trimString(item.get("tp_code")));
			}
		}
		List<Map<String, Object>> resultList = new ArrayList<Map<String,Object>>();
		Map<String, Object> item = null;
		for (T_Base_Type type : childTypes) {
			TypeLevelDto dto = dtosMap.get(type.getTp_code());
			//数量
			item = new HashMap<String, Object>();
			item.put("id", dto.getTp_code()+"_0");
			item.put("tp_code", dto.getTp_code());
			item.put("tp_code_hid", dto.getTp_code());
			item.put("tp_name", dto.getTp_name());
			item.put("project", "数量");
			item.putAll(dto.getAmountMap());
			item.put("total", dto.getTotalAmount());
			item.put("child_type", join(dto.getChild_type()));
			resultList.add(item);
			//金额
			item = new HashMap<String, Object>();
			item.put("id", dto.getTp_code()+"_1");
			item.put("tp_code", dto.getTp_code());
			item.put("tp_code_hid", dto.getTp_code());
			item.put("tp_name", dto.getTp_name());
			item.put("project", "金额");
			item.putAll(dto.getMoneyMap());
			item.put("total", dto.getTotalMoney());
			item.put("child_type", join(dto.getChild_type()));
			resultList.add(item);
		}
		return resultList;
	}
	private String join(List<String> types){
		StringBuffer result = new StringBuffer();
		for (String item : types) {
			result.append(item).append(",");
		}
		if(result.length() > 0){
			result.deleteCharAt(result.length() - 1);
		}
		return result.toString();
	}
	
	@Override
	public List<SellReportCsbDto> listSellReport_csb(Map<String, Object> params) {
		Assert.notNull(params.get(CommonUtil.COMPANYID),"连接超时，请重新登录!");
		return batchReportDAO.listSellReport_csb(params);
	}
	
}
