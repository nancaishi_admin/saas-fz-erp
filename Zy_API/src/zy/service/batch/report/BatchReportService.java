package zy.service.batch.report;

import java.util.List;
import java.util.Map;

import zy.dto.batch.order.BatchOrderDetailReportDto;
import zy.dto.batch.sell.SellDetailReportDto;
import zy.dto.batch.sell.SellDetailSizeReportDto;
import zy.dto.batch.sell.SellReportCsbDto;
import zy.dto.batch.sell.SellReportDto;
import zy.dto.batch.sell.SellSummaryDto;
import zy.entity.PageData;

public interface BatchReportService {
	PageData<SellReportDto> pageSellReport(Map<String, Object> params);
	PageData<SellDetailReportDto> pageSellDetailReport(Map<String, Object> params);
	Map<String, Object> sell_detail_size_title(Map<String, Object> params);
	public PageData<SellDetailSizeReportDto> pageSellDetailSizeReport(Map<String, Object> params);
	PageData<BatchOrderDetailReportDto> pageOrderDetailReport(Map<String, Object> params);
	PageData<SellSummaryDto> pageSellSummary(Map<String, Object> params);
	List<Map<String, Object>> type_level_sell(Map<String, Object> params);
	List<SellReportCsbDto> listSellReport_csb(Map<String, Object> params);
}
