package zy.service.sys.sqb.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import zy.dao.sys.sqb.SqbDAO;
import zy.entity.sys.sqb.T_Sys_Sqb;
import zy.service.sys.sqb.SqbService;

@Service
public class SqbServiceImpl implements SqbService{
	@Resource
	private SqbDAO sqbDAO;
	
	@Override
	public T_Sys_Sqb load(Integer sqb_id) {
		return sqbDAO.load(sqb_id);
	}
	
	@Override
	@Transactional
	public void save(T_Sys_Sqb sqb) {
		Assert.hasText(sqb.getSqb_device_id(),"设备id不能为空");
		Assert.hasText(sqb.getSqb_device_name(),"设备名称不能为空");
		T_Sys_Sqb check = sqbDAO.check(sqb.getSqb_device_id());
		if(check != null){
			throw new RuntimeException("设备id已经存在");
		}
		sqbDAO.save(sqb);
	}
	
}
