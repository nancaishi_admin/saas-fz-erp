package zy.service.sys.role.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import zy.dao.sys.role.RoleDAO;
import zy.entity.sys.menu.T_Sys_Menu;
import zy.entity.sys.role.T_Sys_Role;
import zy.entity.sys.role.T_Sys_RoleMenu;
import zy.service.sys.role.RoleService;
@Service
public class RoleServiceImpl implements RoleService{
	@Resource
	private RoleDAO roleDAO;
	@Override
	public List<T_Sys_Role> list(Map<String, Object> param) {
		return roleDAO.list(param);
	}

	@Override
	public List<T_Sys_Role> dia_list(Map<String, Object> param) {
		return roleDAO.dia_list(param);
	}

	@Override
	public Integer queryByName(T_Sys_Role model) {
		return roleDAO.queryByName(model);
	}

	@Override
	public T_Sys_Role queryByID(Integer id) {
		return roleDAO.queryByID(id);
	}
	@Transactional
	@Override
	public void update(T_Sys_Role model) {
		roleDAO.update(model);		
	}
	@Transactional
	@Override
	public void save(T_Sys_Role model) {
		roleDAO.save(model);		
	}
	@Transactional
	@Override
	public void del(Integer id) {
		roleDAO.del(id);		
	}

	@Override
	public List<T_Sys_Menu> listMenu(Map<String, Object> param) {
		return roleDAO.listMenu(param);
	}
	@Transactional
	@Override
	public void updateMenus(List<T_Sys_Menu> menuList) {
		roleDAO.updateMenus(menuList);
	}

	@Override
	public void updateMenu(T_Sys_Menu menu) {
		roleDAO.updateMenu(menu);
	}
	
	@Override
	public List<T_Sys_Menu> listAllMenu(String mn_version,Integer shop_type) {
		return roleDAO.listAllMenu(mn_version, shop_type);
	}

	@Override
	public List<T_Sys_RoleMenu> listRoleMenu(String ro_code,Integer companyid) {
		return roleDAO.listRoleMenu(ro_code, companyid);
	}
	
	@Override
	@Transactional
	public void rolemenu_save(List<T_Sys_RoleMenu> roleMenus,String ro_code,Integer companyid) {
		Assert.hasText(ro_code,"请选择角色");
		List<T_Sys_RoleMenu> srcMenus = roleDAO.listRoleMenu(ro_code, companyid);
		List<T_Sys_RoleMenu> addMenus = new ArrayList<T_Sys_RoleMenu>();
		List<Integer> delMenus = new ArrayList<Integer>();
		for (T_Sys_RoleMenu item : roleMenus) {
			boolean exist = false;
			for(T_Sys_RoleMenu src : srcMenus){
				if(item.getRm_mn_code().equals(src.getRm_mn_code())){
					exist = true;
					break;
				}
			}
			if(!exist){
				item.setCompanyid(companyid);
				item.setRm_ro_code(ro_code);
				item.setRm_state(0);
				addMenus.add(item);
			}
		}
		for(T_Sys_RoleMenu src : srcMenus){
			boolean exist = false;
			for (T_Sys_RoleMenu item : roleMenus) {
				if(item.getRm_mn_code().equals(src.getRm_mn_code())){
					exist = true;
					break;
				}
			}
			if(!exist){
				delMenus.add(src.getRm_id());
			}
		}
		if (addMenus.size() > 0) {
			roleDAO.saveRoleMenu(addMenus);
		}
		if (delMenus.size() > 0) {
			roleDAO.delRoleMenu(delMenus);
		}
	}
	
}
