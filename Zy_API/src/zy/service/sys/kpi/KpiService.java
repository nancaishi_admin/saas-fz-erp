package zy.service.sys.kpi;

import java.util.List;
import java.util.Map;

import zy.entity.sys.kpi.T_Sys_Kpi;
import zy.entity.sys.kpi.T_Sys_KpiScore;

public interface KpiService {
	List<T_Sys_Kpi> list(Map<String, Object> params);
	List<T_Sys_KpiScore> listScores(Map<String, Object> params);
	void save(T_Sys_Kpi kpi,List<T_Sys_KpiScore> kpiScores);
	void update(T_Sys_Kpi kpi,List<T_Sys_KpiScore> kpiScores);
	void del(String ki_code,String shop_code,Integer companyid);
}
