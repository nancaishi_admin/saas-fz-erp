package zy.service.sys.print.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.sys.print.PrintDAO;
import zy.entity.common.print.Common_Print;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.entity.sys.print.T_Sys_Print;
import zy.entity.sys.print.T_Sys_PrintData;
import zy.entity.sys.print.T_Sys_PrintField;
import zy.entity.sys.print.T_Sys_PrintSet;
import zy.entity.sys.user.T_Sys_User;
import zy.service.sys.print.PrintService;
import zy.util.DateUtil;
import zy.util.StringUtil;

@Service
public class PrintServiceImpl implements PrintService{
	@Resource
	private PrintDAO printDAO;

	@Override
	public List<Common_Print> listCommonPrint(String pt_type) {
		if (StringUtil.isEmpty(pt_type)) {
			throw new IllegalArgumentException("参数pt_type不能为null");
		}
		List<Integer> pt_types = new ArrayList<Integer>();
		String[] typeArr = pt_type.split(",");
		for (String type : typeArr) {
			pt_types.add(Integer.valueOf(type));
		}
		return printDAO.listCommonPrint(pt_types);
	}
	
	@Override
	public List<T_Sys_Print> list(Integer sp_type, String sp_shop_code, Integer companyid) {
		return printDAO.list(sp_type, sp_shop_code, companyid);
	}
	//查询自己的数据，若没有，则查询系统默认数据
	@Override
	public Map<String, Object> loadPrint(Integer sp_id) {
		if (sp_id == null) {
			throw new IllegalArgumentException("参数sp_id不能为null");
		}
		return printDAO.loadPrint(sp_id);
	}
	
	@Override
	public Map<String, Object> loadDefaultPrint(Integer pt_type) {
		if (pt_type == null) {
			throw new IllegalArgumentException("参数pt_type不能为null");
		}
		return printDAO.loadDefaultPrint(pt_type);
	}

	@Override
	@Transactional
	public void save(T_Sys_Print print, List<T_Sys_PrintField> printFields,
			List<T_Sys_PrintData> printDatas, List<T_Sys_PrintSet> printSets,
			T_Sys_User user) {
		if (printFields == null || printFields.size() == 0) {
			throw new IllegalArgumentException("参数printFields不能为null");
		}
		if (printDatas == null || printDatas.size() == 0) {
			throw new IllegalArgumentException("参数printDatas不能为null");
		}
		if (printSets == null || printSets.size() == 0) {
			throw new IllegalArgumentException("参数printSets不能为null");
		}
		print.setCompanyid(user.getCompanyid());
		print.setSp_us_id(user.getUs_id());
		print.setSp_shop_code(user.getUs_shop_code());
		print.setSp_sysdate(DateUtil.getCurrentTime());
		for (T_Sys_PrintField field : printFields) {
			field.setCompanyid(user.getCompanyid());
		}
		for (T_Sys_PrintData data : printDatas) {
			data.setCompanyid(user.getCompanyid());
		}
		for (T_Sys_PrintSet set : printSets) {
			set.setCompanyid(user.getCompanyid());
		}
		printDAO.save(print, printFields, printDatas, printSets);
	}
	
	@Override
	@Transactional
	public void save(T_Sys_Print print, List<T_Sys_PrintField> printFields,
			List<T_Sys_PrintData> printDatas, List<T_Sys_PrintSet> printSets,
			T_Sell_Cashier cashier) {
		if (printFields == null || printFields.size() == 0) {
			throw new IllegalArgumentException("参数printFields不能为null");
		}
		if (printDatas == null || printDatas.size() == 0) {
			throw new IllegalArgumentException("参数printDatas不能为null");
		}
		if (printSets == null || printSets.size() == 0) {
			throw new IllegalArgumentException("参数printSets不能为null");
		}
		print.setCompanyid(cashier.getCompanyid());
		print.setSp_us_id(cashier.getCa_id());
		print.setSp_shop_code(cashier.getCa_shop_code());
		print.setSp_sysdate(DateUtil.getCurrentTime());
		for (T_Sys_PrintField field : printFields) {
			field.setCompanyid(cashier.getCompanyid());
		}
		for (T_Sys_PrintData data : printDatas) {
			data.setCompanyid(cashier.getCompanyid());
		}
		for (T_Sys_PrintSet set : printSets) {
			set.setCompanyid(cashier.getCompanyid());
		}
		printDAO.save(print, printFields, printDatas, printSets);
	}

	@Override
	@Transactional
	public void update(T_Sys_Print print, List<T_Sys_PrintField> printFields, List<T_Sys_PrintData> printDatas, List<T_Sys_PrintSet> printSets) {
		if (printFields == null || printFields.size() == 0) {
			throw new IllegalArgumentException("参数printFields不能为null");
		}
		if (printDatas == null || printDatas.size() == 0) {
			throw new IllegalArgumentException("参数printDatas不能为null");
		}
		if (printSets == null || printSets.size() == 0) {
			throw new IllegalArgumentException("参数printSets不能为null");
		}
		for (T_Sys_PrintSet set : printSets) {
			set.setSps_sp_id(print.getSp_id());
		}
		printDAO.update(print, printFields, printDatas, printSets);
	}

	@Override
	@Transactional
	public void del(Integer sp_id) {
		printDAO.del(sp_id);
	}

}
