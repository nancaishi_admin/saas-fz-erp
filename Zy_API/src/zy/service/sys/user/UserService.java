package zy.service.sys.user;

import java.util.Map;

import zy.entity.PageData;
import zy.entity.sys.user.T_Sys_User;

public interface UserService {
	PageData<T_Sys_User> page(Map<String,Object> param);
	Integer queryByName(T_Sys_User model);
	T_Sys_User queryByID(Integer id);
	void update(T_Sys_User model);
	void updateState(T_Sys_User model);
	void save(T_Sys_User model);
	void del(Integer id);
	void initPwd(Integer id);
	void changePassword(Integer us_id, String old_pwd, String us_pwd);
}
