package zy.service.sys.init.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.money.bank.BankDAO;
import zy.dao.money.bank.BankRunDAO;
import zy.dao.sys.init.InitDAO;
import zy.entity.base.shop.T_Base_Shop;
import zy.entity.batch.client.T_Batch_Client;
import zy.entity.batch.sell.T_Batch_Sell;
import zy.entity.buy.enter.T_Buy_Enter;
import zy.entity.buy.supply.T_Buy_Supply;
import zy.entity.money.bank.T_Money_Bank;
import zy.entity.money.bank.T_Money_BankRun;
import zy.entity.sort.allot.T_Sort_Allot;
import zy.entity.sys.user.T_Sys_User;
import zy.service.sys.init.InitService;
import zy.util.CommonUtil;
import zy.util.DateUtil;

@Service
public class InitServiceImpl implements InitService{
	@Resource
	private InitDAO initDAO;
	@Resource
	private BankDAO bankDAO;
	@Resource
	private BankRunDAO bankRunDAO;
	
	@Override
	@Transactional
	public void enableAccount(T_Sys_User user) {
		if (!CommonUtil.ONE.equals(user.getShoptype())
				&& !CommonUtil.TWO.equals(user.getShoptype())
				&& !CommonUtil.FOUR.equals(user.getShoptype())) {
			throw new RuntimeException("无权限进行启用账套操作");
		}
		T_Base_Shop shop = initDAO.checkShop(user.getUs_shop_code(), user.getCompanyid());
		if (shop == null || shop.getSp_init().equals(1)) {
			throw new RuntimeException("账套已经启用");
		}
		initDAO.updateShopInit(user.getUs_shop_code(), user.getShoptype(), user.getCompanyid());
		if(CommonUtil.ONE.equals(user.getShoptype())){//总公司
			enableAccount_Client(user);
			enableAccount_Supply(user);
			enableAccount_Shop(user);
			enableAccount_Bank(user);
		}else if(CommonUtil.TWO.equals(user.getShoptype())){//分公司
			enableAccount_Client(user);
			enableAccount_Shop(user);
			enableAccount_Bank(user);
		}else if(CommonUtil.FOUR.equals(user.getShoptype())){//加盟店
			enableAccount_Bank(user);
		}
		user.setSp_init(1);
	}
	
	private void enableAccount_Client(T_Sys_User user){
		List<T_Batch_Client> clients = initDAO.listClient4Init(user.getUs_shop_code(), user.getCompanyid());
		if (clients == null || clients.size() == 0) {
			return;
		}
		List<T_Batch_Sell> sells = new ArrayList<T_Batch_Sell>();
		T_Batch_Sell item = null;
		String numberPrefix = CommonUtil.NUMBER_PREFIX_QC_CLIENT + DateUtil.getYearMonthDateYYYYMMDD();
		for (T_Batch_Client client :clients) {
			item = new T_Batch_Sell();
			item.setSe_number(numberPrefix+client.getCi_code());
			item.setSe_make_date(DateUtil.getYearMonthDate());
			item.setSe_client_code(client.getCi_code());
			item.setSe_maker(user.getUs_name());
			item.setSe_manager(user.getUs_name());
			item.setSe_amount(0);
			item.setSe_money(client.getCi_init_debt());
			item.setSe_retailmoney(client.getCi_init_debt());
			item.setSe_costmoney(client.getCi_init_debt());
			item.setSe_rebatemoney(0d);
			item.setSe_discount_money(0d);
			item.setSe_ar_state(CommonUtil.AR_STATE_APPROVED);
			item.setSe_ar_date(DateUtil.getCurrentTime());
			item.setSe_isdraft(0);
			item.setSe_pay_state(CommonUtil.PAY_STATE_UNPAY);
			item.setSe_receivable(client.getCi_init_debt());
			item.setSe_received(0d);
			item.setSe_prepay(0d);
			item.setSe_type(2);
			item.setSe_stream_money(0d);
			item.setSe_sysdate(DateUtil.getCurrentTime());
			item.setSe_us_id(user.getUs_id());
			item.setSe_isprint(0);
			item.setCompanyid(user.getCompanyid());
			sells.add(item);
		}
		initDAO.saveQcClient(sells);
	}
	private void enableAccount_Supply(T_Sys_User user){
		List<T_Buy_Supply> supplies = initDAO.listSupply4Init(user.getCompanyid());
		if (supplies == null || supplies.size() == 0) {
			return;
		}
		List<T_Buy_Enter> enters = new ArrayList<T_Buy_Enter>();
		T_Buy_Enter item = null;
		String numberPrefix = CommonUtil.NUMBER_PREFIX_QC_SUPPLY + DateUtil.getYearMonthDateYYYYMMDD();
		for (T_Buy_Supply supply : supplies) {
			item = new T_Buy_Enter();
			item.setEt_number(numberPrefix+supply.getSp_code());
			item.setEt_make_date(DateUtil.getYearMonthDate());
			item.setEt_supply_code(supply.getSp_code());
			item.setEt_maker(user.getUs_name());
			item.setEt_manager(user.getUs_name());
			item.setEt_amount(0);
			item.setEt_money(supply.getSp_init_debt());
			item.setEt_retailmoney(supply.getSp_init_debt());
			item.setEt_discount_money(0d);
			item.setEt_ar_state(CommonUtil.AR_STATE_APPROVED);
			item.setEt_ar_date(DateUtil.getCurrentTime());
			item.setEt_ar_name(user.getUs_name());
			item.setEt_isdraft(0);
			item.setEt_pay_state(CommonUtil.PAY_STATE_UNPAY);
			item.setEt_payable(supply.getSp_init_debt());
			item.setEt_payabled(0d);
			item.setEt_prepay(0d);
			item.setEt_type(2);
			item.setEt_sysdate(DateUtil.getCurrentTime());
			item.setEt_us_id(user.getUs_id());
			item.setCompanyid(user.getCompanyid());
			enters.add(item);
		}
		initDAO.saveQcSupply(enters);
	}
	private void enableAccount_Shop(T_Sys_User user){
		List<T_Base_Shop> shops = initDAO.listShop4Init(user.getUs_shop_code(), user.getShoptype(), user.getCompanyid());
		if (shops == null || shops.size() == 0) {
			return;
		}
		List<T_Sort_Allot> allots = new ArrayList<T_Sort_Allot>();
		T_Sort_Allot item = null;
		String numberPrefix = CommonUtil.NUMBER_PREFIX_QC_SHOP + DateUtil.getYearMonthDateYYYYMMDD();
		for (T_Base_Shop shop : shops) {
			item = new T_Sort_Allot();
			item.setAt_number(numberPrefix+shop.getSp_code());
			item.setAt_date(DateUtil.getYearMonthDate());
			item.setAt_shop_code(shop.getSp_code());
			item.setAt_maker(user.getUs_name());
			item.setAt_manager(user.getUs_name());
			item.setAt_applyamount(0);
			item.setAt_sendamount(0);
			item.setAt_applymoney(0d);
			item.setAt_sendmoney(shop.getSp_init_debt());
			item.setAt_sendcostmoney(shop.getSp_init_debt());
			item.setAt_sendsellmoney(shop.getSp_init_debt());
			item.setAt_discount_money(0d);
			item.setAt_ar_state(CommonUtil.WANT_AR_STATE_FINISH);
			item.setAt_ar_date(DateUtil.getCurrentTime());
			item.setAt_pay_state(CommonUtil.PAY_STATE_UNPAY);
			item.setAt_receivable(shop.getSp_init_debt());
			item.setAt_received(0d);
			item.setAt_prepay(0d);
			item.setAt_type(2);
			item.setAt_sysdate(DateUtil.getCurrentTime());
			item.setAt_us_id(user.getUs_id());
			item.setAt_isdraft(0);
			item.setCompanyid(user.getCompanyid());
			allots.add(item);
		}
		initDAO.saveQcShop(allots);
	}
	private void enableAccount_Bank(T_Sys_User user){
		List<T_Money_Bank> banks = initDAO.listBank4Init(user.getUs_shop_code(), user.getCompanyid());
		if (banks == null || banks.size() == 0) {
			return;
		}
		List<T_Money_BankRun> bankRuns = new ArrayList<T_Money_BankRun>();
		for (T_Money_Bank bank : banks) {
			bank.setBa_balance(bank.getBa_balance()+bank.getBa_init_balance());
			T_Money_BankRun bankRun = new T_Money_BankRun();
			bankRun.setBr_ba_code(bank.getBa_code());
			bankRun.setBr_balance(bank.getBa_balance());
			bankRun.setBr_bt_code(CommonUtil.BANKRUN_MONEY_QC);
			bankRun.setBr_date(DateUtil.getYearMonthDate());
			if(bank.getBa_init_balance()>0){
				bankRun.setBr_enter(Math.abs(bank.getBa_init_balance()));
			}else {
				bankRun.setBr_out(Math.abs(bank.getBa_init_balance()));
			}
			bankRun.setBr_manager(user.getUs_name());
			bankRun.setBr_number("");
			bankRun.setBr_remark("");
			bankRun.setBr_shop_code(bank.getBa_shop_code());
			bankRun.setBr_sysdate(DateUtil.getCurrentTime());
			bankRun.setCompanyid(user.getCompanyid());
			bankRuns.add(bankRun);
		}
		bankDAO.updateBalanceById(banks);
		bankRunDAO.save(bankRuns);
		
	}
}
