package zy.service.sys.workbench;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.common.todo.Common_ToDo;
import zy.entity.sys.menu.T_Sys_Menu;
import zy.entity.sys.user.T_Sys_User;
import zy.entity.sys.workbench.T_Sys_WorkBench;

public interface WorkBenchService {
	List<T_Sys_Menu> listUpMenu(Map<String, Object> params);
	List<T_Sys_Menu> listSubMenu(Map<String, Object> params);
	List<Common_ToDo> listToDo(Map<String, Object> params);
	PageData<T_Sys_WorkBench> page(Map<String, Object> params);
	List<T_Sys_WorkBench> list(Map<String, Object> params);
	List<T_Sys_WorkBench> listByUser(Map<String, Object> params);
	List<T_Sys_WorkBench> listToDoByUser(Map<String, Object> params);
	 void save(T_Sys_User user,Integer wb_type,String ids);
	 void delete(List<Integer> ids);
	 Map<String, Object> statToDoWarn(Map<String, Object> params);
}
