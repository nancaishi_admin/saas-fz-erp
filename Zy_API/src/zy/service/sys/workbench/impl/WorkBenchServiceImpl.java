package zy.service.sys.workbench.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import zy.dao.sys.workbench.WorkBenchDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.common.todo.Common_ToDo;
import zy.entity.sys.menu.T_Sys_Menu;
import zy.entity.sys.user.T_Sys_User;
import zy.entity.sys.workbench.T_Sys_WorkBench;
import zy.service.sys.workbench.WorkBenchService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;

@Service
public class WorkBenchServiceImpl implements WorkBenchService{
	@Resource
	private WorkBenchDAO workBenchDAO;
	
	@Override
	public List<T_Sys_Menu> listUpMenu(Map<String, Object> params) {
		Assert.notNull(params.get(CommonUtil.COMPANYID),"连接超时，请重新登录!");
		return workBenchDAO.listUpMenu(params);
	}
	
	@Override
	public List<T_Sys_Menu> listSubMenu(Map<String, Object> params) {
		Assert.notNull(params.get(CommonUtil.COMPANYID),"连接超时，请重新登录!");
		return workBenchDAO.listSubMenu(params);
	}
	
	@Override
	public List<Common_ToDo> listToDo(Map<String, Object> params) {
		Assert.notNull(params.get(CommonUtil.COMPANYID),"连接超时，请重新登录!");
		return workBenchDAO.listToDo(params);
	}
	
	@Override
	public PageData<T_Sys_WorkBench> page(Map<String, Object> params) {
		Assert.notNull(params.get(CommonUtil.COMPANYID),"连接超时，请重新登录!");
		Object pageSize = params.get(CommonUtil.PAGESIZE);
		Object pageIndex = params.get(CommonUtil.PAGEINDEX);
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = workBenchDAO.count(params);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		params.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		params.put(CommonUtil.END, _pageSize);
		
		List<T_Sys_WorkBench> list = workBenchDAO.list(params);
		PageData<T_Sys_WorkBench> pageData = new PageData<T_Sys_WorkBench>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	
	@Override
	public List<T_Sys_WorkBench> list(Map<String, Object> params) {
		Assert.notNull(params.get(CommonUtil.COMPANYID),"连接超时，请重新登录!");
		return workBenchDAO.list(params);
	}
	
	@Override
	public List<T_Sys_WorkBench> listByUser(Map<String, Object> params) {
		Assert.notNull(params.get(CommonUtil.COMPANYID),"连接超时，请重新登录!");
		return workBenchDAO.listByUser(params);
	}
	
	@Override
	public List<T_Sys_WorkBench> listToDoByUser(Map<String, Object> params) {
		Assert.notNull(params.get(CommonUtil.COMPANYID),"连接超时，请重新登录!");
		return workBenchDAO.listToDoByUser(params);
	}
	
	@Override
	@Transactional
	public void save(T_Sys_User user,Integer wb_type,String ids) {
		List<T_Sys_WorkBench> workBenchs = new ArrayList<T_Sys_WorkBench>();
		String[] mnIds = ids.split(",");
		List<Integer> mnIdsList = new ArrayList<Integer>();
		for (String mn_id : mnIds) {
			T_Sys_WorkBench workBench = new T_Sys_WorkBench();
			workBench.setWb_us_id(user.getUs_id());
			workBench.setWb_mn_id(Integer.parseInt(mn_id));
			workBench.setWb_oper_usid(user.getUs_id());
			workBench.setWb_weight(0);
			workBench.setWb_type(wb_type);
			workBench.setWb_sysdate(DateUtil.getCurrentTime());
			workBench.setCompanyid(user.getCompanyid());
			workBenchs.add(workBench);
			mnIdsList.add(workBench.getWb_mn_id());
		}
		Assert.notEmpty(workBenchs,"请选择菜单");
		int existCount = workBenchDAO.check(mnIdsList, wb_type, user.getUs_id(), user.getCompanyid());
		if(existCount > 0){
			throw new RuntimeException("该菜单已经存在");
		}
		workBenchDAO.save(workBenchs);
	}
	
	@Override
	@Transactional
	public void delete(List<Integer> ids) {
		Assert.notEmpty(ids,"ids不能为空");
		workBenchDAO.delete(ids);
	}
	
	@Override
	public Map<String, Object> statToDoWarn(Map<String, Object> params) {
		List<String> ids = Arrays.asList(StringUtil.trimString(params.get("todoids")).split(","));
		params.put("ids", ids);
		return workBenchDAO.statToDoWarn(params);
	}
	
	
}
