var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;;
var handle = {
	save:function(){
		var ecl_limitmoney = $.trim($("#ecl_limitmoney").val());
		var ecl_money = $.trim($("#ecl_money").val());
		var ecl_amount = $.trim($("#ecl_amount").val());
		var ecl_usedmoney = $.trim($("#ecl_usedmoney").val());
		
		var ec_type = $("#ec_type").val();
		if(ec_type == "0"){//线上领取
			if(ecl_limitmoney == "" || isNaN(ecl_limitmoney) || ecl_limitmoney <= 0){
				Public.tips({type: 2, content : "请输入满多少钱"});
				$("#ecl_limitmoney").select()
				return;
			}
			if(ecl_money == "" || isNaN(ecl_money) || ecl_money <= 0){
				Public.tips({type: 2, content : "请输入面值"});
				$("#ecl_money").select()
				return;
			}
			if(parseFloat(ecl_money) > parseFloat(ecl_limitmoney)){
				Public.tips({type: 2, content : "面值不能大于使用条件"});
				$("#ecl_money").select()
				return;
			}
			if(ecl_amount == "" || isNaN(ecl_amount) || ecl_amount <= 0){
				Public.tips({type: 2, content : "请输入发放数量"});
				$("#ecl_amount").select()
				return;
			}
		}else if(ec_type == "1"){//收银发放
			if(ecl_usedmoney == "" || isNaN(ecl_usedmoney) || ecl_usedmoney <= 0){
				Public.tips({type: 2, content : "请输入消费金额"});
				$("#ecl_usedmoney").select()
				return;
			}
			if(ecl_money == "" || isNaN(ecl_money) || ecl_money <= 0){
				Public.tips({type: 2, content : "请输入赠送"});
				$("#ecl_money").select()
				return;
			}
			if(ecl_limitmoney == "" || isNaN(ecl_limitmoney) || ecl_limitmoney <= 0){
				Public.tips({type: 2, content : "请输入使用条件消费金额"});
				$("#ecl_limitmoney").select()
				return;
			}
			if(parseFloat(ecl_money) > parseFloat(ecl_limitmoney)){
				Public.tips({type: 2, content : "赠送金额不能大于使用条件"});
				$("#ecl_limitmoney").select()
				return;
			}
		}
		$("#btn-save").attr("disabled",true);
		var url = config.BASEPATH+"sell/ecoupon/temp_save";
		if($.trim($("#ecl_id").val()) != ''){
			url = config.BASEPATH+"sell/ecoupon/temp_update";
		}
		$.ajax({
			type:"POST",
			url:url,
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					W.Public.tips({type: 3, content : data.message});
					handle.loadData(data.data);
					setTimeout("api.zindex()",500);
					THISPAGE.initDom();
				}else{
					W.Public.tips({type: 1, content : data.message});
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.ecl_id;
		pdata.ecl_usedmoney=data.ecl_usedmoney;
		pdata.ecl_money=data.ecl_money;
		pdata.ecl_limitmoney=data.ecl_limitmoney;
		pdata.ecl_amount=data.ecl_amount;
		pdata.ecl_remark=data.ecl_remark;
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initEvent();
	},
	initDom:function(){
		$("#ecl_usedmoney").focus();
	},
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save();
		});
		$("#btn_close").click(function(){
			api.close();
		});
	}
};
THISPAGE.init();