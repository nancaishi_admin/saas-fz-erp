var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var queryurl = config.BASEPATH+'sell/ecoupon/listDetail?number='+$("#ec_number").val();

var handle = {
	approve:function(){
		commonDia = $.dialog({ 
		   	id:'approve_confirm',
		   	title:'审批页面',
		   	data:{},
		   	max: false,
		   	min: false,
		   	lock:true,
		   	width:300,
		   	height:200,
		   	drag: true,
		   	resize:false,
		   	content:'url:'+config.BASEPATH+'approve/to_confirm',
		   	fixed:false,
		   	close : function(){
		   		var ar_infos = commonDia.content.ar_infos;
		   		if(ar_infos != undefined){
		   			handle.doApprove(ar_infos);
		   		}
		   	}
	    });
	},
	doApprove:function(ar_infos){
		$("#btn-approve").attr("disabled",true);
		var params = "";
		params += "number="+$("#ec_number").val();
		params += "&ar_state="+ar_infos.ar_state;
		params += "&ar_describe="+Public.encodeURI(ar_infos.ar_describe);
		$.ajax({
            type: "POST",
            url: config.BASEPATH+"sell/ecoupon/approve",
            data: params,
            cache:false,
			dataType:"json",
            success: function (data) {
            	if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : data.message});
					handle.loadData(data.data);
				}else{
					Public.tips({type: 1, content : data.message});
				}
				$("#btn-approve").attr("disabled",false);
            }
        });
	},
	stop:function(){
		commonDia = $.dialog({ 
		   	id:'approve_stop',
		   	title:'终止电子券方案',
		   	data:{},
		   	max: false,
		   	min: false,
		   	lock:true,
		   	width:300,
		   	height:200,
		   	drag: true,
		   	resize:false,
		   	content:'url:'+config.BASEPATH+'approve/to_stop',
		   	fixed:false,
		   	close : function(){
		   		var ar_infos = commonDia.content.ar_infos;
		   		if(ar_infos != undefined){
		   			handle.doStop(ar_infos);
		   		}
		   	}
	    });
	},
	doStop:function(ar_infos){
		$("#btn-stop").attr("disabled",true);
		var params = "";
		params += "number="+$("#ec_number").val();
		params += "&stop_cause="+Public.encodeURI(ar_infos.ar_describe);
		$.ajax({
            type: "POST",
            url: config.BASEPATH+"sell/ecoupon/stop",
            data: params,
            cache:false,
			dataType:"json",
            success: function (data) {
            	if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : data.message});
					handle.loadData(data.data);
				}else{
					Public.tips({type: 1, content : data.message});
				}
				$("#btn-stop").attr("disabled",false);
            }
        });
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.ec_id;
		pdata.ec_state=data.ec_state;
		pdata.operate='';
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	},
	formatLimitMoney :function(val, opt, row){
		return row.ecl_limitmoney;
	},
	formatMoney :function(val, opt, row){
		return row.ecl_money;
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();
	},
	initDom:function(){
		if('approve' == api.data.oper){
			$("#btn-approve").show();
		}
		var ec_state = $("#ec_state").val();
		if(ec_state == 2){//审核通过、未终止
			$("#btn-stop").show();
		}
		this.initLevelCombo();
		this.$_mode = $("#modeTd").cssRadio({ callback: function($_obj){
		}});
		this.$_mode.setValue($("#ec_mode").val()-1);
		var ec_mode = $("#ec_mode").val();
		if(ec_mode == 1){
			$("#mode_code_td").hide();
		}else if(ec_mode == 2){
			$("#mode_label").text("所选品牌：");
		}else if(ec_mode == 3){
			$("#mode_label").text("所选类别：");
		}else if(ec_mode == 4){
			$("#mode_label").text("所选商品：");
		}
		
		this.$_use_mode = $("#use_modeTd").cssRadio({ callback: function($_obj){
		}});
		this.$_use_mode.setValue($("#ec_use_mode").val()-1);
		var ec_use_mode = $("#ec_use_mode").val();
		if(ec_use_mode == 1){
			$("#use_mode_code_td").hide();
		}else if(ec_use_mode == 2){
			$("#use_mode_label").text("所选品牌：");
		}else if(ec_use_mode == 3){
			$("#use_mode_label").text("所选类别：");
		}else if(ec_use_mode == 4){
			$("#use_mode_label").text("所选商品：");
		}
	},
	initLevelCombo : function(){//1.最高2.较高3.一般4.较低5.最低
		var data = [ 
	                {Code : '1',Name : '最高'}, 
	                {Code : '2',Name : '较高'},
	                {Code : '3',Name : '一般'},
	                {Code : '4',Name : '较低'},
	                {Code : '5',Name : '最低'},
	              ];
		var levelCombo = $('#span_level').combo({
			value : 'Code',
			text : 'Name',
			width : 132,
			height : 80,
			listId : '',
			defaultSelected : 0,
			editable : false,
			callback : {
				onChange : function(data) {
					$("#ec_level").val(data.Code);
				}
			}
		}).getCombo();
		levelCombo.loadData(data,["Code",$("#ec_level").val(),0])
		levelCombo.disable(true);
		var ec_type = $("#ec_type").val();
		if(ec_type == 1){
			$("#ec_type_name").val("收银发放");
		}else{
			$("#ec_type_name").val("线上领取");
		}
	},
	initGrid:function(){
		var ec_type = $("#ec_type").val();
		var _height = $(parent).height()-337,_width = $(parent).width()-2;
		var colModel = [
	    	{label:'满多少元',name: 'ecl_usedmoney', index: 'ecl_usedmoney', width: 100,hidden:(ec_type == 0)},
	    	{label:'赠送',name: 'ecl_money', index: 'ecl_money', width: 100,hidden:(ec_type == 0)},
	    	{label:'满多少元可使用',name: 'ecl_limitmoney', index: 'ecl_limitmoney', width: 100,align:'right',hidden:(ec_type == 0)},
	    	{label:'满多少元',name: 'ecl_limitmoney_f', index: 'ecl_limitmoney', width: 100,align:'right',formatter: handle.formatLimitMoney,hidden:(ec_type == 1)},
	    	{label:'减多少',name: 'ecl_money_f', index: 'ecl_money', width: 100,align:'right',formatter: handle.formatMoney,hidden:(ec_type == 1)},
	    	{label:'发放数量',name: 'ecl_amount', index: 'ecl_amount', width: 100,align:'right',hidden:(ec_type == 1)},
	    	{label:'备注',name: 'ecl_remark', index: 'ecl_remark', width: 180,align:'center'}
	    ];
		$('#grid').jqGrid({
			url:queryurl,
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:-1,//每页条数
			cmTemplate: {sortable:true,title:true},
			shrinkToFit:false,//表格是否自动填充
			cellEdit: true,
            cellsubmit: 'clientArray',
            footerrow: true,
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'ecl_id'  //图标ID
			},
			loadComplete: function(data){
				
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	initEvent:function(){
		$('#btn-approve').on('click', function(e){
			e.preventDefault();
			handle.approve();
		});
		$('#btn-stop').on('click', function(e){
			e.preventDefault();
			handle.stop();
		});
		$("#btn_close").click(function(){
			api.close();
		});
	}
};
THISPAGE.init();