var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;;
var handle = {
	save:function(){
		if (!$("#form1").valid()) {
			return;
		}
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"sell/cashier/update",
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					W.Public.tips({type: 3, content : data.message});
					handle.loadData(data.data);
					setTimeout("api.close()",200);
				}else if(data.stat == 304){
					W.Public.tips({type: 1, content : data.message});
					setTimeout("api.zindex()",200);
				}else{
					W.Public.tips({type: 1, content : data.message});
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.ca_id;
		pdata.ca_em_code=data.ca_em_code;
		pdata.em_name=data.st_name;
		pdata.ca_erase=data.ca_erase;
		pdata.ca_maxmoney=data.ca_maxmoney;
		pdata.ca_minrate=data.ca_minrate;
		pdata.ca_days=data.ca_days;
		pdata.ca_state=data.ca_state;
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initParam();
		this.initDom();
		this.initCombo();
		this.initEvent();
	},
	initParam:function(){
		
	},
	initCombo:function(){
		var state = $("#ca_state").val();
		var pdata = [
				{id:'0',name:'正常'},
				{id:'1',name:'停用'},
				]
		stateCombo = $('#spanState').combo({
			data:pdata,
			value: 'id',
	        text: 'name',
			width : 158,
			height : 30,
			listId : '',
			defaultSelected: 0,
			editable : true,
			callback : {
				onChange : function(data) {
					$("#ca_state").val(data.id);
				}
			}
		}).getCombo();
		stateCombo.loadData(pdata,['id',state,0])
	},
	initDom:function(){
	},
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save();
		});
		$("#btn_close").click(function(){
			api.close();
		});
	}
};$(function(){
	THISPAGE.init();
});