var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var em_code = api.data.em_code;
var handle = {
	updateLimit:function(){
		var data = {};
		data.KEY_CASH=_self.$_chk_cash.chkVal().join()?1:0;
		data.KEY_BACK=_self.$_chk_back.chkVal().join()?1:0;
		data.KEY_CHANGE=_self.$_chk_change.chkVal().join()?1:0;
		data.KEY_SET=_self.$_chk_set.chkVal().join()?1:0;
		
		data.KEY_VIP_MANAGE=_self.$_chk_vip_manage.chkVal().join()?1:0;
		data.KEY_VIP_ADD=_self.$_chk_vip_add.chkVal().join()?1:0;
		data.KEY_POINT_CHANGE=_self.$_chk_point_change.chkVal().join()?1:0;
		data.KEY_GIFT=_self.$_chk_gift.chkVal().join()?1:0;

		data.KEY_VIP_EDIT=_self.$_chk_vip_edit.chkVal().join()?1:0;
		data.KEY_PRODUCT_QUERY=_self.$_chk_product_query.chkVal().join()?1:0;
		data.KEY_PRODUCT_STOCK=_self.$_chk_product_stock.chkVal().join()?1:0;
		data.KEY_BANK_RUN=_self.$_chk_bank_run.chkVal().join()?1:0;

		data.KEY_VOUCHER_QUERY=_self.$_chk_voucher_query.chkVal().join()?1:0;
		data.KEY_VOUCHER_ADD=_self.$_chk_voucher_add.chkVal().join()?1:0;
		data.KEY_CARD_QUERY=_self.$_chk_card_query.chkVal().join()?1:0;
		data.KEY_CARD_ADD=_self.$_chk_card_add.chkVal().join()?1:0;

		data.KEY_VIP_POINT=_self.$_chk_vip_point.chkVal().join()?1:0;
		data.KEY_DAYEND=_self.$_chk_dayend.chkVal().join()?1:0;
		data.KEY_SELL_MOBILE=_self.$_chk_sell_mobile.chkVal().join()?1:0;
		data.KEY_CARD_CHARGE=_self.$_chk_card_charge.chkVal().join()?1:0;

		data.KEY_SWAP_MANAGE=_self.$_chk_swap_manage.chkVal().join()?1:0;
		data.KEY_EDIT_PASS=_self.$_chk_edit_pass.chkVal().join()?1:0;
		data.KEY_SELL_LIST=_self.$_chk_sell_list.chkVal().join()?1:0;
		data.KEY_PUT_UP=_self.$_chk_put_up.chkVal().join()?1:0;
		
		data.KEY_HAND_RATE=_self.$_chk_hand_rate.chkVal().join()?1:0;
//		data.KEY_LINE_SELL=_self.$_chk_line_sell.chkVal().join()?1:0;
//		data.KEY_SHOW_SALVE=_self.$_chk_show_salve.chkVal().join()?1:0;
//		data.KEY_SHOW_AREA=_self.$_chk_show_area.chkVal().join()?1:0;
//		data.KEY_BACK_NUMBER=_self.$_chk_back_number.chkVal().join()?1:0;
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"sell/cashier/updateLimit",
			data:{"data":JSON.stringify(data),"em_code":em_code},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					W.Public.tips({type: 0, content : "权限调整成功"});
					setTimeout("api.close()",200);
				}else{
					W.Public.tips({type: 1, content : data.message});
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	}	
}
var THISPAGE = {
	init:function(){
		this.initLimit();
		this.initEvent();
	},
	initLimit:function(){
		if(null == em_code && '' == em_code){
			Public.tips({type: 1, content : "未选择收银员!"});
			setTimeout("api.close()",200);
		}
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"sell/cashier/limitByCode",
			data:{"em_code":em_code},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					THISPAGE.initParam(data.data);
				}else{
					W.Public.tips({type: 1, content : data.message});
				}
			}
		});
	},
	initParam:function(data){
		_self = this;
		this.$_chk_cash = $("#chk_cash").cssCheckbox();
		this.$_chk_back = $("#chk_back").cssCheckbox();
		this.$_chk_change = $("#chk_change").cssCheckbox();
		this.$_chk_set = $("#chk_set").cssCheckbox();
		
		this.$_chk_vip_manage = $("#chk_vip_manage").cssCheckbox();
		this.$_chk_vip_add = $("#chk_vip_add").cssCheckbox();
		this.$_chk_point_change = $("#chk_point_change").cssCheckbox();
		this.$_chk_gift = $("#chk_gift").cssCheckbox();
		
		this.$_chk_vip_edit = $("#chk_vip_edit").cssCheckbox();
		this.$_chk_product_query = $("#chk_product_query").cssCheckbox();
		this.$_chk_product_stock = $("#chk_product_stock").cssCheckbox();
		this.$_chk_bank_run = $("#chk_bank_run").cssCheckbox();
		
		this.$_chk_voucher_query = $("#chk_voucher_query").cssCheckbox();
		this.$_chk_voucher_add = $("#chk_voucher_add").cssCheckbox();
		this.$_chk_card_query = $("#chk_card_query").cssCheckbox();
		this.$_chk_card_add = $("#chk_card_add").cssCheckbox();
		
		this.$_chk_vip_point = $("#chk_vip_point").cssCheckbox();
		this.$_chk_dayend = $("#chk_dayend").cssCheckbox();
		this.$_chk_sell_mobile = $("#chk_sell_mobile").cssCheckbox();
		this.$_chk_card_charge = $("#chk_card_charge").cssCheckbox();
		
		this.$_chk_swap_manage = $("#chk_swap_manage").cssCheckbox();
		this.$_chk_edit_pass = $("#chk_edit_pass").cssCheckbox();
		this.$_chk_sell_list = $("#chk_sell_list").cssCheckbox();
		this.$_chk_put_up = $("#chk_put_up").cssCheckbox();
		
		this.$_chk_hand_rate = $("#chk_hand_rate").cssCheckbox();
		
//		this.$_chk_line_sell = $("#chk_line_sell").cssCheckbox();
//		this.$_chk_show_salve = $("#chk_show_salve").cssCheckbox();
//		this.$_chk_show_area = $("#chk_show_area").cssCheckbox();
//		this.$_chk_back_number = $("#chk_back_number").cssCheckbox();
		
		this.$_chk_all = $("#chk_all").cssCheckbox({
            callback: function (obj) {
            	var check = _self.$_chk_all.chkVal().join()?1:0;
            	if(check){
            		$_chk_box = $(".chkbox").cssCheckbox().chkAll();
            	}else{
            		$_chk_box = $(".chkbox").cssCheckbox().chkNot();
            	}
            }
        });
		if(undefined != data){
			1 == data.KEY_CASH?_self.$_chk_cash.chkAll():_self.$_chk_cash.chkNot();
			1 == data.KEY_BACK?_self.$_chk_back.chkAll():_self.$_chk_back.chkNot();
			1 == data.KEY_CHANGE?_self.$_chk_change.chkAll():_self.$_chk_change.chkNot();
			1 == data.KEY_SET?_self.$_chk_set.chkAll():_self.$_chk_set.chkNot();
			
			1 == data.KEY_VIP_MANAGE?_self.$_chk_vip_manage.chkAll():_self.$_chk_vip_manage.chkNot();
			1 == data.KEY_VIP_ADD?_self.$_chk_vip_add.chkAll():_self.$_chk_vip_add.chkNot();
			1 == data.KEY_POINT_CHANGE?_self.$_chk_point_change.chkAll():_self.$_chk_point_change.chkNot();
			1 == data.KEY_GIFT?_self.$_chk_gift.chkAll():_self.$_chk_gift.chkNot();

			1 == data.KEY_VIP_EDIT?_self.$_chk_vip_edit.chkAll():_self.$_chk_vip_edit.chkNot();
			1 == data.KEY_PRODUCT_QUERY?_self.$_chk_product_query.chkAll():_self.$_chk_product_query.chkNot();
			1 == data.KEY_PRODUCT_STOCK?_self.$_chk_product_stock.chkAll():_self.$_chk_product_stock.chkNot();
			1 == data.KEY_BANK_RUN?_self.$_chk_bank_run.chkAll():_self.$_chk_bank_run.chkNot();

			1 == data.KEY_VOUCHER_QUERY?_self.$_chk_voucher_query.chkAll():_self.$_chk_voucher_query.chkNot();
			1 == data.KEY_VOUCHER_ADD?_self.$_chk_voucher_add.chkAll():_self.$_chk_voucher_add.chkNot();
			1 == data.KEY_CARD_QUERY?_self.$_chk_card_query.chkAll():_self.$_chk_card_query.chkNot();
			1 == data.KEY_CARD_ADD?_self.$_chk_card_add.chkAll():_self.$_chk_card_add.chkNot();

			1 == data.KEY_VIP_POINT?_self.$_chk_vip_point.chkAll():_self.$_chk_vip_point.chkNot();
			1 == data.KEY_DAYEND?_self.$_chk_dayend.chkAll():_self.$_chk_dayend.chkNot();
			1 == data.KEY_SELL_MOBILE?_self.$_chk_sell_mobile.chkAll():_self.$_chk_sell_mobile.chkNot();
			1 == data.KEY_CARD_CHARGE?_self.$_chk_card_charge.chkAll():_self.$_chk_card_charge.chkNot();

			1 == data.KEY_SWAP_MANAGE?_self.$_chk_swap_manage.chkAll():_self.$_chk_swap_manage.chkNot();
			1 == data.KEY_EDIT_PASS?_self.$_chk_edit_pass.chkAll():_self.$_chk_edit_pass.chkNot();
			1 == data.KEY_SELL_LIST?_self.$_chk_sell_list.chkAll():_self.$_chk_sell_list.chkNot();
			1 == data.KEY_PUT_UP?_self.$_chk_put_up.chkAll():_self.$_chk_put_up.chkNot();

			1 == data.KEY_HAND_RATE?_self.$_chk_hand_rate.chkAll():_self.$_chk_hand_rate.chkNot();
			
//			1 == data.KEY_LINE_SELL?_self.$_chk_line_sell.chkAll():_self.$_chk_line_sell.chkNot();
//			1 == data.KEY_SHOW_SALVE?_self.$_chk_show_salve.chkAll():_self.$_chk_show_salve.chkNot();
//			1 == data.KEY_SHOW_AREA?_self.$_chk_show_area.chkAll():_self.$_chk_show_area.chkNot();
//			1 == data.KEY_BACK_NUMBER?_self.$_chk_back_number.chkAll():_self.$_chk_back_number.chkNot();
		}
	},
	initEvent:function(){
		$("#btn_close").click(function(){
			api.close();
		});
		$("#btn-save").click(function(){
			handle.updateLimit();
		});
	}
}
THISPAGE.init();