var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;

var Utils = {
	doQueryBank : function(code,name){
		commonDia = $.dialog({
			title : '选择银行账户',
			content : 'url:'+config.BASEPATH+'money/bank/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 260,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#"+code).val(selected.ba_code);
					$("#"+name).val(selected.ba_name);
					$("#"+code).valid();
				}
			},
			cancel:true
		});
	}
};

var handle = {
	save:function(){
		if (!$("#form1").valid()) {
			return;
		}
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"sell/card/recharge",
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : data.message});
					handle.loadData(data.data);
					setTimeout("api.zindex()",500);
					THISPAGE.initDom();
				}else{
					Public.tips({type: 1, content : data.message});
					setTimeout("api.zindex()",500);
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.cd_id;
		pdata.cd_money=data.cd_money;
		pdata.cd_used_money=data.cd_used_money;
		pdata.cd_realcash=data.cd_realcash;
		pdata.cd_bankmoney=data.cd_bankmoney;
		pdata.cd_cashrate=data.cd_cashrate;
		pdata.total_money=data.cd_money+data.cd_used_money;
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	}
};

var THISPAGE = {
	init:function (){
		this.initParam();
		this.initDom();
		this.initEvent();
	},
	initParam:function(){
	},
	initDom:function(){
		$("#cdl_date").val(DateToFullDateTimeString(new Date()));
		this.$_cdl_type = $("#span_cdl_type").cssRadio({ callback: function($_obj){
			var val = $_obj.find("input").val();
			$("#cdl_type").val(val);
			if(val == 1){
				$("span[name='label1']").text("充");
				$("span[name='label2']").text("收");
			}else{
				$("span[name='label1']").text("减");
				$("span[name='label2']").text("减");
			}
		}});
	},
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save();
		});
		$("#btn_close").click(function(){
			api.close();
		});
	}
};
THISPAGE.init();