var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.SELL08;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'sell/report/dayend/list';

var Utils = {
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:true,jmd:THISPAGE.$_jmd.chkVal().join()? 1 : 0},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var sp_code = [];
				var sp_name = [];
				for(var i=0;i<selected.length;i++){
					sp_code.push(selected[i].sp_code);
					sp_name.push(selected[i].sp_name);
				}
				$("#de_shop_code").val(sp_code.join(","));
				$("#shop_name").val(sp_name.join(","));
			},
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#em_name").val(selected.em_name);
				$("#de_em_code").val(selected.em_code);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#em_name").val(selected.em_name);
					$("#de_em_code").val(selected.em_code);
				}
			},
			cancel:true
		});
	}
};

var handle = {
	formatState:function(val, opt, row){
		if(val == 0){
			return "未交";
		}else if(val == 1){
			return "已交";
		}
	},
	formatDate:function(val,opt,row){
		return val+"~"+handle.formatNumber(row.de_enddate);
	},
	formatCard:function(val,opt,row){
		return handle.formatNumber(val)+"/"+handle.formatNumber(row.de_cd_send_bank);
	},
	formatCardFill:function(val,opt,row){
		return handle.formatNumber(val)+"/"+handle.formatNumber(row.de_cd_fill_bank);
	},
	formatNumber:function(val){
		if(null != val && "" != val && "null" != val){
			return val;
		}else{
			return 0;
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initState();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		dateRedioClick("theDate");
		this.$_jmd = $("#jmd").cssCheckbox({callback:function($_obj){
			$("#shop_name").val("");
			$("#de_shop_code").val("");
			$('#btn-search').click();
		}});
	},
	initState:function(){
		stateInfo = [
			        {id:"",name:"全部"},
					{id:"0",name:"未交班"},
					{id:"1",name:"已交班"}
				  ];
		stateCombo = $('#span_state').combo({
			data:stateInfo,
			value: 'id',
			text: 'name',
			width :206,
			height:80,
			listId:'',
			defaultSelected: 0,
			editable: true,
			callback:{
				onChange: function(data){
					$("#de_state").val(data.id);
				}
			}
		}).getCombo();
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
	    	{name: 'shop_name', index: 'shop_name',label:'门店名称', width: 100, align: 'left', title: true}, 
            {name: 'de_state', index: 'de_state',label:'状态', width: 40, align: 'center', title: false, formatter: handle.formatState}, 
            {name: 'em_name', index: 'em_name',label:'收银员', width: 60, align: 'left', title: false, fixed: true},
            {name: 'DE_SH_Name', index: 'DE_SH_Name',label:'班次', hidden:true,width: 35, title: false, align: 'center'},
            {name: 'de_begindate', index: 'de_begindate',label:'当班时间',formatter: handle.formatDate, width: 240, title: true, align: 'center'},
            {name: 'de_enddate', index: 'de_enddate',label:'交班时间',hidden:true, title: false},
            {name: 'de_cash', index: 'de_cash',label:'现金', width:70, align: 'right', title: false, fixed: true},
            {name: 'de_wx_money', index: 'de_wx_money',label:'微支付', width:70, align: 'right', title: false, fixed: true},
            {name: 'de_ali_money', index: 'de_ali_money',label:'支付宝', width:70, align: 'right', title: false, fixed: true},
            {name: 'de_bank_money', index: 'de_bank_money',label:'银行卡', width: 60, title: false, align: 'right'},
            {name: 'de_cd_money', index: 'de_cd_money',label:'储值卡', width: 60, align: 'right', title: false},
            {name: 'de_vc_money', index: 'de_vc_money',label:'代金劵', width: 60, title: false, align: 'right'},
            {name: 'de_ec_money', index: 'de_ec_money',label:'优惠券', width: 60, title: false, align: 'right'},
            {name: 'de_mall_money', index: 'de_mall_money',label:'商场卡', width: 60, title: false, align: 'right'},
            {name: 'de_point_money', index: 'de_point_money',label:'积分抵现', width: 60, title: false, align: 'right'},
            {name: 'de_lost_money', index: 'de_lost_money',label:'抹零金额', width: 60, title: false, align: 'right'},
            {name: 'de_cd_send_cash', index: 'de_cd_send_cash',label:'发储卡现金/刷卡', formatter: handle.formatCard ,width: 80, title: false, align: 'right'},
            {name: 'de_cd_send_bank', index: 'de_cd_send_bank',label:'发储卡刷卡', hidden:true, title: false, align: 'right'},
            {name: 'de_cd_fill_cash', index: 'de_cd_fill_cash',label:'充储卡现金/刷卡',formatter: handle.formatCardFill, width: 80, title: false, align: 'right'},
            {name: 'de_cd_fill_bank', index: 'de_cd_fill_bank',label:'充储卡刷卡',hidden:true, title: false, align: 'right'},
            {name: 'de_vc_send_money', index: 'de_vc_send_money',label:'发代金劵', width: 65, title: false, align: 'right'},
            {name: 'de_bills', index: 'de_bills',label:'总单数', width: 50, title: false, align: 'right'},
            {name: 'de_amount', index: 'de_amount',label:'销售数量', width: 60, align: 'right', title: false},
            {name: 'de_money', index: 'de_money',label:'销售金额', width: 70, title: false, align: 'right'},
            {name: 'de_sell_money', index: 'de_sell_money',label:'零售金额', width: 70, title: false, align: 'right'},
            {name: 'de_last_money', index: 'de_last_money',label:'上班余款', width: 60, title: false, align: 'right'},
            {name: 'de_petty_cash', index: 'de_petty_cash',label:'备用金', width: 60, title: false, align: 'right'},
            {name: 'cash_name', index: 'cash_name',label:'现金帐户', width: 90, title: false, align: 'right'},
            {name: 'bank_name', index: 'bank_name',label:'刷卡帐户', width: 90, title: false, align: 'right'},
            {name: 'de_vips', index: 'de_vips',label:'新会员', width: 60, align: 'right', title: false},
            {name: 'de_gifts', index: 'de_gifts',label:'礼品数', width: 60, align: 'right', title: false},
            {name: 'de_vip_point', index: 'de_vip_point',label:'会员用积分', width: 60, title: false, align: 'right'}
	    ];
		$('#grid').jqGrid({
			datatype: 'local',
			width: gridWH.w-12,
			height: gridWH.h-20,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.BASEROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:false,title:false},
			shrinkToFit:false,//表格是否自动填充
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'de_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&de_shop_code='+$("#de_shop_code").val();
		params += '&de_em_code='+$("#de_em_code").val();
		params += "&de_state="+$("#de_state").val();
		params += '&jmd='+(THISPAGE.$_jmd.chkVal().join()? 1 : 0);
		return params;
	},
	reset:function(){
		$("#em_name").val("");
		$("#de_em_code").val("");
		$("#shop_name").val("");
		$("#de_shop_code").val("");
		$("#span_state").getCombo().selectByValue("");
		THISPAGE.$_date.setValue(0);
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();