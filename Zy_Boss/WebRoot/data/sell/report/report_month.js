var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.SELL08;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'sell/report/month';

var Utils = {
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:true},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var sp_code = [];
				var sp_name = [];
				for(var i=0;i<selected.length;i++){
					sp_code.push(selected[i].sp_code);
					sp_name.push(selected[i].sp_name);
				}
				$("#sh_shop_code").val(sp_code.join(","));
				$("#shop_name").val(sp_name.join(","));
			},
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#em_name").val(selected.em_name);
				$("#sh_em_code").val(selected.em_code);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#em_name").val(selected.em_name);
					$("#sh_em_code").val(selected.em_code);
				}
			},
			cancel:true
		});
	}
};

var handle = {
	formatRate:function(val, opt, row){
		if(null != row.da_come && "0" != row.da_come){
			return parseFloat(row.sh_count/row.da_come*100).toFixed(2);
		}else if(row.sh_count == 0){
			return "0.00";
		}else{
			return "100.00";
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();
	},
	initDom:function(){
		this.initYear();
		this.initMonth();
	},
	initYear:function(){
		var tim = new Date();
		var currentYear = tim.getFullYear();
		yearInfo = {
			data:{items:[
					{code:parseInt(currentYear)+"",name:parseInt(currentYear)+""},
					{code:parseInt(currentYear-1)+"",name:parseInt(currentYear-1)+""},
					{code:parseInt(currentYear-2)+"",name:parseInt(currentYear-2)+""},
					{code:parseInt(currentYear-3)+"",name:parseInt(currentYear-3)+""},
					{code:parseInt(currentYear-4)+"",name:parseInt(currentYear-4)+""},
					{code:parseInt(currentYear-5)+"",name:parseInt(currentYear-5)+""},
					{code:parseInt(currentYear-6)+"",name:parseInt(currentYear-6)+""},
					{code:parseInt(currentYear-7)+"",name:parseInt(currentYear-7)+""}
				]}
		}
		yearCombo = $('#span_year').combo({
			data:yearInfo.data.items,
			value: 'code',
			text: 'name',
			width :206,
			height:80,
			listId:'',
			defaultSelected: 0,
			editable: true,
			callback:{
				onChange: function(data){
					$("#year").val(data.code);
				}
			}
		}).getCombo();
	},
	initMonth:function(){
		var tim = new Date();
		var currentMonth = tim.getMonth()+1;
		if(currentMonth < 10){
			currentMonth = "0"+currentMonth;
		}
		var monthInfo = [
			          {code:"01",name:"01"}, 
                      {code:"02",name:"02"}, 
                      {code:"03",name:"03"}, 
                      {code:"04",name:"04"}, 
                      {code:"05",name:"05"}, 
                      {code:"06",name:"06"}, 
                      {code:"07",name:"07"}, 
                      {code:"08",name:"08"}, 
                      {code:"09",name:"09"}, 
                      {code:"10",name:"10"},
                      {code:"11",name:"11"},
                      {code:"12",name:"12"}
				];
		monthCombo = $('#span_month').combo({
			value: 'code',
			text: 'name',
			width :206,
			height:80,
			listId:'',
			defaultSelected: 0,
			editable: true,
			callback:{
				onChange: function(data){
					$("#month").val(data.code);
				}
			}
		}).getCombo().loadData(monthInfo,['code',currentMonth,0]);
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
	    	{label:'日期',name: 'sh_date',index:'sh_date',width:50, align:'center'},
	    	{label:'进店数',name: 'da_come',index:'da_come',width:50, align:'right'},
	    	{label:'接待数',name: 'da_receive',index:'da_receive',width:50, align:'right'},
	    	{label:'试穿数',name: 'da_try',index:'da_try',width:50, align:'right'},
	    	{label:'单数',name: 'sh_count',index:'sh_count',width:50, align:'right'},
	    	{label:'购买率',name: 'sh_rate',index:'sh_rate',width:80, align:'right',formatter:handle.formatRate},
	    	{label:'数量',name: 'sh_amount',index:'sh_amount',width:50, align:'right'},
	    	{label:'金额',name: 'sh_money',index:'sh_money',width:70, align:'right',formatter: PriceLimit.formatBySell},
	    	{label:'抹零',name: 'sh_lost_money',index:'sh_lost_money',width:70,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'优惠券',name: 'sh_ec_money',index:'sh_ec_money',width:70,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'代金券',name: 'sh_vc_money',index:'sh_vc_money',width:70,align:'right',formatter: PriceLimit.formatMoney},   	
	    	{label:'储值卡',name: 'sh_cd_money',index:'sh_cd_money',width:70,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'积分抵现',name: 'sh_point_money',index:'sh_point_money',width:60,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'实际金额',name: 'sh_real_money',index:'sh_real_money',width:70,align:'right',formatter: PriceLimit.formatBySell},
	    	{label:'费用开支',name: 'expense_money',index:'expense_money',width:70,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'其他收入',name: 'income_money',index:'income_money',width:70,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'成本金额',name: 'sh_cost_money',index:'sh_cost_money',width:70,align:'right',formatter: PriceLimit.formatByCost},
	    	{label:'毛利润',name: 'sh_profit',index:'sh_profit',width:70,align:'right',formatter: PriceLimit.formatByProfit},
	    	{label:'利润率',name: 'sh_profit_rate',index:'sh_profit_rate',width:70,align:'right',formatter: PriceLimit.formatByProfit}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'json',
			width: gridWH.w-12,
			height: gridWH.h-50,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			page:1,
			pgbuttons:true,
			recordtext:'共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:-1,//每页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
            userDataOnFooter: true,
			jsonReader: {
				root: 'data.list',
				repeatitems : false,
				userdata: 'data.data',
				id: 'sh_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'date='+$("#year").val()+"-"+$("#month").val();
		params += '&sh_shop_code='+$("#sh_shop_code").val();
		return params;
	},
	reset:function(){
		$("#shop_name").val("");
		$("#sh_shop_code").val("");
		THISPAGE.$_date.setValue(0);
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();