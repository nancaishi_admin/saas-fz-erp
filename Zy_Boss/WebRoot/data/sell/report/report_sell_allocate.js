var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.SELL16;
var _menuParam = config.OPTIONLIMIT;
var isRefresh = false;
var queryurl = config.BASEPATH+'sell/report/sell_allocate';

var Utils = {
	doQueryInShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#in_shop_code").val(selected.sp_code);
				$("#in_shop_name").val(selected.sp_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#in_shop_code").val(selected.sp_code);
					$("#in_shop_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	},
	doQueryOutShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#out_shop_code").val(selected.sp_code);
				$("#out_shop_name").val(selected.sp_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#out_shop_code").val(selected.sp_code);
					$("#out_shop_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	}
};
var handle = {
	operFmatter:function(val, opt, row) {
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-image" title="图片">&#xe654;</i>';
		html_con += '</div>';
		return html_con;
	},
	initAcState:function(){
		acStateInfo = {
			data:{items:[
					{state_code:'',state_name:'全部显示'},
					{state_code:'2',state_name:'已完成'},
					{state_code:'1',state_name:'已调出'},
					{state_code:'3',state_name:'已拒收'}
				]}
		}
		acStateCombo = $('#span_ac_state').combo({
			data:acStateInfo.data.items,
			value: 'state_code',
			text: 'state_name',
			width :206,
			height:80,
			listId:'',
			defaultSelected: 0,
			editable: true,
			callback:{
				onChange: function(data){
					$("#ac_state").val(data.state_code);
				}
			}
		}).getCombo();
	},
	formatArState:function(val, opt, row){
		if(val == 0){
			return '暂存';
		}else if(val == 1){
			return '已调出';
		}else if(val == 2){
			return '完成';
		}else if(val == 3){
			return '已拒收';
		}
		return val;
	},
	formatSellMoney :function(val, opt, row){
		if($.trim(val) != ""){
			return val;
		}
		return PriceLimit.formatMoney(row.acl_amount*row.acl_sell_price);
	},
	formatCostMoney :function(val, opt, row){
		if($.trim(val) != ""){
			return val;
		}
		return PriceLimit.formatMoney(row.acl_amount*row.acl_cost_price);
	},
	formatInSellMoney :function(val, opt, row){
		if($.trim(val) != ""){
			return val;
		}
		return PriceLimit.formatMoney(row.acl_amount*row.acl_in_sell_price);
	},
	formatInCostMoney :function(val, opt, row){
		if($.trim(val) != ""){
			return val;
		}
		return PriceLimit.formatMoney(row.acl_amount*row.acl_in_cost_price);
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		dateRedioClick("theDate");
		handle.initAcState();
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
		    {label:'图片',name: 'operate', width: 40, fixed:true, formatter: handle.operFmatter, title: false,sortable:false,align:"center"},
            {label:'',name: 'ac_out_shop',index: 'ac_out_shop',width:100,hidden:true},
	    	{label:'',name: 'ac_in_shop',index: 'ac_in_shop',width:100,hidden:true},
	    	{label:'',name: 'acl_pd_code',index: 'acl_pd_code',width:100,hidden:true},
            {label:'调拨单号',name: 'ac_number',index:'ac_number',width: 170, align:'left',title: false},
            {label:'调出店铺',name: 'outshop_name',index:'outshop_name',width:110,align:'left', title: false,fixed:true},
            {label:'调入店铺',name: 'inshop_name',index:'inshop_name',width:110, title: false},
            {label:'状态',name: 'ac_state',index: 'ac_state',width:60,formatter: handle.formatArState,align:'center'},
            {label:'调拨日期',name: 'ac_date',index:'ac_date',align:'left',width:80, title: false},
            {label:'商品货号',name: 'pd_no',index:'pd_no',width: 110,align:'left', title: false, fixed:true},
            {label:'商品名称',name: 'pd_name',index:'pd_name',width:120, title: false},     
            {label:'颜色',name: 'cr_name',index:'cr_name',width:60, title: false,align: 'center'},
            {label:'尺码',name: 'sz_name',index:'sz_name',width:60, title: false,align: 'center'},
            {label:'杯型',name: 'br_name',index:'br_name',width:60,align:'center',title:false},           
            {label:'数量',name: 'acl_amount',index:'acl_amount',width:70, title: false,align: 'right'},  
            {label:'调出零售价',name: 'acl_sell_price', index: 'acl_sell_price', width: 80,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'调出零售金额',name: 'acl_sell_money', index: 'acl_sell_money', width: 80,align:'right',formatter: handle.formatSellMoney,sortable:false},
	    	{label:'调出成本价',name: 'acl_cost_price', index: 'acl_cost_price', width: 80,align:'right',formatter: PriceLimit.formatMoney},
		    {label:'调出成本金额',name: 'acl_cost_money', index: 'acl_cost_money', width: 80,align:'right',formatter: handle.formatCostMoney,sortable:false},
	    	{label:'调入零售价',name: 'acl_in_sell_price', index: 'acl_in_sell_price', width: 80,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'调入零售金额',name: 'acl_in_sell_money', index: 'acl_in_sell_money', width: 80,align:'right',formatter: handle.formatInSellMoney,sortable:false},
	    	{label:'调入成本价',name: 'acl_in_cost_price', index: 'acl_in_cost_price', width: 80,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'调入成本金额',name: 'acl_in_cost_money', index: 'acl_in_cost_money', width: 80,align:'right',formatter: handle.formatInCostMoney,sortable:false},
            {label:'单位',name: 'pd_unit',index:'pd_unit',width:60, title: false,align: 'center'},  
            {label:'经办人',name: 'ac_man',index:'ac_man',width:90,align:'center', title: false},
            {label:'接收人',name: 'ac_receiver',index:'ac_receiver',width:90,align:'center', title: false},
            {label:'接收日期',name: 'ac_recedate',index: 'ac_recedate',width:100}
	    ];
		$('#grid').jqGrid({
			url:queryurl,
			datatype: 'local',
			width: gridWH.w-12,
			height: gridWH.h-46,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager:'#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.DATAROWNUM,//每页条数
			rowList:config.DATAROWLIST,//分页条数
			autoScroll: true,
			footerrow :true,
			userDataOnFooter : true,
			shrinkToFit:false,//表格是否自动填充
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
                userdata: 'data.data',
				repeatitems : false,
				id: 'acl_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
				handle.operate('',rowid);
			},
			onPaging: function(pgButton) {
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&in_shop_code='+$("#in_shop_code").val();
		params += '&out_shop_code='+$("#out_shop_code").val();
		params += '&ac_state='+$("#ac_state").val();
		params += '&pd_no='+$("#pd_no").val();
		params += '&ac_number='+$("#ac_number").val();
		return params;
	},
	reset:function(){
		$("#in_shop_code").val("");
		$("#in_shop_name").val("");
		$("#out_shop_code").val("");
		$("#out_shop_name").val("");
		$("#pd_no").val("");
		$("#ac_number").val("");
		THISPAGE.$_date.setValue(0);
		dateRedioClick("theDate");
		handle.initAcState();
	},
	reloadData:function(data){
		var params=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+"?"+params}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn_search').on('click', function(e){
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			};
			THISPAGE.reloadData();
		});
		//查询
		$('#btn_ok').on('click', function(e){
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			};
			THISPAGE.reloadData();
		});
		//重置
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
		 //查看图片
		$('#grid').on('click', '.operating .ui-icon-image', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
        	viewProductImg($("#grid").jqGrid("getRowData", id).acl_pd_code);
        });
	}
}

THISPAGE.init();