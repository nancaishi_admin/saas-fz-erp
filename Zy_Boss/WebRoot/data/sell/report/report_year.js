var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.SELL08;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'sell/report/year';

var Utils = {
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#sh_shop_code").val(selected.sp_code);
				$("#shop_name").val(selected.sp_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#sh_shop_code").val(selected.sp_code);
					$("#shop_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#em_name").val(selected.em_name);
				$("#sh_em_code").val(selected.em_code);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#em_name").val(selected.em_name);
					$("#sh_em_code").val(selected.em_code);
				}
			},
			cancel:true
		});
	}
};

var handle = {
	formatRate:function(val, opt, row){
		if(null != row.da_come && "0" != row.da_come){
			return parseFloat(row.sh_count/row.da_come*100).toFixed(2);
		}else if(row.sh_count == 0){
			return "0.00";
		}else{
			return "100.00";
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();
	},
	initDom:function(){
		this.initYear();
	},
	initYear:function(){
		var tim = new Date();
		var currentYear = tim.getFullYear();
		yearInfo = {
			data:{items:[
					{code:parseInt(currentYear)+"",name:parseInt(currentYear)+""},
					{code:parseInt(currentYear-1)+"",name:parseInt(currentYear-1)+""},
					{code:parseInt(currentYear-2)+"",name:parseInt(currentYear-2)+""},
					{code:parseInt(currentYear-3)+"",name:parseInt(currentYear-3)+""},
					{code:parseInt(currentYear-4)+"",name:parseInt(currentYear-4)+""},
					{code:parseInt(currentYear-5)+"",name:parseInt(currentYear-5)+""},
					{code:parseInt(currentYear-6)+"",name:parseInt(currentYear-6)+""},
					{code:parseInt(currentYear-7)+"",name:parseInt(currentYear-7)+""}
				]}
		}
		yearCombo = $('#span_year').combo({
			data:yearInfo.data.items,
			value: 'code',
			text: 'name',
			width :206,
			height:80,
			listId:'',
			defaultSelected: 0,
			editable: true,
			callback:{
				onChange: function(data){
					$("#year").val(data.code);
				}
			}
		}).getCombo();
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
	    	{label:'月份',name: 'sh_date',index:'sh_date',width:50, align:'center'},
	    	{label:'单据数',name: 'sh_count',index:'sh_count',width:50, align:'right'},
	    	{label:'数量',name: 'sh_amount',index:'sh_amount',width:50, align:'right'},
	    	{label:'金额',name: 'sh_money',index:'sh_money',width:70, align:'right',formatter: PriceLimit.formatBySell},
	    	{label:'抹零',name: 'sh_lost_money',index:'sh_lost_money',width:70,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'优惠券',name: 'sh_ec_money',index:'sh_ec_money',width:70,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'代金券',name: 'sh_vc_money',index:'sh_vc_money',width:70,align:'right',formatter: PriceLimit.formatMoney},   	
	    	{label:'储值卡',name: 'sh_cd_money',index:'sh_cd_money',width:70,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'积分抵现',name: 'sh_point_money',index:'sh_point_money',width:60,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'实际金额',name: 'sh_real_money',index:'sh_real_money',width:70,align:'right',formatter: PriceLimit.formatBySell},
	    	{label:'实际金额',name: 'sh_cost_money',index:'sh_cost_money',width:70,align:'right',formatter: PriceLimit.formatByCost},
	    	{label:'毛利润',name: 'sh_profit',index:'sh_profit',width:70,align:'right',formatter: PriceLimit.formatByProfit},
	    	{label:'利润率',name: 'sh_profit_rate',index:'sh_profit_rate',width:70,align:'right',formatter: PriceLimit.formatByProfit}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'json',
			width: gridWH.w-12,
			height: gridWH.h-50,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			page:1,
			pgbuttons:true,
			recordtext:'共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:-1,//每页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
            userDataOnFooter: true,
			jsonReader: {
				root: 'data.list',
				repeatitems : false,
				userdata: 'data.data',
				id: 'sh_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'date='+$("#year").val();
		params += '&sh_shop_code='+$("#sh_shop_code").val();
		return params;
	},
	reset:function(){
		$("#shop_name").val("");
		$("#sh_shop_code").val("");
		THISPAGE.$_date.setValue(0);
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();