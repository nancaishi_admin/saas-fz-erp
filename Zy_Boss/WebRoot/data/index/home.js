var config = parent.CONFIG;
var system = parent.SYSTEM;

var gaugeOptions = {
    chart: {
        type: 'solidgauge'
    },
    title: null,
    pane: {
        center: ['50%', '85%'],
        size: '140%',
        startAngle: -90,
        endAngle: 90,
        background: {
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
            innerRadius: '60%',
            outerRadius: '100%',
            shape: 'arc'
        }
    },
    tooltip: {
        enabled: false
    },
    // the value axis
    yAxis: {
        stops: [
            [0.1, '#55BF3B'], // green
            [0.5, '#DDDF0D'], // yellow
            [0.9, '#DF5353'] // red
        ],
        lineWidth: 0,
        minorTickInterval: null,
        tickPixelInterval: 400,
        tickWidth: 0,
        title: {
            y: -70
        },
        labels: {
            y: 16
        }
    },
    plotOptions: {
        solidgauge: {
            dataLabels: {
                y: 5,
                borderWidth: 0,
                useHTML: true
            }
        }
    }
};

function formatByDefault(val){
	if($.trim(val) == ""){
		return 0;
	}
	return val;
}

var THISPAGE = {
	init:function(){
		this.initDom();
		this.loadHomeInfo();
		this.statSellByDay();
		this.initPlanProgress();
	},
	initDom:function(){
		if(system.SHOP_TYPE == 1 || system.SHOP_TYPE == 2){
		}else{
			$("#bank_div").hide();
			$("#client_supply_div").hide();
			$("#expense_income_div").show();
		}
	},
	loadHomeInfo:function(){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'home/loadHomeInfo',
			data:{},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					var thisMonth = data.data.thisMonth;
					var lastMonth = data.data.lastMonth;
					var businessInfo = data.data.businessInfo;
					$("#comeAmount_last").text(formatByDefault(lastMonth.comeAmount));
					$("#receiveAmount_last").text(formatByDefault(lastMonth.receiveAmount));
					$("#tryAmount_last").text(formatByDefault(lastMonth.tryAmount));
					$("#dealCount_last").text(formatByDefault(lastMonth.dealCount));
					$("#returnDealCount_last").text(formatByDefault(lastMonth.returnDealCount));
					$("#jointRate_last").text(formatByDefault(lastMonth.jointRate));
					$("#sellMoney_last").text(formatByDefault(lastMonth.sellMoney));
					$("#avgSellPrice_last").text(formatByDefault(lastMonth.avgSellPrice));
					$("#newVipCount_last").text(formatByDefault(lastMonth.newVipCount));
					$("#newCardCount_last").text(formatByDefault(lastMonth.newCardCount));
					
					$("#comeAmount_this").text(formatByDefault(thisMonth.comeAmount));
					$("#receiveAmount_this").text(formatByDefault(thisMonth.receiveAmount));
					$("#tryAmount_this").text(formatByDefault(thisMonth.tryAmount));
					$("#dealCount_this").text(formatByDefault(thisMonth.dealCount));
					$("#returnDealCount_this").text(formatByDefault(thisMonth.returnDealCount));
					$("#jointRate_this").text(formatByDefault(thisMonth.jointRate));
					$("#sellMoney_this").text(formatByDefault(thisMonth.sellMoney));
					$("#avgSellPrice_this").text(formatByDefault(thisMonth.avgSellPrice));
					$("#newVipCount_this").text(formatByDefault(thisMonth.newVipCount));
					$("#newCardCount_this").text(formatByDefault(thisMonth.newCardCount));
					//业务简报
					$("#sellMoney").html(formatByDefault(thisMonth.sellMoney));
					$("#sellProfits").html(formatByDefault(thisMonth.sellProfits));
					$("#sd_amount").html(formatByDefault(businessInfo.sd_amount));
					$("#sd_costmoney").html(formatByDefault(businessInfo.sd_costmoney));
					if(system.SHOP_TYPE == 1 || system.SHOP_TYPE == 2){
						$("#ba_balance").html(formatByDefault(businessInfo.ba_balance));
						$("#ba_balance_cash").html(formatByDefault(businessInfo.ba_balance_cash));
						$("#client_debt").html(formatByDefault(businessInfo.client_debt));
						$("#supply_debt").html(formatByDefault(businessInfo.supply_debt));
					}else{
						$("#expense").html(formatByDefault(businessInfo.expense));
						$("#income").html(formatByDefault(businessInfo.income));
					}
				}
			}
		});
	},
	statSellByDay:function(){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'home/statSellByDay',
			data:{},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					THISPAGE.initColumn(data.data);
				}
			}
		});
	},
	initPlanProgress:function(){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'shop/plan/statByYearMonth',
			data:{},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					THISPAGE.buildMonthProgress(data.data.planmoney_month,data.data.realmoney_month);
					THISPAGE.buildYearProgress(data.data.planmoney_year,data.data.realmoney_year);
				}
			}
		});
	},
	initColumn:function(data){
		var categories = [];
		var sellData = [];
		for (var i = 0; i < data.length; i++) {
			categories.push(data[i].day);
			sellData.push(parseFloat(data[i].sellMoney));
		}
		$('#container').highcharts({
	        chart: {type: 'column'},
	        title: {text: ''},
	        subtitle: {text: ''},
	        yAxis: {min: 0,
	            title: {
	                text: '金额'
	            }
	        },
	        xAxis: {categories: categories,crosshair: true},
	        tooltip: {
	            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	            '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
	            footerFormat: '</table>',
	            shared: true,
	            useHTML: true
	        },
	        plotOptions: {
	            column: {
	                pointPadding: 0.2,
	                borderWidth: 0
	            }
	        },
	        series: [{
	            name: '月零售情况',
	            data: sellData
	        }]
	    });
	},
	buildMonthProgress:function(planMoney,realMoney){
		var monthPercent = 0;
		if(planMoney != 0){
			monthPercent = parseInt(realMoney*100/planMoney);
		}
		$('#container-month').highcharts(Highcharts.merge(gaugeOptions, {
	        yAxis: {
	            min: 0,
	            max: 100,
	            title: {
	                text: '本月完成情况'
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        series: [{
	            name: '本月完成',
	            data: [monthPercent],
	            dataLabels: {
	                format: '<div style="text-align:center"><br/><span style="font-size:18px;color:' +
	                ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}%</span><br/>' +
	                '<span style="font-size:12px;color:silver">￥'+realMoney+'/'+planMoney+'</span></div>'
	            },
	            tooltip: {
	                valueSuffix: ''
	            }
	        }]
	    }));
	},
	buildYearProgress:function(planMoney,realMoney){
		var yearPercent = 0;
		if(planMoney != 0){
			yearPercent = parseInt(realMoney*100/planMoney);
		}
		$('#container-year').highcharts(Highcharts.merge(gaugeOptions, {
	        yAxis: {
	            min: 0,
	            max: 100,
	            title: {
	                text: '本年完成情况'
	            }
	        },
	        series: [{
	            name: '本年完成',
	            data: [yearPercent],
	            dataLabels: {
	                format: '<div style="text-align:center"><span style="font-size:18px;color:' +
	                ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}%</span><br/>' +
	                '<span style="font-size:12px;color:silver">￥'+realMoney+'/'+planMoney+'</span></div>'
	            },
	            tooltip: {
	                valueSuffix: '%'
	            }
	        }]
	    }));
	}
}
THISPAGE.init();