var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var api = frameElement.api;
var multiselect = false;
if(undefined != api.data.multiselect){
	multiselect = api.data.multiselect;
}
var _height = api.config.height-132,_width = api.config.width-34;//获取弹出框弹出宽、高
var queryurl = config.BASEPATH+'buy/supply/list';
var dblClick = false;
var THISPAGE = {
	init:function (){
		this.initGrid();
		this.initEvent();	
	},
	initGrid:function(){
		var colModel = [
			{name: 'sp_code', label:'供应商编号',index: 'sp_code', width: 80, title: false},
	    	{name: 'sp_name', label:'供应商名称',index: 'sp_name', width: 140, title: false},
	    	{name: 'sp_rate',label:'折扣率', index: 'sp_rate',sorttype: 'float',width:80, title: false,align:'right'},
	    	{name: 'sp_payable',label:'', index: 'sp_payable',width:80,align:'right', hidden: true},
	    	{name: 'sp_payabled',label:'', index: 'sp_payabled',width:80,align:'right', hidden: true},
	    	{name: 'sp_prepay',label:'', index: 'sp_prepay',width:80,align:'right', hidden: true}
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:multiselect,//多选
			viewrecords: true,
			rowNum:999,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'sp_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow: function (rowid, iRow, iCol, e) {
				if(!multiselect){
					dblClick = true;
					api.close();
				}
            }
	    });
	},
	reloadData:function(data){
		var param="";
		if(data != null && "" != data){
			param += "?1=1";
			if(data.hasOwnProperty("searchContent") && data.searchContent != ''){
				param += "&searchContent="+Public.encodeURI(data.searchContent);
			}
		}
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		$('#SearchContent').on('keyup',function(event){
			if (event.keyCode==13){
				$('#btn-search').click();
			}
		});
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			var searchContent = $.trim($('#SearchContent').val());
			var pdata = {};
			pdata.searchContent = searchContent;
			THISPAGE.reloadData(pdata);
		});
	}
};
function doSelect(){
	if(multiselect){//多选
		var ids = $("#grid").jqGrid('getGridParam', 'selarrrow');
		if(ids == null || ids.length == 0){
			Public.tips({type: 2, content : "请选择供应商！"});
			return false;
		}
		var result = [];
		for (var i = 0; i < ids.length; i++) {
			result.push($("#grid").jqGrid("getRowData", ids[i]));
		}
		return result;
		
	}else{//单选
		var selectedId = $("#grid").jqGrid('getGridParam', 'selrow');
		if(selectedId == null || selectedId == ""){
			Public.tips({type: 2, content : "请选择供应商！"});
			return false;
		}
		var rowData = $("#grid").jqGrid("getRowData", selectedId);
		return rowData;
	}
}
THISPAGE.init();