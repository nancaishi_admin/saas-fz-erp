var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var handle = {
	save:function(){
		var name = $("#sp_name").val().trim();
	    if(name == ''){
	    	W.Public.tips({type: 1, content : "请输入供货商名称"});
	        $("#sp_name").select().focus();
	     	setTimeout("api.zindex()",400);
	        return;
	    }else{
	    	$("#sp_name").val(name);//防止空格
	    }
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"buy/supply/update",
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					W.Public.tips({type: 3, content : data.message});
					handle.loadData(data.data);
					setTimeout("api.zindex()",500);
					api.close();
				}else if(data.stat == 304){
					W.Public.tips({type: 1, content : data.message});
					setTimeout("api.zindex()",500);
				}else{
					W.Public.tips({type: 1, content : data.message});
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.sp_id;
		pdata.sp_code=data.sp_code;
		pdata.sp_name=data.sp_name;
		pdata.spi_man=data.spi_man;
		pdata.spi_tel=data.spi_tel;
		pdata.spi_mobile=data.spi_mobile;
		pdata.sp_rate=data.sp_rate;
		pdata.sp_buy_cycle=data.sp_buy_cycle;
		pdata.sp_settle_cycle=data.sp_settle_cycle;
		pdata.spi_earnest=data.spi_earnest;
		pdata.spi_deposit=data.spi_deposit;
		pdata.spi_addr=data.spi_addr;
		pdata.spi_remark=data.spi_remark;
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	},
	checkMobile:function(obj){ //验证手机号码
	    var s = obj.value;
		var regu =/^[1-9][0-9]{10}$/; 
		if(s != ''){
			if (!regu.test(s)){ 
				W.Public.tips({type: 1, content : "请输入正确的手机号码!"});
			    obj.value="";
			    setTimeout("api.zindex()",400);
			    return false; 
			}
		}
    },
    doOnlyDouble:function(obj){//验证是否是0-1之间的数字
		var discount = obj.value;
	    if(isNaN(discount)){
	    	W.Public.tips({type: 1, content : "请输入数字!!!"});
		    obj.value="";
		    obj.focus();
		    setTimeout("api.zindex()",400);
		    return;
	    }
	    if(discount<0 || discount>1){
	    	W.Public.tips({type: 1, content : "数请输入0-1之间的数据!!!"});
		    obj.value="";
		    obj.focus();
		    setTimeout("api.zindex()",400);
		    return;
	    }
    },
    isTel:function(obj){
    	var str = obj.value;
    	var cellphone=/^([\d-+]*)$/;
    	if(str!=''){
    		if(!cellphone.test(str)){
    			W.Public.tips({type: 1, content : "请输入正确的电话号码!"});
    			obj.value="";
        	    obj.focus();
        	    setTimeout("api.zindex()",400)
        	    return;
    		}
    	}
    },
    doCheckNumber:function(obj){
    	var discount = obj.value;
    	if(isNaN(discount)){
    		W.Public.tips({type: 1, content : "请输入数字!"});
    		obj.value="";
    	    obj.focus();
    	    setTimeout("api.zindex()",400)
    	    return;
    	}
    }
};
var THISPAGE = {
	init:function (){
		this.areaCombo;
		
		this.initArea();
		this.initParam();
		this.initDom();
		this.initEvent();
	},
	initArea:function(){
		$.ajax({
			type:"POST",
			url:config.BASEPATH +"base/area/listBySupply",
			data:"",
			cache:false,
			dataType:"json",
			success:function(data){
				var seasonInfo = {};
				var selectArea = $("#sp_ar_code").val();
				var index = 0,_sp_ar_name="";
				for(key in data.data.items){
					if(data.data.items[key].sp_ar_code ==  selectArea){
						index = key;
						_sp_ar_name = data.data.items[key].sp_ar_name;
						break;
					}
				}
				THISPAGE.areaCombo = $('#span_sp_ar_Code').combo({
					data:data.data.items,
					value: 'sp_ar_code',
					text: 'sp_ar_name',
					width :207,
					height: 30,
					listId:'',
					defaultSelected: index,
					editable: true,
					ajaxOptions:{
						formatData: function(data){
							seasonInfo = data.data.items;
							return data.data.items;
						}
					},
					callback:{
						onChange: function(data){
							$("#sp_ar_code").val(data.sp_ar_code);
							$("#sp_ar_name").val(data.sp_ar_name);
						}
					}
				}).getCombo(); 
			}
		});
	},
	initParam:function(){
	},
	initDom:function(){
	},
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save();
		});
		$("#btn_close").click(function(){
			api.close();
		});
	}
};
THISPAGE.init();