var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var api = frameElement.api;
var et_type = $("#et_type").val();
var queryurl = config.BASEPATH+'buy/enter/page';
var _height = api.config.height-132,_width = api.config.width-34;//获取弹出框弹出宽、高
var Utils = {
	doQuerySupply : function(){
		commonDia = $.dialog({
			title : '选择供货厂商',
			content : 'url:'+config.BASEPATH+'buy/supply/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#et_supply_code").val(selected.sp_code);
					$("#supply_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	},
	doQueryDepot : function(){
		commonDia = $.dialog({
			title : '选择仓库',
			content : 'url:'+config.BASEPATH+'base/depot/to_list_dialog_bymodule',
			data : {multiselect:false,module:'buy'},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#et_depot_code").val(selected.dp_code);
					$("#depot_name").val(selected.dp_name);
				}
			},
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#et_manager").val(selected.em_name);
				}
			},
			cancel:true
		});
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
	},
	initGrid:function(){
		var colModel = [
	    	{name: 'et_supply_code',label:'',index: 'et_supply_code',hidden:true},
	    	{name: 'et_depot_code',label:'',index: 'et_depot_code',hidden:true},
	    	{name: 'et_handnumber',label:'',index: 'et_handnumber',hidden:true},
	    	{name: 'et_number',label:'单据编号',index: 'et_number',width:150},
	    	{name: 'et_order_number',label:'订单编号',index: 'et_order_number',width:150},
	    	{name: 'supply_name',label:'供货厂商',index: 'supply_name',width:120},
	    	{name: 'depot_name',label:'收货仓库',index: 'depot_name',width:120},
	    	{name: 'et_manager',label:'经办人',index: 'et_manager',width:100},
	    	{name: 'et_amount',label:'总计数量',index: 'et_amount',width:80,align:'right'},
	    	{name: 'et_money',label:'进货金额',index: 'et_money',width:80,align:'right'},
	    	{name: 'et_retailmoney',label:'零售金额',index: 'et_retailmoney',width:80,align:'right'},
	    	{name: 'et_make_date',label:'制单日期',index: 'et_make_date',width:80},
	    	{name: 'et_maker',label:'制单人',index: 'et_maker',width:100}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.BASEROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'et_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow: function (rowid, iRow, iCol, e) {
				api.close();
            }
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'et_type='+$("#et_type").val();
		params += '&et_isdraft=1';
		params += '&begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&et_supply_code='+$("#et_supply_code").val();
		params += '&et_depot_code='+$("#et_depot_code").val();
		params += '&et_manager='+Public.encodeURI($("#et_manager").val());
		params += '&et_number='+Public.encodeURI($("#et_number").val());
		return params;
	},
	reset:function(){
		$("#et_supply_code").val("");
		$("#et_depot_code").val("");
		$("#et_number").val("");
		$("#et_manager").val("");
		$("#supply_name").val("");
		$("#depot_name").val("");
		THISPAGE.$_date.setValue(0);
		$("#begindate").val("");
		$("#enddate").val("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
};

function doSelect(){
	var selectedId = $("#grid").jqGrid('getGridParam', 'selrow');
	if(selectedId == null || selectedId == ""){
		Public.tips({type: 2, content : "请选择草稿！"});
		return false;
	}
	var rowData = $("#grid").jqGrid("getRowData", selectedId);
	return rowData;
}

THISPAGE.init();