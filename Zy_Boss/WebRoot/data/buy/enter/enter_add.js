var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var isRefresh = false;
var et_type = $("#et_type").val();
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var queryurl = config.BASEPATH+'buy/enter/temp_list/'+et_type;
var querysumurl = config.BASEPATH+'buy/enter/temp_sum/'+et_type;
var querysizeurl = config.BASEPATH+'buy/enter/temp_size/'+et_type;

var needListRefresh = false;//列表模式
var needSizeRefresh = false;//尺码模式
var needSumRefresh = false;//汇总模式
var _height = $(parent).height()-330,_width = $(parent).width()-2;
var showMode = {
	display:function(mode){
		var ids = $("#grid").getDataIDs();
		if(ids == undefined || null == ids || ids.length == 0){
			if(mode != 0){
				Public.tips({type: 1, content : "未添加数据，不能切换模式"});
				return;
			}
		}
		$("#CurrentMode").val(mode);
		$("a[name='mode-btn']").removeClass("on");
		$("#list-grid").hide();
		$("#size-grid").hide();
		$("#sum-grid").hide();
		if(mode == 0){
			$("#listBtn").addClass("on");
			$("#list-grid").show();
			showMode.refreshList();
		}else if(mode == 1){
			$("#sizeBtn").addClass("on");
			$("#size-grid").show();
			showMode.refreshSize();
		}else if(mode == 2){
			$("#sumBtn").addClass("on");
			$("#sum-grid").show();
			showMode.refreshSum();
		}
	},
	refreshList:function(){//列表模式刷新数据
		if(!needListRefresh){
    		return;
    	}
    	THISPAGE.reloadGridData();
    	needListRefresh = false;
	},
	refreshSum:function(){
		if(!needSumRefresh){
			return;
		}
		$('#sumGrid').GridUnload();
		THISPAGE.addEvent();
		THISPAGE.initSumGrid();
		needSumRefresh = false;
	},
	refreshSize:function(){
		if(!needSizeRefresh){
			return;
		}
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'buy/enter/temp_size_title/'+et_type,
			cache:false,
			dataType:"json",
            success: function (data) {
            	if(undefined != data && data.stat == 200){
                    var titles = data.data.titles;
                    var sizeCodeMap = data.data.sizeCodeMap;
                    $('#sizeGrid').GridUnload();
                    THISPAGE.initSizeGroupGrid(titles, sizeCodeMap);
                    THISPAGE.addEvent();
                    needSizeRefresh = false;
				}else{
					Public.tips({type: 1, content : data.message});
				}
            }
        });
	},
	refresh:function(){
		var currentMode = $("#CurrentMode").val();
    	if(currentMode == '0'){//列表模式
    		needListRefresh = true;
    		showMode.refreshList();
        	needSizeRefresh = true;
        	needSumRefresh = true;
    	}else if(currentMode == '1'){//尺码模式
    		needSizeRefresh = true;
    		showMode.refreshSize();
    		needListRefresh = true;
        	needSumRefresh = true;
    	}else if(currentMode == '2'){//汇总模式
    		needSumRefresh = true;
    		showMode.refreshSum();
    		needListRefresh = true;
        	needSizeRefresh = true;
    	}
	}
};

var Choose = {
	importOrder:function(){
		commonDia = $.dialog({
			title : '选择订单',
			content : 'url:'+config.BASEPATH+'buy/enter/to_list_order_dialog/'+et_type,
			data : {},
			width : 1080,
			height : 440,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var mainData = selected.mainData;
				$("#et_supply_code").val(mainData.od_supply_code);
				$("#et_depot_code").val(mainData.od_depot_code);
				$("#supply_name").val(mainData.supply_name);
				$("#depot_name").val(mainData.depot_name);
				$("#et_manager").val(mainData.od_manager);
				Choose.doImportOrder(selected.detailIds);
			},
			close:function(){
			},
			cancel:true
		});
	},
	doImportOrder:function(detailIds){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'buy/enter/temp_import_order/'+et_type,
			data:{ids:detailIds.join(",")},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : "导入成功！"});
					showMode.refresh();
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
	},
	importDraft:function(){
		commonDia = $.dialog({
			title : '选择草稿',
			content : 'url:'+config.BASEPATH+'buy/enter/to_list_draft_dialog/'+et_type,
			data : {},
			width : 680,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#et_supply_code").val(selected.et_supply_code);
					$("#et_depot_code").val(selected.et_depot_code);
					$("#supply_name").val(selected.supply_name);
					$("#depot_name").val(selected.depot_name);
					$("#et_make_date").val(selected.et_make_date);
					$("#et_handnumber").val(selected.et_handnumber);
					$("#et_manager").val(selected.et_manager);
					$("#et_remark").val(selected.et_remark);
					Choose.doImportDraft(selected);
				}
			},
			cancel:true
		});
	},
	doImportDraft:function(rowData){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'buy/enter/temp_import_draft/'+et_type,
			data:{et_number:rowData.et_number},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : "导入成功！"});
					showMode.refresh();
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
	},
	doImport:function(){
		isRefresh = false;
		var priceType = THISPAGE.$_priceType.getValue();
		var sp_rate = $("#sp_rate").val();
		var et_supply_code = $("#et_supply_code").val();
		if(et_supply_code == ""){
			Public.tips({type: 2, content : '请选择供应商!'});
			return;
		}
		$.dialog({ 
		   	id:'import',
		   	title:'盘点机导入',
		   	content:'url:'+config.BASEPATH+'common/to_barcodeimport',
		   	data : {postData:{priceType:priceType,sp_rate:sp_rate},
		   			saveSuccUrl:config.BASEPATH+"buy/enter/temp_import/"+et_type
		   		},
		   	max: false,
		   	min: false,
		   	lock:true,
		   	width:700,
		   	height:470,
		   	fixed:false,
		   	drag: true,
		   	resize:false,
		   	lock:true,
		   	close: function () {
		   		if(isRefresh){
		   			showMode.refresh();
		   		}
			}
		});
	},
	selectProduct:function(){
		var priceType = THISPAGE.$_priceType.getValue();
		var sp_rate = $("#sp_rate").val();
		var et_supply_code = $.trim($("#et_supply_code").val());
		var et_depot_code = $.trim($("#et_depot_code").val());
		if (et_supply_code == '') {
			Public.tips({type: 2, content : "请先选择供应商！"});
	        return;
	    }
		if (et_depot_code == '') {
			Public.tips({type: 2, content : "请先选择仓库！"});
	        return;
	    }
		$.dialog({
			title : '商品录入',
			content : 'url:'+config.BASEPATH+'buy/enter/to_select_product/'+et_type,
			data : {priceType:priceType,sp_rate:sp_rate,dp_code:et_depot_code,searchContent:$.trim($("#pd_no").val())},
			width : 1080,
			height : 440,
			max : false,
			min : false,
			cache : false,
			lock: true,
			close:function(){
				showMode.refresh();
			}
		});
	},
	barCode:function(){
		var CurrentMode = $("#CurrentMode").val();
		if(CurrentMode == 1){//若是尺码模式调整到列表模式
			showMode.display(0);
		}
		var barcode = $.trim($("#barcode").val());
		var barcode_amount = $.trim($("#barcode_amount").val());
		if(barcode == ""){
			return;
		}
		if (barcode_amount == "" || isNaN(barcode_amount)) {
			Public.tips({type: 2, content : '请正确输入数量!'});
	        $('#barcode_amount').select();
	        return;
	    }
		var et_supply_code = $("#et_supply_code").val();
		var et_depot_code = $("#et_depot_code").val();
		if(et_supply_code == ""){
			Public.tips({type: 2, content : '请选择供应商!'});
			return;
		}
		if(et_depot_code == ""){
			Public.tips({type: 2, content : '请选择仓库!'});
			return;
		}
		var priceType = THISPAGE.$_priceType.getValue();
		var params = "";
		params += "barcode="+barcode;
		params += "&amount="+barcode_amount;
		params += "&priceType="+priceType;
		params += "&sp_rate="+$("#sp_rate").val();
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"buy/enter/temp_save_bybarcode/"+et_type,
			data:params,
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					if(data.data.result == 3){//条码不存在
						Choose.barcodeNotExist(barcode);
						return;
					}
					needSizeRefresh = true;
					$("#barcode").val("");
					var temp = data.data.temp;
					//列表模式
					if(data.data.result == 1){//update
						var rowData = $("#grid").jqGrid("getRowData", temp.etl_id);
						rowData.etl_amount = temp.etl_amount;
						rowData.etl_unitmoney = rowData.etl_amount * rowData.etl_unitprice;
						rowData.etl_retailmoney = rowData.etl_amount * rowData.etl_retailprice;
						$("#grid").delRowData(temp.etl_id);
						$("#grid").addRowData(temp.etl_id, rowData, 'first');
						THISPAGE.gridTotal();
					}else if(data.data.result == 2){//add
						$("#grid").addRowData(temp.etl_id, temp, 'first');
						THISPAGE.gridTotal();
					}
					//汇总模式
			    	var ids = $("#sumGrid").jqGrid('getDataIDs');
	            	var oldSumRowData = null;
	            	var sum_id = null;
					for(var i=0;i < ids.length;i++){
						var sumRowData = $("#sumGrid").jqGrid("getRowData", ids[i]);
						if(sumRowData.pd_code == temp.pd_code && sumRowData.etl_pi_type == '0'){
							oldSumRowData = sumRowData;
							sum_id = ids[i];
							break;
						}
					}
					if(oldSumRowData != null){
						oldSumRowData.etl_amount = parseInt(oldSumRowData.etl_amount) + parseInt(barcode_amount);
						oldSumRowData.etl_unitmoney = oldSumRowData.etl_amount * oldSumRowData.etl_unitprice;
						oldSumRowData.etl_retailmoney = oldSumRowData.etl_amount * oldSumRowData.etl_retailprice;
						$("#sumGrid").jqGrid('setRowData', sum_id, oldSumRowData);
						THISPAGE.sumGridTotal();
					}else{
						$("#sumGrid").addRowData(temp.etl_id, temp, 'first');
						THISPAGE.sumGridTotal();
					}
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
	},
	barcodeNotExist:function(barcode){
		$("#barcode").val("无此商品").select();
		var BarCodeCheckNotPass = document.getElementById("BarCodeCheckNotPass");
		if(BarCodeCheckNotPass.style.display == 'none'){
			BarCodeCheckNotPass.style.display = '';
			
			var barCodeDiv=$("#BarCodeCheckNotPass");
			var barCodeDivTop=$("#barcode_amount").offset().top
				,barCodeDivLeft=$("#barcode_amount").offset().left;
			barCodeDiv.css(
			{
				"top":barCodeDivTop-150
				,"left":barCodeDivLeft+30
			}
			);
		}
		var CheckNotPassText = document.getElementById("CheckNotPassText");
		if($("#CheckNotPassTextShow").val().indexOf(barcode+";") > -1){
			return;
		}
		CheckNotPassText.innerHTML = CheckNotPassText.innerHTML + "<p>" + barcode + "</p>";
		$("#CheckNotPassTextShow").val($("#CheckNotPassTextShow").val()+barcode+";");
	}
};

var Utils = {
	doQuerySupply : function(){
		commonDia = $.dialog({
			title : '选择供货厂商',
			content : 'url:'+config.BASEPATH+'buy/supply/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#et_supply_code").val(selected.sp_code);
					$("#supply_name").val(selected.sp_name);
					$("#sp_rate").val(selected.sp_rate);
				}
			},
			cancel:true
		});
	},
	doQueryDepot : function(){
		commonDia = $.dialog({
			title : '选择仓库',
			content : 'url:'+config.BASEPATH+'base/depot/to_list_dialog_bymodule',
			data : {multiselect:false,module:'buy'},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#et_depot_code").val(selected.dp_code);
					$("#depot_name").val(selected.dp_name);
				}
			},
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#et_manager").val(selected.em_name);
				}
			},
			cancel:true
		});
	}
};

var handle = {
	temp_updateRemarkById:function(rowid,remark){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'buy/enter/temp_updateRemarkById',
			data:{etl_id:rowid,etl_remark:Public.encodeURI(remark)},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					var rowData = $("#grid").jqGrid("getRowData", rowid);
					var ids = $("#sumGrid").jqGrid('getDataIDs');
					for(var i=0;i < ids.length;i++){
						var row = $("#sumGrid").jqGrid("getRowData", ids[i]);
						if(row.etl_pd_code == rowData.etl_pd_code && row.etl_pi_type == rowData.etl_pi_type){
							$("#sumGrid").jqGrid('setRowData',ids[i],{etl_remark:remark});
						}
					}
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
    },
    temp_updateRemarkByPdCode:function(pd_code,remark,etl_pi_type){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'buy/enter/temp_updateRemarkByPdCode/'+et_type,
			data:{etl_pd_code:pd_code,etl_remark:Public.encodeURI(remark),etl_pi_type:etl_pi_type},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					var ids = $("#grid").jqGrid('getDataIDs');
					for(var i=0;i < ids.length;i++){
						var row = $("#grid").jqGrid("getRowData", ids[i]);
						if(row.etl_pd_code == pd_code && row.etl_pi_type == etl_pi_type){
							$("#grid").jqGrid('setRowData',ids[i],{etl_remark:remark});
						}
					}
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
    },
	temp_updateAmount:function(rowid,amount){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'buy/enter/temp_updateAmount',
			data:{etl_id:rowid,etl_amount:amount},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					needSizeRefresh = true;
					needSumRefresh = true;
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
    },
    temp_updatePrice:function(pd_code,unitPrice,etl_pi_type){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'buy/enter/temp_updatePrice/'+et_type,
			data:{etl_pd_code:pd_code,etl_unitprice:unitPrice,etl_pi_type:etl_pi_type},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					showMode.refresh();
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
    },
	temp_update : function(id,type){
		var priceType = THISPAGE.$_priceType.getValue();
		var sp_rate = $("#sp_rate").val();
		var et_depot_code = $.trim($("#et_depot_code").val());
		if (et_depot_code == '') {
			Public.tips({type: 2, content : "请先选择仓库！"});
	        return;
	    }
		var params = {};
 		var rowData;
 		if(type == '0'){
 			rowData = $("#grid").jqGrid("getRowData", id);
 			params.pd_code = rowData.etl_pd_code;
 		}else if(type == '1'){
			rowData = $("#sizeGrid").jqGrid("getRowData", id);
			params.pd_code = rowData.pd_code;
		}else if(type == '2'){
			rowData = $("#sumGrid").jqGrid("getRowData", id);
			params.pd_code = rowData.etl_pd_code;
		}
 		params.etl_pi_type = rowData.etl_pi_type;
 		params.priceType = priceType;
 		params.sp_rate = sp_rate;
 		params.dp_code = et_depot_code;
		$.dialog({
			title : '修改商品',
			content : 'url:'+config.BASEPATH+'buy/enter/to_temp_update/'+et_type,
			data : params,
			width : 850,
			height : 440,
			max : false,
			min : false,
			cache : false,
			lock: true,
			close:function(){
				showMode.refresh();
			}
		});
	},
	temp_del: function(rowId){//删除
		if(rowId != null && rowId != ''){
			var rowData = $("#grid").jqGrid("getRowData", rowId);
		 	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'buy/enter/temp_del',
					data:{"etl_id":rowId},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){
							Public.tips({type: 3, content : '删除成功!'});
							$('#grid').jqGrid('delRowData', rowId);
							THISPAGE.gridTotal();
							needSizeRefresh = true;
							needSumRefresh = true;
						}else{
							Public.tips({type: 1, content : data.message});
						}
					}
				});
			});
	   }else{
		   Public.tips({type: 1, content : '请选择数据!'});
	   }	
    },
    temp_delByPiCode: function(rowId,type){//删除
		if(rowId != null && rowId != ''){
		 	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
		 		var params = {};
		 		var rowData;
	    		if(type == '1'){
	    			rowData = $("#sizeGrid").jqGrid("getRowData", rowId);
	    			params.pd_code = rowData.pd_code;
	    			params.etl_pi_type = rowData.etl_pi_type;
	    			params.cr_code = rowData.cr_code;
	    			params.br_code = rowData.br_code;
	    		}else if(type == '2'){
	    			rowData = $("#sumGrid").jqGrid("getRowData", rowId);
	    			params.pd_code = rowData.etl_pd_code;
	    			params.etl_pi_type = rowData.etl_pi_type;
	    		}
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'buy/enter/temp_delByPiCode/'+et_type,
					data:params,
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){
							Public.tips({type: 3, content : '删除成功!'});
							showMode.refresh();
						}else{
							Public.tips({type: 1, content : data.message});
						}
					}
				});
			});
	   }else{
		   Public.tips({type: 1, content : '请选择数据!'});
	   }	
    },
    temp_clear:function(){
    	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
			$.ajax({
				type:"POST",
				url:config.BASEPATH+'buy/enter/temp_clear/'+et_type,
				data:{},
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : '删除成功!'});
						$("#grid").clearGridData();
						THISPAGE.gridTotal();
						$("#sumGrid").clearGridData();
						THISPAGE.sumGridTotal();
						if($("#CurrentMode").val() == '1'){
							showMode.refresh();
						}else{
							needSizeRefresh = true;
						}
					}else{
						Public.tips({type: 1, content : data.message});
					}
				}
			});
		});
    },
	save:function(isdraft){
		if (!$("#form1").valid()) {
			return;
		}
		var ids = $("#grid").jqGrid('getDataIDs');
		if (ids.length == 0){
			Public.tips({type: 2, content : '单据明细不存在，请选择货号！'});
			return;
		}
		$("#et_isdraft").val(isdraft);
		$("#btn-save").attr("disabled",true);
		$("#btn-save-draft").attr("disabled",true);
		var saveUrl = config.BASEPATH+"buy/enter/save";
		if($("#et_id").val() != undefined){
			saveUrl = config.BASEPATH+"buy/enter/update";
		}
		$.ajax({
			type:"POST",
			url:saveUrl,
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : '保存成功'});
					handle.loadData(data.data);
					setTimeout("api.zindex()",500);
				}else{
					Public.tips({type: 1, content : data.message});
					setTimeout("api.zindex()",500);
				}
				$("#btn-save").attr("disabled",false);
				$("#btn-save-draft").attr("disabled",false);
			}
		});
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.et_id;
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	},
	operFmatter :function(val, opt, row){
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-pencil" title="修改">&#xe60c;</i>';
		html_con += '<i class="iconfont i-hand ui-icon-trash" title="删除">&#xe60e;</i>';
		html_con += '<i class="iconfont i-hand ui-icon-image" title="图片">&#xe654;</i>';
		html_con += '</div>';
		return html_con;
	},
	formatGift :function(val, opt, row){
		if(row.etl_pi_type == 1){
			return '赠品';
		}
		return '';
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.addEvent();
		this.initGrid();
		this.initEvent();
	},
	initDom:function(){
		$("#et_make_date").val(config.TODAY);
		this.$_priceType = $("#priceTypeTd").cssRadio({ callback: function($_obj){
			 pricrType = $_obj.find("input").val();
			if(pricrType == 1){
				$("#sp_rate").attr("disabled",true);
			}else{
				$("#sp_rate").attr("disabled",false);
			}
		}});
		this.initProperty();
		needListRefresh = false;//列表模式
        needSizeRefresh = true;//尺码模式
        needSumRefresh = true;//汇总模式
        this.selectRow={};
	},
	initProperty:function(){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"base/dict/list?dtl_upcode=KEY_BILL",
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.data != "" && data.data.length > 0){
					var propertys = data.data;
					propertys.unshift({dtl_code:"",dtl_name:"空"});
					$('#span_property').combo({
						value : 'dtl_code',
						text : 'dtl_name',
						width : 207,
						listHeight : 300,
						listId : '',
						defaultSelected : 0,
						editable : false,
						callback : {
							onChange : function(data) {
								if(data.dtl_code != ""){
									$("#et_property").val(data.dtl_name);
								}else{
									$("#et_property").val("");
								}
							}
						}
					}).getCombo().loadData(propertys,["dtl_code",$("#et_property").val(),0]);
				}
			}
		 });
	},
	gridTotal:function(){
    	var grid=$('#grid');
		var etl_amount=grid.getCol('etl_amount',false,'sum');
    	var etl_unitmoney=grid.getCol('etl_unitmoney',false,'sum');
    	var etl_retailmoney=grid.getCol('etl_retailmoney',false,'sum');
    	grid.footerData('set',{pd_no:'合计：',etl_amount:etl_amount,etl_unitmoney:etl_unitmoney,etl_retailmoney:etl_retailmoney});
    	$("#et_amount").val(etl_amount);
    	$("#et_money").val(PriceLimit.formatByEnter(etl_unitmoney));
    	calcPayable();
    },
    sumGridTotal:function(){
    	var grid=$('#sumGrid');
    	var etl_amount=grid.getCol('etl_amount',false,'sum');
    	var etl_unitmoney=grid.getCol('etl_unitmoney',false,'sum');
    	var etl_retailmoney=grid.getCol('etl_retailmoney',false,'sum');
    	grid.footerData('set',{pd_no:'合计：',etl_amount:etl_amount,etl_unitmoney:etl_unitmoney,etl_retailmoney:etl_retailmoney});
    	$("#et_amount").val(etl_amount);
    	$("#et_money").val(PriceLimit.formatByEnter(etl_unitmoney));
    	calcPayable();
    },
	initGrid:function(){
		var self=this;
		var colModel = [
		    {label:'操作',name:'operate', width: 70, fixed:true, formatter: handle.operFmatter, sortable:false,align:'center'},
		    {label:'',name: 'etl_pd_code', index: 'etl_pd_code', hidden:true},
	    	{label:'商品货号',name: 'pd_no', index: 'pd_no', width: 100},
	    	{label:'商品名称',name: 'pd_name', index: 'pd_name', width: 100},
	    	{label:'颜色',name: 'cr_name', index: 'cr_name', width: 60},
	    	{label:'尺码',name: 'sz_name', index: 'sz_name', width: 60},
	    	{label:'杯型',name: 'br_name', index: 'br_name', width: 60},
	    	{label:'单位',name: 'pd_unit', index: 'pd_unit', width: 60},
	    	{label:'数量',name: 'etl_amount', index: 'etl_amount', width: 70,align:'right',sorttype: 'int',editable:true},
	    	{label:'进货价',name: 'etl_unitprice', index: 'etl_unitprice', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByEnter,editable:PriceLimit.hasEnter()},
	    	{label:'进货金额',name: 'etl_unitmoney', index: 'etl_unitmoney', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByEnter},
	    	{label:'零售价',name: 'etl_retailprice', index: 'etl_retailprice', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatBySell},
	    	{label:'零售金额',name: 'etl_retailmoney', index: 'etl_retailmoney', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatBySell},
	    	{label:'赠品',name: 'gift', index: 'etl_pi_type', width: 60,align:'center',formatter: handle.formatGift},
	    	{label:'',name: 'etl_pi_type', index: 'etl_pi_type', width: 100,hidden:true},
	    	{label:'折扣率',name: 'etl_rate', index: 'etl_rate', width: 70,align:'right',sorttype: 'int',formatter: PriceLimit.formatByEnter_Sell},
	    	{label:'订单编号',name: 'etl_order_number', index: 'etl_order_number', width: 140},
	    	{label:'备注',name: 'etl_remark', index: 'etl_remark', width: 180,editable:true}
	    ];
		$('#grid').jqGrid({
			url:queryurl,
			loadonce:true,
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:9999,//每页条数
			cmTemplate: {sortable:true,title:true},
			shrinkToFit:false,//表格是否自动填充
			cellEdit: true,
            cellsubmit: 'clientArray',
            footerrow: true,
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'etl_id'  //图标ID
			},
			loadComplete: function(data){
				var ids = $("#grid").getDataIDs();
				THISPAGE.gridTotal();
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
				handle.temp_update(rowid, '0');
			},
			formatCell:function (rowid, cellname, value, iRow, iCol){
            	self.selectRow.value=value;
				return value;
			},
			beforeSaveCell :function(rowid, cellname, value, iRow, iCol){
				if (cellname == 'etl_amount' && self.selectRow.value != value){
					if(value == '' || isNaN(value) || parseInt(value) <= 0){
						return self.selectRow.value;
					} 
					if(parseInt(self.selectRow.value) == parseInt(value)){
						return self.selectRow.value;
					}
					if(parseFloat(value).toFixed(0).length > 9|| parseInt(value) >1e+9){
						return self.selectRow.value;
					}
					handle.temp_updateAmount(rowid, parseInt(value));
					return parseInt(value);
				}else if(cellname == 'etl_unitprice' && self.selectRow.value != value){
					if(value == '' || isNaN(value) || parseFloat(value) < 0){
						return self.selectRow.value;
					}
					if(parseFloat(self.selectRow.value) == parseFloat(value)){
						return self.selectRow.value;
					}
					if(parseFloat(value).toFixed(2).length > 11 || parseFloat(value) >1e+11){
						return self.selectRow.value;
					}
					var rowData = $("#grid").jqGrid("getRowData", rowid);
					handle.temp_updatePrice(rowData.etl_pd_code, parseFloat(value).toFixed(2), rowData.etl_pi_type);
					return parseFloat(value).toFixed(2);
				}else if(cellname == 'etl_remark' && self.selectRow.value != value){
					handle.temp_updateRemarkById(rowid, value);
				}
				
				return value;
			},
			afterSaveCell:function(rowid, cellname, value, iRow, iCol){
				if (cellname == 'etl_amount' && self.selectRow.value != value){
					var rowData = $("#grid").jqGrid("getRowData", rowid);
					rowData.etl_unitmoney = rowData.etl_amount * rowData.etl_unitprice;
					rowData.etl_retailmoney = rowData.etl_amount * rowData.etl_retailprice;
					rowData.etl_rate = rowData.etl_unitprice / rowData.etl_retailprice;
					$("#grid").jqGrid('setRowData', rowid, rowData);
					THISPAGE.gridTotal();
				}
				
			}
	    });
	},
	initSumGrid:function(){
		var self=this;
		var colModel = [
		    {label:'操作',name:'operate', width: 70, fixed:true, formatter: handle.operFmatter, sortable:false,align:'center'},
	    	{label:'',name: 'etl_pd_code', index: 'etl_pd_code', hidden:true},
	    	{label:'商品货号',name: 'pd_no', index: 'pd_no', width: 100},
	    	{label:'商品名称',name: 'pd_name', index: 'pd_name', width: 100},
	    	{label:'品牌',name: 'bd_name', index: 'pd_bd_code', width: 80},
	    	{label:'类别',name: 'tp_name', index: 'pd_tp_code', width: 80},
	    	{label:'单位',name: 'pd_unit', index: 'pd_unit', width: 60},
	    	{label:'数量',name: 'etl_amount', index: 'etl_amount', width: 70,align:'right',sorttype: 'int'},
	    	{label:'进货价',name: 'etl_unitprice', index: 'etl_unitprice', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByEnter,editable:PriceLimit.hasEnter()},
	    	{label:'进货金额',name: 'etl_unitmoney', index: 'etl_unitmoney', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByEnter},
	    	{label:'零售价',name: 'etl_retailprice', index: 'etl_retailprice', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatBySell},
	    	{label:'零售金额',name: 'etl_retailmoney', index: 'etl_retailmoney', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatBySell},
	    	{label:'赠品',name: 'gift', index: 'etl_pi_type', width: 60,align:'center',formatter: handle.formatGift},
	    	{label:'',name: 'etl_pi_type', index: 'etl_pi_type', width: 100,hidden:true},
	    	{label:'折扣率',name: 'etl_rate', index: 'etl_rate', width: 70,align:'right',sorttype: 'int',formatter: PriceLimit.formatByEnter_Sell},
	    	{label:'备注',name: 'etl_remark', index: 'etl_remark', width: 180,editable:true}
	    ];
		$('#sumGrid').jqGrid({
			url:querysumurl,
			loadonce:true,
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#sumPage',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:9999,//每页条数
			cmTemplate: {sortable:true,title:true},
			shrinkToFit:false,//表格是否自动填充
			cellEdit: true,
            cellsubmit: 'clientArray',
            footerrow: true,
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'etl_id'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.sumGridTotal();
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
				handle.temp_update(rowid, '2');
			},
			formatCell:function (rowid, cellname, value, iRow, iCol){
            	self.selectRow.value=value;
				return value;
			},
			beforeSaveCell :function(rowid, cellname, value, iRow, iCol){
				if(cellname == 'etl_unitprice' && self.selectRow.value != value){
					if(value == '' || isNaN(value) || parseFloat(value) < 0){
						return self.selectRow.value;
					}
					if(parseFloat(self.selectRow.value) == parseFloat(value)){
						return self.selectRow.value;
					}
					if(parseFloat(value).toFixed(2).length > 11 || parseFloat(value) >1e+11){
						return self.selectRow.value;
					}
					var rowData = $("#sumGrid").jqGrid("getRowData", rowid);
					handle.temp_updatePrice(rowData.etl_pd_code, parseFloat(value).toFixed(2), rowData.etl_pi_type);
					return parseFloat(value).toFixed(2);
				}else if(cellname == 'etl_remark' && self.selectRow.value != value){
					var rowData = $("#sumGrid").jqGrid("getRowData", rowid);
					handle.temp_updateRemarkByPdCode(rowData.etl_pd_code, value, rowData.etl_pi_type);
				}
				return value;
			},
			afterSaveCell:function(rowid, cellname, value, iRow, iCol){
				
			}
	    });
	},
	initSizeGroupGrid:function(dyColNames, sizeCodeMap) {
		var self=this;
        var headers = [];
        var dyns = dyColNames;
        var dySize = dyns.length;
        var dms = [];
        for (var i = 0; i < dyns.length; i++) {
            var dys = dyns[i];
            for (var j = 0; j < dys.length; j++) {
                var dyss = null;
                dyss = dys[j].split('_AND_');
                if (i == 0 && j == 0) {
                    dyns[i][j] = dyss[0];
                }
                headers.push(dyss[1]);
                dms.push({label:dyss[1],name: dyss[0], index: dyss[0], width: 45, title: false, align: 'right', sorttype: 'int', sortable: false});
            }
            break;
        }
		var colModel = [
            {label:'',name: 'sizeGroupCode', hidden: true},
            {label:'',name: 'pd_code', index: 'pd_code', width: 100,hidden:true},
            {label:'',name: 'cr_code', index: 'cr_code', width: 100,hidden:true},
            {label:'',name: 'br_code', index: 'br_code', width: 100,hidden:true},
            {label:'操作',name:'operate', width: 70, fixed:true, formatter: handle.operFmatter, sortable:false,align:'center'},
            {label:'商品货号',name: 'pd_no', index: 'pd_no', width: 100},
	    	{label:'商品名称',name: 'pd_name', index: 'pd_name', width: 100},
	    	{label:'颜色',name: 'cr_name', index: 'etl_cr_code', width: 60},
	    	{label:'杯型',name: 'br_name', index: 'etl_br_code', width: 60},
        ];
        colModel = colModel.concat(dms);
        colModel = colModel.concat([
            {label:'数量',name: 'Amount_Total',	index:'Amount_Total',	width: 70, align: 'right', sorttype: 'int', sortable: false},
            {label:'进货价',name: 'unit_price', index: 'unit_price', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByEnter,editable:PriceLimit.hasEnter()},
	    	{label:'进货金额',name: 'unit_money', index: 'unit_money', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByEnter},
	    	{label:'零售价',name: 'retail_price', index: 'retail_price', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatBySell},
	    	{label:'零售金额',name: 'retail_money', index: 'retail_money', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatBySell},
	    	{label:'赠品',name: 'gift', index: 'gift', width: 70,align:'center',formatter: handle.formatGift},
	    	{label:'',name: 'etl_pi_type', index: 'etl_pi_type', width: 100,hidden:true}
	    ]);
        $('#sizeGrid').jqGrid({
			url: querysizeurl,
			loadonce:true,
            datatype: 'json',
            autowidth: true,
            height: _height,
            gridview: true,
            colModel: colModel,
            rownumbers: true,//行号
            pager: '#sizePage',//分页
            pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
            viewrecords: true,
            rowNum: 9999,//每页条数
            shrinkToFit: false,//表格是否自动填充
            cellEdit: true,
            cellsubmit: 'clientArray',
            footerrow: true,
            userDataOnFooter: true,
			jsonReader: {
				root: 'data.data',
				userdata: 'data.userData',
				repeatitems : false,
				id:'id'
			},
			loadComplete: function(data) {
				
			},
			gridComplete:function(){
				Public.resizeSpecifyGrid('sizeGrid', 260 + (dyns.length * 32),2);
			},
			loadError: function(xhr, status, error) {
                Public.tips({type: 1, content : '操作失败了哦，服务可能已过期，请按F5刷新！'});
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
				handle.temp_update(rowid, '1');
			},
			formatCell:function (rowid, cellname, value, iRow, iCol){
            	self.selectRow.value=value;
				return value;
			},
			beforeSaveCell :function(rowid, cellname, value, iRow, iCol){
				if(cellname == 'unit_price' && self.selectRow.value != value){
					if(value == '' || isNaN(value) || parseFloat(value) < 0){
						return self.selectRow.value;
					}
					if(parseFloat(self.selectRow.value) == parseFloat(value)){
						return self.selectRow.value;
					}
					if(parseFloat(value).toFixed(2).length > 11 || parseFloat(value) >1e+11){
						return self.selectRow.value;
					}
					var rowData = $("#sizeGrid").jqGrid("getRowData", rowid);
					handle.temp_updatePrice(rowData.pd_code, parseFloat(value).toFixed(2), rowData.etl_pi_type);
					return parseFloat(value).toFixed(2);
				}
				return value;
			},
			afterSaveCell:function(rowid, cellname, value, iRow, iCol){
				
			},
			onPaging: function(pgButton) {
				
			},
            onSelectRow: function (rowId, status) {
                var sizeG = $("#sizeGrid").jqGrid("getRowData", rowId).sizeGroupCode;
                var bg = $('[sizeGCode=' + sizeG + ']').css("background");
                if (bg == 'none') {
                    return;
                }
                $("[sizeGCode]").css("background-color", '#eee');
                $('[sizeGCode=' + sizeG + ']').css({ "background": "none", "background-color": "#F8FF94"});
            }
	    });
        if (dySize >= 2) {
        	$('#sizeGrid').jqGrid('setGroupHeaders', {
                useColSpanStyle: true,
                groupHeaders:[]
            });
            $('#sizeGrid').jqGrid("setComplexGroupHeaders", {
                complexGroupHeaders: {startColumnName: dyns[0][0], numberOfColumns: dyns[0].length},
                rowspan: dySize,
                topTitle:true
            });
            for (var key in sizeCodeMap) {
                $('#sizeGrid').jqGrid("setComplexGroupHeaders",{
                    complexGroupHeaders: {startColumnName: dyns[0][0], numberOfColumns: dyns[0].length},
                    rowspan: dySize,
                    sizeGroupCode: key,
                    titleTextGroup:sizeCodeMap[key],
                    topTitle:false
                });
            }
        }else{
        	$('#sizeGrid').jqGrid('setGroupHeaders', {
                useColSpanStyle: true,
                groupHeaders: [{startColumnName: dyns[0][0], numberOfColumns: dyns[0].length, titleText: '尺码'}]
            });
        }
    },
	reloadGridData: function () {
        $("#grid").jqGrid('setGridParam', {datatype: 'json', page: 1, url: queryurl}).trigger("reloadGrid");
    },
	initEvent:function(){
		$('#btn-import').on('click', function(e){
			e.preventDefault();
			Choose.doImport();
		});
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save(0);
		});
		$('#btn-save-draft').on('click', function(e){
			e.preventDefault();
			handle.save(1);
		});
		$('#btn-import-draft').on('click', function(e){
			e.preventDefault();
			Choose.importDraft();
		});
		$('#btn-import-order').on('click', function(e){
			e.preventDefault();
			Choose.importOrder();
		});
		$("#btn_close").click(function(e){
			e.preventDefault();
			api.close();
		});
		//清除
        $('#btnClear').click(function(e){
        	e.preventDefault();
        	handle.temp_clear();
		});
        //修改-列表模式
        $('#grid').on('click', '.operating .ui-icon-pencil', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
        	handle.temp_update(id, '0');
        });
        
        //删除-列表模式
		$('#grid').on('click', '.operating .ui-icon-trash', function(e) {
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.temp_del(id);
		});
		
		$('#grid').on('click', '.operating .ui-icon-image', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
        	viewProductImg($("#grid").jqGrid("getRowData", id).etl_pd_code);
        });
	},
	addEvent:function(){
		//移除事件，防止重复绑定
		$('#sumGrid').off('click','.operating .ui-icon-pencil');
		$('#sumGrid').off('click','.operating .ui-icon-trash');
		$('#sumGrid').off('click','.operating .ui-icon-image');
		$('#sizeGrid').off('click','.operating .ui-icon-pencil');
		$('#sizeGrid').off('click','.operating .ui-icon-trash');
		$('#sizeGrid').off('click','.operating .ui-icon-image');
        //修改-尺码模式
        $('#sizeGrid').on('click', '.operating .ui-icon-pencil', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
			handle.temp_update(id, '1');
        });
        //修改-汇总模式
        $('#sumGrid').on('click', '.operating .ui-icon-pencil', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
			handle.temp_update(id, '2');
        });
		
		 //删除-汇总模式
		$('#sumGrid').on('click', '.operating .ui-icon-trash', function(e) {
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.temp_delByPiCode(id, '2');
		});
		//删除-尺码模式
		$('#sizeGrid').on('click', '.operating .ui-icon-trash',function(e) {
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.temp_delByPiCode(id, '1');
		});
		
		$('#sizeGrid').on('click', '.operating .ui-icon-image', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
        	viewProductImg($("#sizeGrid").jqGrid("getRowData", id).pd_code);
        });
        $('#sumGrid').on('click', '.operating .ui-icon-image', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
        	viewProductImg($("#sumGrid").jqGrid("getRowData", id).etl_pd_code);
        });
	}
};

THISPAGE.init();