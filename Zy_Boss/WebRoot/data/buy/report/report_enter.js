var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.BUY14;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'buy/report/pageEnterReport';
var _height = $(parent).height()-298,_width = $(parent).width()-192;

var handle = {
	viewDetail: function(id){
		var rowData = $("#grid").jqGrid("getRowData", id);
		var params = {};
		params.begindate = $("#begindate").val();
		params.enddate = $("#enddate").val();
		params.supply_name = $("#supply_name").val();
		params.et_supply_code = $("#et_supply_code").val();
		params.depot_name = $("#depot_name").val();
		params.et_depot_code = $("#et_depot_code").val();
		params.et_manager = $("#et_manager").val();
		params.bd_name = $("#bd_name").val();
		params.bd_code = $("#bd_code").val();
		params.tp_name = $("#tp_name").val();
		params.tp_code = $("#tp_code").val();
		params.pd_season = $("#pd_season").val();
		params.pd_year = $("#pd_year").val();
		params.pd_code = $("#pd_code").val();
		params.pd_name = $("#pd_name").val();
		var type = $("#type").val();
		switch(type){
			case 'brand':
				params.bd_code = rowData.code;
				params.bd_name = rowData.name;
				break ;
			case 'type':
				params.tp_code = rowData.code;
				params.tp_name = rowData.name;
				break ;
			case 'supply':
				params.et_supply_code = rowData.code;
				params.supply_name = rowData.name;
				break ;
			default :
				break ;
		}
		$.dialog({
			title : false,
			content : 'url:'+config.BASEPATH+"buy/report/to_report_enter_detail",
			data: params,
			max : false,
			min : false,
			cache : false,
			lock: false
		}).max();
	},
	operFmatter : function(val, opt, row){
		if(val == "合计"){
			return val;
		}
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-list" title="明细">&#xe608;</i>';
		html_con += '</div>';
		return html_con;
	},
	formatRealAmount:function(val, opt, row){
		return row.in_amount-row.out_amount;
	},
	formatRealMoney:function(val, opt, row){
		return PriceLimit.formatMoney(row.in_money-row.out_money);
	},
	formatReturnRate:function(val, opt, row){
		if(row.in_amount == 0){
			return "0.00";
		}
		return PriceLimit.formatMoney(row.out_amount/row.in_amount*100);
	},
	formatMoneyProportion:function(val, opt, row){
		if(val == "合计"){
			return "";
		}
		var userData = $("#grid").getGridParam('userData');
		if(userData.in_money-userData.out_money == 0){
			return "0.00";
		}
		return PriceLimit.formatMoney((row.in_money-row.out_money)/(userData.in_money-userData.out_money)*100);
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		dateRedioClick("theDate");
		Public.buildSeasonCombo("span_pd_season", "pd_season");
		Public.buildYearCombo("span_pd_year", "pd_year");
	},
	initGrid:function(){
		var type = $("#type").val();
		var codeLabel = '';
		var nameLabel = '';
		switch(type){
			case 'brand' : 
				codeLabel = '品牌编号';
				nameLabel = '品牌名称';
				break ;
			case 'type' : 
				codeLabel = '类别编号';
				nameLabel = '类别名称';
				break ;
			case 'supply' : 
				codeLabel = '供应商编号';
				nameLabel = '供应商名称';
				break ;
			default :
				break ;
		}
		var colModel = [
	    	{label:'操作',name: 'operate',width: 40, formatter: handle.operFmatter,align:'center', sortable:false},
	    	{label:codeLabel,name: 'code',index:'code',width:100},	
	    	{label:nameLabel,name: 'name',index:'name',width:100},	
        	{label:'进货数量',name: 'in_amount',index:'in_amount',width:80, align:'right'},	
        	{label:'进货金额',name: 'in_money',index:'in_money',width:100, align:'right',formatter: PriceLimit.formatMoney},	
	    	{label:'退货数量',name: 'out_amount',index:'out_amount',width:80, align:'right'},
	    	{label:'退货金额',name: 'out_money',index:'out_money',width:100, align:'right',formatter: PriceLimit.formatMoney},	
	    	{label:'实际数量',name: 'real_amount',index:'real_amount',width:80, align:'right',formatter: handle.formatRealAmount},
	    	{label:'实际金额',name: 'real_money',index:'real_money',width:100, align:'right',formatter: handle.formatRealMoney},	
	    	{label:'零售金额',name: 'sell_money',index:'sell_money',width:100, align:'right',formatter: PriceLimit.formatMoney},	
	    	{label:'退货率(%)',name: 'return_rate',index:'sell_money',width:100, align:'right',formatter: handle.formatReturnRate},	
	    	{label:'金额占比(%)',name: 'money_proportion',index:'money_proportion',width:100, align:'right',formatter: handle.formatMoneyProportion}	
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'local',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.BASEROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:false,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			userDataOnFooter: true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
                userdata: 'data.data',
				repeatitems : false,
				id: 'code'  //图标ID
			},
			loadComplete: function(data){
				var footer = $('#grid').footerData()
				footer.operate = "合计";
				footer.money_proportion = "合计";
		    	$('#grid').footerData('set',footer);
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'type='+$("#type").val();
		params += '&begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&et_supply_code='+$("#et_supply_code").val();
		params += '&et_depot_code='+$("#et_depot_code").val();
		params += '&bd_code='+$("#bd_code").val();
		params += '&tp_code='+$("#tp_code").val();
		params += '&pd_season='+$("#pd_season").val();
		params += '&pd_year='+$("#pd_year").val();
		params += '&et_manager='+Public.encodeURI($("#et_manager").val());
		params += '&pd_code='+$("#pd_code").val();
		return params;
	},
	reset:function(){
		$("#pd_code").val("");
		$("#pd_name").val("");
		$("#et_manager").val("");
		$("#supply_name").val("");
		$("#et_supply_code").val("");
		$("#depot_name").val("");
		$("#et_depot_code").val("");
		$("#bd_name").val("");
		$("#bd_code").val("");
		$("#tp_name").val("");
		$("#tp_code").val("");
		THISPAGE.$_date.setValue(0);
		$("#span_pd_season").getCombo().selectByValue("");
		$("#span_pd_year").getCombo().selectByValue("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		//收银明细
		$('#grid').on('click', '.operating .ui-icon-list', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			var id = $(this).parent().data('id');
			handle.viewDetail(id);
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();