var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.REPORT01;
var _menuParam = config.OPTIONLIMIT;
var shops = {};
var queryurl = config.BASEPATH+'report/sellStock';
var Utils = {
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:true},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var sp_code = [];
				var sp_name = [];
				for(var i=0;i<selected.length;i++){
					sp_code.push(selected[i].sp_code);
					sp_name.push(selected[i].sp_name);
				}
				$("#shl_shop_code").val(sp_code.join(","));
				$("#shop_name").val(sp_name.join(","));
				THISPAGE.initShop();
			},
			close:true,
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择导购员',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false,em_type:0},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#em_name").val(selected.em_name);
				$("#shl_main").val(selected.em_code);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#em_name").val(selected.em_name);
					$("#shl_main").val(selected.em_code);
				}
			},
			cancel:true
		});
	},
	doProduct : function(){
		commonDia = $.dialog({
			title : '选择商品',
			content : 'url:'+config.BASEPATH+'base/product/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#pd_code").val(selected.pd_code);
				$("#pd_name").val(selected.pd_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#pd_code").val(selected.pd_code);
					$("#pd_name").val(selected.pd_name);
				}
			},
			cancel:true
		});
	},
	doQueryType : function(){
		commonDia = $.dialog({
			title : '选择商品类别',
			content : 'url:'+config.BASEPATH+'base/type/to_tree_dialog',
			data : {multiselect:true},
			width : 280,
		   	height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var pd_tp_code = [];
				var pd_tp_name = [];
				for(var i=0;i<selected.length;i++){
					pd_tp_code.push(selected[i].tp_code);
					pd_tp_name.push(selected[i].tp_name);
				}
				$("#tp_code").val(pd_tp_code.join(","));
				$("#tp_name").val(pd_tp_name.join(","));
			},
			close:function(){
				if(commonDia.content.dblClick){
					var selected = commonDia.content.doSelect();
					var pd_tp_code = [];
					var pd_tp_name = [];
					for(var i=0;i<selected.length;i++){
						pd_tp_code.push(selected[i].tp_code);
						pd_tp_name.push(selected[i].tp_name);
					}
					$("#tp_code").val(pd_tp_code.join(","));
					$("#tp_name").val(pd_tp_name.join(","));
				}
			},
			cancel:true
		});
	},
	doQueryBrand : function(){
		commonDia = $.dialog({
			title : '选择品牌',
			content : 'url:'+config.BASEPATH+'base/brand/to_list_dialog',
			data : {multiselect:true},
			width : 320,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var pd_bd_code = [];
				var pd_bd_name = [];
				for(var i=0;i<selected.length;i++){
					pd_bd_code.push(selected[i].bd_code);
					pd_bd_name.push(selected[i].bd_name);
				}
				$("#bd_code").val(pd_bd_code.join(","));
				$("#bd_name").val(pd_bd_name.join(","));
			},
			close:function(){
				if(commonDia.content.dblClick){
					var selected = commonDia.content.doSelect();
					var pd_bd_code = [];
					var pd_bd_name = [];
					for(var i=0;i<selected.length;i++){
						pd_bd_code.push(selected[i].bd_code);
						pd_bd_name.push(selected[i].bd_name);
					}
					$("#bd_code").val(pd_bd_code.join(","));
					$("#bd_name").val(pd_bd_name.join(","));
				}
			},
			cancel:true
		});
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initDict();
		this.initShop();
		this.initEvent();
	},
	initDict:function(){
	  seasonCombo = $('#span_pd_season_Code').combo({
			value: 'dtl_code',
			text: 'dtl_name',
			width :206,
			height: 80,
			listId:'',
			defaultSelected: 0,
			editable: true,
			callback:{
				onChange: function(data){
					if(data.dtl_name == "全部"){
						$("#pd_season").val("");
					}else{
						$("#pd_season").val(data.dtl_name);
					}
				}
			}
		}).getCombo(); 
		$.ajax({
			type:"POST",
			url:config.BASEPATH +"base/dict/listByProduct",
			data:"",
			cache:false,
			dataType:"json",
			success:function(data){
				seasonInfo = data.data.seasonitemsquery;
				seasonCombo.loadData(seasonInfo);
			}
		});
	},
	initShop:function(){
		var sp_code = $("#shl_shop_code").val();
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'base/shop/selectList',
			data:{"sp_code":sp_code},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					shops = data.data;
					$("#grid").jqGrid('GridUnload');
					THISPAGE.initGrid(shops);
				}
			}
		});
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		dateRedioClick("theDate");
		this.initYear();
	},
	initYear:function(){
		var tim = new Date();
		var currentYear = tim.getFullYear();
		yearInfo = {
			data:{items:[
			        {year_Code:"",year_Name:"全部"},
					{year_Code:""+parseInt(currentYear+1)+"",year_Name:""+parseInt(currentYear+1)+""},
					{year_Code:""+parseInt(currentYear)+"",year_Name:""+parseInt(currentYear)+""},
					{year_Code:""+parseInt(currentYear-1)+"",year_Name:""+parseInt(currentYear-1)+""},
					{year_Code:""+parseInt(currentYear-2)+"",year_Name:""+parseInt(currentYear-2)+""},
					{year_Code:""+parseInt(currentYear-3)+"",year_Name:""+parseInt(currentYear-3)+""},
					{year_Code:""+parseInt(currentYear-4)+"",year_Name:""+parseInt(currentYear-4)+""},
					{year_Code:""+parseInt(currentYear-5)+"",year_Name:""+parseInt(currentYear-5)+""},
					{year_Code:""+parseInt(currentYear-6)+"",year_Name:""+parseInt(currentYear-6)+""},
					{year_Code:""+parseInt(currentYear-7)+"",year_Name:""+parseInt(currentYear-7)+""}
				]}
		}
		yearCombo = $('#span_year').combo({
			data:yearInfo.data.items,
			value: 'year_Code',
			text: 'year_Name',
			width :206,
			height:80,
			listId:'',
			defaultSelected: 0,
			editable: true,
			callback:{
				onChange: function(data){
					$("#pd_year").val(data.year_Code);
				}
			}
		}).getCombo();
	  },
	initGrid:function(shops){
		var gridWH = Public.setGrid();
		var colModel = [
            {label:'编号',name: 'shl_sub_code',index:'shl_sub_code',hidden:true},
			{label:'货号',name: 'pd_no',index:'pd_no',width:100, align:'left'},
			{label:'名称',name: 'pd_name',index:'pd_name',width:100, align:'left'},
			{label:'颜色',name: 'cr_name',index:'cr_name',width:70, align:'left'},
			{label:'尺码',name: 'sz_name',index:'sz_name',width:60, align:'left'},
			{label:'杯型',name: 'br_name',index:'br_name',width:45, align:'left'},
			{label:'零售价',name: 'shl_sell_price',index:'shl_sell_price',width:55,align:'right'},
			{label:'总仓库存',name: 'sum_stock',index:'sum_stock',width:60,align:'right'},
			{label:'到仓时间',name: 'sum_date',index:'sum_date',width:88,align:'right'},
			{label:'总销量',name: "sum_amount",index:'sum_amount',width: 60, title: true,sortable:false,align:'right',sorttype:'float'},
			{label:'总库存',name: "total_stock",index:'total_stock',width: 88, title: true,sortable:false,align:'right',sorttype:'float'},
			{label:'售罄率',name: "sell_rate",index:'sell_rate',width: 60, title: true,sortable:false,align:'right',sorttype:'float'}
	    ];
		if(null != shops && shops.length > 0){
			for(i = 0;i < shops.length;i ++){
				colModel.push({label:'存/销',name: "so_"+shops[i].sp_code,index:'',width: 55, title: true,sortable:false,align:'right',sorttype:'int'});
				colModel.push({label:'到仓时间',name: "shl_"+shops[i].sp_code,index:'',width: 88, title: true,sortable:false,align:'right',sorttype:'float'});
//				colModel.push({label:shops[key].sp_name,name: 's'+shops[key].sp_code,index:'s'+shops[key].sp_code,width:100,align:'right'});
			}
		}
		$('#grid').jqGrid({
			datatype: 'local',
			width: gridWH.w-12,
			height: gridWH.h-50,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			page:1,
			pgbuttons:true,
			recordtext:'共 {2} 条',
			multiselect:false,//多选
			viewrecords: true,
			rowNum:15,//每页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
            scroll: 1,//是否滚动
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
	            page: 'data.pageInfo.currentPage',
	            records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'shl_id'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.initRow();
			},
			loadError: function(xhr, status, error){
			}
	    });
		var header = [];
        for (var i=0 ;i < shops.length; i++){ 
        	header.push({startColumnName : 'so_'+shops[i].sp_code,numberOfColumns : 2, titleText : shops[i].sp_name});
        }
        $("#grid").jqGrid('setGroupHeaders', { 
			useColSpanStyle : true,
			groupHeaders : header
		});
	},
	initRow:function(){
		var ids = $("#grid").getDataIDs();
        for (var i = 0; i < ids.length; i++) {
            var before = $("#grid").jqGrid('getRowData', ids[i]);
            var index = 1;
            for (var j = i + 1; j <= ids.length; j++) {
            	if(undefined != ids[j]){
	                var end = $("#grid").jqGrid('getRowData', ids[j]);
	                if (before['bd_name'] == end['bd_name']) {
	                	index++;
	                    $("#grid").setCell(ids[j], 'bd_name', '', { display: 'none' });
	                } else {
	                	index = 1;
	                	break;
	                }
	                if(index > 1){
	                	$("#bd_name_" + ids[i]).prop("rowspan", index);
	                }
            	}
            }
        }
	},
	initParam:function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&pd_code='+$("#pd_code").val();
		params += '&shl_shop_code='+$("#shl_shop_code").val();
		params += '&type='+$("#type").val();
		params += '&bd_code='+$("#bd_code").val();
		params += '&tp_code='+$("#tp_code").val();
		return params;
	},
	reset:function(){
		$("#shop_name").val("");
		$("#shl_shop_code").val("");
		$("#tp_name").val("");
		$("#tp_code").val("");
		$("#bd_name").val("");
		$("#bd_code").val("");
		$("#pd_name").val("");
		$("#pd_code").val("");
		THISPAGE.$_date.setValue(0);
	},
	reloadData:function(){
		var param=THISPAGE.initParam();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();
