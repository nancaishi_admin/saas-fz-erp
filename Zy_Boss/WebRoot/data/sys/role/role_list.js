var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var isRefresh = false;
var _limitMenu = system.MENULIMIT.BASE13;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'sys/role/list';
var handle = {
	operate: function(oper, id){//修改、新增
		var width = 250,height = 210;
		if(oper == 'add'){
			title = '新增';
			url = config.BASEPATH+"sys/role/to_add";
			data = {oper: oper, callback: this.callback};
		}else if(oper == 'edit'){
			title = '修改';
			url = config.BASEPATH+"sys/role/to_update?ro_id="+id;
			data = {oper: oper,callback: this.callback};
		}else{
			return false;
		}
		$.dialog({
			title : title,
			content : 'url:'+url,
			data: data,
			width : width,
			height : height,
			max : false,
			min : false,
			cache : false,
			lock: true
		});
	},
	del: function(rowIds){//删除
		if(rowIds != null && rowIds != ''){
		 	$.dialog.confirm('删除后无法恢复，确定删除吗?', function(){
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'sys/role/del',
					data:{"ro_id":rowIds},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){		 		
							$('#grid').jqGrid('delRowData', rowIds);
						}else{
							Public.tips({type: 1, content : "删除失败"});
						}
					}
				});
			});
	   }else{
	      $.dialog.tips("请选择数据!!!",1,"32X32/fail.png");
	   }	
    },
	callback: function(data, oper, dialogWin){
		var gridData = $("#grid").data('gridData');
		if(!gridData){
			gridData = {};
			$("#grid").data('gridData', gridData);
		}
		gridData[data.id] = data;
		if(oper == "edit") {
			$("#grid").jqGrid('setRowData', data.id, data);
			dialogWin && dialogWin.api.close();
		} 
		if(oper == "add"){
			$("#grid").jqGrid('addRowData', data.id, data, 'first');
		}
	},
	updateRoleMenu:function(id){
		commonDia = $.dialog({
			title : '修改角色菜单',
			content : 'url:'+config.BASEPATH+"sys/role/to_rolemenu_tree/"+id,
			data: {},
			width : 310,
			height : 440,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var nodes = commonDia.content.doSelect(); 
				return handle.saveRoleMenu(id, nodes);
			},
			cancel:true
		});
	},
	saveRoleMenu:function(id,nodes){
		var rowData = $("#grid").jqGrid("getRowData", id);
		var menus = [];
		if (nodes.length > 0) {
			for(var i=0;i<nodes.length;i++){
				if(nodes[i].id == 0){
					continue;
				}
				var menu = {};
				menu.rm_mn_code = nodes[i].id;
				menu.rm_limit = $.trim(nodes[i].mn_limit);
				menus.push(menu);
			}
		}
		if(menus.length==0){
			Public.tips({type: 2, content : "请选择菜单！"});
			return false;
		}
		var jsonString=JSON.stringify(menus);
		$.ajax({
			type:"post",
			url:config.BASEPATH+"sys/role/rolemenu_save",
			data:{ro_id:id,ro_code:rowData.ro_code,menus:jsonString},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
				 	Public.tips({type: 3, content : "保存成功！！"});
				 	setTimeout("commonDia.close();",300);
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
		return false;
	},
	operFmatter:function (val, opt, row) {
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-pencil" title="修改">&#xe60c;</i>';
		html_con += '<i class="iconfont i-hand ui-icon-trash" title="删除">&#xe60e;</i>';
		html_con += '<i class="iconfont i-hand ui-icon-menu" title="菜单">&#xe656;</i>';
		html_con += '</div>';
		return html_con;
	}
};
var THISPAGE = {
	init:function (){
		this.initParam();
		this.initGrid();
		this.initEvent();	
	},
	initParam:function(){
		var pdata = config.SHOP_TYPES_QUERY;
		typeCombo = $('#typeSpan').combo({
			data:pdata,
	        value: 'id',
	        text: 'name',
	        width : 158,
			height : 30,
			listId : '',
			editable : false,
			defaultSelected:-1,
			callback : {
				onChange : function(data) {
					$("#ro_shop_type").val(data.id);
					$("#ty_name").val(data.name);
				}
			}
		}).getCombo();
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
	    	{name: 'operate',label:'操作',width: 70, fixed:true, formatter: handle.operFmatter,align:'center', title: false,sortable:false},
	    	{name: 'ro_code',label:'编号',index: 'ro_code',width:80, title: false},
	    	{name: 'ro_name',label:'名称',index: 'ro_name',width:120, title: false},
	    	{name: 'ty_name',label:'类型',index: 'ty_name',width:120, title: false}
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
			datatype: 'json',
			width: gridWH.w-12,
			height: gridWH.h-20,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:30,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id: 'ro_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	reloadData:function(){
		var param = "?searchContent="+$("#SearchContent").val();
		param += "&shop_type="+$("#ro_shop_type").val();
		$("#grid").jqGrid('setGridParam',{datatype:"json",url:queryurl+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		$('#SearchContent').on('keyup',function(event){
			if (event.keyCode==13){
				$('#search').click();
			}
		});
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
		//新增
		$('#btn-add').on('click', function(e){
			e.preventDefault();
			handle.operate('add');
		});
		//修改
		$('#grid').on('click', '.operating .ui-icon-pencil', function(e){
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.operate('edit', id);
		});
		//删除
		$('#grid').on('click', '.operating .ui-icon-trash', function(e){
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.del(id);
		});
		$('#grid').on('click', '.operating .ui-icon-menu', function(e){
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.updateRoleMenu(id);
		});
	}
};
THISPAGE.init();