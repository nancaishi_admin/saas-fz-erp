var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;;
var handle = {
	save:function(){
		var ro_name = $("#ro_name").val().trim();
	    if(ro_name == ''){
	    	W.Public.tips({type: 1, content : "请输入名称"});
	        $("#ro_name").select().focus();
	     	setTimeout("api.zindex()",400);
	        return;
	    }else{
	    	$("#ro_name").val(ro_name);//防止空格
	    }
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"sys/role/update",
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					W.Public.tips({type: 1, content : data.message});
					handle.loadData(data.data);
					setTimeout("api.close()",500);
				}else if(data.stat == 304){
					W.Public.tips({type: 1, content : data.message});
					setTimeout("api.zindex()",500);
				}else{
					W.Public.tips({type: 1, content : data.message});
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.ro_id;
		pdata.ro_code=data.ro_code;
		pdata.ro_name=data.ro_name;
		pdata.ty_name=data.ty_name;
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initParam();
		this.initDom();
		this.initEvent();
	},
	initParam:function(){
	},
	initDom:function(){
		$("#ro_name").select().focus();
	},
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save();
		});
		$("#btn_close").click(function(){
			api.close();
		});
	}
};
THISPAGE.init();