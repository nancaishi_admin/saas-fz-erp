var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var api = frameElement.api;
var multiselect = false;
if(undefined != api.data.multiselect){
	multiselect = api.data.multiselect;
}
var _height = api.config.height-132,_width = api.config.width-34;//获取弹出框弹出宽、高
var queryurl = config.BASEPATH+'sys/user/page';
var dblClick = false;
var THISPAGE = {
	init:function (){
		this.initGrid();
		this.initEvent();	
	},
	initGrid:function(){
		var colModel = [
			{name: 'us_id', label:'',index: 'us_id', width: 70, hidden: true},
			{name: 'us_code', label:'编号',index: 'us_code', width: 70, title: false},
			{name: 'us_account', label:'账户',index: 'us_account', width: 70, title: false},
	    	{name: 'us_name', label:'名称',index: 'us_name', width: 80, title: false},
	    	{name: 'ro_name',label:'角色',index: 'ro_name',width:120, title: false},
	    	{name: 'shop_name',label:'门店',index: 'shop_name',width:120, title: false}
	    ];
		$('#grid').jqGrid({
            url:queryurl+"?"+THISPAGE.buildParams(),			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:multiselect,//多选
			viewrecords: true,
			rowNum:30,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'us_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow: function (rowid, iRow, iCol, e) {
				if(!multiselect){
					dblClick = true;
					api.close();
				}
            }
	    });
	},
	reloadData:function(){
		var param=this.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+"?"+param}).trigger("reloadGrid");
	},
	buildParams:function(){
		var params = '';
		params += "searchContent="+Public.encodeURI($.trim($('#SearchContent').val()));
		params += "&sp_code="+system.SHOP_CODE;;
		return params;
	},
	initEvent:function(){
		$('#SearchContent').on('keyup',function(event){
			if (event.keyCode==13){
				$('#btn-search').click();
			}
		});
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
	}
};
function doSelect(){
	if(multiselect){//多选
		var ids = $("#grid").jqGrid('getGridParam', 'selarrrow');
		if(ids == null || ids.length == 0){
			Public.tips({type: 2, content : "请选择用户！"});
			return false;
		}
		var result = [];
		for (var i = 0; i < ids.length; i++) {
			result.push($("#grid").jqGrid("getRowData", ids[i]));
		}
		return result;
		
	}else{//单选
		var selectedId = $("#grid").jqGrid('getGridParam', 'selrow');
		if(selectedId == null || selectedId == ""){
			Public.tips({type: 2, content : "请选择用户！"});
			return false;
		}
		var rowData = $("#grid").jqGrid("getRowData", selectedId);
		return rowData;
	}
}
THISPAGE.init();