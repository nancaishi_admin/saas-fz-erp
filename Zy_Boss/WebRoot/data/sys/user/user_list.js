var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var isRefresh = false;
var _limitMenu = system.MENULIMIT.BASE13;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'sys/user/page';
var handle = {
	operate: function(oper, id){//修改、新增
		var width = 580;
		var height = 260;
		if(oper == 'add'){
			title = '新增';
			url = config.BASEPATH+"sys/user/to_add";
			data = {oper: oper, callback: this.callback};
		}else if(oper == 'edit'){
			title = '修改';
			var data = $("#grid").jqGrid('getRowData', id);
			var us_default = data.us_default;
			if(us_default == '1'){
				Public.tips({type: 1, content : "不能修改默认帐户!"});
				return;
			}
			url = config.BASEPATH+"sys/user/to_update?us_id="+id;
			data = {oper: oper,callback: this.callback};
		}else{
			return false;
		}
		$.dialog({
			title : title,
			content : 'url:'+url,
			data: data,
			width : width,
			height : height,
			max : false,
			min : false,
			cache : false,
			lock: true
		});
	},
	del: function(rowIds){//删除
		if(rowIds != null && rowIds != ''){
			var data = $("#grid").jqGrid('getRowData', rowIds);
			if(null != data){
				var us_code = data.us_code;
				var us_default = data.us_default;
				if(us_default == '1'){
					Public.tips({type: 1, content : "不能删除默认帐户!"});
					return;
				}
				if(us_code == system.US_CODE){
					Public.tips({type: 1, content : "不能删除自己的帐户!"});
					return;
				}
				$.dialog.confirm('删除后无法恢复，确定删除吗?', function(){
					$.ajax({
						type:"POST",
						url:config.BASEPATH+'sys/user/del',
						data:{"us_id":rowIds},
						cache:false,
						dataType:"json",
						success:function(data){
							if(undefined != data && data.stat == 200){		 		
								$('#grid').jqGrid('delRowData', rowIds);
							}else{
								Public.tips({type: 1, content : "删除失败"});
							}
						}
					});
				});
			}
	   }else{
	      $.dialog.tips("请选择数据!!!",1,"32X32/fail.png");
	   }	
    },
    initPwd:function(id){
	 	$.dialog.confirm('确定要初始化密码吗?', function(){
			$.ajax({
				type:"POST",
				url:config.BASEPATH+'sys/user/initPwd',
				data:{"us_id":id},
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){		 		
						Public.tips({type: 1, content : "初始化成功!"});
					}else{
						Public.tips({type: 1, content : "初始化失败!"});
					}
				}
			});
		});
    },
    doState:function(obj,id){
    	var data = $("#grid").jqGrid('getRowData', id);
		var us_default = data.us_default;
		if(us_default == '1'){
			Public.tips({type: 1, content : "不能停用默认帐户!"});
			return;
		}
		var us_state = data.us_state,title="";
		if(us_state == '1'){
			title = "确定要启用吗?";
			us_state = "0";
		}else{
			title="确定要停用吗?"
			us_state = "1";
		}
    	$.dialog.confirm(title, function(){
			$.ajax({
				type:"POST",
				url:config.BASEPATH+'sys/user/updateState',
				data:{"us_id":id,"us_state":us_state},
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						$("#grid").jqGrid("setRowData", id,{us_state:us_state});
						if(us_state == "1"){
							$(obj).html("&#xe63c;")
							$(obj).removeClass("ui-icon-stop").addClass("ui-icon-begin");
							$(obj).attr("title","启用");
						}else{
							$(obj).html("&#xe60d;");
							$(obj).removeClass("ui-icon-begin").addClass("ui-icon-stop");
							$(obj).attr("title","停用");
						}
						Public.tips({type: 1, content : "状态修改成功!"});
					}else{
						Public.tips({type: 1, content : "状态修改失败!"});
					}
				}
			});
		});
    },
	callback: function(data, oper, dialogWin){
		if(isRefresh){
			THISPAGE.reloadData();
		}
	},
	formatPayUser:function(val, opt, row){
		var free = "是";
		if(val != "1"){
			free = "否";
		}
		return free;
	},
	doShop:function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_sin_list_dialog',
			data : {},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#sp_code").val(selected.sp_code)
					$("#sp_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	},
	doRole:function(){
		commonDia = $.dialog({
			title : '选择',
			content : 'url:'+config.BASEPATH+'sys/role/to_sin_list_dialog',
			data : {},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#ro_code").val(selected.ro_code)
					$("#ro_name").val(selected.ro_name);
				}
			},
			cancel:true
		});
	},
	operFmatter:function (val, opt, row) {
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-pencil" title="修改">&#xe60c;</i>';
		html_con += '<i class="iconfont i-hand ui-icon-trash" title="删除">&#xe60e;</i>';
		var _state = '&#xe60d;',_title = "停用",_class = "ui-icon-stop";
		if(row.us_state != 0){
			_state = '&#xe63c;';
			_title = "启用";
			_class = "ui-icon-begin"
		}
		html_con += '<i class="iconfont i-hand '+_class+'" title="'+_title+'">'+_state+'</i>';
		html_con += '<i class="iconfont i-hand ui-icon-init" title="初始化">&#xe63a;</i>';
		html_con += '</div>';
		return html_con;
	},
	formatDefault:function (val, opt, row) {
		var _title = "否";
		if(row.us_default == 1){
			_title = "是";
		}
		return _title;
	}
};
var THISPAGE = {
	init:function (){
		this.initParam();
		this.initGrid();
		this.initEvent();	
	},
	initParam:function(){
		var pdata = config.SHOP_TYPES_QUERY;
		typeCombo = $('#typeSpan').combo({
			data:pdata,
	        value: 'id',
	        text: 'name',
	        width : 158,
			height : 50,
			listId : '',
			editable : false,
			defaultSelected:-1,
			callback : {
				onChange : function(data) {
					$("#us_shop_type").val(data.id);
					$("#ty_name").val(data.name);
				}
			}
		}).getCombo();
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
	    	{name: 'operate',label:'操作',width: 90, fixed:true, formatter: handle.operFmatter,align:'center', title: false,sortable:false},
	    	{name: 'us_state',label:'状态',index: 'us_state',hidden:true, title: false},
	    	{name: 'us_default',label:'默认',index: 'us_default',hidden:true, title: false},
	    	{name: 'us_code',label:'编号',index: 'us_code',width:70,hidden:true, title: false},
//	    	{name: 'us_pay',label:'收费',index: 'us_pay',width:60,align:'center',formatter:handle.formatPayUser,title: false},
	    	{name: '_us_default',label:'默认',index: '_us_default',align:'center',width:60,formatter:handle.formatDefault,title: false},
	    	{name: 'us_account',label:'帐号',index: 'us_account',width:80, title: false},
	    	{name: 'us_name',label:'名称',index: 'us_name',width:120, title: false},
	    	{name: 'ty_name',label:'类型',index: 'ty_name',width:80, title: false},
	    	{name: 'ro_name',label:'角色',index: 'ro_name',width:120, title: false},
	    	{name: 'shop_name',label:'门店',index: 'shop_name',width:120, title: false},
//	    	{name: 'us_end',label:'到期时间',index: 'us_end',width:100, title: false},
	    	{name: 'us_last',label:'最后登录',index: 'us_last',width:150, title: false}
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
			datatype: 'json',
			width: gridWH.w-12,
			height: gridWH.h-20,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:30,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'us_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	reloadData:function(){
		var param = "?searchContent="+$("#SearchContent").val();
		param += "&sp_code="+$("#sp_code").val();
		param += "&ro_code="+$("#ro_code").val();
		$("#grid").jqGrid('setGridParam',{datatype:"json",url:queryurl+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		$('#SearchContent').on('keyup',function(event){
			if (event.keyCode==13){
				$('#search').click();
			}
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			$("#SearchContent").val("");
			$("#sp_code").val("");
			$("#sp_name").val("");
			$("#ro_code").val("");
			$("#ro_name").val("");
		});
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
		//新增
		$('#btn-add').on('click', function(e){
			e.preventDefault();
			handle.operate('add');
		});
		$("#btn_shop").on('click',function(e){
			e.preventDefault();
			handle.doShop();
		});
		$("#btn_role").on('click',function(e){
			e.preventDefault();
			handle.doRole();
		});
		//修改
		$('#grid').on('click', '.operating .ui-icon-pencil', function(e){
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.operate('edit', id);
		});
		//删除
		$('#grid').on('click', '.operating .ui-icon-trash', function(e){
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.del(id);
		});
		$('#grid').on('click', '.operating .ui-icon-stop', function(e){
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.doState(this,id);
		});
		$('#grid').on('click', '.operating .ui-icon-begin', function(e){
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.doState(this,id);
		});
		$('#grid').on('click', '.operating .ui-icon-init', function(e){
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.initPwd(id);
		});
	}
};
THISPAGE.init();