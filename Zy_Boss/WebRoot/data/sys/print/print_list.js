var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var sp_type = $("#sp_type").val();
var queryurl = config.BASEPATH+'sys/print/list/'+sp_type;

var api = frameElement.api;
var _height = api.config.height-102,_width = api.config.width-34;//获取弹出框弹出宽、高

var handle = {
	formatOperate : function(val, opt, row){
		return "<input type='button' class='btn_sp' onclick='javascript:handle.doPrint("+opt.rowId+");' value='打印'/>";
	},
	doPrint:function(sp_id){
		var params = "";
		params += "number="+api.data.number;
		if(api.data.displayMode != undefined){
			params += "&displayMode="+api.data.displayMode;
		}
		params += "&sp_id="+sp_id;
		if(api.data.extraParams != undefined){
			params += api.data.extraParams;
		}
		var printUrl = '';
		if(sp_type == '1'){//采购订单
			printUrl = config.BASEPATH+"buy/order/print";
		}else if(sp_type == '2'){//采购入库单
			printUrl = config.BASEPATH+"buy/enter/print";
		}else if(sp_type == '3'){//批发订单
			printUrl = config.BASEPATH+"batch/order/print";
		}else if(sp_type == '4'){//批发出入库单
			printUrl = config.BASEPATH+"batch/sell/print";
		}else if(sp_type == '5'){//配货单据
			printUrl = config.BASEPATH+"sort/allot/print";
		}else if(sp_type == '6'){//店铺单据
			printUrl = config.BASEPATH+"shop/want/print";
		}else if(sp_type == '7'){//库存调拨单
			printUrl = config.BASEPATH+"stock/allocate/print";
		}else if(sp_type == '8'){
			if(api.data.from == 'adjust'){//库存调整单
				printUrl = config.BASEPATH+"stock/adjust/print";
			}else if(api.data.from == 'loss'){//库存报损单
				printUrl = config.BASEPATH+"stock/loss/print";
			}else if(api.data.from == 'overflow'){//库存报溢单
				printUrl = config.BASEPATH+"stock/overflow/print";
			}
		}else if(sp_type == '9'){//供应商结算单
			printUrl = config.BASEPATH+"buy/settle/print";
		}else if(sp_type == '10'){//客户结算单
			printUrl = config.BASEPATH+"batch/settle/print";
		}else if(sp_type == '11'){//店铺结算单
			printUrl = config.BASEPATH+"sort/settle/print";
		}else if(sp_type == '12'){//供应商费用单
			printUrl = config.BASEPATH+"buy/fee/print";
		}else if(sp_type == '13'){//客户费用单
			printUrl = config.BASEPATH+"batch/fee/print";
		}else if(sp_type == '14'){//店铺费用单
			printUrl = config.BASEPATH+"sort/fee/print";
		}else if(sp_type == '15'){//其他收入单
			printUrl = config.BASEPATH+"money/income/print";
		}else if(sp_type == '16'){//银行存取款
			printUrl = config.BASEPATH+"money/access/print";
		}else if(sp_type == '18'){//费用开支单
			printUrl = config.BASEPATH+"money/expense/print";
		}
		if(printUrl == ''){
			Public.tips({type: 1, content : '打印模板暂未开放，请联系管理员！'});
			return;
		}
		$.ajax({
            type: "POST",
            url: printUrl,
            data: params,
            cache:false,
			dataType:"json",
            success: function (data) {
            	if(undefined != data && data.stat == 200){
            		var unit = "mm";
					var html = data.data.styleHtml+data.data.listHtml;
					var headHtml = data.data.styleHtml+data.data.headHtml
					var page_height = data.data.page_height+unit;
					var page_width = data.data.page_width+unit;
					var totalHeadHeight = data.data.totalHeadHeight +unit;
					var page_top = data.data.page_top + unit;
					var page_left = data.data.page_left + unit;
					var page_direction = data.data.page_direction;
					var page_title = data.data.page_title;
					//var LODOP=getLodop(document.getElementById('LODOP_OB'),document.getElementById('LODOP_EM'));
					LODOP.PRINT_INIT(page_title);
					LODOP.SET_PRINT_PAGESIZE(page_direction,page_width,page_height,"");
					LODOP.ADD_PRINT_TABLE(totalHeadHeight,"0mm","100%","100%",html);
					//LODOP.SET_PRINT_STYLEA(0,"TableHeightScope",1);
					LODOP.ADD_PRINT_HTM("0mm","0mm","100%",totalHeadHeight,headHtml);
					LODOP.SET_PRINT_STYLEA(0,"ItemType",1);// ItemType的值：数字型，0--普通项 1--页眉页脚 2--页号项 3--页数项 4--多页项
					LODOP.SET_PRINT_MODE("PRINT_PAGE_PERCENT","100%");//整宽不变形
					LODOP.SET_SHOW_MODE("LANDSCAPE_DEFROTATED", 1);
					LODOP.PREVIEW();
				}else{
					Public.tips({type: 1, content : data.message});
				}
            }
        });
		
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		
	},
	initGrid:function(){
		var colModel = [
	    	{label:'模板名称',name: 'sp_name',index: 'sp_name',width:160},
	    	{label:'操作',name: 'operate', width: 80,formatter: handle.formatOperate},
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:99,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'sp_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	initEvent:function(){
		
	}
}
THISPAGE.init();