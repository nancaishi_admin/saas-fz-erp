var api = frameElement.api, oper = api.opener;
var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var callback = api.data.callback;
var _height = api.config.height-132,_width = api.config.width-34;//获取弹出框弹出宽、高
var queryurl = config.BASEPATH + 'sys/workbench/listBySelf';

var $_matchCon = null;
var Utils = {
    buildUrl: function() {
        var url = "&wb_type="+$("#wb_type").val();
        return url;
    },
    formatType : function(val, opt, row){
		if (val == 0) {
			return '日常工作';
		} else if (val == 1) {
			return '日常报表';
		} else if (val == 2) {
			return '待办事项';
		}
		return '';
	},
	del : function() {
		var ids = $('#grid').jqGrid('getGridParam', 'selarrrow');
        if(ids == null || ids == '' || ids.length == 0){
        	Public.tips({type: 2, content : "你没有选中菜单,请重新选择!"});
        	return;            	
        }
    	$.ajax({
    		type: "POST",
            async: false,
            url: config.BASEPATH + "sys/workbench/del",
            data:{ids:ids.join(",")},
            cache: false,
            dataType:"json",
            success: function (data) {
            	if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : '删除成功!'});
					if (ids instanceof Array) {
                        for (var i = ids.length - 1; i >= 0; i--) {
                            $('#grid').jqGrid('delRowData', ids[i]);
                        }
                    } else {
                        $('#grid').jqGrid('delRowData', ids);
                    }
            		if(api.data.refresh != undefined){
            			api.data.refresh.val("true");
            		}
				}else{
					Public.tips({type: 1, content : '删除失败!'});
				}
            }
    	});
	}
};

var THISPAGE = {
    init: function () {
        this.initDom();
        this.initGrid();
        this.initEvent();
    },
    initDom: function() {
        this.$_TypeLi = $('#td_type').cssRadio({
            callback: function($_obj) {
            	$("#wb_type").val($_obj.find("input").attr("value"));
            	THISPAGE.reloadData();
            }
        });
    },
    initGrid: function () {
        var colModel = [
            {label:'菜单名称',name: 'mn_name', index: 'mn_name', width: 180},
            {label:'类型',name: 'wb_type', index: 'wb_type', width: 100,align:'center', formatter: Utils.formatType}
        ];
        $('#grid').jqGrid({
            url: queryurl + '?' + Utils.buildUrl(),
            datatype: 'json',
            width: _width,
			height: _height,
            altRows: true,
            gridview: true,
            onselectrow: false,
            colModel: colModel,
            rownumbers: true,//行号
            pager: '#page',//分页
            multiselect: true,//多选
            viewrecords: true,
            cmTemplate: {sortable: true, title: false},
            page: 1, //只有一页
			pgbuttons:false,
			pgtext:false,
			rowNum:99999,//每页条数
			recordtext:'{0} - {1} 共 {2} 条',  
            shrinkToFit: false,//ture，则按比例初始化列宽度
            forceFit: false,//调整列宽度会改变表格的宽度  shrinkToFit 为false时，此属性会被忽略
            jsonReader: {
            	root: "data",
				repeatitems : false,
				id: 'wb_id'  //图标ID
            },
            loadComplete: function (data) {
            	
            }
        });
    },
    reloadData: function () {
        var params = Utils.buildUrl();
        $("#grid").jqGrid('setGridParam', {datatype: "json",page:1, url: queryurl + "?" + params}).trigger("reloadGrid");
    },
    initEvent: function () {
    	$('#removeButton').on('click', function(e) {
            e.preventDefault();
            Utils.del();
        });
    }
}
THISPAGE.init();

