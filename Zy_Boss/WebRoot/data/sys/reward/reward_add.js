var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;

var Utils = {
	doQueryIcon:function(){
		commonDia = $.dialog({
			title : '选择图标',
			content : 'url:'+config.BASEPATH+'common/rewardicon/to_list_dialog',
			data : {multiselect:false},
			width : 260,
			height : 260,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					Public.tips({type: 2, content : "请选择图标！"});
					return false;
				}
				$("#rw_icon").val(selected.ri_icon);
				$("#rw_style").val(selected.ri_style);
				$("#div_icon").html('<i class="iconfont '+selected.ri_style+'">'+selected.ri_icon+'</i>');
				return true;
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#rw_icon").val(selected.ri_icon);
					$("#rw_style").val(selected.ri_style);
					$("#div_icon").html('<i class="iconfont '+selected.ri_style+'">'+selected.ri_icon+'</i>');
				}
			},
			cancel:true
		});
	}
};

var handle = {
	save:function(){
		var saveUrl = config.BASEPATH+"sys/reward/save";
		if($("#rw_id").val() != undefined){
			saveUrl = config.BASEPATH+"sys/reward/update";
		}
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:saveUrl,
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					handle.loadData(data.data);
				}else{
					Public.tips({type: 1, content : data.message});
					setTimeout("api.zindex()",500);
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.rw_id;
		pdata.rw_code=data.rw_code;
		pdata.rw_name=data.rw_name;
		pdata.rw_score=data.rw_score;
		pdata.rw_icon=data.rw_icon;
		pdata.rw_style=data.rw_style;
		pdata.rw_remark=data.rw_remark;
		pdata.icon='';
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initEvent();
	},
	initDom:function(){
		$("#rw_name").focus();
	},
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save();
		});
		$("#btn_close").click(function(){
			api.close();
		});
	}
};
THISPAGE.init();