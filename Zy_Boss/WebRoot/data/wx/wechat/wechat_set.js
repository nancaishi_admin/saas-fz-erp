var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;

$("#wx_bg").uploadPreview({ 
	Img: "imgview", 
	Width: 320, 
	Height: 160,
	Callback:function(){
	}
});

var Utils = {
	saveWechatInfo:function(obj){
		$(obj).attr("disabled",true);
		document.forms['form1'].encoding="multipart/form-data";
		$("#form1").ajaxSubmit({
			type:"POST",
			timeout:20000,
			url:config.BASEPATH+"wx/wechat/updateWechatInfo",
			cache:false,
			dataType:"json",
			success:function(data){
			    if(undefined != data && data.stat == 200){
				 	Public.tips({type: 3, content : "修改成功！！"});
				 	$("#wx_id").val($.trim(data.data.wx_id));
				}else{
					Public.tips({type: 1, content : data.message});
				}
			    $(obj).attr("disabled",false);
			}
		 });
	},
	saveWechatSet:function(obj){
		$(obj).attr("disabled",true);
		document.forms['form1'].encoding="multipart/form-data";
		$("#form1").ajaxSubmit({
			type:"POST",
			timeout:20000,
			url:config.BASEPATH+"wx/wechat/updateWechatSet",
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : "修改成功！！"});
					$("#ws_id").val($.trim(data.data.ws_id));
				}else{
					Public.tips({type: 1, content : data.message});
				}
				$(obj).attr("disabled",false);
			}
		});
	}
};


var THISPAGE = {
	init:function (){
		this.initDom();
		this.initEvent();
	},
	initDom:function(){
		this.initMemberType();
	},
	initMemberType:function(){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"vip/membertype/list",
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.data != "" && data.data.length > 0){
					$('#span_membertype').combo({
						value : 'mt_code',
						text : 'mt_name',
						width : 237,
						height : 300,
						listHeight : 300,
						listId : '',
						defaultSelected : 0,
						editable : false,
						callback : {
							onChange : function(data) {
								$("#wx_mt_code").val(data.mt_code);
							}
						}
					}).getCombo().loadData(data.data,[ 'mt_code', $("#wx_mt_code").val(), 0 ]);
				}
			}
		 });
	},
	initEvent:function(){
		
	}
};

THISPAGE.init();