var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;

$("#img_type").uploadPreview({ 
	Img: "imgview", 
	Width: 320, 
	Height: 160,
	Callback:function(){
	}
});

var handle = {
	save:function(){
	    if($("#pt_name").val().trim() == ''){
	    	Public.tips({type: 1, content : "请输入名称"});
	        return;
	    }
		$("#btn-save").attr("disabled",true);
		var url = config.BASEPATH+"wx/producttype/save";
		if($.trim($("#pt_id").val()) != ""){
			url = config.BASEPATH+"wx/producttype/update";
		}
		document.forms['form1'].encoding="multipart/form-data";
		$("#form1").ajaxSubmit({
			type:"POST",
			timeout:20000,
			url:url,
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					handle.loadData(data.data);
				}else{
					Public.tips({type: 1, content : data.message});
				}
				$("#btn-save").attr("disabled",false);
			}
		 });
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.pt_id;
		pdata.pt_code=data.pt_code;
		pdata.pt_upcode=data.pt_upcode;
		pdata.pt_name=data.pt_name;
		pdata.pt_state=data.pt_state;
		pdata.pt_weight=data.pt_weight;
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initEvent();
	},
	initDom:function(){
		this.$_state = $("#state").cssRadio({ callback: function($_obj){
			$("#pt_state").val($_obj.find("input").val());
		}});
		$("#pt_upcode").val(api.data.pt_upcode);
	},
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save();
		});
		$("#btn_close").click(function(){
			api.close();
		});
	}
};
THISPAGE.init();