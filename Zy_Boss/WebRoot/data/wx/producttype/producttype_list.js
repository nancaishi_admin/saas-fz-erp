var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var isRefresh = false;
var _limitMenu = system.MENULIMIT.WECHAT02;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'wx/producttype/list';
var handle = {
	operate: function(oper, id){//修改、新增
		var height = 360;
		var width = 480;
		if(oper == 'add'){
			title = '新增';
			url = config.BASEPATH+"wx/producttype/to_add";
			data = {oper: oper, callback: this.callback};
			var selected = window.parent.frames["treeFrame"].selectedTreeNode;
			if(selected == undefined){
				Public.tips({type: 2, content : "请选择左侧父类!"});
				return false;
			}
			if(selected.id != "0" && selected.pId != "0"){
				Public.tips({type: 2, content : "不能在当前类别下增加子类!"});
				return false;
			}
			data.pt_upcode = selected.id;
		}else if(oper == 'edit'){
			title = '修改';
			url = config.BASEPATH+"wx/producttype/to_update?pt_id="+id;
			data = {oper: oper,callback: this.callback};
		}else{
			return false;
		}
		$.dialog({
			title : title,
			content : 'url:'+url,
			data: data,
			width : width,
			height : height,
			max : false,
			min : false,
			cache : false,
			lock: true
		});
	},
	del: function(rowIds){//删除
		if(rowIds != null && rowIds != ''){
		 	$.dialog.confirm('删除后无法恢复，确定删除吗?', function(){
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'wx/producttype/del',
					data:{"pt_id":rowIds},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){		 		
							$('#grid').jqGrid('delRowData', rowIds);
							Public.tips({type: 3, content : "删除成功"});
						}else{
							Public.tips({type: 1, content : "删除失败"});
						}
					}
				});
			});
	   }else{
	      $.dialog.tips("请选择数据!!!",1,"32X32/fail.png");
	   }	
    },
	callback: function(data, oper, dialogWin){
		var gridData = $("#grid").data('gridData');
		if(!gridData){
			gridData = {};
			$("#grid").data('gridData', gridData);
		}
		gridData[data.id] = data;
		if(oper == "edit") {
			Public.tips({type: 3, content : "修改成功"});
			$("#grid").jqGrid('setRowData', data.id, data);
			dialogWin && dialogWin.api.close();
		} 
		if(oper == "add"){
			Public.tips({type: 3, content : "保存成功"});
			$("#grid").jqGrid('addRowData', data.id, data, 'first');
			dialogWin && dialogWin.api.close();
		}
	},
	formatState:function(val, opt, row){
		if(val == "0"){
			return "正常";
		}else if(val == "1"){
			return "停用";
		}else{
			return val;
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initGrid();
		this.initEvent();	
	},
	initParam:function(){
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
	    	{label:'操作',name: 'operate',width: 70, fixed:true, formatter: Public.operFmatter,align:'center',sortable:false},
	    	{label:'编号',name: 'pt_code',index: 'pt_code',width:80},
	    	{label:'名称',name: 'pt_name',index: 'pt_name',width:120},
	    	{label:'权重',name: 'pt_weight',index: 'pt_weight',width:80,align:'center'},
	    	{label:'状态',name: 'pt_state',index: 'pt_state',width:80,align:'center',formatter:handle.formatState}
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
			datatype: 'json',
			width: gridWH.w-12,
			height: gridWH.h-20,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:999,//每页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id: 'pt_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	reloadData:function(data){
		var param = "?searchContent="+$("#SearchContent").val();
		if(data != undefined && data != null){
			if(data.hasOwnProperty("pt_upcode") && data.pt_upcode != ''){
				param += "&pt_upcode="+data.pt_upcode;
			}
		}
		$("#grid").jqGrid('setGridParam',{datatype:"json",url:queryurl+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		$('#SearchContent').on('keyup',function(event){
			if (event.keyCode==13){
				$('#btn-search').click();
			}
		});
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			};
			THISPAGE.reloadData();
		});
		//新增
		$('#btn-add').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.INSERT)) {
				return ;
			};
			handle.operate('add');
		});
		//修改
		$('#grid').on('click', '.operating .ui-icon-pencil', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.UPDATE)) {
				return ;
			};
			var id = $(this).parent().data('id');
			handle.operate('edit', id);
		});
		//删除
		$('#grid').on('click', '.operating .ui-icon-trash', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.DELETE)) {
				return ;
			};
			var id = $(this).parent().data('id');
			handle.del(id);
		});
	}
};
THISPAGE.init();