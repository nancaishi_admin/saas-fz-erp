var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var api = frameElement.api;
var queryurl = config.BASEPATH+'approve/list?number='+api.data.number+"&ar_type="+api.data.ar_type;
var _height = api.config.height-102,_width = api.config.width-34;//获取弹出框弹出宽、高
var handle = {
	formatArState :function(val, opt, row){//1-审核通过,2-审核未通过,3-反审核,4-终止
		if(val == '1'){
			return '通过';
		}else if(val == '2'){
			return '驳回';
		}else if(val == '3'){
			return '反审核';
		}else if(val == '4'){
			return '终止';
		}else if(val == '5'){
			return '发货';
		}else if(val == '6'){
			return '拒收';
		}else if(val == '7'){
			return '确认';
		}else if(val == '8'){
			return '完成';
		}
		return '';
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		
	},
	initGrid:function(){
		var colModel = [
	    	{label:'状态',name: 'ar_state',index: 'ar_state',width:45,formatter: handle.formatArState},
	    	{label:'时间',name: 'ar_sysdate',index: 'ar_sysdate',width:148},
	    	{label:'操作人',name: 'ar_us_name',index: 'ar_us_name',width:55},
	    	{label:'备注',name: 'ar_describe',index: 'ar_describe',width:100}
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:99,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:true},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'ar_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	initEvent:function(){
		
	}
}
THISPAGE.init();