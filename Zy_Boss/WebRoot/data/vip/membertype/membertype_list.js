var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var isRefresh = false;
var _limitMenu = system.MENULIMIT.VIP02;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'vip/membertype/list';
var pdata = {};
var handle = {
	operate: function(oper, id){//修改、新增
		var height = 400;
		var width = 390;
		if(oper == 'add'){
			title = '新增';
			url = config.BASEPATH+"vip/membertype/to_add";
			data = {oper: oper, callback:this.callback};
		}else if(oper == 'edit'){
			title = '修改';
			url = config.BASEPATH+"vip/membertype/to_update?mt_id="+id;
			data = {oper: oper,callback: this.callback};
		}else{
			return false;
		}
		$.dialog({
			title : title,
			content : 'url:'+url,
			data: data,
			width : width,
			height : height,
			max : false,
			min : false,
			cache : false,
			lock: true
		});
	},
	del: function(rowId){//删除
		if(rowId != null && rowId != ''){
		 	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'vip/membertype/del',
					data:{"mt_id":rowId},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){
							Public.tips({type: 3, content : '删除成功!'});
							$('#grid').jqGrid('delRowData', rowId);
						}else{
							Public.tips({type: 1, content : '删除失败!'});
						}
					}
				});
			});
	   }else{
		   Public.tips({type: 1, content : '请选择数据!'});
	   }	
    },
    callback: function(data, oper, dialogWin){
		var gridData = $("#grid").data('gridData');
		if(!gridData){
			gridData = {};
			$("#grid").data('gridData', gridData);
		}
		gridData[data.id] = data;
		if(oper == "edit") {
			$("#grid").jqGrid('setRowData', data.id, data);
			dialogWin && dialogWin.api.close();
		} 
		if(oper == "add"){
			$("#grid").jqGrid('addRowData', data.id, data, 'first');
		}
	},
	formatAutoUp:function(val, opt, row){
		if(val == "1"){
			return "自动升级";
		}else if(val == "0"){
			return "手动升级";
		}else{
			return val;
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
	    	{name: 'operate',label:'操作',width: 70, formatter: Public.operFmatter,align:'center', sortable:false},
	    	{name: 'mt_code',label:'编号',index: 'mt_code',width:80},
	    	{name: 'mt_name',label:'名称',index: 'mt_name',width:120},
	    	{name: 'mt_discount',label:'享受折扣',index: 'mt_discount',width:100},
	    	{name: 'mt_isauto_up',label:'升级方式',index: 'mt_isauto_up',width:100,align:'center',formatter:handle.formatAutoUp},
	    	{name: 'mt_min_point',label:'升级下限积分',index: 'mt_min_point',width:100},
	    	{name: 'mt_money',label:'消费N元',index: 'mt_money',width:100},
	    	{name: 'mt_point',label:'积多少分',index: 'mt_point',width:100},
	    	{name: 'mt_point_to_money',label:'N积分抵1元',index: 'mt_point_to_money',width:100},
	    	{name: 'mt_mark',label:'备注',index: 'mt_mark',width:180}
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
			datatype: 'json',
			width: gridWH.w-12,
			height: gridWH.h-20,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:99,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'mt_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	reloadData:function(data){
		var param="";
		if(data != null && "" != data){
			param += "?1=1";
			if(data.hasOwnProperty("searchContent") && data.searchContent != ''){
				param += "&searchContent="+Public.encodeURI(data.searchContent);
			}
		}
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		$('#SearchContent').on('keyup',function(){
			if (event.keyCode==13){
				$('#btn-search').click();
			}
		});
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			var searchContent = $.trim($('#SearchContent').val());
			var pdata = {};
			pdata.searchContent = searchContent;
			THISPAGE.reloadData(pdata);
		});
		//新增
		$('#btn-add').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.INSERT)) {
				return ;
			}
			handle.operate('add');
		});
		//修改
		$('#grid').on('click', '.operating .ui-icon-pencil', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.UPDATE)) {
				return ;
			}
			var id = $(this).parent().data('id');
			handle.operate('edit', id);
		});
		//删除
		$('#grid').on('click', '.operating .ui-icon-trash', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.DELETE)) {
				return ;
			}
			var id = $(this).parent().data('id');
			handle.del(id);
		});
	}
}
THISPAGE.init();