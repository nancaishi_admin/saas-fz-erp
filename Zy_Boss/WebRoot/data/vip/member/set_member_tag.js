var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var api = frameElement.api, W = api.opener;
var _limitMenu = system.MENULIMIT.VIP01;
var _menuParam = config.OPTIONLIMIT;

var Utils = {
	deleteTagDAO:function(vt_code,del_type,vt_name){
		var content="";
		var userType=$("#userType").val();
		var usedCount=$("#usedCount").html();
		content="<span style='font-size:16px;font-weight:bold;'>"+vt_name+'</span>标签目前已绑定<span style="color:red;font-size:18px;">'+usedCount+"</span>个会员，确定全部删除吗？";
		if(del_type=="vt"){
			$.dialog.confirm(content,function(){
				Utils.deleteTag(vt_code,del_type); 
			}, function(){
				return;
			});
		}else{
			Utils.deleteTag(vt_code,del_type);
		}
	},
	deleteTag:function(vt_code,del_type){
		var vm_code= $("#vm_code").val();
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"vip/member/del_vip_tag",
			data:{vt_code:vt_code,del_type:del_type,vm_code:vm_code},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : '删除成功!'});
					THISPAGE.reload(vm_code);
				}else {
					Public.tips({type: 1, content : data.message});
				}
	    	  }
		 });
	},
	setMemberTag:function(vm_code,vt_code,vt_name){
		var member_type_count = $("*[name='member_type_used']").length;
		if(member_type_count>=3){
			Public.tips({type: 2, content : '会员最多设置3个标签!'});
			return;
		}
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"vip/member/set_member_tag",
			data:{vt_code:vt_code,vm_code:vm_code,vt_name:vt_name},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					W.isRefresh = true;
					Public.tips({type: 3, content : '添加成功!'});
					THISPAGE.reload(vm_code);
				}else {
					Public.tips({type: 1, content : data.message});
				}
	    	  }
		 });
	},
	add_vip_tag:function(){
		if (!Business.verifyRight(_limitMenu,_menuParam.INSERT)) {
			return;
		}
		var vm_code = $("#vm_code").val();
		$.dialog({
			id:'add_vip_tag',
			title:'新增会员标签',
			max: false,
			min: false,
			lock:true,
			width:300,
			height:120,
			fixed:true,
			drag: true,
			resize:false,
			content:'url:'+config.BASEPATH+'vip/member/to_add_vip_tag?vm_code='+vm_code,
			close: function () {
				THISPAGE.reload(vm_code);
			}
		});
	}
}

var handle = {
	showTagDetailInfo:function(vt_name,vt_sysdate,vt_manager_name,vip_used_count){
		$("#tagDetailInfo").empty();
		$("#tagDetailInfo").append("<li>标签名："+vt_name+"</li>");
		$("#tagDetailInfo").append("<li>创建时间："+vt_sysdate+"</li>");
		$("#tagDetailInfo").append("<li>创建人："+vt_manager_name+"</li>");
		$("#tagDetailInfo").append("<li>已绑定<span style='color:red;font-size:18px;' id='usedCount' value='"+vip_used_count+"'>"+vip_used_count+"</span>个会员</li>");
	},
	hiddenTagDetailInfo:function(){
		$("#tagDetailInfo").empty();
	}
}

var THISPAGE = {
	init:function (){
		this.initDom();
	},
	initDom:function(){
		//查询会员标签。区分已选择，可选择
		//ajax查询，查询后对结果进行分析，加载到对应的内容框中
		var vm_code= $("#vm_code").val();
		this.reload(vm_code);
	},
	reload:function(vm_code){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"vip/member/query_vip_tag",
			data:"vm_code="+vm_code,
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					var jsonobj=eval(data.data);
	
					$("#tagSelectedInfo").empty();
				    $("#tagCanSelectInfo").empty();
					for(var i=0;i<jsonobj.length;i++){
						var vip_member_used=jsonobj[i].vip_member_used;
			    		var vt_name=jsonobj[i].vt_name;
		 	    		var vt_sysdate=jsonobj[i].vt_sysdate;
		 	    		var vt_manager_name=jsonobj[i].vt_manager_name;
		 	    		var vt_id=jsonobj[i].vt_id;
		 	    		var vt_code = jsonobj[i].vt_code;
		 	    		var vip_used_count=jsonobj[i].vip_used_count;
			    		if(eval(vip_member_used)>0){
			    			$("#tagSelectedInfo").append("<span id=\"tag"+vt_id+"\" name=\"member_type_used\"" +
			    					"onMouseOver=\"handle.showTagDetailInfo('"+vt_name+"','"+vt_sysdate  +"','"+vt_manager_name+"','"+vip_used_count+"');\" onMouseOut=\"handle.hiddenTagDetailInfo()\">" +
				    			 	""+vt_name+"" +
				    			 	"<em onClick=\"Utils.deleteTagDAO(\'"+vt_code+"\','vmt','"+vt_name+"')\">X</em></span>");
			    		}else{
			    			$("#tagCanSelectInfo").append("<span id=\"tag"+vt_id+"\" " +
				    			 	"onMouseOver=\"handle.showTagDetailInfo('"+vt_name+"','"+vt_sysdate  +"','"+vt_manager_name+"','"+vip_used_count+"');\"  onMouseOut=\"handle.hiddenTagDetailInfo()\">" +
				    			 	"<a href=\"javascript:;\"  onClick=\"Utils.setMemberTag('"+vm_code+"','"+vt_code+"','"+vt_name+"')\">"+vt_name+"</a>" +
				    			 	"<em onClick=\"Utils.deleteTagDAO(\'"+vt_code+"\','vt','"+vt_name+"')\">X</em></li>");
				    		}
				    		
		    		 }
					 $("#tagCanSelectInfo").append("<span style='color:#008800' onClick='Utils.add_vip_tag()' class='i-hand' ><i class='iconfont'>&#xe639;</i>新增标签 </span>");
				}else{
					Public.tips({type: 1, content : data.message});
				}
	    	  }
		 });
	}
}
THISPAGE.init();