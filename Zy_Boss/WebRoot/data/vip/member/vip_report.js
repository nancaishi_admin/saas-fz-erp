var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var handle = {
	formatWay:function(val, opt, row){//1:电话回访 2:短信回访 3:微信回访 4:推送优惠券
		if(val == "1"){
			return "电话";
		}else if(val == "2"){
			return "短信";
		}else if(val == "3"){
			return "微信";
		}else if(val == "4"){
			return "发优惠券";
		}else{
			return "";
		}
	},
	formatCome:function(val, opt, row){//1:不确定到店 2:确认到店 3:确认不到店
		if(val == "1"){
			return "意向";
		}else if(val == "2"){
			return "确定";
		}else if(val == "3"){
			return "不来";
		}else{
			return "";
		}
	},
	formatType:function(val, opt, row){//1:会员回访 2:生日回访 3:消费回访
		if(val == "1"){
			return "会员回访";
		}else if(val == "2"){
			return "生日回访";
		}else if(val == "3"){
			return "消费回访";
		}else{
			return "";
		}
	},
	formatImage:function(val, opt, row){
		if($.trim(val) == "" ){
			return "";
		}
		return '<img src="'+config.SERVERPATH+val+'" alt="" width="100" height="100" />';
	},
	formatState:function(val, opt, row){//1:会员回访 2:生日回访 3:消费回访
		if(val == "1"){
			return "已用";
		}else{
			return "未用";
		}
	},
	consumeVisit:function(vm_id, vm_cardcode){//消费回访
		/*if (!Business.verifyRight(_limitMenu,_menuParam.INSERT)) {
			return ;
		}*/
		$.dialog({ 
		   	title:'会员回访',
		   	//data:{vi_type:3},//1:会员回访 2:生日回访 3:消费回访
		   	max: false,
		   	min: false,
		   	width:900,height:450,
		   	fixed:false,
		   	drag: true,
		   	resize:false,
		   	content:'url:'+config.BASEPATH+'vip/member/vip_visit_analysis?vm_id='+vm_id+'&vi_type=1&vm_cardcode='+vm_cardcode,
		   	lock:true,
		   	close: function(){
		   		/*if(isRefresh){
		   			THISPAGE.reloadData();
		   			isRefresh = false;
		   		}*/
		   	}
	    });
	}
}
var THISPAGE = {
	init:function(){
		this.initVip();
		this.initDate("sell");
		this.initDate("time");
		this.initDate("try");
		this.initDate("back");
		this.initGrid();
		this.initStyle("chart");
		this.initEvent();
	},
	initStyle:function(type){
		$("#tab_sell_type").children("a").each(function(){
			$(this).removeClass("on");
		});
		$("#btn_"+type).addClass("on")
		$("#type").val(type);
		if('chart' == type){
			$(".sell_wrap").hide();
			$("#sell_container").show();
			THISPAGE.sellData();
		}else{
			$(".sell_wrap").show();
			$("#sell_container").hide();
		}
	},
	initVip:function(){
		var vm_total_money = $("#vm_total_money").text();
		var vm_times = $("#vm_times").text();
		var vm_avg_price = 0;
		if(vm_total_money != null && parseFloat(vm_total_money) > 0){
			if(null != vm_times && parseFloat(vm_times) > 0){
				vm_avg_price = parseFloat(vm_total_money)/parseFloat(vm_times)
			}else{
				vm_avg_price = vm_total_money;
			}
		}
		$("#vm_avg_price").text(parseFloat(vm_avg_price).toFixed(2));
	},
	initDate:function(type){
		var date = new Date();
		$("#"+type+"_enddate").val(DateToFullDateTimeString(date));
		date.setMonth(date.getMonth()-1);
		$("#"+type+"_begindate").val(DateToFullDateTimeString(date));
	},
	initData:function(type){
		var queryurl = config.BASEPATH+"vip/member/"+
		type+"List";
		queryurl += THISPAGE.initParam(type);
		THISPAGE.reloadData(type,queryurl);
	},
	initParam:function(type){
		var param = "?vm_code="+$("#vm_code").val();
		if(type == 'ecoupon'){
			param += "&ecu_state="+$("#ecu_state").val();
		}else{
			param += "&begindate="+$("#"+type+"_begindate").val();
			param += "&enddate="+$("#"+type+"_enddate").val();
		}
		return param;
	},
	initDom:function(){
		var type = $("#type").val();
		THISPAGE.initType(type);
	},
	initType:function(type){
		$(".tab_con").each(function(){
			$(this).hide();
		});
		$(".t-btn").each(function(){
			$(this).removeClass("on");
		});
		$("#tab_"+type).show();
		$("#btn_"+type).addClass("on")
		$("#type").val(type);
	},
	bulidData:function(data){
		var moneyArr = [];
		for(var j = 0;j < 24; j++){
			var number = 0;
			for (var i = 0; i < data.length; i++) {
				var grid = data[i];
				if(parseInt(grid.code) == j){
					if(null != grid.number && "" != grid.number){
						number = grid.number;
					}
				}
			}
			moneyArr.push(parseFloat(number));
		}
		return moneyArr;
	},
	initChart:function(data){
		var hours = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23];
		var pdata = THISPAGE.bulidData(data);
		var chart = new Highcharts.Chart('container', {
		    title: {
		        text: '门店流量分析',
		        x: -20
		    },
		    xAxis: {
		        categories: hours
		    },
		    yAxis: {
		        title: {
		            text: '消费金额'
		        }
		    },
		    plotOptions: {
	            line: {
	                dataLabels: {
	                    enabled: true          // 开启数据标签
	                },
	                enableMouseTracking: false // 关闭鼠标跟踪，对应的提示框、点击事件会失效
	            }
	        },
		    series: [{
		    	type: 'line',
		        name: '时间段',
		        data: pdata
		    }]
		});
	},
	initTime:function(){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'vip/member/timeList'+THISPAGE.initParam("time"),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					THISPAGE.initChart(data.data);
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
	},
	sellData:function(){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'vip/member/shopList'+THISPAGE.initParam("sell"),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					var pdata = {};
					var dateArr = [],numberArr = [];
					for (var i = 0; i < data.data.length; i++) {
						var grid = data.data[i];
						dateArr.push(grid.sh_sysdate);
						numberArr.push(parseFloat(grid.sh_money));
					}
					pdata.dates=dateArr;
					pdata.number=numberArr;
					THISPAGE.sellChart(pdata);
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
	},
	sellChart:function(pdata){
		$(".sell_wrap").hide();
		$("#sell_container").show();
		var chart = new Highcharts.Chart('sell_container', {
		    title: {
		        text: '消费记录',
		        x: -20
		    },
		    xAxis: {
		        categories: pdata.dates
		    },
		    yAxis: {
		        title: {
		            text: '金额'
		        },
		        plotLines: [{
		            value: 0,
		            width: 1,
		            color: '#808080'
		        }]
		    },
		    legend: {
		        layout: 'vertical',
		        align: 'right',
		        verticalAlign: 'middle',
		        borderWidth: 0
		    },
		    series: [{
		    	type: 'column',
		        name: '时间',
		        data: pdata.number
		    }]
		});
	},
	reloadData:function(type,_url){
		$("#grid_"+type).jqGrid('setGridParam',{datatype:"json",page:1,url:_url}).trigger("reloadGrid");
	},
	initGrid:function(){
		var colModel = [
            {label:'图片', name: 'pd_img_path', index: 'pd_img_path', width:100,formatter: handle.formatImage, align: 'left', title: false},
            {label:'时间', name: 'shl_date', index: 'shl_date', width:160, align: 'center', title: false},
            {label:'货号', name: 'pd_no', index: 'pd_no', width:100, align: 'left', title: false},
            {label:'名称', name: 'pd_name', index: 'pd_name', width:140, align: 'left', title: false},
            {label:'颜色', name: 'cr_name', index: 'cr_name', width:60, align: 'left', title: false},
            {label:'尺码', name: 'sz_name', index: 'sz_name', width:50, align: 'center', title: false},
            {label:'杯型', name: 'bs_name', index: 'bs_name', width:50, align: 'center', title: false},
            {label:'数量', name: 'shl_amount', index: 'shl_amount', width:50, align: 'right', title: false},
            {label:'金额', name: 'shl_money', index: 'shl_money', width:70, align: 'right', title: false}
	    ];
		$('#grid_sell').jqGrid({
			datatype: 'local',
			width:850,
			height:200,
			altRows:true,
			gridview: true,
			onselectrow: false,
			cellEdit: false,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page_sell',//分页
			recordtext:'{0} - {1} 共 {2} 条',  
			viewrecords: true,
			pgbuttons:true,
			rowNum:15,//每页条数
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			footerrow :true,
			userDataOnFooter : true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
                userdata: 'data.data',
				repeatitems : false,
				id:'id'
			},
            loadComplete: function (xhr) {
            }
	    });
		THISPAGE.initDom();
//		THISPAGE.initData("sell");
	},
	initTry:function(){
		var colModel = [
            {label:'时间', name: 'tr_sysdate', index: 'tr_sysdate', width:160, align: 'center', title: false},
            {label:'货号', name: 'pd_no', index: 'pd_no', width:100, align: 'left', title: false},
            {label:'名称', name: 'pd_name', index: 'pd_name', width:140, align: 'left', title: false},
            {label:'颜色', name: 'cr_name', index: 'cr_name', width:60, align: 'left', title: false},
            {label:'尺码', name: 'sz_name', index: 'sz_name', width:50, align: 'center', title: false},
            {label:'杯型', name: 'bs_name', index: 'bs_name', width:50, align: 'center', title: false},
            {label:'零售价', name: 'tr_sell_price', index: 'tr_sell_price', width:70, align: 'right', title: false}
	    ];
		$('#grid_try').jqGrid({
			datatype: 'local',
			width:850,
			height:260,
			altRows:true,
			gridview: true,
			onselectrow: false,
			cellEdit: false,
			colModel: colModel,
			rownumbers: true,//行号
			viewrecords: true,
			pgbuttons:true,
			rowNum:-1,//每页条数
			shrinkToFit:false,//表格是否自动填充
			footerrow :false,
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id:'tr_id'
			},
            loadComplete: function (xhr) {
            }
	    });
		THISPAGE.initDom();
	},
	initPush:function(){
		/*var colModel = [
            {label:'货号', name: 'pd_no', index: 'pd_no', width:100, align: 'left', title: false},
            {label:'名称', name: 'pd_name', index: 'pd_name', width:140, align: 'left', title: false},
            {label:'颜色', name: 'cr_name', index: 'cr_name', width:60, align: 'left', title: false},
            {label:'尺码', name: 'sz_name', index: 'sz_name', width:50, align: 'center', title: false},
            {label:'杯型', name: 'bs_name', index: 'bs_name', width:50, align: 'center', title: false},
            {label:'数量', name: 'shl_amount', index: 'shl_amount', width:50, align: 'right', title: false},
            {label:'金额', name: 'shl_money', index: 'shl_money', width:70, align: 'right', title: false}
	    ];
		$('#grid_push').jqGrid({
			datatype: 'local',
			width:850,
			height:260,
			altRows:true,
			gridview: true,
			onselectrow: false,
			cellEdit: false,
			colModel: colModel,
			rownumbers: true,//行号
			viewrecords: false,
			pgbuttons:false,
			rowNum:-1,//每页条数
			shrinkToFit:false,//表格是否自动填充
			footerrow :false,
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id:'id'
			},
            loadComplete: function (xhr) {
            }
	    });*/
		$.ajax({
	    	type:"POST",
	    	url:config.BASEPATH+'vip/member/pushList?vm_code='+$('#vm_code').val(),
	    	async: false,
	    	dataType:"json",
	    	success:function(data){
	    		if(undefined != data && data.stat == 200){
		    		 var innserHTML=" ";
		    		 var totalMoney=0;
		    		 if(data.data == ''){
		    			 innserHTML=innserHTML+"<span>没有发现推荐信息</span>";
		    			 $("#push_div").html(innserHTML);
		    			 return;
		    		 }
		    		 var jsonobj=eval(data.data);
		    		 for(var i=0;i<jsonobj.length;i++){ 
	    				 innserHTML = innserHTML + "<dl>";
	    				 if(jsonobj[i].pdm_img_path != ""){
	    					 innserHTML = innserHTML + "<img src='"+ config.BASEPATH + jsonobj[i].pdm_img_path + "' width='100' height='100' />";
	    				 }else{
	    					 innserHTML = innserHTML + "<img src='"+ config.BASEPATH + "resources/grid/images/nophoto.png' width='100' height='100' />";
	    				 }
		    			 innserHTML = innserHTML + "<span>货号：" + jsonobj[i].pd_no+"</span>" + "<span>名称：" + jsonobj[i].pd_name + "</span>" ;
		    			 innserHTML = innserHTML + "<span>价格：￥" + jsonobj[i].pd_sell_price+"</span>" + "<span>类型：" + jsonobj[i].pd_tp_name + "</span>";
		    			 innserHTML = innserHTML + "<span>品牌：" + jsonobj[i].pd_bd_name+"</span>" + "<span>库存：" + jsonobj[i].sd_amount + "</span>";
		    			 innserHTML = innserHTML + "</dl>";
	    			 };
		    		 $("#push_div").html(innserHTML);
	    		}else{
	    			Public.tips({type: 1, content : data.message});
	    		}
	    	  }
	    });
		THISPAGE.initDom();
	},
	initEcoupon:function(){
		var colModel = [
            {label:'状态', name: 'ecu_state', index: 'ecu_state', width:80, align: 'left',formatter: handle.formatState, title: false},
            {label:'领取时间', name: 'ecu_date', index: 'ecu_date', width:120, align: 'center', title: false},
            {label:'面值', name: 'ecu_money', index: 'ecu_money', width:80, align: 'left', title: false},
            {label:'使用条件', name: 'ecu_limitmoney', index: 'ecu_limitmoney', width:100, align: 'left', title: false},
            {label:'有效期', name: 'ecu_enddate', index: 'ecu_enddate', width:120, align: 'left', title: false},
            {label:'使用时间', name: 'ecu_use_date', index: 'ecu_use_date', width:120, align: 'center', title: false}
	    ];
		$('#grid_ecoupon').jqGrid({
			datatype: 'local',
			width:850,
			height:260,
			altRows:true,
			gridview: true,
			onselectrow: false,
			cellEdit: false,
			colModel: colModel,
			rownumbers: true,//行号
			viewrecords: true,
			pgbuttons:true,
			rowNum:-1,//每页条数
			shrinkToFit:false,//表格是否自动填充
			footerrow :false,
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id:'ecu_id'
			},
			loadComplete: function(data){
				if(undefined != data.data ){
					var _data = data.data;
					for(key in _data){
						var state = _data[key].ecu_state;
						if(state == 0){
							var flag = Public.checkDate(_data[key].ecu_enddate);//1-已过期,2-即将过期
							if(flag == 1){//已经过期
								$("#grid_ecoupon").jqGrid('setRowData',_data[key].ecu_id,false,{color:'#eb3f5f'});
							}
							if(flag == 2){//即将过期
								$("#grid_ecoupon").jqGrid('setRowData',_data[key].ecu_id,false,{color:'blue'});
							}
						}
					}
				}
			}
	    });
		THISPAGE.initDom();
	},
	initBack:function(){
		var colModel = [
            {label:'回访人', name: 'vi_manager', index: 'vi_manager', width:70, align: 'left', title: false},
            {label:'时间', name: 'vi_visit_date', index: 'vi_visit_date', width:100, align: 'center', title: false},
            {label:'类型', name: 'vi_type', index: 'vi_type', width:50, align: 'left',formatter: handle.formatType, title: false},
            {label:'方式', name: 'vi_way', index: 'vi_way', width:50, align: 'left',formatter: handle.formatWay, title: false},
            {label:'内容', name: 'vi_content', index: 'vi_content', width:160, align: 'left', title: false},
            {label:'到店', name: 'vi_is_arrive', index: 'vi_is_arrive', width:50,formatter: handle.formatCome, align: 'center', title: false},
            {label:'预定日期', name: 'vi_next_date', index: 'vi_next_date', width:100, align: 'center', title: false},
            {label:'备注', name: 'vi_remark', index: 'vi_remark', width:150, align: 'right', title: false}
	    ];
		$('#grid_back').jqGrid({
			datatype: 'local',
			width:850,
			height:260,
			altRows:true,
			gridview: true,
			onselectrow: false,
			cellEdit: false,
			colModel: colModel,
			rownumbers: true,//行号
			viewrecords: true,
			pgbuttons:true,
			rowNum:-1,//每页条数
			shrinkToFit:false,//表格是否自动填充
			footerrow :false,
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id:'vi_id'
			},
            loadComplete: function (xhr) {
            }
	    });
		THISPAGE.initDom();
	},
	initEvent:function(){
		$("#query_sell").on('click',function(e){
			THISPAGE.initData("sell");
		});
		$("#btn_sell").on('click',function(e){
			THISPAGE.initType("sell");
//			THISPAGE.initData("sell");
		});
		$("#btn_time").on('click',function(e){
			THISPAGE.initType("time");
			THISPAGE.initTime();
		});
		$("#btn_try").on('click',function(e){
			THISPAGE.initTry();
			THISPAGE.initType("try");
//			THISPAGE.initData("try");
		});
		$("#query_try").on('click',function(e){
			THISPAGE.initData("try");
		});
		$("#btn_push").on('click',function(e){
			THISPAGE.initPush();
			THISPAGE.initType("push");
//			THISPAGE.initData("push");
		});
		$("#query_push").on('click',function(e){
			THISPAGE.initData("push");
		});
		$("#btn_ecoupon").on('click',function(e){
			THISPAGE.initEcoupon();
			THISPAGE.initType("ecoupon");
//			THISPAGE.initData("ecoupon");
		});
		$("#query_ecoupon").on('click',function(e){
			THISPAGE.initData("ecoupon");
		});
		$("#btn_back").on('click',function(e){
			THISPAGE.initBack();
			THISPAGE.initType("back");
//			THISPAGE.initData("back");
		});
		$("#query_back").on('click',function(e){
			THISPAGE.initData("back");
		});
		$("#btn_chart").on('click',function(e){
			e.preventDefault();
			THISPAGE.initStyle("chart");
		});
		$("#btn_list").on('click',function(e){
			e.preventDefault();
			THISPAGE.initStyle("list");
		});
	}
}
THISPAGE.init();