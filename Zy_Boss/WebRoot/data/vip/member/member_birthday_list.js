var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
//var qtyPlaces =  CONFIG.QTYPLACES || 2, pricePlaces =  CONFIG.PRICEPLACES || 2, amountPlaces =  CONFIG.AMOUNTPLACES || 2;
var _limitMenu = system.MENULIMIT.VIP05;
var _menuParam = config.OPTIONLIMIT;
var searchFlag = false;
var queryurl = config.BASEPATH+'vip/member/birthday_list';

var Utils = {
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:true},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var sp_code = [];
				var sp_name = [];
				for(var i=0;i<selected.length;i++){
					sp_code.push(selected[i].sp_code);
					sp_name.push(selected[i].sp_name);
				}
				$("#vm_shop_code").val(sp_code.join(","));
				$("#sp_name").val(sp_name.join(","));
			},
			close:function(){
				if(commonDia.content.dblClick){
					var selected = commonDia.content.doSelect();
					var sp_code = [];
					var sp_name = [];
					for(var i=0;i<selected.length;i++){
						sp_code.push(selected[i].sp_code);
						sp_name.push(selected[i].sp_name);
					}
					$("#vm_shop_code").val(sp_code.join(","));
					$("#sp_name").val(sp_name.join(","));
				}
			},
			cancel:true
		});
	},
	doReport:function(id){
		if(null == id || "" == id){
			Public.tips({type: 2, content : "请选择会员!"});
			return;
		}
		$.dialog({
			title : '智能分析',
			content : 'url:'+config.BASEPATH+'vip/member/to_report?vm_id='+id,
			width : 888,
			height : 545,
			max : false,
			min : false,
			cache : false,
			lock: true
		});
	}
}

var handle = {
	removeButtonClass:function(){
		var todayButton = document.getElementById("todayButton");
		var nearButton = document.getElementById("nearButton");
		var monthButton = document.getElementById("monthButton");
		todayButton.className='t-btn btn-bl';
		nearButton.className='t-btn btn-blc';
		monthButton.className='t-btn btn-bc';
		$("#CurrentMode").val("");
	},
	//查询模块
	doChangeMode:function(obj){
		if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
			return;
		};
		var todayButton = document.getElementById("todayButton");
		var nearButton = document.getElementById("nearButton");
		var monthButton = document.getElementById("monthButton");
		if(obj == 0){//今日生日
			todayButton.className='t-btn btn-bl on';
			nearButton.className='t-btn btn-blc';
			monthButton.className='t-btn btn-bc';
			$("#CurrentMode").val(0);
		}
		else if(obj == 1){//临近生日
			todayButton.className='t-btn btn-bl';
			nearButton.className='t-btn btn-blc on';
			monthButton.className='t-btn btn-bc';
			$("#CurrentMode").val(1);
		}
		else if(obj == 2){//本月生日
			todayButton.className='t-btn btn-bl';
			nearButton.className='t-btn btn-blc';
			monthButton.className='t-btn btn-bc on';
			$("#CurrentMode").val(2);
		}
		
	    $("#begindate").val("");
	    $("#enddate").val("");
	    $("#searchDateType").val("");
	    $('label[id=label]').removeClass('checked');
		//刷新页面重新载入数据
		THISPAGE.reloadData();
	},
	operFmatter:function (val, opt, row) {
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += "<i class='iconfont i-hand' title='回访' onclick=\"handle.consumeVisit('"+row.vm_id+"','"+row.vm_cardcode+"')\" >&#xe69b;</i>";
		html_con += "<i class='iconfont i-hand' title='分析' onclick=\"Utils.doReport('"+row.vm_id+"')\" >&#xe63f;</i>";
		html_con += '</div>';
		return html_con;
	},
	formatBirthday:function(val, opt, row){//0公历生日1为农历生日
		if(row.vm_birthday_type == "0"){
			return val+"（公历）";
		}else if(row.vm_birthday_type == "1"){
			return val+"（农历）";
		}
		return '';
	},
	consumeVisit:function(vm_id, vm_cardcode){//消费回访
		$.dialog({ 
		   	title:'会员回访',
		   	//data:{vi_type:3},//1:会员回访 2:生日回访 3:消费回访
		   	max: false,
		   	min: false,
		   	width:900,height:450,
		   	fixed:false,
		   	drag: true,
		   	resize:false,
		   	content:'url:'+config.BASEPATH+'vip/member/vip_visit_analysis?vm_id='+vm_id+'&vi_type=2&vm_cardcode='+vm_cardcode,
		   	lock:true
		   /*	close: function(){
		   		if(isRefresh){
		   			THISPAGE.reloadData();
		   		}
		   	}*/
	    });
	}
};
var format = {
	money: function(val, opt, row){
		var val = Public.numToCurrency(val);
		return val || '&#160;';
	},
	quantity: function(val, opt, row){
		return val || '&#160;';
	}
};

function optionSelected(name){
	var select=document.getElementById(name);
	if(select.value>0){
		return select.value;
	}else{
		return "";
	}
}
/*function formatState(value){
	var text = '';
	if (value=='0'){
		text='正常';
	}else if(value=='1'){
		text='无效';
	}else if(value=='2'){
		text='挂失';
	}
	return text;
}*/

 
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();
	},
	initDom:function(){
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
		    {label:'操作',name: 'btn',index:'btn',width: 60, formatter: handle.operFmatter,fixed:true,title: false,align:'center',sortable:false},
		    {label:'编号',name: 'vm_code',index: 'vm_code',width:0,hidden:true},
	    	{label:'会员卡号',name: 'vm_cardcode',index:'vm_cardcode',width: 100, title: false,fixed:true},
	    	{label:'会员姓名',name: 'vm_name',index:'vm_name',width: 100, title: false},
	    	{label:'手机号码',name: 'vm_mobile',index:'vm_mobile',width: 100,align:'center'},
	    	{label:'会员类别',name: 'mt_name',index:'mt_name',width: 80, title: false},
	    	{label:'会员等级',name: 'gd_name', index: 'gd_name', width:60, title: false,align:'center'},
	    	{label:'会员生日',name: 'vm_birthday',index:'vm_birthday',width: 120, title: false, align:'center',formatter:handle.formatBirthday},
	    	{label:'会员生日类别',name: 'vm_birthday_type',index:'vm_birthday_type',hidden:true},
	    	{label:'N天未消费',name: 'vm_notbuy_day',index:'vm_lastbuy_date',width: 80, title: false, align:'center'},
	    	{label:'周期消费',name: 'sh_sell_money',index:'sh_sell_money',width: 100, title: false, align:'right'},
	    	{label:'消费金额',name: 'vm_total_money',index:'vm_total_money',width: 100, align:'right'},
	    	{label:'消费次数',name: 'vm_times',index:'vm_times',width: 100, title: false, align:'right'},
	    	{label:'总计积分',name: 'vm_points',index:'vm_points',width: 100, align:'right'},
	    	{label:'剩余积分',name: 'vm_last_points',index:'vm_last_points',width: 100, align:'right'},
	    	{label:'办卡门店',name: 'shop_name',index:'shop_name',width: 120},
	    	{label:'办卡人',name: 'vm_manager',index:'vm_manager',width: 80},
	    	{label:'办卡日期',name: 'vm_date',index:'vm_date',width: 100,align:'center'}
	    ];
		$('#grid').jqGrid({
			url:queryurl+"?"+ THISPAGE.buildParams(),
			//loadonce:true,
			datatype: 'json',
			width: gridWH.w,
			height: gridWH.h-35,
			altRows:true,
			gridview: true,
			onselectrow: false,
			cellEdit: false,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			multiselect:false,//多选
			viewrecords: true,
			cellsubmit: 'clientArray',//可编辑列在编辑完成后会触发保存事件，clientArray表示只保存到表格不提交到服务器
			pgbuttons:true,
			rowNum:config.DATAROWNUM,//每页条数100
			rowList:config.DATAROWLIST,//分页条数
			shrinkToFit:false,//表格是否自动填充
			//scroll: 1,//是否滚动
			autoScroll: true,
			footerrow :true,
			userDataOnFooter : true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
                userdata: 'data.data',
				repeatitems : false,
				id: 'vm_id'  //图标ID
			},
			loadComplete: function(data){
				Public.resizeSpecifyGrid("grid", 110, 32);
			},
			loadError: function(xhr, status, error){
				Public.tips({type: 1, content: '操作失败了哦，服务可能已过期，请按F5刷新！'});
			},
			onPaging: function(pgButton) {
			}
	    });
	},
	reloadData:function(){
		var params=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+"?"+params}).trigger("reloadGrid");
	},
	buildParams : function(){
		var params = "CurrentMode="+$("#CurrentMode").val();
		params +="&vm_shop_code="+$("#vm_shop_code").val();
		params +="&vm_cardcode="+$("#vm_cardcode").val();
		params +="&vm_name="+ Public.encodeURI($("#vm_name").val());
		params +="&vm_mobile="+$("#vm_mobile").val();
		params +="&begindate="+$("#begindate").val();
		params +="&enddate="+$("#enddate").val();
		params +="&begin_notbuy_day="+$("#begin_notbuy_day").val();
		params +="&end_notbuy_day="+$("#end_notbuy_day").val();
		params +="&isHadBirthday="+$("#searchDateType").val();
		params +="&consumeDay="+optionSelected("consumeDay");
		params +="&agowarnDay="+optionSelected("agowarnDay");
		params +="&vipUpdatePower="+system.MENULIMIT.VIP05;
		return params;
	},
	reset:function(){
		 $("#sp_name").val("");
		 $("#vm_shop_code").val("");
	 	 $("#vm_cardcode").val("");
	 	 $("#vm_name").val("");
	 	 $("#vm_mobile").val("");
	 	 $("#begindate").val("");
	 	 $("#enddate").val("");
	 	 $("#begin_notbuy_day").val("");
	 	 $("#end_notbuy_day").val("");
	 	 $("#searchDateType").val("");
	},
	initEvent:function(){
		// 查询
		$('#btn_Search').on('click', function(e){
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return;
			};
			if(checkBirthday()){
				Public.tips({type: 2, content : '生日起始日期不能为空! '});
				return;
			}
			THISPAGE.reloadData();
		});
		// 查询
		$('#btn_ok').on('click', function(e){
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			if(checkBirthday()){
				Public.tips({type: 2, content : '生日起始日期不能为空! '});
				return;
			}
			THISPAGE.reloadData();
		});
		$("#btn_shop").click(function(){
			Utils.doQueryShop();
		});
		// 重置
        $('#btn_reset').on('click', function(e) {
            e.preventDefault();
            THISPAGE.reset();
        });
	}
}

function checkBirthday(){
	var endTime = $("#enddate").val();
	var beginTime = $("#begindate").val();
	var type = $("#searchDateType").val();
	if(type!=""&&(beginTime=="" || endTime=="")){
		return true;
	}else{
		return false;
	}
}
/**具体日期 输入校验**/
/*function valSearchDay(){
	var searchStartDay = $("#startDay").val();
	var searchEndDay = $("#endDay").val();
	if( document.getElementById("searchMore").checked )
	{
		if( $.trim(searchStartDay)=='' ){
			$.dialog.tips("日期不能为空，请输入数字！ ",2,"32X32/fail.png");
			$("#startDay").focus();
			return false;
		}
		if( isNaN(searchStartDay)){
			$.dialog.tips("日期必须是数字，请重新输入！ ",2,"32X32/fail.png");
			$("#startDay").select();
			return false;
		}
		if(searchStartDay<1 || searchStartDay>31){
			$.dialog.tips("日期为1-31的数字，请重新输入正确的日期！ ",2,"32X32/fail.png");
			$("#startDay").select();
			return false;
		}
		if( $.trim(searchEndDay)=='' ){
			$.dialog.tips("日期不能为空，请输入数字！",2,"32X32/fail.png");
			$("#endDay").focus();
			return false;
		}
		if( isNaN(searchEndDay)){
			$.dialog.tips("日期必须是数字，请重新输入！ ",2,"32X32/fail.png");
			$("#endDay").select();
			return false;
		}
		if( searchEndDay<1 || searchEndDay>31 ){
			$.dialog.tips("日期为1-31的数字，请重新输入正确的日期！ ",2,"32X32/fail.png");
			$("#endDay").select();
			return false;
		}
		if(searchStartDay > searchEndDay){
			$.dialog.tips("开始日期不能大于结束日期，请重新输入！ ",2,"32X32/fail.png");
			return false;
		}
	}
	
	return true;
}*/

THISPAGE.init();