var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var api = frameElement.api;
var multiselect = false;
if(undefined != api.data.multiselect){
	multiselect = api.data.multiselect;
}
var _height = api.config.height-132,_width = api.config.width-34;//获取弹出框弹出宽、高
var queryurl = config.BASEPATH+'base/emp/list';
var dblClick = false;
var THISPAGE = {
	init:function (){
		this.initGrid();
		this.initEvent();	
	},
	initGrid:function(){
		var colModel = [
			{name: 'em_code', label:'员工编号',index: 'em_code', width: 70, title: false},
	    	{name: 'em_name', label:'姓名',index: 'em_name', width: 80, title: false},
	    	{name: 'em_shop_code',index: 'em_shop_code',fixed:true,hidden:true }
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:multiselect,//多选
			viewrecords: true,
			rowNum:30,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'em_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow: function (rowid, iRow, iCol, e) {
				if(!multiselect){
					dblClick = true;
					api.close();
				}
            }
	    });
	},
	reloadData:function(data){
		var param="";
		if(data != null && "" != data){
			param += "?1=1";
			if(data.hasOwnProperty("searchContent") && data.searchContent != ''){
				param += "&searchContent="+Public.encodeURI(data.searchContent);
			}
		}
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		$('#SearchContent').on('keyup',function(event){
			if (event.keyCode==13){
				$('#btn-search').click();
			}
		});
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			var searchContent = $.trim($('#SearchContent').val());
			var pdata = {};
			pdata.searchContent = searchContent;
			THISPAGE.reloadData(pdata);
		});
	}
};
function doSelect(){
	if(multiselect){//多选
		/*var ids = $("#grid").jqGrid('getGridParam', 'selarrrow');
		if(ids == null || ids.length == 0){
			Public.tips({type: 2, content : "请选择店铺！"});
			return false;
		}
		var result = [];
		for (var i = 0; i < ids.length; i++) {
			result.push($("#grid").jqGrid("getRowData", ids[i]));
		}
		return result;*/
	}else{//单选
		/*var selectedId = $("#grid").jqGrid('getGridParam', 'selrow');
		if(selectedId == null || selectedId == ""){
			Public.tips({type: 2, content : "请选择店铺！"});
			return false;
		}
		var rowData = $("#grid").jqGrid("getRowData", selectedId);
		return rowData;*/
	}
	
	var rowIds=api.data.rowIds;;//会员ID
	var parentId=api.data.parentId;
	var callback = api.data.callback;
	var parentTmp=parent;
	if ( parentId!=null ){//三层弹出框
		parentTmp=oper.$.dialog.list[parentId].content;
	}
	
	var selectedId = $('#grid').jqGrid('getGridParam','selarrrow');
	if (selectedId!=null && selectedId!='' ){
		var codes="";
		if( selectedId instanceof Array ){
			for(var i=0; i<selectedId.length; i++){
				var rowData = $("#grid").jqGrid("getRowData", selectedId[i]);		
				codes+=rowData.em_code+":"+rowData.em_name+":"+rowData.em_shop_code+ ",";
			}
		}else{
			var rowData = $("#grid").jqGrid("getRowData", selectedId);		
			codes+=rowData.em_code+":"+rowData.em_name+":"+rowData.em_shop_code+ ",";
		}
		codes = codes.substring(0, codes.length-1);
		$.ajax({
	    	async:false,
			type:"POST",
			url:config.BASEPATH+"vip/member/update_assign_emp",
			data:"rowIds="+rowIds+"&codes="+Public.encodeURI(codes),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : '分配成功！'});
					setTimeout("api.close()",500);
					callback();
				}else{
					Public.tips({type: 1, content : data.message});
					setTimeout("api.close()",500);
				}
			}
		});
	}else{
		Public.tips({type: 2, content : "请选择办卡人员！"});
	}
	return true;
}
THISPAGE.init();