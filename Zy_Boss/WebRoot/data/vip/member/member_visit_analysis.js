var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var api = frameElement.api, W = api.opener;
var isRefresh = false;

var Utils = {
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择回访人员',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#vi_manager_code").val(selected.em_code);
					$("#vi_manager").val(selected.em_name);
					$("#vi_shop_code").val(selected.em_shop_code);
				}
			},
			cancel:true
		});
	}
};
var handle = {
	initSatisfaction:function(){
		satisfactionInfo = {
			data:{items:[
					{satisfaction_code:'1',satisfaction_name:'满意'},
					{satisfaction_code:'2',satisfaction_name:'一般'},
					{satisfaction_code:'3',satisfaction_name:'不满意'}
			]}
		}
		satisfactionCombo = $('#span_satisfaction').combo({
			data:satisfactionInfo.data.items,
			value: 'satisfaction_code',
			text: 'satisfaction_name',
			width : 159,
			height: 300,
			listId:'',
			defaultSelected: 0,
			editable: true,
			callback:{
				onChange: function(data){
					$("#vi_satisfaction").val(data.satisfaction_code);
				}
			}
		}).getCombo();
	},
	initIsArrive:function(){
		isArriveInfo = {
			data:{items:[
					{is_arrive_code:'1',is_arrive_name:'不确定到店'},
					{is_arrive_code:'2',is_arrive_name:'确认到店'},
					{is_arrive_code:'3',is_arrive_name:'确认不到店'}
			]}
		}
		isArriveCombo = $('#span_is_arrive').combo({
			data:isArriveInfo.data.items,
			value: 'is_arrive_code',
			text: 'is_arrive_name',
			width : 159,
			height: 300,
			listId:'',
			defaultSelected: 0,
			editable: true,
			callback:{
				onChange: function(data){
					$("#vi_is_arrive").val(data.is_arrive_code);
					handle.changType(data.is_arrive_code);
				}
			}
		}).getCombo();
	},
	changType:function(type){
		if(type==1){
			$("#divnext").show();
			$("#divarrive").hide();
			$("#divno").hide();
		}
		else if(type==2){
			$("#divnext").hide();
			$("#divarrive").show();
			$("#divno").hide();
		}
		else if(type==3){
			$("#divnext").hide();
			$("#divarrive").hide();
			$("#divno").show();
		}
	},
	setValue:function(){
		var vi_is_arrive = $("#vi_is_arrive").val(); 
		var vi_remark = "", vi_date="";
		if(vi_is_arrive=='1'){
			vi_date = $("#nextTime").val();
			vi_remark = $("#txtplan").val();
		}else if(vi_is_arrive=='2'){
			vi_date = $("#arrivedate").val();
			vi_remark = $("#precautions").val();
		}else if(vi_is_arrive=='3'){
			vi_remark = $("#txtother").val();
		}
		$("#vi_date").val(vi_date);
		$("#vi_remark").val(vi_remark);
	},
	getFocus:function(id){
		document.getElementById(id).value="";
		document.getElementById(id).focus();
	},
	doSave:function(){
		//隐藏字段赋值
		handle.setValue();
		if ($("#vi_manager").val()==""){
			Public.tips({type: 2, content : '请选择回访人员'});
			return false;
		}
		if ($("#vi_manager_code").val()==""){
			Public.tips({type: 2, content : '请选择回访人员'});
			return false;
		}
		if ($("#vi_content").val()==""){
			handle.getFocus("vi_content");
			Public.tips({type: 2, content : '请输入回访内容'});
			return false;
		}
		if ($("#vi_content").val().length>200){
			document.getElementById("vi_content").focus();
			Public.tips({type: 2, content : '回访内容过多请重新输入'});
			return false;
		}
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"vip/member/save_visit",
			data:encodeURI(encodeURI($('#form1').serialize())),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : "数据保存成功!!!"});
					W.isRefresh = true;
					setTimeout("api.close()",1000);
				}else{
					Public.tips({type: 1, content : "数据保存失败,请重试，如有疑问，请联系管理员!!!"});
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		handle.initSatisfaction();
		handle.initIsArrive();
	},
	initGrid:function(){
		
	},
	initEvent:function(){
		$("#btn_emp").click(function(){
			Utils.doQueryEmp();
		});
		$("#btn_save").click(function(){
			handle.doSave();
		});
	}
}
THISPAGE.init();