var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.VIP15;
var _menuParam = config.OPTIONLIMIT;
var isRefresh = false;
var queryurl = config.BASEPATH+'vip/report/noshopvip_analysis_list';

var Utils = {
	doQueryMemberType : function(){
		commonDia = $.dialog({
			title : '会员类别',
			content : 'url:'+config.BASEPATH+'vip/membertype/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#vm_mt_code").val(selected.mt_code);
					$("#mt_name").val(selected.mt_name);
				}
			},
			cancel:true
		});
	},
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#vm_shop_code").val(selected.sp_code);
					$("#sp_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#vm_manager_code").val(selected.em_code);
					$("#vm_manager").val(selected.em_name);
				}
			},
			cancel:true
		});
	}
};
var handle = {
	operFmatter:function(val, opt, row) {
		var html_con = "<div class='operating' data-id='" + row.id + "'>";
		if(row.id != ""){
			html_con += "<i class='iconfont i-hand' title='回访' onclick=\"handle.consumeVisit('"+row.id+"','"+row.vm_cardcode+"')\" >&#xe69b;</i>";
		}else{
			html_con += "<span class='ui-icon' style='background:none;'></span><span class='ui-icon' style='background:none;'></span>";
		}
		html_con += "</div>";
		return html_con;
	},
	timesRateFmatter:function(val,opt,row){
		var _val = (100*(row.buy_count/row.vm_times)).toFixed(2) + '%';
		return _val;
	},
	totalMoneyRateFmatter:function(val,opt,row){
		var _val = (100*(row.buy_money/row.vm_total_money)).toFixed(2) + '%';
		return _val;
	},
	consumeVisit:function(vm_id, vm_cardcode){//消费回访
		/*if (!Business.verifyRight(_limitMenu,_menuParam.INSERT)) {
			return ;
		}*/
		$.dialog({ 
		   	title:'会员回访',
		   	//data:{vi_type:3},//1:会员回访 2:生日回访 3:消费回访
		   	max: false,
		   	min: false,
		   	width:900,height:450,
		   	fixed:false,
		   	drag: true,
		   	resize:false,
		   	content:'url:'+config.BASEPATH+'vip/member/vip_visit_analysis?vm_id='+vm_id+'&vi_type=1&vm_cardcode='+vm_cardcode,
		   	lock:true,
		   	close: function(){
		   		if(isRefresh){
		   			THISPAGE.reloadData();
		   			isRefresh = false;
		   		}
		   	}
	    });
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		dateRedioClick("theMonth");
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
		    {label:'操作', name: 'operate', index: 'operate', width:60, fixed:true, formatter: handle.operFmatter, title: false, sortable:false,align:'center'},
		    {label:'卡号', name: 'vm_cardcode', index: 'vm_cardcode', width:110, title: false},       	
		    {label:'姓名',name: 'vm_name', index: 'vm_name', width:70, title: false},
		    {label:'类别',name: 'mt_name', index: 'mt_name', width:80, title: false, align:'center'},
	    	{label:'性别',name: 'vm_sex', index: 'vm_sex', width:50, title: false, align:'center'},
	    	{label:'手机号码',name: 'vm_mobile', index: 'vm_mobile', width: 100, title: false, align:'center'},
	    	{label:'手机号码隐藏',name: 'vm_mobile_hidden', index: 'vm_mobile', width: 100, title: false, align:'center',hidden:true},
	    	{label:'消费次数',name: 'vm_times', index: 'vm_times', width:80, title: true,align:'right',sorttype:'number'},
	    	{label:'累计消费额',name: 'vm_total_money', index: 'vm_total_money', width: 100, title: false,align:'right',sorttype:'float'},
	    	{label:'总计积分',name: 'vm_total_point', index: 'vm_total_point', width:100, title: false,align:'right',sorttype:'float'},
	    	{label:'剩余积分',name: 'vm_points', index: 'vm_points', width:100, title: false,align:'right',sorttype:'float'},
	    	{label:'购物时间',name: 'vm_lastbuy_date', index: 'vm_lastbuy_date', width:100, title: false,align:'center'},
	    	/*{label:'导购员',name: 'TGL_EMP_Name', index: 'MI_LastSeller', width:100, title: false},*/
	    	{label:'消费金额',name: 'vm_lastbuy_money', index: 'vm_lastbuy_money', width:100, title: false,align:'right'},
	    	{label:'办卡时间',name: 'vm_date', index: 'vm_date', width:100, title: false,align:'center'},
	    	{label:'办卡人员',name: 'vm_manager', index: 'vm_manager', width:80, title: false},
	    	{label:'办卡店铺',name: 'sp_name', index: 'sp_name', width:140, title: true}
	    ];
		$('#grid').jqGrid({
			url:queryurl,
			datatype: 'local',
			width: gridWH.w-12,
			height: gridWH.h-70,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager:'#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.DATAROWNUM,//每页条数
			rowList:config.DATAROWLIST,//分页条数
			autoScroll: true,
			footerrow :true,
			userDataOnFooter : true,
			shrinkToFit:false,//表格是否自动填充
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
                userdata: 'data.data',
				repeatitems : false,
				id: 'id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
				handle.operate('',rowid);
			},
			onPaging: function(pgButton) {
			}
	    });
		$("#grid").jqGrid('setGroupHeaders', { 
			useColSpanStyle : true, // 没有表头的列是否与表头列位置的空单元格合并 
			groupHeaders : [{
				startColumnName : 'vm_lastbuy_date', // 对应colModel中的name 
				numberOfColumns : 2, // 跨越的列数 
				titleText : '最后一次购物' 
			}] 
		});
	},
	buildParams : function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&vm_shop_code='+$("#vm_shop_code").val();
		params += '&vm_mt_code='+$("#vm_mt_code").val();
		params += '&vm_manager_code='+$("#vm_manager_code").val();
		return params;
	},
	reset:function(){
		$("#mt_name").val("");
		$("#vm_mt_code").val("");
		$("#sp_name").val("");
		$("#vm_shop_code").val("");
		$("#vm_manager").val("");
		$("#vm_manager_code").val("");
		THISPAGE.$_date.setValue(3);
		dateRedioClick("theMonth");
	},
	reloadData:function(data){
		var params=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+"?"+params}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn_search').on('click', function(e){
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			};
			THISPAGE.reloadData();
		});
			//查询
		$('#btn_ok').on('click', function(e){
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			};
			THISPAGE.reloadData();
		});
		//重置
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
		$("#btn_membertype").click(function(){
			Utils.doQueryMemberType();
		});
		$("#btn_shop").click(function(){
			Utils.doQueryShop();
		});
		$("#btn_emp").click(function(){
			Utils.doQueryEmp();
		});
		$(window).resize(function(){
			Public.resizeSpecifyGrid("grid",111,32);
		});
	}
}

THISPAGE.init();