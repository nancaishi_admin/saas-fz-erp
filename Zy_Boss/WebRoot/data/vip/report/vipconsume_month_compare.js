var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.VIP19;
var _menuParam = config.OPTIONLIMIT;
var isRefresh = false;
var queryurl = config.BASEPATH + 'vip/report/vipconsume_month_compare';//会员消费同比List
var querydetailurl = config.BASEPATH + 'vip/report/vipconsume_month_compare_column';////会员消费同比柱形图

var Utils = {
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#vm_shop_code").val(selected.sp_code);
				$("#shop_name").val(selected.sp_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#vm_shop_code").val(selected.sp_code);
					$("#shop_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initColumn();
		this.initEvent();	
	},
	initDom:function(){
		this.initYear();
	},
    initYear: function () {
        var date = new Date();
        var currentYear = date.getFullYear();
        var yearInfo = {
            data: {items: [
                {year_code: "" + parseInt(currentYear - 5) + "", year_name: "" + parseInt(currentYear - 5) + ""},
                {year_code: "" + parseInt(currentYear - 4) + "", year_name: "" + parseInt(currentYear - 4) + ""},
                {year_code: "" + parseInt(currentYear - 3) + "", year_name: "" + parseInt(currentYear - 3) + ""},
                {year_code: "" + parseInt(currentYear - 2) + "", year_name: "" + parseInt(currentYear - 2) + ""},
                {year_code: "" + parseInt(currentYear - 1) + "", year_name: "" + parseInt(currentYear - 1) + ""},
                {year_code: "" + parseInt(currentYear) + "", year_name: "" + parseInt(currentYear) + ""},
                {year_code: "" + parseInt(currentYear + 1) + "", year_name: "" + parseInt(currentYear + 1) + ""},
                {year_code: "" + parseInt(currentYear + 2) + "", year_name: "" + parseInt(currentYear + 2) + ""},
                {year_code: "" + parseInt(currentYear + 3) + "", year_name: "" + parseInt(currentYear + 3) + ""},
                {year_code: "" + parseInt(currentYear + 4) + "", year_name: "" + parseInt(currentYear + 4) + ""},
                {year_code: "" + parseInt(currentYear + 5) + "", year_name: "" + parseInt(currentYear + 5) + ""}
            ]}
        }
        yearCombo = $('#span_vm_year').combo({
            data: yearInfo.data.items,
            value: 'year_code',
            text: 'year_name',
            width: 60,
            height: 80,
            listId: '',
            defaultSelected: 5,
            editable: true,
            callback: {
                onChange: function (data) {
                    if (data.year_code) {
                        $("#vm_year").val(data.year_code);
                    }
                }
            }
        }).getCombo();
    },
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
	    	{label:'月份', name: 'vm_month', index: 'vm_month', width: 80, title: false,fixed:true,align:'center',cellattr: function(rowId, tv, rawObject, cm, rdata) {return 'id=\'vm_month' + rowId + "\'";}},
	    	{label:'项目',name: 'vm_item', index: 'vm_item', width: 100, title: true},
	    	{label:'消费金额',name: 'vm_money', index: 'vm_money', width: 120, title: false,align:'right'},
	    	{label:'占比',name: 'vm_rate', index: 'vm_rate', width:70, title: false,align:'right'}
	    ];
		$('#grid').jqGrid({
			url:queryurl+"?"+THISPAGE.buildParams(),
			//loadonce:true, 
			datatype: 'json',
			autowidth: true,
			width:gridWH.w,
			height: gridWH.h - 40,
            gridview: true,
            colModel: colModel,
            rownumbers: true,//行号
            pager: '#page',//分页
            viewrecords: false,
            pgbuttons: false,
            pgtext: false,
            rowNum: 1000, //每页显示记录数
            shrinkToFit: true,
            scroll: 1,//是否滚动
            footerrow: true,
            userDataOnFooter: true,
			jsonReader: {
				root: 'data.list',
				userdata: 'data.data',
				repeatitems : false,
				id:'id'
			},
			loadComplete: function(data){
				THISPAGE.Merger('vm_month');
				Public.resizeSpecifyGrid("grid", 140, $(window).width()-500);
			},
			loadError: function(xhr, status, error){
				Public.tips({type: 1, content: '操作失败了哦，服务可能已过期，请按F5刷新！'});
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
			},
			onPaging: function(pgButton) {
			}
	    });
	},
	//柱形图
	initColumn:function(){
		$.ajax({
			type:"POST",
			url:querydetailurl+"?"+THISPAGE.buildParams(),
			cache:false,
			dateType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					eval("$('#container').highcharts({"+data.data+"})");
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
	},
	reloadData:function(data){
		var params=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+"?"+params}).trigger("reloadGrid");
	},
	buildParams:function(){
		var params = '';
		params += '&vm_shop_code='+$("#vm_shop_code").val();
		params += '&vm_year='+$("#vm_year").val();
		return params;
	},
	reset:function(){
		$("#shop_name").val("");
		$("#vm_shop_code").val("");
		THISPAGE.initYear();
	},
	initEvent:function(){
		//查询
		$('#btn_search').on('click', function(e){
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			};
			THISPAGE.reloadData();
			THISPAGE.initColumn();
		});
		//重置
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	},
	Merger:function (CellName) {
        //得到显示到界面的id集合
        var mya = $("#grid").getDataIDs();
        var length = mya.length;
        for (var i = 0; i < length; i++) {
            var before = $("#grid").jqGrid('getRowData', mya[i]);
            //定义合并行数
            var rowSpanTaxCount = 1;
            for (var j = i + 1; j <= length; j++) {
                //和上边的信息对比 如果值一样就合并行数+1 然后设置rowspan 让当前单元格隐藏
                var end = $("#grid").jqGrid('getRowData', mya[j]);
                if (before[CellName] == end[CellName]) {
                    rowSpanTaxCount++;
                    $("#grid").setCell(mya[j], CellName, '', { display: 'none' });
                } else {
                    rowSpanTaxCount = 1;
                    break;
                }
                $("#" + CellName + "" + mya[i] + "").attr("rowspan", rowSpanTaxCount);
            }
        }
    }
}
THISPAGE.init();