var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.VIP14;
var _menuParam = config.OPTIONLIMIT;
var isRefresh = false;
var queryurl = config.BASEPATH+'vip/report/activevip_analysis_list';

var Utils = {
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#vm_shop_code").val(selected.sp_code);
				$("#shop_name").val(selected.sp_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#vm_shop_code").val(selected.sp_code);
					$("#shop_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	}
};
var handle = {
	operFmatter:function(val, opt, row) {
		var html_con = "<div class='operating' data-id='" + row.id + "'>";
		if(row.id != ""){
			html_con += "<i class='iconfont i-hand' title='回访' onclick=\"handle.consumeVisit('"+row.id+"','"+row.vm_cardcode+"')\" >&#xe69b;</i>";
		}else{
			html_con += "<span class='ui-icon' style='background:none;'></span><span class='ui-icon' style='background:none;'></span>";
		}
		html_con += "</div>";
		return html_con;
	},
	timesRateFmatter:function(val,opt,row){
		var _val = (100*(row.buy_count/row.vm_times)).toFixed(2) + '%';
		return _val;
	},
	totalMoneyRateFmatter:function(val,opt,row){
		var _val = (100*(row.buy_money/row.vm_total_money)).toFixed(2) + '%';
		return _val;
	},
	consumeVisit:function(vm_id, vm_cardcode){//消费回访
		/*if (!Business.verifyRight(_limitMenu,_menuParam.INSERT)) {
			return ;
		}*/
		$.dialog({ 
		   	title:'会员回访',
		   	//data:{vi_type:3},//1:会员回访 2:生日回访 3:消费回访
		   	max: false,
		   	min: false,
		   	width:900,height:450,
		   	fixed:false,
		   	drag: true,
		   	resize:false,
		   	content:'url:'+config.BASEPATH+'vip/member/vip_visit_analysis?vm_id='+vm_id+'&vi_type=1&vm_cardcode='+vm_cardcode,
		   	lock:true,
		   	close: function(){
		   		if(isRefresh){
		   			THISPAGE.reloadData();
		   			isRefresh = false;
		   		}
		   	}
	    });
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		dateRedioClick("theMonth");
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
		    {label:'操作', name: 'operate', index: 'operate', width:60, fixed:true, formatter: handle.operFmatter, title: false, sortable:false,align:'center'},
		    {label:'会员卡号', name: 'vm_cardcode', index: 'vm_cardcode', width:110, title: false},
		    {label:'会员姓名',name: 'vm_name', index: 'vm_name', width:70, title: false},
		    {label:'会员类别',name: 'mt_name', index: 'mt_name', width:80, title: false,align:'center'},
		    {label:'会员等级',name: 'gd_name', index: 'gd_name', width:60, title: false,align:'center'},
	    	{label:'性别',name: 'vm_sex', index: 'vm_sex', width:40, title: false,align:'center'},
	    	{label:'手机号码',name: 'vm_mobile', index: 'vm_mobile', width: 100, title: false,align:'center'},
	    	{name: 'vm_mobile_hidden', index: 'vm_mobile', width: 100, title: false,align:'center',hidden:true},
	    	{label:'周期购物数',name: 'buy_count', index: 'buy_count', width:80, title: true,align:'right'},
	    	{label:'总购物次数',name: 'vm_times', index: 'vm_times', width:80, title: true,align:'right'},
	    	{label:'周期次数占比',name: 'vm_times_rate', index: 'vm_times_rate', fixed:true, width:100, title: false,align:'right',formatter:handle.timesRateFmatter},
	    	{label:'周期消费额',name: 'buy_money', index: 'buy_money', width:80, title: false,align:'right'},
	    	{label:'总消费金额',name: 'vm_total_money', index: 'vm_total_money', width:100, title: false,align:'right'},
	    	{label:'周期消费占比',name: 'vm_total_money_rate', index: 'vm_total_money_rate', fixed:true, width:100, title: false,align:'right',formatter:handle.totalMoneyRateFmatter},
	    	{label:'总计积分',name: 'vm_total_point', index: 'vm_total_point', width:100, title: false,align:'right'},
	    	{label:'剩余积分',name: 'vm_points', index: 'vm_points', width:100, title: false,align:'right'}
	    ];
		$('#grid').jqGrid({
			url:queryurl,
			datatype: 'local',
			width: gridWH.w-12,
			height: gridWH.h-46,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager:'#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.DATAROWNUM,//每页条数
			rowList:config.DATAROWLIST,//分页条数
			autoScroll: true,
			footerrow :true,
			userDataOnFooter : true,
			shrinkToFit:false,//表格是否自动填充
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
                userdata: 'data.data',
				repeatitems : false,
				id: 'id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
				handle.operate('',rowid);
			},
			onPaging: function(pgButton) {
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&vm_shop_code='+$("#vm_shop_code").val();
		params += '&number='+$("#number").val();
		return params;
	},
	reset:function(){
		$("#shop_name").val("");
		$("#vm_shop_code").val("");
		$("#number").val("30");
		THISPAGE.$_date.setValue(3);
		dateRedioClick("theMonth");
	},
	reloadData:function(data){
		var params=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+"?"+params}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn_search').on('click', function(e){
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			};
			THISPAGE.reloadData();
		});
			//查询
		$('#btn_ok').on('click', function(e){
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			};
			THISPAGE.reloadData();
		});
		//重置
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	
		$(window).resize(function(){
			Public.resizeSpecifyGrid("grid",111,32);
		});
	}
}

THISPAGE.init();