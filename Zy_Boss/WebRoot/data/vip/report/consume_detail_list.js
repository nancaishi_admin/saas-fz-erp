var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.VIP12;
var _menuParam = config.OPTIONLIMIT;
var searchFlag = false;
var queryurl = config.BASEPATH+'vip/report/consume_detail_list';
/*var Utils={
		doQueryColor:function(value){
	    	var url = 'url:'+CONFIG.BASEPATH+'stock/initQueryColor.action?';
	    	if(value != undefined && value != null && value != ''){
	    		url += '&SearchContent='+Public.encodeURI(value);
	    	}
			$.dialog({ 
			   	id:'batchOrder_add_customer',
			   	title:'颜色查询',
			   	data:{Cr_Name:'Cr_Name',Cr_Code:'Cr_Code'},
			   	lock:true,
			   	max: false,
			   	min: false,
			   	width:400,
			   	height:425,
			   	fixed:false,
			   	drag: true,
			   	resize:false,
			   	//设置页面加载处理
			   	content:url,
			   	ok: function () {
	                this.content.Utils.ok_Click();
	            },
	            cancel: true
		    });
		},
		doQuerySize:function(value){
	    	var url = 'url:'+CONFIG.BASEPATH+'stock/initQuerySize.action?';
	    	if(value != undefined && value != null && value != ''){
	    		url += '&SearchContent='+Public.encodeURI(value);
	    	}
			$.dialog({ 
			   	id:'batchOrder_add_customer',
			   	title:'尺码查询',
			   	data:{Sz_Name:'Sz_Name',Sz_Code:'Sz_Code'},
			   	lock:true,
			   	max: false,
			   	min: false,
			   	width:400,
			   	height:425,
			   	fixed:false,
			   	drag: true,
			   	resize:false,
			   	//设置页面加载处理
			   	content:url,
			   	ok: function () {
	                this.content.Utils.ok_Click();
	            },
	            cancel: true
		    });
		}
}*/
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		dateRedioClick("theDate");
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colNames = ['会员卡号','会员姓名','手机号','经办人','单据编号','消费日期','货号','商品名称','单位','颜色','尺码','杯型','数量','零售价',"折扣率",'折扣价','折扣金额','消费店铺'];
		var colModel = [
		    {name: 'vm_cardcode', index: 'vm_cardcode', width:95, title: false},
			{name: 'vm_name', index: 'vm_name', width:100, title: false},
			{name: 'vm_mobile', index: 'vm_mobile', width:95, title: false},
			{name: 'vm_manager', index: 'vm_manager_code', width:100, title: false,align:'center'},
	    	{name: 'shl_number', index: 'shl_number', width:150, title: false,fixed:true,align:'center'},
	    	{name: 'shl_date', index: 'shl_date', width: 90, title: false},
	    	{name: 'pd_no', index: 'pd_no', width: 80, title: false},
	    	{name: 'pd_name', index: 'pd_name', width:110, title: true},
	    	{name: 'pd_unit', index: 'pd_unit', width:60, title: false},
	    	{name: 'cr_name', index: 'cr_name', width:60, title: false},
	    	{name: 'sz_name', index: 'sz_name', width:60, title: false},
	    	{name: 'br_name', index: 'br_name', width:60, title: false},
	    	{name: 'shl_amount', index: 'shl_amount', width:60, title: false,align:'right' },
	    	{name: 'shl_sell_price', index: 'shl_sell_price', width:90, title: false,align:'right',optionloadonce:true,sortable:true},
	    	{name: 'shl_discount', index: 'shl_discount', width:70, title: false,align:'right',optionloadonce:true,sortable:true},
	    	{name: 'shl_price', index: 'shl_price', width:90, title: false,align:'right',optionloadonce:true,sortable:true},
	    	{name: 'shl_money', index: 'shl_money', width:90, title: false,align:'right',optionloadonce:true,sortable:true},
	    	{name: 'sp_name', index: 'sp_name', width: 120, title: true}
	    ];
		$('#grid').jqGrid({
			url:queryurl,
			datatype: 'local',
			width: gridWH.w-12,
			height: gridWH.h-46,
			gridview: true,
			colNames: colNames,
			colModel: colModel,
			rownumbers: true,//行号
			pager:'#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.DATAROWNUM,//每页条数
			rowList:config.DATAROWLIST,//分页条数
			autoScroll: true,
			footerrow :true,
			userDataOnFooter : true,
			shrinkToFit:false,//表格是否自动填充
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
                userdata: 'data.data',
				repeatitems : false,
				id: 'id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
				handle.operate('',rowid);
			},
			onPaging: function(pgButton) {
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&vm_cardcode='+$("#vm_cardcode").val();
		params += '&vm_mobile='+$("#vm_mobile").val();
		return params;
	},
	reset:function(){
		$("#vm_cardcode").val("");
		$("#vm_mobile").val("");
		THISPAGE.$_date.setValue(0);
		dateRedioClick("theDate");
	},
	reloadData:function(data){
		var params=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+"?"+params}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn_Search').on('click', function(e){
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			};
			THISPAGE.reloadData();
		});
			//查询
		$('#btn_ok').on('click', function(e){
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			};
			THISPAGE.reloadData();
		});
		/*//颜色
		$("#btn_SearchcrName").on('click',function(){
			colorSearch('txtcrName','txtcrNameCode');
		});
		//尺码
		$("#btn_SearchszName").on('click',function(){
			sizeSearch('txtszName','txtszNameCode');
		});*/
		//重置
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	
		$(window).resize(function(){
			Public.resizeSpecifyGrid("grid",111,32);
		});
	}
}

THISPAGE.init();