var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.VIP17;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'vip/report/brand_analysis_list';

var Utils = {
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#vm_shop_code").val(selected.sp_code);
				$("#shop_name").val(selected.sp_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#vm_shop_code").val(selected.sp_code);
					$("#shop_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	}
};

var handle = {
	countSumRateFmatter:function(val,opt,row){
		var _val = (100*row.count_sum_rate).toFixed(2) + '%';
		return _val;
	},
	moneySumRateFmatter:function(val,opt,row){
		var _val = (100*row.money_sum_rate).toFixed(2) + '%';
		return _val;
	},
	profitsSumRateFmatter:function(val,opt,row){
		var _val = (100*row.profits_sum_rate).toFixed(2) + '%';
		return _val;
	},
	operFmatter:function(val, opt, row) {
		/*var html_con = "<div class='operating' data-id='" + row.id + "'>";
		if(row.id != ""){
			html_con += "<i class='iconfont i-hand' title='明细' onclick=\"javascript:openReadonly('"+row.bd_Code+"','"+row.bd_Name+"');\">&#xe607;</i>";
		}else{
			html_con += "<span class='ui-icon' style='background:none;'></span><span class='ui-icon' style='background:none;'></span>";
		}
		html_con += "</div>";
		return html_con;*/
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		dateRedioClick("theMonth");
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
		    /*{label:'操作', name: 'operate', index: 'operate', width:60, fixed:true, formatter: handle.operFmatter, title: false, sortable:false,align:'center'},*/
	    	{label:'品牌编号', name: 'bd_code', index: 'bd_code', width:100, title: false},
		    {label:'品牌名称',name: 'bd_name', index: 'bd_name', width:150, title: false},
		    {label:'消费数量',name: 'count_sum', index: 'count_sum', width:100, title: false,align:'right'},
		    {label:'数量占比',name: 'count_sum_rate', index: 'count_sum_rate', width:100, title: false,align:'right',sortable:false,formatter:handle.countSumRateFmatter},
		    {label:'消费金额',name: 'money_sum', index: 'money_sum', width:120, title: false,align:'right'},
		    {label:'金额占比',name: 'money_sum_rate', index: 'money_sum_rate', width:120, title: false,align:'right',sortable:false,formatter:handle.moneySumRateFmatter},
	    	{label:'成本',name: 'cost_monery_sum', index: 'cost_monery_sum', width:100, title: true,align:'right'},
	    	{label:'利润',name: 'profits_sum', index: 'profits_sum', width:100, title: true,align:'right'},
	    	{label:'利润占比',name: 'profits_sum_rate', index: 'profits_sum_rate', width:100, title: true,align:'right',sortable:false,formatter:handle.profitsSumRateFmatter}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'json',
			width: gridWH.w-12,
			height: gridWH.h-34,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.BASEROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			userDataOnFooter: true,
			jsonReader: {
				root: 'data.list',
				userdata: 'data.data',
				repeatitems : false,
				id: 'id'  //图标ID
			},
			loadComplete: function(data){
				/*THISPAGE.gridTotal();*/
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&vm_shop_code='+$("#vm_shop_code").val();
		params += '&vm_cardcode='+$("#vm_cardcode").val();
		return params;
	},
	reset:function(){
		$("#shop_name").val("");
		$("#vm_shop_code").val("");
		$("#vm_cardcode").val("");
		THISPAGE.$_date.setValue(3);
		dateRedioClick("theMonth");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();