var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var isRefresh = false;
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var queryurl = config.BASEPATH+'money/expense/temp_list';
var _height = $(parent).height()-243,_width = $(parent).width()-2;

var Utils = {
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/list_dialog_bymodule',
			data : {multiselect:false,module:'expense'},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#ep_shop_code").val(selected.sp_code);
				$("#shop_name").val(selected.sp_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#ep_shop_code").val(selected.sp_code);
					$("#shop_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#ep_manager").val(selected.em_name);
				$("#ep_dm_code").val(selected.em_dm_code);
				$("#dm_name").val(selected.em_dm_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#ep_manager").val(selected.em_name);
					$("#ep_dm_code").val(selected.em_dm_code);
					$("#dm_name").val(selected.em_dm_name);
				}
			},
			cancel:true
		});
	},
	doQueryBank : function(){
		commonDia = $.dialog({
			title : '选择银行账户',
			content : 'url:'+config.BASEPATH+'money/bank/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#ep_ba_code").val(selected.ba_code);
				$("#ba_name").val(selected.ba_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#ep_ba_code").val(selected.ba_code);
					$("#ba_name").val(selected.ba_name);
				}
			},
			cancel:true
		});
	},
	doQueryProperty : function(){
		commonDia = $.dialog({
			title : '选择费用类型',
			content : 'url:'+config.BASEPATH+'money/property/to_list_dialog/0',
			data : {multiselect:true},
			width : 350,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var temps = [];
				for(var i=0;i<selected.length;i++){
					var temp = {};
					temp.epl_mp_code = selected[i].pp_code;
					temp.epl_money = 0;
					temps.push(temp);
				}
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'money/expense/temp_save',
					data:{"temps":JSON.stringify(temps)},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){
							Public.tips({type: 3, content : '保存成功'});
							THISPAGE.reloadGridData();
							commonDia.close();
						}else{
							Public.tips({type: 1, content : data.message});
						}
					}
				});
				return false;
			},
			close:function(){
				
			},
			cancel:true
		});
	}
};

var handle = {
	temp_updateRemark:function(rowid,remark){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'money/expense/temp_updateRemark',
			data:{epl_id:rowid,epl_remark:Public.encodeURI(remark)},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
    },
	temp_updateMoney:function(rowid,money){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'money/expense/temp_updateMoney',
			data:{epl_id:rowid,epl_money:money},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
    },
    temp_updateSharemonth:function(rowid,sharemonth){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'money/expense/temp_updateSharemonth',
			data:{epl_id:rowid,epl_sharemonth:sharemonth},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
    },
	temp_del: function(rowId){//删除
		if(rowId != null && rowId != ''){
			var rowData = $("#grid").jqGrid("getRowData", rowId);
		 	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'money/expense/temp_del',
					data:{"epl_id":rowId},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){
							Public.tips({type: 3, content : '删除成功!'});
							$('#grid').jqGrid('delRowData', rowId);
							THISPAGE.gridTotal();
							var ids = $("#grid").getDataIDs();
							if(ids.length == 0){
								THISPAGE.addEmptyData();
							}
						}else{
							Public.tips({type: 1, content : data.message});
						}
					}
				});
			});
	   }else{
		   Public.tips({type: 1, content : '请选择数据!'});
	   }	
    },
    temp_clear:function(){
    	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
			$.ajax({
				type:"POST",
				url:config.BASEPATH+'money/expense/temp_clear',
				data:{},
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : '删除成功!'});
						$("#grid").clearGridData();
						THISPAGE.gridTotal();
						THISPAGE.addEmptyData();
					}else{
						Public.tips({type: 1, content : data.message});
					}
				}
			});
		});
    },
	save:function(){
		if (!$("#form1").valid()) {
			return;
		}
		var ids = $("#grid").jqGrid('getDataIDs');
		if (ids.length == 0){
			Public.tips({type: 2, content : '单据明细不存在，请选择收入类型！'});
			return;
		}
		$("#btn-save").attr("disabled",true);
		var saveUrl = config.BASEPATH+"money/expense/save";
		if($("#ep_id").val() != undefined){
			saveUrl = config.BASEPATH+"money/expense/update";
		}
		$.ajax({
			type:"POST",
			url:saveUrl,
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : '保存成功'});
					handle.loadData(data.data);
				}else{
					Public.tips({type: 1, content : data.message});
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.ep_id;
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	},
	operFmatter :function(val, opt, row){
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-plus" title="新增">&#xe639;</i>';
		html_con += '<i class="iconfont i-hand ui-icon-trash" title="删除">&#xe60e;</i>';
		html_con += '</div>';
		return html_con;
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();
		if($("#ep_id").val() != undefined && $("#ep_share").val() == "1"){//修改页面选中按月分摊
			$('#grid').showCol("epl_sharemonth");
		}
	},
	initDom:function(){
		$("#ep_date").val(config.TODAY);
		this.selectRow={};
		this.$_share = $("#td_share").cssCheckbox({ callback: function($_obj){
        	if($_obj.find("input")[0].checked){//选中
        		$("#ep_share").val("1");
        		$('#grid').showCol("epl_sharemonth");
        	}else{//取消
        		$("#ep_share").val("0");
        		$('#grid').hideCol("epl_sharemonth");
        	}
		}});
	},
	gridTotal:function(){
    	var grid=$('#grid');
		var epl_money=grid.getCol('epl_money',false,'sum');
    	grid.footerData('set',{epl_pp_code:'合计：',epl_money:epl_money});
    	$("#ep_money").val(epl_money.toFixed(2));
    },
    addEmptyData:function(){
    	var emptyData = {epl_pp_code:'',pp_name:'',epl_money:'',epl_remark:''};
    	$("#grid").addRowData(0, emptyData);
    },
	initGrid:function(){
		var self=this;
		var colModel = [
		    {label:'操作',name:'operate', width: 70, fixed:true, formatter: handle.operFmatter, sortable:false,align:'center'},
	    	{label:'编号',name: 'epl_mp_code', index: 'epl_mp_code', width: 100},
	    	{label:'费用名称',name: 'mp_name', index: 'mp_name', width: 100},
	    	{label:'金额',name: 'epl_money', index: 'epl_money', width: 80,align:'right',sorttype: 'float',editable:true,formatter: PriceLimit.formatMoney},
	    	{label:'备注',name: 'epl_remark', index: 'epl_remark', width: 180,editable:true},
	    	{label:'分摊月数',name: 'epl_sharemonth', index: 'epl_sharemonth',width: 80, align: 'right',editable:true,hidden:true}
	    ];
		$('#grid').jqGrid({
			url:queryurl,
			loadonce:true,
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:9999,//每页条数
			cmTemplate: {sortable:true,title:true},
			shrinkToFit:false,//表格是否自动填充
			cellEdit: true,
            cellsubmit: 'clientArray',
            footerrow: true,
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'epl_id'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.gridTotal();
				var ids = $("#grid").getDataIDs();
				if(ids.length == 0){
					THISPAGE.addEmptyData();
				}
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
				handle.temp_update(rowid, '0');
			},
			formatCell:function (rowid, cellname, value, iRow, iCol){
            	self.selectRow.value=value;
				return value;
			},
			beforeSaveCell :function(rowid, cellname, value, iRow, iCol){
				if(rowid == 0){
					return self.selectRow.value;
				}
				if(cellname == 'epl_money'  && self.selectRow.value != value){
					if(value == '' || isNaN(value) || parseFloat(value) == 0){
						return self.selectRow.value;
					}
					if(parseFloat(self.selectRow.value) == parseFloat(value)){
						return self.selectRow.value;
					}
					if(parseFloat(value).toFixed(2).length > 11 || parseFloat(value) >1e+11){
						return self.selectRow.value;
					}
					handle.temp_updateMoney(rowid, parseFloat(value).toFixed(2));
					return parseFloat(value).toFixed(2);
				}else if(cellname == 'epl_remark' && self.selectRow.value != value){
					handle.temp_updateRemark(rowid, value);
				}else if(cellname == 'epl_sharemonth' && self.selectRow.value != value){
					if(self.selectRow.value != value){
						if(value == '' || isNaN(value) || parseInt(value) <= 1){
							return self.selectRow.value;
						}
						if(parseInt(self.selectRow.value) == parseInt(value)){
							return self.selectRow.value;
						}
						if(parseInt(value) > 24){
							return self.selectRow.value;
						}
						handle.temp_updateSharemonth(rowid, parseInt(value));
						return parseInt(value);
					}
				}
				return value;
			},
			afterSaveCell:function(rowid, cellname, value, iRow, iCol){
				if (cellname == 'epl_money' && self.selectRow.value != value){
					THISPAGE.gridTotal();
				}
				
			}
	    });
	},
	reloadGridData: function () {
        $("#grid").jqGrid('setGridParam', {datatype: 'json', page: 1, url: queryurl}).trigger("reloadGrid");
    },
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save();
		});
		$("#btn_close").click(function(e){
			e.preventDefault();
			api.close();
		});
		//清除
        $('#btnClear').click(function(e){
        	e.preventDefault();
        	handle.temp_clear();
		});
        $('#grid').on('click', '.operating .ui-icon-plus', function (e) {
        	e.preventDefault();
        	Utils.doQueryProperty();
        });
        
        //删除-列表模式
		$('#grid').on('click', '.operating .ui-icon-trash', function(e) {
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.temp_del(id);
		});
	}
};

THISPAGE.init();