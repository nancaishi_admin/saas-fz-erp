var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.MONEY17;
var _menuParam = config.OPTIONLIMIT;
var _height = $(parent).height()-285,_width = $(parent).width()-202;
var queryurl = config.BASEPATH+'money/expense/expense_head';
var type= $("#type").val();
if(type == 'shop'){
	queryurl = config.BASEPATH+'money/expense/expense_shop';
}

var Utils = {
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/list_dialog_bymodule',
			data : {multiselect:true,module:'kpi'},
			width : 460,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var codes = [];
				var names = [];
				for(var i=0;i<selected.length;i++){
					codes.push(selected[i].sp_code);
					names.push(selected[i].sp_name);
				}
				$("#shopCode").val(codes.join(","));
				$("#shop_name").val(names.join(","));
			},
			cancel:true
		});
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();
	},
	initDom:function(){
		this.initYear();
	},
	initYear:function(){
		var currentYear = new Date().getFullYear();
		var data = [
			{Code:""+parseInt(currentYear)+"",Name:""+parseInt(currentYear)+""},
			{Code:""+parseInt(currentYear-1)+"",Name:""+parseInt(currentYear-1)+""},
			{Code:""+parseInt(currentYear-2)+"",Name:""+parseInt(currentYear-2)+""},
			{Code:""+parseInt(currentYear-3)+"",Name:""+parseInt(currentYear-3)+""},
			{Code:""+parseInt(currentYear-4)+"",Name:""+parseInt(currentYear-4)+""},
			{Code:""+parseInt(currentYear-5)+"",Name:""+parseInt(currentYear-5)+""}
		];
		$('#span_year').combo({
			value: 'Code',
			text: 'Name',
			width :100,
			height:80,
			listId:'',
			defaultSelected: 0,
			editable: false,
			callback:{
				onChange: function(data){
					$("#year").val(data.Code);
				}
			}
		}).getCombo().loadData(data,["Code",$("#year").val(),0]);
	},
	initGrid:function(){
		var colModel = [
	    	{label: "月份", name: 'month', index: 'month', width: 70,align:'center'},
	    	{label:'业务收入',name: 'business_income',index:'business_income',width:100, align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'业务成本',name: 'business_cost',index:'business_cost',width:100, align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'毛利润',name: 'gross_profit',index:'gross_profit',width:100, align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'费用',name: 'expense',index:'expense',width:100, align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'净利润',name: 'net_profit',index:'net_profit',width:100, align:'right',formatter: PriceLimit.formatMoney}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'local',
			loadonce:true,
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:false,
			recordtext:'{0} - {1} 共 {2} 条',
			pgtext: '',
			multiselect:false,//多选
			viewrecords: true,
			page: 1, //只有一页
			rowNum:9999,//每页条数
			cmTemplate: {sortable:false,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id: 'month'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.gridTotal();
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	gridTotal:function(){
    	var grid=$('#grid');
		var business_income=grid.getCol('business_income',false,'sum');
		var business_cost=grid.getCol('business_cost',false,'sum');
		var gross_profit=grid.getCol('gross_profit',false,'sum');
		var expense=grid.getCol('expense',false,'sum');
		var net_profit=grid.getCol('net_profit',false,'sum');
    	grid.footerData('set',{month:'合计：',
    		business_income:business_income,
    		business_cost:business_cost,
    		gross_profit:gross_profit,
    		expense:expense,
    		net_profit:net_profit
    		});
    },
	buildParams : function(){
		var params = '';
		params += 'type='+$("#type").val();
		params += '&year='+$("#year").val();
		params += '&shopCode='+$("#shopCode").val();
		return params;
	},
	reset:function(){
		$("#shop_name").val("");
		$("#shopCode").val("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			if(type == 'shop'){
				if($.trim($("#shopCode").val()) == ""){
					Public.tips({type: 2, content : '请选择店铺！'});
					return;
				}
			}
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();