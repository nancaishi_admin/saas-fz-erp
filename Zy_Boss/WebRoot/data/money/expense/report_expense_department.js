var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.MONEY17;
var _menuParam = config.OPTIONLIMIT;
var _height = $(parent).height()-285,_width = $(parent).width()-202;
var queryurl = config.BASEPATH+'money/expense/expense_department';

var Utils = {
	doQueryProperty : function(){
		commonDia = $.dialog({
			title : '选择费用类型',
			content : 'url:'+config.BASEPATH+'money/property/to_list_dialog/0',
			data : {multiselect:true},
			width : 350,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var codes = [];
				var names = [];
				for(var i=0;i<selected.length;i++){
					codes.push(selected[i].pp_code);
					names.push(selected[i].pp_name);
				}
				$("#mp_code").val(codes.join(","));
				$("#mp_name").val(names.join(","));
			},
			cancel:true
		});
	},
	doQueryDepartment : function(){
		commonDia = $.dialog({
			title : '选择部门',
			content : 'url:'+config.BASEPATH+'base/department/to_list_dialog',
			data : {multiselect:true},
			width : 350,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				selectDeparts = selected;
				var codes = [];
				var names = [];
				for(var i=0;i<selected.length;i++){
					codes.push(selected[i].dm_code);
					names.push(selected[i].dm_name);
				}
				$("#dm_code").val(codes.join(","));
				$("#dm_name").val(names.join(","));
			},
			cancel:true
		});
	}
};

var allDeparts = [];
var selectDeparts = [];

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		dateRedioClick("theDate");
		this.getAllDepart();
	},
	getAllDepart:function(){/*获取所有部门数据*/
		$.ajax({
			type:"POST",
			async:false,
			url:config.BASEPATH+"base/department/list",
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					allDeparts=data.data;
				}
			}
		});
	},
	initGrid:function(){
		$("#grid").jqGrid('GridUnload');
		
		var colModel = [
	    	{label:'费用编号',name: 'mp_code',index:'mp_code',width:80},
	    	{label:'费用名称',name: 'mp_name',index:'mp_name',width:100}
	    ];
		if(selectDeparts != null && selectDeparts.length > 0){
			for (var i = 0; i < selectDeparts.length; i++) {
				colModel.push({label:selectDeparts[i].dm_name,name: "moneyMap."+selectDeparts[i].dm_code,index:'',width: 80,sortable:false,align:'right',formatter: PriceLimit.formatMoney});
			}
		}else{
			for (var i = 0; i < allDeparts.length; i++) {
				colModel.push({label:allDeparts[i].dm_name,name: "moneyMap."+allDeparts[i].dm_code,index:'',width: 80,sortable:false,align:'right',formatter: PriceLimit.formatMoney});
			}
		}
		colModel = colModel.concat([
			{label:'小计',name: 'total',index: 'total',width:70,align:"right",formatter: PriceLimit.formatMoney}
		]);
		
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'local',
			loadonce:true,
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:false,
			recordtext:'{0} - {1} 共 {2} 条',
			pgtext: '',
			multiselect:false,//多选
			viewrecords: true,
			page: 1, //只有一页
			rowNum:9999,//每页条数
			cmTemplate: {sortable:false,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id: 'month'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.gridTotal();
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	gridTotal:function(){
		var grid = $('#grid');
		var footerData = {mp_code:'合计'};
		
		if(selectDeparts != null && selectDeparts.length > 0){
			for (var i = 0; i < selectDeparts.length; i++) {
				footerData["moneyMap."+selectDeparts[i].dm_code] = grid.getCol("moneyMap."+selectDeparts[i].dm_code,false,'sum');
			}
		}else{
			for (var i = 0; i < allDeparts.length; i++) {
				footerData["moneyMap."+allDeparts[i].dm_code] = grid.getCol("moneyMap."+allDeparts[i].dm_code,false,'sum');
			}
		}
		footerData["total"] = grid.getCol("total",false,'sum');
		grid.footerData('set',footerData);
    },
	buildParams : function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&mp_code='+$("#mp_code").val();
		params += '&dm_code='+$("#dm_code").val();
		return params;
	},
	reset:function(){
		$("#mp_code").val("");
		$("#mp_name").val("");
		$("#dm_code").val("");
		$("#dm_name").val("");
		THISPAGE.$_date.setValue(0);
		selectDeparts = [];
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.initGrid();
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.initGrid();
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();