var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.MONEY35;
var _menuParam = config.OPTIONLIMIT;
var _height = $(parent).height()-285,_width = $(parent).width()-202;
var queryurl = config.BASEPATH+'money/bank/report_sum';

var handle = {
	formatTotal : function(val, opt, row) {
		return PriceLimit.formatMoney(row.br_enter - row.br_out);
	}
};

var Utils = {
	doQueryBank : function(){
		commonDia = $.dialog({
			title : '选择银行账户',
			content : 'url:'+config.BASEPATH+'money/bank/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#ba_code").val(selected.ba_code);
				$("#ba_name").val(selected.ba_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#ba_code").val(selected.ba_code);
					$("#ba_name").val(selected.ba_name);
				}
			},
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#br_manager").val(selected.em_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#br_manager").val(selected.em_name);
				}
			},
			cancel:true
		});
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		dateRedioClick("theDate");
		this.initTypeDom();
	},
	initTypeDom:function(){
		this.$_type_all = $("#type_all").cssCheckbox({ 
			callback: function($_obj){
	        	if($_obj.find("input")[0].checked){//选中
	        		THISPAGE.$_type_1.chkAll();
	        		THISPAGE.$_type_2.chkAll();
	        		THISPAGE.$_type_3.chkAll();
	        		THISPAGE.$_type_4.chkAll();
	        		THISPAGE.$_type_5.chkAll();
	        		THISPAGE.$_type_6.chkAll();
	        		THISPAGE.$_type_7.chkAll();
	        		THISPAGE.$_type_8.chkAll();
	        		THISPAGE.$_type_9.chkAll();
	        		THISPAGE.$_type_10.chkAll();
	        		THISPAGE.$_type_11.chkAll();
	        		THISPAGE.$_type_12.chkAll();
	        		THISPAGE.$_type_13.chkAll();
	        		THISPAGE.$_type_14.chkAll();
	        		THISPAGE.$_type_15.chkAll();
	        		THISPAGE.$_type_16.chkAll();
	        		THISPAGE.$_type_17.chkAll();
	        		$("#type").val("");
	        	}else{//取消
	        		THISPAGE.$_type_1.chkNot();
	        		THISPAGE.$_type_2.chkNot();
	        		THISPAGE.$_type_3.chkNot();
	        		THISPAGE.$_type_4.chkNot();
	        		THISPAGE.$_type_5.chkNot();
	        		THISPAGE.$_type_6.chkNot();
	        		THISPAGE.$_type_7.chkNot();
	        		THISPAGE.$_type_8.chkNot();
	        		THISPAGE.$_type_9.chkNot();
	        		THISPAGE.$_type_10.chkNot();
	        		THISPAGE.$_type_11.chkNot();
	        		THISPAGE.$_type_12.chkNot();
	        		THISPAGE.$_type_13.chkNot();
	        		THISPAGE.$_type_14.chkNot();
	        		THISPAGE.$_type_15.chkNot();
	        		THISPAGE.$_type_16.chkNot();
	        		THISPAGE.$_type_17.chkNot();
	        		$("#type").val("-1");
	        	}
			}
		});
		this.$_type_1 = $("#type_1").cssCheckbox({ callback: function($_obj){THISPAGE.buildTypeValue();}});
		this.$_type_2 = $("#type_2").cssCheckbox({ callback: function($_obj){THISPAGE.buildTypeValue();}});
		this.$_type_3 = $("#type_3").cssCheckbox({ callback: function($_obj){THISPAGE.buildTypeValue();}});
		this.$_type_4 = $("#type_4").cssCheckbox({ callback: function($_obj){THISPAGE.buildTypeValue();}});
		this.$_type_5 = $("#type_5").cssCheckbox({ callback: function($_obj){THISPAGE.buildTypeValue();}});
		this.$_type_6 = $("#type_6").cssCheckbox({ callback: function($_obj){THISPAGE.buildTypeValue();}});
		this.$_type_7 = $("#type_7").cssCheckbox({ callback: function($_obj){THISPAGE.buildTypeValue();}});
		this.$_type_8 = $("#type_8").cssCheckbox({ callback: function($_obj){THISPAGE.buildTypeValue();}});
		this.$_type_9 = $("#type_9").cssCheckbox({ callback: function($_obj){THISPAGE.buildTypeValue();}});
		this.$_type_10 = $("#type_10").cssCheckbox({ callback: function($_obj){THISPAGE.buildTypeValue();}});
		this.$_type_11 = $("#type_11").cssCheckbox({ callback: function($_obj){THISPAGE.buildTypeValue();}});
		this.$_type_12 = $("#type_12").cssCheckbox({ callback: function($_obj){THISPAGE.buildTypeValue();}});
		this.$_type_13 = $("#type_13").cssCheckbox({ callback: function($_obj){THISPAGE.buildTypeValue();}});
		this.$_type_14 = $("#type_14").cssCheckbox({ callback: function($_obj){THISPAGE.buildTypeValue();}});
		this.$_type_15 = $("#type_15").cssCheckbox({ callback: function($_obj){THISPAGE.buildTypeValue();}});
		this.$_type_16 = $("#type_16").cssCheckbox({ callback: function($_obj){THISPAGE.buildTypeValue();}});
		this.$_type_17 = $("#type_17").cssCheckbox({ callback: function($_obj){THISPAGE.buildTypeValue();}});
		
	},
	buildTypeValue:function(){
		var type_all = THISPAGE.$_type_all.chkVal().join() ? true : false;
		var type_1 = THISPAGE.$_type_1.chkVal().join() ? true : false;
		var type_2 = THISPAGE.$_type_2.chkVal().join() ? true : false;
		var type_3 = THISPAGE.$_type_3.chkVal().join() ? true : false;
		var type_4 = THISPAGE.$_type_4.chkVal().join() ? true : false;
		var type_5 = THISPAGE.$_type_5.chkVal().join() ? true : false;
		var type_6 = THISPAGE.$_type_6.chkVal().join() ? true : false;
		var type_7 = THISPAGE.$_type_7.chkVal().join() ? true : false;
		var type_8 = THISPAGE.$_type_8.chkVal().join() ? true : false;
		var type_9 = THISPAGE.$_type_9.chkVal().join() ? true : false;
		var type_10 = THISPAGE.$_type_10.chkVal().join() ? true : false;
		var type_11 = THISPAGE.$_type_11.chkVal().join() ? true : false;
		var type_12 = THISPAGE.$_type_12.chkVal().join() ? true : false;
		var type_13 = THISPAGE.$_type_13.chkVal().join() ? true : false;
		var type_14 = THISPAGE.$_type_14.chkVal().join() ? true : false;
		var type_15 = THISPAGE.$_type_15.chkVal().join() ? true : false;
		var type_16 = THISPAGE.$_type_16.chkVal().join() ? true : false;
		var type_17 = THISPAGE.$_type_17.chkVal().join() ? true : false;
		var count = 0;
		var type = '';
    	if(type_1){type +='1,';count++;}
    	if(type_2){type +='2,';count++;}
    	if(type_3){type +='3,';count++;}
    	if(type_4){type +='4,';count++;}
    	if(type_5){type +='5,';count++;}
    	if(type_6){type +='6,';count++;}
    	if(type_7){type +='7,';count++;}
    	if(type_8){type +='8,';count++;}
    	if(type_9){type +='9,';count++;}
    	if(type_10){type +='10,';count++;}
    	if(type_11){type +='11,';count++;}
    	if(type_12){type +='12,';count++;}
    	if(type_13){type +='13,';count++;}
    	if(type_14){type +='14,';count++;}
    	if(type_15){type +='15,';count++;}
    	if(type_16){type +='16,';count++;}
    	if(type_17){type +='17,';count++;}
    	if(type.length > 0){
    		type = type.substring(0, type.length-1);
    	}else{
    		type = '-1';
    	}
    	if(count == 17){
    		THISPAGE.$_type_all.chkAll();
    		type = '';
    	}else{
    		if(type_all){
    			THISPAGE.$_type_all.chkNot();
    		}
    	}
    	$("#type").val(type);
	},
	initGrid:function(){
		var colModel = [
	    	{label:'编号',name: 'br_ba_code',index: 'ba_code',width:80},
	    	{label:'名称',name: 'bank_name',index: 'ba_name',width:120},
	    	{label:'存款余额',name: 'br_enter',index: 'br_enter',width:120,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'取款余额',name: 'br_out',index: 'br_out',width:120,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'小计金额',name: 'total',index: 'total',width:120,align:'right',formatter: handle.formatTotal}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:-1,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:false,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'ba_code'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.gridTotal();
			},
			loadError: function(xhr, status, error){
			}
	    });
	},
	gridTotal:function(){
    	var grid=$('#grid');
		var br_enter=grid.getCol('br_enter',false,'sum');
		var br_out=grid.getCol('br_out',false,'sum');
    	grid.footerData('set',{br_ba_code:'合计：',br_enter:br_enter,br_out:br_out,total:''});
    },
    buildParams : function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&ba_code='+$("#ba_code").val();
		params += '&type='+$("#type").val();
		params += '&br_manager='+Public.encodeURI($("#br_manager").val());
		return params;
	},
	reset:function(){
		$("#ba_code").val("");
		$("#ba_name").val("");
		$("#br_manager").val("");
		THISPAGE.$_date.setValue(0);
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();