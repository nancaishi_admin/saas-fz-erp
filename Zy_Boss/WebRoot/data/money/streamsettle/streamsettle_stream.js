var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var isRefresh = false;
var _limitMenu = system.MENULIMIT.MONEY19;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'base/stream/page';
var _height = $(parent).height()-294,_width = $(parent).width()-192;
var handle = {
	formatActualDebt:function(val, opt, row){
		return PriceLimit.formatMoney(row.se_payable-row.se_payabled);
	},
	operFmatter : function(val, opt, row){
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-bill" title="物流单">&#xe64f;</i>';
		html_con += '<i class="iconfont i-hand ui-icon-settle" title="结算单">&#xe64c;</i>&nbsp;';
		html_con += '</div>';
		return html_con;
	},
	viewBill:function(rowid){
		var rowData = $("#grid").jqGrid("getRowData", rowid);
		$.dialog({
			title : false,
			content : 'url:'+config.BASEPATH+'money/streamsettle/to_bill',
			data: {se_code:rowData.se_code},
			max : false,
			min : false,
			cache : false,
			lock: false
		}).max();
	},
	viewSettle:function(rowid){
		var rowData = $("#grid").jqGrid("getRowData", rowid);
		$.dialog({
			title : false,
			content : 'url:'+config.BASEPATH+'money/streamsettle/to_list',
			data: {se_code:rowData.se_code},
			max : false,
			min : false,
			cache : false,
			lock: false
		}).max();
	}
};
var THISPAGE = {
	init:function (){
		this.initGrid();
		this.initEvent();	
	},
	initParam:function(){
	},
	gridTotal:function(){
    	var grid=$('#grid');
		var se_init_debt=grid.getCol('se_init_debt',false,'sum');
		var se_payable=grid.getCol('se_payable',false,'sum');
    	var se_payabled=grid.getCol('se_payabled',false,'sum');
    	grid.footerData('set',{se_code:'合计：',
    		se_init_debt:se_init_debt,
    		se_payable:se_payable,
    		se_payabled:se_payabled,
    		actual_debt:''});
    },
	initGrid:function(){
		var colModel = [
	    	{label:'操作',name: 'operate',width: 70, fixed:true, formatter: handle.operFmatter,align:'center', title: false,sortable:false},
            {label:'编号',name: 'se_code', index: 'se_code', width: 80, title: false, fixed: true,align: 'left'},
            {label:'名称',name: 'se_name', index: 'se_name', width: 120, title: false,align: 'left'},
            {label:'负责人',name: 'se_man', index: 'se_man', width: 100, title: false, align: 'left'},
            {label:'手机号码',name: 'se_mobile', index: 'se_mobile', width: 120, title: false, align: 'left'},
            {label:'期初欠款',name: 'se_init_debt', index: 'se_init_debt',sorttype: 'float',width:80, align:'right',formatter: PriceLimit.formatMoney},
			{label:'应付金额',name: 'se_payable', index: 'se_payable',sorttype: 'float',width:80, align:'right',formatter: PriceLimit.formatMoney},
			{label:'已付金额',name: 'se_payabled', index: 'se_payabled',sorttype: 'float',width:80, align:'right',formatter: PriceLimit.formatMoney},
			{label:'实际欠款',name: 'actual_debt', index: 'actual_debt',sorttype: 'float',width:80, align:'right',formatter: handle.formatActualDebt}
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:30,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			footerrow: true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'se_id'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.gridTotal();
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	reloadData:function(){
		var param = "?searchContent="+$("#SearchContent").val();
		$("#grid").jqGrid('setGridParam',{datatype:"json",url:queryurl+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		$('#SearchContent').on('keyup',function(){
			if (event.keyCode==13){
				$('#search').click();
			}
		});
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
		
		$('#grid').on('click', '.operating .ui-icon-settle', function(e) {
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.viewSettle(id);
		});
		$('#grid').on('click', '.operating .ui-icon-bill', function(e) {
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.viewBill(id);
		});
	}
};
THISPAGE.init();