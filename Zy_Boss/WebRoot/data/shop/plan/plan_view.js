var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;

var pl_number = $("#pl_number").val();
var pl_type = $("#pl_type").val();

var handle = {
	approve:function(){
		commonDia = $.dialog({ 
		   	id:'approve_confirm',
		   	title:'审批页面',
		   	data:{},
		   	max: false,
		   	min: false,
		   	lock:true,
		   	width:300,
		   	height:200,
		   	drag: true,
		   	resize:false,
		   	content:'url:'+config.BASEPATH+'approve/to_confirm',
		   	fixed:false,
		   	close : function(){
		   		var ar_infos = commonDia.content.ar_infos;
		   		if(ar_infos != undefined){
		   			handle.doApprove(ar_infos);
		   		}
		   	}
	    });
	},
	doApprove:function(ar_infos){
		$("#btn-approve").attr("disabled",true);
		var params = "";
		params += "number="+pl_number;
		params += "&ar_state="+ar_infos.ar_state;
		params += "&ar_describe="+Public.encodeURI(ar_infos.ar_describe);
		$.ajax({
            type: "POST",
            url: config.BASEPATH+"shop/plan/approve",
            data: params,
            cache:false,
			dataType:"json",
            success: function (data) {
            	if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : '审核成功'});
					handle.loadData(data.data);
				}else{
					Public.tips({type: 1, content : data.message});
				}
				$("#btn-approve").attr("disabled",false);
            }
        });
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.pl_id;
		pdata.pl_number=data.pl_number;
		pdata.pl_ar_state=data.pl_ar_state;
		pdata.pl_ar_date=data.pl_ar_date;
		pdata.operate='';
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initEvent();
		if(pl_type == '1'){
			this.initExpenseGrid();
		}
	},
	initDom:function(){
		if('approve' == api.data.oper){
			$("#btn-approve").show();
		}
	},
	initExpenseGrid:function(){
		var colModel = [
	    	{label:'编号',name: 'pe_expensecode', index: 'pe_expensecode', width: 80, title: false,fixed:true},
	    	{label:'费用名称',name: 'pe_expensename', index: 'pe_expensename', width: 140, title: false},
	    	{label:'金额',name: 'pe_money', index: 'pe_money', width: 100, title: false,align:'right',formatter: PriceLimit.formatMoney}
	    ];
		$('#expensegrid').jqGrid({
			url:config.BASEPATH+'shop/plan/listExpense/'+pl_number,
			loadonce:true, //只从后台加载一次数据，datatype被修改为local,如果弹出框想要查询则再开启为json
			datatype: 'json',
			width: 400,
			height: 150,
			altRows:true,
			gridview: true,
			onselectrow: false,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#expensepage',//分页
			multiselect:false,//多选
			viewrecords: true,
			cmTemplate: {sortable:true,title:false},
			page: 1, //只有一页
			pgbuttons:false,
			pgtext:false,
			rowNum:99999,//每页条数
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'pe_id'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.gridExpenseTotal();
			},
			loadError: function(xhr, status, error){		
				Public.tips({type: 1, content : '操作失败了哦，服务可能已过期，请按F5刷新！'});
			}
	    });
	},
	gridExpenseTotal:function(){
		var grid=$('#expensegrid');
		var pe_money=grid.getCol('pe_money',false,'sum');
    	grid.footerData('set',{pe_expensecode:'合计:',pe_money:pe_money});
    },
    gridMonthDataTotal:function(){
		var grid=$('#monthdatagrid');
		var pm_sell_money_pre=grid.getCol('pm_sell_money_pre',false,'sum');
		var pm_sell_money_plan=grid.getCol('pm_sell_money_plan',false,'sum');
    	grid.footerData('set',{pm_sell_money_pre:pm_sell_money_pre,pm_sell_money_plan:pm_sell_money_plan});
    },
    initMonthDataGrid:function(){
		var colModel = [
	    	{label:'月份',name: 'pm_month', index: 'pm_month', width: 80, fixed:true},
	    	{label:'同期销售',name: 'pm_sell_money_pre', index: 'pm_sell_money_pre', width: 100, align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'计划金额',name: 'pm_sell_money_plan', index: 'pm_sell_money_plan', width: 100,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'备注',name: 'pm_remark', index: 'pm_remark', width: 200}
	    ];
		$('#monthdatagrid').jqGrid({
			url:config.BASEPATH+'shop/plan/listMonth/'+pl_number,
			loadonce:true, //只从后台加载一次数据，datatype被修改为local,如果弹出框想要查询则再开启为json
			datatype: 'json',
			width: 800,
			height: $(parent).height()-200,
			altRows:true,
			gridview: true,
			onselectrow: false,
			colModel: colModel,
			sortable:false,
			rownumbers: true,//行号
			pager: '#monthdatapage',//分页
			multiselect:false,//多选
			viewrecords: true,
			cmTemplate: {sortable:true,title:false},
			page: 1, //只有一页
			pgbuttons:false,
			pgtext:false,
			rowNum:99999,//每页条数
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'pm_id'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.gridMonthDataTotal();
			}
	    });
	},
	initEvent:function(){
		$('#btn-approve').on('click', function(e){
			e.preventDefault();
			handle.approve();
		});
		$("#btn_close").click(function(e){
			e.preventDefault();
			api.close();
		});
	}
};

THISPAGE.init();