var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.SHOP21;
var _menuParam = config.OPTIONLIMIT;

var Utils = {
	display:function(obj,mode){
		$("#CurrentMode").val(mode);
		$("a[name='mode-btn']").removeClass("on");
		$(obj).addClass("on");
		THISPAGE.loadChartData();
	},
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择计划门店',
			content : 'url:'+config.BASEPATH+'base/shop/list_dialog_bymodule',
			data : {multiselect:false,module:'want'},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#pl_shop_code").val(selected.sp_code);
				$("#shop_name").val(selected.sp_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#pl_shop_code").val(selected.sp_code);
					$("#shop_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#wt_manager").val(selected.em_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#wt_manager").val(selected.em_name);
				}
			},
			cancel:true
		});
	}
};


var THISPAGE = {
	init:function (){
		this.initDom();
		this.initEvent();
		this.loadChartData();
	},
	initDom:function(){
		this.initYear();
	},
	initYear:function(){
		var currentYear = new Date().getFullYear();
		var data = [
		    {Code:""+parseInt(currentYear)+"",Name:""+parseInt(currentYear)+""},
			{Code:""+parseInt(currentYear-1)+"",Name:""+parseInt(currentYear-1)+""},
		    {Code:""+parseInt(currentYear-2)+"",Name:""+parseInt(currentYear-2)+""}
		];
		$('#span_year').combo({
			data:data,
			value: 'Code',
			text: 'Name',
			width :120,
			height:80,
			listId:'',
			defaultSelected: 0,
			editable: false,
			callback:{
				onChange: function(data){
					$("#pl_year").val(data.Code);
				}
			}
		}).getCombo();
	},
	loadChartData:function(){
		var CurrentMode = $("#CurrentMode").val();
		var url = null;
		if(CurrentMode == '0'){
			url = config.BASEPATH+'shop/plan/chartByShop?'+THISPAGE.buildParams();
		}else if(CurrentMode == '1'){
			url = config.BASEPATH+'shop/plan/chartByMonth?'+THISPAGE.buildParams();
		}
		$.ajax({
			type:"POST",
			url:url,
			cache:false,
			dataType:"json",
			success:function(data){
				if(data != undefined && data.stat == 200){
					var chartData = data.data;
					if(CurrentMode == '0'){
						THISPAGE.buildChartByShop(chartData);
					}else if(CurrentMode == '1'){
						THISPAGE.buildChartByMonth(chartData);
					}
					
				}
			}
		 });
	},
	buildChartByShop:function(chartData){
		var x = new Array(chartData.length);
		var plan = new Array(chartData.length);
		var real = new Array(chartData.length);
		for(var i=0;i<chartData.length;i++){
			x[i] = '"'+chartData[i].shop_name+'"';
			plan[i] = chartData[i].sell_money_plan;
			real[i] = chartData[i].sell_money_real;
		}
		var chartjs = '';
		chartjs += '{';
		chartjs += '"chart":{"type":"column","margin": 75,"options3d":{"enabled":true,"alpha":0,"beta": 0,"depth": 50,"viewDistance": 25}},';
		chartjs += '"title":{"text":"'+$("#pl_year").val()+'年计划完成图形"},';
		chartjs += '"xAxis": {"title": {"text": "店铺"},"categories": ['+x.toString()+']},';
		chartjs += '"yAxis": {"min": 0,"title": {"text": "金额(元)"}},'; 
		chartjs += '"tooltip":{"headerFormat": "<span style=\'font-size:13px;font-weight:bold\'>{point.key}</br></span>","pointFormat": "<b>{series.name}</b>:<b>{point.y:.2f}</b>"},';
		chartjs += '"plotOptions": {"column": {"pointPadding": 0.2,"borderWidth": 0}},';
		chartjs +='"series": [{"name": "计划金额","data": ['+plan+'],';
		chartjs +='"dataLabels": {"enabled": false,"rotation": -45,"color": "#FFFFFF","align": "right","x": 4,"y": 10,'; 
		chartjs +='"style": {"fontSize": "10px","fontFamily": "Verdana, sans-serif","textShadow": "0 0 3px black"}}},';
		chartjs +='{"name": "完成金额","data": ['+real+'], "color": "#ff0000",';
		chartjs +='"dataLabels": {"enabled": false,"rotation": -70,"color": "#FFFFFF","align": "right","x": 4,"y": 10,'; 
		chartjs +='"style": {"fontSize": "10px","fontFamily": "Verdana, sans-serif","textShadow": "0 0 3px black"}}}]';
		chartjs += '}';
		eval("$('#container').highcharts("+chartjs+")");
	},
	buildChartByMonth:function(chartData){
		var x = new Array(chartData.length);
		var plan = new Array(chartData.length);
		var real = new Array(chartData.length);
		for(var i=0;i<chartData.length;i++){
			x[i] = '"'+chartData[i].month+'月份"';
			plan[i] = chartData[i].sell_money_plan;
			real[i] = chartData[i].sell_money_real;
		}
		var chartjs = '';
		chartjs += '{';
		chartjs += '"chart":{"type":"column","margin": 75,"options3d":{"enabled":true,"alpha":0,"beta": 0,"depth": 50,"viewDistance": 25}},';
		chartjs += '"title":{"text":"'+$("#pl_year").val()+'年计划完成图形"},';
		chartjs += '"xAxis": {"title": {"text": "月份"},"categories": ['+x.toString()+']},';
		chartjs += '"yAxis": {"min": 0,"title": {"text": "金额(元)"}},'; 
		chartjs += '"tooltip":{"headerFormat": "<span style=\'font-size:13px;font-weight:bold\'>{point.key}</br></span>","pointFormat": "<b>{series.name}</b>:<b>{point.y:.2f}</b>"},';
		chartjs += '"plotOptions": {"column": {"pointPadding": 0.2,"borderWidth": 0}},';
		chartjs +='"series": [{"name": "计划金额","data": ['+plan+'],';
		chartjs +='"dataLabels": {"enabled": false,"rotation": -45,"color": "#FFFFFF","align": "right","x": 4,"y": 10,'; 
		chartjs +='"style": {"fontSize": "10px","fontFamily": "Verdana, sans-serif","textShadow": "0 0 3px black"}}},';
		chartjs +='{"name": "完成金额","data": ['+real+'], "color": "#ff0000",';
		chartjs +='"dataLabels": {"enabled": false,"rotation": -70,"color": "#FFFFFF","align": "right","x": 4,"y": 10,'; 
		chartjs +='"style": {"fontSize": "10px","fontFamily": "Verdana, sans-serif","textShadow": "0 0 3px black"}}}]';
		chartjs += '}';
		eval("$('#container').highcharts("+chartjs+")");
	},
	buildParams : function(){
		var params = '';
		params += 'pl_shop_code='+$("#pl_shop_code").val();
		params += '&pl_year='+$("#pl_year").val();
		params += '&month='+$("#month").val();
		return params;
	},
	reset:function(){
		$("#pl_shop_code").val("");
		$("#shop_name").val("");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.loadChartData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();