var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var isRefresh = false;
var _limitMenu = system.MENULIMIT.SHOP08;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'shop/deposit/list';


var Utils = {
		doQueryShop : function(){
			commonDia = $.dialog({
				title : '选择店铺',
				content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
				data : {multiselect:false},
				width : 450,
				height : 370,
				max : false,
				min : false,
				cache : false,
				lock: true,
				ok:function(){
					var selected = commonDia.content.doSelect();
					if (!selected) {
						return false;
					}
				},
				close:function(){
					var selected = commonDia.content.doSelect();
					if(selected){
						$("#sdl_shop_code").val(selected.sp_code);
						$("#shop_name").val(selected.sp_name);
					}
				},
				cancel:true
			});
		}
}
var handle = {
	operFmatter : function(val, opt, row){
		var btnHtml = '';
		if (row.sdl_state == '0') {
            btnHtml += '<input type="button" value="申报" class="btn_sp" onclick="" />';
        }else if (row.sdl_state == '1') {
        	btnHtml += '<input type="button" value="短信提醒" class="btn_wx" onclick="" />';
        }
		return btnHtml;
	},
	sdl_ismessage:function(val, opt, row){
		if(val == '0'){
			return '未发送';
		} else if(val == '1'){
			return '已发送';
		}  else {
			return '';
		}
	},
	sdl_state:function(val, opt, row){
		if(val == '0'){
			return '已预付';
		} else if(val == '1'){
			return '已申报';
		} else if(val == '2'){
			return '已到货';
		}else if(val == '3'){
			return '已销售';
		}else if(val == '4'){
			return '不销售';
		} else {
			return '';
		}
	}
}
 


var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom: function(){
		this.$_date = $('#date');
	 	this.$_date.cssRadio(
		 	{callback:
		 		function($_obj){  //$_obj是radio外面的lable
		 			dateRedioClick($_obj.find("input").attr("id"));
		 		}
		 	}
	 	);
	 	this.$_satae = $('#satae');
	 	this.$_satae.cssRadio(
		 	{callback:
		 		function($_obj){  //$_obj是radio外面的lable
		 			$("#sdl_state").val($($_obj).find('input').val());
		 		}
		 	}
	 	);
	},
	initGrid:function (){
		var gridWH = Public.setGrid();
		var colModel = [
		    {name: 'sdl_id', label:'',index: 'sdl_id', hidden:true},
	    	{name: 'operate', label:'操作', width: 60, fixed:true,title: false,formatter: handle.operFmatter, sortable:false},
	    	{name: 'sdl_number',label:'单据编号', index: 'sdl_number', width: 120, title: false,fixed:true},
	    	{name: 'sd_customer',label:'预付客户', index: 'sd_customer', width: 100, title: false},
	    	{name: 'sd_tel',label:'手机号码', index: 'sd_tel', width: 100, title: false},
	    	{name: 'sdl_sp_name',label:'预付店铺', index: 'sdl_sp_name', width: 100, title: false},
	    	{name: 'sdl_pd_no',label:'商品货号', index: 'sdl_pd_no', width: 80, title: false},
	    	{name: 'sdl_pd_name',label:'名称', index: 'sdl_pd_name', width: 80, title: false}, 
	    	{name: 'sdl_cr_name',label:'颜色', index: 'sdl_cr_name', width:60, title: false,align:'center'},
	    	{name: 'sdl_sz_name',label:'尺码', index: 'sdl_sz_name', width:60, title: false,align:'center'},
	    	{name: 'sdl_br_name',label:'杯型', index: 'sdl_br_name', width:60, title: false,align:'center'},
	    	{name: 'sdl_amount',label:'数量', index: 'sdl_amount', width:60, title: false,align:'right'},
	    	{name: 'sdl_ismessage',label:'短信状态', index: 'sdl_ismessage', width: 80, title: false,align:'center',formatter:handle.sdl_ismessage},
	    	/*{name: 'sd_state',label:'订金状态', index: 'sd_state', width: 100, title: false,align:'center'},*/
	    	{name: 'sdl_state',label:'处理状态', index: 'sdl_state', width: 80, title: false,align:'center',formatter:handle.sdl_state}
	    ];
	    $("#grid").jqGrid({
	    	url:queryurl,
			//loadonce:true, //只从后台加载一次数据，datatype被修改为local,如果弹出框想要查询则再开启为json
			datatype: 'json',
			width: gridWH.w-12,
			height: gridWH.h-20,
			altRows:true,
			gridview: true,
			onselectrow: false,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			multiselect:false,//多选
			viewrecords: true,
			pgbuttons:true,
			rowNum:30,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			shrinkToFit:false,//表格是否自动填充
			autoScroll: true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id:'sdl_id'
			}
	    });
	},
	buildParams:function(){
		var begindate = $("#begindate").val();//开始日期		
		var enddate = $("#enddate").val();//结束日期		
		var sdl_state = $("#sdl_state").val();
		var sdl_shop_code = $("#sdl_shop_code").val();
		var pd_no = Public.encodeURI($("#pd_no").val());
		var sdl_number = Public.encodeURI($("#sdl_number").val());
		var params = '';
		params += 'begindate='+begindate;
		params += '&enddate='+enddate;
		params += '&sdl_state='+sdl_state;
		params += '&sdl_shop_code='+sdl_shop_code;
		params += '&pd_no='+pd_no;
		params += '&sdl_number='+sdl_number;
		return params;
	},
	reset:function(){
		$("#sdl_number").val("");
		$("#pd_no").val("");
		$("#shop_name").val("");
		$("#sdl_shop_code").val("");
		$("#sdl_state5").click();
		$("#sdl_state").val("");
		$("#theDate").click();
		$("#begindate").val("");
		$("#enddate").val("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		//重置
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
};
THISPAGE.init();