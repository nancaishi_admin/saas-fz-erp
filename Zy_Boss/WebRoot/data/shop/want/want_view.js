var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var isRefresh = false;
var wt_number = $("#wt_number").val();
var wt_type = $("#wt_type").val();
var wt_ar_state = $("#wt_ar_state").val();
var isFHD = parseInt($("#wt_applyamount").val())==0;
var isSend = $("#isSend").val() == "1";
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var queryurl = config.BASEPATH+'shop/want/detail_list/'+wt_number;
var querysumurl = config.BASEPATH+'shop/want/detail_sum/'+wt_number;
var querysizeurl = config.BASEPATH+'shop/want/detail_size/'+wt_number;
querysizeurl += '?wt_ar_state='+wt_ar_state+'&isFHD='+(isFHD?'1':'0')+'&isSend='+(isSend ?'1':'0');
var querysizetitleurl = config.BASEPATH+'shop/want/detail_size_title/'+wt_number;
var needListRefresh = false;//列表模式
var needSizeRefresh = false;//尺码模式
var needSumRefresh = false;//汇总模式
var _height = $(parent).height()-353,_width = $(parent).width()-2;
var showMode = {
	display:function(mode){
		var ids = $("#grid").getDataIDs();
		if(ids == undefined || null == ids || ids.length == 0){
			if(mode != 0){
				Public.tips({type: 1, content : "未添加数据，不能切换模式"});
				return;
			}
		}
		$("#CurrentMode").val(mode);
		$("a[name='mode-btn']").removeClass("on");
		$("#list-grid").hide();
		$("#size-grid").hide();
		$("#sum-grid").hide();
		if(mode == 0){
			$("#listBtn").addClass("on");
			$("#list-grid").show();
			showMode.refreshList();
		}else if(mode == 1){
			$("#sizeBtn").addClass("on");
			$("#size-grid").show();
			showMode.refreshSize();
		}else if(mode == 2){
			$("#sumBtn").addClass("on");
			$("#sum-grid").show();
			showMode.refreshSum();
		}
	},
	refreshList:function(){//列表模式刷新数据
		if(!needListRefresh){
    		return;
    	}
    	THISPAGE.reloadGridData();
    	needListRefresh = false;
	},
	refreshSum:function(){
		if(!needSumRefresh){
			return;
		}
		$('#sumGrid').GridUnload();
		THISPAGE.addEvent();
		THISPAGE.initSumGrid();
		needSumRefresh = false;
	},
	refreshSize:function(){
		if(!needSizeRefresh){
			return;
		}
		$.ajax({
			type:"POST",
			url:querysizetitleurl,
			cache:false,
			dataType:"json",
            success: function (data) {
            	if(undefined != data && data.stat == 200){
                    var titles = data.data.titles;
                    var sizeCodeMap = data.data.sizeCodeMap;
                    $('#sizeGrid').GridUnload();
                    THISPAGE.initSizeGroupGrid(titles, sizeCodeMap);
                    THISPAGE.addEvent();
                    needSizeRefresh = false;
				}else{
					Public.tips({type: 1, content : data.message});
				}
            }
        });
	},
	refresh:function(){
		var currentMode = $("#CurrentMode").val();
    	if(currentMode == '0'){//列表模式
    		needListRefresh = true;
    		showMode.refreshList();
        	needSizeRefresh = true;
        	needSumRefresh = true;
    	}else if(currentMode == '1'){//尺码模式
    		needSizeRefresh = true;
    		showMode.refreshSize();
    		needListRefresh = true;
        	needSumRefresh = true;
    	}else if(currentMode == '2'){//汇总模式
    		needSumRefresh = true;
    		showMode.refreshSum();
    		needListRefresh = true;
        	needSizeRefresh = true;
    	}
	}
};

var Choose = {
	selectProduct:function(){
		var wt_shop_code = $.trim($("#wt_shop_code").val());
		var wt_outdp_code = $.trim($("#wt_outdp_code").val());
		if (wt_outdp_code == '') {
			Public.tips({type: 2, content : "请先选择仓库！"});
	        return;
	    }
		$.dialog({
			title : '商品录入',
			content : 'url:'+config.BASEPATH+'shop/want/to_select_product/'+wt_type,
			data : {dp_code:wt_outdp_code,sp_code:wt_shop_code,from:'send',wt_number:wt_number},
			width : 1080,
			height : 440,
			max : false,
			min : false,
			cache : false,
			lock: true,
			close:function(){
				showMode.refresh();
			}
		});
	},
	barCode:function(){
		var CurrentMode = $("#CurrentMode").val();
		if(CurrentMode == 1){//若是尺码模式调整到列表模式
			showMode.display(0);
		}
		var barcode = $.trim($("#barcode").val());
		var barcode_amount = $.trim($("#barcode_amount").val());
		if(barcode == ""){
			return;
		}
		if (barcode_amount == "" || isNaN(barcode_amount)) {
			Public.tips({type: 2, content : '请正确输入数量!'});
	        $('#barcode_amount').select();
	        return;
	    }
		var wt_shop_code = $.trim($("#wt_shop_code").val());
		if (wt_shop_code == '') {
			Public.tips({type: 2, content : "请先选择要货门店！"});
	        return;
	    }
		var params = "";
		params += "barcode="+barcode;
		params += "&amount="+barcode_amount;
		params += "&sp_code="+wt_shop_code;
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"shop/want/detail_save_bybarcode/"+wt_type+"/"+wt_number,
			data:params,
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					if(data.data.result == 3){//条码不存在
						Choose.barcodeNotExist(barcode);
						return;
					}
					needSizeRefresh = true;
					$("#barcode").val("");
					var temp = data.data.temp;
					//列表模式
					if(data.data.result == 1){//update
						var rowData = $("#grid").jqGrid("getRowData", temp.wtl_id);
						rowData.wtl_sendamount = temp.wtl_sendamount;
						rowData.wtl_sendmoney = rowData.wtl_sendamount * rowData.wtl_unitprice;
						rowData.wtl_sendcostmoney = rowData.wtl_sendamount * rowData.wtl_costprice;
						$("#grid").delRowData(temp.wtl_id);
						$("#grid").addRowData(temp.wtl_id, rowData, 'first');
						THISPAGE.gridTotal();
					}else if(data.data.result == 2){//add
						$("#grid").addRowData(temp.wtl_id, temp, 'first');
						THISPAGE.gridTotal();
					}
					//汇总模式
			    	var ids = $("#sumGrid").jqGrid('getDataIDs');
	            	var oldSumRowData = null;
	            	var sum_id = null;
					for(var i=0;i < ids.length;i++){
						var sumRowData = $("#sumGrid").jqGrid("getRowData", ids[i]);
						if(sumRowData.pd_code == temp.pd_code){
							oldSumRowData = sumRowData;
							sum_id = ids[i];
							break;
						}
					}
					if(oldSumRowData != null){
						oldSumRowData.wtl_sendamount = parseInt(oldSumRowData.wtl_sendamount) + parseInt(barcode_amount);
						oldSumRowData.wtl_sendmoney = oldSumRowData.wtl_sendamount * oldSumRowData.wtl_unitprice;
						oldSumRowData.wtl_sendcostmoney = oldSumRowData.wtl_sendamount * oldSumRowData.wtl_costprice;
						$("#sumGrid").jqGrid('setRowData', sum_id, oldSumRowData);
						THISPAGE.sumGridTotal();
					}else{
						$("#sumGrid").addRowData(temp.wtl_id, temp, 'first');
						THISPAGE.sumGridTotal();
					}
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
	},
	barcodeNotExist:function(barcode){
		$("#barcode").val("无此商品").select();
		var BarCodeCheckNotPass = document.getElementById("BarCodeCheckNotPass");
		if(BarCodeCheckNotPass.style.display == 'none'){
			BarCodeCheckNotPass.style.display = '';
			
			var barCodeDiv=$("#BarCodeCheckNotPass");
			var barCodeDivTop=$("#barcode_amount").offset().top
				,barCodeDivLeft=$("#barcode_amount").offset().left;
			barCodeDiv.css(
			{
				"top":barCodeDivTop-150
				,"left":barCodeDivLeft+30
			}
			);
		}
		var CheckNotPassText = document.getElementById("CheckNotPassText");
		if($("#CheckNotPassTextShow").val().indexOf(barcode+";") > -1){
			return;
		}
		CheckNotPassText.innerHTML = CheckNotPassText.innerHTML + "<p>" + barcode + "</p>";
		$("#CheckNotPassTextShow").val($("#CheckNotPassTextShow").val()+barcode+";");
	}
};

var handle = {
	detail_updateAmount:function(rowid,amount){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'shop/want/detail_updateAmount',
			data:{wtl_id:rowid,wtl_sendamount:wt_type==0?amount:-amount},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					needSizeRefresh = true;
					needSumRefresh = true;
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
    },
    detail_updatePrice:function(pd_code,unitPrice){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'shop/want/detail_updatePrice',
			data:{wtl_number:wt_number,wtl_pd_code:pd_code,wtl_unitprice:unitPrice},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					showMode.refresh();
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
    },
    detail_del: function(rowId){//删除
		if(rowId != null && rowId != ''){
			var rowData = $("#grid").jqGrid("getRowData", rowId);
			if(rowData.wtl_applyamount != 0){
				Public.tips({type: 2, content : '申请数量大于0的数据不能删除!'});
				return;
			}
		 	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'shop/want/detail_del',
					data:{"wtl_id":rowId},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){
							Public.tips({type: 3, content : '删除成功!'});
							$('#grid').jqGrid('delRowData', rowId);
							THISPAGE.gridTotal();
							needSizeRefresh = true;
							needSumRefresh = true;
						}else{
							Public.tips({type: 1, content : data.message});
						}
					}
				});
			});
	   }else{
		   Public.tips({type: 1, content : '请选择数据!'});
	   }	
    },
    detail_update : function(id,type){
		var wt_shop_code = $.trim($("#wt_shop_code").val());
		var wt_outdp_code = $.trim($("#wt_outdp_code").val());
		if (wt_outdp_code == '') {
			Public.tips({type: 2, content : "请先选择仓库！"});
	        return;
	    }
		var params = {};
 		var rowData;
 		if(type == '0'){
 			rowData = $("#grid").jqGrid("getRowData", id);
 			params.pd_code = rowData.wtl_pd_code;
 		}else if(type == '1'){
			rowData = $("#sizeGrid").jqGrid("getRowData", id);
			params.pd_code = rowData.pd_code;
		}else if(type == '2'){
			rowData = $("#sumGrid").jqGrid("getRowData", id);
			params.pd_code = rowData.wtl_pd_code;
		}
 		params.sp_code = wt_shop_code;
 		params.dp_code = wt_outdp_code;
 		params.from = 'send';
 		params.wt_number = wt_number;
		$.dialog({
			title : '修改商品',
			content : 'url:'+config.BASEPATH+'shop/want/to_temp_update/'+wt_type,
			data : params,
			width : 850,
			height : 440,
			max : false,
			min : false,
			cache : false,
			lock: true,
			close:function(){
				showMode.refresh();
			}
		});
	},
	detail_automatch: function(){//删除
		$.dialog.confirm('自动匹配后将实发数量修改为申请数量，确定要自动匹配吗？', function(){
			$.ajax({
				type:"POST",
				url:config.BASEPATH+'shop/want/detail_automatch',
				data:{"number":wt_number},
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : '自动匹配成功!'});
						showMode.refresh();
					}else{
						Public.tips({type: 1, content : data.message});
					}
				}
			});
		});
    },
	approve:function(){
		commonDia = $.dialog({ 
		   	id:'approve_confirm',
		   	title:'审批页面',
		   	data:{},
		   	max: false,
		   	min: false,
		   	lock:true,
		   	width:300,
		   	height:200,
		   	drag: true,
		   	resize:false,
		   	content:'url:'+config.BASEPATH+'approve/to_confirm',
		   	fixed:false,
		   	close : function(){
		   		var ar_infos = commonDia.content.ar_infos;
		   		if(ar_infos != undefined){
		   			handle.doApprove(ar_infos);
		   		}
		   	}
	    });
	},
	doApprove:function(ar_infos){
		$("#btn-approve").attr("disabled",true);
		var params = "";
		params += "number="+wt_number;
		params += "&ar_state="+ar_infos.ar_state;
		params += "&ar_describe="+Public.encodeURI(ar_infos.ar_describe);
		$.ajax({
            type: "POST",
            url: config.BASEPATH+"shop/want/approve",
            data: params,
            cache:false,
			dataType:"json",
            success: function (data) {
            	if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : '审核成功'});
					handle.loadData(data.data);
				}else{
					Public.tips({type: 1, content : data.message});
				}
				$("#btn-approve").attr("disabled",false);
            }
        });
	},
	doSend:function(){
		var wt_sendamount = $("#wt_sendamount").val();
		if(wt_sendamount == 0){
			Public.tips({type: 2, content : "发货数量不能为0"});
			return;
		}
		$.dialog.confirm('发货后将修改库存，确定发货吗？', function(){
			$("#btn-send").attr("disabled",true);
			var params = "";
			params += "number="+wt_number;
			params += "&wt_outdp_code="+$("#wt_outdp_code").val();
			$.ajax({
	            type: "POST",
	            url: config.BASEPATH+"shop/want/send",
	            data: params,
	            cache:false,
				dataType:"json",
	            success: function (data) {
	            	if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : "发货成功"});
						handle.loadData(data.data);
					}else{
						Public.tips({type: 1, content : data.message});
					}
					$("#btn-send").attr("disabled",false);
	            }
	        });
		});
	},
	doReceive:function(){
		$.dialog.confirm('接收后将修改库存，确定接收吗？', function(){
			$("#btn-receive").attr("disabled",true);
			$("#btn-reject").attr("disabled",true);
			var params = "";
			params += "number="+wt_number;
			params += "&wt_indp_code="+$("#wt_indp_code").val();
			$.ajax({
	            type: "POST",
	            url: config.BASEPATH+"shop/want/receive",
	            data: params,
	            cache:false,
				dataType:"json",
	            success: function (data) {
	            	if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : "接收成功"});
						handle.loadData(data.data);
					}else{
						Public.tips({type: 1, content : data.message});
					}
					$("#btn-receive").attr("disabled",false);
					$("#btn-reject").attr("disabled",false);
	            }
	        });
		});
	},
	doReject:function(){
		$.dialog.confirm('确定拒收吗？', function(){
			$("#btn-receive").attr("disabled",true);
			$("#btn-reject").attr("disabled",true);
			var params = "";
			params += "number="+wt_number;
			$.ajax({
				type: "POST",
				url: config.BASEPATH+"shop/want/reject",
				data: params,
				cache:false,
				dataType:"json",
				success: function (data) {
					if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : "拒收成功"});
						handle.loadData(data.data);
					}else{
						Public.tips({type: 1, content : data.message});
					}
					$("#btn-receive").attr("disabled",false);
					$("#btn-reject").attr("disabled",false);
				}
			});
		});
	},
	doRejectConfirm:function(){
		$.dialog.confirm('确认后将修改库存，是否确认吗？', function(){
			$("#btn-reject-confirm").attr("disabled",true);
			var params = "";
			params += "number="+wt_number;
			$.ajax({
				type: "POST",
				url: config.BASEPATH+"shop/want/rejectconfirm",
				data: params,
				cache:false,
				dataType:"json",
				success: function (data) {
					if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : "确认成功"});
						handle.loadData(data.data);
					}else{
						Public.tips({type: 1, content : data.message});
					}
					$("#btn-reject-confirm").attr("disabled",false);
				}
			});
		});
	},
	doReverse:function(){
		$.dialog.confirm('确定要反审核单据吗？', function(){
			$("#btn-reverse").attr("disabled",true);
			var params = "";
			params += "number="+wt_number;
			$.ajax({
	            type: "POST",
	            url: config.BASEPATH+"batch/sell/reverse",
	            data: params,
	            cache:false,
				dataType:"json",
	            success: function (data) {
	            	if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : "反审核成功"});
						handle.loadData(data.data);
					}else{
						Public.tips({type: 1, content : data.message});
					}
					$("#btn-reverse").attr("disabled",false);
	            }
	        });
		});
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.wt_id;
		pdata.wt_number=data.wt_number;
		pdata.wt_ar_state=data.wt_ar_state;
		pdata.wt_ar_date=data.wt_ar_date;
		pdata.operate='';
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	},
	doPrint:function(){
		var CurrentMode = $("#CurrentMode").val();
		var number = $("#wt_number").val();
		$.dialog({ 
		   	id:'print_model_select',
		   	title:'单据打印模板',
		   	data:{'displayMode':CurrentMode,'number':number,'extraParams':'&isSend='+(isSend ?'1':'0')},
		   	max: false,
		   	min: false,
		   	lock:true,
		   	width:350,
		   	height:300,
		   	fixed:false,
		   	resize:false,
		   	content:'url:'+config.BASEPATH+'sys/print/to_list/6'
	    });
	},
	operFmatter :function(val, opt, row){
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		if(isSend){
			html_con += '<i class="iconfont i-hand ui-icon-pencil" title="修改">&#xe60c;</i>';
		}
		html_con += '<i class="iconfont i-hand ui-icon-image" title="图片">&#xe654;</i>';
		html_con += '</div>';
		return html_con;
	},
	operFmatter_List :function(val, opt, row){
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		if(isSend){
			html_con += '<i class="iconfont i-hand ui-icon-pencil" title="修改">&#xe60c;</i>';
			html_con += '<i class="iconfont i-hand ui-icon-trash" title="删除">&#xe60e;</i>';
		}
		html_con += '<i class="iconfont i-hand ui-icon-image" title="图片">&#xe654;</i>';
		html_con += '</div>';
		return html_con;
	},
	formatGapAmount:function(val, opt, row){
		return row.wtl_applyamount-row.wtl_sendamount;
	},
	formatApplyMoney:function(val, opt, row){
		if(row.wtl_unitprice == undefined){
			return PriceLimit.formatBySell(val);
		}
		return PriceLimit.formatBySell(row.wtl_unitprice * row.wtl_applyamount);
	},
	formatSendMoney:function(val, opt, row){
		if(row.wtl_unitprice == undefined){
			return PriceLimit.formatBySell(val);
		}
		return PriceLimit.formatBySell(row.wtl_unitprice * row.wtl_sendamount);
	},
	formatApplyCostMoney:function(val, opt, row){
		if(row.wtl_costprice == undefined){
			return PriceLimit.formatByCost(val);
		}
		return PriceLimit.formatByCost(row.wtl_costprice * row.wtl_applyamount);
	},
	formatSendCostMoney:function(val, opt, row){
		if(row.wtl_costprice == undefined){
			return PriceLimit.formatByCost(val);
		}
		return PriceLimit.formatByCost(row.wtl_costprice * row.wtl_sendamount);
	}
};

var Utils = {
	doQueryOutDepot : function(){
		commonDia = $.dialog({
			title : '选择仓库',
			content : 'url:'+config.BASEPATH+'base/depot/to_list_dialog_bymodule',
			data : {multiselect:false,module:'want'},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#wt_outdp_code").val(selected.dp_code);
					$("#outdepot_name").val(selected.dp_name);
				}
			},
			cancel:true
		});
	},
	doQueryInDepot : function(){
		commonDia = $.dialog({
			title : '选择仓库',
			content : 'url:'+config.BASEPATH+'base/depot/to_list_dialog_bymodule',
			data : {multiselect:false,module:'want'},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#wt_indp_code").val(selected.dp_code);
					$("#indepot_name").val(selected.dp_name);
				}
			},
			cancel:true
		});
	}
}


var THISPAGE = {
	init:function (){
		this.initDom();
		this.addEvent();
		this.initGrid();
		this.initEvent();
	},
	initDom:function(){
		needListRefresh = false;//列表模式
        needSizeRefresh = true;//尺码模式
        needSumRefresh = true;//汇总模式
        this.selectRow={};
        var wt_ar_state = $("#wt_ar_state").val();
        if('approve' == api.data.oper){
			$("#btn-approve").show();
		}
        if('rejectconfirm' == api.data.oper){
        	$("#btn-reject-confirm").show();
        }
        if('receive' == api.data.oper){
			$("#btn-receive").show();
			$("#btn-reject").show();
		}
        this.$_auto_match = $("#auto_match").cssCheckbox({ callback: function($_obj){
        	if($_obj.find("input")[0].checked){//选中
        		handle.detail_automatch();
        	}else{//取消
        	}
		}});
	},
	gridTotal:function(){
    	var grid=$('#grid');
		var wtl_applyamount=grid.getCol('wtl_applyamount',false,'sum');
		var wtl_sendamount=grid.getCol('wtl_sendamount',false,'sum');
    	var wtl_applymoney=grid.getCol('wtl_applymoney',false,'sum');
    	var wtl_sendmoney=grid.getCol('wtl_sendmoney',false,'sum');
    	var wtl_applycostmoney=grid.getCol('wtl_applycostmoney',false,'sum');
    	var wtl_sendcostmoney=grid.getCol('wtl_sendcostmoney',false,'sum');
    	grid.footerData('set',{
    		wtl_applyamount:wtl_applyamount,
    		wtl_sendamount:wtl_sendamount,
    		wtl_applymoney:wtl_applymoney,
    		wtl_sendmoney:wtl_sendmoney,
    		wtl_applycostmoney:wtl_applycostmoney,
    		wtl_sendcostmoney:wtl_sendcostmoney});
    	$("#wt_applyamount").val(wtl_applyamount);
    	$("#wt_sendamount").val(wtl_sendamount);
    	$("#wt_applymoney").val(PriceLimit.formatByBatch(wtl_applymoney));
    	$("#wt_sendmoney").val(PriceLimit.formatByBatch(wtl_sendmoney));
    },
    sumGridTotal:function(){
    	var grid=$('#sumGrid');
    	var wtl_applyamount=grid.getCol('wtl_applyamount',false,'sum');
		var wtl_sendamount=grid.getCol('wtl_sendamount',false,'sum');
    	var wtl_applymoney=grid.getCol('wtl_applymoney',false,'sum');
    	var wtl_sendmoney=grid.getCol('wtl_sendmoney',false,'sum');
    	var wtl_applycostmoney=grid.getCol('wtl_applycostmoney',false,'sum');
    	var wtl_sendcostmoney=grid.getCol('wtl_sendcostmoney',false,'sum');
    	grid.footerData('set',{
    		wtl_applyamount:wtl_applyamount,
    		wtl_sendamount:wtl_sendamount,
    		wtl_applymoney:wtl_applymoney,
    		wtl_sendmoney:wtl_sendmoney,
    		wtl_applycostmoney:wtl_applycostmoney,
    		wtl_sendcostmoney:wtl_sendcostmoney});
    	$("#wt_applyamount").val(wtl_applyamount);
    	$("#wt_sendamount").val(wtl_sendamount);
    	$("#wt_applymoney").val(PriceLimit.formatByBatch(wtl_applymoney));
    	$("#wt_sendmoney").val(PriceLimit.formatByBatch(wtl_sendmoney));
    },
	initGrid:function(){
		var self=this;
		var colModel = [
		    {label:'操作',name:'operate', width: 70, fixed:true, formatter: handle.operFmatter_List, sortable:false,align:'center'},
		    {label:'',name: 'wtl_pd_code', index: 'wtl_pd_code', hidden:true},
	    	{label:'商品货号',name: 'pd_no', index: 'pd_no', width: 100},
	    	{label:'商品名称',name: 'pd_name', index: 'pd_name', width: 100},
	    	{label:'颜色',name: 'cr_name', index: 'cr_name', width: 60},
	    	{label:'尺码',name: 'sz_name', index: 'sz_name', width: 60},
	    	{label:'杯型',name: 'br_name', index: 'br_name', width: 60},
	    	{label:'单位',name: 'pd_unit', index: 'pd_unit', width: 60},
	    	{label:'申请数量',name: 'wtl_applyamount', index: 'wtl_applyamount', width: 70,align:'right',sorttype: 'int'},
	    	{label:'实发数量',name: 'wtl_sendamount', index: 'wtl_sendamount', width: 70,align:'right',sorttype: 'int',editable:isSend},
	    	{label:'相差数量',name: 'subamount', index: 'subamount', width: 70,align:'right',sorttype: 'int',formatter: handle.formatGapAmount},
	    	{label:'零售价',name: 'wtl_unitprice', index: 'wtl_unitprice', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatBySell,editable:isSend},
	    	{label:'申请金额',name: 'wtl_applymoney', index: 'wtl_applymoney', width: 80,align:'right',sorttype: 'float',formatter: handle.formatApplyMoney},
	    	{label:'实发金额',name: 'wtl_sendmoney', index: 'wtl_sendmoney', width: 80,align:'right',sorttype: 'float',formatter: handle.formatSendMoney},
	    	{label:'成本价',name: 'wtl_costprice', index: 'wtl_costprice', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByCost},
	    	{label:'成本金额',name: 'wtl_applycostmoney', index: 'wtl_applycostmoney', width: 80,align:'right',sorttype: 'float',formatter: handle.formatApplyCostMoney},
	    	{label:'成本金额',name: 'wtl_sendcostmoney', index: 'wtl_sendcostmoney', width: 80,align:'right',sorttype: 'float',formatter: handle.formatSendCostMoney},
	    	{label:'备注',name: 'wtl_remark', index: 'wtl_remark', width: 180}
	    ];
		$('#grid').jqGrid({
			url:queryurl,
			loadonce:true,
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:9999,//每页条数
			cmTemplate: {sortable:true,title:true},
			shrinkToFit:false,//表格是否自动填充
			cellEdit: true,
            cellsubmit: 'clientArray',
            footerrow: true,
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'wtl_id'  //图标ID
			},
			loadComplete: function(data){
				var ids = $("#grid").getDataIDs();
				THISPAGE.gridTotal();
				if((wt_ar_state == 0 || wt_ar_state == 1 || wt_ar_state == 2) && !isSend){
					if(isFHD){
						$("#grid").setGridParam().hideCol("wtl_applyamount").hideCol("subamount").hideCol("wtl_applymoney").hideCol("wtl_applycostmoney");
					}else{
						$("#grid").setGridParam().hideCol("wtl_sendamount").hideCol("subamount").hideCol("wtl_sendmoney").hideCol("wtl_sendcostmoney");
					}
				}else{
					for(var i=0;i < ids.length;i++){
						var rowData = $("#grid").jqGrid("getRowData", ids[i]);
						if(rowData.subamount > 0){
							$("#grid").jqGrid('setRowData', ids[i], false, { color: '#FF0000' });
						}else if(rowData.subamount < 0){
							$("#grid").jqGrid('setRowData', ids[i], false, { color: 'blue' });
						}
					}
				}
			},
			loadError: function(xhr, status, error){		
			},
			formatCell:function (rowid, cellname, value, iRow, iCol){
            	self.selectRow.value=value;
				return value;
			},
			beforeSaveCell :function(rowid, cellname, value, iRow, iCol){
				if (cellname == 'wtl_sendamount' && self.selectRow.value != value){
					if(value == '' || isNaN(value) || parseInt(value) <= 0){
						return self.selectRow.value;
					} 
					if(parseInt(self.selectRow.value) == parseInt(value)){
						return self.selectRow.value;
					}
					if(parseFloat(value).toFixed(0).length > 9|| parseInt(value) >1e+9){
						return self.selectRow.value;
					}
					handle.detail_updateAmount(rowid, parseInt(value));
					return parseInt(value);
				}else if(cellname == 'wtl_unitprice' && self.selectRow.value != value){
					if(value == '' || isNaN(value) || parseFloat(value) < 0){
						return self.selectRow.value;
					}
					if(parseFloat(self.selectRow.value) == parseFloat(value)){
						return self.selectRow.value;
					}
					if(parseFloat(value).toFixed(2).length > 11 || parseFloat(value) >1e+11){
						return self.selectRow.value;
					}
					var rowData = $("#grid").jqGrid("getRowData", rowid);
					if(cellname == 'wtl_unitprice'){
						handle.detail_updatePrice(rowData.wtl_pd_code, parseFloat(value).toFixed(2));
					}
					return parseFloat(value).toFixed(2);
				}
				return value;
			},
			afterSaveCell:function(rowid, cellname, value, iRow, iCol){
				if (cellname == 'wtl_sendamount' && self.selectRow.value != value){
					var rowData = $("#grid").jqGrid("getRowData", rowid);
					rowData.wtl_sendmoney = rowData.wtl_sendamount * rowData.wtl_unitprice;
					rowData.wtl_sendcostmoney = rowData.wtl_sendamount * rowData.wtl_costprice;
					rowData.subamount = rowData.wtl_applyamount - rowData.wtl_sendamount;
					$("#grid").jqGrid('setRowData', rowid, rowData);
					THISPAGE.gridTotal();
					if(rowData.subamount > 0){
						$("#grid").jqGrid('setRowData', rowid, false, { color: '#FF0000' });
					}else if(rowData.subamount < 0){
						$("#grid").jqGrid('setRowData', rowid, false, { color: 'blue' });
					}else{
						$("#grid").jqGrid('setRowData', rowid, false, { color: '#696969' });
					}
				}
				
			}
	    });
	},
	initSumGrid:function(){
		var self=this;
		var colModel = [
		    {label:'操作',name:'operate', width: 70, fixed:true, formatter: handle.operFmatter, sortable:false,align:'center'},
	    	{label:'',name: 'wtl_pd_code', index: 'wtl_pd_code', hidden:true},
	    	{label:'商品货号',name: 'pd_no', index: 'pd_no', width: 100},
	    	{label:'商品名称',name: 'pd_name', index: 'pd_name', width: 100},
	    	{label:'品牌',name: 'bd_name', index: 'pd_bd_code', width: 80},
	    	{label:'类别',name: 'tp_name', index: 'pd_tp_code', width: 80},
	    	{label:'单位',name: 'pd_unit', index: 'pd_unit', width: 60},
	    	{label:'申请数量',name: 'wtl_applyamount', index: 'wtl_applyamount', width: 70,align:'right',sorttype: 'int'},
	    	{label:'实发数量',name: 'wtl_sendamount', index: 'wtl_sendamount', width: 70,align:'right',sorttype: 'int'},
	    	{label:'相差数量',name: 'subamount', index: 'subamount', width: 70,align:'right',sorttype: 'int',formatter: handle.formatGapAmount},
	    	{label:'零售价',name: 'wtl_unitprice', index: 'wtl_unitprice', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatBySell},
	    	{label:'申请金额',name: 'wtl_applymoney', index: 'wtl_applymoney', width: 80,align:'right',sorttype: 'float',formatter: handle.formatApplyMoney},
	    	{label:'实发金额',name: 'wtl_sendmoney', index: 'wtl_sendmoney', width: 80,align:'right',sorttype: 'float',formatter: handle.formatSendMoney},
	    	{label:'成本价',name: 'wtl_costprice', index: 'wtl_costprice', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByCost},
	    	{label:'成本金额',name: 'wtl_applycostmoney', index: 'wtl_applycostmoney', width: 80,align:'right',sorttype: 'float',formatter: handle.formatApplyCostMoney},
	    	{label:'成本金额',name: 'wtl_sendcostmoney', index: 'wtl_sendcostmoney', width: 80,align:'right',sorttype: 'float',formatter: handle.formatSendCostMoney},
	    	{label:'备注',name: 'wtl_remark', index: 'wtl_remark', width: 180}
	    ];
		$('#sumGrid').jqGrid({
			url:querysumurl,
			loadonce:true,
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#sumPage',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:9999,//每页条数
			cmTemplate: {sortable:true,title:true},
			shrinkToFit:false,//表格是否自动填充
            footerrow: true,
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'wtl_id'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.sumGridTotal();
				var ids = $("#sumGrid").getDataIDs();
				if((wt_ar_state == 0 || wt_ar_state == 1 || wt_ar_state == 2) && !isSend){
					if(isFHD){
						$("#sumGrid").setGridParam().hideCol("wtl_applyamount").hideCol("subamount").hideCol("wtl_applymoney").hideCol("wtl_applycostmoney");
					}else{
						$("#sumGrid").setGridParam().hideCol("wtl_sendamount").hideCol("subamount").hideCol("wtl_sendmoney").hideCol("wtl_sendcostmoney");
					}
				}else{
					for(var i=0;i < ids.length;i++){
						var rowData = $("#sumGrid").jqGrid("getRowData", ids[i]);
						if(rowData.subamount > 0){
							$("#sumGrid").jqGrid('setRowData', ids[i], false, { color: '#FF0000' });
						}else if(rowData.subamount < 0){
							$("#sumGrid").jqGrid('setRowData', ids[i], false, { color: 'blue' });
						}
					}
				}
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	initSizeGroupGrid:function(dyColNames, sizeCodeMap) {
		var self=this;
        var headers = [];
        var dyns = dyColNames;
        var dySize = dyns.length;
        var dms = [];
        for (var i = 0; i < dyns.length; i++) {
            var dys = dyns[i];
            for (var j = 0; j < dys.length; j++) {
                var dyss = null;
                dyss = dys[j].split('_AND_');
                if (i == 0 && j == 0) {
                    dyns[i][j] = dyss[0];
                }
                headers.push(dyss[1]);
                dms.push({label:dyss[1],name: dyss[0], index: dyss[0], width: 45, title: false, align: 'right', sorttype: 'int', sortable: false});
            }
            break;
        }
		var colModel = [
            {label:'',name: 'sizeGroupCode', hidden: true},
            {label:'',name: 'pd_code', index: 'pd_code', width: 100,hidden:true},
            {label:'',name: 'cr_code', index: 'cr_code', width: 100,hidden:true},
            {label:'',name: 'br_code', index: 'br_code', width: 100,hidden:true},
            {label:'操作',name:'operate', width: 70, fixed:true, formatter: handle.operFmatter, sortable:false,align:'center'},
            {label:'商品货号',name: 'pd_no', index: 'pd_no', width: 100},
	    	{label:'商品名称',name: 'pd_name', index: 'pd_name', width: 100},
	    	{label:'颜色',name: 'cr_name', index: 'wtl_cr_code', width: 80},
	    	{label:'杯型',name: 'br_name', index: 'wtl_br_code', width: 80},
        ];
        colModel = colModel.concat(dms);
        colModel = colModel.concat([
            {label:'数量',name: 'Amount_Total',	index:'Amount_Total',	width: 70, align: 'right', sorttype: 'int', sortable: false},
            {label:'零售价',name: 'unit_price', index: 'unit_price', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatBySell},
	    	{label:'零售金额',name: 'unit_money', index: 'unit_money', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatBySell},
	    	{label:'成本价',name: 'cost_price', index: 'cost_price', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByCost},
	    	{label:'成本金额',name: 'cost_money', index: 'cost_money', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByCost}
	    ]);
        $('#sizeGrid').jqGrid({
			url: querysizeurl,
			loadonce:true,
            datatype: 'json',
            autowidth: true,
			height: _height,
            gridview: true,
            colModel: colModel,
            rownumbers: true,//行号
            pager: '#sizePage',//分页
            pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
            viewrecords: true,
            rowNum: 9999,//每页条数
            shrinkToFit: false,//表格是否自动填充
            cellEdit: true,
            cellsubmit: 'clientArray',
            footerrow: true,
            userDataOnFooter: true,
			jsonReader: {
				root: 'data.data',
				userdata: 'data.userData',
				repeatitems : false,
				id:'id'
			},
			loadComplete: function(data) {
				
			},
			gridComplete:function(){
				Public.resizeSpecifyGrid('sizeGrid', 290 + (dyns.length * 32),2);
			},
			loadError: function(xhr, status, error) {
                Public.tips({type: 1, content : '操作失败了哦，服务可能已过期，请按F5刷新！'});
			},
			onPaging: function(pgButton) {
				
			},
            onSelectRow: function (rowId, status) {
                var sizeG = $("#sizeGrid").jqGrid("getRowData", rowId).sizeGroupCode;
                var bg = $('[sizeGCode=' + sizeG + ']').css("background");
                if (bg == 'none') {
                    return;
                }
                $("[sizeGCode]").css("background-color", '#eee');
                $('[sizeGCode=' + sizeG + ']').css({ "background": "none", "background-color": "#F8FF94"});
            }
	    });
        if (dySize >= 2) {
        	$('#sizeGrid').jqGrid('setGroupHeaders', {
                useColSpanStyle: true,
                groupHeaders:[]
            });
            $('#sizeGrid').jqGrid("setComplexGroupHeaders", {
                complexGroupHeaders: {startColumnName: dyns[0][0], numberOfColumns: dyns[0].length},
                rowspan: dySize,
                topTitle:true
            });
            for (var key in sizeCodeMap) {
                $('#sizeGrid').jqGrid("setComplexGroupHeaders",{
                    complexGroupHeaders: {startColumnName: dyns[0][0], numberOfColumns: dyns[0].length},
                    rowspan: dySize,
                    sizeGroupCode: key,
                    titleTextGroup:sizeCodeMap[key],
                    topTitle:false
                });
            }
        }else{
        	$('#sizeGrid').jqGrid('setGroupHeaders', {
                useColSpanStyle: true,
                groupHeaders: [{startColumnName: dyns[0][0], numberOfColumns: dyns[0].length, titleText: '尺码'}]
            });
        }
    },
	reloadGridData: function () {
        $("#grid").jqGrid('setGridParam', {datatype: 'json', page: 1, url: queryurl}).trigger("reloadGrid");
    },
	initEvent:function(){
		$('#btn-approve').on('click', function(e){
			e.preventDefault();
			handle.approve();
		});
		$('#btn-send').on('click', function(e){
			e.preventDefault();
			handle.doSend();
		});
		$('#btn-receive').on('click', function(e){
			e.preventDefault();
			handle.doReceive();
		});
		$('#btn-reject').on('click', function(e){
			e.preventDefault();
			handle.doReject();
		});
		$('#btn-reject-confirm').on('click', function(e){
			e.preventDefault();
			handle.doRejectConfirm();
		});
		$('#btn-reverse').on('click', function(e){
			e.preventDefault();
			handle.doReverse();
		});
		$("#btn_close").click(function(e){
			e.preventDefault();
			api.close();
		});
		//修改-列表模式
        $('#grid').on('click', '.operating .ui-icon-pencil', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
        	handle.detail_update(id, '0');
        });
        
        //删除-列表模式
		$('#grid').on('click', '.operating .ui-icon-trash', function(e) {
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.detail_del(id);
		});
		$('#grid').on('click', '.operating .ui-icon-image', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
        	viewProductImg($("#grid").jqGrid("getRowData", id).wtl_pd_code);
        });
	},
	addEvent:function(){
		//移除事件，防止重复绑定
		$('#sumGrid').off('click','.operating .ui-icon-pencil');
		$('#sizeGrid').off('click','.operating .ui-icon-pencil');
		$('#sumGrid').off('click','.operating .ui-icon-image');
		$('#sizeGrid').off('click','.operating .ui-icon-image');
		
		//修改-尺码模式
        $('#sizeGrid').on('click', '.operating .ui-icon-pencil', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
			handle.detail_update(id, '1');
        });
        //修改-汇总模式
        $('#sumGrid').on('click', '.operating .ui-icon-pencil', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
			handle.detail_update(id, '2');
        });
		$('#sizeGrid').on('click', '.operating .ui-icon-image', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
        	viewProductImg($("#sizeGrid").jqGrid("getRowData", id).pd_code);
        });
        $('#sumGrid').on('click', '.operating .ui-icon-image', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
        	viewProductImg($("#sumGrid").jqGrid("getRowData", id).wtl_pd_code);
        });
	}
};

THISPAGE.init();