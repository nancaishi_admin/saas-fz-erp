var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var isRefresh = false;
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var queryurl = config.BASEPATH+'shop/price/price_list';

var _height = $(parent).height()-210,_width = $(parent).width()-31;

var handle = {
	/*doPrint:function(){
		var CurrentMode = $("#CurrentMode").val();
		var number = $("#sp_number").val();
		$.dialog({ 
		   	id:'print_model_select',
		   	title:'单据打印模板',
		   	data:{'displayMode':CurrentMode,'number':number},
		   	max: false,
		   	min: false,
		   	lock:true,
		   	width:350,
		   	height:300,
		   	fixed:false,
		   	resize:false,
		   	content:'url:'+config.BASEPATH+'sys/print/to_list/1'
	    });
	},*/
	approve:function(){
		commonDia = $.dialog({ 
			id:'approve_confirm',
			title:'审批页面',
			data:{},
			max: false,
			min: false,
			lock:true,
			width:300,
			height:200,
			drag: true,
			resize:false,
			content:'url:'+config.BASEPATH+'approve/to_confirm',
			fixed:false,
			close : function(){
				var ar_infos = commonDia.content.ar_infos;
			   	if(ar_infos != undefined){
			   		handle.doApprove(ar_infos);
			   	}
			}
		});
	},
	doApprove:function(ar_infos){
		$("#btn-approve").attr("disabled",true);
		var params = "";
		params += "number="+$("#sp_number").val();
		params += "&ar_state="+ar_infos.ar_state;
		params += "&ar_describe="+Public.encodeURI(ar_infos.ar_describe);
		$.ajax({
            type: "POST",
            url: config.BASEPATH+"shop/price/approve",
            data: params,
            cache:false,
			dataType:"json",
            success: function (data) {
            	if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : '审核成功'});
					W.isRefresh = true;
					setTimeout("api.close()",500);
				}else{
					Public.tips({type: 1, content : data.message});
				}
				$("#btn-approve").attr("disabled",false);
            }
        });
	},
	operFmatter :function(val, opt, row){
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-image" title="图片">&#xe654;</i>';
		html_con += '</div>';
		return html_con;
	},
	formatPoint:function(val, options, row){
		if(val == null || $.trim(val) == ''){
			return "";
		}
		val = $.trim(val);
		var show="";
		if(val=="1"){
			show="是";
		}else if(val=="0"){
			show="否";
		}
		return show;
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();
	},
	initDom:function(){
		if('approve' == api.data.oper){
			$("#btn-approve").show();
		}
	},
	initGrid:function(){
		var colModel = [
			{label:'',name: 'spl_id',index: 'spl_id', fixed:true, title: false,sortable:false,hidden:true},
			{label:'',name: 'spl_shop_code', index: 'spl_shop_code',title: false,fixed:true,hidden:true},
			{label:'',name: 'spl_pd_code', index: 'spl_pd_code',title: false,fixed:true,hidden:true},
		    {label:'操作',name:'operate', width: 50, fixed:true, formatter: handle.operFmatter, sortable:false,align:'center'},
		    {label:'店铺名称',name: 'sp_name', index: 'sp_name', width: 100},
	    	{label:'商品货号',name: 'pd_no', index: 'pd_no', width: 100},
	    	{label:'商品名称',name: 'pd_name', index: 'pd_name', width: 100},
	    	{label:'是否积分',name: 'spl_pd_point', index: 'spl_pd_point',align:'center', width: 100,formatter:handle.formatPoint},
	    ];
		var sp_is_sellprice = $("#sp_is_sellprice").val();
		if(sp_is_sellprice == "1"){
			colModel = colModel.concat([{label:'原零售价',name: 'spl_old_sellprice', index: 'spl_old_sellprice',width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatBySell,title: false}]);
			colModel = colModel.concat([{label:'现零售价',name: 'spl_new_sellprice', index: 'spl_new_sellprice',width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatBySell,title: false}]);
		}
		
		var sp_is_vipprice = $("#sp_is_vipprice").val();
		if(sp_is_vipprice == "1"){
			colModel = colModel.concat([{label:'原会员价',name: 'spl_old_vipprice', index: 'spl_old_vipprice',width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByVip,title: false}]);
			colModel = colModel.concat([{label:'现会员价',name: 'spl_new_vipprice', index: 'spl_new_vipprice',width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByVip,title: false}]);
		}
		var sp_is_sortprice = $("#sp_is_sortprice").val();
		if(sp_is_sortprice == "1"){
			colModel = colModel.concat([{label:'原配送价',name: 'spl_old_sortprice', index: 'spl_old_sortprice',width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByDistribute,title: false}]);
			colModel = colModel.concat([{label:'现配送价',name: 'spl_new_sortprice', index: 'spl_new_sortprice',width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByDistribute,title: false}]);
		}
		var sp_is_costprice = $("#sp_is_costprice").val();
		if(sp_is_costprice == "1"){
			colModel = colModel.concat([{label:'原成本价',name: 'spl_old_costprice', index: 'spl_old_costprice',width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByCost,title: false}]);
			colModel = colModel.concat([{label:'现成本价',name: 'spl_new_costprice', index: 'spl_new_costprice',width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByCost,title: false}]);
		}
    	
		$('#grid').jqGrid({
			url:queryurl+"?sp_number="+$("#sp_number").val(),
			datatype: 'json',
			width: _width,
			height: _height,
			altRows:true,
			gridview: true,
			onselectrow: false,
			cellEdit: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			cellEdit: true,
			cellsubmit: 'clientArray',//可编辑列在编辑完成后会触发保存事件，clientArray表示只保存到表格不提交到服务器
			rowNum:-1,//每页条数
			cmTemplate: {sortable:true,title:true},
			shrinkToFit:false,//表格是否自动填充
			//scroll: 1,//是否滚动
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'spl_id'  //图标ID
			},
			loadComplete: function(data){
				var ids = $("#grid").getDataIDs();
			},
			formatCell:function (rowid, cellname, value, iRow, iCol){
			},
			beforeSaveCell : function (rowid, cellname, value, iRow, iCol){
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
			}
	    });
	},
	reloadGridData:function(){
	},
	initEvent:function(){
		$('#grid').on('click', '.operating .ui-icon-image', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
  			var row=$('#grid').getRowData(id);
  			viewProductImg(row.spl_pd_code);
        });
		$('#btn-approve').on('click', function(e){
			e.preventDefault();
			handle.approve();
		});
		$("#btn_close").click(function(e){
			e.preventDefault();
			W.isRefresh = false;
			api.close();
		});
	}
};

THISPAGE.init();
