var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.SHOP26;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'shop/kpipk/page';

var handle = {
	operate: function(oper, id){//修改、新增
		if(oper == 'add'){
			url = config.BASEPATH+"shop/kpipk/to_add";
			data = {oper: oper, callback:this.callback};
		}else if(oper == 'view'){
			url = config.BASEPATH+"shop/kpipk/to_view?kp_id="+id;
			data = {oper: oper,callback: this.callback};
		}else if(oper == 'approve'){
			url = config.BASEPATH+"shop/kpipk/to_view?kp_id="+id;
			data = {oper: oper,callback: this.callback};
		}else{
			return false;
		}
		$.dialog({
			title : false,
			content : 'url:'+url,
			data: data,
			max : false,
			min : false,
			cache : false,
			lock: false
		}).max();
	},
	del: function(rowId){//删除
	 	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
			$.ajax({
				type:"POST",
				url:config.BASEPATH+'shop/kpipk/del',
				data:{"kp_id":rowId},
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : '删除成功!'});
						$('#grid').jqGrid('delRowData', rowId);
					}else{
						Public.tips({type: 1, content : data.message});
					}
				}
			});
		});	
    },
    callback: function(data, oper, dialogWin){
		if(oper == "approve") {
			$("#grid").jqGrid('setRowData', data.id, data);
			dialogWin && dialogWin.api.close();
		}
		if(oper == "view") {
			$("#grid").jqGrid('setRowData', data.id, data);
			dialogWin && dialogWin.api.close();
		}
		if(oper == "add"){
			THISPAGE.reloadData();
			dialogWin && dialogWin.api.close();
		}
	},
	operFmatter : function(val, opt, row){
		var btnHtml = '';
		if (row.kp_state == '1') {
        	btnHtml += '<input type="button" value="查看" class="btn_ck" onclick="javascript:handle.operate(\'view\',' + opt.rowId + ');" />';
        }else if (row.kp_state == '0') {
        	btnHtml += '<input type="button" value="总结" class="btn_sp" onclick="javascript:handle.operate(\'approve\',' + opt.rowId + ');" />';
        }
		return btnHtml;
	},
	formatState:function(val, opt, row){
		if(val == 0){
			return '未总结';
		}else if(val == 1){
			return '已总结';
		}
		return val;
	},
	formatType:function(val, opt, row){
		if(val == 0){
			return '店铺';
		}else if(val == 1){
			return '员工组';
		}else if(val == 2){
			return '员工';
		}
		return val;
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		this.$_kp_type = $("#td_kp_type").cssRadio({ callback: function($_obj){
			$("#kp_type").val($_obj.find("input").val());
		}});
		this.$_kp_state = $("#td_kp_state").cssRadio({ callback: function($_obj){
			$("#kp_state").val($_obj.find("input").val());
		}});
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
	    	{label:'操作',name: 'operate',width: 60, formatter: handle.operFmatter,align:'center', sortable:false},
	    	{label:'',name: 'kp_us_id',index: 'kp_us_id',width:100,hidden:true},
	    	{label:'单据编号',name: 'kp_number',index: 'kp_number',width:150},
	    	{label:'考核范围',name: 'kp_type',index: 'kp_type',width:100,formatter: handle.formatType,align:'center'},
	    	{label:'考核对象',name: 'kpl_name',index: '',width:180,title:true},
	    	{label:'开始时间',name: 'kp_begin',index: 'kp_begin',width:80},
	    	{label:'结束时间',name: 'kp_end',index: 'kp_end',width:80},
	    	{label:'标准分数',name: 'kp_score',index: 'kp_score',width:80,align:'right'},
	    	{label:'奖励',name: 'reward_names',index: '',width:140},
	    	{label:'状态',name: 'kp_state',index: 'kp_state',width:80,formatter: handle.formatState,align:'center'}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'json',
			width: gridWH.w-12,
			height: gridWH.h-20,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.BASEROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:false,title:false},
			shrinkToFit:false,//表格是否自动填充
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'kp_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&kp_type='+$("#kp_type").val();
		params += '&kp_state='+$("#kp_state").val();
		params += '&kp_number='+Public.encodeURI($("#kp_number").val());
		return params;
	},
	reset:function(){
		$("#kp_number").val("");
		THISPAGE.$_date.setValue(0);
		THISPAGE.$_kp_type.setValue(0);
		THISPAGE.$_kp_state.setValue(0);
		$("#begindate").val("");
		$("#enddate").val("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		//新增
		$('#btn-add').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.INSERT)) {
				return ;
			}
			handle.operate('add');
		});
		$('#btn-del').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.DELETE)) {
				return ;
			}
			var rowId = $('#grid').jqGrid('getGridParam','selrow');
			if(rowId == null || rowId == ''){
				Public.tips({type: 1, content : "您未选择任何数据"});
				return;
			}
			var rowData =$("#grid").jqGrid("getRowData", rowId);
			if(rowData.kp_state != "未总结"){
				Public.tips({type: 1, content : "考核单据已总结，不能删除"});
				return;
			}
			handle.del(rowId)
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();