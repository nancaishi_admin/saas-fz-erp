var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;

var AllowExt=".jpg|.gif|.bmp|.png|";
var FileExt,ImgFileSize;
var ImgObj=new Image();//建立一个图像对象

var Utils = {
	doQueryShop : function(){
		var shopCodeBefore = $("#sp_shop_code").val();
		commonDia = W.$.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:true},
			width : 550,
			height : 390,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var datas = commonDia.content.doSelect();
				if($.isEmptyObject(datas)){
					return false;
				} else {
					var codes = [];
					var names = [];
					for(var i=0;i<datas.length;i++){
						codes.push(datas[i].sp_code);
						names.push(datas[i].sp_name);
					}
					$("#sp_shop_code").val(codes.join(","));
					$("#shop_name").val(names.join(","));
				}
			},
			close:function(){
			},
			cancel:true
		});
	}
}
var handle = {
	countPoint:function(obj,countId){
		var haveBean = obj.value.length;//已用字数
		document.getElementById(countId).value = 200 - haveBean;
		if(haveBean>200){
			W.Public.tips({type: 2, content : "输入字数不能超过200!"});
			obj.value = obj.value.substr(0,200);
		}
	},
	/*savePhoto:function(){
        try{
            
           	var options = {
		       	url:"<%=basePath%>office/doUploadImgPro.action",
		        type:"post",
		        timeout:15000,
				dataType:"html",
		        success:function(data){
		        	if (data=="sizeError") {
		        		$.dialog.tips("图片超出200K，请重新选择！",2,"32X32/fail.png");
		        	}else if(data=="uploadError"){
		        		$.dialog.tips("上传图片出错，请联系管理员！",2,"32X32/fail.png");
		        	} else {
		        		$.dialog.tips("上传成功!",2,"32X32/succ.png");
		        		document.getElementById("uploadDiv").style.display="none";
		        		document.getElementById("imageDiv").style.display="";
		        		document.getElementById("image").src = "<%=basePath%>"+data;
		        		document.getElementById("Cp_PicPath").value = "<%=basePath%>"+data;
		        	}
			    }
		    };
	    	$("#form1").ajaxSubmit(options);
        }catch(e){
        	W.Public.tips({type: 2, content : "文件路径异常！请检查"});
        }
	},*/
	save:function(){
		var sp_shop_code = $("#sp_shop_code").val().trim();
		if(sp_shop_code == ''){
	    	W.Public.tips({type: 2, content : "请选择店铺！"});
	        $("#shop_name").select().focus();
	        return;
	    }
		var sp_title = $("#sp_title").val().trim();
	    if(sp_title == ''){
	    	W.Public.tips({type: 2, content : "请输入方案标题!"});
	        $("#sp_title").select().focus();
	        return;
	    }else{
	    	$("#sp_title").val(sp_title);//防止空格
	    }
	    var sp_info = $("#sp_info").val();
	    if(sp_info == ''){
	    	W.Public.tips({type: 2, content : "请输入方案说明!"});
	        $("#sp_info").select().focus();
	        return;
	    }else{
	    	$("#sp_info").val(sp_info);//防止空格
	    }
	    
	    if(form1.uploadPro.value.length == 0){
        	W.Public.tips({type: 2, content : "请选择需要上传的文件!"});
           return;
        }

        FileExt=form1.uploadPro.value;
        FileExt=FileExt.substr(FileExt.lastIndexOf(".")).toLowerCase();
        if(AllowExt!=0 && AllowExt.indexOf(FileExt+"|")==-1) //判断文件类型是否允许上传
		{
        	W.Public.tips({type: 2, content : "该文件类型不允许上传,请上传"+AllowExt+"类型的文件！"});
		    return;
		}
		
		var file_upl = document.getElementById('uploadPro');
		file_upl.select();
		
        document.forms['form1'].encoding="multipart/form-data";
		$("#btn-save").attr("disabled",true);
		var options = {
            url:config.BASEPATH +"shop/program/save",
		    type:"post",
		    timeout:15000,
		    dataType:"json",
		    success:function(data){
		    	if(undefined != data && data.stat == 200){
		        	W.Public.tips({type: 3, content : data.message});
		        	setTimeout("api.zindex()",500);
					W.isRefresh = true;
					api.close();
				}else{
					W.Public.tips({type: 2, content : data.message});
				}
		    	$("#btn-save").attr("disabled",false);
			}
		};
		$("#form1").ajaxSubmit(options);
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initEvent();
	},
	initDom:function(){
		//图片预览
		$("#uploadPro").uploadPreview({ 
	    	Img: "viewImg", 
	    	Width: 180, 
	    	Height: 132,
	    	Callback:function(){
	    		document.getElementById("ImgViews").style.display="";
	    	}
	    });
	},
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save();
		});
		$("#btn_close").click(function(){
			W.isRefresh = false;
			api.close();
		});
	}
};
THISPAGE.init();