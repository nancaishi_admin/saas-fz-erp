var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var isRefresh = false;
var _limitMenu = system.MENULIMIT.SHOP02;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'shop/complaint/list';

var Utils = {
		doQueryShop : function(){
			commonDia = $.dialog({
				title : '选择店铺',
				content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
				data : {multiselect:false},
				width : 450,
				height : 370,
				max : false,
				min : false,
				cache : false,
				lock: true,
				ok:function(){
					var selected = commonDia.content.doSelect();
					if (!selected) {
						return false;
					}
				},
				close:function(){
					var selected = commonDia.content.doSelect();
					if(selected){
						$("#sc_shop_code").val(selected.sp_code);
						$("#shop_name").val(selected.sp_name);
					}
				},
				cancel:true
			});
		}
}
var handle = {
	toUpdate:function(oper, id){
		if (!Business.verifyRight(_limitMenu,_menuParam.UPDATE)) {
			return ;
		}
		var url = config.BASEPATH+"shop/complaint/to_update?sc_id="+id;
		var data = {oper: oper, callback: this.callback};
		$.dialog({
			title : '顾客投诉处理',
			content : 'url:'+url,
			data: data,
			width:520,
			height:360,
			max : false,
			min : false,
			cache : false,
			lock: true,
			close:function(){
				if(isRefresh){
					THISPAGE.reloadData();
				}
			}
		});
	},
	operFmatter : function(val, opt, row){
		var btnHtml = '';
		if (row.sc_state == '0') {
            btnHtml += '<input type="button" value="处理" class="btn_sp" onclick="javascript:handle.toUpdate(\'service\',\'' + row.sc_id + '\');" />';
        }
		return btnHtml;
	},
	sc_type : function(val, opt, row){
		if(val == '1'){
			return '产品';
		} else if(val == '2'){
			return '服务';
		} else if(val == '3'){
			return '其他';
		} else {
			return '';
		}
	},
	sc_state : function(val, opt, row){
		if(val == '0'){
			return '未处理';
		} else if(val == '1'){
			return '已处理';
		} else if(val == '2'){
			return '完成';
		} else {
			return '';
		}
	},
	sc_satis : function(val, opt, row){
		if(val == '1'){
			return '满意';
		} else if(val == '2'){
			return '一般';
		} else if(val == '3'){
			return '不满意';
		} else {
			return '';
		}
	}
}
 


var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom: function(){
		this.$_date = $('#date');
	 	this.$_date.cssRadio(
		 	{callback:
		 		function($_obj){  //$_obj是radio外面的lable
		 			dateRedioClick($_obj.find("input").attr("id"));
		 		}
		 	}
	 	);
	 	this.$_satisId = $('#satisId');
	 	this.$_satisId.cssRadio(
		 	{callback:
		 		function($_obj){  //$_obj是radio外面的lable
		 			$("#sc_satis").val($($_obj).find('input').val());
		 		}
		 	}
	 	);
	 	this.$_statusId = $('#statusId');
	 	this.$_statusId.cssRadio(
		 	{callback:
		 		function($_obj){  //$_obj是radio外面的lable
		 			$("#sc_state").val($($_obj).find('input').val());
		 		}
		 	}
	 	);
	 	this.$_typeId = $('#typeId');
	 	this.$_typeId.cssRadio(
		 	{callback:
		 		function($_obj){  //$_obj是radio外面的lable
		 			$("#sc_type").val($($_obj).find('input').val());
		 		}
		 	}
	 	);
	},
	initGrid:function (){
		var gridWH = Public.setGrid();
		var colModel = [
		    {name: 'operate', label:'操作', width: 60, fixed:true,title: false,formatter: handle.operFmatter, sortable:false},
			{name: 'sc_username',label:'顾客姓名',index:'sc_username',width: 80,align:'center', title: false,fixed:true},
			{name: 'sc_mobile',label:'联系电话',index:'sc_mobile',width: 100, title: false},
			{name: 'sc_type',label:'投诉类型',index:'sc_type',width: 60,formatter:handle.sc_type},
			{name: 'sc_content',label:'投诉内容',index:'sc_content',width: 120,title:true},
			{name: 'sc_selman',label:'被投诉人',index:'sc_selman',width: 70,title:true},
			{name: 'sc_processname',label:'处理人',index:'sc_processname',width: 80},
			{name: 'sc_acceptdate',label:'处理时间',index:'sc_acceptdate',width: 80},
			{name: 'sc_processinfo',label:'处理内容',index:'sc_processinfo',width: 120,title:true},
			{name: 'sc_state',label:'状态',index:'sc_state',width: 60, title: false,formatter:handle.sc_state},
			{name: 'sc_enddate',label:'打分日期',index:'sc_enddate',width: 80,align:'center'},
			{name: 'sc_satis',label:'顾客打分',index:'sc_satis',width: 60,align:'center',formatter:handle.sc_satis},
			{name: 'sc_shop_name',label:'店铺名称',index:'sc_shop_name',width: 100,title:true}
	    ];
	    $("#grid").jqGrid({
	    	url:queryurl,
			//loadonce:true, //只从后台加载一次数据，datatype被修改为local,如果弹出框想要查询则再开启为json
			datatype: 'json',
			width: gridWH.w-12,
			height: gridWH.h-20,
			altRows:true,
			gridview: true,
			onselectrow: false,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			multiselect:false,//多选
			viewrecords: true,
			pgbuttons:true,
			rowNum:30,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			shrinkToFit:false,//表格是否自动填充
			autoScroll: true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id:'sc_id'
			}
	    });
	},
	buildParams:function(){
		var begindate = $("#begindate").val();//开始日期		
		var enddate = $("#enddate").val();//结束日期		
		var sc_satis = $("#sc_satis").val();
		var sc_state = $("#sc_state").val();
		var sc_type = $("#sc_type").val();
		var sc_username = Public.encodeURI($("#sc_username").val());
		var sc_processname = Public.encodeURI($("#sc_processname").val());
		var sc_shop_code = $("#sc_shop_code").val();
		var params = '';
		params += 'begindate='+begindate;
		params += '&enddate='+enddate;
		params += '&sc_satis='+sc_satis;
		params += '&sc_state='+sc_state;
		params += '&sc_type='+sc_type;
		params += '&sc_username='+sc_username;
		params += '&sc_processname='+sc_processname;
		params += '&sc_shop_code='+sc_shop_code;
		return params;
	},
	reset:function(){
		$("#sc_shop_code").val("");
		$("#shop_name").val("");
		$("#sc_processname").val("");
		$("#sc_username").val("");
		$("#satis").click();
		$("#sc_satis").val("");
		$("#status").click();
		$("#sc_state").val("");
		$("#type").click();
		$("#sc_type").val("");
		$("#theDate").click();
		$("#begindate").val("");
		$("#enddate").val("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		//重置
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
};
THISPAGE.init();