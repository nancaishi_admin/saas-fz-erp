var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var isRefresh = false;
var _limitMenu = system.MENULIMIT.SHOP18;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'shop/sale/rateList';

var Utils = {
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#shop_code").val(selected.sp_code);
					$("#shop_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	},
	doQueryBrand : function(){
		commonDia = $.dialog({
			title : '选择品牌',
			content : 'url:'+config.BASEPATH+'base/brand/to_list_dialog',
			data : {multiselect:false},
			width : 320,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#bd_code").val(selected.bd_code);
					$("#bd_name").val(selected.bd_name);
				}
			},
			cancel:true
		});
	},
	doQueryType : function(){
		commonDia = $.dialog({
			title : '选择商品类别',
			content : 'url:'+config.BASEPATH+'base/type/to_tree_dialog',
			data : {multiselect:false},
			width : 280,
		   	height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#tp_code").val(selected.tp_code);
					$("#tp_name").val(selected.tp_name);
					$("#tp_upcode").val(selected.tp_upcode);
				}
			},
			cancel:true
		});
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $('#date');
	 	this.$_date.cssRadio(
		 	{callback:
		 		function($_obj){  //$_obj是radio外面的lable
		 			dateRedioClick($_obj.find("input").attr("id"));
		 		}
		 	}
	 	);
	 	dateRedioClick("theDate");
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
			{label:'店铺名称', name: 'shop_name', index: 'shop_name', width :100, title: false ,fixed: true},
			{label:'收银流水号', name: 'shl_number', index: 'shl_number', width :120, title: false ,fixed: true},
			{label:'促销编号', name: 'shl_sale_code', index: 'shl_sale_code', width: 130, title: false, fixed: true},
			{label:'收银日期', name: 'shl_date', index: 'shl_date', width: 80, title: false, fixed: true},
			{label:'商品货号', name: 'pd_no', index: 'pd_no', width: 100, title: false, fixed: true},
			{label:'商品名称', name: 'pd_name', index: 'pd_name', width :130, title: false ,fixed: true},
			{label:'类别', name: 'tp_name', index: 'tp_name', width :50, title: false ,fixed: true},
			{label:'品牌', name: 'bd_name', index: 'bd_name', width :100, title: false ,fixed: true},
			{label:'颜色', name: 'cr_name', index: 'cr_name', width: 50, title: false, fixed: true},
			{label:'尺码', name: 'sz_name', index: 'sz_name', width: 50, title: false, fixed: true},
			{label:'杯型', name: 'br_name', index: 'br_name', width: 50, title: false, fixed: true},
			{label:'数量', name: 'shl_amount', index: 'shl_amount', width :50, title: false ,fixed: true, align:'right'},
			{label:'单位', name: 'pd_unit', index: 'pd_unit', width :50, title: false ,fixed: true},
			{label:'零售价', name: 'shl_sell_price', index: 'shl_sell_price', width :60, title: false ,fixed: true, align:'right'},
			{label:'零售金额', name: 'shl_sell_money', index: 'shl_sell_money', width :60, title: false ,fixed: true, align:'right'},
			{label:'折扣价', name: 'shl_price', index: 'shl_price', width :60, title: false ,fixed: true, align:'right'},
			{label:'折扣金额', name: 'shl_money', index: 'shl_money', width :60, title: false ,fixed: true, align:'right'},
			{label:'成本', name: 'shl_cost_money', index: 'shl_cost_money', width :60, title: false ,fixed: true, align:'right'},
			{label:'利润', name: 'shl_profits', index: 'shl_profits', width :60, title: false ,fixed: true, align:'right'},
			{label:'让利金额', name: 'shl_discountmonery', index: 'shl_discountmonery', width :60, title: false ,fixed: true, align:'right'},
			{label:'导购员', name: 'main_name', index: 'main_name', width: 60, title: false, fixed: true},
			{label:'收银员', name: 'ca_name', index: 'ca_name', width: 50, title: false, fixed: true},
			{label:'消费会员', name: 'vip_name', index: 'vip_name', width: 100, title: false, fixed: true}
	    ];
		$('#grid').jqGrid({
			url:queryurl,
			//loadonce:true, //只从后台加载一次数据，datatype被修改为local,如果弹出框想要查询则再开启为json
			datatype: 'local',
			width: gridWH.w-12,
			height: gridWH.h-34,
			altRows:true,
			gridview: true,
			onselectrow: false,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			multiselect:false,//多选
			viewrecords: true,
			pgbuttons:true,
			rowNum:30,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			shrinkToFit:false,//表格是否自动填充
			autoScroll: true,
			footerrow: true,
			userDataOnFooter: true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
                userdata: 'data.data',
				repeatitems : false,
				id:'ss_id'
			},
			loadComplete: function(data){
				
			},
			loadError: function(xhr, status, error){		
				Public.tips({type: 1, content : '操作失败了哦，服务可能已过期，请按F5刷新!'});
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
			}
	    });
	},
	buildParams:function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&shop_code='+$("#shop_code").val();
		params += '&tp_code='+$("#tp_code").val();
		params += '&bd_code='+$("#bd_code").val();
		params += '&pd_no='+Public.encodeURI($("#pd_no").val());
		params += '&pd_name='+Public.encodeURI($("#pd_name").val());
		params += '&ss_code='+Public.encodeURI($("#ss_code").val());
		return params;
	},
	reset:function(){
		$("#ss_code").val("");
		$("#pd_name").val("");
		$("#pd_no").val("");
		$("#bd_code").val("");
		$("#bd_name").val("");
		$("#tp_code").val("");
		$("#tp_name").val("");
		$("#tp_upcode").val("");
		$("#shop_code").val("");
		$("#shop_name").val("");
		$("#theDate").click();
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		//重置
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();
