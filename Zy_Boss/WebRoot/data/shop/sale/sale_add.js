var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
/********************************公共部分***********************************/
function allinputChange(obj,inputName){
	var inputValue = obj.value;
	if(isNaN(inputValue)){
		Public.tips({type: 1, content: '请输入数字!'});
		$(obj).focus().select();
	    return;
	}
	$("input[name='"+inputName+"']").each(function(){   
		$(this).val(obj.value);
	});
}
function allinputPrice(obj,inputName,priceName){
	var inputValue = obj.value;
	if(isNaN(inputValue)){
		Public.tips({type: 1, content: '请输入数字!'});
		$(obj).focus().select();
	    return;
	}
	$("input[name='"+inputName+"']").each(function(index){
		var priceArr = $("input[name='"+priceName+"']");
		var price = $(priceArr[index]).val();
		if(price != undefined && price != null && price != ""){
			price = parseFloat(price);
		}else{
			price = 0;
		}
		$(this).val((obj.value*price).toFixed(2));
	});
}
function vadiNumber(obj){
	var inputValue = obj.value;
	if(isNaN(inputValue)){
		Public.tips({type: 1, content: '请输入数字!'});
		$(obj).val(0);
	    return;
	}
	if(parseFloat(inputValue)<0){
		Public.tips({type: 1, content: '请输入正数!'});
		$(obj).val(0);
	    return;
	}
	if(inputValue.indexOf(".") > -1){
		obj.value=parseFloat(inputValue).toFixed(2);
		return;
	}
}

function delTableRow(obj,tb_id,tr_index){
	$(obj).parent().parent().remove();
	var index = $("#"+tb_id+" tr").size();
	if(index != null && parseInt(index) > 0){
		index = 0;
		$("."+tr_index).each(function(){
			$(this).html(++index);
		});
	}
}
var Utils = {
	init:function (){
		this.initPriority();
	},
	initPriority:function(){
		var seasonInfo = {
			data:{items:[
					{priority_Code:'1',priority_Name:'最高'},
					{priority_Code:'2',priority_Name:'较高'},
					{priority_Code:'3',priority_Name:'一般'},
					{priority_Code:'4',priority_Name:'较低'},
					{priority_Code:'5',priority_Name:'最低'}
				]}
		}
		seasonCombo = $('#span_priority').combo({
			data:seasonInfo.data.items,
			value: 'priority_Code',
			text: 'priority_Name',
			width :190,
			height:160,
			listId:'',
			defaultSelected: 0,
			editable: true,
			callback:{
				onChange: function(data){
					$("#ss_priority").val(data.priority_Code);
				}
			}
		}).getCombo();
    },
    doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:true},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var datas = commonDia.content.doSelect();
				if($.isEmptyObject(datas)){
					return false;
				} else {
					var codes = [];
					var names = [];
					for(var i=0;i<datas.length;i++){
						codes.push(datas[i].sp_code);
						names.push(datas[i].sp_name);
					}
					$("#ss_shop_code").val(codes.join(","));
					$("#ss_sp_name").val(names.join(","));
				}
			},
			close:function(){
			},
			cancel:true
		});
	},
	doQueryMemberType : function(){
		commonDia = $.dialog({
			title : '会员类别',
			content : 'url:'+config.BASEPATH+'vip/membertype/to_list_dialog_sale',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#ss_mt_code").val(selected.mt_code);
					$("#mt_name").val(selected.mt_name);
				}
			},
			cancel:true
		});
	}
}
Utils.init();
/********************************商品分类部分***********************************/
var TYPEHTML = {
		getIndex:function(code,index){
			return "<td align='center' class='index"+code+"'>"+index+"</td>";
		},
		getCtCode:function(code,ctCode){
			return "<td>&nbsp;"+ctCode+"<input type='hidden' value='"+ctCode+"' id='sst_tp_code"+code+"' name='sst_tp_code"+code+"' style='text-align: right;'/></td>";
		},
		getCtName:function(ctName){
			return "<td>&nbsp;"+ctName+"</td>";
		},
		getDiscount:function(code){
			return "<td><input class='main_Input w100' type='text' value='1.0' id='sst_discount"+code+"' name='sst_discount"+code+"' onblur=\"vadiNumber(this)\" style='text-align: right;'/></td>";
		},
		getBuyFull:function(code){
			return "<td><input type='text' class='main_Input w100' value='0.0' id='sst_buy_full"+code+"' name='sst_buy_full"+code+"' onblur=\"vadiNumber(this)\" style='text-align: right;'/></td>";
		},
		getBuyNumber:function(code){
			return "<td><input type='text' class='main_Input  w100' value='0' id='sst_buy_number"+code+"' name='sst_buy_number"+code+"' onblur=\"vadiNumber(this)\" style='text-align: right;'/></td>";
		},
		getDonaTionAmount:function(code){
			return "<td><input class='main_Input w100' type='text' value='0.0' id='sst_donation_amount"+code+"' name='sst_donation_amount"+code+"' onblur=\"vadiNumber(this)\" style='text-align: right;'/></td>";
		},
		getReduceAmount:function(code){
			return "<td><input class='main_Input w100' type='text' value='0.0' id='sst_reduce_amount"+code+"' name='sst_reduce_amount"+code+"' onblur=\"vadiNumber(this)\" style='text-align: right;'/></td>";
		},
		getIncreasedAmount:function(code){
			return "<td><input class='main_Input w100' type='text' value='0.0' id='sst_increased_amount"+code+"' name='sst_increased_amount"+code+"' onblur=\"vadiNumber(this)\" style='text-align: right;'/></td>";
		},
		getDelete:function(code){
			return "<td valign='top'><input type='button' class='btn_sp' value='删除' onclick=\"javascript:delTableRow(this,'table"+code+"','index"+code+"');\"/></td>";
		},
		getDelAndAdd:function(code,index){
			return "<td valign='top'>" +
					"<input type='button' class='btn_sp' value='商品' onclick=\"javascript:queryStockProductInfo('0','"+code+"','"+index+"');\"/>"+
					"<input type='button' class='btn_sp' value='删除' onclick=\"javascript:delProductRow(this,'"+code+"','"+index+"',event);\"/>" +
					"</td>";
		},
		getDelAndAddGift:function(code,index){
			return "<td valign='top'>" +
					"<input type='button' class='btn_sp' value='赠品' onclick=\"javascript:queryStockProductInfo('1','"+code+"','"+index+"');\"/>"+
					"<input type='button' class='btn_sp' value='删除' onclick=\"javascript:delProductRow(this,'"+code+"','"+index+"',event);\"/>" +
					"</td>";
		}
}
//显示类型
function doQueryType(){
	commonDia = $.dialog({
		title : '选择商品类别',
		content : 'url:'+config.BASEPATH+'base/type/to_tree_dialog',
		data : {multiselect:true},
		width : 280,
	   	height : 320,
		max : false,
		min : false,
		cache : false,
		lock: true,
		ok:function(){
			var selected = commonDia.content.doSelect();
			if (!selected) {
				return false;
			}
		},
		close:function(){
			var selected = commonDia.content.doSelect();
			if(selected){
				var pd_tp_code = [];
				var pd_tp_name = [];
				for(var i=0;i<selected.length;i++){
					pd_tp_code.push(selected[i].tp_code);
					pd_tp_name.push(selected[i].tp_name);
				}
				var ss_model = $("#ss_model").val();
				var html = eval("insertRows"+ss_model+"('"+pd_tp_code.join(",")+"','"+pd_tp_name.join(",")+"','"+ss_model+"')");
				$("#table"+ss_model).append(html);
			}
		},
		cancel:true
	});
}
function insertRows121(tp_code,tp_name,code){ 
	var html = "",hadCtCode = "";
	$("input[name='sst_tp_code"+code+"']").each(function(){
		hadCtCode += $(this).val()+",";
	});
	if(tp_code != null){
		var tp_codes = tp_code.split(",");
		var tp_names = tp_name.split(",");
		var index = $("#table"+code+" tr").size();
		for(i=0;i<tp_codes.length;i++){
			if(hadCtCode != "" && hadCtCode.indexOf(tp_codes[i]) > -1){
				index --;
				continue;
			}else{
				html += "<tr>";
				html += TYPEHTML.getIndex(code, (parseInt(index)+i));
				html += TYPEHTML.getCtCode(code, tp_codes[i]);
				html += TYPEHTML.getCtName(tp_names[i])
				html += TYPEHTML.getDiscount(code);
				html += TYPEHTML.getDelete(code);
				html += "</tr>";
			}
		}
	}
	return html;
}
function insertRows122(tp_code,tp_name,code){ 
	var html = "";
	if(tp_code != null){
		var tp_codes = tp_code.split(",");
		var tp_names = tp_name.split(",");
		var index = $("#table"+code+" tr").size();
		for(i=0;i<tp_codes.length;i++){
			html += "<tr>";
			html += TYPEHTML.getIndex(code, (parseInt(index)+i));
			html += TYPEHTML.getCtCode(code, tp_codes[i]);
			html += TYPEHTML.getCtName(tp_names[i])
			html += TYPEHTML.getBuyFull(code);
			html += TYPEHTML.getDiscount(code);
			html += TYPEHTML.getDelete(code);
			html += "</tr>";
		}
	}
	return html;
}
function insertRows123(tp_code,tp_name,code){ 
	var html = "";
	if(tp_code != null){
		var tp_codes = tp_code.split(",");
		var tp_names = tp_name.split(",");
		var index = $("#table"+code+" tr").size();
		for(i=0;i<tp_codes.length;i++){
			html += "<tr>";
			html += TYPEHTML.getIndex(code, (parseInt(index)+i));
			html += TYPEHTML.getCtCode(code, tp_codes[i]);
			html += TYPEHTML.getCtName(tp_names[i])
			html += TYPEHTML.getBuyNumber(code);
			html += TYPEHTML.getDiscount(code);
			html += TYPEHTML.getDelete(code);
			html += "</tr>";
		}
	}
	return html;
}

/*function insertRows321(ctCode,ctName,code){ 
	var html = "";
	if(ctCode != null && ctCode.indexOf(",") > -1){
		var ct_Codes = ctCode.split(",");
		var ct_Names = ctName.split(",");
		var index = $("#table"+code+" tr").size();
		for(i=0;i<ct_Codes.length-1;i++){
			html += "<tr>";
			html += TYPEHTML.getIndex(code, (parseInt(index)+i));
			html += TYPEHTML.getCtCode(code, ct_Codes[i]);
			html += TYPEHTML.getCtName(ct_Names[i])
			html += TYPEHTML.getBuyFull(code);
			html += TYPEHTML.getDonaTionAmount(code);
			html += TYPEHTML.getDelete(code);
			html += "</tr>";
		}
	}
	return html;
}*/

function insertRows322(tp_code,tp_name,code){ 
	var html = "";
	if(tp_code != null){
		var tp_codes = tp_code.split(",");
		var tp_names = tp_name.split(",");
		var index = $("#table"+code+" tr").size();
		for(i=0;i<tp_codes.length;i++){
			html += "<tr>";
			html += TYPEHTML.getIndex(code, (parseInt(index)+i));
			html += TYPEHTML.getCtCode(code, tp_codes[i]);
			html += TYPEHTML.getCtName(tp_names[i]);
			html += TYPEHTML.getBuyFull(code);
			html += TYPEHTML.getReduceAmount(code);
			html += TYPEHTML.getDelete(code);
			html += "</tr>";
		}
	}
	return html;
} 

function insertRows323(tp_code,tp_name,code){ 
	var html = "";
	if(tp_code != null){
		var tp_codes = tp_code.split(",");
		var tp_names = tp_name.split(",");
		var index = $("#table"+code+" tr").size();
		$(".black-tr").each(function(){
			$(this).attr("class","write-tr");
		});
		for(i=0;i<tp_codes.length;i++){
			var style = "write-tr";
			if(i === 0){
				style = "black-tr";
			}
			html += "<tr class='"+style+"' onclick=\"javascript:doChangeIndex(this,'"+code+"','"+(parseInt(index)+i)+"');\">";
			html += TYPEHTML.getIndex(code, (parseInt(index)+i));
			html += TYPEHTML.getCtCode(code, tp_codes[i]);
			html += TYPEHTML.getCtName(tp_names[i]);
			html += TYPEHTML.getBuyFull(code);
			html += TYPEHTML.getIncreasedAmount(code);
			html += TYPEHTML.getDelAndAdd(code,(parseInt(index)+i));
			html += "</tr>";
		}
		$(".com-product").each(function(){
			$(this).hide();
		});
		$(".product-"+code+"-"+index).show();
	}
	return html;
} 
function insertRows324(tp_code,tp_name,code){ 
	var html = "";
	if(tp_code != null){
		var tp_codes = tp_code.split(",");
		var tp_names = tp_name.split(",");
		var index = $("#table"+code+" tr").size();
		$(".black-tr").each(function(){
			$(this).attr("class","write-tr");
		});
		for(i=0;i<tp_codes.length;i++){
			var style = "write-tr";
			if(i === 0){
				style = "black-tr";
			}
			html += "<tr class='"+style+"' onclick=\"javascript:doChangeIndex(this,'"+code+"','"+(parseInt(index)+i)+"');\">";
			html += TYPEHTML.getIndex(code, (parseInt(index)+i));
			html += TYPEHTML.getCtCode(code, tp_codes[i]);
			html += TYPEHTML.getCtName(tp_names[i]);
			html += TYPEHTML.getBuyNumber(code);
			html += TYPEHTML.getDelAndAddGift(code,(parseInt(index)+i));
			html += "</tr>";
		}
		$(".com-product").each(function(){
			$(this).hide();
		});
		$(".product-"+code+"-"+index).show();
	}
	return html;
} 
function insertRows325(tp_code,tp_name,code){ 
	var html = "";
	if(tp_code != null){
		var tp_codes = tp_code.split(",");
		var tp_names = tp_name.split(",");
		var index = $("#table"+code+" tr").size();
		$(".black-tr").each(function(){
			$(this).attr("class","write-tr");
		});
		for(i=0;i<tp_codes.length;i++){
			var style = "write-tr";
			if(i === 0){
				style = "black-tr";
			}
			html += "<tr class='"+style+"' onclick=\"javascript:doChangeIndex(this,'"+code+"','"+(parseInt(index)+i)+"');\">";
			html += TYPEHTML.getIndex(code, (parseInt(index)+i));
			html += TYPEHTML.getCtCode(code, tp_codes[i]);
			html += TYPEHTML.getCtName(tp_names[i]);
			html += TYPEHTML.getBuyFull(code);
			html += TYPEHTML.getDelAndAddGift(code,(parseInt(index)+i));
			html += "</tr>";
		}
		$(".com-product").each(function(){
			$(this).hide();
		});
		$(".product-"+code+"-"+index).show();
	}
	return html;
} 
/********************************商品品牌部分***********************************/
var BRANDHTML = {
		getIndex:function(code,index){
			return "<td align='center' class='index"+code+"'>"+index+"</td>";
		},
		getCode:function(code,bdCode){
			return "<td>&nbsp;"+bdCode+"<input type='hidden' value='"+bdCode+"' id='ssb_bd_code"+code+"' name='ssb_bd_code"+code+"' style='text-align: right;'/></td>";
		},
		getName:function(bdName){
			return "<td>&nbsp;"+bdName+"</td>";
		},
		getDiscount:function(code){
			return "<td><input class='main_Input w100' type='text' value='1.0' id='ssb_discount"+code+"' name='ssb_discount"+code+"' onblur=\"vadiNumber(this)\" style='text-align: right;'/></td>";
		},
		getBuyFull:function(code){
			return "<td><input type='text' class='main_Input w100' value='0.0' id='ssb_buy_full"+code+"' name='ssb_buy_full"+code+"' onblur=\"vadiNumber(this)\" style='text-align: right;'/></td>";
		},
		getBuyNumber:function(code){
			return "<td><input type='text' class='main_Input  w100' value='0' id='ssb_buy_number"+code+"' name='ssb_buy_number"+code+"' onblur=\"vadiNumber(this)\" style='text-align: right;'/></td>";
		},
		getDonaTionAmount:function(code){
			return "<td><input class='main_Input w100' type='text' value='0.0' id='ssb_donation_amount"+code+"' name='ssb_donation_amount"+code+"' onblur=\"vadiNumber(this)\" style='text-align: right;'/></td>";
		},
		getReduceAmount:function(code){
			return "<td><input class='main_Input w100' type='text' value='0.0' id='ssb_reduce_amount"+code+"' name='ssb_reduce_amount"+code+"' onblur=\"vadiNumber(this)\" style='text-align: right;'/></td>";
		},
		getIncreasedAmount:function(code){
			return "<td><input class='main_Input w100' type='text' value='0.0' id='ssb_increased_amount"+code+"' name='ssb_increased_amount"+code+"' onblur=\"vadiNumber(this)\" style='text-align: right;'/></td>";
		},
		getDelete:function(code){
			return "<td valign='top'><input type='button' class='btn_sp' value='删除' onclick=\"javascript:delTableRow(this,'table"+code+"','index"+code+"');\"/></td>";
		},
		getDelAndAdd:function(code,index){
			return "<td valign='top'>" +
					"<input type='button' class='btn_sp' value='商品' onclick=\"javascript:queryStockProductInfo('0','"+code+"','"+index+"');\"/>"+
					"<input type='button' class='btn_sp' value='删除' onclick=\"javascript:delProductRow(this,'"+code+"','"+index+"',event);\"/>" +
					"</td>";
		},
		getDelAndAddGift:function(code,index){
			return "<td valign='top'>" +
					"<input type='button' class='btn_sp' value='赠品' onclick=\"javascript:queryStockProductInfo('1','"+code+"','"+index+"');\"/>"+
					"<input type='button' class='btn_sp' value='删除' onclick=\"javascript:delProductRow(this,'"+code+"','"+index+"',event);\"/>" +
					"</td>";
		}
}
function doQueryBrand(){
	commonDia = $.dialog({
		title : '选择品牌',
		content : 'url:'+config.BASEPATH+'base/brand/to_list_dialog',
		data : {multiselect:true},
		width : 320,
		height : 370,
		max : false,
		min : false,
		cache : false,
		lock: true,
		ok:function(){
			var selected = commonDia.content.doSelect();
			if (!selected) {
				return false;
			}
		},
		close:function(){
			var selected = commonDia.content.doSelect();
			if(selected){
				var bd_codes = [];
				var bd_names = [];
				for(var i=0;i<selected.length;i++){
					bd_codes.push(selected[i].bd_code);
					bd_names.push(selected[i].bd_name);
				}
				var ss_model = $("#ss_model").val();
				var html = eval("insertRows"+ss_model+"('"+bd_codes.join(",")+"','"+bd_names.join(",")+"','"+ss_model+"')");
				$("#table"+ss_model).append(html);
			}
		},
		cancel:true
	});
}
function insertRows131(bd_code,bd_name,code){ 
	var html = "",hadBdCode="";
	$("input[name='SB_Bd_Code"+code+"']").each(function(){
		hadBdCode += $(this).val()+",";
	});
	if(bd_code != null){
		var bd_Codes = bd_code.split(",");
		var bd_Names = bd_name.split(",");
		var index = $("#table"+code+" tr").size();
		for(i=0;i<bd_Codes.length;i++){
			if(hadBdCode != "" && hadBdCode.indexOf(bd_Codes[i]) > -1){
				index --;
				continue;
			}else{
				html += "<tr>";
				html += BRANDHTML.getIndex(code, (parseInt(index)+i));
				html += BRANDHTML.getCode(code, bd_Codes[i]);
				html += BRANDHTML.getName(bd_Names[i])
				html += BRANDHTML.getDiscount(code);
				html += BRANDHTML.getDelete(code);
				html += "</tr>";
			}
		}
	}
	return html;
} 

function insertRows132(bd_code,bd_name,code){ 
	var html = "";
	if(bd_code != null){
		var bd_Codes = bd_code.split(",");
		var bd_Names = bd_name.split(",");
		var index = $("#table"+code+" tr").size();
		for(i=0;i<bd_Codes.length;i++){
			html += "<tr>";
			html += BRANDHTML.getIndex(code, (parseInt(index)+i));
			html += BRANDHTML.getCode(code, bd_Codes[i]);
			html += BRANDHTML.getName(bd_Names[i]);
			html += BRANDHTML.getBuyFull(code);
			html += BRANDHTML.getDiscount(code);
			html += BRANDHTML.getDelete(code);
			html += "</tr>";
		}
	}
	return html;
} 


function insertRows133(bd_code,bd_name,code){ 
	var html = "";
	if(bd_code != null){
		var bd_Codes = bd_code.split(",");
		var bd_Names = bd_name.split(",");
		var index = $("#table"+code+" tr").size();
		for(i=0;i<bd_Codes.length;i++){
			html += "<tr>";
			html += BRANDHTML.getIndex(code, (parseInt(index)+i));
			html += BRANDHTML.getCode(code, bd_Codes[i]);
			html += BRANDHTML.getName(bd_Names[i]);
			html += BRANDHTML.getBuyNumber(code);
			html += BRANDHTML.getDiscount(code);
			html += BRANDHTML.getDelete(code);
			html += "</tr>";
		}
	}
	return html;
} 
/*function insertRows331(bd_code,bd_name,code){ 
	var html = "";
	if(bd_code != null && bd_code.indexOf(",") > -1){
		var bd_Codes = bd_code.split(",");
		var bd_Names = bd_name.split(",");
		var index = $("#table"+code+" tr").size();
		for(i=0;i<bd_Codes.length-1;i++){
			html += "<tr>";
			html += BRANDHTML.getIndex(code, (parseInt(index)+i));
			html += BRANDHTML.getCode(code, bd_Codes[i]);
			html += BRANDHTML.getName(bd_Names[i]);
			html += BRANDHTML.getBuyFull(code);
			html += BRANDHTML.getDonaTionAmount(code);
			html += BRANDHTML.getDelete(code);
			html += "</tr>";
		}
	}
	return html;
} */
function insertRows332(bd_code,bd_name,code){ 
	var html = "";
	if(bd_code != null){
		var bd_Codes = bd_code.split(",");
		var bd_Names = bd_name.split(",");
		var index = $("#table"+code+" tr").size();
		for(i=0;i<bd_Codes.length;i++){
			html += "<tr>";
			html += BRANDHTML.getIndex(code, (parseInt(index)+i));
			html += BRANDHTML.getCode(code, bd_Codes[i]);
			html += BRANDHTML.getName(bd_Names[i]);
			html += BRANDHTML.getBuyFull(code);
			html += BRANDHTML.getReduceAmount(code);
			html += BRANDHTML.getDelete(code);
			html += "</tr>";
		}
	}
	return html;
} 
function insertRows333(bd_code,bd_name,code){ 
	var html = "";
	if(bd_code != null){
		var bd_Codes = bd_code.split(",");
		var bd_Names = bd_name.split(",");
		var index = $("#table"+code+" tr").size();
		$(".black-tr").each(function(){
			$(this).attr("class","write-tr");
		});
		for(i=0;i<bd_Codes.length;i++){
			var style = "write-tr";
			if(i === 0){
				style = "black-tr";
			}
			html += "<tr class='"+style+"' onclick=\"javascript:doChangeIndex(this,'"+code+"','"+(parseInt(index)+i)+"');\">";
			html += BRANDHTML.getIndex(code, (parseInt(index)+i));
			html += BRANDHTML.getCode(code, bd_Codes[i]);
			html += BRANDHTML.getName(bd_Names[i]);
			html += BRANDHTML.getBuyFull(code);
			html += BRANDHTML.getIncreasedAmount(code);
			html += BRANDHTML.getDelAndAdd(code,(parseInt(index)+i));
			html += "</tr>";
		}
		$(".com-product").each(function(){
			$(this).hide();
		});
		$(".product-"+code+"-"+index).show();
	}
	return html;
} 

function insertRows334(bd_code,bd_name,code){ 
	var html = "";
	if(bd_code != null){
		var bd_Codes = bd_code.split(",");
		var bd_Names = bd_name.split(",");
		var index = $("#table"+code+" tr").size();
		$(".black-tr").each(function(){
			$(this).attr("class","write-tr");
		});
		for(i=0;i<bd_Codes.length;i++){
			var style = "write-tr";
			if(i === 0){
				style = "black-tr";
			}
			html += "<tr class='"+style+"' onclick=\"javascript:doChangeIndex(this,'"+code+"','"+(parseInt(index)+i)+"');\">";
			html += BRANDHTML.getIndex(code, (parseInt(index)+i));
			html += BRANDHTML.getCode(code, bd_Codes[i]);
			html += BRANDHTML.getName(bd_Names[i]);
			html += BRANDHTML.getBuyNumber(code);
			html += BRANDHTML.getDelAndAddGift(code, (parseInt(index)+i));
			html += "</tr>";
		}
		$(".com-product").each(function(){
			$(this).hide();
		});
		$(".product-"+code+"-"+index).show();
	}
	return html;
} 
function insertRows335(bd_code,bd_name,code){ 
	var html = "";
	if(bd_code != null){
		var bd_Codes = bd_code.split(",");
		var bd_Names = bd_name.split(",");
		var index = $("#table"+code+" tr").size();
		$(".black-tr").each(function(){
			$(this).attr("class","write-tr");
		});
		for(i=0;i<bd_Codes.length;i++){
			var style = "write-tr";
			if(i === 0){
				style = "black-tr";
			}
			html += "<tr class='"+style+"' onclick=\"javascript:doChangeIndex(this,'"+code+"','"+(parseInt(index)+i)+"');\">";
			html += BRANDHTML.getIndex(code, (parseInt(index)+i));
			html += BRANDHTML.getCode(code, bd_Codes[i]);
			html += BRANDHTML.getName(bd_Names[i]);
			html += BRANDHTML.getBuyFull(code);
			html += BRANDHTML.getDelAndAddGift(code, (parseInt(index)+i));
			html += "</tr>";
		}
		$(".com-product").each(function(){
			$(this).hide();
		});
		$(".product-"+code+"-"+index).show();
	}
	return html;
} 
/*********************************商品选择***************************************/
var PRODUCTHTML = {
		getIndex:function(code,index){
			return "<td align='center' class='index"+code+"'>"+index+"</td>";
		},
		getCode:function(code,piNo,piCode){
			return "<td>&nbsp;"+Public.subLenString(piNo)+"<input type='hidden' value='"+piCode+"' id='ssp_pd_code"+code+"' name='ssp_pd_code"+code+"' style='text-align: right;'/></td>";
		},
		getName:function(piName){
			return "<td>&nbsp;"+Public.subLenString(piName)+"</td>";
		},
		getRetailPrice:function(code,price){
			return "<td>&nbsp;"+price+"<input type='hidden' id='RetailPrice"+code+"' name='RetailPrice"+code+"' value='"+price+"'/></td>";
		},
		getDiscount:function(code){
			return "<td><input class='main_Input w100' type='text' value='1.0' id='ssp_discount"+code+"' name='ssp_discount"+code+"' onblur=\"vadiNumber(this)\" style='text-align: right;'/></td>";
		},
		getBuyFull:function(code){
			return "<td><input type='text' class='main_Input w100' value='0.0' id='ssp_buy_full"+code+"' name='ssp_buy_full"+code+"' onblur=\"vadiNumber(this)\" style='text-align: right;'/></td>";
		},
		getBuyNumber:function(code){
			return "<td><input type='text' class='main_Input  w100' value='0' id='ssp_buy_number"+code+"' name='ssp_buy_number"+code+"' onblur=\"vadiNumber(this)\" style='text-align: right;'/></td>";
		},
		getDonaTionAmount:function(code){
			return "<td><input class='main_Input w100' type='text' value='0.0' id='ssp_donation_amount"+code+"' name='ssp_donation_amount"+code+"' onblur=\"vadiNumber(this)\" style='text-align: right;'/></td>";
		},
		getReduceAmount:function(code){
			return "<td><input class='main_Input w100' type='text' value='0.0' id='ssp_reduce_amount"+code+"' name='ssp_reduce_amount"+code+"' onblur=\"vadiNumber(this)\" style='text-align: right;'/></td>";
		},
		getIncreasedAmount:function(code){
			return "<td><input class='main_Input w100' type='text' value='0.0' id='ssp_increased_amount"+code+"' name='ssp_increased_amount"+code+"' onblur=\"vadiNumber(this)\" onblur=\"vadiNumber(this)\" style='text-align: right;'/></td>";
		},
		getSpecialOffer:function(code){
			return "<td><input type='text' class='main_Input  w100' value='0.0' id='ssp_special_offer"+code+"' name='ssp_special_offer"+code+"' onblur=\"vadiNumber(this)\" style='text-align: right;'/></td>";
		},
		getDelete:function(code){
			return "<td valign='top'><input type='button' class='btn_sp' value='删除' onclick=\"javascript:delTableRow(this,'table"+code+"','index"+code+"');\"/></td>";
		},
		getDelAndAdd:function(code,index){
			return "<td valign='top'>" +
					"<input type='button' class='btn_sp' value='商品' onclick=\"javascript:queryStockProductInfo('0','"+code+"','"+index+"');\"/>"+
					"<input type='button' class='btn_sp' value='删除' onclick=\"javascript:delProductRow(this,'"+code+"','"+index+"',event);\"/>" +
					"</td>";
		},
		getDelAndAddGift:function(code,index){
			return "<td valign='top'>" +
					"<input type='button' class='btn_sp' value='赠品' onclick=\"javascript:queryStockProductInfo('1','"+code+"','"+index+"');\"/>"+
					"<input type='button' class='btn_sp' value='删除' onclick=\"javascript:delProductRow(this,'"+code+"','"+index+"',event);\"/>" +
					"</td>";
		}
}
//查询商品
function queryProductInfo(){
	commonDia = $.dialog({
		title : '选择商品',
		content : 'url:'+config.BASEPATH+'base/product/to_list_dialog_sale',
		data : {multiselect:true},
		width : 650,
		height : 320,
		max : false,
		min : false,
		cache : false,
		lock: true,
		ok:function(){
			var selected = commonDia.content.doSelect();
			if (!selected) {
				return false;
			}
		},
		close:function(){
			var selected = commonDia.content.doSelect();
			if(selected){
				var pd_codes = [];
				var pd_nos = [];
				var pd_names = [];
				var pd_sell_prices = [];
				for(var i=0;i<selected.length;i++){
					pd_codes.push(selected[i].pd_code);
					pd_nos.push(selected[i].pd_no);
					pd_names.push(selected[i].pd_name);
					pd_sell_prices.push(selected[i].pd_sell_price);
				}
				var ss_model = $("#ss_model").val();
				var html = eval("insertRows"+ss_model+"('"+pd_codes.join(",")+"','"+pd_nos.join(",")+"','"+pd_names.join(",")+"','"+pd_sell_prices.join(",")+"','"+ss_model+"')");
		   		$("#table"+ss_model).append(html);
			}
		},
		cancel:true
	});
}

function insertRows141(pi_code,pi_no,pi_name,retailPrice,code){ 
	var html = "",hadPiCode = "";
	$("input[name='ssp_pd_code"+code+"']").each(function(){
		hadPiCode += $(this).val()+",";
	});
	if(pi_code != null){
		var pi_codes = pi_code.split(",");
		var pi_nos = pi_no.split(",");
		var pi_names = pi_name.split(",");
		var pi_retailPrices = retailPrice.split(",");
		var index = $("#table"+code+" tr").size();
		for(i=0;i<pi_codes.length;i++){
			if(hadPiCode != "" && hadPiCode.indexOf(pi_codes[i]) > -1){
				index --;
				continue;
			}else{
				html += "<tr>";
				html += PRODUCTHTML.getIndex(code, (parseInt(index)+i));
				html += PRODUCTHTML.getCode(code, pi_nos[i], pi_codes[i]);
				html += PRODUCTHTML.getName(pi_names[i]);
				html += PRODUCTHTML.getRetailPrice(code,pi_retailPrices[i]);
				html += PRODUCTHTML.getDiscount(code);
				html += PRODUCTHTML.getDelete(code);
				html += "</tr>";
			}
		}
	}
	return html;
} 
function insertRows142(pi_code,pi_no,pi_name,retailPrice,code){ 
	var html = "";
	if(pi_code != null){
		var pi_codes = pi_code.split(",");
		var pi_nos = pi_no.split(",");
		var pi_names = pi_name.split(",");
		var pi_retailPrices = retailPrice.split(",");
		var index = $("#table"+code+" tr").size();
		for(i=0;i<pi_codes.length;i++){
			html += "<tr>";
			html += PRODUCTHTML.getIndex(code, (parseInt(index)+i));
			html += PRODUCTHTML.getCode(code, pi_nos[i], pi_codes[i]);
			html += PRODUCTHTML.getName(pi_names[i]);
			html += PRODUCTHTML.getRetailPrice(code,pi_retailPrices[i]);
			html += PRODUCTHTML.getBuyFull(code);
			html += PRODUCTHTML.getDiscount(code);
			html += PRODUCTHTML.getDelete(code);
			html += "</tr>";
		}
	}
	return html;
} 
function insertRows143(pi_code,pi_no,pi_name,retailPrice,code){ 
	var html = "";
	if(pi_code != null){
		var pi_codes = pi_code.split(",");
		var pi_nos = pi_no.split(",");
		var pi_names = pi_name.split(",");
		var pi_retailPrices = retailPrice.split(",");
		var index = $("#table"+code+" tr").size();
		for(i=0;i<pi_nos.length;i++){
			html += "<tr>";
			html += PRODUCTHTML.getIndex(code, (parseInt(index)+i));
			html += PRODUCTHTML.getCode(code, pi_nos[i], pi_codes[i]);
			html += PRODUCTHTML.getName(pi_names[i]);
			html += PRODUCTHTML.getRetailPrice(code,pi_retailPrices[i]);
			html += PRODUCTHTML.getBuyNumber(code);
			html += PRODUCTHTML.getDiscount(code);
			html += PRODUCTHTML.getDelete(code);
			html += "</tr>";
		}
	}
	return html;
} 
function insertRows241(pi_code,pi_no,pi_name,retailPrice,code){ 
	var html = "",hadPiCode = "";
	$("input[name='ssp_pd_code"+code+"']").each(function(){
		hadPiCode += $(this).val()+",";
	});
	if(pi_code != null){
		var pi_codes = pi_code.split(",");
		var pi_nos = pi_no.split(",");
		var pi_names = pi_name.split(",");
		var pi_retailPrices = retailPrice.split(",");
		var index = $("#table"+code+" tr").size();
		for(i=0;i<pi_codes.length;i++){
			if(hadPiCode != "" && hadPiCode.indexOf(pi_codes[i]) > -1){
				index --;
				continue;
			}else{
				html += "<tr>";
				html += PRODUCTHTML.getIndex(code, (parseInt(index)+i));
				html += PRODUCTHTML.getCode(code, pi_nos[i], pi_codes[i]);
				html += PRODUCTHTML.getName(pi_names[i]);
				html += PRODUCTHTML.getRetailPrice(code,pi_retailPrices[i]);
				html += PRODUCTHTML.getSpecialOffer(code);
				html += PRODUCTHTML.getDelete(code);
				html += "</tr>";
			}
		}
	}
	return html;
} 
function insertRows242(pi_code,pi_no,pi_name,retailPrice,code){ 
	var html = "";
	if(pi_code != null){
		var pi_codes = pi_code.split(",");
		var pi_nos = pi_no.split(",");
		var pi_names = pi_name.split(",");
		var pi_retailPrices = retailPrice.split(",");
		var index = $("#table"+code+" tr").size();
		for(i=0;i<pi_codes.length;i++){
			html += "<tr>";
			html += PRODUCTHTML.getIndex(code, (parseInt(index)+i));
			html += PRODUCTHTML.getCode(code, pi_nos[i], pi_codes[i]);
			html += PRODUCTHTML.getName(pi_names[i]);
			html += PRODUCTHTML.getRetailPrice(code,pi_retailPrices[i]);
			html += PRODUCTHTML.getBuyNumber(code);
			html += PRODUCTHTML.getSpecialOffer(code);
			html += PRODUCTHTML.getDelete(code);
			html += "</tr>";
		}
	}
	return html;
} 
/*function insertRows341(pi_no,pi_name,retailPrice,code){ 
	var html = "";
	if(pi_no != null && pi_no.indexOf(",") > -1){
		var pi_nos = pi_no.split(",");
		var pi_names = pi_name.split(",");
		var pi_retailPrices = retailPrice.split(",");
		var index = $("#table"+code+" tr").size();
		for(i=0;i<pi_nos.length-1;i++){
			html += "<tr>";
			html += PRODUCTHTML.getIndex(code, (parseInt(index)+i));
			html += PRODUCTHTML.getCode(code, pi_nos[i]);
			html += PRODUCTHTML.getName(pi_names[i]);
			html += PRODUCTHTML.getRetailPrice(code,pi_retailPrices[i]);
			html += PRODUCTHTML.getBuyFull(code);
			html += PRODUCTHTML.getDonaTionAmount(code);
			html += PRODUCTHTML.getDelete(code);
			html += "</tr>";
		}
	}
	return html;
} */
function insertRows342(pi_code,pi_no,pi_name,retailPrice,code){ 
	var html = "";
	if(pi_code != null){
		var pi_codes = pi_code.split(",");
		var pi_nos = pi_no.split(",");
		var pi_names = pi_name.split(",");
		var pi_retailPrices = retailPrice.split(",");
		var index = $("#table"+code+" tr").size();
		for(i=0;i<pi_codes.length;i++){
			html += "<tr>";
			html += PRODUCTHTML.getIndex(code, (parseInt(index)+i));
			html += PRODUCTHTML.getCode(code, pi_nos[i], pi_codes[i]);
			html += PRODUCTHTML.getName(pi_names[i]);
			html += PRODUCTHTML.getRetailPrice(code,pi_retailPrices[i]);
			html += PRODUCTHTML.getBuyFull(code);
			html += PRODUCTHTML.getReduceAmount(code);
			html += PRODUCTHTML.getDelete(code);
			html += "</tr>";
		}
	}
	return html;
} 
function insertRows343(pi_code,pi_no,pi_name,retailPrice,code){ 
	var html = "";
	if(pi_code != null){
		var pi_codes = pi_code.split(",");
		var pi_nos = pi_no.split(",");
		var pi_names = pi_name.split(",");
		var pi_retailPrices = retailPrice.split(",");
		var index = $("#table"+code+" tr").size();
		$(".black-tr").each(function(){
			$(this).attr("class","write-tr");
		});
		for(i=0;i<pi_codes.length;i++){
			var style = "write-tr";
			if(i === 0){
				style = "black-tr";
			}
			html += "<tr class='"+style+"' onclick=\"javascript:doChangeIndex(this,'"+code+"','"+(parseInt(index)+i)+"');\">";
			html += PRODUCTHTML.getIndex(code, (parseInt(index)+i));
			html += PRODUCTHTML.getCode(code, pi_nos[i], pi_codes[i]);
			html += PRODUCTHTML.getName(pi_names[i]);
			html += PRODUCTHTML.getRetailPrice(code,pi_retailPrices[i]);
			html += PRODUCTHTML.getBuyFull(code);
			html += PRODUCTHTML.getIncreasedAmount(code);
			html += PRODUCTHTML.getDelAndAdd(code,(parseInt(index)+i));
			html += "</tr>";
		}
		$(".com-product").each(function(){
			$(this).hide();
		});
		$(".product-"+code+"-"+index).show();
	}
	return html;
} 

function insertRows344(pi_code,pi_no,pi_name,retailPrice,code){ 
	var html = "";
	if(pi_no != null){
		var pi_codes = pi_code.split(",");
		var pi_nos = pi_no.split(",");
		var pi_names = pi_name.split(",");
		var pi_retailPrices = retailPrice.split(",");
		var index = $("#table"+code+" tr").size();
		$(".black-tr").each(function(){
			$(this).attr("class","write-tr");
		});
		for(i=0;i<pi_codes.length;i++){
			var style = "write-tr";
			if(i === 0){
				style = "black-tr";
			}
			html += "<tr class='"+style+"' onclick=\"javascript:doChangeIndex(this,'"+code+"','"+(parseInt(index)+i)+"');\">";
			html += PRODUCTHTML.getIndex(code, (parseInt(index)+i));
			html += PRODUCTHTML.getCode(code, pi_nos[i], pi_codes[i]);
			html += PRODUCTHTML.getName(pi_names[i]);
			html += PRODUCTHTML.getRetailPrice(code,pi_retailPrices[i]);
			html += PRODUCTHTML.getBuyNumber(code);
			html += PRODUCTHTML.getDelAndAddGift(code, (parseInt(index)+i));
			html += "</tr>";
		}
		$(".com-product").each(function(){
			$(this).hide();
		});
		$(".product-"+code+"-"+index).show();
	}
	return html;
}
function insertRows345(pi_code,pi_no,pi_name,retailPrice,code){ 
	var html = "";
	if(pi_code != null){
		var pi_codes = pi_code.split(",");
		var pi_nos = pi_no.split(",");
		var pi_names = pi_name.split(",");
		var pi_retailPrices = retailPrice.split(",");
		var index = $("#table"+code+" tr").size();
		$(".black-tr").each(function(){
			$(this).attr("class","write-tr");
		});
		for(i=0;i<pi_codes.length;i++){
			var style = "write-tr";
			if(i === 0){
				style = "black-tr";
			}
			html += "<tr class='"+style+"' onclick=\"javascript:doChangeIndex(this,'"+code+"','"+(parseInt(index)+i)+"');\">";
			html += PRODUCTHTML.getIndex(code, (parseInt(index)+i));
			html += PRODUCTHTML.getCode(code, pi_nos[i], pi_codes[i]);
			html += PRODUCTHTML.getName(pi_names[i]);
			html += PRODUCTHTML.getRetailPrice(code,pi_retailPrices[i]);
			html += PRODUCTHTML.getBuyFull(code);
			html += PRODUCTHTML.getDelAndAddGift(code, (parseInt(index)+i));
			html += "</tr>";
		}
		$(".com-product").each(function(){
			$(this).hide();
		});
		$(".product-"+code+"-"+index).show();
	}
	return html;
} 
/***************************赠送的商品*******************************/
var GIFTHTML = {
		getIndex:function(code,index){
			return "<td align='center' class='gtindex"+code+"'>"+index+"</td>";
		},
		getCode:function(code,pdCode,crCode,szCode,brCode,pdNo,sd_code,index){
			var html = "<td>&nbsp;"+Public.subLenString(pdNo)+"<input type='hidden' value='"+sd_code+"' id='ssp_subcode"+code+"_"+index+"' name='ssp_subcode"+code+"_"+index+"' />";
			html += "<input type='hidden' value='"+pdCode+"' id='ssp_pd_code"+code+"_"+index+"' name='ssp_pd_code"+code+"_"+index+"' />"
			html += "<input type='hidden' value='"+crCode+"' id='ssp_cr_code"+code+"_"+index+"' name='ssp_cr_code"+code+"_"+index+"' />"
			html += "<input type='hidden' value='"+szCode+"' id='ssp_sz_code"+code+"_"+index+"' name='ssp_sz_code"+code+"_"+index+"' />"
			html += "<input type='hidden' value='"+brCode+"' id='ssp_br_code"+code+"_"+index+"' name='ssp_br_code"+code+"_"+index+"' />"
			html += "</td>";
			return html;
		},
		getName:function(pdName){
			return "<td>&nbsp;"+Public.subLenString(pdName)+"</td>";
		},
		getCrName:function(cr_name){
			return "<td>&nbsp;"+cr_name+"</td>";;
		},
		getSzName:function(sz_name){
			return "<td>&nbsp;"+sz_name+"</td>";;
		},
		getBsName:function(br_name){
			return "<td>&nbsp;"+br_name+"</td>";;
		},
		getCount:function(code,index){
			return "<td><input type='text' class='main_Input  w100' value='1' id='ssp_count"+code+"_"+index+"' name='ssp_count"+code+"_"+index+"' onblur=\"vadiNumber(this)\" style='text-align: right;'/></td>";
		},
		getDelete:function(code){
			return "<td valign='top'><input type='button' class='btn_sp' value='删除' onclick=\"javascript:delGiftTableRow(this,'gttable"+code+"','gtindex"+code+"');\"/></td>";
		}
}
function delGiftTableRow(obj,tb_id,tr_index){
	$(obj).parent().parent().remove();
	var index = $("#"+tb_id+" tr").size();
	if(index != null && parseInt(index) > 0){
		index = 0;
		$("."+tr_index).each(function(){
			$(this).html(++index);
		});
	}
}
//查询库存中商品
function queryStockProductInfo(giftFlag,code,pageindex){
	commonDia = $.dialog({
		title : '选择库存商品',
		content : 'url:'+config.BASEPATH+'stock/data/to_list_dialog_sale',
		data : {multiselect:true,isGift:giftFlag},
		width:760,
	   	height:400,
		max : false,
		min : false,
		cache : false,
		lock: true,
		ok:function(){
			var selected = commonDia.content.doSelect();
			if (!selected) {
				return false;
			}
		},
		close:function(){
			var selected = commonDia.content.doSelect();
			if(selected){
				var pd_codes = [];
				var pd_nos = [];
				var pd_names = [];
				var sd_codes = [];
				var cr_codes = [];
				var sz_codes = [];
				var br_codes = [];
				var cr_names = [];
				var sz_names = [];
				var br_names = [];
				var pd_sell_prices = [];
				for(var i=0;i<selected.length;i++){
					pd_codes.push(selected[i].sd_pd_code);
					pd_nos.push(selected[i].pd_no);
					pd_names.push(selected[i].pd_name);
					sd_codes.push(selected[i].sd_code);
					cr_codes.push(selected[i].sd_cr_code);
					sz_codes.push(selected[i].sd_sz_code);
					br_codes.push(selected[i].sd_br_code);
					cr_names.push(selected[i].cr_name);
					sz_names.push(selected[i].sz_name);
					br_names.push(selected[i].br_name);
				}
				var data = {pd_code:pd_codes.join(","),pd_no:pd_nos.join(","),pd_name:pd_names.join(","),sd_code:sd_codes.join(","),cr_code:cr_codes.join(","),sz_code:sz_codes.join(","),br_code:br_codes.join(","),cr_name:cr_names.join(","),sz_name:sz_names.join(","),br_name:br_names.join(",")}
		   		var html = insertRows(data,code,pageindex);
		   		$("#gttable"+code).append(html);
			}
		},
		cancel:true
	});
}

function insertRows(data,code,pageindex){ 
	var html = "",hadPiCode = "";
	$("input[name='ssp_subcode"+code+"_"+pageindex+"']").each(function(){
		hadPiCode += $(this).val()+",";
	});
	if(data.sd_code != undefined && data.sd_code != null){
		var pd_codes = data.pd_code.split(",");
		var pd_nos = data.pd_no.split(",");
		var pd_names = data.pd_name.split(",");
		var sd_codes = data.sd_code.split(",");
		var cr_codes = data.cr_code.split(",");
		var sz_codes = data.sz_code.split(",");
		var br_codes = data.br_code.split(",");
		var cr_names = data.cr_name.split(",");
		var sz_names = data.sz_name.split(",");
		var br_names = data.br_name.split(",");
		var index = $("#gttable"+code+" tr").size();
		for(i=0;i<sd_codes.length;i++){
			if(hadPiCode != "" && hadPiCode.indexOf(sd_codes[i]) > -1){
				index --;
				continue;
			}else{
				html += "<tr class='product-"+code+"-"+pageindex+" com-product'>";
				html += GIFTHTML.getIndex(code, (parseInt(index)+i));
				html += GIFTHTML.getCode(code,pd_codes[i],cr_codes[i],sz_codes[i],br_codes[i],pd_nos[i],sd_codes[i],pageindex);
				html += GIFTHTML.getName(pd_names[i]);
				html += GIFTHTML.getCrName(cr_names[i]);
				html += GIFTHTML.getSzName(sz_names[i]);
				html += GIFTHTML.getBsName(br_names[i]);
				html += GIFTHTML.getCount(code,pageindex);
				html += GIFTHTML.getDelete(code);
				html += "</tr>";
			}
		}
	}
	return html;
} 

/****************************买满送添加金额多条**************************************/
var MONEYHTML = {
		getIndex:function(code,index){
			return "<td align='center' class='index"+code+"'>"+index+"</td>";
		},
		getCtCode:function(code,ctCode){
			return "<td>&nbsp;"+ctCode+"<input type='hidden' value='"+ctCode+"' id='ST_Ct_Code"+code+"' name='ST_Ct_Code"+code+"' style='text-align: right;'/></td>";
		},
		getCtName:function(ctName){
			return "<td>&nbsp;"+ctName+"</td>";
		},
		getDiscount:function(code){
			return "<td><input class='main_Input w100' type='text' value='1.0' id='ssa_discount"+code+"' name='ssa_discount"+code+"' onblur=\"vadiNumber(this)\" style='text-align: right;'/></td>";
		},
		getBuyFull:function(code){
			return "<td><input type='text' class='main_Input w100' value='0.0' id='ssa_buy_full"+code+"' name='ssa_buy_full"+code+"' onblur=\"vadiNumber(this)\" style='text-align: right;'/></td>";
		},
		getBuyNumber:function(code){
			return "<td><input type='text' class='main_Input  w100' value='0' id='ssa_buy_number"+code+"' name='ssa_buy_number"+code+"' onblur=\"vadiNumber(this)\" style='text-align: right;'/></td>";
		},
		getDonationNumber:function(code){
			return "<td><input type='text' class='main_Input  w100' value='0' id='ssa_donation_number"+code+"' name='ssa_donation_number"+code+"' onblur=\"vadiNumber(this)\" style='text-align: right;'/></td>";
		},
		getDonaTionAmount:function(code){
			return "<td><input class='main_Input w100' type='text' value='0.0' id='ssa_donation_amount"+code+"' name='ssa_donation_amount"+code+"' onblur=\"vadiNumber(this)\" style='text-align: right;'/></td>";
		},
		getReduceAmount:function(code){
			return "<td><input class='main_Input w100' type='text' value='0.0' id='ssa_reduce_amount"+code+"' name='ssa_reduce_amount"+code+"' onblur=\"vadiNumber(this)\" style='text-align: right;'/></td>";
		},
		getIncreasedAmount:function(code){
			return "<td><input class='main_Input w100' type='text' value='0.0' id='ssa_increased_amount"+code+"' name='ssa_increased_amount"+code+"' onblur=\"vadiNumber(this)\" style='text-align: right;'/></td>";
		},
		getDelete:function(code){
			return "<td valign='top'><input type='button' class='btn_sp' value='删除' onclick=\"javascript:delTableRow(this,'table"+code+"','index"+code+"');\"/></td>";
		},
		getDelAndAdd:function(code,index){
			return "<td valign='top'>" +
					"<input type='button' class='btn_sp' value='商品' onclick=\"javascript:queryStockProductInfo('0','"+code+"','"+index+"');\"/>"+
					"<input type='button' class='btn_sp' value='删除' onclick=\"javascript:delProductRow(this,'"+code+"','"+index+"',event);\"/>" +
					"</td>";
		}
		,
		getDelAndAddGift:function(code,index){
			return "<td valign='top'>" +
					"<input type='button' class='btn_sp' value='赠品' onclick=\"javascript:queryStockProductInfo('1','"+code+"','"+index+"');\"/>"+
					"<input type='button' class='btn_sp' value='删除' onclick=\"javascript:delProductRow(this,'"+code+"','"+index+"',event);\"/>" +
					"</td>";
		}
}
/**
 * @param obj 对象,code 促销类型编号,index 行号 ,evt 窗口事件
 * */
function delProductRow(obj,code,row,evt){
	$(obj).parent().parent().remove();
	//重新计算行号
	var index = $("#table"+code+" tr").size();
	if(index != null && parseInt(index) > 0){
		index = 0;
		$(".index"+code).each(function(){
			$(this).html(++index);
		});
	}
	//删除相关的商品或赠品
	$(".product-"+code+"-"+row).each(function(){
		$(this).remove();
	});
	var blank_len = $(".black-tr").length;
	if(blank_len == 0){//如果已经选择了其他行，则不重置
		index = 0;
		$(".write-tr").each(function(){
			index ++;
			if(index == 1){//把赠品恢复到第一条
				$(this).attr("class","black-tr");
				$(".com-product").each(function(){
					$(this).hide();
				});
				$(".product-"+code+"-"+index).show();
			}
		});
	}
	//与换行事件冒泡，进行阻止
	if(evt != undefined){
		try{
			var bro=$.browser;
		    if(bro.msie){
		    	var version=bro.version;
		    	var num = (parseInt(version));
		    	if(num < 7){
		    		evt.cancelBubble = false;
		    	}
		    }else{
		    	evt.stopPropagation();
		    }
		}catch(e){
		}
	}
}
function doChangeIndex(obj,code,index){
	$(".black-tr").each(function(){
		$(this).attr("class","write-tr");
	});
	$(obj).attr("class","black-tr");
	$(".com-product").each(function(){
		$(this).hide();
	});
	$(".product-"+code+"-"+index).show();
}
/*function insertRows311(code){ 
	var html = "";
	var index = $("#table"+code+" tr").size();
	html += "<tr>";
	html += MONEYHTML.getIndex(code, (parseInt(index)));
	html += MONEYHTML.getBuyFull(code);
	html += MONEYHTML.getDonaTionAmount(code);
	html += MONEYHTML.getDelete(code);
	html += "</tr>";
	$("#table"+code).append(html);
} */
function insertRows311(code){ 
	var html = "";
	var index = $("#table"+code+" tr").size();
	html += "<tr>";
	html += MONEYHTML.getIndex(code, (parseInt(index)));
	html += MONEYHTML.getBuyNumber(code);
	html += MONEYHTML.getDonationNumber(code);
	html += MONEYHTML.getDelete(code);
	html += "</tr>";
	$("#table"+code).append(html);
} 
function insertRows312(code){ 
	var html = "";
	var index = $("#table"+code+" tr").size();
	html += "<tr>";
	html += MONEYHTML.getIndex(code, (parseInt(index)));
	html += MONEYHTML.getBuyFull(code);
	html += MONEYHTML.getReduceAmount(code);
	html += MONEYHTML.getDelete(code);
	html += "</tr>";
	$("#table"+code).append(html);
} 
function insertRows313(code){ 
	var html = "";
	var index = $("#table"+code+" tr").size();
	$(".black-tr").each(function(){
		$(this).attr("class","write-tr");
	});
	html += "<tr class='black-tr' onclick=\"javascript:doChangeIndex(this,'"+code+"','"+index+"');\">";
	html += MONEYHTML.getIndex(code, (parseInt(index)));
	html += MONEYHTML.getBuyFull(code);
	html += MONEYHTML.getIncreasedAmount(code);
	html += MONEYHTML.getDelAndAdd(code,index);
	html += "</tr>";
	$("#table"+code).append(html);
	$(".com-product").each(function(){
		$(this).hide();
	});
	$(".product-"+code+"-"+index).show();
} 
function insertRows314(code){ 
	var html = "";
	var index = $("#table"+code+" tr").size();
	$(".black-tr").each(function(){
		$(this).attr("class","write-tr");
	});
	html += "<tr class='black-tr' onclick=\"javascript:doChangeIndex(this,'"+code+"','"+index+"');\">";
	html += MONEYHTML.getIndex(code, (parseInt(index)));
	html += MONEYHTML.getBuyNumber(code);
	html += MONEYHTML.getDelAndAddGift(code,index);
	html += "</tr>";
	$("#table"+code).append(html);
	$(".com-product").each(function(){
		$(this).hide();
	});
	$(".product-"+code+"-"+index).show();
}
function insertRows315(code){ 
	var html = "";
	var index = $("#table"+code+" tr").size();
	$(".black-tr").each(function(){
		$(this).attr("class","write-tr");
	});
	html += "<tr class='black-tr' onclick=\"javascript:doChangeIndex(this,'"+code+"','"+index+"');\">";
	html += MONEYHTML.getIndex(code, (parseInt(index)));
	html += MONEYHTML.getBuyFull(code);
	html += MONEYHTML.getDelAndAddGift(code,index);
	html += "</tr>";
	$("#table"+code).append(html);
	$(".com-product").each(function(){
		$(this).hide();
	});
	$(".product-"+code+"-"+index).show();
}
function insertRows112(code){ 
	var html = "";
	var index = $("#table"+code+" tr").size();
	html += "<tr>";
	html += MONEYHTML.getIndex(code, (parseInt(index)));
	html += MONEYHTML.getBuyFull(code);
	html += MONEYHTML.getDiscount(code);
	html += MONEYHTML.getDelete(code);
	html += "</tr>";
	$("#table"+code).append(html);
} 
function insertRows113(code){ 
	var html = "";
	var index = $("#table"+code+" tr").size();
	html += "<tr>";
	html += MONEYHTML.getIndex(code, (parseInt(index)));
	html += MONEYHTML.getBuyNumber(code);
	html += MONEYHTML.getDiscount(code);
	html += MONEYHTML.getDelete(code);
	html += "</tr>";
	$("#table"+code).append(html);
} 
/*************************************验证促销方案**************************************************/
var VERIFYPARAM = {
	ifDiscount:function(discount){
		if(discount == ""){
			$.dialog.tips("请输入折扣!",2,"32X32/fail.png");return false;
		}
		if(isNaN(discount)){
			$.dialog.tips("折扣只能输入数字!",2,"32X32/fail.png");return false;
		}
		if(parseFloat(discount)<=0 || parseFloat(discount)>1){
			$.dialog.tips("折扣只能输入0~1数字，并且不能为0!",2,"32X32/fail.png");return false;
		}
		return true;
	},
	ifMoney:function(money){
		if(money == ""){
			$.dialog.tips("请输入金额!",2,"32X32/fail.png");return false;
		}
		if(isNaN(money)){
			$.dialog.tips("金额只能输入数字!",2,"32X32/fail.png");return false;
		}
		return true;
	},
	ifNumber:function(number){
		if(number == ""){
			$.dialog.tips("请输入买满数量!",2,"32X32/fail.png");return false;
		}
		if(isNaN(number)){
			$.dialog.tips("买满数量只能输入数字!",2,"32X32/fail.png");	return false;
		}
		return true;
	},
	ifNumberPresent:function(number){
		if(number == ""){
			$.dialog.tips("请输入赠送数量!",2,"32X32/fail.png");return false;
		}
		if(isNaN(number)){
			$.dialog.tips("赠送数量只能输入数字!",2,"32X32/fail.png");	return false;
		}
		return true;
	}
}
var VERIFY = {
		test313:function(){
			var buyFull313s = $("input[name='ssa_buy_full313']");
 			var increasedAmount313s = $("input[name='ssa_increased_amount313']");
 			var flag = true;
 			if(buyFull313s == null || buyFull313s.length==0){
 				Public.tips({type: 1, content : '请添加金额条件!'});
 				flag = false;
 			}
 			for (var i=0;i<buyFull313s.length;i++){
 				var soCode313s = $("input[name='ssp_subcode313_"+(i+1)+"']");
 	 			var ppCount313s = $("input[name='ssp_count313_"+(i+1)+"']");
 	 			if(soCode313s.length==0){
 	 				Public.tips({type: 1, content : '请添加赠品!'});
 	 				flag = false;
 	 				break;
 	 			}
 	 			if(!VERIFYPARAM.ifMoney($(buyFull313s[i]).val())){
 	 				Public.tips({type:1,content:'输入值不符合!'});
 	 				flag = false;
 	 				break;
 	 			}
 	 			if(!VERIFYPARAM.ifMoney($(increasedAmount313s[i]).val())){
 	 				Public.tips({type:1,content:'输入值不符合!'});
 	 				flag = false;
 	 				break;
 	 			}
 	 			var index = 0;
 	 			for(var j=0;j<soCode313s.length;j++){
 	 				if(!VERIFYPARAM.ifNumberPresent($(ppCount313s[j]).val())){
 	 					Public.tips({type:1,content:'输入值不符合!'});
 	 					flag = false;
 	 					index = 1; 
 	 					break;
 	 				}
 	 			}
 	 			if(index == 1){
					break;
				}
	 		}
 			return flag;
		},
		test314:function(){
			var buyNumber314 = $("input[name='ssa_buy_number314']");
			var flag = true;
			if(buyNumber314 == null || buyNumber314.length==0){
 				Public.tips({type: 1, content : '请添加数量条件!'});
 				flag = false;
 			}
			for (var i=0;i<buyNumber314.length;i++){
				if(!VERIFYPARAM.ifNumber($(buyNumber314[i]).val())){
					Public.tips({type:1,content:'输入值不符合!'});
					flag = false;
					break;
				}
				var soCode314s = $("input[name='ssp_subcode314_"+(i+1)+"']");
 	 			var ppCount314s = $("input[name='ssp_count314_"+(i+1)+"']");
				if(soCode314s == null || soCode314s.length == 0){
					Public.tips({type: 1, content : '请添加赠品!'});
					flag = false;
					break;
				}
				index = 0;
				for(var j=0;j<soCode314s.length;j++){
					if(!VERIFYPARAM.ifNumberPresent($(ppCount314s[j]).val())){
						Public.tips({type:1,content:'输入值不符合!'});
						flag = false;
						index = 1;
						break;
					}
				}
				if(index == 1){
					break;
				}
			}
			return flag;
		},
		test315:function(){
			var buyFull315 = $("input[name='ssa_buy_full315']");
			var flag = true;
			if(buyFull315 == null || buyFull315.length==0){
 				Public.tips({type: 1, content : '请添加金额条件!'});
 				flag = false;
 			}
			for (var i=0;i<buyFull315.length;i++){
				if(!VERIFYPARAM.ifMoney($(buyFull315[i]).val())){
					Public.tips({type:1,content:'输入值不符合!'});
					flag = false;
					break;
				}
				var soCode315s = $("input[name='ssp_subcode315_"+(i+1)+"']");
 	 			var ppCount315s = $("input[name='ssp_count315_"+(i+1)+"']");
				if(soCode315s == null || soCode315s.length == 0){
					Public.tips({type: 1, content : '请添加赠品!'});
					flag = false;
					break;
				}
				index = 0;
				for(var j=0;j<soCode315s.length;j++){
					if(!VERIFYPARAM.ifNumberPresent($(ppCount315s[j]).val())){
						Public.tips({type:1,content:'输入值不符合!'});
						flag = false;
						index = 1;
						break;
					}
				}
				if(index == 1){
					break;
				}
			}
			return flag;
		},
		test323:function(){
			var buyFull323s = $("input[name='sst_buy_full323']");
			var increasedAmount323s = $("input[name='sst_increased_amount323']");
			var flag = true;
			if(buyFull323s = null || buyFull323s.length == 0){
				Public.tips({type:1,content:'请添加类别!'});
				flag = false;
			}
			for(var i=0;i<buyFull323s.length;i++){
				if(!VERIFYPARAM.ifMoney($(buyFull323s[i]).val())){
					Public.tips({type:1,content:'输入值不符合!'});
					flag = false;
					break;
	 			}
	 			if(!VERIFYPARAM.ifMoney($(increasedAmount323s[i]).val())){
	 				Public.tips({type:1,content:'输入值不符合!'});
	 				flag = false;
	 				break;
	 			}
				var soCode323s = $("input[name='ssp_subcode323_"+(i+1)+"']");
 	 			var ppCount323s = $("input[name='ssp_count323_"+(i+1)+"']");
				
				if(soCode323s.length==0){
					Public.tips({type:1,content:'请添加赠品!'});
					flag = false;
					break;
				}
				var index = 0;
				for(var i=0;i<soCode323s.length;i++){
					if(!VERIFYPARAM.ifNumberPresent($(ppCount323s[i]).val())){
						Public.tips({type:1,content:'输入值不符合!'});
						flag = false;
						index = 1;
						break;
					}
				}
				if(index == 1){
					break;
				}
			}
			return flag;
		},
		test324:function(){
			var buyNumber324s = $("input[name='sst_buy_number324']");
			var flag = true;
			if(buyNumber324s == null || buyNumber324s.length == 0){
				Public.tips({type:1,content:'请添加类别!'});
				flag = false;
			}
			for(var i=0;i<buyNumber324s.length;i++){
				if(!VERIFYPARAM.ifNumber($(buyNumber324s[i]).val())){
					Public.tips({type:1,content:'输入值不符合!'});
					flag = false;
					break;
	 			}
				var soCode324s = $("input[name='ssp_subcode324_"+(i+1)+"']");
 	 			var ppCount324s = $("input[name='ssp_count324_"+(i+1)+"']");
				
				if(soCode324s == null || soCode324s.length==0){
					Public.tips({type:1,content:'请添加赠品!'});
					flag = false;
					break;
				}
				var index = 0;
				for(var j=0;j<soCode324s.length;j++){
					if(!VERIFYPARAM.ifNumberPresent($(ppCount324s[j]).val())){
						Public.tips({type:1,content:'输入值不符合!'});
						flag = false;
						index = 1;
						break;
					}
				}
				if(index == 1){
					break;
				}
			}
			return flag;
		},
		test325:function(){
			var buyFull325s = $("input[name='sst_buy_full325']");
			var flag = true;
			if(buyFull325s == null || buyFull325s.length == 0){
				Public.tips({type:1,content:'请添加类别!'});
				flag = false;
			}
			for(var i=0;i<buyFull325s.length;i++){
				if(!VERIFYPARAM.ifMoney($(buyFull325s[i]).val())){
					Public.tips({type:1,content:'输入值不符合!'});
					flag = false;
					break;
	 			}
				var soCode325s = $("input[name='ssp_subcode325_"+(i+1)+"']");
 	 			var ppCount325s = $("input[name='ssp_count325_"+(i+1)+"']");
				
				if(soCode325s == null || soCode325s.length==0){
					Public.tips({type:1,content:'请添加赠品!'});
					flag = false;
					break;
				}
				var index = 0;
				for(var j=0;j<soCode325s.length;j++){
					if(!VERIFYPARAM.ifNumberPresent($(ppCount325s[j]).val())){
						Public.tips({type:1,content:'输入值不符合!'});
						flag = false;
						index = 1;
						break;
					}
				}
				if(index == 1){
					break;
				}
			}
			return flag;
		},
		test333:function(){
			var buyFull333s = $("input[name='ssb_buy_full333']");
			var increasedAmount333s = $("input[name='ssb_increased_amount333']");
			var flag = true;
			if(buyFull333s == null || buyFull333s.length == 0){
				Public.tips({type:1,content:'请添加品牌!'});
				flag = false;
			}
			for(var i=0;i<buyFull333s.length;i++){
				if(!VERIFYPARAM.ifMoney($(buyFull333s[i]).val())){
					Public.tips({type:1,content:'输入值不符合!'});
					flag = false;
					break;
	 			}
	 			if(!VERIFYPARAM.ifMoney($(increasedAmount333s[i]).val())){
	 				Public.tips({type:1,content:'输入值不符合!'});
					flag = false;
					break;
	 			}
	 			var soCode333s = $("input[name='ssp_subcode333_"+(i+1)+"']");
 	 			var ppCount333s = $("input[name='ssp_count333_"+(i+1)+"']");
	 			if(soCode333s == null || soCode333s.length==0){
	 				Public.tips({type:1,content:'请添加商品!'});
	 				flag = false;
					break;
	 			}
	 			var index = 0;
	 			for(var j=0;j<soCode333s.length;j++){
	 				if(!VERIFYPARAM.ifNumberPresent($(ppCount333s[j]).val())){
	 					Public.tips({type:1,content:'输入值不符合!'});
						flag = false;
						index = 1;
						break;
	 				}
	 			}
	 			if(index == 1){
					break;
				}
			}
			return flag;
		},
		test334:function(){
			var buyNumber334s = $("input[name='ssb_buy_number334']");
			var flag = true;
			if(buyNumber334s == null || buyNumber334s.length == 0){
				Public.tips({type:1,content:'请添加品牌!'});
				flag = false;
			}
			for(var i=0;i<buyNumber334s.length;i++){
				if(!VERIFYPARAM.ifNumber($(buyNumber334s[i]).val())){
					Public.tips({type:1,content:'输入值不符合!'});
					flag = false;
					break;
	 			}
				var soCode334s = $("input[name='ssp_subcode334_"+(i+1)+"']");
 	 			var ppCount334s = $("input[name='ssp_count334_"+(i+1)+"']");
				if(soCode334s == null || soCode334s.length==0){
					Public.tips({type:1,content:'请添加赠品!'});
					flag = false;
					break;
				}
				var index = 0;
				for(var j=0;j<soCode334s.length;j++){
					if(!VERIFYPARAM.ifNumberPresent($(ppCount334s[j]).val())){
						Public.tips({type:1,content:'输入值不符合!'});
						flag = false;
						index = 1;
						break;
					}
				}
				if(index == 1){
					break;
				}
			}
			return flag;
		},
		test335:function(){
			var buyFull335s = $("input[name='ssb_buy_full335']");
			var flag = true;
			if(buyFull335s == null || buyFull335s.length == 0){
				Public.tips({type:1,content:'请添加品牌!'});
				flag = false;
			}
			for(var i=0;i<buyFull335s.length;i++){
				if(!VERIFYPARAM.ifMoney($(buyFull335s[i]).val())){
					Public.tips({type:1,content:'输入值不符合!'});
					flag = false;
					break;
	 			}
				var soCode335s = $("input[name='ssp_subcode335_"+(i+1)+"']");
 	 			var ppCount335s = $("input[name='ssp_count335_"+(i+1)+"']");
				if(soCode335s == null || soCode335s.length==0){
					Public.tips({type:1,content:'请添加赠品!'});
					flag = false;
					break;
				}
				var index = 0;
				for(var j=0;j<soCode335s.length;j++){
					if(!VERIFYPARAM.ifNumberPresent($(ppCount335s[j]).val())){
						Public.tips({type:1,content:'输入值不符合!'});
						flag = false;
						index = 1;
						break;
					}
				}
				if(index == 1){
					break;
				}
			}
			return flag;
		},
		test343:function(){
			var buyFull343s = $("input[name='ssp_buy_full343']");
			var increasedAmount343s = $("input[name='ssp_increased_amount343']");
			var flag = true;
			if(buyFull343s == null || buyFull343s.length == 0){
				Public.tips({type:1,content:'请添加商品!'});
				flag = false;
			}
			for(var i=0;i<buyFull343s.length;i++){
				if(!VERIFYPARAM.ifMoney(buyFull343s[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
					flag = false;
					break;
	 			}
	 			if(!VERIFYPARAM.ifMoney(increasedAmount343s[i].value)){
	 				Public.tips({type:1,content:'输入值不符合!'});
					flag = false;
					break;
	 			}
				var soCode343s = $("input[name='ssp_subcode343_"+(i+1)+"']");
 	 			var ppCount343s = $("input[name='ssp_count343_"+(i+1)+"']");
				if(soCode343s == null || soCode343s.length==0){
					Public.tips({type:1,content:'请添加赠品!'});
					flag = false;
					break;
				}
				var index = 0;
				for(var j=0;j<soCode343s.length;j++){
					if(!VERIFYPARAM.ifNumberPresent(ppCount343s[j].value)){
						Public.tips({type:1,content:'输入值不符合!'});
						flag = false;
						index = 1;
						break;
					}
				}
				if(index == 1){
					break;
				}
			}
			return flag;
		},
		test344:function(){
			var buyNumber344s = $("input[name='ssp_buy_number344']");
			var flag = true;
			if(buyNumber344s == null || buyNumber344s.length == 0){
				Public.tips({type:1,content:'请添加商品!'});
				flag = false;
			}
			for(var i=0;i<buyNumber344s.length;i++){
				if(!VERIFYPARAM.ifNumber(buyNumber344s[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
					flag = false;
					break;
	 			}
				var soCode344s = $("input[name='ssp_subcode344_"+(i+1)+"']");
 	 			var ppCount344s = $("input[name='ssp_count344_"+(i+1)+"']");
				if(soCode344s == null || soCode344s.length==0){
					Public.tips({type:1,content:'请添加赠品!'});
					flag = false;
				}
				var index = 0;
				for(var j=0;j<soCode344s.length;j++){
					if(!VERIFYPARAM.ifNumberPresent(ppCount344s[j].value)){
						Public.tips({type:1,content:'输入值不符合!'});
						flag = false;
						index = 1;
						break;
					}
				}
				if(index == 1){
					break;
				}
			}
			return flag;
		},
		test345:function(){
			var buyFull345s = $("input[name='ssp_buy_full345']");
			var flag = true;
			if(buyFull345s == null || buyFull345s.length == 0){
				Public.tips({type:1,content:'请添加商品!'});
				flag = false;
			}
			for(var i=0;i<buyFull345s.length;i++){
				if(!VERIFYPARAM.ifMoney(buyFull345s[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
					flag = false;
					break;
	 			}
				var soCode345s = $("input[name='ssp_subcode345_"+(i+1)+"']");
 	 			var ppCount345s = $("input[name='ssp_count345_"+(i+1)+"']");
				if(soCode345s == null || soCode345s.length==0){
					Public.tips({type:1,content:'请添加赠品!'});
					flag = false;
				}
				var index = 0;
				for(var j=0;j<soCode345s.length;j++){
					if(!VERIFYPARAM.ifNumberPresent(ppCount345s[j].value)){
						Public.tips({type:1,content:'输入值不符合!'});
						flag = false;
						index = 1;
						break;
					}
				}
				if(index == 1){
					break;
				}
			}
			return flag;
		}
}

/*************************************保存促销方案**************************************************/

function saveSales(){
		var ss_model = $("#ss_model").val();
		if(ss_model == "111"){
			var discount111 = $("#ssa_discount111").val();
			if(!VERIFYPARAM.ifDiscount(discount111)){
				Public.tips({type:1,content:'输入值不符合!'});
				return;
			}
		}
		if(ss_model == "112"){
			var buyFulls = document.getElementsByName("ssa_buy_full112");
			var discounts = document.getElementsByName("ssa_discount112");
			for(var i=0;i<buyFulls.length;i++){
				if(!VERIFYPARAM.ifMoney(buyFulls[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
					return;
				}
				if(!VERIFYPARAM.ifDiscount(discounts[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
					return;
				}
			}
		}
		if(ss_model == "113"){
			var buyNumbers = document.getElementsByName("ssa_buy_number113");
			var discounts = document.getElementsByName("ssa_discount113");
			for(var i=0;i<buyNumbers.length;i++){
				if(!VERIFYPARAM.ifNumber(buyNumbers[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
					return;
				}
				if(!VERIFYPARAM.ifDiscount(discounts[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
					return;
				}
			}
		}
		if(ss_model == "121"){
			var discount121s = document.getElementsByName("sst_discount121");
			if(discount121s.length == 0){
				Public.tips({type:1,content:'请添加类别!'});
				return;
			}
			for(var i=0;i<discount121s.length;i++){
				if(!VERIFYPARAM.ifDiscount(discount121s[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
	 				return;
	 			}
			}
		}
		if(ss_model == "122"){
			var discount122s = document.getElementsByName("sst_discount122");
			var buyFull122s = document.getElementsByName("sst_buy_full122");
			
			if(discount122s.length == 0){
				Public.tips({type:1,content:'请添加类别!'});
				return;
			}
			for(var i=0;i<discount122s.length;i++){
				if(!VERIFYPARAM.ifMoney(buyFull122s[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
					return;
				}
				if(!VERIFYPARAM.ifDiscount(discount122s[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
	 				return;
	 			}
			}
		}
		if(ss_model == "123"){
			var discount123s = document.getElementsByName("sst_discount123");
			var buyNumber123s = document.getElementsByName("sst_buy_number123");
			
			if(discount123s.length == 0){
				Public.tips({type:1,content:'请添加类别!'});
				return;
			}
			for(var i=0;i<discount123s.length;i++){
				if(!VERIFYPARAM.ifNumber(buyNumber123s[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
					return;
				}
				if(!VERIFYPARAM.ifDiscount(discount123s[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
	 				return;
	 			}
			}
		}
		
		if(ss_model == "131"){
			var discount131s = document.getElementsByName("ssb_discount131");
			if(discount131s.length == 0){
				Public.tips({type:1,content:'请添加品牌!'});
				return;
			}
			for(var i=0;i<discount131s.length;i++){
				if(!VERIFYPARAM.ifDiscount(discount131s[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
	 				return;
	 			}
			}
		}
		if(ss_model == "132"){
			var discount132s = document.getElementsByName("ssb_discount132");
			var buyFull132s = document.getElementsByName("ssb_buy_full132");
			
			if(discount132s.length == 0){
				Public.tips({type:1,content:'请添加品牌!'});
				return;
			}
			for(var i=0;i<discount132s.length;i++){
				if(!VERIFYPARAM.ifMoney(buyFull132s[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
					return;
				}
				if(!VERIFYPARAM.ifDiscount(discount132s[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
	 				return;
	 			}
			}
		}
		
		if(ss_model == "133"){
			var discount133s = document.getElementsByName("ssb_discount133");
			var buyNumber133s = document.getElementsByName("ssb_buy_number133");
			
			if(discount133s.length == 0){
				Public.tips({type:1,content:'请添加类别!'});
				return;
			}
			for(var i=0;i<discount133s.length;i++){
				if(!VERIFYPARAM.ifNumber(buyNumber133s[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
					return;
				}
				if(!VERIFYPARAM.ifDiscount(discount133s[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
	 				return;
	 			}
			}
		}
		
		if(ss_model == "141"){
			var discount141s = document.getElementsByName("ssp_discount141");
			if(discount141s.length == 0){
				Public.tips({type:1,content:'请添加商品!'});
				return;
			}
			for(var i=0;i<discount141s.length;i++){
				if(!VERIFYPARAM.ifDiscount(discount141s[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
	 				return;
	 			}
			}
		}
		if(ss_model == "142"){
			var discount142s = document.getElementsByName("ssp_discount142");
			var buyFull142s = document.getElementsByName("ssp_buy_full142");
			if(discount142s.length == 0){
				Public.tips({type:1,content:'请添加商品!'});
				return;
			}
			for(var i=0;i<discount142s.length;i++){
				if(!VERIFYPARAM.ifMoney(buyFull142s[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
					return;
				}
				if(!VERIFYPARAM.ifDiscount(discount142s[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
	 				return;
	 			}
			}
		}
		
		if(ss_model == "143"){
			var discount143s = document.getElementsByName("ssp_discount143");
			var buyNumber143s = document.getElementsByName("ssp_buy_number143");
			if(discount143s.length == 0){
				Public.tips({type:1,content:'请添加商品!'});
				return;
			}
			for(var i=0;i<discount143s.length;i++){
				if(!VERIFYPARAM.ifNumber(buyNumber143s[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
					return;
				}
				if(!VERIFYPARAM.ifDiscount(discount143s[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
	 				return;
	 			}
			}
		}
		
		if(ss_model == "241"){
			var specialoffer241s = document.getElementsByName("ssp_special_offer241");
			if(specialoffer241s.length == 0){
				Public.tips({type:1,content:'请添加商品!'});
				return;
			}
			for(var i=0;i<specialoffer241s.length;i++){
				if(!VERIFYPARAM.ifMoney(specialoffer241s[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
	 				return;
	 			}
			}
		}
		
		if(ss_model == "242"){
			var specialoffer242s = document.getElementsByName("ssp_special_offer242");
			var buyNumber242s = document.getElementsByName("ssp_buy_number242");
			if(specialoffer242s.length == 0){
				Public.tips({type:1,content:'请添加商品!'});
				return;
			}
			for(var i=0;i<specialoffer242s.length;i++){
				if(!VERIFYPARAM.ifNumber(buyNumber242s[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
					return;
				}
				if(!VERIFYPARAM.ifMoney(specialoffer242s[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
	 				return;
	 			}
			}
		}
		
		/*if(ss_model == "311"){
			var buyFull311s = document.getElementsByName("SA_Buy_Full311");
			var donationAmount311 = document.getElementsByName("SA_Donation_Amount311");
			if(buyFull311s.length==0){
				Public.tips({type:1,content:'请添加促销条件!'});
				return;
			}
			for(var i=0;i<buyFull311s.length;i++){
				if(!VERIFYPARAM.ifMoney(buyFull311s[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
					return;
				}
				if(!VERIFYPARAM.ifMoney(donationAmount311[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
					return;
				}
			}
		}*/
		if(ss_model == "311"){
			var buyNumbers = document.getElementsByName("ssa_buy_number311");
			var donationNumbers = document.getElementsByName("ssa_donation_number311");
			for(var i=0;i<buyNumbers.length;i++){
				if(!VERIFYPARAM.ifNumber(buyNumbers[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
					return;
				}
				if(!VERIFYPARAM.ifNumber(donationNumbers[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
					return;
				}
			}
		}
		
		if(ss_model == "312"){
			var buyFull312s = document.getElementsByName("ssa_buy_full312");
			var reduceAmount312 = document.getElementsByName("ssa_reduce_amount312");
			if(buyFull312s.length==0){
				Public.tips({type:1,content:'请添加促销条件!'});
				return;
			}
			for(var i=0;i<buyFull312s.length;i++){
				if(!VERIFYPARAM.ifMoney(buyFull312s[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
					return;
				}
				if(!VERIFYPARAM.ifMoney(reduceAmount312[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
					return;
				}
			}
		}
		
		if(ss_model == "313"){
			if(!VERIFY.test313()){
				return;
			}
		}
		if(ss_model == "314"){
			if(!VERIFY.test314()){
				return;
			}
		}
		if(ss_model == "315"){
			if(!VERIFY.test315()){
				return;
			}
		}
		/*if(ss_model == "321"){
			var buyFull321s = document.getElementsByName("ST_Buy_Full321");
			var donationAmount321s = document.getElementsByName("ST_Donation_Amount321");
			if(buyFull321s.length == 0){
				Public.tips({type:1,content:'请添加类别'});
				return;
			}
			for(var i=0;i<buyFull321s.length;i++){
				if(!VERIFYPARAM.ifMoney(buyFull321s[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
	 				return;
	 			}
	 			if(!VERIFYPARAM.ifMoney(donationAmount321s[i].value)){
	 				Public.tips({type:1,content:'输入值不符合!'});
	 				return;
	 			}
			}
		}*/
		
		if(ss_model == "322"){
			var buyFull322s = document.getElementsByName("sst_buy_full322");
			var reduceAmount322s = document.getElementsByName("sst_reduce_amount322");
			if(buyFull322s.length == 0){
				Public.tips({type:1,content:'请添加类别'});
				return;
			}
			for(var i=0;i<buyFull322s.length;i++){
				if(!VERIFYPARAM.ifMoney(buyFull322s[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
	 				return;
	 			}
	 			if(!VERIFYPARAM.ifMoney(reduceAmount322s[i].value)){
	 				Public.tips({type:1,content:'输入值不符合!'});
	 				return;
	 			}
			}
		}
		
		if(ss_model == "323"){
			if(!VERIFY.test323()){
				return;
			}
		}
		
		if(ss_model == "324"){
			if(!VERIFY.test324()){
				return;
			}
		}
		
		if(ss_model == "325"){
			if(!VERIFY.test325()){
				return;
			}
		}
		
		/*if(ss_model == "331"){
			var buyFull331s = document.getElementsByName("SB_Buy_Full331");
			var donationAmount331s = document.getElementsByName("SB_Donation_Amount331");
			if(buyFull331s.length == 0){
				Public.tips({type:1,content:'请添加品牌!'});
				return;
			}
			for(var i=0;i<buyFull331s.length;i++){
				if(!VERIFYPARAM.ifMoney(buyFull331s[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
	 				return;
	 			}
	 			if(!VERIFYPARAM.ifMoney(donationAmount331s[i].value)){
	 				Public.tips({type:1,content:'输入值不符合!'});
	 				return;
	 			}
			}
		}*/
		
		if(ss_model == "332"){
			var buyFull332s = document.getElementsByName("ssb_buy_full332");
			var reduceAmount332s = document.getElementsByName("ssb_reduce_amount332");
			if(buyFull332s.length == 0){
				Public.tips({type:1,content:'请添加品牌!'});
				return;
			}
			for(var i=0;i<buyFull332s.length;i++){
				if(!VERIFYPARAM.ifMoney(buyFull332s[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
	 				return;
	 			}
	 			if(!VERIFYPARAM.ifMoney(reduceAmount332s[i].value)){
	 				Public.tips({type:1,content:'输入值不符合!'});
	 				return;
	 			}
			}
		}
		
		if(ss_model == "333"){
			if(!VERIFY.test333()){
				return;
			}
		}
		
		if(ss_model == "334"){
			if(!VERIFY.test334()){
				return;
			}
		}
		
		if(ss_model == "335"){
			if(!VERIFY.test335()){
				return;
			}
		}
		
		/*if(ss_model == "341"){
			var buyFull341s = document.getElementsByName("SP_Buy_Full341");
			var donationAmount341s = document.getElementsByName("SP_Donation_Amount341");
			if(buyFull341s.length == 0){
				Public.tips({type:1,content:'请添加商品!'});
				return;
			}
			for(var i=0;i<buyFull341s.length;i++){
				if(!VERIFYPARAM.ifMoney(buyFull341s[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
	 				return;
	 			}
	 			if(!VERIFYPARAM.ifMoney(donationAmount341s[i].value)){
	 				Public.tips({type:1,content:'输入值不符合!'});
	 				return;
	 			}
			}
		}*/
		
		if(ss_model == "342"){
			var buyFull342s = document.getElementsByName("ssp_buy_full342");
			var reduceAmount342s = document.getElementsByName("ssp_reduce_amount342");
			if(buyFull342s.length == 0){
				Public.tips({type:1,content:'请添加商品!'});
				return;
			}
			for(var i=0;i<buyFull342s.length;i++){
				if(!VERIFYPARAM.ifMoney(buyFull342s[i].value)){
					Public.tips({type:1,content:'输入值不符合!'});
	 				return;
	 			}
	 			if(!VERIFYPARAM.ifMoney(reduceAmount342s[i].value)){
	 				Public.tips({type:1,content:'输入值不符合!'});
	 				return;
	 			}
			}
		}
		
		if(ss_model == "343"){
			if(!VERIFY.test343()){
				return;
			}
		}
		
		if(ss_model == "344"){
			if(!VERIFY.test344()){
				return;
			}
		}
		
		if(ss_model == "345"){
			if(!VERIFY.test345()){
				return;
			}
		}
		
		$("#btn_save").attr("disabled",true);
		$("#btn_pn").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"shop/sale/save",
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : '添加成功！'});
					W.isRefresh = true;
					setTimeout("api.close()",500);
				}else{
					Public.tips({type: 1, content : '添加失败，请联系管理员！'});
				}
				$("#btn-save").attr("disabled",false);
				$("#btn_pn").attr("disabled",true);
			}
		});
	}