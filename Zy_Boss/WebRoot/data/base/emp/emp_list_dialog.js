var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var api = frameElement.api;
var multiselect = false;
if(undefined != api.data.multiselect){
	multiselect = api.data.multiselect;
}
var _height = api.config.height-132,_width = api.config.width-34;//获取弹出框弹出宽、高
var queryurl = config.BASEPATH+'base/emp/list';
var dblClick = false;
var handle = {
	formatType:function(val){
		if(val == 0){
			return "导购员";
		}
		if(val == 1){
			return "业务员";
		}
		if(val == 2){
			return "职员";
		}
		if(val == 3){
			return "店长";
		}
	}
}
var THISPAGE = {
	init:function (){
		this.initGrid();
		this.initEvent();	
	},
	initGrid:function(){
		var colModel = [
			{name: 'em_code', label:'员工编号',index: 'em_code', width: 70, title: false},
	    	{name: 'em_name', label:'姓名',index: 'em_name', width: 80, title: false},
	    	{name: 'em_dm_code', label:'',index: 'em_dm_code', width: 80, hidden: true},
	    	{name: 'em_dm_name', label:'',index: 'em_dm_name', width: 80, hidden: true},
	    	{name: 'em_type', label:'类型',index: 'em_type', width: 80, title: false,formatter:handle.formatType},
	    	{name: 'em_shop_code',index: 'em_shop_code', hidden:true, title: false}
	    ];
		$('#grid').jqGrid({
            url:queryurl+"?"+THISPAGE.initParam(),			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:multiselect,//多选
			viewrecords: true,
			rowNum:30,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'em_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow: function (rowid, iRow, iCol, e) {
				if(!multiselect){
					dblClick = true;
					api.close();
				}
            }
	    });
	},
	initParam:function(){
		var param="1=1";
		var searchContent = $.trim($('#SearchContent').val());
		param += "&searchContent="+Public.encodeURI(searchContent);
		if(undefined != api.data.em_type && null != api.data.em_type){
			param += "&em_type="+api.data.em_type
		}
		return param;
	},
	reloadData:function(){
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+"?"+THISPAGE.initParam()}).trigger("reloadGrid");
	},
	initEvent:function(){
		$('#SearchContent').on('keyup',function(event){
			if (event.keyCode==13){
				$('#btn-search').click();
			}
		});
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
	}
};
function doSelect(){
	if(multiselect){//多选
		var ids = $("#grid").jqGrid('getGridParam', 'selarrrow');
		if(ids == null || ids.length == 0){
			Public.tips({type: 2, content : "请选择店铺！"});
			return false;
		}
		var result = [];
		for (var i = 0; i < ids.length; i++) {
			result.push($("#grid").jqGrid("getRowData", ids[i]));
		}
		return result;
		
	}else{//单选
		var selectedId = $("#grid").jqGrid('getGridParam', 'selrow');
		if(selectedId == null || selectedId == ""){
			Public.tips({type: 2, content : "请选择店铺！"});
			return false;
		}
		var rowData = $("#grid").jqGrid("getRowData", selectedId);
		return rowData;
	}
}
THISPAGE.init();