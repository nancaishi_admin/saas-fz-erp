var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var handle = {
	save:function(){
		var name = $("#em_name").val().trim();
	    if(name == ''){
	    	W.Public.tips({type: 1, content : "请输入姓名"});
	        $("#em_name").select().focus();
	     	setTimeout("api.zindex()",400);
	        return;
	    }else{
	    	$("#em_name").val(name);//防止空格
	    }
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"base/emp/save",
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					W.Public.tips({type: 3, content : data.message});
					handle.loadData(data.data);
					setTimeout("api.zindex()",500);
					api.close();
				}else if(data.stat == 304){
					W.Public.tips({type: 1, content : data.message});
					setTimeout("api.zindex()",500);
				}else{
					W.Public.tips({type: 1, content : data.message});
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.em_id;
		pdata.em_code=data.em_code;
		pdata.em_name=data.em_name;
		pdata.em_sex=data.em_sex;
		pdata.em_birthday=data.em_birthday;
		pdata.em_mobile=data.em_mobile;
		pdata.em_state=data.em_state_name;
		pdata.em_dm_name=data.em_dm_name;
		pdata.em_type=data.em_type_name;
		pdata.em_shop_name=data.em_shop_name;
		pdata.em_indate=data.em_indate;
		pdata.em_addr=data.em_addr;
		pdata.em_workdate=data.em_workdate;
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	},
	isIdCardNo:function(obj) {
		var num = obj.value; 
		var idCard =  /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;  
	    //身份证号码为15位或者18位，15位时全为数字，18位前17位为数字，最后一位是校验位，可能为数字或字符X。   
	    if (!idCard.test(num)){ 
	    	W.Public.tips({type: 1, content : "输入的身份证号长度不对，或者号码不符合规定"});
	        setTimeout("api.zindex()",400);
			return false; 
         }
	},
	checkMobile:function(obj){ //验证手机号码
	    var s = obj.value;
		var regu =/^[1-9][0-9]{10}$/; 
		if (!regu.test(s)){ 
			W.Public.tips({type: 1, content : "请输入正确的手机号码!"});
		    obj.value="";
		    setTimeout("api.zindex()",400);
		    return false; 
		}
    },
    doOnlyDouble:function(obj){//验证是否是0-1之间的数字
		var discount = obj.value;
	    if(isNaN(discount)){
	    	W.Public.tips({type: 1, content : "请输入数字!!!"});
		    obj.value="";
		    obj.focus();
		    setTimeout("api.zindex()",400);
		    return;
	    }
	    if(discount<0 || discount>1){
	    	W.Public.tips({type: 1, content : "数请输入0-1之间的数据!!!"});
		    obj.value="";
		    obj.focus();
		    setTimeout("api.zindex()",400);
		    return;
	    }
    }
};
var THISPAGE = {
	init:function (){
		this.storeCombo
		,this.sexCombo
		,this.marryCombo
		,this.stateCombo
		,this.typeCombo
		,this.depotCombo;
		
		this.initSex();
		this.initMarry();
		this.initType();
		this.initDepart();
		this.initShop();
		this.initState();
		this.initDate();
		this.initParam();
		this.initDom();
		this.initEvent();
	},
	initSex:function(){
		var seasonInfo = {
				data:{items:[
					{em_Code:'男',em_Name:'男'},
					{em_Code:'女',em_Name:'女'},
					]}
		}
		THISPAGE.sexCombo = $('#span_Sex').combo({
			data:seasonInfo.data.items,
			value: 'em_Code',
			text: 'em_Name',
			width : 161,
			height: 300,
			listId:'',
			defaultSelected: 0,
			editable: true,
			callback:{
				onChange: function(data){
					$("#em_sex").val(data.em_Code);
					$("#sex").val(data.em_Name);
				}
			}
		}).getCombo();
	},
	initMarry:function(){
		var seasonInfo = {
				data:{items:[
					{Marry_Code:'已婚',Marry_Name:'已婚'},
					{Marry_Code:'未婚',Marry_Name:'未婚'},
					]}
		}
		THISPAGE.marryCombo = $('#span_Marry').combo({
			data:seasonInfo.data.items,
			value: 'Marry_Code',
			text: 'Marry_Name',
			width :161,
			height: 30,
			listId:'',
			defaultSelected: 0,
			editable: true,
			callback:{
				onChange: function(data){
					$("#em_marry").val(data.Marry_Code);
					$("#marray").val(data.Marry_Name);
				}
			}
		}).getCombo();
	},
	initType:function(){
		var seasonInfo = {
				data:{items:[
					{type_Code:'0',type_Name:'导购员'},
					{type_Code:'1',type_Name:'业务员'},
					{type_Code:'2',type_Name:'职员'},
					{type_Code:'3',type_Name:'店长'}
					]}
		}
		THISPAGE.typeCombo = $('#span_Type').combo({
			data:seasonInfo.data.items,
			value: 'type_Code',
			text: 'type_Name',
			width :161,
			height: 30,
			listId:'',
			defaultSelected: 2,
			editable: true,
			callback:{
				onChange: function(data){
					$("#em_type").val(data.type_Code);
					$("#em_type_name").val(data.type_Name);
					if(data.type_Code=="2"){
				        document.getElementById('em_royaltyrate').disabled = true;
				    	document.getElementById('em_royaltyrate').value="" ;
					}else{
					    document.getElementById('em_royaltyrate').disabled = false;
					    document.getElementById('em_royaltyrate').focus(); 
					}
					if(data.type_Code=="0"){//
						$("#Em_Pass_Tr").show();
					}else{
						$("#Em_Pass_Tr").hide();
					}
				}
			}
		}).getCombo();
	},
	initDepart:function(){
		var seasonInfo = {};
		THISPAGE.depotCombo = $('#span_em_dm_Code').combo({
			data:config.BASEPATH +"base/department/listByEmp",
			value: 'em_dm_code',
			text: 'em_dm_name',
			width :161,
			height: 30,
			listId:'',
			defaultSelected: 0,
			editable: true,
			ajaxOptions:{
				formatData: function(data){
					seasonInfo = data.data.items;
					return data.data.items;
				}
			},
			callback:{
				onChange: function(data){
					$("#em_dm_code").val(data.em_dm_code);
					$("#em_dm_name").val(data.em_dm_name);
				}
			}
		}).getCombo(); 
	},
	initShop:function(){
		var seasonInfo = {};
		THISPAGE.storeCombo = $('#span_em_shop_Code').combo({
			data:config.BASEPATH +"base/shop/listByEmp",
			value: 'em_shop_code',
			text: 'em_shop_name',
			width :161,
			height: 30,
			listId:'',
			defaultSelected: 0,
			editable: true,
			ajaxOptions:{
				formatData: function(data){
					seasonInfo = data.data.items;
					return data.data.items;
				}
			},
			callback:{
				onChange: function(data){
					$("#em_shop_code").val(data.em_shop_code);
					$("#em_shop_name").val(data.em_shop_name);
				}
			}
		}).getCombo(); 
	},
	initState:function(){
		var seasonInfo = {
				data:{items:[
					{state_Code:'0',state_Name:'在职'},
					{state_Code:'1',state_Name:'离职'},
					]}
		}
		THISPAGE.stateCombo = $('#span_State').combo({
			data:seasonInfo.data.items,
			value: 'state_Code',
			text: 'state_Name',
			width :161,
			height: 30,
			listId:'',
			defaultSelected: 0,
			editable: true,
			callback:{
				onChange: function(data){
					$("#em_state").val(data.state_Code);
					$("#em_state_name").val(data.state_Name);
				}
			}
		}).getCombo();
	},
	initDate:function(){
		var mydate = new Date();
		var currentdate = mydate.getFullYear() + "-" + (mydate.getMonth()+1) + "-" + mydate.getDate(); 
		document.getElementById('em_birthday').value = currentdate;
		document.getElementById('em_indate').value = currentdate;
		document.getElementById('em_workdate').value = currentdate;
	},
	initParam:function(){
	},
	initDom:function(){
		$("#em_name").val("").focus();
	},
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save();
		});
		$("#btn_close").click(function(){
			api.close();
		});
	}
};
THISPAGE.init();