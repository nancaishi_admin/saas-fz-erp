var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var isRefresh = false;
var _limitMenu = system.MENULIMIT.BASE18;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'base/brand/page';
var handle = {
	operate: function(oper, id){//修改、新增
		var height = 200;
		var width = 250;
		if(oper == 'add'){
			title = '新增';
			url = config.BASEPATH+"base/brand/to_add";
			data = {oper: oper, callback: this.callback};
		}else if(oper == 'edit'){
			title = '修改';
			url = config.BASEPATH+"base/brand/to_update?bd_id="+id;
			data = {oper: oper,callback: this.callback};
		}else if(oper == 'import'){
			title = '颜色批量导入';
			height = 350;
			width = 550;
			url = config.BASEPATH+"base/brand/to_import_brand";
			data = {oper: oper,callback: this.callback};
		}else{
			return false;
		}
		$.dialog({
			title : title,
			content : 'url:'+url,
			data: data,
			width : width,
			height : height,
			max : false,
			min : false,
			cache : false,
			lock: true
		});
	},
	del: function(rowIds){//删除
		if(rowIds != null && rowIds != ''){
		 	$.dialog.confirm('删除后无法恢复，确定删除吗?', function(){
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'base/brand/del',
					data:{"bd_id":rowIds},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){		 		
							$('#grid').jqGrid('delRowData', rowIds);
						}else{
							Public.tips({type: 1, content : "删除失败"});
						}
					}
				});
			});
	   }else{
	      $.dialog.tips("请选择数据!!!",1,"32X32/fail.png");
	   }	
    },
	callback: function(data, oper, dialogWin){
		var gridData = $("#grid").data('gridData');
		if(!gridData){
			gridData = {};
			$("#grid").data('gridData', gridData);
		}
		gridData[data.id] = data;
		if(oper == "edit") {
			$("#grid").jqGrid('setRowData', data.id, data);
			dialogWin && dialogWin.api.close();
		} 
		if(oper == "add"){
			$("#grid").jqGrid('addRowData', data.id, data, 'first');
		}
	},
	formatState:function(val, opt, row){
		if(val == "0"){
			return "正常";
		}else if(val == "1"){
			return "停用";
		}else{
			return val;
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initGrid();
		this.initEvent();	
	},
	initParam:function(){
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
	    	{name: 'operate',label:'操作',width: 70, fixed:true, formatter: Public.operFmatter,align:'center',sortable:false},
	    	{name: 'bd_code',label:'编号',index: 'bd_code',width:80},
	    	{name: 'bd_name',label:'名称',index: 'bd_name',width:120},
	    	{name: 'bd_state',label:'状态',index: 'bd_state',width:80,align:'center',formatter:handle.formatState}
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
			datatype: 'json',
			width: gridWH.w-12,
			height: gridWH.h-20,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:30,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'bd_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	reloadData:function(){
		var param = "?searchContent="+Public.encodeURI($("#SearchContent").val());
		$("#grid").jqGrid('setGridParam',{datatype:"json",url:queryurl+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		$('#SearchContent').on('keyup',function(event){
			if (event.keyCode==13){
				$('#btn-search').click();
			}
		});
		//刷新
		$('#btn-flush').on('click', function(e){
			e.preventDefault();
			$("#SearchContent").val("")
			$('#btn-search').click();
		});
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			};
			THISPAGE.reloadData();
		});
		//新增
		$('#btn-add').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.INSERT)) {
				return ;
			};
			handle.operate('add');
		});
		//修改
		$('#grid').on('click', '.operating .ui-icon-pencil', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.UPDATE)) {
				return ;
			};
			var id = $(this).parent().data('id');
			handle.operate('edit', id);
		});
		//删除
		$('#grid').on('click', '.operating .ui-icon-trash', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.DELETE)) {
				return ;
			};
			var id = $(this).parent().data('id');
			handle.del(id);
		});
		$('#btn-import').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.IMPORT)) {
				return ;
			};
			handle.operate('import');
		});
	}
};
THISPAGE.init();