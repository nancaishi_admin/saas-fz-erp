var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var api = frameElement.api, W = api.opener;
var multiselect = false;
if(undefined != api.data.multiselect){
	multiselect = api.data.multiselect;
}
var _height = api.config.height-132,_width = api.config.width-34;//获取弹出框弹出宽、高
var queryurl = config.BASEPATH+'base/product/list';
var handle = {
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
	},
	initGrid:function(){
		var colModel = [
		    {name: 'pd_code', label:'商品编号',index: 'pd_code', hidden:true},
			{name: 'pd_no', label:'商品货号',index: 'pd_no', width: 70, title: false},
	    	{name: 'pd_name', label:'名称',index: 'pd_name', width: 120, title: false},
	    	{name: 'pd_bd_name',label:'品牌', index: 'pd_bd_name', width: 100, title: true,sortable:true},
			{name: 'pd_tp_name',label:'类型', index: 'pd_tp_name', width: 100, title: true,sortable:true}
	    ];
		$('#grid').jqGrid({
            url:queryurl+"?searchContent="+$("#SearchContent").val(),			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:multiselect,//多选
			viewrecords: true,
			rowNum:30,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'pd_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow: function (rowid, iRow, iCol, e) {
				if(!multiselect){
					api.close();
				}
            }
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'searchContent='+$("#SearchContent").val();
		return params;
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		$('#SearchContent').on('keyup',function(event){
			if (event.keyCode==13){
				$('#btn-search').click();
			}
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
		//重置
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			$("#SearchContent").val('');
		});
	}
};
function doSelect(){
	if(multiselect){//多选
		var ids = $("#grid").jqGrid('getGridParam', 'selarrrow');
		if(ids == null || ids.length == 0){
			Public.tips({type: 2, content : "请选择商品！"});
			return false;
		}
		
		var pd_codes = [];
		for (var i = 0; i < ids.length; i++) {
			var rowData = $('#grid').jqGrid('getRowData',ids[i]);
			pd_codes.push(rowData.pd_code);
		}
		var data = {pd_code:pd_codes.join(","),sp_shop_code:$("#sp_shop_code").val(),discount:$("#discount").val(),sp_shop_type:$("#sp_shop_type").val()};
		$.ajax({
			type: "POST",
	        async: false,
	        url: config.BASEPATH+"shop/price/save_temp_list" ,
	        data:data,
	        cache: false,
	        dataType: "json",
	        success: function (data) {
	        	if(data != "success"){
	        		
	        	}
	        }
		});
		
	}else{//单选
		var selectedId = $("#grid").jqGrid('getGridParam', 'selrow');
		if(selectedId == null || selectedId == ""){
			Public.tips({type: 2, content : "请选择商品！"});
			return false;
		}
		var rowData = $("#grid").jqGrid("getRowData", selectedId);
		return rowData;
	}
}
THISPAGE.init();