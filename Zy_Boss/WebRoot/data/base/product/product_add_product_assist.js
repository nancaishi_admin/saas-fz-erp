var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}

var priceCombo;
var priceInfo = {};
var isChangeCombo;
var isChangeInfo = {};
var isBuyCombo;
var isBuyInfo = {};
var isVipSaleCombo;
var isVipSaleInfo = {};
var isPresentCombo;
var isPresentInfo = {};
var isGiftCombo;
var isGiftInfo = {};
var isPointCombo;
var isPointInfo = {};
var isSaleCombo;
var isSaleInfo = {};

var handle = {
	save:function(){
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"base/product/save_product_assist",
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : data.message});
					W.isRefresh = true;
					setTimeout("api.close()",500);
				}else{
					Public.tips({type: 1, content : data.message});
					setTimeout("api.zindex()",500);
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	update:function(){
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"base/product/update_product_assist",
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : data.message});
					W.isRefresh = true;
					setTimeout("api.close()",500);
				}else{
					Public.tips({type: 1, content : data.message});
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	updateSetCombo:function(){
		isSaleCombo.loadData(isSaleInfo.data.items,['isSale_Code',$("#pda_sale_bak").val()],0);
		isChangeCombo.loadData(isChangeInfo.data.items,['isChange_Code',$("#pda_change_bak").val()],0);
		isGiftCombo.loadData(isGiftInfo.data.items,['isGift_Code',$("#pda_gift_bak").val()],0);
		isVipSaleCombo.loadData(isVipSaleInfo.data.items,['isVipSale_Code',$("#pda_vip_sale_bak").val()],0);
		isPresentCombo.loadData(isPresentInfo.data.items,['isPresent_Code',$("#pda_present_bak").val()],0);
		isBuyCombo.loadData(isBuyInfo.data.items,['isBuy_Code',$("#pda_buy_bak").val()],0);
		isPointCombo.loadData(isPointInfo.data.items,['isPoint_Code',$("#pda_point_bak").val()],0);
	}
};

var comboOpts = {
	value : 'Code',
	text : 'Name',
	width : 142,
	height : 300,
	listHeight : 300,
	listId : '',
	defaultSelected : 0,
	editable : false
};
var ComboUtil = {
	dictCombo : function(){
		$.ajax({
			type:"POST",
			url:config.BASEPATH +"base/dict/listByProduct",
			cache:false,
			dataType:"json",
			success:function(data){
				//价格特性
				priceInfo = data.data.priceitems;
				priceCombo = $('#span_price').combo({
					data:priceInfo,
					value: 'dtl_code',
					text: 'dtl_name',
					width : 142,
					height : 80,
					listId:'',
					defaultSelected: 0,
					editable: true,
					ajaxOptions:{
						formatData: function(data){
							priceInfo = data.data.priceitems;
							return data.data.priceitems;
						}
					},
					callback:{
						onChange: function(data){
							$("#pda_price_name").val(data.dtl_name);
						}
					}
				}).getCombo(); 
				
				if($("#pda_id").val() != ""){
					priceCombo.loadData(priceInfo,['dtl_name',$("#pda_price_name_bak").val()],0);
				}
			}
		 });
	},
	isChangeCombo : function(){
		isChangeInfo = {
				data:{items:[
					{isChange_Code:'0',isChange_Name:'否'},
					{isChange_Code:'1',isChange_Name:'是'},
					]}
		}
		isChangeCombo = $('#span_change').combo({
			data:isChangeInfo.data.items,
			value: 'isChange_Code',
			text: 'isChange_Name',
			width : 142,
			height : 80,
			listId:'',
			defaultSelected: 1,
			editable: true,
			callback:{
				onChange: function(data){
					$("#pda_change").val(data.isChange_Code);
				}
			}
		}).getCombo();
	},
	isBuyCombo : function(){
		isBuyInfo = {
				data:{items:[
					{isBuy_Code:'0',isBuy_Name:'否'},
					{isBuy_Code:'1',isBuy_Name:'是'},
					]}
		}
		isBuyCombo = $('#span_buy').combo({
			data:isBuyInfo.data.items,
			value: 'isBuy_Code',
			text: 'isBuy_Name',
			width : 142,
			height : 80,
			listId:'',
			defaultSelected: 1,
			editable: true,
			callback:{
				onChange: function(data){
					$("#pda_buy").val(data.isBuy_Code);
				}
			}
		}).getCombo();
	},
	isVipSaleCombo : function(){
		isVipSaleInfo = {
				data:{items:[
					{isVipSale_Code:'0',isVipSale_Name:'否'},
					{isVipSale_Code:'1',isVipSale_Name:'是'},
					]}
		}
		isVipSaleCombo = $('#span_vip_sale').combo({
			data:isVipSaleInfo.data.items,
			value: 'isVipSale_Code',
			text: 'isVipSale_Name',
			width : 142,
			height : 80,
			listId:'',
			defaultSelected: 1,
			editable: true,
			callback:{
				onChange: function(data){
					$("#pda_vip_sale").val(data.isVipSale_Code);
				}
			}
		}).getCombo();
	},
	isPresentCombo : function(){
		isPresentInfo = {
				data:{items:[
					{isPresent_Code:'0',isPresent_Name:'否'},
					{isPresent_Code:'1',isPresent_Name:'是'},
					]}
		}
		isPresentCombo = $('#span_present').combo({
			data:isPresentInfo.data.items,
			value: 'isPresent_Code',
			text: 'isPresent_Name',
			width : 142,
			height : 80,
			listId:'',
			defaultSelected: 0,
			editable: true,
			callback:{
				onChange: function(data){
					$("#pda_present").val(data.isPresent_Code);
				}
			}
		}).getCombo();
	},
	isGiftCombo : function(){
		isGiftInfo = {
				data:{items:[
					{isGift_Code:'0',isGift_Name:'否'},
					{isGift_Code:'1',isGift_Name:'是'},
					]}
		}
		isGiftCombo = $('#span_gift').combo({
			data:isGiftInfo.data.items,
			value: 'isGift_Code',
			text: 'isGift_Name',
			width : 142,
			height : 80,
			listId:'',
			defaultSelected: 0,
			editable: true,
			callback:{
				onChange: function(data){
					$("#pda_gift").val(data.isGift_Code);
				}
			}
		}).getCombo();
	},
	isPointCombo : function(){
		isPointInfo = {
				data:{items:[
					{isPoint_Code:'0',isPoint_Name:'否'},
					{isPoint_Code:'1',isPoint_Name:'是'},
					]}
		}
		isPointCombo = $('#span_point').combo({
			data:isPointInfo.data.items,
			value: 'isPoint_Code',
			text: 'isPoint_Name',
			width : 142,
			height : 80,
			listId:'',
			defaultSelected: 1,
			editable: true,
			callback:{
				onChange: function(data){
					$("#pda_point").val(data.isPoint_Code);
				}
			}
		}).getCombo();
	},
	isSaleCombo : function(){
		isSaleInfo = {
				data:{items:[
					{isSale_Code:'0',isSale_Name:'否'},
					{isSale_Code:'1',isSale_Name:'是'},
					]}
		}
		isSaleCombo = $('#span_sale').combo({
			data:isSaleInfo.data.items,
			value: 'isSale_Code',
			text: 'isSale_Name',
			width : 142,
			height : 80,
			listId:'',
			defaultSelected: 1,
			editable: true,
			callback:{
				onChange: function(data){
					$("#pda_sale").val(data.isSale_Code);
				}
			}
		}).getCombo();
	}
};

var THISPAGE = {
	init:function (){
		this.initParam();
		this.initDom();
		this.initDate();
		this.initEvent();
	},
	initParam:function(){
	},
	initDom:function(){
		ComboUtil.dictCombo();
		ComboUtil.isChangeCombo();
		ComboUtil.isBuyCombo();
		ComboUtil.isVipSaleCombo();
		ComboUtil.isPresentCombo();
		ComboUtil.isGiftCombo();
		ComboUtil.isPointCombo();
		ComboUtil.isSaleCombo();
		if($("#pda_id").val() != ""){
			handle.updateSetCombo();
		}
	},
	initDate:function(){
	},
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			var pda_id = $('#pda_id').val();
			if (pda_id==''){
				handle.save();
	    	}else{
	    		handle.update();
	    	}
		});
		$("#btn_close").click(function(){
			api.close();
		});
	}
};
THISPAGE.init();