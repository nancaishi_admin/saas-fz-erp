var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var isRefresh = false;
var api = frameElement.api;
var _limitMenu = system.MENULIMIT.BASE23;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'base/product/assist_template';
var _height = api.config.height-142,_width = api.config.width-34;//获取弹出框弹出宽、高
var handle = {
	getAssistById: function(rowId){
		if(rowId != null && rowId != ''){
			$.ajax({
				type:"POST",
				url:config.BASEPATH +"base/product/get_assist_byid",
				data:"pda_id="+rowId,
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						W.pd_size.value = data.data.pda_size;
						W.pd_fill.value = data.data.pda_fill;
						W.pd_color.value = data.data.pda_color;
						W.pd_place.value = data.data.pda_place;
						W.pd_model.value = data.data.pda_model;
						W.pd_safe.value = data.data.pda_safe;
						W.pd_execute.value = data.data.pda_execute;
						W.pd_grade.value = data.data.pda_grade;
						W.pd_salesman_comm.value = data.data.pda_salesman_comm;
						W.pd_salesman_commrate.value = data.data.pda_salesman_commrate;
						W.pd_gbcode.value = data.data.pda_gbcode;
						W.pd_wash_explain.value = data.data.pda_wash_explain;
						W.pd_batch_price1.value = data.data.pda_batch_price1;
						W.pd_batch_price2.value = data.data.pda_batch_price2;
						W.pd_batch_price3.value = data.data.pda_batch_price3;
						W.priceCombo.loadData(W.priceInfo,['dtl_name',data.data.pda_price_name],0);
						W.pd_price_name.value = data.data.pda_price_name;
						W.isSaleCombo.loadData(W.isSaleInfo.data.items,['isSale_Code',data.data.pda_sale+''],0);
						W.pd_sale.value = data.data.pda_sale;
						W.isChangeCombo.loadData(W.isChangeInfo.data.items,['isChange_Code',data.data.pda_change+''],0);
						W.pd_change.value = data.data.pda_change;
						W.isGiftCombo.loadData(W.isGiftInfo.data.items,['isGift_Code',data.data.pda_gift+''],0);
						W.pd_gift.value = data.data.pda_gift;
						W.isVipSaleCombo.loadData(W.isVipSaleInfo.data.items,['isVipSale_Code',data.data.pda_vip_sale+''],0);
						W.pd_vip_sale.value = data.data.pda_vip_sale;
						W.isPresentCombo.loadData(W.isPresentInfo.data.items,['isPresent_Code',data.data.pda_present+''],0);
						W.pd_present.value = data.data.pda_present;
						W.isBuyCombo.loadData(W.isBuyInfo.data.items,['isBuy_Code',data.data.pda_buy+''],0);
						W.pd_buy.value = data.data.pda_buy;
						W.isPointCombo.loadData(W.isPointInfo.data.items,['isPoint_Code',data.data.pda_point+''],0);
						W.pd_point.value = data.data.pda_point;
					}else{
						Public.tips({type: 1, content : data.message});
					}
				}
			});
		}else{
			Public.tips({type: 1, content : '请选择数据!'});
		}	
	},
	del: function(rowId){//删除
		if(rowId != null && rowId != ''){
			$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
				$.ajax({
					type:"POST",
					url:config.BASEPATH +"base/product/del_product_assist",
					data:"pda_id="+rowId,
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){
							Public.tips({type: 3, content : '删除成功!'});
							THISPAGE.reloadData();
						}else{
							Public.tips({type: 1, content : data.message});
						}
					}
				});
			});
		}else{
			Public.tips({type: 1, content : '请选择数据!'});
		}	
	},
	toSetProductAssist:function(pda_id){//商品资料辅助设置
    	W.$.dialog({
    	   	id:'setProductAssist',
    	   	title:'辅助属性模板设置',
    	   	max: false,
    	   	min: false,
    	   	width:790,
    	   	height:430,
    	   	fixed:false,
			drag: true,
    	   	content:"url:"+config.BASEPATH+"base/product/to_add_product_assist?pda_id="+pda_id,
    	   	close: function(){
    	   		THISPAGE.reloadData();
  		   	}
        });
	},
	operFmatter:function(val, opt, row) {
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-shenpi" title="使用">&#xe618;</i>';
		html_con += '<i class="iconfont i-hand ui-icon-pencil" title="修改">&#xe60c;</i>';
		html_con += '<i class="iconfont i-hand ui-icon-trash" title="删除">&#xe60e;</i>';
		html_con += '</div>';
		return html_con;
	}
}

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
		    {name: 'operate', label:'操作',width: 80, fixed:true, formatter: handle.operFmatter, title: false,sortable:false},
	    	{name: 'pda_name', label:'模板名称',index: 'pda_name', width: 160, title: false,sortable:false},
	    ];
		$('#grid').jqGrid({
			url:queryurl,			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			viewrecords: true,
			rowNum:-1,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id: 'pda_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){
			}
	    });
	},
	buildParams : function(){
	},
	reset:function(){
	},
	reloadData:function(data){
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl}).trigger("reloadGrid");
	},
	initEvent:function(){
		//使用
		$('#grid').on('click', '.operating .ui-icon-shenpi', function(e){
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.getAssistById(id);
		});
		//删除
		$('#grid').on('click', '.operating .ui-icon-trash', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.DELETE)) {
				return ;
			}
			var id = $(this).parent().data('id');
			handle.del(id);
		});
		$("#btn_close").click(function(){
			api.close();
		});
		//新增
		$('#btn-add').on('click', function(e){
			if (!Business.verifyRight(_limitMenu,_menuParam.INSERT)) {
				return ;
			}
			e.preventDefault();
			handle.toSetProductAssist(0);
		});
		//修改
		$('#grid').on('click', '.operating .ui-icon-pencil', function(e){
			if (!Business.verifyRight(_limitMenu,_menuParam.INSERT)) {
				return ;
			}
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.toSetProductAssist(id);
		});
	}
}
THISPAGE.init();