var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var api = frameElement.api, W = api.opener;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
if(!config){
	config = parent.parent.parent.parent.CONFIG;
	system = parent.parent.parent.parent.SYSTEM;
}
var isRefresh = false;
var _limitMenu = system.MENULIMIT.BASE23;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'base/product/barcodelist';
var pdata = {};
var AllowExt=".jpg||.gif||.png||.jpeg||.JPG||.GIF||.PNG||.JPEG";
var FileExt;
/**企业端照片上传功能，限制四张照片*/
var mark = 0;
function imglength(){
	var imgLength = $(".imgsdel").length;
	mark = imgLength;
}
//删除按钮
$(".imgslist li").hover(function(){
	$(this).find("span").show(500);
},function(){
	$(this).find("span").hide(500);
});
//图片预览
$("#uploadify").uploadPreview({ 
	Img: "imgview", 
	Width: 320, 
	Height: 240,
	Callback:function(){
	}
});
var pd_code = $("#pd_code").val();
var handle = {
	del: function(rowId){//删除
		if(rowId != null && rowId != ''){
		 	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'base/emp/del',
					data:{"em_id":rowId},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){
							Public.tips({type: 3, content : '删除成功!'});
							$('#grid').jqGrid('delRowData', rowId);
						}else{
							Public.tips({type: 1, content : '删除失败!'});
						}
					}
				});
			});
	   }else{
		   Public.tips({type: 1, content : '请选择数据!'});
	   }	
    },
    callback: function(data, oper, dialogWin){
		var gridData = $("#grid").data('gridData');
		if(!gridData){
			gridData = {};
			$("#grid").data('gridData', gridData);
		}
		gridData[data.id] = data;
		if(oper == "edit") {
			$("#grid").jqGrid('setRowData', data.id, data);
			dialogWin && dialogWin.api.close();
		} 
		if(oper == "add"){
			$("#grid").jqGrid('addRowData', data.id, data, 'first');
		}
	},
	productimglist : function(){
		var cr_code = $("#cr_code").val();
		var params = "pd_code="+pd_code+"&cr_code="+cr_code;
		$.ajax({
			type:"get",
			url:config.BASEPATH +"base/product/productimglist?"+params,
			data:"",
			cache:true,
			dataType:"json",
			success:function(data){
				$("#imgList").empty();
				var serverPath = $("#serverPath").val();
				if(data.data.items.length>0){
					for(var i = 0;i<data.data.items.length;i++){
						var item = data.data.items[i];
						var li = '<li>'
								+'<span class="imgsdel">'
								+'<a href="javascript:void(0)" title="删除" onclick="handle.deleteimg(\''+item.pdm_id+'\',\''+item.pdm_img_path+'\')"></a>'
								+'</span>'
								+'<img src="'+serverPath+item.pdm_img_path+'" onload="AutoResizeImage(180,132,this);" />'
								+'<b>'+item.cr_name+'</b>'
								+'</li>';
						$("#imgList").append(li);
						$(".imgslist li").hover(function(){
							$(this).find("span").show(500);
						},function(){
							$(this).find("span").hide(500);
						});
					}
				}
				if(mark == 0){
					imglength();
				}
			}
		});
	},
	saveimg:function(){
        try{
        	if(mark >= 4){
        		Public.tips({type: 2, content : '不能超过四张图片！'});
        		return;
        	}
        	
        	if($("#uploadify").val() == ""){
        		Public.tips({type: 2, content : '请选择需要上传的文件！'});
        		return;
        	}
            if(form1.uploadify.value.length == 0){
            	Public.tips({type: 2, content : '请选择需要上传的文件！'});
	            form1.uploadify.focus();
	            return;
            }
            FileExt=form1.uploadify.value;
            FileExt=FileExt.substr(FileExt.lastIndexOf(".")).toLowerCase();
            if(AllowExt!=0&&AllowExt.indexOf(FileExt+"|")==-1){//判断文件类型是否允许上传
            	Public.tips({type: 2, content : '该文件类型不允许上传,请上传'+AllowExt+'类型的文件！'});
			    return;
			}
			
			var file_upl = document.getElementById('uploadify');
			file_upl.select();
			
            document.forms['form1'].encoding="multipart/form-data";
            $('#photo_td1').attr('disabled','disabled');
            var options = {
            	url:config.BASEPATH +"base/product/saveimg",
		        type:"post",
		        timeout:15000,
		        dataType:"json",
		        success:function(data){
		        	if(undefined != data && data.stat == 200){
		        		Public.tips({type: 3, content : data.message});
		        		W.img_refresh = true;
		        		handle.productimglist();
		        		mark ++;
					}else{
						Public.tips({type: 2, content : data.message});
					}
		        	$("#photo_td1").attr("disabled",false);
			    }
		    };
		    $("#form1").ajaxSubmit(options);
           	
        }catch(e){
        	Public.tips({type: 2, content : '文件路径异常！请检查'});
        }
	},
	deleteimg:function(pdm_id,pdm_img_path){
		if($("#imgList li").length<=1){
			Public.tips({type: 2, content : '至少需要保留一张图片!'});
			return;
		}
		if(confirm("确定要删除吗？")){
			$.ajax({
	       		type:"POST",
	       		url:config.BASEPATH +"base/product/deleteimg",
	       		data:"pdm_id="+pdm_id+"&pdm_img_path="+pdm_img_path,
	       		cache:false,
	       		dataType:"json",
	       		success:function(data){
	       			if(undefined != data && data.stat == 200){
	       				Public.tips({type: 3, content : data.message});
	       				handle.productimglist();
	       				mark --;
	       			}else{
	       				Public.tips({type: 2, content : data.message});
	       			}
	       		}
	       	});
		}
	}
};
var ComboUtil = {
	colorCombo:function(){
		$.ajax({
			type:"POST",
			url:config.BASEPATH +"base/product/colorByProductImg?pd_code="+pd_code,
			cache:false,
			dataType:"json",
			success:function(data){
				var colorInfo = {};
				colorCombo = $('#span_color').combo({
					data:data.data.items,
					value: 'cr_code',
					text: 'cr_name',
					width : 82,
					height : 80,
					listId:'',
					defaultSelected: 0,
					editable: true,
					ajaxOptions:{
						formatData: function(data){
							colorInfo = data.data.items;
							return data.data.items;
						}
					},
					callback:{
						onChange: function(data){
							if (data.cr_code != "") {
								$("#cr_code").val(data.cr_code);
		                    }else{
		                    	$("#cr_code").val("");
		                    }
							handle.productimglist();
						}
					}
				}).getCombo(); 
			}
		 });
	}
}
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initEvent();	
	},
	initDom:function(){
		ComboUtil.colorCombo();
	},
	initEvent:function(){
		//删除
		$('#grid').on('click', '.operating .ui-icon-trash', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.DELETE)) {
				return ;
			}
			var id = $(this).parent().data('id');
			handle.del(id);
		});
	}
}
THISPAGE.init();