var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var isRefresh = false;
var _limitMenu = system.MENULIMIT.BASE23;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'base/product/shop_price_list';
var handle = {
};

function buildUrlParams(){
	var params="pd_code="+$("#pd_code").val();
	return params;
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
	},
	initGrid:function(){
		var colModel = [
	    	{name: 'pdp_shop_code',label:'分店编号',index: 'pdp_shop_code',width: 80,title: false},
	    	{name: 'shop_name',label:'分店名称',index: 'shop_name',width: 120,title: false},
	    	{name: 'pdp_sell_price',label:'零售价',index: 'pdp_sell_price',width: 90,align:'right',title: false},
	    	{name: 'pdp_cost_price',label:'成本价',index: 'pdp_cost_price',width: 90,align:'right',title: false},
	    	{name: 'pdp_sort_price',label:'配送价',index: 'pdp_sort_price',width: 90,align:'right',title: false},
	    	{name: 'shop_type',label:'店铺类型', index: 'shop_type', width: 100, title: false}
	    ];
		var params=buildUrlParams(); 
		$('#grid').jqGrid({
			url:queryurl+"?"+params,
			datatype: 'json',
			width:750,
			height: 290,
			gridview: true,
			onselectrow: false,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			cellsubmit: 'clientArray',//可编辑列在编辑完成后会触发保存事件，clientArray表示只保存到表格不提交到服务器
			rowNum:30,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:true,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'pdp_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){
			}
	    });
	},
	reloadData:function(data){
		var param="?";
		param += buildUrlParams(); 
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+param}).trigger("reloadGrid");
	},
	initEvent:function(){
	}
}
THISPAGE.init();