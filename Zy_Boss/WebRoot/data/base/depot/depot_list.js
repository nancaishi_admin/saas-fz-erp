var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var isRefresh = false;
var _limitMenu = system.MENULIMIT.BASE02;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'base/depot/page';
var handle = {
	operate: function(oper, id){//修改、新增
		var height = 240;
		var width = 500;
		if(oper == 'add'){
			title = '新增';
			url = config.BASEPATH+"base/depot/to_add";
			data = {oper: oper, callback: this.callback};
		}else if(oper == 'edit'){
			title = '修改';
			url = config.BASEPATH+"base/depot/to_update?dp_id="+id;
			data = {oper: oper,callback: this.callback};
		}else{
			return false;
		}
		$.dialog({
			title : title,
			content : 'url:'+url,
			data: data,
			width : width,
			height : height,
			max : false,
			min : false,
			cache : false,
			lock: true
		});
	},
	del: function(id){//删除
		if(id != null && id != ''){
			var grid = $("#grid").jqGrid("getRowData", id);
			var dp_default = grid.dp_default;
			if(null != dp_default && 1 == dp_default){
				Public.tips({type: 2, content : "主仓库，无法删除!"});
				return;
			}
		 	$.dialog.confirm('删除后无法恢复，确定删除吗?', function(){
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'base/depot/del',
					data:{"dp_id":id},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){		 		
							$('#grid').jqGrid('delRowData', id);
						}else{
							Public.tips({type: 1, content : data.message});
							setTimeout("api.zindex()",500);
						}
					}
				});
			});
	   }else{
		   Public.tips({type: 1, content : "请选择数据!"});
	   }	
    },
	callback: function(data, oper, dialogWin){
		if(isRefresh){
			THISPAGE.reloadData();
		}
	},
	defaultFmatter:function(val,opt,row){
		var _name = "是";
		if(row.dp_default != 1){
			_name = "否";
		}
		return _name;
	},
	initDefault:function(id,code){
		if(confirm("设置主仓库，该门店其他仓库则更新为从仓库?")){
			if(id != null && id != ''){
				$.ajax({
					type:"POST",
					url:config.BASEPATH+"base/depot/updateDefault",
					data:{"dp_id":id,"dp_shop_code":code},
					dataType:"json",
					success:function(data){
						if(data != null && data.stat == 200){
							Public.tips({type: 0, content : "修改成功!"});
							THISPAGE.reloadData();
						}else{
							Public.tips({type: 0, content : "修改失败!"});
						}
					}
				});
			}
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initGrid();
		this.initParam();
		this.initEvent();	
	},
	initParam:function(){
		shopCombo = $('#spanShop').combo({
			value: 'sp_code',
	        text: 'sp_name',
			width : 158,
			height : 30,
			listId : '',
			defaultSelected: 0,
			editable : true,
			callback : {
				onChange : function(data) {
					$("#dp_shop_code").val(data.sp_code);
				}
			}
		}).getCombo();
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'base/shop/up_sub_list',
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					data.data.unshift({sp_id:"",sp_name:""});
					shopCombo.loadData(data.data);
				}
			}
		});
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
	    	{name: 'operate',label:'操作',width: 70, fixed:true, formatter: Public.operFmatter,align:'center', title: false,sortable:false},
	    	{name: '_dp_default',label:'主仓库',index: '_dp_default',align:'center',formatter:handle.defaultFmatter,width:60, title: false},
	    	{name: 'dp_default',label:'主仓库',index: 'dp_default',hidden:true},
	    	{name: 'dp_code',label:'编号',index: 'dp_code',width:80, title: false},
	    	{name: 'dp_name',label:'名称',index: 'dp_name',width:120, title: false},
	    	{name: 'ty_name',label:'类型',index: 'ty_name',width:70, title: false},
	    	{name: 'dp_shop_code',label:'门店编号',index: 'dp_shop_code',hidden:true},
	    	{name: 'shop_name',label:'所属门店',index: 'shop_name',width:120, title: false},
	    	{name: 'dp_man',label:'联系人',index: 'dp_man',width:70, title: false},
	    	{name: 'dp_tel',label:'电话',index: 'dp_tel',width:100, title: false},
	    	{name: 'dp_mobile',label:'手机',index: 'dp_mobile',width:100, title: false}
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
			datatype: 'json',
			width: gridWH.w-12,
			height: gridWH.h-20,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			page:1,
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:30,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'dp_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	reloadData:function(){
		var param = "?searchContent="+Public.encodeURI($("#SearchContent").val());
		param += "&dp_shop_code="+$("#dp_shop_code").val();
		$("#grid").jqGrid('setGridParam',{datatype:"json",url:queryurl+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		$('#SearchContent').on('keyup',function(){
			if (event.keyCode==13){
				$('#search').click();
			}
		});
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
		//新增
		$('#btn-add').on('click', function(e){
			e.preventDefault();
			handle.operate('add');
		});
		$('#btn-import').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.IMPORT)) {
				return ;
			};
			handle.operate('add');
		});
		$('#btn-default').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.UPDATE)) {
				return ;
			};
			var id = $("#grid").jqGrid('getGridParam', 'selrow');
			var grid = $("#grid").jqGrid("getRowData", id);
			handle.initDefault(id,grid.dp_shop_code);
		});
		//修改
		$('#grid').on('click', '.operating .ui-icon-pencil', function(e){
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.operate('edit', id);
		});
		//删除
		$('#grid').on('click', '.operating .ui-icon-trash', function(e){
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.del(id);
		});
	}
};
THISPAGE.init();