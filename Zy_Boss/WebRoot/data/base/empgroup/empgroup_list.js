var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.SHOP26;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'base/empgroup/list';
var querylisturl = config.BASEPATH+'base/empgroup/listDetails';
var _height = $(parent).height()-255,_width = $(parent).width()-240;

var handle = {
	operate: function(oper, id){//修改、新增
		var height = 425;
		var width = 500;
		if(oper == 'add'){
			title = '新增员工组';
			url = config.BASEPATH+"base/empgroup/to_add";
			data = {oper: oper, callback:this.callback};
		}else if(oper == 'edit'){
			title = '修改员工组';
			url = config.BASEPATH+"base/empgroup/to_update?eg_id="+id;
			data = {oper: oper,callback: this.callback};
		}else{
			return false;
		}
		$.dialog({
			title : title,
			content : 'url:'+url,
			data: data,
			width : width,
			height : height,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				return this.content.ok_Click();
			},
			cancel:true
		});
	},
	del: function(rowId){//删除
		if(rowId != null && rowId != ''){
		 	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'base/empgroup/del',
					data:{"eg_id":rowId},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){
							Public.tips({type: 3, content : '删除成功!'});
							THISPAGE.reloadData();
						}else{
							Public.tips({type: 1, content : '删除失败!'});
						}
					}
				});
			});
	   }else{
		   Public.tips({type: 1, content : '请选择数据!'});
	   }	
    },
	callback: function(data, oper, dialogWin){
		var gridData = $("#grid").data('gridData');
		if(!gridData){
			gridData = {};
			$("#grid").data('gridData', gridData);
		}
		gridData[data.id] = data;
		if(oper == "edit") {
			$("#grid").jqGrid('setRowData', data.id, data);
			dialogWin && dialogWin.api.close();
			Public.tips({type: 3, content : '修改成功！'});
		} 
		if(oper == "add"){
			$("#grid").jqGrid('addRowData', data.id, data);
			dialogWin && dialogWin.api.close();
			Public.tips({type: 3, content : '保存成功！'});
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initGridList();
		this.initEvent();
	},
	initDom:function(){
		
	},
	initGrid:function(){
		var colModel = [
		    {label:'操作',name: 'operate',width: 60, formatter: Public.operFmatter,align:'center'},
	    	{label:'员工组编号',name: 'eg_code',index:'eg_code',width:100},	
	    	{label:'员工组名称',name: 'eg_name',index:'eg_name',width:100},	
	    	{label:'备注',name: 'eg_remark',index:'eg_remark',width:180}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'json',
			width: _width/2,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			page: 1, //只有一页
			pgbuttons:false,
			pgtext:false,
			rowNum:99999,//每页条数
			cmTemplate: {sortable:false,title:false},
			shrinkToFit:false,//表格是否自动填充
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id: 'eg_id'  //图标ID
			},
			loadComplete: function(data){
		    	var ids = $("#grid").getDataIDs();
				if(ids.length>0){
					$("#grid tr:eq(1)").trigger("click");
				}else{
					$("#listGrid").clearGridData();
				}
			},
			loadError: function(xhr, status, error){		
			},
			onCellSelect:function(rowid,iRow,iCol,e){
				var rowData = $('#grid').jqGrid('getRowData', rowid);
				$("#listGrid").jqGrid('setGridParam',{datatype:"json",page:1,url:querylisturl+'?eg_code='+rowData.eg_code}).trigger("reloadGrid");
			}
	    });
	},
	initGridList:function(){
		var colModel = [
	    	{label:'员工编号',name: 'egl_em_code',index:'egl_em_code',width:100},	
	    	{label:'员工名称',name: 'em_name',index:'em_name',width:100}
	    ];
		$('#listGrid').jqGrid({
			datatype: 'local',
			width: _width/2,
			height: _height,
			altRows:true,
			gridview: true,
			onselectrow: false,
			colModel: colModel,
			rownumbers: true,//行号
			viewrecords: true,
			pager: '#listPage',//分页
			recordtext:'{0} - {1} 共 {2} 条',  
			page: 1, //只有一页
			pgbuttons:false,
			pgtext:false,
			rowNum:99999,//每页条数
			shrinkToFit:false,
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id: 'egl_id'  //图标ID
			},
			loadComplete: function(data){
				
			},
			loadError: function(xhr, status, error){
			}
	    });
	},
	buildParams : function(){
		var params = 'searchContent='+Public.encodeURI($("#SearchContent").val());
		return params;
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		$('#btn-add').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.INSERT)) {
				return ;
			}
			handle.operate('add');
		});
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
		//修改
		$('#grid').on('click', '.operating .ui-icon-pencil', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.UPDATE)) {
				return ;
			}
			var id = $(this).parent().data('id');
			handle.operate('edit', id);
		});
		//删除
		$('#grid').on('click', '.operating .ui-icon-trash', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.DELETE)) {
				return ;
			}
			var id = $(this).parent().data('id');
			handle.del(id);
		});
	}
}
THISPAGE.init();