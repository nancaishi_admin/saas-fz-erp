var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var isRefresh = false;
var _limitMenu = system.MENULIMIT.BASE15;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'base/size/listGroup';
var querylisturl = config.BASEPATH+'base/size/listByID';
var handle = {
	operate: function(oper, id){//修改、新增
		var height = 350;
		var width = 340;
		if(oper == 'add'){
			title = '新增';
			url = config.BASEPATH+"base/size/to_add_group";
			data = {oper: oper,callback: this.callback};
		}else if(oper == 'edit'){
			title = '修改';
			url = config.BASEPATH+"base/size/to_update_group?szg_id="+id;
			data = {oper: oper,callback: this.callback};
		}else{
			return false;
		}
		$.dialog({
			title : title,
			content : 'url:'+url,
			data: data,
			width : width,
			height : height,
			max : false,
			min : false,
			cache : false,
			lock: true
		});
	},
	del: function(rowIds){//删除
		if(rowIds != null && rowIds != ''){
		 	$.dialog.confirm('删除后无法恢复，确定删除吗?', function(){
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'base/size/delGroup',
					data:{"szg_id":rowIds},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){		 		
							THISPAGE.reloadData();
						}else{
							Public.tips({type: 1, content : "删除失败"});
						}
					}
				});
			});
	   }else{
	      $.dialog.tips("请选择数据!!!",1,"32X32/fail.png");
	   }	
    },
	callback: function(data, oper, dialogWin){
		if(data == '1'){
			THISPAGE.reloadData();
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initGrid();
		this.initEvent();	
		this.initGridList();
	},
	initParam:function(){
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
	    	{name: 'operate',label:'操作',width: 70, fixed:true, formatter: Public.operFmatter,align:'center', title: false,sortable:false},
	    	{name: 'szg_code',label:'编号',index: 'szg_code',width:70, title: false},
	    	{name: 'szg_name',label:'名称',index: 'szg_name',width:100, title: false}
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
			datatype: 'json',
			width: 320,
			height: gridWH.h-20,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:30,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id: 'szg_id'  //图标ID
			},
			loadComplete: function(data){
				var ids = $("#grid").getDataIDs();
				if(ids.length>0){
					$("#grid tr:eq(1)").trigger("click");
				}else{
					$("#gridList").clearGridData();
				}
			},
			loadError: function(xhr, status, error){		
			},
			onCellSelect:function(rowid,iRow,iCol,e){
				var params = "";
				params += "szl_szg_code="+$("#grid").jqGrid("getRowData",rowid).szg_code;
				$("#gridList").jqGrid('setGridParam',{datatype:"json",url:querylisturl+"?"+params}).trigger("reloadGrid");
			}
	    });
	},
	initGridList:function(){
		var gridWH = Public.setGrid();
		var colModel = [
            {name: 'operate',label:'操作',width: 70, fixed:true, formatter: Public.operDelFmatter,align:'center', title: false,sortable:false},
	    	{label:'编号',name: 'szl_sz_code', index: 'szl_sz_code', width: 80},
	    	{label:'名称',name: 'sz_name', index: 'sz_name', width: 120},
	    	{label:'顺序',name: 'szl_order', index: 'szl_order', width:60}
	    ];
		$('#gridList').jqGrid({
			datatype: 'local',
			width: 400,
			height: gridWH.h-20,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#pageList',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:30,//每页条数
			cmTemplate: {sortable:true,title:true},
			shrinkToFit:false,//表格是否自动填充
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'szl_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	reloadData:function(){
		var param = "?searchContent="+$("#SearchContent").val();
		$("#grid").jqGrid('setGridParam',{datatype:"json",url:queryurl+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		$('#SearchContent').on('keyup',function(event){
			if (event.keyCode==13){
				$('#search').click();
			}
		});
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			};
			THISPAGE.reloadData();
		});
		//新增
		$('#btn-add').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.INSERT)) {
				return ;
			};
			handle.operate('add');
		});
		//修改
		$('#grid').on('click', '.operating .ui-icon-pencil', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.UPDATE)) {
				return ;
			};
			var id = $(this).parent().data('id');
			handle.operate('edit', id);
		});
		//删除
		$('#grid').on('click', '.operating .ui-icon-trash', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.DELETE)) {
				return ;
			};
			var id = $(this).parent().data('id');
			handle.del(id);
		});
	}
};
THISPAGE.init();