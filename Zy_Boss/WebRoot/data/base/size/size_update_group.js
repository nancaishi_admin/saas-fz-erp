var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;;
var handle = {
	initParam:function(){
		var rightItem = form1.right_item.options;
		var sz_codes = [];
		for(i=0;i<rightItem.length;i++){
			var param = {};
			param.sz_code = rightItem.item(i).value;
			sz_codes.push(param);
		}
		return sz_codes;
	},
	save:function(){
		var name = $("#szg_name").val().trim();
	    if(name == ''){
	    	W.Public.tips({type: 1, content : "请输入名称"});
	        $("#szg_name").select().focus();
	     	setTimeout("api.zindex()",400);
	        return;
	    }
	    var codes = handle.initParam();
	    if(null == codes || codes.length == 0){
	    	W.Public.tips({type: 1, content : "请选择尺码!"});
	    	return;
	    }
	    var pdata = {};
	    pdata.szg_name = name;
	    pdata.szg_id = $("#szg_id").val()
	    pdata.szg_code = $("#szg_code").val();
	    pdata.data = codes;
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"base/size/updateGroup",
			data:{"jsondata":JSON.stringify(pdata)},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					W.Public.tips({type: 1, content : data.message});
					setTimeout("api.close()",500);
					handle.loadData(1);
				}else{
					W.Public.tips({type: 1, content : "保存失败!"});
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	loadData:function(data){
		if(callback && typeof callback == 'function'){
			callback(data,oper,window);
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initParam();
		this.initDom();
		this.initEvent();
	},
	initParam:function(){
	},
	initDom:function(){
		$("#szg_name").select().focus();
	},
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save();
		});
		$("#btn_close").click(function(){
			api.close();
		});
	}
};
THISPAGE.init();