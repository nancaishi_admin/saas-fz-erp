var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var api = frameElement.api;
var multiselect = false;
if(undefined != api.data.multiselect){
	multiselect = api.data.multiselect;
}
var _height = api.config.height-132,_width = api.config.width-34;//获取弹出框弹出宽、高
var queryurl = '';
if(api.data.module == 'want'){//店铺发货单
	queryurl = config.BASEPATH+'base/shop/want_list';
}else if(api.data.module == 'allot'){//配货
	queryurl = config.BASEPATH+'base/shop/allot_list';
}else if(api.data.module == 'income'){//其他收入
	queryurl = config.BASEPATH+'base/shop/up_sub_list';
}else if(api.data.module == 'expense'){//费用开支单
	queryurl = config.BASEPATH+'base/shop/up_sub_list';
}else if(api.data.module == 'vipset'){//会员设置
	queryurl = config.BASEPATH+'base/shop/up_sub_list';
}else if(api.data.module == 'wxproduct'){//微信发布商品
	queryurl = config.BASEPATH+'base/shop/up_sub_list';
}else if(api.data.module == 'stock'){//库存报表
	queryurl = config.BASEPATH+'base/shop/up_sub_list';
}else if(api.data.module == 'kpi'){//KPI考核、PK工具
	queryurl = config.BASEPATH+'base/shop/sub_list';
}else if(api.data.module == 'reward'){//奖励
	queryurl = config.BASEPATH+'base/shop/up_sub_list';
}else if(api.data.module == 'sub'){//查询下级店铺
	queryurl = config.BASEPATH+'base/shop/sub_list';
}

var dblClick = false;
var THISPAGE = {
	init:function (){
		this.initGrid();
		this.initEvent();	
	},
	initGrid:function(){
		var colModel = [
			{label:'编号',name: 'sp_code', index: 'sp_code', width: 90, title: false},
	    	{label:'名称',name: 'sp_name', index: 'sp_name', width: 180, title: false},
	    	{label:'店铺类型',name: 'ty_name', index: 'ty_name', width: 80, title: false},
	    	{label:'',name: 'sp_rate', index: 'sp_rate', width: 100, hidden: true},
	    	{label:'',name: 'sp_receivable', index: 'sp_receivable', width: 100, hidden: true},
	    	{label:'',name: 'sp_received', index: 'sp_received', width: 100, hidden: true},
	    	{label:'',name: 'sp_prepay', index: 'sp_prepay', width: 100, hidden: true}
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:multiselect,//多选
			viewrecords: true,
			rowNum:-1,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'sp_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow: function (rowid, iRow, iCol, e) {
				if(!multiselect){
					dblClick = true;
					api.close();
				}
            }
	    });
	},
	reloadData:function(data){
		var param="";
		if(data != null && "" != data){
			param += "?1=1";
			if(data.hasOwnProperty("searchContent") && data.searchContent != ''){
				param += "&searchContent="+Public.encodeURI(data.searchContent);
			}
		}
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		$('#SearchContent').on('keyup',function(event){
			if (event.keyCode==13){
				$('#btn-search').click();
			}
		});
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			var searchContent = $.trim($('#SearchContent').val());
			var pdata = {};
			pdata.searchContent = searchContent;
			THISPAGE.reloadData(pdata);
		});
	}
};
function doSelect(){
	if(multiselect){//多选
		var ids = $("#grid").jqGrid('getGridParam', 'selarrrow');
		if(ids == null || ids.length == 0){
			Public.tips({type: 2, content : "请选择店铺！"});
			return false;
		}
		var result = [];
		for (var i = 0; i < ids.length; i++) {
			result.push($("#grid").jqGrid("getRowData", ids[i]));
		}
		return result;
		
	}else{//单选
		var selectedId = $("#grid").jqGrid('getGridParam', 'selrow');
		if(selectedId == null || selectedId == ""){
			Public.tips({type: 2, content : "请选择店铺！"});
			return false;
		}
		var rowData = $("#grid").jqGrid("getRowData", selectedId);
		return rowData;
	}
}
THISPAGE.init();