var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var isRefresh = false,ty_id = "";
var _limitMenu = system.MENULIMIT.BASE28;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'base/shop/pageZone';
var pdata = {};
var handle = {
	operate: function(oper, id){//修改、新增
		var height = 340;
		var width = 540;
		if(oper == 'add'){
			title = '新增';
			url = config.BASEPATH+"base/shop/to_add_zone";
			data = {oper: oper, callback:this.callback};
		}else if(oper == 'edit'){
			title = '修改';
			url = config.BASEPATH+"base/shop/to_update_zone?sp_id="+id;
			data = {oper: oper,callback: this.callback};
		}else{
			return false;
		}
		$.dialog({
			title : title,
			content : 'url:'+url,
			data: data,
			width : width,
			height : height,
			max : false,
			min : false,
			cache : false,
			lock: true
		});
	},
	del: function(rowId){//删除
		if(rowId != null && rowId != ''){
		 	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'base/shop/del',
					data:{"sp_id":rowId},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){
							Public.tips({type: 0, content : '删除成功!'});
							$('#grid').jqGrid('delRowData', rowId);
						}else{
							Public.tips({type: 1, content : '删除失败!'});
						}
					}
				});
			});
	   }else{
		   Public.tips({type: 3, content : '请选择数据!'});
	   }	
    },
    callback: function(data, oper, dialogWin){
		if(isRefresh){
			THISPAGE.reloadData();
		}
	},
	doState:function(obj,id){
    	var data = $("#grid").jqGrid('getRowData', id);
		var sp_state = data.sp_state,title="";
		if(sp_state == '1'){
			title = "确定要启用吗?";
			sp_state = "0";
		}else{
			title="确定要停用吗?";
			sp_state = "1";
		}
    	$.dialog.confirm(title, function(){
			$.ajax({
				type:"POST",
				url:config.BASEPATH+'base/shop/updateState',
				data:{"sp_id":id,"sp_state":sp_state},
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						$("#grid").jqGrid("setRowData", id,{sp_state:sp_state,_sp_state:''});
						if(sp_state == "1"){
							$(obj).html("&#xe63c;");
							$(obj).removeClass("ui-icon-stop").addClass("ui-icon-begin");
							$(obj).attr("title","启用");
						}else{
							$(obj).html("&#xe60d;");
							$(obj).removeClass("ui-icon-begin").addClass("ui-icon-stop");
							$(obj).attr("title","停用");
						}
						Public.tips({type: 0, content : "状态修改成功!"});
					}else{
						Public.tips({type: 1, content : "状态修改失败!"});
					}
				}
			});
		});
    },
	stateFmatter:function(val,opt,row){
		var _name = "停用";
		if(row.sp_state != 1){
			_name = "正常";
		}
		return _name;
	},
	operFmatter:function (val, opt, row) {
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-pencil" title="修改">&#xe60c;</i>';
		var _state = '&#xe60d;',_title = "停用",_class = "ui-icon-stop";
		if(row.sp_state != 0){
			_state = '&#xe63c;';
			_title = "启用";
			_class = "ui-icon-begin";
		}
		html_con += '<i class="iconfont i-hand '+_class+'" title="'+_title+'">'+_state+'</i>';
		html_con += '</div>';
		return html_con;
	}
};
var THISPAGE = {
	init:function (){
		this.initGrid();
		this.initEvent();	
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
	    	{name: 'operate',label:'操作',width: 70, formatter: handle.operFmatter,align:'center', title: false,sortable:false},
	    	{name: 'sp_code',label:'编号',index: 'sp_code',width:80},
	    	{name: 'sp_name',label:'名称',index: 'sp_name',width:120},
	    	{name: 'ty_name',label:'类型',index: 'ty_name',width:100},
	    	{name: '_sp_state',label:'状态',index: '_sp_state',formatter:handle.stateFmatter,width:60},
	    	{name: 'sp_state',label:'状态隐藏值',index: 'sp_state',hidden:true},
	    	{name: 'sp_shop_type',label:'类型ID',index: 'sp_shop_type',hidden:true},
	    	{name: 'sp_end',label:'有效期',index: 'sp_end',width:100},
	    	{name: 'spi_man',label:'联系人',index: 'spi_man',width:80},
	    	{name: 'spi_mobile',label:'手机号',index: 'spi_mobile',width:100}
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
			datatype: 'json',
			width: gridWH.w-12,
			height: gridWH.h-20,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:30,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'sp_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	reloadData:function(data){
		var param="?1=1";
		param += "&searchContent="+Public.encodeURI($("#SearchContent").val());
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		$('#SearchContent').on('keyup',function(event){
			if (event.keyCode==13){
				$('#btn-search').click();
			}
		});
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		//新增
		$('#btn-add').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.INSERT)) {
				return ;
			}
			handle.operate('add');
		});
		//修改
		$('#grid').on('click', '.operating .ui-icon-pencil', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.UPDATE)) {
				return ;
			}
			var id = $(this).parent().data('id');
			handle.operate('edit', id);
		});
		$('#grid').on('click', '.operating .ui-icon-stop', function(e){
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.doState(this,id);
		});
		$('#grid').on('click', '.operating .ui-icon-begin', function(e){
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.doState(this,id);
		});
		//删除
		$('#grid').on('click', '.operating .ui-icon-trash', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.DELETE)) {
				return ;
			}
			var id = $(this).parent().data('id');
			handle.del(id);
		});
	}
};
THISPAGE.init();