var config = parent.parent.parent.CONFIG,system=parent.parent.parent.SYSTEM;
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var shop_type = config.SHOP_TYPE,allAreaData = null;
var handle = {
	save:function(){
		var sp_name = $("#sp_name").val().trim();
		var sp_shop_type = $("#sp_shop_type").val();		 	
		var sp_rate = $("#sp_rate").val();
		var spi_tel =  $("#spi_tel").val().trim();
		var spi_man = $("#spi_man").val().trim();
		var spi_mobile = $("#spi_mobile").val().trim();
	    var spi_province = $("#spi_province").val();
	    var spi_city = $("#spi_city").val()
	    var spi_town = $("#spi_town").val();
	    var spi_addr = $("#spi_addr").val().trim();  
	    if(sp_name == ''){
	    	W.Public.tips({type: 1, content : "名称不能为空"});
	        $("#sp_name").select().focus();
	     	setTimeout("api.zindex()",400);
	        return;
	    }else{
	    	$("#sp_name").val(sp_name);//防止空格
	    }
		//验证联系人
	    if(spi_man == null || spi_man == ''){
	    	$("#spi_man").focus();
		    W.Public.tips({type: 1, content : "联系人不能为空"});
	        setTimeout("api.zindex()",400);
	        return false;
	    }
		//验证手机号
	    if(spi_mobile == null || spi_mobile == ''){
	        $("#spi_mobile").focus();
	        W.Public.tips({type: 1, content : "手机不能为空"});
	        setTimeout("api.zindex()",400);
	        return false;
	    }else{
	    	if (!W.isMobilePhone(spi_mobile)) {
				W.Public.tips({type: 1, content : "手机号不正确"});
				setTimeout("api.zindex()",500);
				 $("#spi_mobile").select().focus();
				return;
			}
	    }
		//验证店铺电话
	    if(spi_tel == null || spi_tel == ''){
	        $("#spi_tel").focus();
	        W.Public.tips({type: 1, content : "电话不能为空"});
	        setTimeout("api.zindex()",400);
	        return false;
	    }
	    //验证折扣率, 不能为空
	    if(sp_rate == null || sp_rate == ''){
	        $("#sp_rate").focus();
	        W.Public.tips({type: 1, content : "折扣率不能为空"});
	        setTimeout("api.zindex()",400);
	        return;
	    }
	    if(spi_province == "" || spi_city=="" || spi_town==""  || spi_addr==""){
	    	   W.Public.tips({type: 1, content : "店铺地址不完整"});
	    	   setTimeout("api.zindex()",400);
	    	   return;
	    }
	    if(sp_shop_type == ''){
		   W.Public.tips({type: 1, content : "类型不能为空"});
		   setTimeout("api.zindex()",400);
    	   return; 
	    }  
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"base/shop/update",
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					W.isRefresh = true;
					setTimeout("api.close()",300);
					handle.loadData();
				}else{
					W.Public.tips({type: 1, content : data.message});
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	loadData:function(data){
		if(callback && typeof callback == 'function'){
			callback(data,oper,window);
		}
	}
};
var vadiUtil={
	vadiDouble:function(obj) {
		var data = $(obj).val();
		if (isNaN(data)) {
			W.Public.tips({type: 1, content : "请输入数字"});
			$(obj).val("").focus();
			return;
		}
		if (data<0 || data>1) {
			W.Public.tips({type: 1, content : "数字必须0-1之间"});
			$(obj).val("").focus();
			return;
		}
	},
	vadiNumber:function(obj) {
		var data = $(obj).val();
		if (isNaN(data)) {
			W.Public.tips({type: 1, content : "请输入整数"});
			$(obj).val("").focus();
			return;
		}
	}
}

var THISPAGE = {
	init:function (){
		this.initParam();
		this.initCombo();
		this.initEvent();
		this.queryProvince();
	},
	initParam:function(){
		var sp_init = $("#sp_init").val();
		if(null != sp_init && "1" == sp_init){
			$("#sp_init_debt").attr("disabled",true);
		}
	},
	initCombo:function(){
		var sp_main = $("#sp_main").val();
		pdata = [{id:"0",name:"否"},{id:"1",name:"是"}];
		mainCombo = $('#spanMain').combo({
			data:pdata,
	        value: 'id',
	        text: 'name',
	        width : 158,
			height : 30,
			listHeight :120,
			listId : '',
			editable : false,
			defaultSelected:['id',sp_main],
			callback : {
				onChange : function(data) {
					$("#sp_main").val(data.id);
				}
			}
		}).getCombo();
		provinceCombo = $('#spanProvince').combo({
			data:allAreaData,
			value: 'cy_id',
	        text: 'cy_name',
			width : 131,
			height : 30,
			listHeight :120,
			listId : '',
			editable : true,
			callback : {
				onChange : function(data) {
					$("#spi_province").val(data.cy_name);
					THISPAGE.queryCity(data.cy_id);
				}
			}
		}).getCombo();
		cityCombo = $('#spanCity').combo({
			value: 'cy_id',
	        text: 'cy_name',
			width : 131,
			height : 30,
			listHeight :120,
			listId : '',
			defaultSelected : 0,
			editable : false,
			callback : {
				onChange : function(data) {
					$("#spi_city").val(data.cy_name);
					THISPAGE.queryTown(data.cy_id);
				}
			}
		}).getCombo();
		townCombo = $('#spanTown').combo({
			value: 'cy_id',
	        text: 'cy_name',
			width : 131,
			height : 30,
			listHeight :120,
			listId : '',
			defaultSelected : 0,
			editable : false,
			callback : {
				onChange : function(data) {
					$("#spi_town").val(data.cy_name);
				}
			}
		}).getCombo();
	},
	queryProvince:function(){
		var spi_province = $("#spi_province").val();
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"common/city/queryProvince",
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.data != "" && data.data.length > 0){
					allAreaData = data.data;
					allAreaData.unshift({cy_id:"",cy_name:"--请选择--"});
					provinceCombo.loadData(allAreaData,['cy_name',spi_province,0]);
				}
			}
		 });
	},
	queryCity:function(id){
		var spi_city = $("#spi_city").val();
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"common/city/queryCity",
			data:{"cy_id":id},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.data != "" && data.data.length > 0){
					cityCombo.loadData(data.data,['cy_name',spi_city,0]);
				}
			}
		 });
	},
	queryTown:function(id){
		var spi_town = $("#spi_town").val();
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"common/city/queryTown",
			data:{"cy_id":id},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.data != "" && data.data.length > 0){
					townCombo.loadData(data.data,['cy_name',spi_town,0]);
				}
			}
		 });
	},
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save();
		});
		$("#btn_close").click(function(){
			api.close();
		});
	}
};
THISPAGE.init();