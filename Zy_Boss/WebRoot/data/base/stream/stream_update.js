var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var sz_sz_code = api.data.sz_sz_code;
var handle = {
	save:function(){
		var name = $("#se_name").val().trim();
	    if(name == ''){
	    	W.Public.tips({type: 1, content : "请输入名称"});
	        $("#se_name").select().focus();
	     	setTimeout("api.zindex()",400);
	        return;
	    }else{
	    	$("#se_name").val(name);//防止空格
	    }
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"base/stream/update",
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					W.Public.tips({type: 3, content : data.message});
					handle.loadData(data.data);
					setTimeout("api.zindex()",500);
					api.close();
				}else if(data.stat == 304){
					W.Public.tips({type: 1, content : data.message});
					setTimeout("api.zindex()",500);
				}else{
					W.Public.tips({type: 1, content : data.message});
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.se_id;
		pdata.se_code=data.se_code;
		pdata.se_name=data.se_name;
		pdata.se_man=data.se_man;
		pdata.se_tel=data.se_tel;
		pdata.se_addr=data.se_addr;
		pdata.se_mobile=data.se_mobile;
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	},
	checkMobile:function(obj){ //验证手机号码
	    var s = obj.value;
		var regu =/^[1-9][0-9]{10}$/; 
		if(s != ''){
			if (!regu.test(s)){ 
				W.Public.tips({type: 1, content : "请输入正确的手机号码!"});
			    obj.value="";
			    setTimeout("api.zindex()",400);
			    return false; 
			}
		}
    },
    isTel:function(obj){
    	var str = obj.value;
    	var cellphone=/^([\d-+]*)$/;
    	if(str!=''){
    		if(!cellphone.test(str)){
    			W.Public.tips({type: 1, content : "请输入正确的电话号码!"});
    			obj.value="";
        	    obj.focus();
        	    setTimeout("api.zindex()",400)
        	    return;
    		}
    	}
    }
};
var THISPAGE = {
	init:function (){
		this.initParam();
		this.initDom();
		this.initEvent();
	},
	initParam:function(){
	},
	initDom:function(){
		$("#se_name").select().focus();
	},
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save();
		});
		$("#btn_close").click(function(){
			api.close();
		});
	}
};
THISPAGE.init();