var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var api = frameElement.api, W = api.opener;
function doClose(){
	api.close();
}
var handle = {
	importTxt:function(){
		var txt = document.getElementById("txt").value;
    	if(txt==""){
    		$.dialog.tips("请选择需要上传的文件！",1,"32X32/fail.png");
    		return;
    	}
    	//获取后缀名
    	var suffix =/\.[^\.]+/.exec(txt);
    	if(suffix!=".txt"){
    		$.dialog.tips("上传文件类型为txt文本，请重新选择！",2,"32X32/fail.png");
    		return;
    	}
    	$("#btn_import").attr("disabled",true);
    	$("#uploadFile").ajaxSubmit({
			type:"POST",
			url:config.BASEPATH+"common/importTxt",
			dataType:"json",
			success:function(data){
			    if(undefined != data && data.stat == 200){
			    	$('#grid').clearGridData();
			    	$('#grid_fail').clearGridData();
					$("#grid").jqGrid('setGridParam', {datatype : 'local',data : data.data.okdata}).trigger("reloadGrid");
					$("#grid_fail").jqGrid('setGridParam', {datatype : 'local',data : data.data.faildata}).trigger("reloadGrid");
				}else{
					Public.tips({type: 1, content : data.message});
				}
			    $("#btn_import").attr("disabled",false);
			}
		 });
	},
	exportFailData:function(){
		var ids = $("#grid_fail").jqGrid('getDataIDs');
		if(ids == "" || ids.length == 0){
			Public.tips({type: 2, content : "当前不存在错误数据！"});
			return;
		}
		var failData = [];
		for(var i=0;i < ids.length;i++){
			var rowData = $("#grid_fail").jqGrid("getRowData", ids[i]);
			failData.push({barCode:rowData.barCode,amount:rowData.amount});
		}
		$("#failJson").val(JSON.stringify(failData));
		form1.action=config.BASEPATH+'common/export_fail';
		form1.submit();
	},
	importSuccessData:function(){
		var ids = $("#grid").jqGrid('getDataIDs');
		if(ids == "" || ids.length == 0){
			Public.tips({type: 2, content : "当前不存在成功数据！"});
			return;
		}
		var postData = {};
		var datas = [];
		for(var i=0;i < ids.length;i++){
			var rowData = $("#grid").jqGrid("getRowData", ids[i]);
			datas.push({barCode:rowData.barCode,amount:rowData.amount});
		}
		postData.datas = datas;
		if(api.data.postData != undefined){
			postData = $.extend(true,postData,api.data.postData);
		}
		$("#btn_import_succ").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:api.data.saveSuccUrl,
			data:JSON.stringify(postData),
			cache:false,
			dataType:"json",
			contentType:"application/json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : '保存成功'});
					setTimeout("api.close()",500);
					W.isRefresh = true;
				}else{
					Public.tips({type: 1, content : data.message});
				}
				$("#btn_import_succ").attr("disabled",false);
			}
		});
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid_Succ();
		this.initGrid_Fail();
		this.initEvent();
	},
	initDom:function(){
		
	},
	initGrid_Succ:function(){
		var colModel = [
	    	{label:'货号条形码',name: 'barCode', index: 'barCode', width: 100},
	    	{label:'数量',name: 'amount', index: 'amount', width: 60}
	    ];
		$('#grid').jqGrid({
			url:'',
			datatype: 'local',
			width: 300,
			height: 300,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:9999,//每页条数
			cmTemplate: {sortable:true,title:true},
			shrinkToFit:true,//表格是否自动填充
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'barCode'  //图标ID
			},
			loadComplete: function(data){
				
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	initGrid_Fail:function(){
		var colModel = [
	    	{label:'货号条形码',name: 'barCode', index: 'barCode', width: 100},
	    	{label:'数量',name: 'amount', index: 'amount', width: 60}
	    ];
		$('#grid_fail').jqGrid({
			url:'',
			datatype: 'local',
			width: 300,
			height: 300,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page_fail',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:9999,//每页条数
			cmTemplate: {sortable:true,title:true},
			shrinkToFit:true,//表格是否自动填充
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'barCode'  //图标ID
			},
			loadComplete: function(data){
				
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	initEvent:function(){
		
	}
};

THISPAGE.init();