var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var isRefresh = false;
var se_type = $("#se_type").val();
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var queryurl = config.BASEPATH+'batch/sell/temp_list/'+se_type;
var querysumurl = config.BASEPATH+'batch/sell/temp_sum/'+se_type;
var querysizeurl = config.BASEPATH+'batch/sell/temp_size/'+se_type;
var querysizetitleurl = config.BASEPATH+'batch/sell/temp_size_title/'+se_type;

var needListRefresh = false;//列表模式
var needSizeRefresh = false;//尺码模式
var needSumRefresh = false;//汇总模式
var _height = $(parent).height()-353,_width = $(parent).width()-2;
var showMode = {
	display:function(mode){
		var ids = $("#grid").getDataIDs();
		if(ids == undefined || null == ids || ids.length == 0){
			if(mode != 0){
				Public.tips({type: 1, content : "未添加数据，不能切换模式"});
				return;
			}
		}
		$("#CurrentMode").val(mode);
		$("a[name='mode-btn']").removeClass("on");
		$("#list-grid").hide();
		$("#size-grid").hide();
		$("#sum-grid").hide();
		if(mode == 0){
			$("#listBtn").addClass("on");
			$("#list-grid").show();
			showMode.refreshList();
		}else if(mode == 1){
			$("#sizeBtn").addClass("on");
			$("#size-grid").show();
			showMode.refreshSize();
		}else if(mode == 2){
			$("#sumBtn").addClass("on");
			$("#sum-grid").show();
			showMode.refreshSum();
		}
	},
	refreshList:function(){//列表模式刷新数据
		if(!needListRefresh){
    		return;
    	}
    	THISPAGE.reloadGridData();
    	needListRefresh = false;
	},
	refreshSum:function(){
		if(!needSumRefresh){
			return;
		}
		$('#sumGrid').GridUnload();
		THISPAGE.addEvent();
		THISPAGE.initSumGrid();
		needSumRefresh = false;
	},
	refreshSize:function(){
		if(!needSizeRefresh){
			return;
		}
		$.ajax({
			type:"POST",
			url:querysizetitleurl,
			cache:false,
			dataType:"json",
            success: function (data) {
            	if(undefined != data && data.stat == 200){
                    var titles = data.data.titles;
                    var sizeCodeMap = data.data.sizeCodeMap;
                    $('#sizeGrid').GridUnload();
                    THISPAGE.initSizeGroupGrid(titles, sizeCodeMap);
                    THISPAGE.addEvent();
                    needSizeRefresh = false;
				}else{
					Public.tips({type: 1, content : data.message});
				}
            }
        });
	},
	refresh:function(){
		var currentMode = $("#CurrentMode").val();
    	if(currentMode == '0'){//列表模式
    		needListRefresh = true;
    		showMode.refreshList();
        	needSizeRefresh = true;
        	needSumRefresh = true;
    	}else if(currentMode == '1'){//尺码模式
    		needSizeRefresh = true;
    		showMode.refreshSize();
    		needListRefresh = true;
        	needSumRefresh = true;
    	}else if(currentMode == '2'){//汇总模式
    		needSumRefresh = true;
    		showMode.refreshSum();
    		needListRefresh = true;
        	needSizeRefresh = true;
    	}
	}
};

var Choose = {
	importOrder:function(){
		commonDia = $.dialog({
			title : '选择订单',
			content : 'url:'+config.BASEPATH+'batch/sell/to_list_order_dialog/'+se_type,
			data : {},
			width : 1080,
			height : 440,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var mainData = selected.mainData;
				$("#se_client_code").val(mainData.od_client_code);
				$("#se_depot_code").val(mainData.od_depot_code);
				$("#client_name").val(mainData.client_name);
				$("#depot_name").val(mainData.depot_name);
				$("#se_manager").val(mainData.od_manager);
				Choose.doImportOrder(selected.detailIds);
			},
			close:function(){
			},
			cancel:true
		});
	},
	doImportOrder:function(detailIds){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'batch/sell/temp_import_order/'+se_type,
			data:{ids:detailIds.join(",")},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : "导入成功！"});
					showMode.refresh();
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
	},
	doImport:function(){
		isRefresh = false;
		var priceType = THISPAGE.$_priceType.getValue();
		var ci_rate = $("#ci_rate").val();
		var batch_price = $("#batch_price").val();
		var se_client_code = $("#se_client_code").val();
		if(se_client_code == ""){
			Public.tips({type: 2, content : '请选择批发客户!'});
			return;
		}
		$.dialog({ 
		   	id:'import',
		   	title:'盘点机导入',
		   	content:'url:'+config.BASEPATH+'common/to_barcodeimport',
		   	data : {postData:{priceType:priceType,ci_rate:ci_rate,batch_price:batch_price,ci_code:se_client_code},
					saveSuccUrl:config.BASEPATH+"batch/sell/temp_import/"+se_type
		   		},
		   	max: false,
		   	min: false,
		   	lock:true,
		   	width:700,
		   	height:470,
		   	fixed:false,
		   	drag: true,
		   	resize:false,
		   	lock:true,
		   	close: function () {
		   		if(isRefresh){
		   			showMode.refresh();
		   		}
			}
		});
	},
	selectProduct:function(){
		var priceType = THISPAGE.$_priceType.getValue();
		var se_client_code = $.trim($("#se_client_code").val());
		var se_depot_code = $.trim($("#se_depot_code").val());
		if (se_client_code == '') {
			Public.tips({type: 2, content : "请先选择批发客户！"});
	        return;
	    }
		if (se_depot_code == '') {
			Public.tips({type: 2, content : "请先选择仓库！"});
	        return;
	    }
		var ci_rate = $("#ci_rate").val();
		var batch_price = $("#batch_price").val();
		$.dialog({
			title : '商品录入',
			content : 'url:'+config.BASEPATH+'batch/sell/to_select_product/'+se_type,
			data : {priceType:priceType,ci_rate:ci_rate,batch_price:batch_price,dp_code:se_depot_code,ci_code:se_client_code
					,searchContent:$.trim($("#pd_no").val())},
			width : 1080,
			height : 440,
			max : false,
			min : false,
			cache : false,
			lock: true,
			close:function(){
				showMode.refresh();
			}
		});
	},
	barCode:function(){
		var CurrentMode = $("#CurrentMode").val();
		if(CurrentMode == 1){//若是尺码模式调整到列表模式
			showMode.display(0);
		}
		var barcode = $.trim($("#barcode").val());
		var barcode_amount = $.trim($("#barcode_amount").val());
		if(barcode == ""){
			return;
		}
		if (barcode_amount == "" || isNaN(barcode_amount)) {
			Public.tips({type: 2, content : '请正确输入数量!'});
	        $('#barcode_amount').select();
	        return;
	    }
		var se_client_code = $("#se_client_code").val();
		var se_depot_code = $("#se_depot_code").val();
		if(se_client_code == ""){
			Public.tips({type: 2, content : '请选择批发客户!'});
			return;
		}
		if(se_depot_code == ""){
			Public.tips({type: 2, content : '请选择仓库!'});
			return;
		}
		var priceType = THISPAGE.$_priceType.getValue();
		var params = "";
		params += "barcode="+barcode;
		params += "&amount="+barcode_amount;
		params += "&ci_code="+se_client_code;
		params += "&priceType="+priceType;
		params += "&ci_rate="+$("#ci_rate").val();
		params += "&batch_price="+$("#batch_price").val();
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"batch/sell/temp_save_bybarcode/"+se_type,
			data:params,
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					if(data.data.result == 3){//条码不存在
						Choose.barcodeNotExist(barcode);
						return;
					}
					needSizeRefresh = true;
					$("#barcode").val("");
					var temp = data.data.temp;
					//列表模式
					if(data.data.result == 1){//update
						var rowData = $("#grid").jqGrid("getRowData", temp.sel_id);
						rowData.sel_amount = temp.sel_amount;
						rowData.sel_unitmoney = rowData.sel_amount * rowData.sel_unitprice;
						rowData.sel_retailmoney = rowData.sel_amount * rowData.sel_retailprice;
						rowData.sel_costmoney = rowData.sel_amount * rowData.sel_costprice;
						rowData.sel_rebatemoney = rowData.sel_amount * rowData.sel_rebateprice;
						$("#grid").delRowData(temp.sel_id);
						$("#grid").addRowData(temp.sel_id, rowData, 'first');
						THISPAGE.gridTotal();
					}else if(data.data.result == 2){//add
						$("#grid").addRowData(temp.sel_id, temp, 'first');
						THISPAGE.gridTotal();
					}
					//汇总模式
			    	var ids = $("#sumGrid").jqGrid('getDataIDs');
	            	var oldSumRowData = null;
	            	var sum_id = null;
					for(var i=0;i < ids.length;i++){
						var sumRowData = $("#sumGrid").jqGrid("getRowData", ids[i]);
						if(sumRowData.pd_code == temp.pd_code && sumRowData.sel_pi_type == '0'){
							oldSumRowData = sumRowData;
							sum_id = ids[i];
							break;
						}
					}
					if(oldSumRowData != null){
						oldSumRowData.sel_amount = parseInt(oldSumRowData.sel_amount) + parseInt(barcode_amount);
						oldSumRowData.sel_unitmoney = oldSumRowData.sel_amount * oldSumRowData.sel_unitprice;
						oldSumRowData.sel_retailmoney = oldSumRowData.sel_amount * oldSumRowData.sel_retailprice;
						oldSumRowData.sel_costmoney = oldSumRowData.sel_amount * oldSumRowData.sel_costprice;
						oldSumRowData.sel_rebatemoney = oldSumRowData.sel_amount * oldSumRowData.sel_rebateprice;
						$("#sumGrid").jqGrid('setRowData', sum_id, oldSumRowData);
						THISPAGE.sumGridTotal();
					}else{
						$("#sumGrid").addRowData(temp.sel_id, temp, 'first');
						THISPAGE.sumGridTotal();
					}
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
	},
	barcodeNotExist:function(barcode){
		$("#barcode").val("无此商品").select();
		var BarCodeCheckNotPass = document.getElementById("BarCodeCheckNotPass");
		if(BarCodeCheckNotPass.style.display == 'none'){
			BarCodeCheckNotPass.style.display = '';
			
			var barCodeDiv=$("#BarCodeCheckNotPass");
			var barCodeDivTop=$("#barcode_amount").offset().top
				,barCodeDivLeft=$("#barcode_amount").offset().left;
			barCodeDiv.css(
			{
				"top":barCodeDivTop-150
				,"left":barCodeDivLeft+30
			}
			);
		}
		var CheckNotPassText = document.getElementById("CheckNotPassText");
		if($("#CheckNotPassTextShow").val().indexOf(barcode+";") > -1){
			return;
		}
		CheckNotPassText.innerHTML = CheckNotPassText.innerHTML + "<p>" + barcode + "</p>";
		$("#CheckNotPassTextShow").val($("#CheckNotPassTextShow").val()+barcode+";");
	}
};

var Utils = {
	doQueryClient : function(){
		commonDia = $.dialog({
			title : '选择批发客户',
			content : 'url:'+config.BASEPATH+'batch/client/to_list_dialog',
			data : {multiselect:false},
			width : 650,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#se_client_code").val(selected.ci_code);
				$("#client_name").val(selected.ci_name);
				$("#ci_rate").val(selected.ci_rate);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#se_client_code").val(selected.ci_code);
					$("#client_name").val(selected.ci_name);
					$("#ci_rate").val(selected.ci_rate);
				}
			},
			cancel:true
		});
	},
	doQueryClientShop : function(){//选择批发客户店铺
		var ci_code = $("#se_client_code").val();
		if(ci_code == ""){
			Public.tips({type: 2, content : '请选择批发客户!'});
			return;
		}
		commonDia = $.dialog({
			title : '选择批发客户店铺',
			content : 'url:'+config.BASEPATH+'batch/client/to_list_shop_dialog?ci_code='+ci_code,
			data : {multiselect:false},
			width:750,
		   	height:420,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#se_client_shop_code").val(selected.cis_code);
				$("#client_shop_name").val(selected.cis_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#se_client_shop_code").val(selected.cis_code);
					$("#client_shop_name").val(selected.cis_name);
				}
			},
			cancel:true
		});
	},
	doQueryDepot : function(){
		commonDia = $.dialog({
			title : '选择仓库',
			content : 'url:'+config.BASEPATH+'base/depot/to_list_dialog_bymodule',
			data : {multiselect:false,module:'batch'},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#se_depot_code").val(selected.dp_code);
					$("#depot_name").val(selected.dp_name);
				}
			},
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#se_manager").val(selected.em_name);
				}
			},
			cancel:true
		});
	},
	doQueryStream : function(){
		commonDia = $.dialog({
			title : '选择物流公司',
			content : 'url:'+config.BASEPATH+'base/stream/to_list_dialog',
			data : {multiselect:false},
			width : 350,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#se_stream_code").val(selected.se_code);
				$("#stream_name").val(selected.se_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#se_stream_code").val(selected.se_code);
					$("#stream_name").val(selected.se_name);
				}
			},
			cancel:true
		});
	}
};

var handle = {
	temp_updateRemarkById:function(rowid,remark){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'batch/sell/temp_updateRemarkById',
			data:{sel_id:rowid,sel_remark:Public.encodeURI(remark)},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					var rowData = $("#grid").jqGrid("getRowData", rowid);
					var ids = $("#sumGrid").jqGrid('getDataIDs');
					for(var i=0;i < ids.length;i++){
						var row = $("#sumGrid").jqGrid("getRowData", ids[i]);
						if(row.sel_pd_code == rowData.sel_pd_code && row.sel_pi_type == rowData.sel_pi_type){
							$("#sumGrid").jqGrid('setRowData',ids[i],{sel_remark:remark});
						}
					}
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
    },
    temp_updateRemarkByPdCode:function(pd_code,remark,sel_pi_type){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'batch/sell/temp_updateRemarkByPdCode/'+se_type,
			data:{sel_pd_code:pd_code,sel_remark:Public.encodeURI(remark),sel_pi_type:sel_pi_type},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					var ids = $("#grid").jqGrid('getDataIDs');
					for(var i=0;i < ids.length;i++){
						var row = $("#grid").jqGrid("getRowData", ids[i]);
						if(row.sel_pd_code == pd_code && row.sel_pi_type == sel_pi_type){
							$("#grid").jqGrid('setRowData',ids[i],{sel_remark:remark});
						}
					}
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
    },
	temp_updateAmount:function(rowid,amount){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'batch/sell/temp_updateAmount',
			data:{sel_id:rowid,sel_amount:amount},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					needSizeRefresh = true;
					needSumRefresh = true;
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
    },
    temp_updatePrice:function(pd_code,unitPrice,sel_pi_type){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'batch/sell/temp_updatePrice/'+se_type,
			data:{sel_pd_code:pd_code,sel_unitprice:unitPrice,sel_pi_type:sel_pi_type},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					showMode.refresh();
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
    },
    temp_updateRebatePrice:function(pd_code,rebatePrice){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'batch/sell/temp_updateRebatePrice/'+se_type,
			data:{sel_pd_code:pd_code,sel_rebateprice:rebatePrice},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					showMode.refresh();
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
    },
	temp_update : function(id,type){
		var priceType = THISPAGE.$_priceType.getValue();
		var se_client_code = $.trim($("#se_client_code").val());
		var se_depot_code = $.trim($("#se_depot_code").val());
		if (se_client_code == '') {
			Public.tips({type: 2, content : "请先选择批发客户！"});
	        return;
	    }
		if (se_depot_code == '') {
			Public.tips({type: 2, content : "请先选择仓库！"});
	        return;
	    }
		var ci_rate = $("#ci_rate").val();
		var batch_price = $("#batch_price").val();
		var params = {};
 		var rowData;
 		if(type == '0'){
 			rowData = $("#grid").jqGrid("getRowData", id);
 			params.pd_code = rowData.sel_pd_code;
 		}else if(type == '1'){
			rowData = $("#sizeGrid").jqGrid("getRowData", id);
			params.pd_code = rowData.pd_code;
		}else if(type == '2'){
			rowData = $("#sumGrid").jqGrid("getRowData", id);
			params.pd_code = rowData.sel_pd_code;
		}
 		params.sel_pi_type = rowData.sel_pi_type;
 		params.priceType = priceType;
 		params.ci_rate = ci_rate;
 		params.batch_price = batch_price;
 		params.ci_code = se_client_code;
 		params.dp_code = se_depot_code;
		$.dialog({
			title : '修改商品',
			content : 'url:'+config.BASEPATH+'batch/sell/to_temp_update/'+se_type,
			data : params,
			width : 850,
			height : 440,
			max : false,
			min : false,
			cache : false,
			lock: true,
			close:function(){
				showMode.refresh();
			}
		});
	},
	temp_del: function(rowId){//删除
		if(rowId != null && rowId != ''){
			var rowData = $("#grid").jqGrid("getRowData", rowId);
		 	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'batch/sell/temp_del',
					data:{"sel_id":rowId},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){
							Public.tips({type: 3, content : '删除成功!'});
							$('#grid').jqGrid('delRowData', rowId);
							THISPAGE.gridTotal();
							needSizeRefresh = true;
							needSumRefresh = true;
						}else{
							Public.tips({type: 1, content : data.message});
						}
					}
				});
			});
	   }else{
		   Public.tips({type: 1, content : '请选择数据!'});
	   }	
    },
    temp_delByPiCode: function(rowId,type){//删除
		if(rowId != null && rowId != ''){
		 	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
		 		var params = {};
		 		var rowData;
	    		if(type == '1'){
	    			rowData = $("#sizeGrid").jqGrid("getRowData", rowId);
	    			params.pd_code = rowData.pd_code;
	    			params.sel_pi_type = rowData.sel_pi_type;
	    			params.cr_code = rowData.cr_code;
	    			params.br_code = rowData.br_code;
	    		}else if(type == '2'){
	    			rowData = $("#sumGrid").jqGrid("getRowData", rowId);
	    			params.pd_code = rowData.sel_pd_code;
	    			params.sel_pi_type = rowData.sel_pi_type;
	    		}
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'batch/sell/temp_delByPiCode/'+se_type,
					data:params,
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){
							Public.tips({type: 3, content : '删除成功!'});
							showMode.refresh();
						}else{
							Public.tips({type: 1, content : data.message});
						}
					}
				});
			});
	   }else{
		   Public.tips({type: 1, content : '请选择数据!'});
	   }	
    },
    temp_clear:function(){
    	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
			$.ajax({
				type:"POST",
				url:config.BASEPATH+'batch/sell/temp_clear/'+se_type,
				data:{},
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : '删除成功!'});
						$("#grid").clearGridData();
						THISPAGE.gridTotal();
						$("#sumGrid").clearGridData();
						THISPAGE.sumGridTotal();
						if($("#CurrentMode").val() == '1'){
							showMode.refresh();
						}else{
							needSizeRefresh = true;
						}
					}else{
						Public.tips({type: 1, content : data.message});
					}
				}
			});
		});
    },
	save:function(isdraft){
		if (!$("#form1").valid()) {
			return;
		}
		var ids = $("#grid").jqGrid('getDataIDs');
		if (ids.length == 0){
			Public.tips({type: 2, content : '单据明细不存在，请选择货号！'});
			return;
		}
		$("#se_isdraft").val(isdraft);
		$("#btn-save").attr("disabled",true);
		var saveUrl = config.BASEPATH+"batch/sell/save";
		if($("#se_id").val() != undefined){
			saveUrl = config.BASEPATH+"batch/sell/update";
		}
		$.ajax({
			type:"POST",
			url:saveUrl,
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : '保存成功'});
					handle.loadData(data.data);
					setTimeout("api.zindex()",500);
				}else{
					Public.tips({type: 1, content : data.message});
					setTimeout("api.zindex()",500);
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.se_id;
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	},
	operFmatter :function(val, opt, row){
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-pencil" title="修改">&#xe60c;</i>';
		html_con += '<i class="iconfont i-hand ui-icon-trash" title="删除">&#xe60e;</i>';
		html_con += '<i class="iconfont i-hand ui-icon-image" title="图片">&#xe654;</i>';
		html_con += '</div>';
		return html_con;
	},
	formatGift :function(val, opt, row){
		if(row.sel_pi_type == 1){
			return '赠品';
		}
		return '';
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.addEvent();
		this.initGrid();
		this.initEvent();
	},
	initDom:function(){
		$("#se_make_date").val(config.TODAY);
		$("#se_delivery_date").val(config.TODAY);
		this.initProperty();
		this.initBatchPrice();
		$('#span_batch_price').getCombo().disable(true);
		this.$_priceType = $("#priceTypeTd").cssRadio({ callback: function($_obj){
			 pricrType = $_obj.find("input").val();
			if(pricrType == 1){//最近批发价
				$('#span_batch_price').getCombo().disable(true);
				$("#ci_rate").attr("disabled",true);
			}else if(pricrType == 2){//折扣率
				$('#span_batch_price').getCombo().disable(true);
				$("#ci_rate").attr("disabled",false);
			}else if(pricrType == 3){//批发价
				$('#span_batch_price').getCombo().disable(false);
				$("#ci_rate").attr("disabled",true);
			}
		}});
		needListRefresh = false;//列表模式
        needSizeRefresh = true;//尺码模式
        needSumRefresh = true;//汇总模式
        this.selectRow={};
	},
	initProperty:function(){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"base/dict/list?dtl_upcode=KEY_BILL",
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.data != "" && data.data.length > 0){
					$('#span_property').combo({
						value : 'dtl_code',
						text : 'dtl_name',
						width : 155,
						listHeight : 300,
						listId : '',
						defaultSelected : 0,
						editable : false,
						callback : {
							onChange : function(data) {
								$("#se_property").val(data.dtl_name);
							}
						}
					}).getCombo().loadData(data.data,["dtl_code",$("#se_property").val(),0]);
				}
			}
		 });
	},
	initBatchPrice:function(){
		var data = [ 
            {Code : '0',Name : '批发价'}, 
            {Code : '1',Name : '批发价1'},
            {Code : '2',Name : '批发价2'},
            {Code : '3',Name : '批发价3'}
        ];
		$('#span_batch_price').combo({
			value : 'Code',
			text : 'Name',
			width : 70,
			listHeight : 300,
			listId : '',
			defaultSelected : 0,
			editable : false,
			callback : {
				onChange : function(data) {
					$("#batch_price").val(data.Code);
				}
			}
		}).getCombo().loadData(data);
	},
	gridTotal:function(){
    	var grid=$('#grid');
		var sel_amount=grid.getCol('sel_amount',false,'sum');
    	var sel_unitmoney=grid.getCol('sel_unitmoney',false,'sum');
    	var sel_costmoney=grid.getCol('sel_costmoney',false,'sum');
    	var sel_retailmoney=grid.getCol('sel_retailmoney',false,'sum');
    	var sel_rebatemoney=grid.getCol('sel_rebatemoney',false,'sum');
    	grid.footerData('set',{sel_amount:sel_amount,sel_unitmoney:sel_unitmoney,sel_costmoney:sel_costmoney,sel_retailmoney:sel_retailmoney,sel_rebatemoney:sel_rebatemoney});
    	$("#se_amount").val(sel_amount);
    	$("#se_money").val(PriceLimit.formatByBatch(sel_unitmoney));
    	$("#se_rebatemoney").val(PriceLimit.formatByBatch(sel_rebatemoney));
    	calcReceivable();
    },
    sumGridTotal:function(){
    	var grid=$('#sumGrid');
    	var sel_amount=grid.getCol('sel_amount',false,'sum');
    	var sel_unitmoney=grid.getCol('sel_unitmoney',false,'sum');
    	var sel_costmoney=grid.getCol('sel_costmoney',false,'sum');
    	var sel_retailmoney=grid.getCol('sel_retailmoney',false,'sum');
    	var sel_rebatemoney=grid.getCol('sel_rebatemoney',false,'sum');
    	grid.footerData('set',{sel_amount:sel_amount,sel_unitmoney:sel_unitmoney,sel_costmoney:sel_costmoney,sel_retailmoney:sel_retailmoney,sel_rebatemoney:sel_rebatemoney});
    	$("#se_amount").val(sel_amount);
    	$("#se_money").val(PriceLimit.formatByBatch(sel_unitmoney));
    	$("#se_rebatemoney").val(PriceLimit.formatByBatch(sel_rebatemoney));
    	calcReceivable();
    },
	initGrid:function(){
		var self=this;
		var colModel = [
		    {label:'操作',name:'operate', width: 70, fixed:true, formatter: handle.operFmatter, sortable:false,align:'center'},
		    {label:'',name: 'sel_pd_code', index: 'sel_pd_code', hidden:true},
	    	{label:'商品货号',name: 'pd_no', index: 'pd_no', width: 100},
	    	{label:'商品名称',name: 'pd_name', index: 'pd_name', width: 100},
	    	{label:'颜色',name: 'cr_name', index: 'cr_name', width: 60},
	    	{label:'尺码',name: 'sz_name', index: 'sz_name', width: 60},
	    	{label:'杯型',name: 'br_name', index: 'br_name', width: 60},
	    	{label:'单位',name: 'pd_unit', index: 'pd_unit', width: 60},
	    	{label:'数量',name: 'sel_amount', index: 'sel_amount', width: 70,align:'right',sorttype: 'int',editable:true},
	    	{label:'批发价',name: 'sel_unitprice', index: 'sel_unitprice', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByBatch,editable:PriceLimit.hasBatch()},
	    	{label:'批发金额',name: 'sel_unitmoney', index: 'sel_unitmoney', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByBatch},
	    	{label:'成本价',name: 'sel_costprice', index: 'sel_costprice', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByCost},
	    	{label:'成本金额',name: 'sel_costmoney', index: 'sel_costmoney', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByCost},
	    	{label:'单件返点',name: 'sel_rebateprice', index: 'sel_rebateprice', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByBatch,editable:PriceLimit.hasBatch()},
	    	{label:'返点金额',name: 'sel_rebatemoney', index: 'sel_rebatemoney', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByBatch},
	    	{label:'零售价',name: 'sel_retailprice', index: 'sel_retailprice', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatBySell},
	    	{label:'零售金额',name: 'sel_retailmoney', index: 'sel_retailmoney', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatBySell},
	    	{label:'赠品',name: 'gift', index: 'sel_pi_type', width: 60,align:'center',formatter: handle.formatGift},
	    	{label:'',name: 'sel_pi_type', index: 'sel_pi_type', width: 100,hidden:true},
	    	{label:'折扣率',name: 'sel_rate', index: 'sel_rate', width: 70,align:'right',sorttype: 'int',formatter: PriceLimit.formatByBatch_Sell},
	    	{label:'订单编号',name: 'sel_order_number', index: 'sel_order_number', width: 140},
	    	{label:'备注',name: 'sel_remark', index: 'sel_remark', width: 180,editable:true}
	    ];
		$('#grid').jqGrid({
			url:queryurl,
			loadonce:true,
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:9999,//每页条数
			cmTemplate: {sortable:true,title:true},
			shrinkToFit:false,//表格是否自动填充
			cellEdit: true,
            cellsubmit: 'clientArray',
            footerrow: true,
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'sel_id'  //图标ID
			},
			loadComplete: function(data){
				var ids = $("#grid").getDataIDs();
				THISPAGE.gridTotal();
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
				handle.temp_update(rowid, '0');
			},
			formatCell:function (rowid, cellname, value, iRow, iCol){
            	self.selectRow.value=value;
				return value;
			},
			beforeSaveCell :function(rowid, cellname, value, iRow, iCol){
				if (cellname == 'sel_amount' && self.selectRow.value != value){
					if(value == '' || isNaN(value) || parseInt(value) <= 0){
						return self.selectRow.value;
					} 
					if(parseInt(self.selectRow.value) == parseInt(value)){
						return self.selectRow.value;
					}
					if(parseFloat(value).toFixed(0).length > 9|| parseInt(value) >1e+9){
						return self.selectRow.value;
					}
					handle.temp_updateAmount(rowid, parseInt(value));
					return parseInt(value);
				}else if((cellname == 'sel_unitprice' || cellname == 'sel_rebateprice')  && self.selectRow.value != value){
					if(value == '' || isNaN(value) || parseFloat(value) < 0){
						return self.selectRow.value;
					}
					if(parseFloat(self.selectRow.value) == parseFloat(value)){
						return self.selectRow.value;
					}
					if(parseFloat(value).toFixed(2).length > 11 || parseFloat(value) >1e+11){
						return self.selectRow.value;
					}
					var rowData = $("#grid").jqGrid("getRowData", rowid);
					if(rowData.sel_pi_type != '0'){//
						Public.tips({type: 2, content : '赠品不能修改价格'});
						return self.selectRow.value;
					}
					if(cellname == 'sel_unitprice'){
						handle.temp_updatePrice(rowData.sel_pd_code, parseFloat(value).toFixed(2), rowData.sel_pi_type);
					}else if(cellname == 'sel_rebateprice'){
						handle.temp_updateRebatePrice(rowData.sel_pd_code, parseFloat(value).toFixed(2));
					}
					return parseFloat(value).toFixed(2);
				}else if(cellname == 'sel_remark' && self.selectRow.value != value){
					handle.temp_updateRemarkById(rowid, value);
				}
				
				return value;
			},
			afterSaveCell:function(rowid, cellname, value, iRow, iCol){
				if (cellname == 'sel_amount' && self.selectRow.value != value){
					var rowData = $("#grid").jqGrid("getRowData", rowid);
					rowData.sel_unitmoney = rowData.sel_amount * rowData.sel_unitprice;
					rowData.sel_retailmoney = rowData.sel_amount * rowData.sel_retailprice;
					rowData.sel_costmoney = rowData.sel_amount * rowData.sel_costprice;
					rowData.sel_rebatemoney = rowData.sel_amount * rowData.sel_rebateprice;
					rowData.sel_rate = rowData.sel_unitprice / rowData.sel_retailprice;
					$("#grid").jqGrid('setRowData', rowid, rowData);
					THISPAGE.gridTotal();
				}
				
			}
	    });
	},
	initSumGrid:function(){
		var self=this;
		var colModel = [
		    {label:'操作',name:'operate', width: 70, fixed:true, formatter: handle.operFmatter, sortable:false,align:'center'},
	    	{label:'',name: 'sel_pd_code', index: 'sel_pd_code', hidden:true},
	    	{label:'商品货号',name: 'pd_no', index: 'pd_no', width: 100},
	    	{label:'商品名称',name: 'pd_name', index: 'pd_name', width: 100},
	    	{label:'品牌',name: 'bd_name', index: 'pd_bd_code', width: 80},
	    	{label:'类别',name: 'tp_name', index: 'pd_tp_code', width: 80},
	    	{label:'单位',name: 'pd_unit', index: 'pd_unit', width: 60},
	    	{label:'数量',name: 'sel_amount', index: 'sel_amount', width: 70,align:'right',sorttype: 'int'},
	    	{label:'批发价',name: 'sel_unitprice', index: 'sel_unitprice', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByBatch,editable:PriceLimit.hasBatch()},
	    	{label:'批发金额',name: 'sel_unitmoney', index: 'sel_unitmoney', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByBatch},
	    	{label:'成本价',name: 'sel_costprice', index: 'sel_costprice', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByCost},
	    	{label:'成本金额',name: 'sel_costmoney', index: 'sel_costmoney', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByCost},
	    	{label:'单件返点',name: 'sel_rebateprice', index: 'sel_rebateprice', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByBatch,editable:PriceLimit.hasBatch()},
	    	{label:'返点金额',name: 'sel_rebatemoney', index: 'sel_rebatemoney', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByBatch},
	    	{label:'零售价',name: 'sel_retailprice', index: 'sel_retailprice', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatBySell},
	    	{label:'零售金额',name: 'sel_retailmoney', index: 'sel_retailmoney', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatBySell},
	    	{label:'赠品',name: 'gift', index: 'sel_pi_type', width: 60,align:'center',formatter: handle.formatGift},
	    	{label:'',name: 'sel_pi_type', index: 'sel_pi_type', width: 100,hidden:true},
	    	{label:'折扣率',name: 'sel_rate', index: 'sel_rate', width: 70,align:'right',sorttype: 'int',formatter: PriceLimit.formatByBatch_Sell},
	    	{label:'备注',name: 'sel_remark', index: 'sel_remark', width: 180,editable:true}
	    ];
		$('#sumGrid').jqGrid({
			url:querysumurl,
			loadonce:true,
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#sumPage',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:9999,//每页条数
			cmTemplate: {sortable:true,title:true},
			shrinkToFit:false,//表格是否自动填充
			cellEdit: true,
            cellsubmit: 'clientArray',
            footerrow: true,
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'sel_id'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.sumGridTotal();
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
				handle.temp_update(rowid, '2');
			},
			formatCell:function (rowid, cellname, value, iRow, iCol){
            	self.selectRow.value=value;
				return value;
			},
			beforeSaveCell :function(rowid, cellname, value, iRow, iCol){
				if((cellname == 'sel_unitprice' || cellname == 'sel_rebateprice') && self.selectRow.value != value){
					if(value == '' || isNaN(value) || parseFloat(value) < 0){
						return self.selectRow.value;
					}
					if(parseFloat(self.selectRow.value) == parseFloat(value)){
						return self.selectRow.value;
					}
					if(parseFloat(value).toFixed(2).length > 11 || parseFloat(value) >1e+11){
						return self.selectRow.value;
					}
					var rowData = $("#sumGrid").jqGrid("getRowData", rowid);
					if(rowData.sel_pi_type != '0'){//
						Public.tips({type: 2, content : '赠品不能修改价格'});
						return self.selectRow.value;
					}
					if(cellname == 'sel_unitprice'){
						handle.temp_updatePrice(rowData.sel_pd_code, parseFloat(value).toFixed(2), rowData.sel_pi_type);
					}else if(cellname == 'sel_rebateprice'){
						handle.temp_updateRebatePrice(rowData.sel_pd_code, parseFloat(value).toFixed(2));
					}
					return parseFloat(value).toFixed(2);
				}else if(cellname == 'sel_remark' && self.selectRow.value != value){
					var rowData = $("#sumGrid").jqGrid("getRowData", rowid);
					handle.temp_updateRemarkByPdCode(rowData.sel_pd_code, value, rowData.sel_pi_type);
				}
				return value;
			},
			afterSaveCell:function(rowid, cellname, value, iRow, iCol){
				
			}
	    });
	},
	initSizeGroupGrid:function(dyColNames, sizeCodeMap) {
		var self=this;
        var headers = [];
        var dyns = dyColNames;
        var dySize = dyns.length;
        var dms = [];
        for (var i = 0; i < dyns.length; i++) {
            var dys = dyns[i];
            for (var j = 0; j < dys.length; j++) {
                var dyss = null;
                dyss = dys[j].split('_AND_');
                if (i == 0 && j == 0) {
                    dyns[i][j] = dyss[0];
                }
                headers.push(dyss[1]);
                dms.push({label:dyss[1],name: dyss[0], index: dyss[0], width: 45, title: false, align: 'right', sorttype: 'int', sortable: false});
            }
            break;
        }
		var colModel = [
            {label:'',name: 'sizeGroupCode', hidden: true},
            {label:'',name: 'pd_code', index: 'pd_code', width: 100,hidden:true},
            {label:'',name: 'cr_code', index: 'cr_code', width: 100,hidden:true},
            {label:'',name: 'br_code', index: 'br_code', width: 100,hidden:true},
            {label:'操作',name:'operate', width: 70, fixed:true, formatter: handle.operFmatter, sortable:false,align:'center'},
            {label:'商品货号',name: 'pd_no', index: 'pd_no', width: 100},
	    	{label:'商品名称',name: 'pd_name', index: 'pd_name', width: 100},
	    	{label:'颜色',name: 'cr_name', index: 'sel_cr_code', width: 80},
	    	{label:'杯型',name: 'br_name', index: 'sel_br_code', width: 80},
        ];
        colModel = colModel.concat(dms);
        colModel = colModel.concat([
            {label:'数量',name: 'Amount_Total',	index:'Amount_Total',	width: 70, align: 'right', sorttype: 'int', sortable: false},
            {label:'批发价',name: 'unit_price', index: 'unit_price', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByBatch,editable:PriceLimit.hasBatch()},
	    	{label:'批发金额',name: 'unit_money', index: 'unit_money', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByBatch},
	    	{label:'成本价',name: 'cost_price', index: 'cost_price', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByCost},
	    	{label:'成本金额',name: 'cost_money', index: 'cost_money', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByCost},
	    	{label:'单件返点',name: 'rebate_price', index: 'rebate_price', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByBatch,editable:PriceLimit.hasBatch()},
	    	{label:'返点金额',name: 'rebate_money', index: 'rebate_money', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByBatch},
	    	{label:'零售价',name: 'retail_price', index: 'retail_price', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatBySell},
	    	{label:'零售金额',name: 'retail_money', index: 'retail_money', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatBySell},
	    	{label:'赠品',name: 'gift', index: 'gift', width: 60,align:'center',formatter: handle.formatGift},
	    	{label:'',name: 'sel_pi_type', index: 'sel_pi_type', width: 100,hidden:true}
	    ]);
        $('#sizeGrid').jqGrid({
			url: querysizeurl,
			loadonce:true,
            datatype: 'json',
            autowidth: true,
			height: _height,
            gridview: true,
            colModel: colModel,
            rownumbers: true,//行号
            pager: '#sizePage',//分页
            pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
            viewrecords: true,
            rowNum: 9999,//每页条数
            shrinkToFit: false,//表格是否自动填充
            cellEdit: true,
            cellsubmit: 'clientArray',
            footerrow: true,
            userDataOnFooter: true,
			jsonReader: {
				root: 'data.data',
				userdata: 'data.userData',
				repeatitems : false,
				id:'id'
			},
			loadComplete: function(data) {
				
			},
			gridComplete:function(){
				Public.resizeSpecifyGrid('sizeGrid', 290 + (dyns.length * 32),2);
			},
			loadError: function(xhr, status, error) {
                Public.tips({type: 1, content : '操作失败了哦，服务可能已过期，请按F5刷新！'});
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
				handle.temp_update(rowid, '1');
			},
			formatCell:function (rowid, cellname, value, iRow, iCol){
            	self.selectRow.value=value;
				return value;
			},
			beforeSaveCell :function(rowid, cellname, value, iRow, iCol){
				if((cellname == 'unit_price' || cellname == 'rebate_price') && self.selectRow.value != value){
					if(value == '' || isNaN(value) || parseFloat(value) < 0){
						return self.selectRow.value;
					}
					if(parseFloat(self.selectRow.value) == parseFloat(value)){
						return self.selectRow.value;
					}
					if(parseFloat(value).toFixed(2).length > 11 || parseFloat(value) >1e+11){
						return self.selectRow.value;
					}
					var rowData = $("#sizeGrid").jqGrid("getRowData", rowid);
					if(rowData.sel_pi_type != '0'){//
						Public.tips({type: 2, content : '赠品不能修改价格'});
						return self.selectRow.value;
					}
					if(cellname == 'unit_price'){
						handle.temp_updatePrice(rowData.pd_code, parseFloat(value).toFixed(2), rowData.sel_pi_type);
					}else if(cellname == 'rebate_price'){
						handle.temp_updateRebatePrice(rowData.pd_code, parseFloat(value).toFixed(2));
					}
					return parseFloat(value).toFixed(2);
				}
				return value;
			},
			afterSaveCell:function(rowid, cellname, value, iRow, iCol){
				
			},
			onPaging: function(pgButton) {
				
			},
            onSelectRow: function (rowId, status) {
                var sizeG = $("#sizeGrid").jqGrid("getRowData", rowId).sizeGroupCode;
                var bg = $('[sizeGCode=' + sizeG + ']').css("background");
                if (bg == 'none') {
                    return;
                }
                $("[sizeGCode]").css("background-color", '#eee');
                $('[sizeGCode=' + sizeG + ']').css({ "background": "none", "background-color": "#F8FF94"});
            }
	    });
        if (dySize >= 2) {
        	$('#sizeGrid').jqGrid('setGroupHeaders', {
                useColSpanStyle: true,
                groupHeaders:[]
            });
            $('#sizeGrid').jqGrid("setComplexGroupHeaders", {
                complexGroupHeaders: {startColumnName: dyns[0][0], numberOfColumns: dyns[0].length},
                rowspan: dySize,
                topTitle:true
            });
            for (var key in sizeCodeMap) {
                $('#sizeGrid').jqGrid("setComplexGroupHeaders",{
                    complexGroupHeaders: {startColumnName: dyns[0][0], numberOfColumns: dyns[0].length},
                    rowspan: dySize,
                    sizeGroupCode: key,
                    titleTextGroup:sizeCodeMap[key],
                    topTitle:false
                });
            }
        }else{
        	$('#sizeGrid').jqGrid('setGroupHeaders', {
                useColSpanStyle: true,
                groupHeaders: [{startColumnName: dyns[0][0], numberOfColumns: dyns[0].length, titleText: '尺码'}]
            });
        }
    },
	reloadGridData: function () {
        $("#grid").jqGrid('setGridParam', {datatype: 'json', page: 1, url: queryurl}).trigger("reloadGrid");
    },
	initEvent:function(){
		$('#btn-import').on('click', function(e){
			e.preventDefault();
			Choose.doImport();
		});
		$('#btn-import-order').on('click', function(e){
			e.preventDefault();
			Choose.importOrder();
		});
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save(0);
		});
		$("#btn_close").click(function(e){
			e.preventDefault();
			api.close();
		});
		//清除
        $('#btnClear').click(function(e){
        	e.preventDefault();
        	handle.temp_clear();
		});
        //修改-列表模式
        $('#grid').on('click', '.operating .ui-icon-pencil', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
        	handle.temp_update(id, '0');
        });
        
        //删除-列表模式
		$('#grid').on('click', '.operating .ui-icon-trash', function(e) {
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.temp_del(id);
		});
		
		$('#grid').on('click', '.operating .ui-icon-image', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
        	viewProductImg($("#grid").jqGrid("getRowData", id).sel_pd_code);
        });
	},
	addEvent:function(){
		//移除事件，防止重复绑定
		$('#sumGrid').off('click','.operating .ui-icon-pencil');
		$('#sumGrid').off('click','.operating .ui-icon-trash');
		$('#sumGrid').off('click','.operating .ui-icon-image');
		$('#sizeGrid').off('click','.operating .ui-icon-pencil');
		$('#sizeGrid').off('click','.operating .ui-icon-trash');
		$('#sizeGrid').off('click','.operating .ui-icon-image');
        //修改-尺码模式
        $('#sizeGrid').on('click', '.operating .ui-icon-pencil', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
			handle.temp_update(id, '1');
        });
        //修改-汇总模式
        $('#sumGrid').on('click', '.operating .ui-icon-pencil', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
			handle.temp_update(id, '2');
        });
		
		 //删除-汇总模式
		$('#sumGrid').on('click', '.operating .ui-icon-trash', function(e) {
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.temp_delByPiCode(id, '2');
		});
		//删除-尺码模式
		$('#sizeGrid').on('click', '.operating .ui-icon-trash',function(e) {
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.temp_delByPiCode(id, '1');
		});
		
		$('#sizeGrid').on('click', '.operating .ui-icon-image', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
        	viewProductImg($("#sizeGrid").jqGrid("getRowData", id).pd_code);
        });
        $('#sumGrid').on('click', '.operating .ui-icon-image', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
        	viewProductImg($("#sumGrid").jqGrid("getRowData", id).sel_pd_code);
        });
	}
};

THISPAGE.init();