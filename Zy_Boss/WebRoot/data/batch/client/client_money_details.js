var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var queryurl = config.BASEPATH+'batch/client/pageMoneyDetails';
var _height = $(parent).height()-285,_width = $(parent).width()-192;

var Utils = {
	doQueryClient : function(){
		commonDia = $.dialog({
			title : '选择批发客户',
			content : 'url:'+config.BASEPATH+'batch/client/to_list_dialog',
			data : {multiselect:false},
			width : 650,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#client_code").val(selected.ci_code);
				$("#client_name").val(selected.ci_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#client_code").val(selected.ci_code);
					$("#client_name").val(selected.ci_name);
				}
			},
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#manager").val(selected.em_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#manager").val(selected.em_name);
				}
			},
			cancel:true
		});
	}
};

var handle = {
	openDetail:function(number,dl_type){
    	var url = '';
    	var data = {oper: 'readonly'};
    	if(dl_type == '0' || dl_type == '1'){
    		url = config.BASEPATH+"batch/sell/to_view/"+number;
    	}else if(dl_type == '3'){
    		url = config.BASEPATH+"batch/fee/to_view/"+number;
    	}else if(dl_type == '4' || dl_type == '5'){
    		url = config.BASEPATH+"batch/prepay/to_view/"+number;
    	}else if(dl_type == '6'){
    		url = config.BASEPATH+"batch/settle/to_view/"+number;
    	}
    	if(url == ''){
    		return;
    	}
    	$.dialog({
			title : false,
			content : 'url:'+url,
			data: data,
			max : false,
			min : false,
			cache : false,
			lock: false
		}).max();
	},
	formatNumberLink : function(val, opt, row){
		var btnHtml = '<a href="javascript:void(0);" onclick="javascript:handle.openDetail(\''+row.number+'\',' + row.type + ');">'+row.number+'</a>';
		return btnHtml;
	},
	formatType:function(val, opt, row){
		if(val == 0){
			return '销售单';
		}else if(val == 1){
			return '退货单';
		}else if(val == 2){
			return '期初单';
		}else if(val == 3){
			return '费用单';
		}else if(val == 4){
			return '预收款单';
		}else if(val == 5){
			return '退款单';
		}else if(val == 6){
			return '结算单';
		}
		return val;
	},
	formatPayState:function(val, opt, row){
		if(val == 0){
			return '未付款';
		}else if(val == 1){
			return '未付清';
		}else if(val == 2){
			return '已付清';
		}
		return val;
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		dateRedioClick("theDate");
		this.initTypeDom();
		this.initPayStateDom();
	},
	initTypeDom:function(){
		this.$_type_all = $("#type_all").cssCheckbox({ 
			callback: function($_obj){
	        	if($_obj.find("input")[0].checked){//选中
	        		THISPAGE.$_type_0.chkAll();
	        		THISPAGE.$_type_1.chkAll();
	        		THISPAGE.$_type_2.chkAll();
	        		THISPAGE.$_type_3.chkAll();
	        		THISPAGE.$_type_4.chkAll();
	        		THISPAGE.$_type_5.chkAll();
	        		$("#type").val("");
	        	}else{//取消
	        		THISPAGE.$_type_0.chkNot();
	        		THISPAGE.$_type_1.chkNot();
	        		THISPAGE.$_type_2.chkNot();
	        		THISPAGE.$_type_3.chkNot();
	        		THISPAGE.$_type_4.chkNot();
	        		THISPAGE.$_type_5.chkNot();
	        		$("#type").val("-1");
	        	}
			}
		});
		this.$_type_0 = $("#type_0").cssCheckbox({ 
			callback: function($_obj){
				THISPAGE.buildTypeValue();
			}
		});
		this.$_type_1 = $("#type_1").cssCheckbox({ 
			callback: function($_obj){
				THISPAGE.buildTypeValue();
			}
		});
		this.$_type_2 = $("#type_2").cssCheckbox({ 
			callback: function($_obj){
				THISPAGE.buildTypeValue();
			}
		});
		this.$_type_3 = $("#type_3").cssCheckbox({ 
			callback: function($_obj){
				THISPAGE.buildTypeValue();
			}
		});
		this.$_type_4 = $("#type_4").cssCheckbox({ 
			callback: function($_obj){
				THISPAGE.buildTypeValue();
			}
		});
		this.$_type_5 = $("#type_5").cssCheckbox({ 
			callback: function($_obj){
				THISPAGE.buildTypeValue();
			}
		});
	},
	buildTypeValue:function(){
		var type_all = THISPAGE.$_type_all.chkVal().join() ? true : false;
		var type_0 = THISPAGE.$_type_0.chkVal().join() ? true : false;
		var type_1 = THISPAGE.$_type_1.chkVal().join() ? true : false;
		var type_2 = THISPAGE.$_type_2.chkVal().join() ? true : false;
		var type_3 = THISPAGE.$_type_3.chkVal().join() ? true : false;
		var type_4 = THISPAGE.$_type_4.chkVal().join() ? true : false;
		var type_5 = THISPAGE.$_type_5.chkVal().join() ? true : false;
		var count = 0;
		var type = '';
    	if(type_0){type +='0,';count++;}
    	if(type_1){type +='1,';count++;}
    	if(type_2){type +='2,';count++;}
    	if(type_3){type +='3,';count++;}
    	if(type_4){type +='4,';count++;}
    	if(type_5){type +='5,';count++;}
    	if(type.length > 0){
    		type = type.substring(0, type.length-1);
    	}else{
    		type = '-1';
    	}
    	if(count == 6){
    		THISPAGE.$_type_all.chkAll();
    		type = '';
    	}else{
    		if(type_all){
    			THISPAGE.$_type_all.chkNot();
    		}
    	}
    	$("#type").val(type);
	},
	initPayStateDom:function(){
		this.$_pay_state_all = $("#pay_state_all").cssCheckbox({ 
			callback: function($_obj){
	        	if($_obj.find("input")[0].checked){//选中
	        		THISPAGE.$_pay_state_0.chkAll();
	        		THISPAGE.$_pay_state_1.chkAll();
	        		THISPAGE.$_pay_state_2.chkAll();
	        		$("#pay_state").val("");
	        	}else{//取消
	        		THISPAGE.$_pay_state_0.chkNot();
	        		THISPAGE.$_pay_state_1.chkNot();
	        		THISPAGE.$_pay_state_2.chkNot();
	        		$("#pay_state").val("-1");
	        	}
			}
		});
		this.$_pay_state_0 = $("#pay_state_0").cssCheckbox({ 
			callback: function($_obj){
				THISPAGE.buildPayStateValue();
			}
		});
		this.$_pay_state_1 = $("#pay_state_1").cssCheckbox({ 
			callback: function($_obj){
				THISPAGE.buildPayStateValue();
			}
		});
		this.$_pay_state_2 = $("#pay_state_2").cssCheckbox({ 
			callback: function($_obj){
				THISPAGE.buildPayStateValue();
			}
		});
		
	},
	buildPayStateValue:function(){
		var pay_state_all = THISPAGE.$_pay_state_all.chkVal().join() ? true : false;
		var pay_state_0 = THISPAGE.$_pay_state_0.chkVal().join() ? true : false;
		var pay_state_1 = THISPAGE.$_pay_state_1.chkVal().join() ? true : false;
		var pay_state_2 = THISPAGE.$_pay_state_2.chkVal().join() ? true : false;
		var count = 0;
		var pay_state = '';
    	if(pay_state_0){pay_state +='0,';count++;}
    	if(pay_state_1){pay_state +='1,';count++;}
    	if(pay_state_2){pay_state +='2,';count++;}
    	if(pay_state.length > 0){
    		pay_state = pay_state.substring(0, pay_state.length-1);
    	}else{
    		pay_state = '-1';
    	}
    	if(count == 3){
    		THISPAGE.$_pay_state_all.chkAll();
    		pay_state = '';
    	}else{
    		if(pay_state_all){
    			THISPAGE.$_pay_state_all.chkNot();
    		}
    	}
    	$("#pay_state").val(pay_state);
	},
	initGrid:function(){
		var colModel = [
	    	{label:'日期',name: 'make_date',index: 'make_date',width:100,align:'center'},
	    	{label:'单据编号',name: 'numberlink',index: 'number',width:150,formatter: handle.formatNumberLink},
	    	{label:'客户名称',name: 'client_name',index: 'client_name',width:150},
	    	{label:'类型',name: 'type',index: 'type',width:100,formatter: handle.formatType,align:'center'},
	    	{label:'付款状态',name: 'pay_state',index: 'pay_state',width:100,formatter: handle.formatPayState,align:'center'},
	    	{label:'总金额',name: 'money',index: 'money',width:80,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'优惠金额',name: 'discount_money',index: 'discount_money',width:80,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'使用预收款',name: 'prepay',index: 'prepay',width:80,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'已收金额',name: 'received',index: 'received',width:80,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'未收金额',name: 'receivable',index: 'receivable',width:80,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'经办人',name: 'manager',index: 'manager',width:100}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.BASEROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:false,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			userDataOnFooter: true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
                userdata: 'data.data',
				repeatitems : false,
				id: 'id'  //图标ID
			},
			loadComplete: function(data){
		    	$('#grid').footerData('set',{make_date:"合计"});
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += '&begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&client_code='+$("#client_code").val();
		params += '&type='+$("#type").val();
		params += '&pay_state='+$("#pay_state").val();
		params += '&manager='+Public.encodeURI($("#manager").val());
		params += '&number='+Public.encodeURI($("#number").val());
		return params;
	},
	reset:function(){
		THISPAGE.$_date.setValue(0);
		$("#client_name").val("");
		$("#client_code").val("");
		$("#manager").val("");
		$("#number").val("");
		THISPAGE.$_type_all.chkAll();
		THISPAGE.$_type_0.chkAll();
		THISPAGE.$_type_1.chkAll();
		THISPAGE.$_type_2.chkAll();
		THISPAGE.$_type_3.chkAll();
		THISPAGE.$_type_4.chkAll();
		THISPAGE.$_type_5.chkAll();
		$("#type").val("");
		THISPAGE.$_pay_state_all.chkAll();
		THISPAGE.$_pay_state_0.chkAll();
		THISPAGE.$_pay_state_1.chkAll();
		THISPAGE.$_pay_state_2.chkAll();
		$("#pay_state").val("");
	},
	reloadData:function(){
		var type = $("#type").val();
		var pay_state = $("#pay_state").val();
		if(type == "-1"){
			Public.tips({type: 2, content : "请选择单据类型"});
			return;
		}
		if(pay_state == "-1"){
			Public.tips({type: 2, content : "请选择付款状态"});
			return;
		}
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();