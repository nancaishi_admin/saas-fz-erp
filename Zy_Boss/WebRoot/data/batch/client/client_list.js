var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var isRefresh = false;
var _limitMenu = system.MENULIMIT.BATCH04;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'batch/client/page';

var handle = {
	operate: function(oper, id){//修改、新增
		var height = 380;
		var width = 650;
		if(oper == 'add'){
			title = '新增客户';
			url = config.BASEPATH+"batch/client/to_add";
			data = {oper: oper, callback:this.callback};
		}else if(oper == 'edit'){
			title = '修改客户';
			url = config.BASEPATH+"batch/client/to_update?ci_id="+id;
			data = {oper: oper,callback: this.callback};
		}else{
			return false;
		}
		$.dialog({
			title : title,
			content : 'url:'+url,
			data: data,
			width : width,
			height : height,
			max : false,
			min : false,
			cache : false,
			lock: true
		});
	},
	del: function(rowId,ci_code){//删除
		if(rowId != null && rowId != ''){
		 	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'batch/client/del',
					data:{"ci_id":rowId,"ci_code":ci_code},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){
							Public.tips({type: 3, content : '删除成功!'});
							$('#grid').jqGrid('delRowData', rowId);
						}else{
							Public.tips({type: 1, content : '删除失败!'});
						}
					}
				});
			});
	   }else{
		   Public.tips({type: 1, content : '请选择数据!'});
	   }	
    },
    callback: function(data, oper, dialogWin){
		var gridData = $("#grid").data('gridData');
		if(!gridData){
			gridData = {};
			$("#grid").data('gridData', gridData);
		}
		gridData[data.id] = data;
		if(oper == "edit") {
			$("#grid").jqGrid('setRowData', data.id, data);
			dialogWin && dialogWin.api.close();
			Public.tips({type: 3, content : '修改成功！'});
		} 
		if(oper == "add"){
			$("#grid").jqGrid('addRowData', data.id, data, 'first');
			dialogWin && dialogWin.api.close();
			Public.tips({type: 3, content : '保存成功！'});
		}
	},
	toClientShop:function(ci_id, ci_code){//店铺信息
		$.dialog({ 
		   	title:'客户店铺信息',
		   	max: false,
		   	min: false,
		   	width:850,
		   	height:520,
		   	fixed:false,
		   	drag: true,
		   	resize:false,
		   	content:'url:'+config.BASEPATH+'batch/client/to_client_shop_list?ci_id='+ci_id+'&ci_code='+ci_code,
		   	lock:true
	    });
	},
	operFmatter:function (val, opt, row) {
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-pencil" title="修改">&#xe60c;</i>';
		html_con += '<i class="iconfont i-hand ui-icon-trash" title="删除">&#xe60e;</i>';
		html_con += "<i class='iconfont i-hand ui-icon-shop' title='拥有店铺'>&#xe610;</i>";
		html_con += '</div>';
		return html_con;
	},
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
			{label:'操作',name: 'operate',width: 70, formatter: handle.operFmatter,align:'center', title: false,sortable:false},
			{label:'客户编号',name: 'ci_code', index: 'ci_code', width:80, title: false,fixed:true},
			{label:'客户名称',name: 'ci_name', index: 'ci_name', width: 140, title: false},
			{label:'联系人',name: 'ci_man', index: 'ci_man', width: 100, title: false,align:'left'},
			{label:'电话',name: 'ci_tel', index: 'ci_tel', width: 140, title: false},
			{label:'手机号码',name: 'ci_mobile', index: 'ci_mobile', width: 100, title: false},
			{label:'期初金额',name: 'ci_init_debt', index: 'ci_init_debt',sorttype: 'float',width:100, align:'right',formatter: PriceLimit.formatMoney},
			{label:'折扣率',name: 'ci_rate', index: 'ci_rate',sorttype: 'float',width:80, title: false,align:'right'},
			{label:'批发周期',name: 'ci_batch_cycle', index: 'ci_batch_cycle', width:80, title: false,align:'center'},
			{label:'结算周期',name: 'ci_settle_cycle', index: 'ci_settle_cycle', width: 80, title: false,align:'center'},
			{label:'保证金',name: 'ci_earnest', index: 'ci_earnest', width:80, title: false,align:'right'},
			{label:'装修押金',name: 'ci_deposit', index: 'ci_deposit', width:80, title: false,align:'right'},
			{label:'地址',name: 'ci_addr', index: 'ci_addr', width:150, title: false,align:'left'},
			{label:'备注信息',name: 'ci_remark', index: 'ci_remark', width: 180, title: false}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'json',
			width: gridWH.w-12,
			height: gridWH.h-20,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.BASEROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'ci_id'
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams:function(){
		var params = '';
		params += "searchContent="+Public.encodeURI($.trim($('#SearchContent').val()));
		return params;
	},
	reloadData:function(){
		var param=this.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		$('#SearchContent').on('keyup',function(){
			if (event.keyCode==13){
				$('#btn-search').click();
			}
		});
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
//			commonDia = $.dialog({
//				title : '选择品牌',
//				content : 'url:'+config.BASEPATH+'batch/client/to_list_dialog',
//				data : {multiselect:false},
//				width : 450,
//				height : 320,
//				max : false,
//				min : false,
//				cache : false,
//				lock: true,
//				ok:function(){
//					var selected = commonDia.content.doSelect();
//					if (!selected) {
//						return false;
//					}
//					alert(selected.ci_code+"--"+selected.ci_name);
//				},
//				close:function(){
//					if(commonDia.content.dblClick){
//						var selected = commonDia.content.doSelect();
//						alert(selected.ci_code+"--"+selected.ci_name);
//					}
//				},
//				cancel:true
//			});
		});
		//新增
		$('#btn-add').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.INSERT)) {
				return ;
			}
			handle.operate('add');
		});
		//修改
		$('#grid').on('click', '.operating .ui-icon-pencil', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.UPDATE)) {
				return ;
			}
			var id = $(this).parent().data('id');
			handle.operate('edit', id);
		});
		//删除
		$('#grid').on('click', '.operating .ui-icon-trash', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.DELETE)) {
				return ;
			}
			var id = $(this).parent().data('id');
			var rowData = $("#grid").jqGrid("getRowData",id);
			handle.del(id,rowData.ci_code);
		});
		//查看客户店铺信息
		$('#grid').on('click', '.operating .ui-icon-shop', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.UPDATE)) {
				return ;
			}
			var id = $(this).parent().data('id');
			var rowData = $("#grid").jqGrid("getRowData",id);
			handle.toClientShop(id,rowData.ci_code);
		});
	}
}
THISPAGE.init();