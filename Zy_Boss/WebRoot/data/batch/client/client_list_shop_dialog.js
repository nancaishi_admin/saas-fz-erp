var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var api = frameElement.api, W = api.opener;
var searchFlag = false;
var queryurl = config.BASEPATH+'batch/client/shop_list';
var multiselect = false;
if(undefined != api.data.multiselect){
	multiselect = api.data.multiselect;
}
var dblClick = false;
var queryConditions = {
		searchContent: ''
	};
var handle = {
};
var format = {
};

function formatCisStatus(value){
	if(value == '0'){
		return '启用';
	}
	return '停用';
}

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		$('#txtSearch').placeholder();
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
	    	{name: 'cis_code', index: 'cis_code', width:60, title: false ,label:'店铺编号'},
            {name: 'cis_name', index: 'cis_name', width:120, title: false, align: 'left',label:'店铺名称'},
            {name: 'cis_linkman', index: 'cis_linkman', width:60, title: false,label:'联系人'},
            {name: 'cis_link_tel', index: 'cis_link_tel', width:80, title: false,label:'固定电话'},
            {name: 'cis_link_mobile', index: 'cis_link_mobile', width:80, title: false,label:'移动电话'},
            {name: 'cis_link_adr', index: 'cis_link_adr', width:200, title: true,label:'店铺地址'},
            {name: 'cis_id',hidden:true}
	    ];
		$('#grid').jqGrid({
			url:queryurl+"?"+this.buildParams(),
			loadonce:true,
			datatype:'json',
			width:gridWH.w-12,
			height:gridWH.h-14,
			altRows:true,
			gridview: true,
			onselectrow: false,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			multiselect:multiselect,//多选
			viewrecords: true,
			cmTemplate: {sortable:true,title:false},
			page: 1, //只有一页
			pgbuttons:false,
			pgtext:false,
			rowNum:99999,//每页条数
			/*rowList:CONFIG.BASEROWLIST,//分页条数*/
			shrinkToFit:false,//表格是否自动填充
			//scroll: 1,//是否滚动
			jsonReader: {
				root: 'data',
				id: 'cis_id',  
				repeatitems : false
			},
			loadComplete: function(data){
				Public.resizeSpecifyGrid("grid",79,32);
			},
			loadError: function(xhr, status, error){		
				Public.tips({type: 1, content : '操作失败了哦，服务可能已过期，请按F5刷新！'});
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
				if(!multiselect){
					dblClick = true;
					api.close();
				}
			}
	    });
	},
	buildParams:function(){
		var params = '';
		params += "searchContent="+Public.encodeURI($.trim($('#SearchContent').val()));
		params += "&ci_code="+$("#ci_code").val();
		params += "&cis_stutas=0";
		return params;
	},
	reloadData:function(){
		var param=this.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",url:queryurl+"?"+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
		$('#txtSearch').on('keyup',function(){
			if (event.keyCode==13){
				$('#btn-search').click();
			}
		});
	}
}

function doSelect(){
	if(multiselect){//多选
		var ids = $("#grid").jqGrid('getGridParam', 'selarrrow');
		if(ids == null || ids.length == 0){
			Public.tips({type: 2, content : "请选择客户店铺！"});
			return false;
		}
		var result = [];
		for (var i = 0; i < ids.length; i++) {
			result.push($("#grid").jqGrid("getRowData", ids[i]));
		}
		return result;
		
	}else{//单选
		var selectedId = $("#grid").jqGrid('getGridParam', 'selrow');
		if(selectedId == null || selectedId == ""){
			Public.tips({type: 2, content : "请选择客户店铺！"});
			return false;
		}
		var rowData = $("#grid").jqGrid("getRowData", selectedId);
		return rowData;
	}
}
THISPAGE.init();
