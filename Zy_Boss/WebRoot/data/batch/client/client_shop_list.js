var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var api = frameElement.api, W = api.opener;
var searchFlag = false;
var queryurl = config.BASEPATH+'batch/client/shop_list';
var _limitMenu = system.MENULIMIT.BATCH04;
var _menuParam = config.OPTIONLIMIT;
var queryConditions = {
		searchContent: ''
	};
var handle = {
	//修改、新增
	operate: function(oper, rowId){
		var url
			,title
			,data
			;
		var ci_code = $("#ci_code").val();
		if(oper == 'add'){
			title = '新增店铺';
			data = {oper: oper, callback: this.callback};
			url =  config.BASEPATH+"batch/client/to_add_shop_list?ci_code="+ci_code;
		}else if (oper=='edit'){
			title = '修改店铺';
			data = {oper: oper, rowId: rowId, callback: this.callback};
			url =  config.BASEPATH+"batch/client/to_update_shop_list?cis_id="+rowId+"&ci_code="+ci_code;
		}else{
			title = '查看店铺';
			data = {akira:true,oper: oper, rowId: rowId, callback: this.callback};
			url =  CONFIG.BASEPATH+"base/updateCustomerStoreInfo.action?akira=1&bcs_id="+rowId;
		}
		$.dialog({
			title : title,
			content : 'url:'+url,
			data: data,
			width : 550,
			height : 300,
			max : false,
			min : false,
			cache : false,
			lock: false
		});
	},
	//删除
	del: function(id){
		if(id != null && id != ''){
		 	$.dialog.confirm('数据删除后无法恢复，确定要刪除吗？', function(){
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'batch/client/del_client_shop?cis_id='+id,
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){
							Public.tips({type: 3, content : '删除成功!'});
							$('#grid').jqGrid('delRowData', id);
						}else{
							Public.tips({type: 1, content : '删除失败!'});
						}
					}
				});
			});
	   }else{
	      $.dialog.tips("请选择数据!!!",1,"32X32/fail.png");
	   }
	},
	callback: function(data, oper, dialogWin){
		var gridData = $("#grid").data('gridData');
		if(oper == "edit") {
			$("#grid").jqGrid('setRowData', data.id, data);
			dialogWin && dialogWin.api.close();
		} else {
			$("#grid").jqGrid('addRowData', data.id, data, 'first');
			dialogWin && dialogWin.api.zindex();
		}
	}
};
var format = {
};

function formatCisStatus(value){
	if(value == '0'){
		return '启用';
	}
	return '停用';
}

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		$('#txtSearch').placeholder();
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
		    {name: 'operate',index:'operate', width: 60, fixed:true, formatter: Public.operFmatter, title: false,sortable:false,label:'操作'},
	    	{name: 'cis_code', index: 'cis_code', width:60, title: false ,label:'店铺编号'},
            {name: 'cis_name', index: 'cis_name', width:120, title: false, align: 'left',label:'店铺名称'},
            {name: 'cis_linkman', index: 'cis_linkman', width:60, title: false,label:'联系人'},
            {name: 'cis_link_tel', index: 'cis_link_tel', width:80, title: false,label:'固定电话'},
            {name: 'cis_link_mobile', index: 'cis_link_mobile', width:80, title: false,label:'移动电话'},
            {name: 'cis_link_adr', index: 'cis_link_adr', width:200, title: true,label:'店铺地址'},
            {name: 'cis_stutas', index: 'cis_stutas', width:60, title: false,label:'状态',formatter:formatCisStatus},
            {name: 'cis_id',hidden:true}
	    ];
		$('#grid').jqGrid({
			url:queryurl+"?"+this.buildParams(),
			loadonce:true,
			datatype:'json',
			width:gridWH.w-12,
			height:gridWH.h-14,
			altRows:true,
			gridview: true,
			onselectrow: false,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			multiselect:false,//多选
			viewrecords: true,
			cmTemplate: {sortable:true,title:false},
			page: 1, //只有一页
			pgbuttons:false,
			pgtext:false,
			rowNum:99999,//每页条数
			/*rowList:CONFIG.BASEROWLIST,//分页条数*/
			shrinkToFit:false,//表格是否自动填充
			//scroll: 1,//是否滚动
			jsonReader: {
				root: 'data',
				id: 'cis_id',  
				repeatitems : false
			},
			loadComplete: function(data){
				Public.resizeSpecifyGrid("grid",79,32);
			},
			loadError: function(xhr, status, error){		
				Public.tips({type: 1, content : '操作失败了哦，服务可能已过期，请按F5刷新！'});
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
				handle.operate('',rowid);
			}
	    });
	},
	buildParams:function(){
		var params = '';
		params += "searchContent="+Public.encodeURI($.trim($('#SearchContent').val()));
		params += "&ci_code="+$("#ci_code").val();
		params += "&cis_stutas=";
		return params;
	},
	reloadData:function(){
		var param=this.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",url:queryurl+"?"+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			};
			THISPAGE.reloadData();
		});
		
		$('#txtSearch').on('keyup',function(){
			if (event.keyCode==13){
				$('#btn-search').click();
			}
		});
		
		//新增
		$('#btn-add').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.INSERT)) {
				return ;
			};
			handle.operate('add');
		});
		//修改
		$('#grid').on('click', '.operating .ui-icon-pencil', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.UPDATE)) {
				return ;
			};
			var id = $(this).parent().data('id');
			handle.operate('edit', id);
		});
		//删除
		$('#grid').on('click', '.operating .ui-icon-trash', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.DELETE)) {
				return ;
			};
			var id = $(this).parent().data('id');
			handle.del(id);
		});
		
		//刷新
		$('#btn-flush').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
		$(window).resize(function(){
			Public.resizeSpecifyGrid("grid",79,32);
		});
	}
}
THISPAGE.init();
