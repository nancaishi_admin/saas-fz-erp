var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var isRefresh = false;
var _limitMenu = system.MENULIMIT.MONEY08;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'batch/client/page';
var _height = $(parent).height()-294,_width = $(parent).width()-32;

var handle = {
	formatActualDebt:function(val, opt, row){
		return PriceLimit.formatMoney(row.ci_receivable-row.ci_received-row.ci_prepay);
	},
	operFmatter : function(val, opt, row){
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-settle" title="结算明细">&#xe64c;</i>&nbsp;';
		html_con += '<i class="iconfont i-hand ui-icon-dealings" title="往来明细">&#xe64f;</i>';
		html_con += '</div>';
		return html_con;
	},
	viewDealings:function(rowid){
		var rowData = $("#grid").jqGrid("getRowData", rowid);
		$.dialog({
			title : false,
			content : 'url:'+config.BASEPATH+'batch/dealings/to_list',
			data: {ci_code:rowData.ci_code},
			max : false,
			min : false,
			cache : false,
			lock: false
		}).max();
	},
	viewSettleDetail:function(rowid){
		var rowData = $("#grid").jqGrid("getRowData", rowid);
		$.dialog({
			title : false,
			content : 'url:'+config.BASEPATH+'batch/settle/to_list_detail',
			data: {ci_code:rowData.ci_code},
			max : false,
			min : false,
			cache : false,
			lock: false
		}).max();
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		
	},
	gridTotal:function(){
    	var grid=$('#grid');
		var ci_init_debt=grid.getCol('ci_init_debt',false,'sum');
		var ci_receivable=grid.getCol('ci_receivable',false,'sum');
		var ci_received=grid.getCol('ci_received',false,'sum');
    	var ci_prepay=grid.getCol('ci_prepay',false,'sum');
    	grid.footerData('set',{ci_code:'合计：',
    		ci_init_debt:ci_init_debt,
    		ci_receivable:ci_receivable,
    		ci_received:ci_received,
    		ci_prepay:ci_prepay,
    		actual_debt:''});
    },
	initGrid:function(){
		var colModel = [
			{label:'操作',name: 'operate',width: 60, formatter: handle.operFmatter,align:'center', title: false,sortable:false},
			{label:'客户编号',name: 'ci_code', index: 'ci_code', width:80, title: false,fixed:true},
			{label:'客户名称',name: 'ci_name', index: 'ci_name', width: 140, title: false},
			{label:'联系人',name: 'ci_man', index: 'ci_man', width: 100, title: false,align:'left'},
			{label:'电话',name: 'ci_tel', index: 'ci_tel', width: 140, title: false},
			{label:'手机号码',name: 'ci_mobile', index: 'ci_mobile', width: 100, title: false},
			{label:'期初金额',name: 'ci_init_debt', index: 'ci_init_debt',sorttype: 'float',width:100, align:'right',formatter: PriceLimit.formatMoney},
			{label:'应收金额',name: 'ci_receivable', index: 'ci_receivable',sorttype: 'float',width:100, align:'right',formatter: PriceLimit.formatMoney},
			{label:'已收金额',name: 'ci_received', index: 'ci_received',sorttype: 'float',width:100, align:'right',formatter: PriceLimit.formatMoney},
			{label:'预收款余额',name: 'ci_prepay', index: 'ci_prepay',sorttype: 'float',width:100, align:'right',formatter: PriceLimit.formatMoney},
			{label:'实际欠款',name: 'actual_debt', index: 'actual_debt',sorttype: 'float',width:100, align:'right',formatter: handle.formatActualDebt}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.BASEROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'ci_id'
			},
			loadComplete: function(data){
				THISPAGE.gridTotal();
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams:function(){
		var params = '';
		params += "searchContent="+Public.encodeURI($.trim($('#SearchContent').val()));
		return params;
	},
	reloadData:function(){
		var param=this.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		$('#SearchContent').on('keyup',function(){
			if (event.keyCode==13){
				$('#btn-search').click();
			}
		});
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#grid').on('click', '.operating .ui-icon-settle', function(e) {
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.viewSettleDetail(id);
		});
		$('#grid').on('click', '.operating .ui-icon-dealings', function(e) {
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.viewDealings(id);
		});
		
	}
}
THISPAGE.init();