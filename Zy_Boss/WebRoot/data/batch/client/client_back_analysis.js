var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var isRefresh = false;
var _limitMenu = system.MENULIMIT.MONEY08;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'batch/client/pageBackAnalysis';
var _height = $(parent).height()-294,_width = $(parent).width()-202;

var Utils = {
	doQueryClient : function(){
		commonDia = $.dialog({
			title : '选择批发客户',
			content : 'url:'+config.BASEPATH+'batch/client/to_list_dialog',
			data : {multiselect:false},
			width : 650,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#ci_code").val(selected.ci_code);
				$("#ci_name").val(selected.ci_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#ci_code").val(selected.ci_code);
					$("#ci_name").val(selected.ci_name);
				}
			},
			cancel:true
		});
	}
};


var handle = {
	formatRealMoney:function(val, opt, row){
		return PriceLimit.formatMoney(row.sell_money-row.return_money);
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		dateRedioClick("theDate");
	},
	gridTotal:function(){
    	var grid=$('#grid');
		var ci_receivable=grid.getCol('ci_receivable',false,'sum');
		var ci_received=grid.getCol('ci_received',false,'sum');
    	var ci_prepay=grid.getCol('ci_prepay',false,'sum');
    	grid.footerData('set',{ci_code:'合计：',
    		ci_receivable:ci_receivable,
    		ci_received:ci_received,
    		ci_prepay:ci_prepay,
    		actual_debt:''});
    },
	initGrid:function(){
		var colModel = [
			{label:'客户编号',name: 'ci_code', index: 'ci_code', width:80, title: false,fixed:true},
			{label:'客户名称',name: 'ci_name', index: 'ci_name', width: 140, title: false},
			{label:'期初金额',name: 'init_money', index: 'init_money',sorttype: 'float',width:100, align:'right',formatter: PriceLimit.formatMoney},
			{label:'销售金额',name: 'sell_money', index: 'sell_money',sorttype: 'float',width:100, align:'right',formatter: PriceLimit.formatMoney},
			{label:'退货金额',name: 'return_money', index: 'return_money',sorttype: 'float',width:100, align:'right',formatter: PriceLimit.formatMoney},
			{label:'净销售额',name: 'real_money', index: '',sorttype: 'float',width:100, align:'right',formatter: handle.formatRealMoney},
			{label:'费用单金额',name: 'fee_money', index: 'fee_money',sorttype: 'float',width:100, align:'right',formatter: PriceLimit.formatMoney},
			{label:'应收金额',name: 'receivable', index: 'receivable',sorttype: 'float',width:100, align:'right',formatter: PriceLimit.formatMoney},
			{label:'优惠金额',name: 'discount_money', index: 'discount_money',sorttype: 'float',width:100, align:'right',formatter: PriceLimit.formatMoney},
			{label:'实收金额',name: 'received', index: 'received',sorttype: 'float',width:100, align:'right',formatter: PriceLimit.formatMoney},
			{label:'使用预收款金额',name: 'prepay', index: 'prepay',sorttype: 'float',width:100, align:'right',formatter: PriceLimit.formatMoney},
			{label:'预收款金额',name: 'ci_prepay', index: 'ci_prepay',sorttype: 'float',width:100, align:'right',formatter: PriceLimit.formatMoney},
			{label:'信誉额度',name: 'ci_credit_limit', index: 'ci_credit_limit',sorttype: 'float',width:100, align:'right',formatter: PriceLimit.formatMoney},
			{label:'未发货天数',name: 'nosenddays', index: 'nosenddays',width:100, align:'right'}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:9999,//每页条数
//			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id: 'ci_id'
			},
			loadComplete: function(data){
				THISPAGE.gridTotal();
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams:function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&ci_code='+$("#ci_code").val();
		return params;
	},
	reset:function(){
		THISPAGE.$_date.setValue(0);
		$("#ci_name").val("");
		$("#ci_code").val("");
	},
	reloadData:function(){
		var param=this.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();