var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var isRefresh = false;
var api = frameElement.api, W = api.opener;
var queryurl = config.BASEPATH+'batch/settle/page';
var _height = $(parent).height()-144,_width = $(parent).width()-32;

var Utils = {
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#st_manager").val(selected.em_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#st_manager").val(selected.em_name);
				}
			},
			cancel:true
		});
	}
};

var handle = {
	operate: function(oper, id){//修改、新增
		var url = config.BASEPATH+"batch/settle/to_view?st_id="+id;
		var data = {oper: 'readonly'};
		$.dialog({
			title : false,
			content : 'url:'+url,
			data: data,
			max : false,
			min : false,
			cache : false,
			resize:false,
			lock: false
		}).max();
	},
    callback: function(data, oper, dialogWin){
		if(oper == "edit") {
			THISPAGE.reloadData();
			dialogWin && dialogWin.api.close();
		}
		if(oper == "approve") {
			$("#grid").jqGrid('setRowData', data.id, data);
			dialogWin && dialogWin.api.close();
		}
		if(oper == "view") {
			$("#grid").jqGrid('setRowData', data.id, data);
			dialogWin && dialogWin.api.close();
		}
		if(oper == "add"){
			THISPAGE.reloadData();
			dialogWin && dialogWin.api.close();
		}
	},
	formatNumberLink : function(val, opt, row){
		var btnHtml = '<a href="javascript:void(0);" onclick="javascript:handle.operate(\'view\',' + opt.rowId + ');">'+row.st_number+'</a>';
		return btnHtml;
	},
	formatArState:function(val, opt, row){//0：未审核；1：已审核2：已退回
		if(val == 0){
			return '未审核';
		}else if(val == 1){
			return '已审核';
		}else if(val == 2){
			return '已退回';
		}
		return val;
	},
	formatEntire:function(val, opt, row){
		if(val == 0){
			return '否';
		}else if(val == 1){
			return '是';
		}
		return val;
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
	    	{label:'单据编号',name: 'st_number',index: 'st_number',width:150,hidden:true},
	    	{label:'单据编号',name: 'numberlink',index: 'st_number',width:150,formatter: handle.formatNumberLink},
	    	{label:'客户名称',name: 'client_name',index: 'client_name',width:120},
	    	{label:'银行账户',name: 'ba_name',index: 'ba_name',width:120},
	    	{label:'经办人',name: 'st_manager',index: 'st_manager',width:100},
	    	{label:'结算日期',name: 'st_date',index: 'st_date',width:80},
	    	{label:'优惠金额',name: 'st_discount_money',index: 'st_discount_money',width:80,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'使用预收金额',name: 'st_prepay',index: 'st_prepay',width:80,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'实收金额',name: 'st_received',index: 'st_received',width:80,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'转入预收金额',name: 'st_receivedmore',index: 'st_receivedmore',width:80,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'整单结算',name: 'st_entire',index: 'st_entire',width:80,formatter: handle.formatEntire},
	    	{label:'审核状态',name: 'st_ar_state',index: 'st_ar_state',width:80,formatter: handle.formatArState},
	    	{label:'审核日期',name: 'st_ar_date',index: 'st_ar_date',width:80},
	    	{label:'制单人',name: 'st_maker',index: 'st_maker',width:100}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.BASEROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'st_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'st_ar_state=1';
		params += '&begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&st_client_code='+api.data.ci_code;
		params += '&st_manager='+Public.encodeURI($("#st_manager").val());
		params += '&st_number='+Public.encodeURI($("#st_number").val());
		return params;
	},
	reset:function(){
		$("#st_number").val("");
		$("#st_manager").val("");
		THISPAGE.$_date.setValue(0);
		$("#begindate").val("");
		$("#enddate").val("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn-close').on('click', function(e){
			e.preventDefault();
			api.close();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();