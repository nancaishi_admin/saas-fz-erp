var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.BATCH19;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'batch/report/listSellReport_csb';
var _height = $(parent).height()-180,_width = $(parent).width()-32;
var api = frameElement.api;
if(api == undefined){
	$("#btn_close").hide();
	_height = $(parent).height()-298;
	_width = $(parent).width()-205;
}

var handle = {
	viewDetail: function(id){
		var rowData = $("#grid").jqGrid("getRowData", id);
		var params = {};
		params.begindate = $("#begindate").val();
		params.enddate = $("#enddate").val();
		params.client_name = $("#client_name").val();
		params.se_client_code = $("#se_client_code").val();
		params.depot_name = $("#depot_name").val();
		params.se_depot_code = $("#se_depot_code").val();
		params.se_manager = $("#se_manager").val();
		params.bd_name = $("#bd_name").val();
		params.bd_code = $("#bd_code").val();
		params.tp_name = $("#tp_name").val();
		params.tp_code = $("#tp_code").val();
		params.pd_season = $("#pd_season").val();
		params.pd_year = $("#pd_year").val();
		params.pd_code = $("#pd_code").val();
		params.pd_name = $("#pd_name").val();
		params.cr_name = $("#cr_name").val();
		params.cr_code = $("#cr_code").val();
		params.sz_name = $("#sz_name").val();
		params.sz_code = $("#sz_code").val();
		params.br_name = $("#br_name").val();
		params.br_code = $("#br_code").val();
		var type = $("#type").val();
		var url = '';
		switch(type){
			case 'color':
				params.cr_code = rowData.code;
				params.cr_name = rowData.name;
				url = 'url:'+config.BASEPATH+"batch/report/to_report_sell_csb/size";
				break ;
			case 'size':
				params.sz_code = rowData.code;
				params.sz_name = rowData.name;
				url = 'url:'+config.BASEPATH+"batch/report/to_report_sell_csb/color";
				break ;
			case 'bra':
				params.br_code = rowData.code;
				params.br_name = rowData.name;
				if($.trim(rowData.code) == ""){
					params.br_code = "empty";
				}
				url = 'url:'+config.BASEPATH+"batch/report/to_report_sell_cs";
				break ;
			default :
				break ;
		}
		$.dialog({
			title : false,
			content : url,
			data: params,
			max : false,
			min : false,
			cache : false,
			lock: false
		}).max();
	},
	operFmatter : function(val, opt, row){
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-list" title="明细">&#xe608;</i>';
		html_con += '</div>';
		return html_con;
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		dateRedioClick("theDate");
		this.initParams();
		Public.buildSeasonCombo("span_pd_season", "pd_season");
		Public.buildYearCombo("span_pd_year", "pd_year");
	},
	initParams:function(){
		if(api == undefined){
			return;
		}
		var params = api.data;
		$("#begindate").val($.trim(params.begindate));
		$("#enddate").val($.trim(params.enddate));
		$("#client_name").val($.trim(params.client_name));
		$("#se_client_code").val($.trim(params.se_client_code));
		$("#depot_name").val($.trim(params.depot_name));
		$("#se_depot_code").val($.trim(params.se_depot_code));
		$("#se_manager").val($.trim(params.se_manager));
		$("#bd_name").val($.trim(params.bd_name));
		$("#bd_code").val($.trim(params.bd_code));
		$("#tp_name").val($.trim(params.tp_name));
		$("#tp_code").val($.trim(params.tp_code));
		$("#pd_season").val($.trim(params.pd_season));
		$("#pd_year").val($.trim(params.pd_year));
		$("#pd_code").val($.trim(params.pd_code));
		$("#pd_name").val($.trim(params.pd_name));
		$("#cr_name").val($.trim(params.cr_name));
		$("#cr_code").val($.trim(params.cr_code));
		$("#sz_name").val($.trim(params.sz_name));
		$("#sz_code").val($.trim(params.sz_code));
		$("#br_name").val($.trim(params.br_name));
		$("#br_code").val($.trim(params.br_code));
	},
	gridTotal:function(){
    	var grid=$('#grid');
		var amount=grid.getCol('amount',false,'sum');
    	var money=grid.getCol('money',false,'sum');
    	grid.footerData('set',{code:'合计',amount:amount,money:money});
    },
	initGrid:function(){
		var operHid = false;
		if(api){
			operHid = true;
		}
		var type = $("#type").val();
		var codeLabel = '';
		var nameLabel = '';
		switch(type){
			case 'color' : 
				codeLabel = '颜色编号';
				nameLabel = '颜色名称';
				break ;
			case 'size' : 
				codeLabel = '尺码编号';
				nameLabel = '尺码名称';
				break ;
			case 'bra' : 
				codeLabel = '杯型编号';
				nameLabel = '杯型名称';
				break ;
			default :
				break ;
		}
		var colModel = [
	    	{label:'操作',name: 'operate',width: 40, formatter: handle.operFmatter,align:'center', sortable:false,hidden:operHid},
	    	{label:codeLabel,name: 'code',index:'code',width:100},	
	    	{label:nameLabel,name: 'name',index:'name',width:100},	
        	{label:'数量',name: 'amount',index:'amount',width:80, align:'right',sorttype: 'float'},	
        	{label:'金额',name: 'money',index:'money',width:100, align:'right',formatter: PriceLimit.formatMoney,sorttype: 'float'},	
	    	{label:'数量占比(%)',name: 'amount_proportion',index:'amount_proportion',width:100, align:'right',sorttype: 'float'},	
	    	{label:'金额占比(%)',name: 'money_proportion',index:'money_proportion',width:100, align:'right',sorttype: 'float'}	
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),
            loadonce:true,
			datatype: 'local',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:9999,//每页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			userDataOnFooter: true,
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id: 'id'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.gridTotal();
				var footer = $('#grid').footerData();
				var ids = $("#grid").getDataIDs();
				for (var i = 0; i < ids.length; i++) {
					var rowData = $("#grid").jqGrid("getRowData", ids[i]);
					if(footer.amount != 0){
						$('#grid').setCell(ids[i],'amount_proportion',PriceLimit.formatMoney(100*rowData.amount/footer.amount));
					}
					if(footer.money != 0){
						$('#grid').setCell(ids[i],'money_proportion',PriceLimit.formatMoney(100*rowData.money/footer.money));
					}
				}
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'type='+$("#type").val();
		params += '&begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&se_client_code='+$("#se_client_code").val();
		params += '&se_depot_code='+$("#se_depot_code").val();
		params += '&bd_code='+$("#bd_code").val();
		params += '&tp_code='+$("#tp_code").val();
		params += '&pd_season='+$("#pd_season").val();
		params += '&pd_year='+$("#pd_year").val();
		params += '&se_manager='+Public.encodeURI($("#se_manager").val());
		params += '&pd_code='+$("#pd_code").val();
		params += '&cr_code='+$("#cr_code").val();
		params += '&sz_code='+$("#sz_code").val();
		params += '&br_code='+$("#br_code").val();
		return params;
	},
	reset:function(){
		$("#pd_code").val("");$("#pd_name").val("");
		$("#se_manager").val("");
		$("#se_client_code").val("");$("#client_name").val("");
		$("#se_depot_code").val("");$("#depot_name").val("");
		$("#bd_code").val("");$("#bd_name").val("");
		$("#tp_code").val("");$("#tp_name").val("");
		$("#cr_code").val("");$("#cr_name").val("");
		$("#sz_code").val("");$("#sz_name").val("");
		$("#br_code").val("");$("#br_name").val("");
		THISPAGE.$_date.setValue(0);
		$("#span_pd_season").getCombo().selectByValue("");
		$("#span_pd_year").getCombo().selectByValue("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		//收银明细
		$('#grid').on('click', '.operating .ui-icon-list', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			var id = $(this).parent().data('id');
			handle.viewDetail(id);
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
		$("#btn_close").click(function(e){
			e.preventDefault();
			api.close();
		});
	}
}
THISPAGE.init();