var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var queryurl = config.BASEPATH+'batch/report/pageOrderDetailReport';
var _height = $(parent).height()-285,_width = $(parent).width()-192;

var handle = {
	formatState:function(val, opt, row){
		if(val == 0 || val == 4){
			return '未完成';
		}else if(val == 1 || val == 2 || val == 3){
			return '已完成';
		}
		return val;
	},
	formatGapAmount:function(val, opt, row){
		return row.odl_amount-row.odl_realamount;
	},
	formatRealMoney:function(val, opt, row){
		if(row.odl_unitprice == undefined){
			return PriceLimit.formatByBatch(val);
		}
		return PriceLimit.formatByBatch(row.odl_unitprice * row.odl_realamount);
	},
	formatGapMoney:function(val, opt, row){
		if(row.odl_unitprice == undefined){
			return PriceLimit.formatByBatch(val);
		}
		return PriceLimit.formatByBatch(row.odl_unitprice * (row.odl_amount-row.odl_realamount));
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		dateRedioClick("theDate");
		Public.buildSeasonCombo("span_pd_season", "pd_season");
		Public.buildYearCombo("span_pd_year", "pd_year");
	},
	initGrid:function(){
		var colModel = [
		    {label:'操作',name:'operate', width: 40, fixed:true, formatter: Public.operImgFmatter,align:'center'},
	    	{label:'日期',name: 'od_make_date',index:'od_make_date',width:100},
	    	{label:'单据编号',name: 'odl_number',index:'odl_number',width:135},
	    	{label:'状态',name: 'od_state',index:'od_state',width:100,formatter: handle.formatState,align:'center'},
	    	{label:'客户名称',name: 'client_name',index:'client_name',width:100},
	    	{label:'仓库名称',name: 'depot_name',index:'depot_name',width:100},
	    	{label:'',name: 'odl_pd_code',index:'odl_pd_code',width:100,hidden:true},
	    	{label:'商品货号',name: 'pd_no',index:'pd_no',width:100},
	    	{label:'商品名称',name: 'pd_name',index:'pd_name',width:100},
	    	{label:'颜色',name: 'cr_name', index: 'cr_name', width: 60},
	    	{label:'尺码',name: 'sz_name', index: 'sz_name', width: 60},
	    	{label:'杯型',name: 'br_name', index: 'br_name', width: 60,align:'center'},
	    	{label:'单位',name: 'pd_unit', index: 'pd_unit', width: 60,align:'center'},
	    	{label:'单价',name: 'odl_unitprice', index: 'odl_unitprice', width: 80,align:'right',formatter: PriceLimit.formatByBatch},
	    	{label:'订单数量',name: 'odl_amount', index: 'odl_amount', width: 70,align:'right'},
	    	{label:'订单金额',name: 'odl_unitmoney', index: 'odl_unitmoney', width: 80,align:'right',formatter: PriceLimit.formatByBatch},
	    	{label:'已发数量',name: 'odl_realamount', index: 'odl_realamount', width: 70,align:'right'},
	    	{label:'已发金额',name: 'realmoney', index: 'odl_unitmoney', width: 80,align:'right',formatter: handle.formatRealMoney},
	    	{label:'未发数量',name: 'gapamount',index: 'odl_amount-odl_realamount',width:70,align:'right',formatter: handle.formatGapAmount},
	    	{label:'未发金额',name: 'gapmoney', index: 'odl_unitmoney', width: 80,align:'right',formatter: handle.formatGapMoney},
	    	{label:'品牌',name: 'bd_name', index: 'pd_bd_code', width: 80},
	    	{label:'类别',name: 'tp_name', index: 'pd_tp_code', width: 80},
	    	{label:'季节',name: 'pd_season', index: 'pd_season', width: 80,align:'center'},
	    	{label:'年份',name: 'pd_year', index: 'pd_year', width: 80,align:'center'}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'local',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.BASEROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:false,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			userDataOnFooter: true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
                userdata: 'data.data',
				repeatitems : false,
				id: 'odl_id'  //图标ID
			},
			loadComplete: function(data){
		    	$('#grid').footerData('set',{od_make_date:'合计'});
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'od_type=0';
		params += '&begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&od_client_code='+$("#od_client_code").val();
		params += '&od_depot_code='+$("#od_depot_code").val();
		params += '&bd_code='+$("#bd_code").val();
		params += '&tp_code='+$("#tp_code").val();
		params += '&pd_season='+$("#pd_season").val();
		params += '&pd_year='+$("#pd_year").val();
		params += '&od_manager='+Public.encodeURI($("#od_manager").val());
		params += '&pd_code='+$("#pd_code").val();
		return params;
	},
	reset:function(){
		$("#pd_code").val("");
		$("#pd_name").val("");
		$("#od_manager").val("");
		$("#client_name").val("");
		$("#od_client_code").val("");
		$("#depot_name").val("");
		$("#od_depot_code").val("");
		$("#bd_name").val("");
		$("#bd_code").val("");
		$("#tp_name").val("");
		$("#tp_code").val("");
		THISPAGE.$_date.setValue(0);
		$("#span_pd_season").getCombo().selectByValue("");
		$("#span_pd_year").getCombo().selectByValue("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
		$('#grid').on('click', '.operating .ui-icon-image', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
        	viewProductImg($("#grid").jqGrid("getRowData", id).odl_pd_code);
        });
	}
}
THISPAGE.init();