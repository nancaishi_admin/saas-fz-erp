var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var queryurl = config.BASEPATH+'stock/data/single_track';
var _height = $(parent).height()-298,_width = $(parent).width()-192;

var handle = {
	formatApplyMoney:function(val, opt, row){
		if(row.wtl_unitprice == undefined){
			return PriceLimit.formatBySell(val);
		}
		return PriceLimit.formatBySell(row.wtl_unitprice * row.wtl_applyamount);
	},
	formatSendMoney:function(val, opt, row){
		if(row.wtl_unitprice == undefined){
			return PriceLimit.formatBySell(val);
		}
		return PriceLimit.formatBySell(row.wtl_unitprice * row.wtl_sendamount);
	},
	formatSendCostMoney:function(val, opt, row){
		if(row.wtl_costprice == undefined){
			return PriceLimit.formatByCost(val);
		}
		return PriceLimit.formatByCost(row.wtl_costprice * row.wtl_sendamount);
	},
	formatType:function(val, opt, row){
		if(val == 0){
			return '补货单';
		}else if(val == 1){
			return '退货单';
		}
		return val;
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		dateRedioClick("theDate");
	},
	initSeason:function(){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"base/dict/list?dtl_upcode=KEY_SEASON",
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.data != "" && data.data.length > 0){
					var seasons = data.data;
					seasons.unshift({dtl_code:"",dtl_name:"全部"});
					$('#span_pd_season').combo({
						value : 'dtl_code',
						text : 'dtl_name',
						width : 207,
						listHeight : 300,
						listId : '',
						defaultSelected : 0,
						editable : false,
						callback : {
							onChange : function(data) {
								if(data.dtl_code != ""){
									$("#pd_season").val(data.dtl_name);
								}else{
									$("#pd_season").val("");
								}
							}
						}
					}).getCombo().loadData(seasons,["dtl_name",$("#pd_season").val(),0]);
				}
			}
		 });
	},
	initYear:function(){
		var currentYear = new Date().getFullYear();
		var data = [
		    {Code:"",Name:"全部"},
			{Code:""+parseInt(currentYear+1)+"",Name:""+parseInt(currentYear+1)+""},
			{Code:""+parseInt(currentYear)+"",Name:""+parseInt(currentYear)+""},
			{Code:""+parseInt(currentYear-1)+"",Name:""+parseInt(currentYear-1)+""},
			{Code:""+parseInt(currentYear-2)+"",Name:""+parseInt(currentYear-2)+""},
			{Code:""+parseInt(currentYear-3)+"",Name:""+parseInt(currentYear-3)+""},
			{Code:""+parseInt(currentYear-4)+"",Name:""+parseInt(currentYear-4)+""},
			{Code:""+parseInt(currentYear-5)+"",Name:""+parseInt(currentYear-5)+""}
		];
		$('#span_pd_year').combo({
			value: 'Code',
			text: 'Name',
			width :206,
			height:80,
			listId:'',
			defaultSelected: 0,
			editable: false,
			callback:{
				onChange: function(data){
					$("#pd_year").val(data.Code);
				}
			}
		}).getCombo().loadData(data,["Code",$("#pd_year").val(),0]);
	},
	initGrid:function(){
		var colModel = [
	    	{label:'操作日期',name: 'dl_date',index:'dl_date',width:100},
	    	{label:'仓库名称',name: 'dp_name',index:'dp_code',width:100},
	    	{label:'类型',name: 'dl_type',index:'dl_type',width:100},
	    	{label:'单据编号',name: 'dl_number',index:'dl_number',width:140},
	    	{label:'',name: 'pd_code',index:'pd_code',width:100,hidden:true},
	    	{label:'商品货号',name: 'pd_no',index:'pd_no',width:100},
	    	{label:'商品名称',name: 'pd_name',index:'pd_name',width:100},
	    	{label:'品牌',name: 'bd_name', index: 'pd_bd_code', width: 80},
	    	{label:'类别',name: 'tp_name', index: 'pd_tp_code', width: 80},
	    	{label:'颜色',name: 'cr_name', index: 'cr_name', width: 60},
	    	{label:'尺码',name: 'sz_name', index: 'sz_name', width: 60},
	    	{label:'杯型',name: 'br_name', index: 'br_name', width: 60,align:'center'},
	    	{label:'入库数量',name: 'dl_in_amount', index: 'dl_in_amount', width: 70,align:'right'},
	    	{label:'出库数量',name: 'dl_out_amount', index: 'dl_out_amount', width: 70,align:'right'}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'local',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.DATAROWNUM,//每页条数
			rowList:config.DATAROWLIST,//分页条数
			cmTemplate: {sortable:false,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			userDataOnFooter: true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
                userdata: 'data.data',
				repeatitems : false,
				id: 'acl_id'  //图标ID
			},
			loadComplete: function(data){
		    	$('#grid').footerData('set',{ac_date:"合计"});
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&dp_code='+$("#dp_code").val();
		params += '&pd_code='+$("#pd_code").val();
		params += '&cr_code='+$("#cr_code").val();
		params += '&sz_code='+$("#sz_code").val();
		return params;
	},
	reset:function(){
		$("#dp_code").val("");$("#depot_name").val("");
		$("#pd_code").val("");$("#pd_name").val("");
		$("#cr_code").val("");$("#cr_name").val("");
		$("#sz_code").val("");$("#sz_name").val("");
		THISPAGE.$_date.setValue(0);
	},
	reloadData:function(){
		var pd_code = $.trim($("#pd_code").val());
		if(pd_code == ""){
			Public.tips({type: 2, content : "请选择商品！"});
			return;
		}
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
		$('#grid').on('click', '.operating .ui-icon-image', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
        	viewProductImg($("#grid").jqGrid("getRowData", id).acl_pd_code);
        });
	}
}
THISPAGE.init();