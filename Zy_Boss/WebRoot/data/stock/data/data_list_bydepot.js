var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var isRefresh = false;
var _limitMenu = system.MENULIMIT.STOCK11;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'stock/data/page_bydepot';
var _height = $(parent).height()-278,_width = $(parent).width()-192;
var Utils = {
	doQueryDepot : function(){
		commonDia = $.dialog({
			title : '选择仓库',
			content : 'url:'+config.BASEPATH+'base/depot/to_list_dialog_bymodule',
			data : {multiselect:true,module:'stock'},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				selectDepots = selected;
				var codes = [];
				var names = [];
				for(var i=0;i<selected.length;i++){
					codes.push(selected[i].dp_code);
					names.push(selected[i].dp_name);
				}
				$("#depot_code").val(codes.join(","));
				$("#depot_name").val(names.join(","));
			},
			cancel:true
		});
	}
};


var allDepots = [];
var selectDepots = [];
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_exactQuery = $("#exactQuery").cssCheckbox();
		this.getAllDepot();
	},
	getAllDepot:function(){/*获取所有仓库数据*/
		$.ajax({
			type:"POST",
			async:false,
			url:config.BASEPATH+"base/depot/list4stock?sidx=dp_code&sord=asc",
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					allDepots=data.data;
				}
			}
		});
	},
	initGrid:function(){
		$("#grid").jqGrid('GridUnload');
		var colModel = [
	    	{label:'',name: 'sd_pd_code',index: 'sd_pd_code',width:100, hidden:true},
	    	{label:'操作',name:'operate', width: 40, fixed:true, formatter: Public.operImgFmatter, align:'center'},
	    	{label:'商品货号',name: 'pd_no',index: 'pd_no',width:100, fixed:true},
	    	{label:'商品名称',name: 'pd_name',index: 'pd_name',width:150, fixed:true},
	    	{label:'颜色',name: 'cr_name',index: 'sd_cr_code',width:70},
	    	{label:'尺码',name: 'sz_name',index: 'sd_sz_code',width:70},
	    	{label:'杯型',name: 'br_name',index: 'sd_br_code',width:70},
	    	{label:'',name: 'pd_szg_code',index: 'pd_szg_code',width:70,hidden:true}
	    ];
		if(selectDepots != null && selectDepots.length > 0){
			for (var i = 0; i < selectDepots.length; i++) {
				colModel.push({label:selectDepots[i].dp_name,name: "amountMap."+selectDepots[i].dp_code,index:'',width: 80, title: false,sortable:false,align:'center'});
			}
		}else{
			for (var i = 0; i < allDepots.length; i++) {
				colModel.push({label:allDepots[i].dp_name,name: "amountMap."+allDepots[i].dp_code,index:'',width: 80, title: false,sortable:false,align:'center'});
			}
		}
		colModel = colModel.concat([
			{label:'小计',name: 'sd_amount',index: 'sd_amount',width:70,align:"right"}
		]);
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.DATAROWNUM,//每页条数
			rowList:config.DATAROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			userDataOnFooter: true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
                userdata: 'data.data',
				repeatitems : false,
				id: 'sd_id'  //图标ID
			},
			loadComplete: function(data){
				var userData = $("#grid").getGridParam('userData');
				var footData = {};
				for(var key in userData.amountMap){
					footData["amountMap."+key] = userData.amountMap[key];
				}
				$("#grid").footerData('set',footData);
			},
			loadError: function(xhr, status, error){		
			}
	    });
		
	},
	buildParams : function(){
		var params = '';
		params += '&pd_no='+Public.encodeURI($("#pd_no").val());
		params += '&pd_name='+Public.encodeURI($("#pd_name").val());
		params += '&exactQuery='+(THISPAGE.$_exactQuery.chkVal().join()? 1 : 0);
		if(selectDepots.length > 0){
			params += '&depot_code='+$("#depot_code").val();
		}else{
			var dp_code = [];
			for (var i = 0; i < allDepots.length; i++) {
				dp_code.push(allDepots[i].dp_code);
			}
			params += '&depot_code='+dp_code.join(",");
		}
		return params;
	},
	reset:function(){
		$("#pd_no").val("");
		$("#pd_name").val("");
		$("#depot_name").val("");
		$("#depot_code").val("");
		selectDepots = [];
	},
	initEvent:function(){
		$('#pd_no').on('keyup',function(event){
			if (event.keyCode==13){
				$('#btn-search').click();
			}
		});
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.initGrid();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.initGrid();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
		$('#grid').on('click', '.operating .ui-icon-image', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
        	viewProductImg($("#grid").jqGrid("getRowData", id).sd_pd_code);
        });
	}
}
THISPAGE.init();