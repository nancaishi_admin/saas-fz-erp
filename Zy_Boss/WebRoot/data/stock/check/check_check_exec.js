var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var ba_number = $("#ba_number").val();
var queryurl = config.BASEPATH+'stock/check/check_list4exec/'+ba_number;
var _height = $(parent).height()-240,_width = $(parent).width()-2;

var handle = {
	exec:function(ba_isexec){//1全部处理2部分处理
		var tips = '';
		if(ba_isexec == 1){
			tips = '确定要全部处理吗？';
		}else if(ba_isexec == 2){
			tips = '确定要部分处理吗？';
		}
		$.dialog.confirm(tips, function(){
			$("#btn-exec-all").attr("disabled",true);
			$("#btn-exec-part").attr("disabled",true);
			$.ajax({
				type:"POST",
				url:config.BASEPATH+'stock/check/check_exec/'+ba_number+'/'+ba_isexec,
				data:{},
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						if(callback && typeof callback == 'function'){
							callback({state:"exec_succ"},oper,window);
						}
					}else{
						Public.tips({type: 1, content : data.message});
						$("#btn-exec-all").attr("disabled",false);
						$("#btn-exec-part").attr("disabled",false);
					}
				}
			});
		});	
	},
	missing:function(){
		$.dialog({ 
		   	title:'查看漏盘',
		   	content:'url:'+config.BASEPATH+'stock/check/to_check_missing',
			data : {
				ba_id : $("#ba_id").val(),
				ba_number : $("#ba_number").val(),
				ba_scope : $("#ba_scope").val(),
				ba_scope_code : $("#ba_scope_code").val(),
				dp_code:$("#ba_dp_code").val()
			},
		   	width:800,
		   	height:500,
		   	max: false,
		   	min: false,
		   	cache : false,
			lock: true
		});
	},
	recheck:function(rowid){
		var rowData = $("#grid").jqGrid("getRowData", rowid);
		$.dialog.confirm('确定要重盘吗？', function(){
			$.ajax({
				type:"POST",
				url:config.BASEPATH+'stock/check/check_recheck/'+rowData.ck_number,
				data:{},
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : '重盘成功!'});
						THISPAGE.reloadGridData();
						if(callback && typeof callback == 'function'){
							callback({},oper,window);
						}
					}else{
						Public.tips({type: 1, content : data.message});
					}
				}
			});
		});	
	},
	del:function(rowid){
		$.dialog.confirm('数据删除后无法恢复，确定要删除吗？', function(){
			$.ajax({
				type:"POST",
				url:config.BASEPATH+'stock/check/check_temp_clear_all',
				data:{"ckl_ba_number":ba_number},
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : '删除成功!'});
						$('#grid').jqGrid('delRowData', rowid);
					}else{
						Public.tips({type: 1, content : data.message});
					}
				}
			});
		});	
	},
	formatSubAmount :function(val, opt, row){
		return row.ck_amount-row.ck_stockamount;
	},
	operFmatter :function(val, opt, row){
		var btnHtml = '';
		if($.trim(row.ck_number) == ""){
            btnHtml += '<input type="button" value="删除" class="btn_ck" onclick="javascript:handle.del(' + opt.rowId + ');" />';
        }else {
            btnHtml += '<input type="button" value="重盘" class="btn_ck" onclick="javascript:handle.recheck(' + opt.rowId + ');" />';
        }
		return btnHtml;
	},
	formatState:function(val, opt, row){
		if($.trim(row.ck_number) == ""){
            return '盘点中';
        }else {
        	return '完成';
        }
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();
	},
	initDom:function(){
		
	},
	gridTotal:function(){
    	var grid=$('#grid');
		var ck_stockamount=grid.getCol('ck_stockamount',false,'sum');
		var ck_amount=grid.getCol('ck_amount',false,'sum');
    	grid.footerData('set',{us_name:'合计：',ck_stockamount:ck_stockamount,ck_amount:ck_amount,sub_amount:''});
    	$("#ba_amount").val(ck_amount);
    	$("#ba_stockamount").val(ck_stockamount);
    },
	initGrid:function(){
		var colModel = [
		    {label:'操作',name:'operate', width: 60, fixed:true, formatter: handle.operFmatter, sortable:false,align:'center'},
		    {label:'',name: 'ck_us_id', index: 'ck_us_id', hidden:true},
		    {label:'',name: 'ck_number', index: 'ck_number', hidden:true},
	    	{label:'盘点人',name: 'us_name', index: 'us_name', width: 100},
	    	{label:'状态',name: 'state',  width: 100,formatter: handle.formatState,align:'center'},
	    	{label:'完成时间',name: 'ck_date', index: 'ck_date', width: 140},
	    	{label:'盘点库存',name: 'ck_stockamount', index: 'ck_stockamount', width: 80,align:'right'},
	    	{label:'盘点数量',name: 'ck_amount', index: 'ck_amount', width: 80,align:'right'},
	    	{label:'相差数量',name: 'sub_amount', index: '', width: 70,align:'right',formatter: handle.formatSubAmount},
	    	{label:'备注',name: 'ck_remark', index: 'ck_remark', width: 180}
	    ];
		$('#grid').jqGrid({
			url:queryurl,
			loadonce:true,
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:99,//每页条数
			cmTemplate: {sortable:false,title:true},
			shrinkToFit:false,//表格是否自动填充
            footerrow: true,
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'ck_us_id'  //图标ID
			},
			loadComplete: function(data){
				var ids = $("#grid").getDataIDs();
				THISPAGE.gridTotal();
				var canExec = true;
				var ids = $('#grid').jqGrid('getDataIDs');
				if(ids.length == 0){
					canExec = false;
				}else{
					for(var i=0;i < ids.length;i++){
						var rowData = $('#grid').jqGrid("getRowData", ids[i]);
						if (rowData.ck_number==''){
							$("#grid").setRowData( ids[i], false, { color: '#FF0000' });
							canExec = false;
						}
					}
				}
				if(canExec){
					$("#btn-exec-all").attr("disabled",false);
					$("#btn-exec-part").attr("disabled",false);
				}
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	reloadGridData: function () {
        $("#grid").jqGrid('setGridParam', {datatype: 'json', page: 1, url: queryurl}).trigger("reloadGrid");
    },
	initEvent:function(){
		$("#btn-exec-all").click(function(e){
			e.preventDefault();
			handle.exec(1);
		});
		$("#btn-exec-part").click(function(e){
			e.preventDefault();
			handle.exec(2);
		});
		$("#btn-missing").click(function(e){
			e.preventDefault();
			handle.missing();
		});
		$("#btn_close").click(function(e){
			e.preventDefault();
			api.close();
		});
	}
};

THISPAGE.init();