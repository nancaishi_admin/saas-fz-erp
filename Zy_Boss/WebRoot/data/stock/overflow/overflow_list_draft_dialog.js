var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var api = frameElement.api;
var queryurl = config.BASEPATH+'stock/overflow/page';
var _height = api.config.height-132,_width = api.config.width-34;//获取弹出框弹出宽、高
var Utils = {
	doQueryDepot : function(code,name){
		commonDia = $.dialog({
			title : '选择仓库',
			content : 'url:'+config.BASEPATH+'base/depot/to_list_dialog_bymodule',
			data : {multiselect:false,module:'stock'},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#"+code).val(selected.dp_code);
					$("#"+name).val(selected.dp_name);
				}
			},
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#of_manager").val(selected.em_name);
				}
			},
			cancel:true
		});
	}
};

var dblClick = false;

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
	},
	initGrid:function(){
		var colModel = [
			{name: 'of_dp_code',label:'',index: 'of_dp_code',hidden:true},
	    	{name: 'of_number',label:'单据编号',index: 'of_number',width:150},
	    	{name: 'of_date',label:'申请日期',index: 'of_date',width:80},
	    	{name: 'depot_name',label:'报损仓库',index: 'depot_name',width:120},
	    	{name: 'of_manager',label:'经办人',index: 'of_manager',width:100},
	    	{name: 'of_amount',label:'总计数量',index: 'of_amount',width:80,align:'right'},
	    	{name: 'of_money',label:'金额',index: 'of_money',width:80,align:'right',formatter: PriceLimit.formatByCost},
	    	{name: 'of_remark',label:'备注',index: 'of_remark',width:100,hidden:true}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.BASEROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'of_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow: function (rowid, iRow, iCol, e) {
				dblClick = true;
				api.close();
            }
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'of_isdraft=1';
		params += '&begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&of_dp_code='+$("#of_dp_code").val();
		params += '&of_manager='+Public.encodeURI($("#of_manager").val());
		params += '&of_number='+Public.encodeURI($("#of_number").val());
		return params;
	},
	reset:function(){
		$("#depot_name").val("");
		$("#of_dp_code").val("");
		$("#of_number").val("");
		$("#of_manager").val("");
		THISPAGE.$_date.setValue(0);
		$("#begindate").val("");
		$("#enddate").val("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}

function doSelect(){
	var selectedId = $("#grid").jqGrid('getGridParam', 'selrow');
	if(selectedId == null || selectedId == ""){
		Public.tips({type: 2, content : "请选择草稿！"});
		return false;
	}
	var rowData = $("#grid").jqGrid("getRowData", selectedId);
	return rowData;
}

THISPAGE.init();