
var xdPrint={
	getLodopObject:function(){
		var lopBarCode;
		if(document.getElementById('LODOP_OB')!=null&&document.getElementById('LODOP_OB')!=undefined){
			lopBarCode=getLodop(document.getElementById('LODOP_OB'),document.getElementById('LODOP_EM'));
		}else if (document.getElementById('lopBarCode')==null||document.getElementById('lopBarCode')==undefined){
			var lopBarCode=$('<object id="lopBarCode" classid="clsid:2105C259-1E0C-4534-8141-A753534CB4CA" width=0 height=0>'
					+'<embed id="lopEmBarCode" type="application/x-print-lodop" width=0 height=0 pluginspage="install_lodop32.exe"></embed>'
					+'</object>');
			$('body').append(lopBarCode);
			
			lopBarCode=getLodop(document.getElementById('lopBarCode'),document.getElementById('lopEmBarCode'));
		}else{
			lopBarCode=getLodop(document.getElementById('lopBarCode'),document.getElementById('lopEmBarCode'));	
		}
		return lopBarCode;
	}
	/**
	 *打印条形码
	**/
	,barCode:function (print_state,bc_barcodes,bc_colornames,bc_pd_nos,bc_branames,bc_sizenames,pd_bd_names,pd_sell_prices,pd_sign_prices,pd_vip_prices,pd_grades,pd_safes,pd_fills,bc_pd_names,pd_executes,pd_tp_names,pd_fabrics,pd_in_fabrics,pd_places,pd_wash_explains){
		
		var bs_line = 1;//显示列数
		var left = 0;
		var bs_hight_spacing = 0;
		var bs_width_spacing = 0;
		var bs_width = 35;//条形码宽度
		var bs_height = 15;//条形码高度
		var bs_cr_ifshow = "0";//颜色
		var bs_pdno_ifshow = "0";//货号
		var bs_pdname_ifshow = "0";
		var bs_sz_ifshow = "0";//尺码
		var bs_bs_ifshow = "0";//杯型
		var bs_bd_ifshow = "0";//品牌
		var bs_price_ifshow = "0";//价格
		var bs_signprice_ifshow = "0";//价格
		var bs_vipprice_ifshow = "0";//价格
		var bs_class_ifshow = "0";//打印等级是否显示0:不显示 1:显示
		var bs_executionstandard_ifshow = "0";//打印执行标准是否显示0:不显示 1:显示
		var bs_safestandard_ifshow = "0";//打印标准是否显示0:不显示 1:显示
		var bs_filling_ifshow = "0";
		var bs_infabric_ifshow = "0";
		var bs_type_ifshow = "0";
		var bs_place_ifshow = "0";
		var bs_fabric_ifshow = "0";
		var bs_washexplain_ifshow = "0";
		
		var bs_show_fiename = "1";//显示字段名称
		var bs_cr_row = "0";
		var bs_pdno_row = "0";
		var bs_sz_row = "0";
		var bs_bs_row = "0";
		var bs_bd_row = "0";
		var bs_price_row = "0";
		var bs_vipprice_row = "0";
		var bs_signprice_row = "0";
		var bs_executionstandard_row = "0";
		var bs_infabric_row = "0";
		var bs_type_row = "0";
		var bs_place_row = "0";
		var bs_fabric_row = "0";
		var bs_washexplain_row = "0";
		
		var bs_pdname_row = "0";//品名
		var bs_class_row = "0";//等级
		var bs_safestandard_row = "0";//标准
		var bs_filling_row = "0";//成分
		var bs_font_name = "宋体";//字体
		var bs_font_size = 9;//字体大小
		var bs_bold = "0";//是否加粗
		var bs_p_space = 0;//商品信息间距
		var bs_ps_space = 0;//信息和条码距离
		
		var ptToMm = 0.35;//每增加1字号对应的毫米
		var top = 0;
		var bs_bartype = '128Auto';
		
		var bs_subcode_showup,bs_price_showdown;
		 
		var bs_paper_width = 0;//条形码纸宽度
		var bs_paper_height = 0;//条形码纸高度
		var bs_top_backgauge = 0;//整页上边距
		var bs_left_backgauge = 0;//整页左边距
		var bs_language=0;//语言
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"base/product/ajax_barcode_printset",
			data:"",
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					bs_line = parseInt(data.data.bs_line,10);	
					bs_bartype = data.data.bs_bartype;
					bs_hight_spacing = parseFloat(data.data.bs_hight_spacing,10);
					bs_width_spacing = parseFloat(data.data.bs_width_spacing,10);
					bs_width = parseFloat(data.data.bs_width,10);
					bs_height = parseFloat(data.data.bs_height,10);
					bs_cr_ifshow = data.data.bs_cr_ifshow;
					bs_pdno_ifshow = data.data.bs_pdno_ifshow;
					bs_pdname_ifshow = data.data.bs_pdname_ifshow;
					bs_sz_ifshow = data.data.bs_sz_ifshow;
					bs_bs_ifshow = data.data.bs_bs_ifshow;
					bs_bd_ifshow = data.data.bs_bd_ifshow;
					bs_price_ifshow = data.data.bs_price_ifshow;
					bs_signprice_ifshow = data.data.bs_signprice_ifshow;
					bs_vipprice_ifshow = data.data.bs_vipprice_ifshow;
					bs_class_ifshow = data.data.bs_class_ifshow;
					bs_executionstandard_ifshow= data.data.bs_executionstandard_ifshow;
					bs_safestandard_ifshow = data.data.bs_safestandard_ifshow;
					bs_filling_ifshow = data.data.bs_filling_ifshow;
//					bs_show_fiename = data.data.bs_show_fiename;
					bs_infabric_ifshow = data.data.bs_infabric_ifshow;
					bs_type_ifshow = data.data.bs_type_ifshow;
					bs_place_ifshow = data.data.bs_place_ifshow;
					bs_fabric_ifshow = data.data.bs_fabric_ifshow;
					bs_washexplain_ifshow = data.data.bs_washexplain_ifshow;
					
					bs_font_name = data.data.bs_font_name;
					bs_font_size = parseInt(data.data.bs_font_size,10);//字体
					bs_bold = data.data.bs_bold;
					bs_p_space = parseFloat(data.data.bs_p_space,10);
					bs_ps_space = parseFloat(data.data.bs_ps_space,10);
					
					bs_cr_row = data.data.bs_cr_row;
					bs_pdno_row = data.data.bs_pdno_row;
					bs_sz_row = data.data.bs_sz_row;
					bs_bs_row = data.data.bs_bs_row;
					bs_bd_row = data.data.bs_bd_row;
					bs_price_row = data.data.bs_price_row;
					bs_signprice_row = data.data.bs_signprice_row;
					bs_vipprice_row = data.data.bs_vipprice_row;
					bs_pdname_row = data.data.bs_pdname_row;
					bs_class_row = data.data.bs_class_row;
					bs_safestandard_row = data.data.bs_safestandard_row;
					bs_filling_row = data.data.bs_filling_row;
					bs_executionstandard_row=data.data.bs_executionstandard_row;
					bs_infabric_row=data.data.bs_infabric_row;
					bs_type_row=data.data.bs_type_row;
					bs_place_row=data.data.bs_place_row;
					bs_fabric_row=data.data.bs_fabric_row;
					bs_washexplain_row=data.data.bs_washexplain_row;
					
					bs_subcode_showup=data.data.bs_subcode_showup;
					bs_price_showdown=data.data.bs_price_showdown;
					bs_language = data.data.bs_language;
					
					top = ((bs_font_size*ptToMm)*7)+(bs_p_space*7)+bs_ps_space;//条形码打印方位 = (字体*字高*7行)+商品信息间距+信息和条码距离
					bs_paper_width = parseFloat(data.data.bs_paper_width,10);
					bs_paper_height = parseFloat(data.data.bs_paper_height,10);
					bs_top_backgauge = parseFloat(data.data.bs_top_backgauge,10);
					bs_left_backgauge = parseFloat(data.data.bs_left_backgauge,10);
					var morePaperWidth = (bs_paper_width*bs_line)+2+(bs_width_spacing*(bs_line-1));//多行显示时，纸张宽度
					var lopBarCode=xdPrint.getLodopObject();
					
					lopBarCode.SET_PRINTER_INDEX(-1);
					var piText = "",piNameText = "",crText = ""
						,bsText = "",szText = ""
						,bdText = "",retailText = "￥"
						,priceVipText="￥",priceSignText="￥"
						,classText = "",safeStandardText = ""
						,filling = "",executionStandardText=""
						,typeText = "",fabricText = ""
						,infabricText = "",placeText = ""
						,washexplainText = "";
					if(bs_show_fiename == "1"){//是否显示字段名称
						if(bs_language=="0"){
							piText = "货号:";piNameText = "品名:";
							crText = "颜色:";bsText = "杯型:";
							szText = "尺码:";bdText = "品牌:";
							retailText = "零售价:￥";
							priceVipText = "会员价:￥";
							priceSignText = "标牌价:￥";
							classText = "等级:";
							safeStandardText = "安全标准:";filling = "成分:";
							executionStandardText="执行标准:";
							typeText = "类别：";fabricText = "面料：";
							infabricText = "里料：";placeText = "产地：";
							washexplainText = "洗涤说明：";
						}else{
							piText = "No:";piNameText = "Name:";
							crText = "Col:";bsText = "Cup:";
							szText = "Size:";bdText = "Brand:";
							retailText = "RMB:";
							priceVipText = "vipRMB:";
							priceSignText = "signRMB:";
							classText = "Grade:";
							safeStandardText = "Standard:";filling = "Ingredient:";
							executionStandardText="Executive Standard:";
							typeText = "Type:";fabricText = "Fabric:";
							infabricText = "InFabric:";placeText = "Place:";
							washexplainText = "WashExplain:";
						}
						
					}
					
					var oneInfoHie = (bs_font_size*ptToMm)+bs_p_space;
					var intTag = 1;
					lopBarCode.SET_PRINT_STYLE("FontName",bs_font_name);
					lopBarCode.SET_PRINT_STYLE("FontSize",bs_font_size);
					lopBarCode.SET_PRINT_STYLE("Bold",bs_bold); 
					lopBarCode.SET_PRINT_PAGESIZE(1,morePaperWidth+"mm",bs_paper_height+"mm","USER");
					var countBarCode=bc_barcodes.length-1
						,countBatch=bs_line*25;
					for(var j=0,colIndex=0;j<=countBarCode;j++,colIndex++){
						var topTextHig = 0;
						var printTextArr = new Array();
						if(j%bs_line==0){//新增页面
							lopBarCode.NEWPAGE();
						}
						for ( var iRow = 1; iRow < 20; iRow++) {
							if((bs_cr_ifshow == "1" && bs_cr_row == iRow) || (bs_pdno_ifshow == "1" && bs_pdno_row==iRow) || (bs_sz_ifshow == "1" && bs_sz_row==iRow) 
									|| (bs_bs_ifshow == "1" && bs_bs_row==iRow) || (bs_bd_ifshow == "1" && bs_bd_row==iRow) || (bs_price_ifshow == "1" && bs_price_row==iRow)
									|| (bs_signprice_ifshow == "1" && bs_signprice_row==iRow)||(bs_vipprice_ifshow == "1" && bs_vipprice_row==iRow) 
									|| (bs_pdname_ifshow == "1" && bs_pdname_row==iRow) || (bs_class_ifshow == "1" && bs_class_row==iRow) 
									|| (bs_safestandard_ifshow == "1" && bs_safestandard_row==iRow) || (bs_filling_ifshow == "1" && bs_filling_row==iRow)
									|| (bs_executionstandard_ifshow == "1" && bs_executionstandard_row==iRow)|| (bs_infabric_ifshow == "1" && bs_infabric_row==iRow)
									|| (bs_fabric_ifshow == "1" && bs_fabric_row==iRow)|| (bs_type_ifshow == "1" && bs_type_row==iRow)|| (bs_place_ifshow == "1" && bs_place_row==iRow)
									|| (bs_washexplain_ifshow == "1" && bs_washexplain_row==iRow)){
								var oneRow = "";
								if(bs_pdno_ifshow == "1" && bs_pdno_row == iRow){
									oneRow += piText+bc_pd_nos[j]+" ";
								}
								if(bs_pdname_ifshow == "1" && bs_pdname_row==iRow){
									oneRow += piNameText+bc_pd_names[j]+" ";
								}
								if(bs_cr_ifshow == "1" && bs_cr_row == iRow){
									oneRow += crText+bc_colornames[j]+" ";
								}
								if(bs_sz_ifshow == "1" && bs_sz_row==iRow){
									oneRow += szText+bc_sizenames[j]+" ";
								}
								if(bs_bs_ifshow == "1" && bs_bs_row==iRow){
									oneRow += bsText+bc_branames[j]+" ";
								}
								if(bs_bd_ifshow == "1" && bs_bd_row==iRow){
									oneRow += bdText+pd_bd_names[j]+" ";
								}
								if(bs_price_ifshow == "1" && bs_price_row==iRow){
									oneRow += retailText+pd_sell_prices[j]+" ";
								}
								if(bs_signprice_ifshow == "1" && bs_signprice_row==iRow){
									oneRow += priceSignText+pd_sign_prices[j]+" ";
								}
								if(bs_vipprice_ifshow == "1" && bs_vipprice_row==iRow){
									oneRow += priceVipText+pd_vip_prices[j]+" ";
								}
								if(bs_class_ifshow == "1" && bs_class_row==iRow){
									oneRow += classText+pd_grades[j]+" ";
								}
								if(bs_safestandard_ifshow == "1" && bs_safestandard_row==iRow){
									oneRow += safeStandardText+pd_safes[j]+" ";
								}
								if(bs_filling_ifshow == "1" && bs_filling_row==iRow){
									oneRow += filling+pd_fills[j]+" ";
								}
								if(bs_executionstandard_ifshow == "1" && bs_executionstandard_row==iRow){
									oneRow += executionStandardText+pd_executes[j]+" ";
								}
								if(bs_infabric_ifshow == "1" && bs_infabric_row==iRow){
									oneRow += infabricText+pd_in_fabrics[j]+" ";
								}
								if(bs_fabric_ifshow == "1" && bs_fabric_row==iRow){
									oneRow += fabricText+pd_fabrics[j]+" ";
								}
								if(bs_type_ifshow == "1" && bs_type_row==iRow){
									oneRow += typeText+pd_tp_names[j]+" ";
								}
								if(bs_place_ifshow == "1" && bs_place_row==iRow){
									oneRow += placeText+pd_places[j]+" ";
								}
								if(bs_washexplain_ifshow == "1" && bs_washexplain_row==iRow){
									oneRow += washexplainText+pd_wash_explains[j]+" ";
								}
								printTextArr.push(oneRow);
							}else if(j == 0){
								top = top - oneInfoHie;
							}
						}
						if (colIndex==bs_line){//列数
							colIndex=0;
						}
						
						var leftPage=bs_left_backgauge+bs_paper_width*colIndex+(colIndex*bs_width_spacing);
						lopBarCode.ADD_PRINT_TEXT(bs_top_backgauge,leftPage+"mm",bs_paper_width+"mm","100%",printTextArr[0]);
						lopBarCode.SET_PRINT_STYLEA(0,"LineSpacing",(-2+bs_p_space)+"mm");
						for(var k=1;k<printTextArr.length;k++){ 
							lopBarCode.ADD_PRINT_TEXT(bs_p_space+"mm",0,bs_paper_width+"mm","100%",printTextArr[k]);
							lopBarCode.SET_PRINT_STYLEA(0,"LinkedItem",-1);
							lopBarCode.SET_PRINT_STYLEA(0,"LineSpacing",(-2+bs_p_space)+"mm");	
						}
						
						if (bs_subcode_showup==1){//条形码文字
							lopBarCode.ADD_PRINT_TEXT(bs_p_space+"mm",0,bs_paper_width+"mm","100%",bc_barcodes[j]);
							lopBarCode.SET_PRINT_STYLEA("Last","LinkedItem",-1);
							lopBarCode.SET_PRINT_STYLEA(0,"LineSpacing",(-2+bs_p_space)+"mm");	
						}					
						lopBarCode.ADD_PRINT_BARCODE(bs_ps_space+"mm","0mm",bs_width+"mm",bs_height+"mm",bs_bartype,bc_barcodes[j]);
						lopBarCode.SET_PRINT_STYLEA(0,"ShowBarText",1);
						lopBarCode.SET_PRINT_STYLEA("Last","LinkedItem",-1);
						lopBarCode.SET_PRINT_STYLEA("Last","LineSpacing",(-2+bs_p_space)+"mm");
							
						if (bs_price_showdown==1){//底部零售价
							lopBarCode.ADD_PRINT_TEXT((bs_p_space+bs_height+1)+"mm","0mm",bs_paper_width+"mm","100%",retailText+pd_sell_prices[j]);//RightMargin:1mm
							lopBarCode.SET_PRINT_STYLEA(0,"LinkedItem",-2);
							lopBarCode.SET_PRINT_STYLEA(0,"LineSpacing",(-2+bs_p_space)+"mm");	
						}
						
						if (
								(countBarCode==j)//最后一个条码
								||(j>0&&(j+1)%countBatch==0)//
								){
							if(print_state == '0'){//直接打印
								lopBarCode.PRINT();
							}else{//打印预览
								lopBarCode.PREVIEW();
							}
							lopBarCode.SET_PRINT_STYLE("FontName",bs_font_name);
							lopBarCode.SET_PRINT_STYLE("FontSize",bs_font_size);
							lopBarCode.SET_PRINT_STYLE("Bold",bs_bold); 
							lopBarCode.SET_PRINT_PAGESIZE(1,morePaperWidth+"mm",bs_paper_height+"mm","USER");	
						}
					}
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
	}
}
