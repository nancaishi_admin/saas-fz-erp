<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/comm-search.js\"></sc"+"ript>");
</script>
</head>
<body>
<form name="form1" method="post" action="" id="form1">
<div class="wrapper">
	 <div class="mod-search cf">
		 <div class="fl">
			<div id="filter-menu" class="ui-btn-menu fl">
	   			<span style="float:left" >
	     			<strong>年份：</strong>
	     			<span class="ui-combo-wrap" id="span_vm_year" style="width:58px;"></span>
                    <input type="hidden" id="vm_year" name="vm_year" value=""/>
		  		</span>
		 		&nbsp;&nbsp;店&nbsp;&nbsp;铺：
				<input type="text" id="shop_name" name="shop_name" class="main_Input atfer_select" readonly="readonly" value=""/>
				<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryShop();"/>
				<input type="hidden" id="vm_shop_code" name="vm_shop_code" value=""/>
			</div>
		 </div>
	     <div class="fl-m">
	     	<a class="ui-btn ui-btn-sp mrb" id="btn_search">查询</a>
       		<a class="ui-btn mrb" id="btn_reset">重置</a>
	     </div>
	 </div>
	 <!-- 报 表   -->
    <div class="grid-wrap" style="float:left;width:500px;">
        <table id="grid">
        </table>
        <div id="page"></div>
    </div>
    <!-- 柱形图   -->
    <div id="container" style="background:#fff;margin-left:15px;float:left;border:#ccc solid 1px;" ></div>
</div>
</form>

<script type="text/javascript">
document.getElementById("container").style.height=(document.documentElement.clientHeight-80) + "px";
document.write("<scr"+"ipt src=\"<%=basePath%>resources/chart/highcharts.4.0.3.js\"></sc"+"ript>");
document.getElementById("container").style.width=$(window).width()-550 + "px";
</script>
<script src="<%=basePath%>data/vip/report/vipconsume_month_compare.js"></script>
</body>
</html>
