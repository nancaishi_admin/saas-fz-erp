<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<style>
	.searchbar td {padding: 2px  2px}
	.tab_nav{float:left;margin-left:10px;height:35px;border-radius:5px;overflow:hidden;}
	.tab_nav a{float:left;height:33px;padding:0 10px;line-height:33px;border:#ddd solid 1px;margin:0;background:#f8f8f8;}
	.tab_nav a.selted{background:#5bb75b;color:#fff;border:#0b0 solid 1px;}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery-1.11.1.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=false&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/comm-search.js\"></sc"+"ript>");
</script> 
</head>
<body id="main_content">
<input type="hidden" name="rts_day" id="rts_day" value="${rts_day}"/>
<form name="form1" method="post" action="" id="form1">
<input type="hidden" name="flushFlag" id="flushFlag" value=""/>
<div class="mainwra">
	<div class="mod-search cf">
		<div class="fl">
			<div id="filter-menu" class="ui-btn-menu fl">
		   		 <span style="float:left" class="ui-btn menu-btn">
		     		 <strong>消费店铺</strong>
	   				 <input type="text" id="sp_name" name="sp_name" class="ui-input ui-input-ph" style="width:158px" value=""/>
	   				 <input type="button" class="btn_select" id="btn_shop" name="btn_shop"/>
	   				 <input type="hidden" id="card_shop_code" name="card_shop_code" value=""/>
					 <b></b>
			  	 </span>
			 	<div class="con" style="width:340px;" >
					<ul class="ul-inline">
						<li style="width:340px;">
							<table class="searchbar">
								  <tr>
									  <td>发卡人员：
								  		<input type="text" id="card_manager" name="card_manager" class="main_Input"  style="width:158px" value=""/>
								  		<input type="button" class="btn_select" id="btn_emp" name="btn_emp"/>
			        					<input type="hidden" id="card_manager_code" name="card_manager_code" value=""/>
							  		</td>
								  </tr>
								  <tr>
										<td>会员卡号：
										    <input type="text" id="card_code" name="card_code" class="main_Input" style="width:184px" value=""/>
										</td>
								  </tr>
								  <tr>
										 <td>消费金额：
										 	<input type="text" id="begin_buy_money" name="begin_buy_money" class="main_Input" value="" style="width: 77px;" onkeyup="this.value=this.value.replace(/[^\d]/g,'')"/>至
										 	<input type="text" id="end_buy_money" name="end_buy_money" class="main_Input" value="" style="width: 77px;" onkeyup="this.value=this.value.replace(/[^\d]/g,'')"/>
										</td>
								  </tr>
								  <tr>
									 <td>回访状态：
									    <span class="ui-combo-wrap" id="span_visit"></span>
		            					<input type="hidden" id="visit" name="visit" value="0"/> 
									 </td>
								 </tr>
							</table>
						</li>
						<li style="float:right;margin-top:10px;">
							<a class="ui-btn ui-btn-sp mrb" id="btn_ok">确认</a>
		       				<a class="ui-btn mrb" id="btn_reset">重置</a>
						</li>
					</ul>
				</div>
			</div>
			<span style="float:left;margin-left:10px;">
				<a class="ui-btn fl mrb" id="btn_Search">查询</a>
			</span>
			<span class="tab_nav">
				<c:forEach var="setup" items="${returnSetUps}" varStatus="status">
					<a href="javascript:void(0);" class="${setup.rts_second==1? 'selted' : ''}" id="button_${setup.rts_day}" onclick="javascript:handle.doChangeMode(this, ${setup.rts_day});">${setup.rts_name}</a>
				</c:forEach>
			</span>
		</div>
		<!-- <div class="fr">
	      <a href="javascript:void(0);" class="ui-btn ui-btn-sp mrb" id="btn-export" >导出</a>
	    </div> -->
	</div>
	<div class="grid-wrap">
	    <table id="grid">
	    </table>
	    <div id="page"></div>
	</div>
</div>
</form>
<script src="<%=basePath%>data/vip/visit/visit_list.js"></script>
</body>
</html>