<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title>消费回访设置添加</title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
</script> 
</head>
<body>
<table class="pad_t20"  style="font-size: 12px" >
	<col style="width:120px"/>
	<col style="width:300px"/>
	<tr>
		<td align="right"><font color="red">*</font>回访名称：</td>
		<td align="left">
			<input class="main_Input" type="text" name="rts_name" id="rts_name" value=""/>
		</td>
		
	</tr>
	<tr>
	   <td align="right"><font color="red">*</font>回访天数：</td>
		<td align="left">
			<input class="main_Input" type="text" name="rts_day" id="rts_day" value=""/>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<input type="button" value="确认" class="btn_close" id="submit_id" name="submit_id" onclick="javascript:saveReturnInfo();"/>
			<input type="button" value="关闭" class="btn_close" onclick="javascript:doClose();"/>
		</td>
	</tr>
</table>
</body>
<script>
	var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
    var parentTotal=parent.vipReturn.rows.length;//已有会员回访设置个数
    var allDaysArray=parent.$("input[name='allDaysArray']").val();//回访日期所形成的字符集
    //将字符串拆成数组
    var daysArray=allDaysArray.split(",");
 	var api = frameElement.api, W = api.opener;
 	$(document).ready(function(){
		$("#rts_name").focus();
	});
	
	//保存消费回访设置
	function saveReturnInfo(){
	    var rts_name = $.trim($("#rts_name").val());
	    if(rts_name == ""){
	    	Public.tips({type: 2, content : '请输入回访名称！'});
	    	return;
	    }
		var rts_day = $.trim($("#rts_day").val());
	    if(rts_day == ""){
	    	Public.tips({type: 2, content : '请输入回访天数！'});
	    	return;
	    }else if(isNaN(rts_day)){
	    	Public.tips({type: 2, content : '回访天数只能输入数字！'});
	    	return;
	    }else{
	       //如果输入天数，和数组中重复，则重新输入
	       for(var i=0;i<daysArray.length;i++){
	          if(daysArray[i]==rts_day){
	        	 Public.tips({type: 2, content : '回访天数已存在！'});
	             return;
	          }
	       }
	    }
	    //第几次回访
	    var rts_second=parentTotal+1;
		$.ajax({
	    	async:false,
			type:"POST",
			url:config.BASEPATH+"vip/set/saveReturnSet",
			data:"rts_name="+encodeURI(encodeURI(rts_name))+"&rts_day="+rts_day+"&rts_second="+rts_second,
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : '添加成功!'});
					api.close();
				}else{
					Public.tips({type: 1, content : '删除失败,请重试!'});
				}
			}
		});
	         
	}
	 // 关闭按钮
	function doClose(){
		api.close();
	}
	
</script>
</html>