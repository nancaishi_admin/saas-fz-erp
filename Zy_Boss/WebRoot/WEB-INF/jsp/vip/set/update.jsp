<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
* { margin:0; padding:0; list-style:none;}
html { height:100%; overflow:hidden; background:#fff;font-size:12px;color:#666;}
body { height:100%; overflow:hidden;}
div {background:#fff; line-height:1.6;} 
.main { background:#fff; position:absolute; right:15px; top:15px; bottom:75px; left:15px; overflow:auto;overflow-x:hidden;padding:10px;}
.bottom { position:absolute; left:15px; bottom:15px; right:15px; height:50px;}
html { _padding:15px 15px 50px;} 
.main { _height:100%; _margin-right:15px; _position:relative; _top:0; _right:0; _bottom:0; _left:0;}
.bottom { _height:50px;_margin-right:15px; _margin-top:10px; _position:relative; _top:0; _right:0; _bottom:0; _left:0;}
.vipsetlist{border:none;float:left;width:100%;margin-top:10px;}
.vipsetlist h3{border-bottom:#08c solid 1px;float:left;width:100%;height:30px;line-height:30px;}

.tabs{border-style:solid;border-color:#ccc;border-width:1px 0 0px 1px;height:34px;line-height:34px;width:360px;margin:10px 0 0 0px;overflow:hidden;}
.tabs li{display:inline;float:left;width:89px;height:34px;border-right:#ccc solid 1px;overflow:hidden;cursor:pointer;text-align:center;}
.ontab{background:#eee;border-top:#08c solid 2px;height:32px;color:#08c;font-weight:700;}
.offtab{color:#666;}
.block{display:block;}
.tabcontent{width:100%;border:#ddd solid 0px;height:300px;overflow:hidden;margin-top:0px;}
.tabcontent ul{width:100%;height:300px;overflow-x:hidden;overflow-y:auto;}

.tables{border:#ccc solid 1px;border-collapse:collapse;line-height:40px;}
.tables th{border:#ddd solid 1px;background:#f1f1f1;}
.tables td{border:#ddd solid 1px;}

.dottable{line-height:40px;}
.dottable td{border-bottom:#ddd dotted 1px;height:40px;}

.bottons{background:#08b;border:none;color:#fff;font-size:12px;height:28px;width:40px;text-align:center;}
.inputs{border:#ccc solid 1px;height:23px;line-height:23px;padding:0 5px;}
.colorPackerBox{position:absolute;width:216px;overflow:hidden;border-left:1px solid #e3e3e3;border-top:1px solid #e3e3e3;}
.div_cellBox{float:left;width:72px;}
.span_colorCell{float:left;width:11px;height:11px;border-right:1px solid #e3e3e3;border-bottom:1px solid #e3e3e3;text-indent:-999em;overflow:hidden;cursor:hand;cursor:pointer;}
.overShowbox{position:relative;background:#e3e3e3;padding:0 0 1px;border:1px solid #eee;overflow:hidden;text-align:left;font-size:11px;clear:both; }
.overShowbox span{display: -moz-inline-stack;display:inline-block;*display:inline;*zoom:1;height:15px;line-height:15px;vertical-align:middle;text-align:left;width:58px;}
.overShowbox span.span_overBg{width:40px;margin-right:5px;}
.overShowbox span.span_close{font-size:12px;position:absolute;top:0px;right:1px;width:26px;cursor:hand;cursor:pointer;}
.bodnone{border:#fff solid 1px;}
.bodov{border:#0af solid 1px;}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.soColorPacker-1.0.js\"></sc"+"ript>");
</script> 
</head>
<body id="main_content">
<form name="form1" method="post" action="" id="form1">
<div class="main" style="border:#ccc solid 1px;">
	<div class="vipsetlist">
    	<h3>会员等级设置</h3>
        <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-top:10px;" class="tables">
			<tr>
				<th style="width:80px;">等级</th>    
				<th style="width:100px;">消费下限</th>
				<th style="width:100px;">消费上限</th>
				<th style="width:80px;">颜色</th>
				<th >内容说明</th>		
			</tr>
			<c:choose>
			<c:when test="${not empty grades}">
				<c:forEach var="grade" items="${grades }" varStatus="status">
					<tr>
						<td align="left">
							<input type="hidden" name="gd_code${status.index+1 }" value="${grade.gd_code }" />
							<input class="main_Input bodnone" type="text" id="gd_name${status.index+1 }" name="gd_name${status.index+1 }" 
							value="${grade.gd_name }" onfocus="this.className='main_Input bodov'"
							onblur="javascript:handle.upperOnblur('gd_lower${status.index+1 }','gd_upper${status.index+1 }','gd_remark${status.index+1 }','gd_name${status.index+1 }');this.className='main_Input bodnone'" 
							style="width:60px"/></td>
						<td align="left">
							<input class="main_Input" type="text" id="gd_lower${status.index+1 }" name="gd_lower${status.index+1 }" 
								value="${grade.gd_lower }" style="width:80px" onfocus="this.className='main_Input bodov'"
								onkeyup="javascript:handle.lowerOnblur('gd_lower${status.index+1 }','gd_upper${status.index+1 }','gd_remark${status.index+1 }','gd_name${status.index+1 }');this.className='main_Input bodnone'" 
							/>
						</td>
						<td align="left">
							<input class="main_Input bodnone" type="text" id="gd_upper${status.index+1 }" name="gd_upper${status.index+1 }" 
								value="${grade.gd_upper }" style="width:80px" onfocus="this.className='main_Input bodov'"
								onkeyup="javascript:handle.upperOnblur('gd_lower${status.index+1 }','gd_upper${status.index+1 }','gd_remark${status.index+1 }','gd_name${status.index+1 }');this.className='main_Input bodnone'" 
								/>
						</td>
						<td align="left">
							<input class="main_Input bodnone" type="text" name="gd_color${status.index+1 }" id="txt_pack_${status.index+1 }"
								value="${grade.gd_color }" 
								style="width:60px; background-color: ${grade.gd_color };color:${grade.gd_color };cursor:hand" 
								onclick="javascript:handle.clickColor(this);"/>
						</td>
						<td align="left">
							<input class="main_Input bodnone" onfocus="this.className='main_Input bodov'" 
								onblur="javascript:handle.upperOnblur('gd_lower${status.index+1 }','gd_upper${status.index+1 }','gd_remark${status.index+1 }','gd_name${status.index+1 }');this.className='main_Input bodnone'" 
								type="text" id="gd_remark${status.index+1 }" name="gd_remark${status.index+1 }" value="${grade.gd_remark }" style="width:400px;" />
						</td>
					</tr>
				</c:forEach>
			</c:when>
			<c:otherwise>
				<c:forEach var="item" begin="1" end="4" step="1" >
					<tr>
						<td align="left">
							<input type="hidden" name="gd_code${item }" value="${item }" />
							<input class="main_Input" type="text" id="gd_name${item }" name="gd_name${item }" 
							value=""
							onblur="javascript:handle.upperOnblur('gd_lower${item }','gd_upper${item }','gd_remark${item }','gd_name${item }');" 
							style="width:60px"/></td>
						<td align="left">
							<input class="main_Input" type="text" id="gd_lower${item }" name="gd_lower${item }" 
								value="0" style="width:80px" 
								onkeyup="javascript:handle.lowerOnblur('gd_lower${item }','gd_upper${item }','gd_remark${item }','gd_name${item }');" 
							/>
						</td>
						<td align="left">
							<input class="main_Input" type="text" id="gd_upper${item }" name="gd_upper${item }" 
								value="1" style="width:80px"
								onkeyup="javascript:handle.upperOnblur('gd_lower${item }','gd_upper${item }','gd_remark${item }','gd_name${item }');" 
								/>
						</td>
						<td align="left">
							<input class="main_Input" type="text" name="gd_color${item }" id="txt_pack_${item }"
								value="" 
								style="width:60px; cursor:hand" 
								onclick="javascript:handle.clickColor(this);"/>
						</td>
						<td align="left"><input class="main_Input" type="text" id="gd_remark${item }" name="gd_remark${item }" value="" style="width:400px;" /></td>
					</tr>
				</c:forEach>
			</c:otherwise>
			</c:choose>
			<tr>
				<td colspan="5">
					<span style="color:blue;margin-left:10px;">注：这里的消费是指周期的消费。</span>
				</td>
			</tr>
		</table>
    </div>
	<div class="vipsetlist">
    	<h3>生日提醒设置</h3>
        <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-top:10px;" class="dottable">
        <tr>
           <td align="right">店铺：</td>
          <td>
             	<input type="text" id="sp_name" name="sp_name" class="ui-input ui-input-ph" style="width:158px;" readonly="readonly" value="${birthdaySms.shop_name }"/>
   				<input type="button" class="btn_select" id="btn_shop"/>
   				<input type="hidden" id="vb_shop_code" name="vb_shop_code" value="${birthdaySms.vb_shop_code }"/>
          </td>
        </tr>
		<tr>
		  
          <td width="120" align="right">短信发送模式：</td>
          <td>
          	<input name="vb_state" type="radio" value="0" ${birthdaySms.vb_state eq 0 ? 'checked' : '' }/>手动发送
          	<input name="vb_state" type="radio" value="1" ${birthdaySms.vb_state eq 1 ? 'checked' : '' }/>自动发送
          </td>
        </tr>
        <tr>
          <td width="120" align="right">提前天数：</td>
          <td>
            会员生日提前<input class="main_Input" type="text" id="vb_agowarn_day" name="vb_agowarn_day" style="width:50px;" value="${birthdaySms.vb_agowarn_day }"/>天提醒
          </td>
        </tr>
        <tr>
          <td width="120" align="right" valign="top">短信内容：</td>
          <td>
            <textarea id="vb_sms_content" name="vb_sms_content"   onkeyup="handle.smsWordNumber()" style="color:#888;font-size: 10pt;font-family: 黑体,宋体(GB);margin-top:5px;width:450px;height:100px;border:#ccc solid 1px;overflow-x:hidden;overflow-y:auto;">${birthdaySms.vb_sms_content }</textarea>
            <p style="color: red;">最多输入63个字，还可以输入<input disabled maxLength="4" style="text-align:center;" id="total" name="total" size="3" value="${63-fn:length(birthdaySms.vb_sms_content)}"/>个字</p>
          </td>
        </tr>
	</table>
    </div>
    <div class="vipsetlist">
    	<h3>消费回访设置 <input name="button" type="button" class="bottons" id="button" value="添加" onclick="javascript:handle.returnSet();" /></h3>
    	<c:choose>
    	<c:when test="${empty returnSetUps}">
			<table width="100%" id="vipReturn" class="dottable">
				<tr>
		          <td width="120" align="right">第1次：<input type="hidden" id="rts_second1" name="rts_second" value="1"/></td>
		          <td>
		            <input class="main_Input" type="text" id="rts_name1" name="rts_name1" style="width:80px;text-align: right;" value="感恩回访"/>
		          	<input class="main_Input" type="text" id="rts_day1" name="rts_day1" style="width:50px;text-align: right;" value="1"/> 天
		          </td>
		        </tr>
				<tr>
		          <td width="120" align="right">第2次：<input type="hidden" id="rts_second2" name="rts_second" value="2"/></td>
		          <td>
		            <input class="main_Input" type="text" id="rts_name2" name="rts_name2" style="width:80px;text-align: right;" value="洗涤保养回访"/>
		          	<input class="main_Input" type="text" id="rts_day2" name="rts_day2" style="width:50px;text-align: right;" value="3"/> 天
		          	<input type="button" class="btn_ck" value="删除" id="" onclick="javascript:handle.delTableRowNoSave(this);"/>
		          </td>
		        </tr>
				<tr>
		          <td width="120" align="right">第3次：<input type="hidden" id="rts_second3" name="rts_second" value="3"/></td>
		          <td>
		            <input class="main_Input" type="text" id="rts_name3" name="rts_name3" style="width:80px;text-align: right;" value="满意度回访"/>
		          	<input class="main_Input" type="text" id="rts_day3" name="rts_day3" style="width:50px;text-align: right;" value="7"/> 天
		             <input type="button" class="btn_ck" value="删除" id="" onclick="javascript:handle.delTableRowNoSave(this);"/>
		              <input type="hidden" name="allDaysArray" value="1,3,7"/><!-- 必须配置 -->
		          </td>
		        </tr>
			</table>
		</c:when>
		<c:otherwise>
		<table width="100%" border="0" cellpadding="0" cellspacing="0" id="vipReturn" class="dottable">
		<input type="hidden" name="allDaysArray" value="${allDaysArray}" /> 
		<c:forEach var="returnSetUp" items="${returnSetUps }">
			<tr>
	          <td width="120" align="right">第${returnSetUp.rts_second}次：
	          	<input type="hidden" id="rts_second${returnSetUp.rts_second}" name="rts_second" value="${returnSetUp.rts_second}"/>
	          </td>
	          <td>
	          	<input class="main_Input" type="text" id="rts_name${returnSetUp.rts_second}" name="rts_name${returnSetUp.rts_second}" style="width:80px;text-align: right;" value="${returnSetUp.rts_name}"/>
	          	<input class="main_Input" type="text" id="rts_day${returnSetUp.rts_second}" name="rts_day${returnSetUp.rts_second}" style="width:50px;text-align: right;" value="${returnSetUp.rts_day}"/> 天
	          	<c:if test="${returnSetUp.rts_second != 1}">
	          	<input type="button" class="btn_ck" value="删除" id="" onclick="javascript:handle.delTableRow(this,'${returnSetUp.rts_id}');"/>
	          	</c:if>
	          </td>
	        </tr>
	    </c:forEach>
		</table>
		</c:otherwise>
		</c:choose>
    </div>
    <div class="vipsetlist">
    	<h3>会员年龄段设置 <input name="button" type="button" class="bottons" id="button" value="添加" onclick="javascript:handle.ageGroupSet();" /></h3>
		<table width="100%" border="0" cellpadding="0" cellspacing="0" id="vipAgeGroup" class="tables">
			<tr>
				<th style="width:80px;">编号</th>
				<th style="width:80px;">≥起始年龄</th>    
				<th style="width:80px;">≤终止年龄</th>
				<th style="width:100px;">操作</th>
				<th></th>	
			</tr>
			<c:choose>
			<c:when test="${not empty ageGroupSetUp}">
			<c:forEach var="ageGroupSetUp" items="${ageGroupSetUp }" varStatus="status">
			<tr>
				<td style="text-align: center;">${status.index + 1 }</td>
				<td style="text-align: center;">${ageGroupSetUp.ags_beg_age }</td>
				<td style="text-align: center;">${ageGroupSetUp.ags_end_age }</td>
				<td style="text-align: center;">
					<input type="button" class="btn_ck" value="修改" id="" onclick="javascript:handle.updateAgeGroupSet(this,'${ageGroupSetUp.ags_id}');"/>
					<input type="button" class="btn_ck" value="删除" id="" onclick="javascript:handle.delAgeGroupRow(this,'${ageGroupSetUp.ags_id}');"/>
				</td>
				<td></td>
	        </tr>
	    	</c:forEach>
	    	</c:when>
	    	</c:choose>
		</table>
    </div>
    <div class="vipsetlist">
    	<h3>系统参数</h3>
        <table  width="100%"  border="0" cellpadding="0" cellspacing="0" style="margin-top:10px;" class="dottable">
	        <tr>
	          <td width="120">休眠客户消费天数：</td>
	          <td width="80">
				<input type="text" class="main_Input" style="width: 40px;" value="${lossDay }"  onkeyup="javascript:handle.doCheckNumber(this);" onkeydown="javascript:handle.doCheckNumber(this);" id="vs_loss_day" name="vs_loss_day" />天
	          </td>
	          <td>默认值180天,如需要变动，请输入具体数字</td>
	        </tr>
	        <tr>
	          <td>会员默认消费周期：</td>
	          <td>
				<input type="text" class="main_Input" style="width: 40px;" value="${consumeDay }"  onkeyup="javascript:handle.doCheckNumber(this);" onkeydown="javascript:handle.doCheckNumber(this);" id="vs_consume_day" name="vs_consume_day" />天
	          </td>
	          <td>默认值90天,如需要变动，请输入具体数字</td>
	        </tr>
	        <tr>
	          <td>忠诚客户消费次数：</td>
	          <td>
				<input type="text" class="main_Input" style="width: 40px;" value="${loyalVipTimes }"  onkeyup="value=value.replace(/[^\d]/g,'')" id="vs_loyalvip_times" name="vs_loyalvip_times" />次
	          </td>
	          <td>默认值3次,如需要变动，请输入具体数字</td>
	        </tr>
	        <tr>
	          <td>高价客户消费金额：</td>
	          <td>
				<input type="text" class="main_Input" style="width: 40px;" value="${richVipMoney }"  onkeyup="value=value.replace(/[^\d.]/g,'')" id="vs_richvip_money" name="vs_richvip_money" />元
	          </td>
	          <td>默认值1000元,如需要变动，请输入具体数字</td>
	        </tr>
		</table>
    </div>
</div>
<div class="bottom">
	<a class="t-btn btn-red" style="margin-top:9px;" onclick="javascript:handle.updateVipSet();">保存</a>
</div>
</form>
<script src="<%=basePath%>data/vip/set/vipset_update.js"></script>
</body>
</html>