<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
.left-tree{border:#ddd solid 1px;width:240px;top:15px;left:15px;bottom:15px;position:absolute;font-family:Microsoft YaHei;overflow-x:hidden;overflow-y:auto;}
.left-tree ul{float:let;margin:10px 15px 0;width:210px;height:auto;}
.left-tree ul h3{cursor:pointer;float:left;height:30px;line-height:30px;width:210px;overflow:hidden;}
.left-tree ul li{cursor:pointer;float:left;width:200px;line-height:30px;overflow:auto;padding-left:10px;display:none;}
.left-tree ul li dl{float:left;width:190px;line-height:30px;overflow:auto;padding-left:10px;display:none;}
.left-tree ul li i{font-size:10px;}
.right-div{top:15px;left:265px;right:15px;bottom:15px;position:absolute;}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery-1.11.1.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/comm-search.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
</script> 
</head>
<body id="main_content">
<form name="form1" method="post" action="" id="form1">
<input type="hidden" name="flushFlag" id="flushFlag" value=""/>

<input type="hidden" id="lossDay" name="lossDay" value="${lossDay }"/>
<input type="hidden" id="consumeDay" name="consumeDay"  value="${consumeDay }"/>
<input type="hidden" id="loyalVipTimes" name="loyalVipTimes"  value="${loyalVipTimes }"/>
<input type="hidden" id="richVipMoney" name="richVipMoney"  value="${richVipMoney }"/>
<input type="hidden" id="select_member_type" name="select_member_type" value=""/>
<div class="left-tree">
	<ul>
		<h3><i class="iconfont">&#xe63c;</i>会员模型</h3>
		<li>
			<span><i class="iconfont">&#xe65b;</i> 活跃会员</span>
			<dl>
				<dd><a href="javascript:;" onclick="Utils.queryVipMembers(1);">活跃-忠诚-高价</a></dd>
				<dd><a href="javascript:;" onclick="Utils.queryVipMembers(2);">活跃-忠诚-低价</a></dd>
				<dd><a href="javascript:;" onclick="Utils.queryVipMembers(3);">活跃-非忠诚-高价</a></dd>
				<dd><a href="javascript:;" onclick="Utils.queryVipMembers(4);">活跃-非忠诚-低价</a></dd>
			</dl>
		</li>
		<li>
			<span><i class="iconfont">&#xe65b;</i> 休眠会员</span>
			<dl>
				<dd><a href="javascript:;" onclick="Utils.queryVipMembers(5);">休眠的-忠诚-高价客户</a></dd>
				<dd><a href="javascript:;" onclick="Utils.queryVipMembers(6);">休眠的-忠诚-低价客户</a></dd>
				<dd><a href="javascript:;" onclick="Utils.queryVipMembers(7);">休眠的-非忠诚-高价客户</a></dd>
				<dd><a href="javascript:;" onclick="Utils.queryVipMembers(8);">休眠的-非忠诚-低价客户</a></dd>
			</dl>
		</li>
		<li>
			<span><i class="iconfont">&#xe65b;</i> 流失会员</span>
			<dl>
				<dd><a href="javascript:;" onclick="Utils.queryVipMembers(9);">流失的-忠诚-高价客户</a></dd>
				<dd><a href="javascript:;" onclick="Utils.queryVipMembers(10);">流失的-忠诚-低价客户</a></dd>
				<dd><a href="javascript:;" onclick="Utils.queryVipMembers(11);">流失的-非忠诚-高价客户</a></dd>
				<dd><a href="javascript:;" onclick="Utils.queryVipMembers(12);">流失的-非忠诚-低价客户</a></dd>
			</dl>
		</li>
	</ul>
</div>
<div class="right-div">
	<div class="mod-search cf">
	   <div class="fl">
	    	<div id="filter-menu" class="ui-btn-menu fl">
	    		 <span style="float:left" class="ui-btn menu-btn">
		     		 <strong>查询条件</strong>
		     			 <input type="text" class="main_Input Wdate-select" readonly="readonly" id="begindate" name="begindate" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'enddate\',{d:0})}'})"  value="" />
							-
	                     <input type="text" class="main_Input Wdate-select" readonly="readonly" id="enddate" name="enddate" onclick="WdatePicker({minDate:'#F{$dp.$D(\'begindate\',{d:0})}'})"   value="" />
					 <b></b>
			  	 </span>
		 		<div class="con" >
					<ul class="ul-inline">
						<li>
							<table class="searchbar">
								<tr>     
									<td align="right">日期选择：</td>
									<td id="date">
			                            <label class="radio" id="date_default" style="width:30px">
							  	 			<input name="redio1" type="radio" id="theDate" value="1" checked="checked"/>今天
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="yesterday" value="2" />昨天
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theWeek" value="3" />本周
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theMonth" value="4" />本月
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theSeason" value="5" />本季
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theYear" value="6" />本年
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="lastYear" value="7" />去年
								  	 	</label>
							  	 	</td>
								 </tr>
								 <tr>
									  <td align="right">会员类别：</td>
									  <td>
									  	<input class="main_Input" type="text" id="mt_name" name="mt_name" value="" readonly="readonly" style="width:170px; " />
									     <input type="button" id="btn_membertype" name="btn_membertype" value="" class="btn_select"/>
									     <input type="hidden" name="vm_mt_code" id="vm_mt_code" value=""/>
							  		</td>
								  </tr>
								  <tr>
									  <td align="right">发卡店铺：</td>
									  <td>
									  	<input class="main_Input" type="text" id="sp_name" name="sp_name" value="" readonly="readonly" style="width:170px; " />
									     <input type="button" id="btn_shop" name="btn_shop" value="" class="btn_select"/>
									     <input type="hidden" name="vm_shop_code" id="vm_shop_code" value=""/>
							  		</td>
								  </tr>
								  <tr>
									  <td align="right">发卡人员：</td>
									  <td>
									  	<input class="main_Input" type="text" id="vm_manager" name="vm_manager" value="" readonly="readonly" style="width:170px; " />
									     <input type="button" id="btn_emp" name="btn_emp" value="" class="btn_select"/>
									     <input type="hidden" name="vm_manager_code" id="vm_manager_code" value=""/>
							  		</td>
								  </tr>
								  </tr>
								  <tr>
										<td align="right">会员卡号：</td>
										<td>
											<input type="text" id="vm_cardcode" name="vm_cardcode" class="main_Input" value=""/>
										</td>
									</tr>
									<tr>
										<td align="right">会员姓名：</td>
										<td>
											<input type="text" id="vm_name" name="vm_name" class="main_Input" value=""/>
										</td>
									</tr>
									<tr>
										<td align="right">手机号码：</td>
										<td>
											<input type="text" id="vm_mobile" name="vm_mobile" class="main_Input" value=""/>
										</td>
									</tr>
									<tr>
										<td align="right">会员标签：</td>
										<td>
											<input type="text" id="vm_tag_name" name="vm_tag_name" class="main_Input" value=""/>
										</td>
									</tr>
									<tr>
										<td align="right">总计金额：</td>
										<td>
											<input class="main_Input" type="text" onkeyup="value=value.replace(/[^\d.]/g,'')" id="begin_total_money" name="begin_total_money" style="width:83px"/> 至
								  	 		<input class="main_Input" type="text" onkeyup="value=value.replace(/[^\d.]/g,'')" id="end_total_money" name="end_total_money" style="width:83px" />
										</td>
									</tr>
									<tr>
										<td align="right">剩余积分：</td>
										<td>
											<input class="main_Input" type="text" onkeyup="value=value.replace(/[^\d.]/g,'')" id="begin_last_points" name="begin_last_points" style="width:83px"/> 至
								  	 		<input class="main_Input" type="text" onkeyup="value=value.replace(/[^\d.]/g,'')" id="end_last_points" name="end_last_points" style="width:83px" />
										</td>
									</tr>
									<tr>
										<td align="right">N天未消费：</td>
										<td>
											<input class="main_Input" type="text" onkeyup="value=value.replace(/[^\d]/g,'')" id="begin_notbuy_day" name="begin_notbuy_day" style="width:83px"/> 至
								  	 		<input class="main_Input" type="text" onkeyup="value=value.replace(/[^\d]/g,'')" id="end_notbuy_day" name="end_notbuy_day" style="width:83px" />
										</td>
									</tr>
							</table>
						</li>
						<li style="float:right;">
							<a class="ui-btn ui-btn-sp mrb" id="btn_ok">确认</a>
		       				<a class="ui-btn mrb" id="btn_reset">重置</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	    <div class="fl-m">
	    	<a class="ui-btn mrb" id="btn-search" name="btn-search">查询</a>
	    	<a href="javascript:void(0);" class="ui-btn ui-btn-sp mrb" id="btn-add" >新增</a>
	    	<a href="javascript:void(0);" class="ui-btn ui-btn-sp mrb" id="btn-report" >智能分析</a>
	    	<a href="javascript:void(0);" class="ui-btn ui-btn-sp mrb" id="btn-import" >导入</a>
		</div>
	    <!-- <div class="fr">
	    	<a href="javascript:void(0);" class="ui-btn ui-btn-sp mrb" id="btn-add" >新增</a>
	    </div>  -->
	 </div>
	<div class="grid-wrap">
	    <table id="grid">
	    </table>
    	<div id="page"></div>
	</div>
</div>
</form>
<script src="<%=basePath%>data/vip/member/member_list.js"></script>
<script type="text/javascript">
$(function(){
	$(".left-tree ul").first().find("li").css("display","block");
	$(".left-tree ul h3").click(function(){
		$(this).parent().find("li").toggle();
	});
	$(".left-tree ul li span").click(function(){
		$(this).parent().find("dl").toggle();
	});
});
</script>
</body>
</html>