<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery-1.11.1.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/comm-search.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
</script> 
<style>
  .vipInfo{float:left;height:35px;line-height:35px;width:100%;}
  .vipInfo h3{float:left;width:80px;font-size:14px;color:#888;}
  .vipInfo .vip_name{font-size:14px;color:#f60;font-weight:700;}
  .tag_chose,.tag_choose,.tagInfo{float:left;width:100%;}
  .tag_chose h3,.tag_choose h3,.tagInfo h3{float:left;width:100%;font-size:14px;color:#888;height:35px;line-height:35px;}
  .tag_chose dl{float:left;width:100%;font-family:Microsoft YaHei;}
  .tag_chose dl span{float:left;position:relative;border:#f60 solid 1px;color:#f60;padding:5px 10px;margin:10px 10px 0 0;}
  .tag_choose dl{float:left;width:98%;background:#eee;padding:5px 1% 10px;font-family:Microsoft YaHei;}
  .tag_choose dl span{float:left;position:relative;border:#ddd solid 1px;background:#fff;color:#666;padding:5px 10px;margin:10px 10px 0 0;}
  .tag_chose dl span em,.tag_choose dl span em{position:absolute;height:15px;width:15px;font-size:11px;top:-8px;right:-8px;border-radius:50%;background:#f60;color:#fff;text-align:center;cursor:pointer;}
  .tagInfo ul{float:left;width:100%;border:#ddd solid 1px;height:120px;}
  .tagInfo ul li{display:inline;float:left;height:25px;line-height:25px;padding:0 1%;width:98%;}
  .addNewTag{float:left;width:100%;text-align:right;margin-top:10px;}
</style>
</head>
<body>
<input type="hidden" id="userType" value="1">
<input type="hidden" id="vm_code" value="${vm_code }">
<input type="hidden" id="vm_name" value="${vm_name }">
<input type="hidden" id="vm_cardcode" value="${vm_cardcode }">
<form name="form1" method="post" action="" id="form1" style="width: 100%-4px">
	<div class="mainwra">
		<div class="vipInfo">
			<h3>会员信息：</h3>
			<span class="vip_name" id="mi_name">${vm_name }</span>
			(卡号：<span>${vm_cardcode }</span>)
		</div>
		<div class="tag_chose">
			<h3>已选标签：</h3>
			<dl id="tagSelectedInfo"></dl>
		</div>
		<div class="tag_choose">
			<h3>可选标签：</h3>
			<dl id="tagCanSelectInfo">
			</dl>
		</div>
		<div class="tagInfo">
			<h3>标签详情：</h3>
			<ul class="tagDetailInfo" id="tagDetailInfo"></ul>
		</div>
	</div>	
</form>
<script src="<%=basePath%>data/vip/member/set_member_tag.js"></script>
</body>
</html>