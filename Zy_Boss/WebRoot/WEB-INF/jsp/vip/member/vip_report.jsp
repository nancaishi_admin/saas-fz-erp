<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
.zhinengtuijian{float:left;border: 1px solid #ddd;width:100%;padding-bottom:10px;}
.zhinengtuijian dl{float:left;width:98%;margin:10px 1% 0;}
.zhinengtuijian dl img{float:left;width:100px;height:100px;padding:5px;border:#ddd solid 1px;}
.zhinengtuijian dl span{float:left;width:200px;line-height:25px;margin-left:10px;}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=false&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/comm-search.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/chart/highcharts.5.0.12.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.imagePreview.js\"></sc"+"ript>");
</script>
<script>
	//图片放大
	$(function(){
		$("#vip_photo").preview();
	});
</script>
</head>
<body>
<input type="hidden" id="vm_id" name="vm_id" value="${member.vm_id}" />
<input type="hidden" id="vm_code" name="vm_code" value="${member.vm_code}" />
<div class="vipimg">
	<c:choose>
		<c:when test="${null != member.vm_img_path and member.vm_img_path != ''}">
			<img id="vip_photo" src="<%=basePath%>${member.vm_img_path}" height="84" width="84" alt="" />
		</c:when>
		<c:otherwise>
			<img id="vip_photo" src="<%=basePath%>resources/grid/images/nophoto.png" height="84" width="84" alt="" />
		</c:otherwise>
	</c:choose>
</div>
<div class="vip_report">
	<dl><i class="num"></i><span>卡号：</span><span>${member.vm_cardcode}</span></dl>
	<dl><i class="name"></i><span>姓名：</span><span>${member.vm_name}</span></dl>
	<dl><i class="phone"></i><span>手机号：</span><span>${member.vm_mobile}</span></dl>
	<dl><i class="birth"></i>
		<span>生日：</span>
		<span>${member.vm_birthday}</span>
	</dl>
	<dl><i class="wechat"></i><span>办卡天数：</span><span>${member.vm_date}</span></dl>
	<dl><i class="color"></i><span>消费总额：</span><span id="vm_total_money">${member.vm_total_money}</span>元</dl>
	<dl><i class="color"></i><span>消费次数：</span><span id="vm_times">${member.vm_times}</span>次</dl>
	<dl><i class="color"></i><span>最后购买：</span><span>${member.vm_lastbuy_date}</span></dl>
	<dl><i class="state"></i><span>最后消费：</span><span>${member.vm_lastbuy_money}</span>元</dl>
	<dl><i class="color"></i><span>平均消费：</span><span id="vm_avg_price"></span>元</dl>
	<dl><i class="phone"></i><span>身高：</span><span>${memberinfo.vmi_height}</span></dl>
	<dl><i class="phone"></i><span>体重：</span><span>${memberinfo.vmi_weight}</span></dl>
	<dl><i class="phone"></i><span>常穿品牌：</span><span>${memberinfo.vmi_like_brand}</span></dl>
	<dl style="width:50%;"><i class="phone"></i><span>性格特点：</span><span>${memberinfo.vmi_character}</span></dl>
</div>
<div class="tabs">
    <span id="tab_type" style="float:left; padding-right:10px;">
      	<input type="hidden" id="type" name="type" value="sell"/>
	    <a id="btn_sell" name="btn_sell" class="t-btn btn-blc on"><i></i>消费记录</a>
      	<a id="btn_time" name="btn_time" class="t-btn btn-blc on"><i></i>消费时段</a>
	    <a id="btn_try" name="btn_try" class="t-btn btn-bc"><i></i>试穿记录</a>
	    <a id="btn_push" name="btn_push" class="t-btn btn-bc"><i></i>智能推荐</a>
	    <a id="btn_ecoupon" name="btn_ecoupon" class="t-btn btn-bc"><i></i>优惠券信息</a>
	    <a id="btn_back" name="btn_back" class="t-btn btn-bc"><i></i>回访记录</a>
    </span>
    <span style="float:right; padding-right:10px;">
    	<a id="btn_to_back" name="btn_to_back" class="t-btn btn-bc" style="float:right" onclick="handle.consumeVisit('${member.vm_id}','${member.vm_cardcode}')"><i></i>回访</a>
    </span>
</div>
<div id="tabbox">
    <ul class="tab_conbox">
        <li id="tab_sell" class="tab_con">
			<span style="float:left;width:580px;">
	    		 <strong>时间:</strong>
	    		 <input type="text" class="main_Input Wdate-select" readonly="readonly" id="sell_begindate" name="sell_begindate" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'sell_enddate\',{d:0})}'})"  value="" />
                 - <input type="text" class="main_Input Wdate-select" readonly="readonly" id="sell_enddate" name="sell_enddate" onclick="WdatePicker({minDate:'#F{$dp.$D(\'sell_begindate\',{d:0})}'})"   value="" />
	  	 		 <a class="ui-btn" id="query_sell" name="query_sell">查询</a>
	  	 		 <span id="tab_sell_type" style="float:right; padding-right:10px;">
		        	<input type="hidden" id="type" name="type" value="chart"/>
				    <a id="btn_chart" name="btn_chart" class="t-btn btn-blc on"><i></i>图形</a>
				    <a id="btn_list" name="btn_list" class="t-btn btn-bc"><i></i>列表</a>
	             </span>
			</span>
	        <div class="grid-wrap sell_wrap" style="display:none;">
			    <table id="grid_sell">
			    </table>
			    <div id="page_sell"></div>
	        </div>
	        <div id="sell_container" style="border:#ccc solid 1px; height:300px;background:#fff;"></div>
        </li>
        <li id="tab_time" class="tab_con">
        	<span style="float:left;width:580px;">
	    		 <strong>时间:</strong>
	    		 <input type="text" class="main_Input Wdate-select" readonly="readonly" id="time_begindate" name="time_begindate" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'time_enddate\',{d:0})}'})"  value="" />
                 - <input type="text" class="main_Input Wdate-select" readonly="readonly" id="time_enddate" name="time_enddate" onclick="WdatePicker({minDate:'#F{$dp.$D(\'time_begindate\',{d:0})}'})"   value="" />
	  	 		 <a class="ui-btn" id="query_time" name="query_time">查询</a>
			</span>
    		<div id="container" style="border:#ccc solid 1px;height:280px;background:#fff;"></div>
    	</li>
        <li id="tab_try" class="tab_con">
			<span style="float:left;width:580px;">
	    		 <strong>时间:</strong>
	    		 <input type="text" class="main_Input Wdate-select" readonly="readonly" id="try_begindate" name="try_begindate" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'try_enddate\',{d:0})}'})"  value="" />
                 - <input type="text" class="main_Input Wdate-select" readonly="readonly" id="try_enddate" name="try_enddate" onclick="WdatePicker({minDate:'#F{$dp.$D(\'try_begindate\',{d:0})}'})"   value="" />
	  	 		 <a class="ui-btn" id="query_try" name="query_try">查询</a>
	  	 		 <span style="color:green;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;提示:只查询时间50条数据</span>
			</span>
	        <div class="grid-wrap">
				    <table id="grid_try">
				    </table>
				    <div id="page_try"></div>
		      </div>
        </li>
        <li id="tab_push" class="tab_con">
        	<div id="push_div" class="zhinengtuijian" style="overflow: auto;height: 375px" >
	        
	        </div>
	        <!-- <div class="grid-wrap">
				    <table id="grid_push">
				    </table>
				    <div id="page_push"></div>
		      </div> -->
        </li>
        <li id="tab_ecoupon" class="tab_con">
			<span style="float:left;width:580px;">
	    		 <strong>状态:</strong>
	    		 <input type="radio" checked value="" onclick="javascript:$('#ecoupon_month').val('');" class="one_month" name="ecoupon_date"/>全部&nbsp;
               	 <input type="radio" value="1" class="three_month" onclick="javascript:$('#ecoupon_month').val(1);" name="ecoupon_date"/>已用&nbsp;
               	 <input type="radio" value="0" class="six_month" onclick="javascript:$('#ecoupon_month').val(0);" name="ecoupon_date"/>未用&nbsp;
               	 <input type="hidden" name="ecu_state" id="ecu_state" value=""/>
	  	 		 <a class="ui-btn" id="query_ecoupon" name="query_ecoupon">查询</a>
			</span>
	        <div class="grid-wrap">
				    <table id="grid_ecoupon">
				    </table>
				    <div id="page_ecoupon"></div>
		      </div>
        </li>
        <li id="tab_back" class="tab_con">
			<span style="float:left;width:580px;">
	    		 <strong>时间:</strong>
	    		 <input type="text" class="main_Input Wdate-select" readonly="readonly" id="back_begindate" name="back_begindate" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'back_enddate\',{d:0})}'})"  value="" />
                 - <input type="text" class="main_Input Wdate-select" readonly="readonly" id="back_enddate" name="back_enddate" onclick="WdatePicker({minDate:'#F{$dp.$D(\'back_begindate\',{d:0})}'})"   value="" />
	  	 		 <a class="ui-btn" id="query_back" name="query_back">查询</a>
			</span>
	        <div class="grid-wrap">
				    <table id="grid_back">
				    </table>
				    <div id="page_back"></div>
		      </div>
        </li>
    </ul>
</div> 
</body>
<script src="<%=basePath%>data/vip/member/vip_report.js"></script>
</html>