<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/css/base.css" rel="stylesheet" type="text/css"/>
<style>
	body{
		overflow:hidden;
		margin:0;
		padding:0;
	}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery-1.11.1.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/validator.js\"></sc"+"ript>");
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<table class="pad_t20" width="100%" height="90%" border="0" >
    <tr>
        <td style="height:6px">
        	<input type="hidden" id="vm_code" name="vm_code" value="${vm_code }"/>
        </td>
    </tr>
	<tr>
		<td align="right" width="25%"><font color="red"></font>标签：</td>
		<td width="70%" align="left">
       		<input class="main_Input" type="text"  rows="8" cols="20" name="vt_name" id="vt_name" maxlength="30"></input>
       </td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<input type="button" value="确认" class="ui-btn ui-btn-sp mrb" onclick="javascript:return save();"/>
			<input type="button" value="关闭" class="ui-btn mrb" onclick="javascript:doClose();"/>
		</td>
	</tr>
</table>
</form>
<script>
var api = frameElement.api, W = api.opener;
var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
function save(){
	var vm_code=$("#vm_code").val();
    var vt_name=$("#vt_name").val();
    if($.trim(vt_name) ==""){
    	W.Public.tips({type: 2, content : "标签名不能为空。请重新输入!"});
	   	$("#vt_name").focus();
       	return
    }
    vt_name = W.Public.encodeURI(vt_name);
	$.ajax({
		type:"POST",
		url:config.BASEPATH+"vip/member/save_vip_tag",
		data:{vm_code:vm_code,vt_name:vt_name},
		cache:false,
		dataType:"json",
		success:function(data){
			if(undefined != data && data.stat == 200){
				W.Public.tips({type: 3, content : "保存成功!"});
				doClose();
			}else {
				W.Public.tips({type: 1, content : data.message});
			}
		}
	}); 
}
function doClose(){
	api.close();
}
</script>
</body>
</html>