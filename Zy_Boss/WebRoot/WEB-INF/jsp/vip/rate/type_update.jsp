<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
</script> 
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" id="tr_id" name="tr_id" value="${typeRate.tr_id}"/>
<div class="shop_box">
	<table class="pad_t20" width="100%" height="90%">
		<tr>
			<td align="right" width="28%">会员类别：</td>
			<td align="left">
				<input class="main_Input"  type="text" name="mt_name" id="mt_name" value="${typeRate.mt_name}" />
			</td>
		</tr>
		<tr>
			<td align="right" width="28%">商品品牌：</td>
			<td align="left">
				<input class="main_Input"  type="text" name="tp_name" id="tp_name" value="${typeRate.tp_name}"/>
			</td>
		</tr>
		<tr>
			<td align="right" width="28%">折扣率：</td>
			<td align="left">
				<input class="main_Input"  type="text" name="tr_rate" id="tr_rate" value="${typeRate.tr_rate}" onblur="javascript:checkDiscount();"/>
			</td>
		</tr>
	</table>
</div>
</form>
<div class="footdiv">
	<input class="ui-btn ui-btn-sp mrb" id="btn-save" name="btn-save" type="button" value="确定"/>
	<input class="ui-btn mrb" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
<script src="<%=basePath%>data/vip/rate/rate_type_update.js"></script>
</body>
</html>