<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<style>
.main_Input{
	width: 195px;
}
.notpassbarcode {
	background: url(<%=basePath%>resources/grid/images/poptxt1.png);
	width: 198px;
	height: 143px;
	position: absolute;
	font-size: 12px;
}
.notcheckbody {
	float: left;
	width: 155px;
	margin: 15px 0 0 10px;
	height: 110px;
	overflow-x: hidden;
	overflow-y: auto;
	SCROLLBAR-HIGHLIGHT-COLOR: #fee7cf;
	SCROLLBAR-ARROW-COLOR: #fee7cf;
}
.notcheckbody p {
	float: left;
	width: 155px;
	height: 16px;
	overflow: hidden;
	padding-left: 5px;
}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.extend.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.messages_zh.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc" + "ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.size.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/validator.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
</script>
<script>
//控制控件只能输入正整数 
function onlyPositiveNumber(e){ 
	var key = window.event ? e.keyCode : e.which;  
    var keychar = String.fromCharCode(key);   
    var result = (Validator.Number).test(keychar);   
    if(!result){      
	     return false;  
	 }else{     
	     return true;
	 } 
}
function valNumber(obj){
	var num = obj.value; 
	var k=window.event.keyCode;
	if(k == 109 || k == 189){
    	obj.value=obj.value.replace("-","");
    	return;
    }
	if(num.length > 0){
		if(num.match(Validator.Number) == null){
			obj.value="";
			Public.tips({type: 2, content : '请正确输入数量!'});
			$("#barcode_amount").focus()
			return false;
		}
	}
}
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" id="ba_id" name="ba_id" value="${batch.ba_id }"/>
<input type="hidden" id="ba_number" name="ba_number" value="${batch.ba_number }"/>
<input type="hidden" id="ba_scope" name="ba_scope" value="${batch.ba_scope }"/>
<input type="hidden" id="ba_scope_code" name="ba_scope_code" value="${batch.ba_scope_code }"/>
<input type="hidden" id="ck_dp_code" name="ck_dp_code" value="${batch.ba_dp_code }"/>
<input type="hidden" id="ckl_isrepair" name="ckl_isrepair" value="1"/>
<div class="border">
	<table width="100%" cellspacing="0" cellpadding="0">
	    <tr>
	      <td class="top-b border-b">
	        <input id="btn-submit-repair" class="t-btn btn-red" type="button" value="提交补盘" onclick="javascript:handle.submitRepair();"/>
	        <a id="btn-import" class="t-btn btn-black" title="">盘点机导入</a>
	        <input id="btn_close" class="t-btn" type="button" value="返回"/>
	        <span id="errorTip" class="errorTip"></span>
	      </td>
	    </tr>
    </table>
</div>
<div class="grid-wrap">
    <div id="list-grid" style="display:'';width: 100%;">
        <table id="grid"></table>
        <div id="page"></div>
    </div>
    <div id="size-grid" style="display:none; width: 100%;">
        <table id="sizeGrid"></table>
        <div id="sizePage"></div>
    </div>
    <div id="sum-grid" style="display:none;width: 100%;">
       <table id="sumGrid"></table>
       <div id="sumPage"></div>
   </div>
</div>
<div>
	<table cellpadding="0" cellspacing="0"  class="top-b border-t" width="100%">
		<tr>
			<td width="220">
				货号查询：<input name="pd_no" type="text" id="pd_no" class="ui-input ui-input-ph w120" onkeyup="javascript:if (event.keyCode==13){Choose.selectProduct();};"/>
               <input type="button" value=" " class="btn_select" onclick="javascript:Choose.selectProduct();"/>
			</td>
			<td width="280">
				扫描条码：<input name="text" type="text" id="barcode" onkeyup="javascript:if (event.keyCode==13){Choose.barCode()};" class="ui-input ui-input-ph w146"/>
				<input id="barcode_amount" class="ui-input ui-input-ph" value="1" style="width:20px" maxlength="4"
						onkeypress="return onlyPositiveNumber(event)"
						onkeyup="javascript:valNumber(this);"/>
			</td>
			<td >
				<input type="hidden" id="CurrentMode" value="0"/>
				<a id="sumBtn" name="mode-btn" class="t-btn btn-bl"  onclick="showMode.display(2);"><i></i>汇总</a>
			    <a id="listBtn" name="mode-btn" class="t-btn btn-blc on"  onclick="showMode.display(0);"><i></i>列表</a>
			    <a id="sizeBtn" name="mode-btn" class="t-btn btn-bc" onclick="showMode.display(1);"><i></i>尺码</a>
			    <a class="t-btn btn-red" id="btnClear">清除</a>
			</td>
		</tr>
	</table>
	<table width="100%">
		<tr class="list first">
			<td align="right" width="60px">盘点批次：</td>
			<td width="200px">
				<input class="main_Input" type="text" value="${batch.ba_number}" readonly="readonly" />
			</td>
			<td align="right" width="60px">盘点仓库：</td>
			<td width="200px">
				<input class="main_Input" type="text" readonly="readonly" value="${batch.depot_name }"/>
			</td>
				<c:choose>
       				<c:when test="${batch.ba_scope eq 0}">
       					<td align="right" width="60px">全场盘点：</td>
       					<td width="200px"><input class="main_Input" type="text" readonly="readonly" value="全场"/></td>
       				</c:when>
       				<c:when test="${batch.ba_scope eq 1}">
       					<td align="right" width="60px">类别盘点：</td>
						<td width="200px"><input class="main_Input" type="text" readonly="readonly" value="${batch.ba_scope_name}"/></td>
       				</c:when>
       				<c:when test="${batch.ba_scope eq 2}">
       					<td align="right" width="60px">品牌盘点：</td>
						<td width="200px"><input class="main_Input" type="text" readonly="readonly" value="${batch.ba_scope_name}"/></td>
       				</c:when>
       				<c:when test="${batch.ba_scope eq 3}">
       					<td align="right" width="60px">单品盘点：</td>
						<td width="200px"><input class="main_Input" type="text" readonly="readonly" value="${batch.ba_scope_name}"/></td>
       				</c:when>
       				<c:when test="${batch.ba_scope eq 4}">
       					<td align="right" width="60px">年份盘点：</td>
						<td width="200px"><input class="main_Input" type="text" readonly="readonly" value="${batch.ba_scope_name}"/></td>
       				</c:when>
       				<c:when test="${batch.ba_scope eq 5}">
       					<td align="right" width="60px">季节盘点：</td>
						<td width="200px"><input class="main_Input" type="text" readonly="readonly" value="${batch.ba_scope_name}"/></td>
       				</c:when>
       				<c:otherwise>
       				</c:otherwise>
       			</c:choose>
			<td align="right" width="60px">发起时间：</td>
			<td>
				<input readonly type="text" class="main_Input Wdate" value="${batch.ba_date}" style="width:197px;"/>
			</td>
		</tr>
		<tr class="list">
			<td align="right">盘点备注：</td>
			<td>
				<input class="main_Input" type="text" value="${batch.ba_remark}" readonly="readonly"/>
			</td>
			<td align="right">库存数量：</td>
			<td>
				<input class="main_Input" type="text" name="ck_stockamount" id="ck_stockamount" value="" readonly="readonly"/>
			</td>
			<td align="right">盘点数量：</td>
			<td>
				<input class="main_Input" type="text" name="ck_amount" id="ck_amount" value="" readonly="readonly"/>
			</td>
			<td align="right">差异数量：</td>
			<td>
				<input class="main_Input" type="text" id="sub_amount" value="" readonly="readonly"/>
			</td>
		</tr>
		<tr class="list last">
			<td align="right">操作方式：</td>
			<td id="repairType">
				<label class="radio radiow7">
			        <input type="radio" name="repairType" checked="checked" value="1"/>
			        <b>累加</b>
			    </label>
			    <label class="radio radiow7">
			        <input type="radio" name="repairType" value="2"/>
			        <b>覆盖</b>
			    </label>
				<input type="hidden" name="repair_type" id="repair_type" value="1"/>
			</td>
			<td align="right">总计金额：</td>
			<td>
				<input class="main_Input" type="text" id="ck_money" value="" readonly="readonly"/>
			</td>
			<td align="right">备注：</td>
			<td colspan="3">
				<input class="main_Input" type="text" name="ck_remark" id="ck_remark" value=""/>
			</td>
		</tr>
	</table>
</div>
<input type="hidden" id="CheckNotPassTextShow" value=""/>
<div id="BarCodeCheckNotPass" class="notpassbarcode" title="双击关闭错误提示" style="display:none;" ondblclick="this.style.display='none'">
 	<span class="notcheckbody" id="CheckNotPassText"></span>
</div>
</form>
<script src="<%=basePath%>data/stock/check/check_check_add.js"></script>
</body>
</html>