<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<style>
.main_Input{
	width: 195px;
}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/validator.js\"></sc"+"ript>");
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" id="ba_id" name="ba_id" value="${batch.ba_id }"/>
<input type="hidden" id="ba_number" name="ba_number" value="${batch.ba_number }"/>
<input type="hidden" id="ba_scope" name="ba_scope" value="${batch.ba_scope }"/>
<input type="hidden" id="ba_scope_code" name="ba_scope_code" value="${batch.ba_scope_code }"/>
<input type="hidden" id="ba_dp_code" name="ba_dp_code" value="${batch.ba_dp_code }"/>
<div class="border">
	<table width="100%" cellspacing="0" cellpadding="0">
	    <tr>
	      <td class="top-b border-b">
	        <input id="btn-exec-all" class="t-btn btn-red" type="button" value="全部处理" disabled="disabled"/>
	        <input id="btn-exec-part" class="t-btn btn-green" type="button" value="部分处理" disabled="disabled"/>
	        <input id="btn-missing" class="t-btn btn-cblue" type="button" value="漏盘数据"/>
	        <input id="btn_close" class="t-btn" type="button" value="返回"/>
	        <span id="errorTip" class="errorTip"></span>
	      </td>
	    </tr>
    </table>
</div>
<div class="grid-wrap">
    <div id="list-grid" style="display:'';width: 100%;">
        <table id="grid"></table>
        <div id="page"></div>
    </div>
</div>
<div>
	<table width="100%">
		<tr class="list first">
			<td align="right" width="60px">盘点批次：</td>
			<td width="200px">
				<input class="main_Input" type="text" value="${batch.ba_number}" readonly="readonly" />
			</td>
			<td align="right" width="60px">盘点仓库：</td>
			<td width="200px">
				<input class="main_Input" type="text" readonly="readonly" value="${batch.depot_name }"/>
			</td>
				<c:choose>
       				<c:when test="${batch.ba_scope eq 0}">
       					<td align="right" width="60px">全场盘点：</td>
       					<td width="200px"><input class="main_Input" type="text" readonly="readonly" value="全场"/></td>
       				</c:when>
       				<c:when test="${batch.ba_scope eq 1}">
       					<td align="right" width="60px">类别盘点：</td>
						<td width="200px"><input class="main_Input" type="text" readonly="readonly" value="${batch.ba_scope_name}"/></td>
       				</c:when>
       				<c:when test="${batch.ba_scope eq 2}">
       					<td align="right" width="60px">品牌盘点：</td>
						<td width="200px"><input class="main_Input" type="text" readonly="readonly" value="${batch.ba_scope_name}"/></td>
       				</c:when>
       				<c:when test="${batch.ba_scope eq 3}">
       					<td align="right" width="60px">单品盘点：</td>
						<td width="200px"><input class="main_Input" type="text" readonly="readonly" value="${batch.ba_scope_name}"/></td>
       				</c:when>
       				<c:when test="${batch.ba_scope eq 4}">
       					<td align="right" width="60px">年份盘点：</td>
						<td width="200px"><input class="main_Input" type="text" readonly="readonly" value="${batch.ba_scope_name}"/></td>
       				</c:when>
       				<c:when test="${batch.ba_scope eq 5}">
       					<td align="right" width="60px">季节盘点：</td>
						<td width="200px"><input class="main_Input" type="text" readonly="readonly" value="${batch.ba_scope_name}"/></td>
       				</c:when>
       				<c:otherwise>
       				</c:otherwise>
       			</c:choose>
			<td align="right" width="60px">发起时间：</td>
			<td>
				<input readonly type="text" class="main_Input Wdate" value="${batch.ba_date}" style="width:197px;"/>
			</td>
		</tr>
		<tr class="list last">
			<td align="right">盘点备注：</td>
			<td>
				<input class="main_Input" type="text" value="${batch.ba_remark}" readonly="readonly"/>
			</td>
			<td align="right">库存数量：</td>
			<td>
				<input class="main_Input" type="text" name="ba_stockamount" id="ba_stockamount" value="" readonly="readonly"/>
			</td>
			<td align="right">盘点数量：</td>
			<td>
				<input class="main_Input" type="text" name="ba_amount" id="ba_amount" value="" readonly="readonly"/>
			</td>
		</tr>
	</table>
</div>
</form>
<script src="<%=basePath%>data/stock/check/check_check_exec.js"></script>
</body>
</html>