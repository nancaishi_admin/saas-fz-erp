<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery-1.11.1.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/comm-search.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
</script> 
</head>
<body id="main_content">
<form name="form1" method="post" action="" id="form1">
<input type="hidden" name="flushFlag" id="flushFlag" value=""/>
<input type="hidden" id="fromJsp" value="${param.fromJsp}"/>
<div class="wrapper">
	<div class="mod-search cf">
	   <div class="fl">
	    	<div id="filter-menu" class="ui-btn-menu fl">
	    		 <span style="float:left" class="ui-btn menu-btn">
		     		 <strong>查询条件</strong>
		     			 <input type="text" class="main_Input Wdate-select" readonly="readonly" id="begindate" name="begindate" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'enddate\',{d:0})}'})"  value="" />
							-
	                     <input type="text" class="main_Input Wdate-select" readonly="readonly" id="enddate" name="enddate" onclick="WdatePicker({minDate:'#F{$dp.$D(\'begindate\',{d:0})}'})"   value="" />
					 <b></b>
			  	 </span>
		 		<div class="con" >
					<ul class="ul-inline">
						<li>
							<table class="searchbar">
								<tr>     
									<td align="right">日期选择：</td>
									<td id="date">
			                            <label class="radio" id="date_default" style="width:30px">
							  	 			<input name="redio1" type="radio" id="theDate" value="1" checked="checked"/>今天
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="yesterday" value="2" />昨天
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theWeek" value="3" />本周
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theMonth" value="4" />本月
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theSeason" value="5" />本季
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theYear" value="6" />本年
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="lastYear" value="7" />去年
								  	 	</label>
							  	 	</td>
								 </tr>
								  <tr>
									  <td align="right">盘点仓库：</td>
										<td>
										  	<input class="main_Input" type="text" id="depot_name" name="depot_name" value="" readonly="readonly" style="width:170px; " />
										     <input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryDepot();"/>
										     <input type="hidden" name="ba_dp_code" id="ba_dp_code" value=""/>
								  		</td>
								  </tr>
									<tr>
										<td align="right">盘点批号：</td>
										<td>
											<input type="text" id="ba_number" name="ba_number" class="main_Input" value="${param.ba_number}"/>
										</td>
									</tr>
									<tr>
										<td align="right">商品货号：</td>
										<td>
											<input type="text" id="pd_no" name="pd_no" class="main_Input" value=""/>
										</td>
									</tr>
									<tr>
										<td align="right">商品名称：</td>
										<td>
											<input type="text" id="pd_name" name="pd_name" class="main_Input" value=""/>
										</td>
									</tr>
									<tr>
										<td align="right">商品类别：</td>
										<td>
										  	<input class="main_Input" type="text" readonly="readonly" name="tp_name" id="tp_name" value="" style="width:170px;"/>
										  	<input type="hidden" name="pd_tp_code" id="pd_tp_code" value=""/>
											<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryType();"/>
								  		</td>
									</tr>
									<tr>
										<td align="right">商品品牌：</td>
										<td>
										  	<input class="main_Input" type="text" readonly="readonly" name="bd_name" id="bd_name" value="" style="width:170px;"/>
										  	<input type="hidden" name="pd_bd_code" id="pd_bd_code" value=""/>
											<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryBrand();"/>
								  		</td>
									</tr>
							</table>
						</li>
						<li style="float:right;">
							<a class="ui-btn ui-btn-sp mrb" id="btn_ok">确认</a>
		       				<a class="ui-btn mrb" id="btn_reset">重置</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	    <div class="fl-m">
	    	<span id="loss" style="width:50px;float:left;">
				<label class="chk" style="margin-top:6px;">
					<input type="checkbox" checked="checked"/>损
				</label>
			</span>
			<span id="overflow" style="width:50px;float:left;">
				<label class="chk" style="margin-top:6px;">
		  			<input type="checkbox" checked="checked"/>溢
		  		</label>
		  	</span>
		  	<span id="flat" style="width:50px;float:left;">
		  		<label class="chk" style="margin-top:6px;">
					<input type="checkbox" checked="checked"/>平
				</label>
			</span>
			<a class="ui-btn mrb" id="btn-search" name="btn-search">查询</a>
			<span style="color:red;">注：红色代表库存报损，蓝色代表库存报溢</span>
		</div>
	    <div class="fr">
    		<input type="hidden" id="CurrentMode" value="0"/>
		    <a id="listBtn" name="mode-btn" class="t-btn btn-blc on"  onclick="showMode.display(0);"><i></i>列表</a>
			<a id="sumBtn" name="mode-btn" class="t-btn btn-bl"  onclick="showMode.display(2);"><i></i>汇总</a>
			<input id="btn_close" class="t-btn iconfont" type="button" style="display:none;" onclick="javascript:api.close();" value="返回"/>
	    </div> 
	 </div>
	 
	 <div class="grid-wrap">
	    <div id="list-grid" style="display:'';width: 100%;">
	        <table id="grid"></table>
	        <div id="page"></div>
	    </div>
	    <div id="sum-grid" style="display:none;width: 100%;">
	       <table id="sumGrid"></table>
	       <div id="sumPage"></div>
	   </div>
	</div>
	 
</div>
</form>
<script src="<%=basePath%>data/stock/check/check_check_diff.js"></script>
</body>
</html>