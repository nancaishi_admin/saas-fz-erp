<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.extend.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.messages_zh.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" id="ba_id" name="ba_id" value="${batch.ba_id }"/>
<input type="hidden" id="ba_number" name="ba_number" value="${batch.ba_number }"/>
<div class="border">
	<table width="100%" cellspacing="0" cellpadding="0">
	    <tr>
	      <td class="top-b border-b">
	        <input id="btn-approve" class="t-btn btn-green" type="button" value="审核" style="display:none;"/>
	        <input id="btn_close" class="t-btn iconfont" type="button" value="返回"/>
	        <span id="errorTip" class="errorTip"></span>
	      </td>
	    </tr>
    </table>
</div>
<div>
	<table width="100%">
		<tr class="list first">
			<td align="right" width="60px">盘点仓库：</td>
			<td width="220px">
				<input class="main_Input" type="text" readonly="readonly" name="depot_name" id="depot_name" value="${batch.depot_name }"/>
			</td>
			<td align="right" width="60px">经办人员：</td>
			<td>
				<input class="main_Input" type="text" readonly="readonly" name="ba_manager" id="ba_manager" value="${batch.ba_manager }"/>
			</td>
		</tr>
		<tr class="list">
			<td align="right">盘点范围：</td>
				<c:choose>
       				<c:when test="${batch.ba_scope eq 0}">
       					<td><input class="main_Input" type="text" readonly="readonly" value="全场盘点"/></td>
       				</c:when>
       				<c:when test="${batch.ba_scope eq 1}">
       					<td><input class="main_Input" type="text" readonly="readonly" value="类别盘点"/></td>
       					<td align="right">类别盘点：</td>
						<td><input class="main_Input" type="text" readonly="readonly" value="${batch.ba_scope_name}"/></td>
       				</c:when>
       				<c:when test="${batch.ba_scope eq 2}">
       					<td><input class="main_Input" type="text" readonly="readonly" value="品牌盘点"/></td>
       					<td align="right">品牌盘点：</td>
						<td><input class="main_Input" type="text" readonly="readonly" value="${batch.ba_scope_name}"/></td>
       				</c:when>
       				<c:when test="${batch.ba_scope eq 3}">
       					<td><input class="main_Input" type="text" readonly="readonly" value="单品盘点"/></td>
       					<td align="right">单品盘点：</td>
						<td><input class="main_Input" type="text" readonly="readonly" value="${batch.ba_scope_name}"/></td>
       				</c:when>
       				<c:when test="${batch.ba_scope eq 4}">
       					<td><input class="main_Input" type="text" readonly="readonly" value="年份盘点"/></td>
       					<td align="right">年份盘点：</td>
						<td><input class="main_Input" type="text" readonly="readonly" value="${batch.ba_scope_name}"/></td>
       				</c:when>
       				<c:when test="${batch.ba_scope eq 5}">
       					<td><input class="main_Input" type="text" readonly="readonly" value="季节盘点"/></td>
       					<td align="right">季节盘点：</td>
						<td><input class="main_Input" type="text" readonly="readonly" value="${batch.ba_scope_name}"/></td>
       				</c:when>
       				<c:otherwise>
       				</c:otherwise>
       			</c:choose>
		</tr>
		<tr class="list last">
			<td align="right">备注：</td>
			<td colspan="3">
				<input class="main_Input" type="text" name="ba_remark" id="ba_remark" value="${batch.ba_remark}" style="width:495px;"/>
			</td>
		</tr>
		<tr class="list last" style="display:${batch.ba_ar_state eq 2 ? '' : 'none' };">
			<td align="right">驳回原因：</td>
			<td colspan="3">
				<input class="main_Input" type="text" value="${batch.ar_describe}" style="width:495px;color:red;"/>
			</td>
		</tr>
	</table>
</div>
</form>
<script src="<%=basePath%>data/stock/check/check_batch_view.js"></script>
</body>
</html>