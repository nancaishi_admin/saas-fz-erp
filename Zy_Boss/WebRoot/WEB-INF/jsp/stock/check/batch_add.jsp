<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.extend.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.messages_zh.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<div class="border">
	<table width="100%" cellspacing="0" cellpadding="0">
	    <tr>
	      <td class="top-b border-b">
	        <input id="btn-save" class="t-btn btn-red iconfont" type="button" value="保存"/>
	        <input id="btn_close" class="t-btn iconfont" type="button" value="返回"/>
	        <span id="errorTip" class="errorTip"></span>
	      </td>
	    </tr>
    </table>
</div>
<div>
	<table width="100%">
		<tr class="list first">
			<td align="right" width="60px">盘点仓库：</td>
			<td width="220px">
				<input class="main_Input" type="text" readonly="readonly" name="depot_name" id="depot_name" value="" style="width:170px;"/>
				<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryDepot();"/>
				<input type="hidden" name="ba_dp_code" id="ba_dp_code" value="" />
			</td>
			<td align="right" width="60px">经办人员：</td>
			<td>
				<input class="main_Input" type="text" readonly="readonly" name="ba_manager" id="ba_manager" value="" style="width:170px;"/>
				<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryEmp();"/>
			</td>
		</tr>
		<tr class="list">
			<td align="right">盘点范围：</td>
			<td>
				<span class="ui-combo-wrap" id="span_scope"></span>
				<input type="hidden" name="ba_scope" id="ba_scope"/>
				<input type="hidden" name="ba_scope_code" id="ba_scope_code"/>
				<input type="hidden" name="ba_scope_name" id="ba_scope_name"/>
			</td>
			<td align="right" id="scopeLabel">类别盘点：</td>
			<td id="typeTd">
				<input class="main_Input" type="text" readonly="readonly" value="" style="width:170px;"/>
				<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryType(this);"/>
			</td>
			<td id="brandTd">
				<input class="main_Input" type="text" readonly="readonly" value="" style="width:170px;"/>
				<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryBrand(this);"/>
			</td>
			<td id="productTd">
				<input class="main_Input" type="text" readonly="readonly" value="" style="width:170px;"/>
				<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryProduct(this);"/>
			</td>
			<td id="seasonTd">
				<span class="ui-combo-wrap" id="span_season"></span>
			</td>
			<td id="yearTd">
				<span class="ui-combo-wrap" id="span_year"></span>
			</td>
		</tr>
		<tr class="list last">
			<td align="right">备注：</td>
			<td colspan="3">
				<input class="main_Input" type="text" name="ba_remark" id="ba_remark" value="" style="width:495px;"/>
			</td>
		</tr>
	</table>
</div>
</form>
<script>
	$().ready(function() {
		$("#form1").validate({
			ignore: "",
			onfocusout : function(element) {
				$(element).valid();
			},
			errorPlacement: function(error, element) { //错误信息位置设置方法
				element.focus();
				$("#errorTip").empty();
				error.appendTo( $("#errorTip") );
			},
			rules : {
				ba_dp_code : "required",
				ba_manager : "required",
				ba_scope : "required"
			},
			messages : {
				ba_dp_code : "请选择盘点仓库",
				ba_manager : "请选择经办人",
				ba_scope : "请选择盘点范围"
			}
		});
	});
</script>
<script src="<%=basePath%>data/stock/check/check_batch_add.js"></script>
</body>
</html>