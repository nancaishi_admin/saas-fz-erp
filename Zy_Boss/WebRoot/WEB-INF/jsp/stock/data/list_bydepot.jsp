<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery-1.11.1.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/comm-search.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
</script> 
</head>
<body id="main_content">
<form name="form1" method="post" action="" id="form1">
<input type="hidden" name="flushFlag" id="flushFlag" value=""/>
<div class="wrapper">
	<div class="mod-search cf">
		<div class="fl">
			<div id="filter-menu" class="ui-btn-menu fl">
				<span style="float:left" class="ui-btn menu-btn"> 
					<strong>货号</strong>
					<input type="text" id="pd_no" name="pd_no" class="main_Input" value="" /> 
					<span id="exactQuery">
					<label class="chk over" style="margin-top: 6px;">
						<input name="box" type="checkbox" />精确
					</label>
					</span>
					<b></b>
				</span>
				<div class="con" style="width:340px;">
					<ul class="ul-inline">
						<li>
							<table class="searchbar">
								<tr>
									<td align="right">商品名称：</td>
									<td>
										<input type="text" id="pd_name" name="pd_name" class="main_Input" value="" />
									</td>
								</tr>
								<tr>
									<td align="right">仓库：</td>
									<td>
										<input class="main_Input" type="text" id="depot_name" name="depot_name" value="" readonly="readonly" style="width:170px; " /> 
										<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryDepot();" /> 
										<input type="hidden" name="depot_code" id="depot_code" value="" />
									</td>
								</tr>

							</table>
						</li>
						<li style="float:right;margin-top: 10px;">
							<a class="ui-btn ui-btn-sp mrb" id="btn_ok">确认</a> 
							<a class="ui-btn mrb" id="btn_reset">重置</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="fl-m">
			<a class="ui-btn mrb" id="btn-search" name="btn-search">查询</a>
		</div>
		<div class="fr">
		
		</div>
	</div>
		<div class="grid-wrap" id="list-grid">
			<table id="grid"></table>
			<div id="page"></div>
		</div>
		<div class="grid-wrap" id="size-grid" style="display:none;">
			<table id="sizeGrid"></table>
        	<div id="sizePage"></div>
		</div>
	</div>
</form>
<script src="<%=basePath%>data/stock/data/data_list_bydepot.js"></script>
</body>
</html>