<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD xhtml 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/dtd/xhtml1-Strict.dtd"/>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<link type="text/css" rel="stylesheet" href="<%=basePath%>resources/css/leadingIn/leadingInResult.css"/>
</head>
<body>
	<div style="padding:10px;" id="info">
		<c:choose>
			<c:when test="${not empty t_Import_Info}">
				状态：
				<fron class="state">导入完成！</fron>
				<br>
				时间：${t_Import_Info.ii_sysdate}
				<br>
				文件名称：${t_Import_Info.ii_filename}
				<br>
				详细信息:
				<br>
				共：${t_Import_Info.ii_total}条数据；成功导入：${t_Import_Info.ii_success}条数据，失败：${t_Import_Info.ii_fail}条数据。
				<br>
				<a class="link" href="<%=serverPath%>/excel_upload/${t_Import_Info.ii_error_filename}">下载错误数据文件</a>
			</c:when>
			<c:otherwise>
				没有导入记录
			</c:otherwise>
		</c:choose>
	</div>
</body>
</html>
