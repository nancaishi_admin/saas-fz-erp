<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<title>密码修改</title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
</script>
<script>
$(function(){
	$("#btn_save").click(function(){
		var old_pass = $.trim($("#old_pass").val());
		var new_pass = $.trim($("#new_pass").val());
		var two_pass = $.trim($("#two_pass").val());
		var reg = /^[\@A-Za-z0-9\!#$\%\^\&\*\.\~]{6,22}$/;
		if(old_pass == null || old_pass == ''){
			Public.tips({type: 2, content : "请填写当前密码!"});
			document.getElementById("old_pass").focus();
			return;
		}
	    if(new_pass == null || new_pass == ''){
	    	Public.tips({type: 2, content : "请输入新的密码！"});
	        document.getElementById("new_pass").focus();
	        return;
	    }
	    if(reg.test(new_pass)){
		    if(new_pass != two_pass){
		    	Public.tips({type: 2, content : "你两次输入的密码不一致!"});
		    	return;
			}else{
				var url = "sys/user/changePassword";
				$.ajax({
					type:"POST",
					url:'<%=basePath%>'+url,
					data:{'old_pwd':old_pass,'us_pwd':new_pass},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){
							Public.tips({type: 3, content : "修改成功！"});
							$("#old_pass").val('');
							$("#new_pass").val('');
							$("#two_pass").val('');
						}else{
							Public.tips({type: 1, content : data.message});
						}
					}
				}); 
			}
		}else{
			Public.tips({type: 2, content : "你输入的密码格式不正确!"});
			document.getElementById("new_pass").focus();
			return;
		}
	    
	    <%--   if(new_pass != two_pass){
	    	Public.tips({type: 2, content : "两次输入的密码不一致！"});
	    	return;
		} else {
		    
		}--%>
	});
});
</script>
</head>
<body>
<form name="form1" method="post" action="" id="form1">
<div class="mainwra">
	<div class="border" style="border-bottom:#ddd solid 1px;">
  		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td colspan="2"  class="top-b border-b">
					<a class="t-btn btn-red" id="btn_save" name="btn_save">保存</a>
				</td>
			</tr>
			<tr class="list ">
				<td align="right"><font color="red">*</font>当前密码：</td>
				<td align="left">
					<input class="main_Input" type="password" id="old_pass" value=""/>
				</td>
			</tr>
			<tr class="list ">
				<td align="right"><font color="red">*</font>新的密码：</td>
				<td align="left">
					<input class="main_Input" type="password" id="new_pass" value=""/>
				</td>
			</tr>
			<tr class="list last">
				<td align="right"><font color="red">*</font>确认密码：</td>
				<td align="left">
					<input class="main_Input" type="password" name="us_pwd" id="two_pass" value=""/>
				</td>
			</tr>
		</table>
	</div> 
</div>
</form>
</body>
</html>