<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/comm-search.js\"></sc"+"ript>");
</script> 
</head>
<body id="main_content">
<form name="form1" method="post" action="" id="form1">
<div class="wrapper">
<div class="mod-search cf">
	    <div class="fl">
	      <div id="filter-menu" class="ui-btn-menu fl">
	    		 <span style="float:left" class="ui-btn menu-btn">
		     			 条件：<input type="text" class="main_Input" id="SearchContent" name="SearchContent"  value="" />
			  	 </span>
		 		<div class="con" style="width:280px;">
					<ul class="ul-inline">
						<li>
							<table class="searchbar">
								 <tr>
									  <td align="right">门店：</td>
									  <td>
									  	<input class="main_Input" type="text" id="sp_name" name="sp_name" value="" readonly="readonly" style="width:170px; " />
									  	<input type="button" class="btn_select" value="" name="btn_shop" id="btn_shop"/>
									    <input type="hidden" name="sp_code" id="sp_code" value=""/>
							  		  </td>
								 </tr>
								 <tr>
									  <td align="right">角色：</td>
									  <td>
									  	<input class="main_Input" type="text" id="ro_name" name="ro_name" value="" readonly="readonly" style="width:170px; " />
									  	<input type="button" class="btn_select" value="" name="btn_role" id="btn_role"/>
									    <input type="hidden" name="ro_code" id="ro_code" value=""/>
							  		  </td>
								 </tr>
							</table>
						</li>
						<li style="float:right;margin-top:10px;">
							<a class="ui-btn ui-btn-sp mrb" id="btn_ok">确认</a>
		       				<a class="ui-btn mrb" id="btn_reset">重置</a>
						</li>
					</ul>
				</div>
			</div>
	    </div>
	    <div class="fl">
	    	<a class="ui-btn mrb" id="btn-search" name="btn-search">查询</a>
	    	<a href="javascript:void(0);" class="ui-btn ui-btn-sp" id="btn-add" >新增</a>&nbsp;&nbsp;
	    </div>
	 </div>
	<div class="grid-wrap">
	    <table id="grid">
	    </table>
    	<div id="page"></div>
	</div>
</div>
</form>
<script src="<%=basePath%>data/sys/user/user_list.js"></script>
</body>
</html>