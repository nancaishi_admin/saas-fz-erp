<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/css/default.css"" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
</script>
</head>
<body>
<form name="form1" method="post" action="" id="form1">
	<input type="hidden" id="us_id" name="us_id" value="${model.us_id}"/>
	<table class="pad_t20" width="100%" >
		<tr style="line-height:45px; ">
			<td nowrap="nowrap">
				帐号：<input class="main_Input" type="text" name="us_account" id="us_account" value="${model.us_account}" maxlength=15 title="不能超过十五个字符"/>
			</td>
			<td nowrap="nowrap">
				名称：<input class="main_Input" type="text" name="us_name" id="us_name" value="${model.us_name}" maxlength=10/>
			</td>
		</tr>
		<tr style="line-height:45px; ">
			<td  nowrap="nowrap" align="left">
				角色：<input class="main_Input" type="text" id="ro_name" name="ro_name" value="${model.ro_name}" readonly="readonly" style="width:170px; " />
			  	<input type="button" class="btn_select" value="" name="btn_role" id="btn_role"/>
			    <input type="hidden" name="us_ro_code" id="us_ro_code" value="${model.us_ro_code}"/>
			    <input type="hidden" name="ro_shop_type" id="ro_shop_type" value="${model.shoptype}"/>
			</td>
			<td  nowrap="nowrap" align="left">
				门店：<input class="main_Input" type="text" id="sp_name" name="sp_name" value="${model.shop_name}" readonly="readonly" style="width:170px; " />
			  	<input type="button" class="btn_select" value="" name="btn_shop" id="btn_shop"/>
			    <input type="hidden" name="us_shop_code" id="us_shop_code" value="${model.us_shop_code}"/>
			</td>
		</tr>
		<tr style="line-height:45px; ">
			<td align="left" colspan="2">权限：
				<input type="hidden" name="us_limit" id="us_limit" value="${model.us_limit}"/>
				<input type="checkbox" name="price_limit" value="C" />&nbsp;成本价
				<input type="checkbox" name="price_limit" value="S" />&nbsp;零售价
				<input type="checkbox" name="price_limit" value="D" />&nbsp;配送价
				<input type="checkbox" name="price_limit" value="V" />&nbsp;会员价
				<input type="checkbox" name="price_limit" value="W" />&nbsp;批发价
				<input type="checkbox" name="price_limit" value="E" />&nbsp;进货价
				<input type="checkbox" name="price_limit" value="P" />&nbsp;利润
			</td>
		</tr>
	</table>
</form>
<div class="footdiv">
	<input class="btn_confirm" type="button" id="btn-save" name="btn-save" value="确定"/>
	<input class="btn_close" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
<script src="<%=basePath%>data/sys/user/user_update.js"></script>
</body>
</html>