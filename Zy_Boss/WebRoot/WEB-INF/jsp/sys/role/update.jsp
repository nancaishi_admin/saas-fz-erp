<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/css/default.css"" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
</script>
</head>
<body>
<form name="form1" method="post" action="" id="form1">
	<input type="hidden" id="ro_id" name="ro_id" value="${model.ro_id}"/>
	<table class="pad_t20" width="100%" style="padding:10px;">
		<tr style="line-height:45px; ">
			<td align="right" >编号：</td>
			<td align="left">
			 	<input type="text" readonly="readonly" class="main_Input w146" id="ro_code" name="ro_code" value="${model.ro_code}"/>
			</td>
		</tr>
		<tr style="line-height:45px; ">
			<td align="right" >名称：</td>
			<td align="left">
			 	<input type="text" class="main_Input w146" id="ro_name" name="ro_name" value="${model.ro_name}"/>
			</td>
		</tr>
		<tr style="line-height:45px; ">
			<td align="right" >类型：</td>
			<td align="left">
		 		<input type="text" readonly="readonly" class="main_Input w146" id="ty_name" name="ty_name" value="${model.ty_name}"/>
			</td>
		</tr>
	</table>
</form>
<div class="footdiv">
	<input class="btn_confirm" type="button" id="btn-save" name="btn-save" value="确定"/>
	<input class="btn_close" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
<script src="<%=basePath%>data/sys/role/role_update.js"></script>
</body>
</html>