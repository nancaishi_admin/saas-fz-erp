<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<base target="_self">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<title>启用帐套</title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=false&skin=mac\"></sc"+"ript>");
</script>
</head>
<body id="main_content">
	<div class="mainwra">
  <div class="border" style="border-bottom:#ddd solid 1px;">
    <table  width="100%" cellpadding="0" cellspacing="0">
    	<c:choose>
			<c:when test="${ sessionScope.user.sp_init eq 1}">
				<tr>
			        <td>
				   		<p>&nbsp;&nbsp;&nbsp;&nbsp;已启用套帐</p>
				   		<p><h3>&nbsp;&nbsp;&nbsp;&nbsp;您已经【开启帐套】，可以进行业务数据录入！</h3></p>
			        </td>
			      </tr>
			</c:when>
			<c:when test="${ sessionScope.user.sp_init eq 0}">
				<tr>
			        <td class="top-b border-b">
			          <input type="button" id="btn-save" onclick="javascript:doEnableAccount();" class="t-btn btn-red" value="启用"/>
			        </td>
			      </tr>
			      <tr>
			        <td>
				   		<p>&nbsp;&nbsp;&nbsp;&nbsp;在您完成了期初建账以后，必须进行【开启帐套】处理，然后才能进行业务数据录入，</p>
				   		<p>&nbsp;&nbsp;&nbsp;&nbsp;【开启帐套】之后将不能修改期初数据。请谨慎操作！</p>
			        </td>
			      </tr>
			</c:when>
		</c:choose>
    </table>
  </div>
</div>
<script>
	function doEnableAccount(){
		$.dialog.confirm("【开启帐套】之后将不能修改期初数据，确定要【开启帐套】吗?", 
			function(){
				$("#btn-save").attr("disabled",true);
				$.ajax({
					type:"POST",
					url:"<%=basePath%>sys/init/enableAccount",
					data:{},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){
							Public.tips({type: 3, content : "恭喜:启用帐套成功!"});
							setTimeout("window.location.reload()",1000);
						}else{
							Public.tips({type: 1, content : data.message});
							$("#btn-save").attr("disabled",false);
						}
					}
				});
			},
			function(){return;}
		);
	}
	<%-- var batchLimit = '<%=batchLimit%>';
	var IsFinishInit = "<%=IsFinishInit%>";
 	//保存供应商数据
	function doModify(){
 		if (IsFinishInit == "1") {
			$.dialog.tips("已经启用帐套！",2,"32X32/fail.png");
			return;
		}
 		if(batchLimit.indexOf('U')>=0){
 			$.dialog.confirm(
				'【开启帐套】之后将不能修改期初数据，确定要【开启帐套】吗?', 
				function(){
					$.ajax({
						type:"POST",
						url:"<%=basePath%>accountInit/enableAccount.action",
						cache:false,
						dataType:"html",
						success:function(data){
							if(data == "success"){
								alert("恭喜:启用帐套成功!");
								$.dialog.tips("保存成功！",2,"32X32/succ.png");
								window.location.reload();
							} else if(data == "lock"){
								$.dialog.tips("您已经【启用帐套】!",2,"32X32/fail.png");
								window.location.reload();
							} else {
								$.dialog.tips("【启用帐套】失败,请联系系统管理员!",2,"32X32/fail.png");
								window.location.reload();
							}
						}
					});
				},
				function(){return;}
			);
	    }else{
	       $.dialog.tips("对不起，您没有修改权限,请联系系统管理员！",2,"32X32/fail.png");
	    }
	} --%>
</script>
</body>
</html>