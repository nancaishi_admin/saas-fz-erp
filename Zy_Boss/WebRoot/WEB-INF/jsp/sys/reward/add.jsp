<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link type="text/css" rel="stylesheet" href="<%=basePath%>resources/css/info.css"/>
<style type="text/css">
	table tr td b {color:red;}
	.list td{line-height:32px; }
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<div class="border">
	<table width="100%">
		<tr class="list first">
			<td align="right" width="90px">名称：</td>
			<td>
				<input class="main_Input" type="text" name="rw_name" id="rw_name" value=""/>
			</td>
		</tr>
		<tr class="list">
			<td align="right">分数：</td>
			<td>
				<input class="main_Input" type="text" name="rw_score" id="rw_score" value=""
					onkeypress="return onlyDoubleNumber(this,event)"
					onkeyup="javascript:valDoubleNumber(this);"/>
			</td>
		</tr>
		<tr class="list">
			<td align="right">备注：</td>
			<td>
				<input class="main_Input" type="text" name="rw_remark" id="rw_remark" value=""/>
			</td>
		</tr>
		<tr class="list last">
			<td align="right">图标：</td>
			<td>
				<div id="div_icon" class="icons" style="width:170px;" onclick="javascript:Utils.doQueryIcon();">
					<i class="iconfont bg_red">&#xe61f;</i>
				</div>
				<input type="hidden" name="rw_icon" id="rw_icon" value="&#xe61f;" />
				<input type="hidden" name="rw_style" id="rw_style" value="bg_red" />
			</td>
		</tr>
	</table>
</div>
</form>
<div class="footdiv">
	<input class="ui-btn ui-btn-sp mrb" id="btn-save" name="btn-save" type="button" value="确定"/>
	<input class="ui-btn mrb" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
<script src="<%=basePath%>data/sys/reward/reward_add.js"></script>
</body>
</html>