<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <meta http-equiv=Content-Type content="text/html; charset=utf-8"/>
    <link type="text/css" rel="stylesheet" href="<%=basePath%>resources/css/iconfont/iconfont.css"/>
    <link type="text/css" rel="stylesheet" href="<%=basePath%>resources/css/info.css"/>
	<script type="text/javascript">
		document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
		document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=false&skin=mac\"></sc"+"ript>");
	</script>
    <script>
		function addTags(obj,name){
			window.parent.addTags(obj,name);
		}
	</script>	
</head>
<body id="main_content">
<div class="main-menu">
	<div class="basebock">
    	<div class="icon-menu">
        	<div class="icons">
				<ul>
					<li>
						<a class="last_menu" dataType="iframe" dataLink="sys/reward/to_list" id="reward_0" jerichotabindex="reward_0" 
								onclick="javascript:addTags(this,'奖励设置');">
							<i class="iconfont bg_green">&#x3435;</i>
							<span class="last_menu">奖励设置</span>
						</a>
					</li>
					<li>
						<a class="last_menu" dataType="iframe" dataLink="base/shopreward/to_list" id="reward_1" jerichotabindex="reward_1" 
								onclick="javascript:addTags(this,'店铺奖励明细');">
							<i class="iconfont bg_blue">&#x3435;</i>
							<span class="last_menu">店铺奖励明细</span>
						</a>
					</li>
					<li>
						<a class="last_menu" dataType="iframe" dataLink="base/empreward/to_list" id="reward_2" jerichotabindex="reward_2" 
								onclick="javascript:addTags(this,'员工奖励明细');">
							<i class="iconfont bg_red">&#x3435;</i>
							<span class="last_menu">员工奖励明细</span>
						</a>
					</li>
				</ul>
            </div>
        </div>
        <div class="list-menu">
        </div>  
    </div>
</div>
</body>
</html>

