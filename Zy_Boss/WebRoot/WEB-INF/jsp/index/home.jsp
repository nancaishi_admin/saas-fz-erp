<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>首页</title>
<meta http-equiv=Content-Type content="text/html; charset=utf-8"/>
<link type="text/css" rel="stylesheet" href="<%=basePath%>resources/css/home.css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/chart/highcharts.5.0.12.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/chart/highcharts-more.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/chart/modules/solid-gauge.js\"></sc"+"ript>");
</script>
</head>
<body>
	<table class="m_tables" width="100%" border="0" cellspacing="15" cellpadding="0">
  <tr>
    <td rowspan="2" width="230">
        <div class="part1">
            <div class="part-title">
                <b>数据统计(同期/本月)</b>
            </div>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="">
              <tr class="color_g">
                <td align="right" style="width:80px;">进店人数：</td>
                <td>
                	<span id="comeAmount_last"></span>/<span id="comeAmount_this"></span>
                </td>
              </tr>
              <tr class="color_b">
                <td align="right">接待人数：</td>
                <td>
                	<span id="receiveAmount_last"></span>/<span id="receiveAmount_this"></span>
                </td>
              </tr>
              <tr class="color_g">
                <td align="right">试穿人数：</td>
                <td>
                	<span id="tryAmount_last"></span>/<span id="tryAmount_this"></span>
                </td>
              </tr>
              <tr class="color_b">
                <td align="right">成交单数：</td>
                <td>
                	<span id="dealCount_last"></span>/<span id="dealCount_this"></span>
                </td>
              </tr>
              <tr class="color_g">
                <td align="right">退货单数：</td>
                <td>
                	<span id="returnDealCount_last"></span>/<span id="returnDealCount_this"></span>
                </td>
              </tr>
              <tr class="color_b">
                <td align="right">连&nbsp;&nbsp;带&nbsp;&nbsp;率：</td>
                <td>
                	<span id="jointRate_last"></span>/<span id="jointRate_this"></span>
                </td>
              </tr>
              <tr class="color_g">
                <td align="right">销售金额：</td>
                <td>
                	<span id="sellMoney_last"></span>/<span id="sellMoney_this"></span>
                </td>
              </tr>
              <tr class="color_b">
                <td align="right">客单均价：</td>
                <td>
                	<span id="avgSellPrice_last"></span>/<span id="avgSellPrice_this"></span>
                </td>
              </tr>
              <tr class="color_g">
                <td align="right">发会员卡：</td>
                <td>
                	<span id="newVipCount_last"></span>/<span id="newVipCount_this"></span>
                </td>
              </tr>
              <tr class="color_b">
                <td align="right">发储值卡：</td>
                <td>
                	<span id="newCardCount_last"></span>/<span id="newCardCount_this"></span>
                </td>
              </tr>
            </table>
        </div>
    </td>
    <td>
        <div class="part2">
            <div class="part-title">
                <b>月度零售</b>
            </div>
            <div class="part-list" id="container" style="min-width:580px;height:204px">
            </div>
        </div>    
    </td>
    <td width="230" rowspan="2">
    	<div class="part3">
            <div class="part-title">
                <b>业务简报</b>
            </div>
            <div class="busyw">
                <i class="yw01"></i>
                <ul>
                    <li><span>库存总量:</span><b id="sd_amount"></b></li>
                    <li><span>库存成本:</span><b id="sd_costmoney"></b></li>
                </ul>
            </div>
            <div class="busyw" id="bank_div">
                <i class="yw01"></i>
                <ul>
                    <li><span>现金:</span><b id="ba_balance_cash"></b></li>
                    <li><span>银行存款:</span><b id="ba_balance"></b></li>
                </ul>
            </div>
            <div class="busyw" id="client_supply_div">
                <i class="yw02"></i>
                <ul>
                    <li><span>客户欠款:</span><b id="client_debt"></b></li>
                    <li><span>供应商欠款:</span><b id="supply_debt"></b></li>
                </ul>
            </div>
            <div class="busyw" style="display:none;" id="expense_income_div">
                <i class="yw02"></i>
                <ul>
                    <li><span>费用开支:</span><b id="expense"></b></li>
                    <li><span>其他收入:</span><b id="income"></b></li>
                </ul>
            </div>
            <div class="busyw">
                <i class="yw03"></i>
                <ul>
                    <li><span>月销售收入:</span><b id="sellMoney"></b></li>
                    <li><span>月销售毛利:</span><b id="sellProfits"></b></li>
                </ul>
            </div>
        </div>
    
    
    
        <%-- <div class="part3">
            <div class="part-title">
                <b>日志信息</b>
            </div>
            <div class="part-list" >
                <ul>
                	<c:forEach var="log" items="${logs}" varStatus="as">
                		<li><a href="javascript:void(0);">${as.count}.${log.lg_title }</a></li>
					</c:forEach>
                </ul>
            </div>
        </div>     --%>
    </td>
  </tr>
  <tr>
    <td>
        <div class="part4">
            <div class="part-title">
                <b>计划进度</b>
            </div>
            <div id="container-month" style="width: 50%; height: 200px; float: left"></div>
            <div id="container-year" style="width: 50%; height: 200px; float: left"></div>
        </div>    
    </td>
  </tr>
</table>
<script src="<%=basePath%>data/index/home.js"></script>
</body>
</html>
