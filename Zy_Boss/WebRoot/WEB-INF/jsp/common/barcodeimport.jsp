<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=gbk"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<title></title>
<link type="text/css" rel="stylesheet" href="<%=basePath%>resources/css/base.css"/>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.form.js\"></sc"+"ript>"); 
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
</script>
</head>
<body style="overflow: hidden;">
<form name="uploadFile" method="post" action="" id="uploadFile" enctype="multipart/form-data">
<table width="670" align="center" style="margin-top:5px;">
  <tr>
    <td height="50" width="160">
      <div class="btn_uptxt">
        <input class="fileInputs" type="file" id="txt" name="txt" />
      </div>
    </td>
    <td>
    	<input id="btn_import" class="t-btn btn-black" type="button" value="导入" onclick="javascript:handle.importTxt();"/>
    	<input id="btn_import_succ" class="t-btn btn-red" type="button" value="保存成功数据" onclick="javascript:handle.importSuccessData();"/>
    	<input class="t-btn btn-bblue" type="button" value="导出失败数据" onclick="javascript:handle.exportFailData();"/>
    	<input id="btn_close" class="t-btn" type="button" value="返回" onclick="javascript:doClose();"/>
    </td>
  </tr>
</table>
</form>
<form name="form1" method="post" action="" id="form1">
<input type="hidden" id="okJsonStr" name="okJsonStr" value=""/>
<input type="hidden" id="failJson" name="failJson" value=""/>
<table width="670" style="height:100%;margin-left:15px" border="0">
  <tr>
	<td width="270px">
		<fieldset style="padding:0px 0px; margin:0px; width:320px; ">
			<legend style="color: green;"><b>导入成功的数据</b></legend>
			<div class="grid-wrap">
				<div style="display:'';width: 100%;">
			        <table id="grid"></table>
			        <div id="page"></div>
			    </div>
			</div>
		</fieldset>
	</td>
	<td valign="top">
		<fieldset style="padding:0px 0px; margin:0px;width:320px; ">
		<legend style="color: red;"><b>导入失败的数据</b></legend>
			<div class="grid-wrap">
				<div style="display:'';width: 100%;">
			        <table id="grid_fail"></table>
			        <div id="page_fail"></div>
			    </div>
			</div>
		</fieldset>
	</td>
  </tr>
</table>

</form>
<script src="<%=basePath%>data/common/common_barcodeimport.js"></script>
</body>
</html>