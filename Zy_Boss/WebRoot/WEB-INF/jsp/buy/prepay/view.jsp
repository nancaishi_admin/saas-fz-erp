<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/validator.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
</script>
<script src="<%=printPath%>/CLodopfuncs.js"></script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" id="pp_id" name="pp_id" value="${prepay.pp_id }"/>
<input type="hidden" id="pp_number" name="pp_number" value="${prepay.pp_number }"/>
<input type="hidden" id="pp_ar_state" name="pp_ar_state" value="${prepay.pp_ar_state }"/>
<div class="mainwra">
<div class="border">
	<table width="100%" cellspacing="0" cellpadding="0" style="border-bottom:#ccc solid 1px;">
	    <tr>
	      <td colspan="4" class="top-b border-b">
	      	<input id="btn-approve" class="t-btn btn-green" type="button" value="审核" style="display:none;"/>
	        <input id="btn-reverse" class="t-btn btn-red" type="button" value="反审核" style="display:none;"/>
	        <input id="btn-print" class="t-btn btn-cblue" type="button" value="打印预览" onclick="javascript:handle.doPrint();"/>
	        <input id="btn_close" class="t-btn" type="button" value="返回"/>
	      </td>
	    </tr>
	    
	    <tr class="list first">
			<td align="right" width="60px">供货厂商：</td>
			<td width="165px">
				<input class="main_Input w146" type="text" readonly="readonly" name="supply_name" id="supply_name" value="${prepay.supply_name }"/>
				<input type="hidden" name="pp_supply_code" id="pp_supply_code" value="${prepay.pp_supply_code }" />
			</td>
			<td align="right" width="80px">单据编号：</td>
			<td>
				<input type="text" class="main_Input w146" value="${prepay.pp_number }" readonly="readonly"/>
			</td>
		</tr>
		<tr class="list">
			<td align="right">经办人员：</td>
			<td>
				<input class="main_Input w146" type="text" readonly="readonly" name="pp_manager" id="pp_manager" value="${prepay.pp_manager }" />
			</td>
			<td align="right">预收日期：</td>
			<td>
				<input readonly type="text" class="main_Input w146"  name="pp_date" id="pp_date" value="${prepay.pp_date }" />
			</td>
		</tr>
		<tr class="list">
			<td align="right">银行账户：</td>
			<td>
				<input class="main_Input w146" type="text" readonly="readonly" name="ba_name" id="ba_name" value="${prepay.ba_name }"/>
				<input type="hidden" name="pp_ba_code" id="pp_ba_code" value="${prepay.pp_ba_code }" />
			</td>
			<td align="right">预收金额：</td>
			<td>
				<input type="text" class="main_Input w146"  name="pp_money" id="pp_money"  value="${prepay.pp_money }" readonly="readonly" />
			</td>
		</tr>
		<tr class="list last">
			<td align="right">备注：</td>
			<td colspan="3">
				<input class="main_Input w146" style="width:411px;" type="text" name="pp_remark" id="pp_remark" value="${prepay.pp_remark }" readonly="readonly"/>
			</td>
		</tr>
    </table>
</div>
</div>

	<div class="danj" id="danju" style="display:none;">
        <style>
            .danj {width: 750px;margin: 0px auto;}
            .danj_tit {width: 750px;text-align: center;font-weight: bold;line-height: 30px;font-size: 18px;}
            .NoM {width: 750px;text-align: center;}
            .NoM_left {width: 355px;padding-right: 15px;float: left;text-align: right;}
            .NoM_left b {border-bottom: 1px solid #000;padding: 0px 10px;}
            .NoM_right {width: 355px;padding-left: 15px;float: left;text-align: left;}
            .NoM_right b {border-bottom: 1px solid #000;padding: 0px 10px;}
            .KK {width: 708px;border: 1px solid #000;line-height: 30px;padding: 10px 20px;text-indent: 2em;padding-bottom: 50px;}
            .clear {clear: both;height: 10px;}
            .KK b {border-bottom: 1px solid #000;padding: 0px 10px;}
            .KK span {padding-left: 30px;width: 678px;float: left;padding-top: 10px;}
            .KK span i {font-style: normal;float: left;width: 120px;}
            .KK span em {float: right;text-align: right;font-style: normal;}
            .bto_Z {line-height: 30px;width: 700px;text-align: right;}
        </style>

        <div class="NoM"><span class="NoM_left"> No.<b>${prepay.pp_number }
        </b></span><span class="NoM_right"><b>${prepay.pp_date }</span>
        </div>
        <div class="clear"></div>
        <div class="KK">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${prepay.pp_date }
            ，&nbsp;&nbsp;&nbsp;向供应商:<b>${prepay.supply_name }
        </b><br/>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;预付金额（￥）<b>${prepay.pp_money }
        </b>元，&nbsp;&nbsp;&nbsp;&nbsp;预付银行:<b>${prepay.ba_name }
        </b><br/>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            备注：<b>${prepay.pp_remark }
        </b><br/>
            <span>此&nbsp;&nbsp;&nbsp;&nbsp;据</span>
            <span><i>单位盖章：</i><em>经办人：<b>${prepay.pp_manager }
            </b></em></span>

            <div class="clear"></div>
        </div>
        <div class="bto_Z">制单人：<b>${prepay.pp_maker }
        </b></div>
    </div>
</form>
<script src="<%=basePath%>data/buy/prepay/prepay_view.js"></script>
</body>
</html>