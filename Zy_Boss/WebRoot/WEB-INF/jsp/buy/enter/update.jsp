<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<style>
.notpassbarcode {
	background: url(<%=basePath%>resources/grid/images/poptxt1.png);
	width: 198px;
	height: 143px;
	position: absolute;
	font-size: 12px;
}
.notcheckbody {
	float: left;
	width: 155px;
	margin: 15px 0 0 10px;
	height: 110px;
	overflow-x: hidden;
	overflow-y: auto;
	SCROLLBAR-HIGHLIGHT-COLOR: #fee7cf;
	SCROLLBAR-ARROW-COLOR: #fee7cf;
}
.notcheckbody p {
	float: left;
	width: 155px;
	height: 16px;
	overflow: hidden;
	padding-left: 5px;
}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.extend.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.messages_zh.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc" + "ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.size.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/validator.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
</script>
<script>
//控制控件只能输入正整数 
function onlyPositiveNumber(e){ 
	var key = window.event ? e.keyCode : e.which;  
    var keychar = String.fromCharCode(key);   
    var result = (Validator.Number).test(keychar);   
    if(!result){      
	     return false;  
	 }else{     
	     return true;
	 } 
}
function valNumber(obj){
	var num = obj.value; 
	var k=window.event.keyCode;
	if(k == 109 || k == 189){
    	obj.value=obj.value.replace("-","");
    	return;
    }
	if(num.length > 0){
		if(num.match(Validator.Number) == null){
			obj.value="";
			Public.tips({type: 2, content : '请正确输入数量!'});
			$("#barcode_amount").focus()
			return false;
		}
	}
}
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" id="et_id" name="et_id" value="${enter.et_id }"/>
<input type="hidden" id="et_type" name="et_type" value="${enter.et_type }"/>
<input type="hidden" id="et_number" name="et_number" value="${enter.et_number }"/>
<input type="hidden" id="et_isdraft" name="et_isdraft" value="0"/>
<div class="border">
	<table width="100%" cellspacing="0" cellpadding="0">
	    <tr>
	      <td class="top-b border-b">
	        <input id="btn-save" class="t-btn btn-red" type="button" value="保存单据"/>
	        <a id="btn-import" class="t-btn btn-black" title="">盘点机导入</a>
	        <input id="btn_close" class="t-btn" type="button" value="返回"/>
	        <span id="errorTip" class="errorTip"></span>
	      </td>
	    </tr>
    </table>

		<table width="100%">
			<tr class="list first">
				<td align="right" width="60px">供货厂商：</td>
				<td width="220px">
					<input class="main_Input" type="text" readonly="readonly" name="supply_name" id="supply_name" value="${enter.supply_name }" style="width:170px;"/>
					<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQuerySupply();"/>
					<input type="hidden" name="et_supply_code" id="et_supply_code" value="${enter.et_supply_code }" />
				</td>
				<td align="right" width="60px">收获仓库：</td>
				<td width="220px">
					<input class="main_Input" type="text" readonly="readonly" name="depot_name" id="depot_name" value="${enter.depot_name }" style="width:170px;"/>
					<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryDepot();"/>
					<input type="hidden" name="et_depot_code" id="et_depot_code" value="${enter.et_depot_code }" />
				</td>
				<td align="right" width="60px">手工单号：</td>
				<td width="220px">
					<input class="main_Input" type="text" name="et_handnumber" id="et_handnumber" value="${enter.et_handnumber }" />
				</td>
				<td align="right" width="60px">单据性质：</td>
				<td>
					<span class="ui-combo-wrap" id="span_property"></span>
					<input type="hidden" name="et_property" id="et_property" value="${enter.et_property }"/>
				</td>
			</tr>
			<tr class="list">
				<td align="right">经办人员：</td>
				<td>
					<input class="main_Input" type="text" readonly="readonly" name="et_manager" id="et_manager" value="${enter.et_manager }" style="width:170px;"/>
					<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryEmp();"/>
				</td>
				<td align="right">制单日期：</td>
				<td>
					<input readonly type="text" class="main_Input Wdate"  name="et_make_date" id="et_make_date" onclick="WdatePicker()" value="${enter.et_make_date }" style="width:198px;"/>
				</td>
				<td align="right">总计数量：</td>
				<td>
					<input class="main_Input" type="text" name="et_amount" id="et_amount" value="${enter.et_amount }" readonly="readonly"/>
				</td>
				<td align="right">总计金额：</td>
				<td>
					<input class="main_Input" type="text" id="et_money" value="${enter.et_money }" readonly="readonly"/>
				</td>
			</tr>
			<tr class="list last">
				<td align="right">备注：</td>
				<td colspan="3">
					<input class="main_Input" type="text" name="et_remark" id="et_remark" value="${enter.et_remark }" style="width:495px;"/>
				</td>
				<td align="right" style="display:${et_type eq 1 ? 'none' : '' };">优惠金额：</td>
				<td style="display:${et_type eq 1 ? 'none' : '' };">
					<input class="main_Input" type="text" id="et_discount_money" name="et_discount_money" value="${enter.et_discount_money }"
							onkeypress="return onlyDoubleNumber(event)"/>
				</td>
				<td align="right" style="display:${et_type eq 1 ? 'none' : '' };">应付金额：</td>
				<td style="display:${et_type eq 1 ? 'none' : '' };">
					<input class="main_Input" type="text" id="et_payable" value="${enter.et_payable }" readonly="readonly"/>
				</td>
			</tr>
		</table>
	<table cellpadding="0" cellspacing="0"  class="top-b border-t" width="100%">
		<tr>
			<td width="220">
				货号查询：<input name="pi_no" type="text" id="pi_no" class="ui-input ui-input-ph w120" onkeyup="javascript:if (event.keyCode==13){popups.input()};"/>
               <input type="button" value=" " class="btn_select" onclick="javascript:Choose.selectProduct();"/>
			</td>
			<td width="280">
				扫描条码：<input name="text" type="text" id="barcode" onkeyup="javascript:if (event.keyCode==13){Choose.barCode()};" class="ui-input ui-input-ph w146"/>
				<input id="barcode_amount" class="ui-input ui-input-ph" value="1" style="width:20px" maxlength="4"
						onkeypress="return onlyPositiveNumber(event)"
						onkeyup="javascript:valNumber(this);"/>
			</td>
			<td width="270" id="priceTypeTd">
			    <label class="radio radiow7">
			        <input type="radio" name="priceType" checked="checked" value="1"/>
			        <b>最后一次进价</b>
			    </label>
			    <label class="radio radiow6">
			        <input type="radio" name="priceType" value="2"/>
			        <b>进货折扣率:</b>
			    </label>
			    <input name="text" type="text" id="sp_rate" disabled="disabled" class="ui-input ui-input-ph" style="width:40px; color:red;" 
			    		value="1.0" maxlength="4"
			           onkeyup="javascript:doOnlyDouble(this);"
			           onkeypress="javascript:if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"
			           onkeydown="javascript:doOnlyDouble(this);"
			           />
			    
			</td>
			<td>
				<input type="hidden" id="CurrentMode" value="0"/>
				<a id="sumBtn" name="mode-btn" class="t-btn btn-bl"  onclick="showMode.display(2);"><i></i>汇总</a>
			    <a id="listBtn" name="mode-btn" class="t-btn btn-blc on"  onclick="showMode.display(0);"><i></i>列表</a>
			    <a id="sizeBtn" name="mode-btn" class="t-btn btn-bc" onclick="showMode.display(1);"><i></i>尺码</a>
			    <a class="t-btn btn-red" id="btnClear">清除</a>
			</td>
		</tr>
	</table>
</div>
<div class="grid-wrap">
    <div id="list-grid" style="display:'';width: 100%;">
        <table id="grid"></table>
        <div id="page"></div>
    </div>
    <div id="size-grid" style="display:none; width: 100%;">
        <table id="sizeGrid"></table>
        <div id="sizePage"></div>
    </div>
    <div id="sum-grid" style="display:none;width: 100%;">
       <table id="sumGrid"></table>
       <div id="sumPage"></div>
   </div>
</div>
<input type="hidden" id="CheckNotPassTextShow" value=""/>
<div id="BarCodeCheckNotPass" class="notpassbarcode" title="双击关闭错误提示" style="display:none;" ondblclick="this.style.display='none'">
 	<span class="notcheckbody" id="CheckNotPassText"></span>
</div>

</form>
<script>
	function calcPayable(){
		var et_discount_money = $("#et_discount_money").val();
		if(et_discount_money == "" || isNaN(et_discount_money)){
			return;
		}
		$("#et_payable").val(PriceLimit.formatByEnter($("#et_money").val()-et_discount_money));
	}
	$().ready(function() {
		$("#form1").validate({
			ignore: "",
			onfocusout : function(element) {
				$(element).valid();
			},
			errorPlacement: function(error, element) { //错误信息位置设置方法
				element.focus();
				$("#errorTip").empty();
				error.appendTo( $("#errorTip") );
			},
			rules : {
				et_supply_code : "required",
				et_depot_code : "required",
				et_manager : "required"
			},
			messages : {
				et_supply_code : "请选择供应厂商",
				et_depot_code : "请选择仓库",
				et_manager : "请选择经办人"
			}
		});
	});
</script>
<script src="<%=basePath%>data/buy/enter/enter_add.js"></script>
</body>
</html>