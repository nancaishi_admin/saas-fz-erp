<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/comm-search.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
</script> 
</head>
<body id="main_content">
<form name="form1" method="post" action="" id="form1">
<input type="hidden" id="wt_type" name="wt_type" value="${wt_type }"/>
<input type="hidden" id="shop_type" name="shop_type" value="${sessionScope.user.shoptype }"/>
<input type="hidden" name="flushFlag" id="flushFlag" value=""/>
<div class="mainwra">
	<div class="mod-search cf">
	   <div class="fl">
	    	<div id="filter-menu" class="ui-btn-menu fl">
	    		 <span style="float:left" class="ui-btn menu-btn">
		     		 <strong>查询条件</strong>
		     			 <input type="text" class="main_Input Wdate-select" readonly="readonly" id="begindate" name="begindate" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'enddate\',{d:0})}'})"  value="" />
							-
	                     <input type="text" class="main_Input Wdate-select" readonly="readonly" id="enddate" name="enddate" onclick="WdatePicker({minDate:'#F{$dp.$D(\'begindate\',{d:0})}'})"   value="" />
					 <b></b>
			  	 </span>
		 		<div class="con" >
					<ul class="ul-inline">
						<li>
							<table class="searchbar">
								<tr>     
									<td align="right">日期选择：</td>
									<td id="date">
			                            <label class="radio" id="date_default" style="width:30px">
							  	 			<input name="redio1" type="radio" id="theDate" value="1" checked="checked"/>今天
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="yesterday" value="2" />昨天
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theWeek" value="3" />本周
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theMonth" value="4" />本月
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theSeason" value="5" />本季
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theYear" value="6" />本年
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="lastYear" value="7" />去年
								  	 	</label>
							  	 	</td>
								 </tr>
								 <tr>     
									<td align="right">商品类型：</td>
									<td id="td_wp_type">
			                            <label class="radio" style="width:30px"><input name="radio_wp_type" type="radio" value="" checked="checked"/>全部</label>
								  	 	<label class="radio" style="width:30px"><input name="radio_wp_type" type="radio" value="0" />正常商品</label>
								  	 	<label class="radio" style="width:30px"><input name="radio_wp_type" type="radio" value="1" />新品</label>
								  	 	<label class="radio" style="width:30px"><input name="radio_wp_type" type="radio" value="2" />折扣商品</label>
								  	 	<input type="hidden" id="wp_type" name="wp_type" value=""/>
							  	 	</td>
								 </tr>
								 <tr>     
									<td align="right">是否试穿：</td>
									<td id="td_wp_istry">
			                            <label class="radio" style="width:30px"><input name="radio_wp_istry" type="radio" value="" checked="checked"/>全部</label>
								  	 	<label class="radio" style="width:30px"><input name="radio_wp_istry" type="radio" value="1" />是</label>
								  	 	<label class="radio" style="width:30px"><input name="radio_wp_istry" type="radio" value="0" />否</label>
								  	 	<input type="hidden" id="wp_istry" name="wp_istry" value=""/>
							  	 	</td>
								 </tr>
								 <tr>     
									<td align="right">上下架：</td>
									<td id="td_wp_state">
			                            <label class="radio" style="width:30px"><input name="radio_wp_state" type="radio" value="" checked="checked"/>全部</label>
								  	 	<label class="radio" style="width:30px"><input name="radio_wp_state" type="radio" value="0" />上架</label>
								  	 	<label class="radio" style="width:30px"><input name="radio_wp_state" type="radio" value="1" />下架</label>
								  	 	<input type="hidden" id="wp_state" name="wp_state" value=""/>
							  	 	</td>
								 </tr>
								  <tr>
									  <td align="right">商品分类：</td>
										<td>
										  	<input class="main_Input" type="text" id="pt_name" name="pt_name" value="" readonly="readonly" style="width:170px; " />
										     <input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryProductType();"/>
										     <input type="hidden" name="wp_pt_code" id="wp_pt_code" value=""/>
								  		</td>
								  </tr>
									<tr>
										<td align="right">商品货号：</td>
										<td>
											<input type="text" id="pd_no" name="pd_no" class="main_Input" value=""/>
										</td>
									</tr>
									<tr>
										<td align="right">商品名称：</td>
										<td>
											<input type="text" id="pd_name" name="pd_name" class="main_Input" value=""/>
										</td>
									</tr>
							</table>
						</li>
						<li style="float:right;">
							<a class="ui-btn ui-btn-sp mrb" id="btn_ok">确认</a>
		       				<a class="ui-btn mrb" id="btn_reset">重置</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	    <div class="fl-m">
	    	<a class="ui-btn mrb" id="btn-search" name="btn-search">查询</a>
		</div>
	    <div class="fr">
	    	<a href="javascript:void(0);" class="ui-btn ui-btn-sp mrb" id="btn-add" >新增</a>
	    	<a href="javascript:void(0);" class="ui-btn ui-btn-sp mrb" id="btn-del" >删除</a>
	    </div> 
	 </div>
	<div class="grid-wrap">
	    <table id="grid">
	    </table>
    	<div id="page"></div>
	</div>
</div>
</form>
<script src="<%=basePath%>data/wx/product/product_list.js"></script>
</body>
</html>