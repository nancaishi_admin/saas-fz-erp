<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>导航页</title>
    <meta http-equiv=Content-Type content="text/html; charset=utf-8"/>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
	table tr td b {color:red;}
	.list td{line-height:32px; }
	.main_Input{width: 225px; height: 16px;}
	.width100{width:100px;}
	.mutab {
	    height: 32px;
	    margin: 10px 0 0 11px;
	}
	ul {
	    list-style: none;
	}
	.mutab li {
	    cursor: pointer;
	    display: inline;
	    float: left;
	    margin-right: 5px;
	}
	.select {
	    background: #fff;
	    border-style: solid;
	    border-width: 2px 1px 1px;
	    border-color: #08c #ddd #ddd;
	    height: 30px;
	    text-align: center;
	    line-height: 30px;
	    width: 120px;
	    color: #08c;
	    font-weight: 700;
	    cursor: hand;
	}
	.menu {
	    background: #f1f1f1;
	    border-style: solid;
	    border-width: 1px 1px 0;
	    border-color: #ddd;
	    height: 30px;
	    text-align: center;
	    line-height: 30px;
	    width: 120px;
	    cursor: hand;
	}
	.content {
	    border: #ddd 1px solid;
	    height: 460px;
	    width: 98%;
	    background-color: #FFFFFF;
	    margin: 0 0 0 10px;
	}
	.topdiv{
	    bottom: 0;
	    height: 28px;
	    right: 0;
	    padding-top:-40px;
	    right: 0;
	    text-align: right;
	}
	.paycon{float:left;margin:10px 0 0 10px;width: 98%;height:auto;padding-bottom:10px;}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.form.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/UpImgPreview.js\"></sc"+"ript>");
</script>
<script>
function selContent(showId){
	$("#menu1").removeClass().addClass("menu");
	$("#menu2").removeClass().addClass("menu");
	$("#menu3").removeClass().addClass("menu");
	$("#menu4").removeClass().addClass("menu");
	$("#menu"+showId).removeClass().addClass("select");
	$("#tagContent1").hide();
	$("#tagContent2").hide();
	$("#tagContent3").hide();
	$("#tagContent4").hide();
	$("#tagContent"+showId).show();
}
</script>
</head>
<body>
<form name="form1" method="post" action="" id="form1">
<div class="border">
	<div class="mutab">
		<ul>
			<li id="menu1" class="select" onclick="selContent('1')">微信公众号设置</li>
			<li id="menu2" class="menu" onclick="selContent('2')" >菜单设置</li>
			<li id="menu3" class="menu" onclick="selContent('3')" >主题设置</li>
			<li id="menu4" class="menu" onclick="selContent('4')" >积分兑红包</li>
		</ul>
	</div>
	<div>
		<div class="paycon" id="tagContent1">
			<input type="hidden" id="wx_id" name="wx_id" value="${wechatInfo.wx_id }"/>
			<table width="100%">
				<tr class="list first">
					<td align="right" width="120">公众号类型：</td>
					<td>
						<input class="main_Input" type="text" value="认证服务号" disabled="disabled"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right" width="100"><b>*</b>公众号名称：</td>
					<td>
						<input class="main_Input" type="text" name="wx_name" id="wx_name" value="${wechatInfo.wx_name }" maxlength="50"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right"><b>*</b>原始ID：</td>
					<td>
						<input class="main_Input" type="text" name="wx_originalid" id="wx_originalid" value="${wechatInfo.wx_originalid }" maxlength="50"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right"><b>*</b>微信号：</td>
					<td>
						<input class="main_Input" type="text" name="wx_accountchat" id="wx_accountchat" value="${wechatInfo.wx_accountchat }" maxlength="100"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right"><b>*</b>AppId：</td>
					<td>
						<input class="main_Input" type="text" name="wx_appid" id="wx_appid" value="${wechatInfo.wx_appid }" maxlength="50"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right"><b>*</b>AppSecret：</td>
					<td>
						<input class="main_Input" type="text" name="wx_appsecret" id="wx_appsecret" value="${wechatInfo.wx_appsecret }" maxlength="50"/>
					</td>
				</tr>
				<!-- <tr class="list">
					<td align="right">网页授权文件名称：</td>
					<td>
						<input class="main_Input" type="text" name="us_name" id="us_name" value="" maxlength="200"/><span style="color:red;">*.txt 认证文件名称</span>
					</td>
				</tr>
				<tr class="list">
					<td align="right">网页授权文件内容：</td>
					<td>
						<input class="main_Input" type="text" name="us_name" id="us_name" value="" maxlength="200"/><span style="color:red;">*.txt 认证文件名称</span>
					</td>
				</tr> -->
				<tr class="list">
					<td align="right">商户号：</td>
					<td>
						<input class="main_Input" type="text" name="wx_mch_id" id="wx_mch_id" value="${wechatInfo.wx_mch_id }" maxlength="50"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">商户key：</td>
					<td>
						<input class="main_Input" type="password" name="wx_mch_key" id="wx_mch_key" value="" maxlength="100"/>
						<font style="color:green;">${!empty wechatInfo.wx_mch_key ? '商户key已设置':'' }</font>
					</td>
				</tr>
				<tr class="list">
					<td align="right">商户证书：</td>
					<td>
						<input class="main_Input" type="file" name="mchCer" id="mchCer"/>
						<span style="color:red;">*.p12证书文件</span>
					</td>
				</tr>
				<tr class="list">
					<td align="right">证书密码：</td>
					<td>
						<input class="main_Input" type="password" name="wx_mch_pwd" id="wx_mch_pwd" value="" maxlength="50"/>
						<font style="color:green;">${!empty wechatInfo.wx_mch_pwd ? '密码已设置':'' }</font>
					</td>
				</tr>
				<tr class="list">
					<td align="right"><b>*</b>会员默认类别：</td>
					<td>
						<span class="ui-combo-wrap" id="span_membertype" style="width:237px;">
						</span>
						<input type="hidden" name="wx_mt_code" id="wx_mt_code" value="${wechatInfo.wx_mt_code }"/>
					</td>
				</tr>
				<tr class="list last">
					<td align="right"></td>
					<td>
						<input class="t-btn btn-red" type="button" value="保存设置" onclick="javascript:Utils.saveWechatInfo();"/>
					</td>
				</tr>
			</table>
			
		</div>
		<div class="paycon" id="tagContent2" style="display:none;overflow-y:auto;">
			
		</div>
		<div class="paycon" id="tagContent3" style="display:none;overflow-y:auto;">
			<input type="hidden" id="ws_id" name="ws_id" value="${wechatSet.ws_id }"/>
			<table width="100%">
				<tr class="list first">
					<td align="right" width="100"><b>*</b>微信标题：</td>
					<td>
						<input class="main_Input" type="text" name="ws_title" id="ws_title" value="${wechatSet.ws_title }" maxlength="50"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">微信背景图：</td>
					<td>
						<input class="main_Input" type="file" name="wx_bg" id="wx_bg" />
						<span style="color:red;">【建议尺寸720*360，大小不要超过200K。】</span>
					</td>
				</tr>
				<tr class="list">
					<td align="right"></td>
					<td>
						<c:choose>
							<c:when test="${ !empty wechatSet.ws_bgimg_path}">
								<img id="imgview" style="display:inherit;" width="320" height="160" 
									src="<%=serverPath%>${wechatSet.ws_bgimg_path}"/>
							</c:when>
							<c:otherwise>
								<img id="imgview" style="display:inherit;" width="320" height="160" />
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
				<tr class="list last">
					<td align="right"></td>
					<td>
						<input class="t-btn btn-red" type="button" value="保存设置" onclick="javascript:Utils.saveWechatSet();"/>
					</td>
				</tr>
			</table>
		</div>
		<div class="paycon" id="tagContent4" style="display:none;overflow-y:auto;">
			
		</div>
	</div>
</div>
</form>
<script src="<%=basePath%>data/wx/wechat/wechat_set.js"></script>
</body>
</html>

