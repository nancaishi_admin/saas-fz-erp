<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>导航页</title>
    <meta http-equiv=Content-Type content="text/html; charset=utf-8"/>
    <link type="text/css" rel="stylesheet" href="<%=basePath%>resources/css/iconfont/iconfont.css"/>
    <link type="text/css" rel="stylesheet" href="<%=basePath%>resources/css/info.css"/>
	<script type="text/javascript">
		document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
		document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=false&skin=mac\"></sc"+"ript>");
	</script>
    <script>
		function addTags(obj,name){
			window.parent.addTags(obj,name);
		}
	</script>	
</head>
<body id="main_content">
<div class="main-menu">
	<div class="basebock">
    	<div class="icon-menu">
        	<div class="icons">
				<ul>
					<li>
						<a class="last_menu" dataType="iframe" dataLink="base/shopline/to_shop_line" id="_10"
							jerichotabindex="10" onclick="javascript:addTags(this,'开通微店');">
							<i class="iconfont bg_green">&#xe612;</i>
							<span class="last_menu">开通微店</span>
						</a>
					</li>
					<li>
						<a class="last_menu" dataType="iframe" dataLink="wx/producttype/to_all" id="_20"
							jerichotabindex="20" onclick="javascript:addTags(this,'商品分类');">
							<i class="iconfont bg_green">&#xe631;</i>
							<span class="last_menu">商品分类</span>
						</a>
					</li>
					<li>
						<a class="last_menu" dataType="iframe" dataLink="wx/product/to_list" id="_30"
							jerichotabindex="30" onclick="javascript:addTags(this,'发布商品');">
							<i class="iconfont bg_green">&#xe608;</i>
							<span class="last_menu">发布商品</span>
						</a>
					</li>
				</ul>
            </div>
        </div>
        
    </div>
</div>
</body>
</html>

