<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
	table tr td b {color:red;}
	.list td{line-height:32px; }
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.extend.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.messages_zh.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
</script>
<script type="text/javascript">
function onlyNumber(obj,event,canNegative){//canNegative:是否可以输入负数
	var keyCode = event.keyCode||event.which;
	if(keyCode == 8 || keyCode == 9 || keyCode == 37 || keyCode == 39){//退格键、tab键、左右方向键
		return;
	}
	if(canNegative != undefined && canNegative){//可以输入负数
		if(!(((keyCode >= 48) && (keyCode <= 57)) || (keyCode == 13) || (keyCode == 46) || (keyCode == 45))){ 
			return false;
		}
	}else{
		if(!(((keyCode >= 48) && (keyCode <= 57)) || (keyCode == 13) || (keyCode == 46))){ 
			return false;
		}
	}
	var position = 0; 
	if (document.selection) {	//for IE8
		obj.focus();
		var sel = document.selection.createRange();
		position = obj.value.length - sel.text.length;
	}else{
		position = obj.selectionStart;
	}
	var ch=String.fromCharCode(keyCode);
	var value = obj.value.substring(0,position)+ch+obj.value.substring(position);//输入后的值
	
	var reg = /^\d+\.?\d{0,2}$/;
	if(canNegative != undefined && canNegative){//可以输入负数
		reg = /^(-)?\d+\.?\d{0,2}$/;
		if(value == "-"){
			return true;
		}
	}
	if(!(reg.test(value))){//验证只能输入两位小数
		return false;
	}
 }

</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" name="ci_id" id="ci_id" value="${client.ci_id }"/>
<input type="hidden" name="ci_code" id="ci_code" value="${client.ci_code }"/>
<div class="border">
	<table width="100%">
		<tr class="list first">
			<td align="right" width="90px"><b>*</b>客户名称：</td>
			<td width="220px">
				<input class="main_Input" type="text" name="ci_name" id="ci_name" value="${client.ci_name }"/>
			</td>
			<td align="right" width="80px"><b>*</b>所在地区：</td>
			<td>
				<input readonly="readonly" class="main_Input" type="text" name="ar_name" id="ar_name" value="${client.ar_name }" style="width:170px;"/>
				<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryArea();"/>
				<input class="main_Input" type="hidden" name="ci_area" id="ci_area" value="${client.ci_area }"/>
			</td>
		</tr>
		<tr class="list">
			<td align="right">销售价格：</td>
			<td>
				<span class="ui-combo-wrap" id="span_ci_default">
				</span>
				<input type="hidden" name="ci_default" id="ci_default"/>
			</td>
			<td align="right">折扣率：</td>
			<td>
				<input class="main_Input" type="text" name="ci_rate" id="ci_rate" value="${client.ci_rate }"
						onkeypress="javascript:return onlyNumber(this,event);"/>
			</td>
		</tr>
		<tr class="list">
			<td align="right">联系人：</td>
			<td>
				<input class="main_Input" type="text" name="ci_man" id="ci_man" value="${client.ci_man }"/>
			</td>
			<td align="right">手机号码：</td>
			<td>
				<input class="main_Input" type="text" name="ci_mobile" id="ci_mobile" value="${client.ci_mobile }"/>
			</td>
		</tr>
		<tr class="list">
			<td align="right">电话：</td>
			<td>
				<input class="main_Input" type="text" name="ci_tel" id="ci_tel" value="${client.ci_tel }"/>
			</td>
			<td align="right">地址：</td>
			<td>
				<input class="main_Input" type="text" name="ci_addr" id="ci_addr" value="${client.ci_addr }"/>
			</td>
		</tr>
		<tr class="list">
			<td align="right">开户银行：</td>
			<td>
				<input class="main_Input" type="text" name="ci_bank_open" id="ci_bank_open" value="${client.ci_bank_open }"/>
			</td>
			<td align="right">银行帐号：</td>
			<td>
				<input class="main_Input" type="text" name="ci_bank_code" id="ci_bank_code" value="${client.ci_bank_code }"/>
			</td>
		</tr>
		<tr class="list">
			<td align="right">批发周期：</td>
			<td>
				<input class="main_Input" type="text" name="ci_batch_cycle" id="ci_batch_cycle" value="${client.ci_batch_cycle }"/>
			</td>
			<td align="right">结算周期：</td>
			<td>
				<input class="main_Input" type="text" name="ci_settle_cycle" id="ci_settle_cycle" value="${client.ci_settle_cycle }"/>
			</td>
		</tr>
		<tr class="list">
			<td align="right">保证金：</td>
			<td>
				<input class="main_Input" type="text" name="ci_earnest" id="ci_earnest" value="${client.ci_earnest }"
					onkeypress="javascript:return onlyNumber(this,event);"/>
			</td>
			<td align="right">装修押金：</td>
			<td>
				<input class="main_Input" type="text" name="ci_deposit" id="ci_deposit" value="${client.ci_deposit }"
					onkeypress="javascript:return onlyNumber(this,event);"/>
			</td>
		</tr>
		<tr class="list">
			<td align="right">开启信誉额度：</td>
			<td id="use_credit">
				<label class="radio" style="width:30px">
	  	 			<input name="redio1" type="radio" value="0" ${client.ci_use_credit eq 0 ? 'checked' : '' }/>否
		  	 	</label>
		  	 	<label class="radio" style="width:30px">
		  	 		<input name="redio1" type="radio" value="1" ${client.ci_use_credit eq 1 ? 'checked' : '' }/>是
		  	 	</label>
				<input type="hidden" id="ci_use_credit" name="ci_use_credit" value="${client.ci_use_credit }"/>
			</td>
			<td align="right">信誉额度：</td>
			<td>
				<input class="main_Input" type="text" name="ci_credit_limit" id="ci_credit_limit" value="${client.ci_credit_limit }"
						${client.ci_use_credit eq 0 ? 'disabled' : '' }
						onkeypress="javascript:return onlyNumber(this,event);"/>
			</td>
		</tr>
		<tr class="list">
			<td align="right">期初应收款：</td>
			<td>
				<input class="main_Input" type="text" name="ci_init_debt" id="ci_init_debt" value="${client.ci_init_debt }"
						onkeypress="javascript:return onlyNumber(this,event,true);"
						${sessionScope.user.sp_init eq 1 ? 'disabled' : ''}/>
			</td>
			<td align="right">备注：</td>
			<td>
				<input class="main_Input" type="text" name="ci_remark" id="ci_remark" value="${client.ci_remark }"/>
			</td>
		</tr>
	  	<tr class="list last">
			<td align="right"></td>
			<td id="errorTip" class="errorTip" colspan="3"></td>
		</tr>
	</table>
</div>
</form>
<div class="footdiv">
	<input class="ui-btn ui-btn-sp mrb" id="btn-save" name="btn-save" type="button" value="确定"/>
	<input class="ui-btn mrb" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
<script>
	$().ready(function() {
		$("#form1").validate({
			ignore: "",
			errorLabelContainer:"#errorTip",
			wrapper:"span",
			onfocusout : function(element) {
				$(element).valid();
			},
			showErrors : function(errors, errorList) {
				this.defaultShowErrors();
				$('#errorTip span:hidden').remove()// 删除所有隐藏的span标签
				$('#errorTip span:eq(0)').nextAll().remove()// 第一个span下所有跟随的同胞级span删除
			},
			rules : {
				ci_name : "required",
				ci_area : "required",
				ci_rate : {required : true,isDouble:true},
				ci_mobile : {isMobile : true},
				ci_tel : {isPhone : true},
				ci_batch_cycle : {digits:true},
				ci_settle_cycle : {digits:true},
				ci_earnest : {number:true},
				ci_deposit : {number:true},
				ci_credit_limit : {number:true},
				ci_init_debt : {number:true}
			},
			messages : {
				ci_name : "请输入客户名称",
				ci_area : "请选择区域",
				ci_rate : {required : "请输入折扣率",isDouble : "请输入有效的折扣率"},
				ci_mobile : "请输入正确的手机号码",
				ci_tel : "请输入正确的电话号码",
				ci_batch_cycle : "请输入正确的批发周期",
				ci_settle_cycle : "请输入正确的结算周期",
				ci_earnest : "请输入正确的保证金",
				ci_deposit : "请输入正确的装修押金",
				ci_credit_limit : "请输入正确的信誉额度",
				ci_init_debt : "请输入正确的期初应收款"
			}
		});
	});
</script>
<script src="<%=basePath%>data/batch/client/client_add.js"></script>
</body>
</html>