<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<%-- <link href="<%=basePath%>resources/dialog/css/default_dialog.css" rel="stylesheet" type="text/css"/> --%>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<style>
body{
	overflow:hidden;
	margin:0;
	padding:0;
	background:#fff;
}
td{padding:2px;}
.footdiv{position:fixed;bottom:0;left:0;right:0;height:30px;padding-top:5px;border-top:#ddd solid 1px;background:#f4f4f4;}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/hanzi2pinyin.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/validator.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
</script>
<script>
var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var api = frameElement.api, W = api.opener;
var callback = api.data.callback;
var oper = api.data.oper;
//验证手机号码
function checkTel(obj){
  	var s = obj.value;
	var regu = /(\(\d{3,4}\)|\d{3,4}-|\s)?\d/;
	if(s != ''){
		if (!regu.test(s)){
		    W.Public.tips({type: 2, content : "请输入正确的电话号码!"});
		    obj.value="";
		    return;
		}
	}
}
//验证手机号码
function checkMobile(obj){
  	var s = obj.value;
	var regu =/^[0-9]*[1-9][0-9]*$/;
	if(s != ''){
		if (!regu.test(s)){ 
			W.Public.tips({type: 2, content : "请输入正确的手机号码!"});
		    obj.value="";
		    return; 
		}
	}
}
function doNo(obj){
    if(obj.value==0){
		 document.getElementById("cis_stutas").value = 0;
    }else{
		 document.getElementById("cis_stutas").value = 1; 
    }
 }

function doYes(obj){
   if(obj.value==1){
	   document.getElementById("cis_stutas").value = 1; 
   }else{
	  document.getElementById("cis_stutas").value = 0;
   }
}

var THISPAGE={
	init:function(){
		this.initDom();
		this.initEvent();
	},
	initDom:function(){
		$_cis_name=$("#cis_name");
		document.getElementById("cis_name").focus();
	},
	checkForm:function(){
		if($_cis_name.val()==''){
			W.Public.tips({type: 2, content : "请输入店铺名称！"});
			$_cis_name.focus();
			return false;
		}
		return true;
	}, 
	save:function(){
		if (!this.checkForm()){
			return;
		}
		$("#btn-save").attr("disabled",true);
			$.ajax({
				type:"POST",
				url:config.BASEPATH+"batch/client/save_client_shop",
				data:encodeURI(encodeURI($('#form1').serialize())),
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						W.Public.tips({type: 3, content : "数据保存成功!"});
						var cis_name = $("#cis_name").val();
						var cis_linkman = $("#cis_linkman").val();
						var cis_link_tel = $("#cis_link_tel").val();
						var cis_link_mobile = $("#cis_link_mobile").val();
						var cis_link_adr = $("#cis_link_adr").val();
						var cis_stutas = $("#cis_stutas").val();
						var rowJson={"id":data.data.cis_id,"cis_code":data.data.cis_code,"cis_name":cis_name,"cis_linkman":cis_linkman
								,"cis_link_tel":cis_link_tel
								,"cis_link_mobile":cis_link_mobile
								,"cis_link_adr":cis_link_adr
								,"cis_stutas":cis_stutas};
						if(callback && typeof callback == 'function'){
							callback(rowJson,oper,window);
						}
						$("#btn-save").attr("disabled",false);
						$("#cis_name").focus();
						THISPAGE.reset();
					}else{
						W.Public.tips({type: 1, content : "数据保存失败!"});
						$("#btn-save").attr("disabled",false);
						return;
					}
				}
			});
		},
		reset:function(){
			$("#form1")[0].reset();
		},
		cancel:function(){
			api.close();
		},
		initEvent:function(){
			$("#btn-save").on("click", function(e){
				e.preventDefault();
				THISPAGE.save();
			});
			$("#btn-cancel").on("click", function(e){
				e.preventDefault();
				THISPAGE.cancel();
			});
		}
};
	
$(function(){
	THISPAGE.init();
});
</script>
</head>
<body>
<form name="form1" method="post" action="" id="form1">
<input type="hidden" name="cis_ci_code" id="cis_ci_code" value="${ci_code }"/>
<input type="hidden" name="cis_stutas" id="cis_stutas" value="0"/>
<table class="pad_t20" width="100%" height="90%">
	<tr>
		<td align="right" width="25%"><font color="red">*</font>店铺名称：</td>
		<td align="left">
		<input class="main_Input" type="text" name="cis_name" value="" id=cis_name />
		</td>
	</tr>
	<tr>
		<td align="right" width="25%">联系人：</td>
		<td align="left">
		<input class="main_Input" type="text" name="cis_linkman" value="" id="cis_linkman" />
		</td>
	</tr>
	<tr>
		<td align="right" width="25%">固定电话：</td>
		<td align="left">
		<input class="main_Input" type="text" name="cis_link_tel" value="" id="cis_link_tel" maxlength="15" title="" value="" onblur="checkTel(this);"/>
		</td>
	</tr>
	<tr>
		<td align="right" width="25%">移动电话：</td>
		<td align="left">
		<input class="main_Input" type="text" name="cis_link_mobile" id="cis_link_mobile" maxlength="11" value="" onblur="javascript:checkMobile(this);"/>
		</td>
	</tr>
	<tr>
		<td align="right" width="25%">店铺地址：</td>
		<td align="left">
		<input class="main_Input" type="text" name="cis_link_adr" value="" id="cis_link_adr" />
		</td>
	</tr>
	<tr>
		<td align="right" width="25%">状态：</td>
		<td align="left">
			<input  type="radio" name="isLine" value="0" checked onclick="javascript:doNo(this);"/>启用
			<input  type="radio" name="isLine" value="1" onclick="javascript:doYes(this);"/>停用
		</td>
	</tr>
</table>
<div class="footdiv">
	<input class="btn_confirm" type="button" id="btn-save"  value="确定"/>
	<input class="btn_close" type="button" id="btn-cancel" value="取消"/>
</div>
</form>
</body>
</html>