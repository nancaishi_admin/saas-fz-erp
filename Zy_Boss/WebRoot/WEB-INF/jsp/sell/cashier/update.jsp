<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
	.list td{line-height:35px; }
	.errorTip{color:red;height:30px;font: 12px Microsoft Yahei;}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.messages_zh.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
</script> 
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" name="ca_id" id="ca_id" value="${model.ca_id}"/>
<div class="border">
	<table width="100%">
				<tr class="list first">
					<td align="right" width="100px">所属店铺：</td>
					<td>
						<input class="main_Input w146" type="text" name="shop_name" id="shop_name" value="${model.shop_name}" disabled="disabled"/>
						<input type="hidden" name="ca_shop_code" id="ca_shop_code" value="${model.ca_shop_code}"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right" width="100px"><font color="red">*</font>员工：</td>
					<td>
						<input class="main_Input w146" type="text" name="em_name" id="em_name" value="${model.em_name}" disabled="disabled" />
						<input type="hidden" name="ca_em_code" id="ca_em_code" value="${model.ca_em_code}"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right"><font color="red">*</font>最高让利：</td>
					<td>
						<input class="main_Input w146" type="text" id="ca_maxmoney" name="ca_maxmoney" value="${model.ca_maxmoney}"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right"><font color="red">*</font>最低折扣：</td>
					<td>
						<input class="main_Input w146" type="text" id="ca_minrate" name="ca_minrate" value="${model.ca_minrate}"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right"><font color="red">*</font>最高抹零：</td>
					<td>
						<input class="main_Input w146" type="text" id="ca_erase" name="ca_erase" value="${model.ca_erase}"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right"><font color="red">*</font>零售天数：</td>
					<td>
						<input class="main_Input w146" type="text" id="ca_days" name="ca_days" value="${model.ca_days}"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">状态：</td>
					<td>
						<span class="ui-combo-wrap" id="spanState" ></span>
						<input type="hidden" name="ca_state" id="ca_state" value="${model.ca_state}"/>
					</td>
				</tr>
				<tr class="list last">
					<td id="errorTip" class="errorTip" colspan="2" style="padding-left: 80px;"></td>
				</tr>
			</table>
</div>
</form>
<div class="footdiv">
	<input class="ui-btn ui-btn-sp mrb" id="btn-save" name="btn-save" type="button" value="确定"/>
	<input class="ui-btn mrb" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
<script>
	$().ready(function() {
		$("#form1").validate({
			ignore: "",
			onfocusout : function(element) {
				$(element).valid();
			},
			errorPlacement: function(error, element) { //错误信息位置设置方法
				element.focus();
				$("#errorTip").empty();
				error.appendTo( $("#errorTip") );
			},
			rules : {
				ca_em_code : {required : true},
				ca_maxmoney : {required : true, number:true},
				ca_minrate : {required : true, range:[0,1]},
				ca_days:{required : true,digits:true}
			},
			messages : {
				ca_em_code : {required:"请选择员工"},
				ca_maxmoney : {required:"请输入最高让利",number:"只能为数字"},
				ca_minrate : {required:"请输入最低折扣"},
				ca_days:{required:"请输入零售查询天数",digits:"零售查询只能为正数"}
			}
		});
	});
</script>
<script src="<%=basePath%>data/sell/cashier/cashier_update.js"></script>
</body>
</html>