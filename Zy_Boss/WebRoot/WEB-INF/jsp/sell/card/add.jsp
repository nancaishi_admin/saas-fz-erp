<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
	table tr td b {color:red;}
	.list td{line-height:35px; }
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.extend.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.messages_zh.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<div class="border">
			<table width="100%">
				<tr class="list first">
					<td align="right" width="80px"><b>*</b>起始编号：</td>
					<td width="130px">
						<input class="main_Input" type="text" name="start_no" id="start_no" value="" maxlength="11"/>
					</td>
					<td align="right" width="80px"><b>*</b>发放日期：</td>
					<td>
						<input readonly type="text" class="main_Input Wdate"  name="cd_grantdate" id="cd_grantdate" onclick="WdatePicker()" value="" style="width:198px;"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right"><b>*</b>新增数量：</td>
					<td>
						<input class="main_Input" type="text" name="add_count" id="add_count" value="1"/>
					</td>
					<td align="right">有效日期：</td>
					<td>
						<input readonly type="text" class="main_Input Wdate"  name="cd_enddate" id="cd_enddate" onclick="WdatePicker()" value="" style="width:198px;"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">标识字符：</td>
					<td>
						<input class="main_Input" type="text" name="mark" id="mark" value="" style="width:100px;"/>
						<span id="span_prefix">
							<label class="radio" style="width:30px">
				  	 			<input name="redio1" type="radio" value="0" checked="checked"/>前置
					  	 	</label>
					  	 	<label class="radio" style="width:30px">
					  	 		<input name="redio1" type="radio" value="1" />后置
					  	 	</label>
							<input type="hidden" id="prefix" name="prefix" value="0"/>
						</span>
						
					</td>
					<td align="right"><b>*</b>发卡店铺：</td>
					<td>
						<input class="main_Input" type="text" readonly="readonly" name="sp_name" id="sp_name" value="" style="width:170px;"/>
						<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryShop();"/>
						<input type="hidden" name="cd_shop_code" id="cd_shop_code" value=""/>
					</td>
				</tr>
				<tr class="list">
					<td align="right"><b>*</b>初始金额：</td>
					<td>
						<input class="main_Input" type="text" name="cd_money" id="cd_money" value=""/>
					</td>
					<td align="right"><b>*</b>经办人员：</td>
					<td>
						<input class="main_Input" type="text" readonly="readonly" name="cd_manager" id="cd_manager" value="" style="width:170px;"/>
						<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryEmp();"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right"><b>*</b>实收现金：</td>
					<td>
						<input class="main_Input" type="text" name="cd_realcash" id="cd_realcash" value=""/>
					</td>
					<td align="right"><b>*</b>实收刷卡：</td>
					<td>
						<input class="main_Input" type="text" name="cd_bankmoney" id="cd_bankmoney" value=""/>
					</td>
				</tr>
				<tr class="list">
					<td align="right"><b>*</b>现金账户：</td>
					<td>
						<input class="main_Input" type="text" readonly="readonly" name="ba_name" id="ba_name" value="" style="width:170px;"/>
						<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryBank('cdl_ba_code','ba_name');"/>
						<input type="hidden" name="cdl_ba_code" id="cdl_ba_code" value=""/>
					</td>
					<td align="right"><b>*</b>刷卡账户：</td>
					<td>
						<input class="main_Input" type="text" readonly="readonly" name="bank_name" id="bank_name" value="" style="width:170px;"/>
						<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryBank('cdl_bank_code','bank_name');"/>
						<input type="hidden" name="cdl_bank_code" id="cdl_bank_code" value=""/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">用户姓名：</td>
					<td>
						<input class="main_Input" type="text" name="cd_name" id="cd_name" value=""/>
					</td>
					<td align="right">手机号码：</td>
					<td>
						<input class="main_Input" type="text" name="cd_mobile" id="cd_mobile" value=""/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">身份证号：</td>
					<td>
						<input class="main_Input" type="text" name="cd_idcard" id="cd_idcard" value=""/>
					</td>
				</tr>
				<tr class="list last">
					<td align="right"></td>
					<td id="errorTip" class="errorTip" colspan="3"></td>
				</tr>
			</table>
</div>
<div class="footdiv">
	<input class="ui-btn ui-btn-sp mrb" id="btn-save" name="btn-save" type="button" value="确定"/>
	<input class="ui-btn mrb" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
</form>
<script>
	$().ready(function() {
		$("#form1").validate({
			ignore: "",
			errorLabelContainer:"#errorTip",
			wrapper:"span",
			onfocusout : function(element) {
				$(element).valid();
			},
			showErrors : function(errors, errorList) {
				this.defaultShowErrors();
				$('#errorTip span:hidden').remove()// 删除所有隐藏的span标签
				$('#errorTip span:eq(0)').nextAll().remove()// 第一个span下所有跟随的同胞级span删除
			},
			rules : {
				start_no : {
					required : true,
					digits:true
				},
				add_count : {
					required : true,
					digits:true,
					min:1
				},
				cd_shop_code : {
					required : true
				},
				cd_manager : {
					required : true
				},
				cd_money : {
					required : true
				},
				cd_realcash : {
					required : true
				},
				cd_bankmoney : {
					required : true
				},
				cdl_ba_code : {
					required : true
				},
				cdl_bank_code : {
					required : true
				},
				cd_mobile : {
					isMobile : true
				}
			},
			messages : {
				start_no : {
					required : "请输入起始编号",
					digits : "请输入整数"
				},
				add_count : {
					required : "请输入新增数量",
					digits : "请输入正整数"
				},
				cd_shop_code : "请选择发放店铺",
				cd_manager : "请选择经办人",
				cd_money : "请输入面值",
				cd_realcash : "请输入实收现金",
				cd_bankmoney : "请输入实收刷卡",
				cdl_ba_code : "请选择现金账户",
				cdl_bank_code : "请选择刷卡账户",
				cd_mobile : "请输入正确的手机号码"
			}
		});
	});
</script>
<script src="<%=basePath%>data/sell/card/card_add.js"></script>
</body>
</html>