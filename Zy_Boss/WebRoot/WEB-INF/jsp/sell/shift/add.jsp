<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
	.list td{line-height:35px; }
	.errorTip{color:red;height:30px;font: 12px Microsoft Yahei;}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
</script> 
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<div class="border">
	<table width="100%">
				<tr class="list first">
					<td align="right" width="100px"><font color="red">*</font>所属店铺：</td>
					<td>
						<input class="main_Input" type="text" id="sp_name" name="sp_name" value="" readonly="readonly" style="width:170px; " />
						<input type="button" id="btn_shop" name="btn_shop" value="" class="btn_select"/>
						<input type="hidden" name="st_shop_code" id="st_shop_code" value=""/>
					</td>
				</tr>
				<tr class="list">
					<td align="right" width="100px"><font color="red">*</font>班次名称：</td>
					<td>
						<input class="main_Input" type="text" name="st_name" id="st_name" value="" maxlength="20"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right"><font color="red">*</font>上班时间：</td>
					<td>
						<input class="main_Input" type="text" id="st_begintime" name="st_begintime" value=""
			 				onclick="WdatePicker({isShowToday:false,dateFmt:'HH:mm',maxDate:'#F{$dp.$D(\'st_endtime\',{d:0})}'})"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right"><font color="red">*</font>下班时间：</td>
					<td>
						<input class="main_Input" type="text" id="st_endtime" name="st_endtime" value=""
							onclick="WdatePicker({isShowToday:false,dateFmt:'HH:mm',minDate:'#F{$dp.$D(\'st_begintime\',{d:0})}'})"/>
					</td>
				</tr>
				<tr class="list last">
					<td id="errorTip" class="errorTip" colspan="2" style="padding-left: 80px;"></td>
				</tr>
			</table>
</div>
</form>
<div class="footdiv">
	<input class="ui-btn ui-btn-sp mrb" id="btn-save" name="btn-save" type="button" value="确定"/>
	<input class="ui-btn mrb" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
<script>
	$().ready(function() {
		$("#form1").validate({
			ignore: "",
			onfocusout : function(element) {
				$(element).valid();
			},
			errorPlacement: function(error, element) { //错误信息位置设置方法
				element.focus();
				$("#errorTip").empty();
				error.appendTo( $("#errorTip") );
			},
			rules : {
				st_shop_code : "required",
				sp_name : "required",
				st_name : "required",
				st_begintime : "required",
				st_endtime : "required"
			},
			messages : {
				st_shop_code : "请输入所属店铺",
				sp_name : "请输入所属店铺",
				st_name : "请输入班次名称",
				st_begintime : "请选择上班时间",
				st_endtime : "请选择下班时间"
			}
		});
	});
</script>
<script src="<%=basePath%>data/sell/shift/shift_add.js"></script>
</body>
</html>