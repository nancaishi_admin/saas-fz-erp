<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Pragma" content="no-cache"/>
    <meta http-equiv="Cache-Control" content="no-cache"/>
    <meta http-equiv="Expires" content="0"/>
    <title>对比分析报表</title>
    <link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
	<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
	<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
	<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript">
    document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
    document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
    document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
    document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
    document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
    document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
    document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
    document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
    document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/comm-search.js\"></sc"+"ript>");
    </script>
</head>
<body>
<div class="mainwra">
    <div class="mod-search cf">
        <div class="fl">
        	<ul class="ul-inline">
	        <li>店铺名称：
	          	<input class="ui-input" style="width:174px;" type="text" readonly="readonly" name="shop_name" id="shop_name" value="" />
				<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryShop();"/>
				<input type="hidden" id="shopCode" name="shopCode" value=""/>
	        </li>
	        <li>年份：
	          	<span class="ui-combo-wrap" id="span_year"></span>
				<input type="hidden" name="year" id="year"/>
	        </li>
	        <li>月份：
	          	<span class="ui-combo-wrap" id="span_month"></span>
				<input type="hidden" name="month" id="month"/>
	        </li>
	      </ul>
        </div>
        <div class="fl-m">
        	<a class="ui-btn fl mrb" id="btn-search">查询</a>
        </div>
        <div class="fr">
        </div>
    </div>
    <div class="grid-wrap">
        <table id="grid">
        </table>
        <div id="page"></div>
    </div>
</div>
<script src="<%=basePath%>data/report/kpi_analysis_day.js"></script>
</body>
</html>