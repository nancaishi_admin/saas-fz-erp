<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
</script> 
</head>
<body>
<form method="post" action="" id="form1" name="form1">
	<input type="hidden" id="sp_id" name="sp_id" value="${model.sp_id}"/>
	<input type="hidden" id="spi_id" name="spi_id" value="${model.spi_id}"/>
	<input type="hidden" id="sp_init" name="sp_init" value="${model.sp_init}"/>
	<table class="table_add">
		<tr>
			<td width="60px" align="right"><font color="red">*</font>名称：
			</td>
			<td width="180px" >
				<input class="main_Input w146" type="text" name="sp_name" id="sp_name" value="${model.sp_name}" maxlength="20"/>
			</td>
			<td align="right" width="60px" ><b style="color:red;">*</b>类型：</td>
			<td width="180px">
				<input class="main_Input w146" type="text" name="ty_name" id="ty_name" readonly value="${model.ty_name}"/>
				<input type="hidden" name="sp_shop_type"  id="sp_shop_type" value="${model.sp_shop_type}"/>
			</td>  		 
		</tr>
	    <tr>
	    	<td align="right" >查看总仓： 
			</td>
			<td>
				<span class="ui-combo-wrap" id="spanMain"></span>
				<input type="hidden" name="sp_main" id="sp_main" value="${model.sp_main}"/>
			</td>
		  	<td align="right"><b style="color:red;">*</b>折扣：
		  	</td>
			<td>
		  	 	<input type="text" class="main_Input w146" name="sp_rate" id="sp_rate" maxlength=4 onkeyup="javascript:vadiUtil.vadiDouble(this);" value="${model.sp_rate}"/>
		  	</td>			
	     </tr>
	     <tr>
		    <td align="right"><font color="red">*</font>电话： 
		    </td>
			<td>
		    	<input class="main_Input w146" type="text" name="spi_tel" id="spi_tel" title="参照格式：0527-84380016" maxlength="15" value="${model.spi_tel}"/>
		    </td>
			<td align="right"><font color="red">*</font>联系人：
			</td>
			<td>
				<input class="main_Input w146" type="text" name="spi_man" id="spi_man" maxlength="16" value="${model.spi_man}"/>
			</td>
		</tr>
		<tr>
			<td align="right"><font color="red">*</font>手机：
			</td>
			<td>
				<input class="main_Input w146" type="text" name="spi_mobile"  id="spi_mobile" value="${model.spi_mobile}" maxlength="11" />
			</td>
			<td align="right">结账周期：
			</td>
			<td>
				<input class="main_Input w146" type="text" name="sp_settle_cycle" id="sp_settle_cycle" maxlength="3" onkeyup="javascript:vadiUtil.vadiNumber(this);" value="${model.sp_settle_cycle}"/>
			</td>
		</tr>
		<tr>
			<td align="right" >期初欠款： 
			</td>
			<td>
				<input type="text" class="main_Input w146" name="sp_init_debt"  id="sp_init_debt" value="${model.sp_init_debt}"
					${sessionScope.user.sp_init eq 1 ? 'disabled' : ''}/>
			</td>
			<td align="right">配送周期：
			</td>
			<td>
				<input class="main_Input w146" type="text" name="sp_sort_cycle" id="sp_sort_cycle" maxlength="3" onkeyup="javascript:vadiUtil.vadiNumber(this);" value="${model.sp_sort_cycle}"/>
			</td>
		</tr>
		<tr>
		  	<td align="right"><font color="red">*</font>地区：
		  	</td>
		  	<td colspan="3">
  				<span class="ui-combo-wrap" id="spanProvince"></span>&nbsp;
				<input type="hidden" class="main_Input w146" name="spi_province" id="spi_province" value="${model.spi_province}"/>
  				<span class="ui-combo-wrap" id="spanCity"></span>&nbsp;
				<input type="hidden" class="main_Input w146" name="spi_city" id="spi_city" value="${model.spi_city}"/>
  				<span class="ui-combo-wrap" id="spanTown"></span>&nbsp;
				<input type="hidden" class="main_Input w146" name="spi_town" id="spi_town" value="${model.spi_town}"/>
		  	</td>
		</tr>
		<tr>
			<td align="right">地址：
			</td>
			<td colspan="3">
				<input class="main_Input" type="text" name="spi_addr" id="spi_addr" style="width:395px" value="${model.spi_addr}"/>
			</td>
		</tr>
		<tr>
			<td align="right">备注：
			</td>
		  	<td colspan="3">
				<input class="main_Input" type="text" name="spi_remark" id="spi_remark" value="" style="width:395px" value="${model.spi_remark}"/>
			</td>
		</tr>
	</table>
</form>
<div class="footdiv">
	<input class="btn_confirm" type="button" id="btn-save" name="btn-save" value="确定"/>
	<input class="btn_close" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
<script src="<%=basePath%>data/base/shop/shop_update.js"></script>
</body>
</html>
