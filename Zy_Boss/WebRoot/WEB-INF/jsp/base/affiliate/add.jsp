<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
	.list td{line-height:35px; }
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
</script> 
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" id="af_upcode" name="af_upcode" value=""/>
<div class="border">
	<table width="100%">
				<tr class="list first">
					<td align="right" width="80px">分公司名称：</td>
					<td>
						<input class="main_Input" type="text" name="af_name" id="af_name" value="" maxlength="30"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">联系人：</td>
					<td>
						<input class="main_Input" type="text" name="af_man" id="af_man" value="" maxlength="20"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">联系电话：</td>
					<td>
						<input class="main_Input" type="text" name="af_tel" id="af_tel" value="" maxlength="13"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">手机号码：</td>
					<td>
						<input class="main_Input" type="text" name="af_mobile" id="af_mobile" value="" maxlength="13"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">联系地址：</td>
					<td>
						<input class="main_Input" type="text" name="af_addr" id="af_addr" value="" maxlength="50"/>
					</td>
				</tr>
				<tr class="list last">
					<td align="right">备注：</td>
					<td>
						<input class="main_Input" type="text" name="af_remark" id="af_remark" value="" maxlength="100"/>
					</td>
				</tr>
			</table>
</div>
</form>
<div class="footdiv">
	<input class="ui-btn ui-btn-sp mrb" id="btn-save" name="btn-save" type="button" value="确定"/>
	<input class="ui-btn mrb" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
<script src="<%=basePath%>data/base/affiliate/affiliate_add.js"></script>
</body>
</html>