<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>导航页</title>
    <meta http-equiv=Content-Type content="text/html; charset=utf-8"/>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
	table tr td b {color:red;}
	.list td{line-height:32px; }
	.main_Input{width: 225px; height: 16px;}
	.width100{width:100px;}
	.mutab {
	    height: 32px;
	    margin: 10px 0 0 11px;
	}
	ul {
	    list-style: none;
	}
	.mutab li {
	    cursor: pointer;
	    display: inline;
	    float: left;
	    margin-right: 5px;
	}
	.select {
	    background: #fff;
	    border-style: solid;
	    border-width: 2px 1px 1px;
	    border-color: #08c #ddd #ddd;
	    height: 30px;
	    text-align: center;
	    line-height: 30px;
	    width: 120px;
	    color: #08c;
	    font-weight: 700;
	    cursor: hand;
	}
	.menu {
	    background: #f1f1f1;
	    border-style: solid;
	    border-width: 1px 1px 0;
	    border-color: #ddd;
	    height: 30px;
	    text-align: center;
	    line-height: 30px;
	    width: 120px;
	    cursor: hand;
	}
	.content {
	    border: #ddd 1px solid;
	    height: 460px;
	    width: 98%;
	    background-color: #FFFFFF;
	    margin: 0 0 0 10px;
	}
	.topdiv{
	    bottom: 0;
	    height: 28px;
	    right: 0;
	    padding-top:-40px;
	    right: 0;
	    text-align: right;
	}
	.paycon {
	    position: absolute;
	    top: 52px;
	    border: #ddd solid 1px;
	    left: 0px;
	    right: 0px;
	    bottom: 15px;
	    overflow-x: hidden;
	    overflow-y: auto;
	}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.form.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/UpImgPreview.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/DialogSelect.js\"></sc"+"ript>");
</script>
</head>
<body>
<form name="form1" method="post" action="" id="form1">
<div class="border">
	<div>
		<c:choose>
			<c:when test="${sessionScope.user.shoptype eq 3 }">
			</c:when>
			<c:otherwise>
				<table width="100%" cellspacing="0" cellpadding="0">
				    <tr>
				      <td class="top-b border-b">
				      </td>
				      <td class="top-b border-b" width="80" align="right">
				         <a class="t-btn btn-red" id="btn-save" onclick="javascript:Utils.save(this);">保存</a>
				       </td>
				    </tr>
			    </table>
			</c:otherwise>
		</c:choose>
		<div class="paycon" id="tagContent1">
			<input type="hidden" id="spl_id" name="spl_id" value="${shopLine.spl_id }"/>
			<table width="100%">
				<tr class="list first">
					<td align="right" width="120">店铺名称：</td>
					<td>
						<input class="main_Input" type="text" value="${shopLine.sp_name }" disabled="disabled"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right" width="100">联系人：</td>
					<td>
						<input class="main_Input" type="text" value="${shopLine.spi_man }" disabled="disabled"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">手机号码：</td>
					<td>
						<input class="main_Input" type="text" value="${shopLine.spi_mobile }" disabled="disabled"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">店铺电话：</td>
					<td>
						<input class="main_Input" type="text" value="${shopLine.spi_tel }" disabled="disabled"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">详细地址：</td>
					<td>
						<input type="hidden" id="area" value="${shopLine.spi_province }${shopLine.spi_city }${shopLine.spi_town }"/>
						<input class="main_Input" type="text" value="${shopLine.spi_addr }" disabled="disabled"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right"><b>*</b>收单门店：</td>
					<td>
						<c:choose>
							<c:when test="${ shopLine.sp_shop_type eq '1' or shopLine.sp_shop_type eq '2'}">
								<input class="main_Input" type="text" readonly="readonly" name="ordershop_name" id="ordershop_name" value="${shopLine.ordershop_name }" style="width:200px"/>
								<input type="button" value="" class="btn_select" onclick="javascript:DialogSelect.doQueryShop(false,'spl_ordershop_code','ordershop_name','sub');"/>
								<input type="text" name="spl_ordershop_code" id="spl_ordershop_code" value="${shopLine.spl_ordershop_code }" />
							</c:when>
							<c:otherwise>
								<input class="main_Input" type="text" name="ordershop_name" id="ordershop_name" value="${shopLine.ordershop_name }" maxlength="50"/>
								<input type="text" name="spl_ordershop_code" id="spl_ordershop_code" value="${shopLine.spl_ordershop_code }"/>
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
				<tr class="list">
					<td align="right"><b>*</b>主营项目：</td>
					<td>
						<input class="main_Input" type="text" name="spl_project" id="spl_project" value="${shopLine.spl_project }" maxlength="50"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right"><b>*</b>营业时间：</td>
					<td>
						<input class="main_Input" type="text" name="spl_office_hour" id="spl_office_hour" value="${shopLine.spl_office_hour }" maxlength="50"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right"><b>*</b>店铺LOGO：</td>
					<td>
						<input class="main_Input" type="file" name="img_logo" id="img_logo" />
						<span style="color:red;">【建议尺寸720*360，大小不要超过200K。】</span>
						<c:choose>
							<c:when test="${ !empty shopLine.spl_logo}">
								<img id="imgview" style="display:inherit;" width="320" height="160" 
									src="<%=serverPath%>${shopLine.spl_logo}"/>
							</c:when>
							<c:otherwise>
								<img id="imgview" style="display:inherit;" width="320" height="160" />
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
				<tr class="list">
					<td align="right"><b>*</b>地图标注：</td>
					<td>
						<div id="allmap" style="width:762px;height:460px;"></div>
						<input type="text" id="spl_longitude" name="spl_longitude" value=""/>
						<input type="text" id="spl_latitude" name="spl_latitude" value=""/>
					</td>
				</tr>
			</table>
			
		</div>
		
	</div>
</div>
</form>
<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=GTUFwLfW2e74CPmPd7OvEDTeBQsvjx1K"></script>
<script src="<%=basePath%>data/base/shopline/shop_line.js"></script>
<script>
function initMap(){
	$("#allmap").height(460);
	$("#allmap").width(762);
	
	//百度地图API功能
	var map = new BMap.Map("allmap");
	map.enableScrollWheelZoom(true);
	
	var myCity = new BMap.LocalCity();
	myCity.get(function(result){
		var point = new BMap.Point(result.center.lng,result.center.lat);
		map.centerAndZoom(point,13);
	});
	
	map.addEventListener("click",function(e){
		map.clearOverlays();
		var point = new BMap.Point(e.point.lng,e.point.lat);
		var marker = new BMap.Marker(point);
		map.addOverlay(marker);
		$("#spl_longitude").val(e.point.lng);
		$("#spl_latitude").val(e.point.lat);
	});
	 map.centerAndZoom($("#area").val());
}
initMap();
</script>
</body>
</html>

