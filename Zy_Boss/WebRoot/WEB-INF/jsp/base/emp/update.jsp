<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
	.list td{line-height:35px; }
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
</script> 
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<div class="border">
	<table style="width:100%;height:260;"  class="pad_t20" border="0">
  		<tr>
   			<td width="80px" style="padding-top:3px;">
   				<div align="right">编号:</div>
   			</td>
    		<td width="155px">
    			<input class="main_Input"  style="width:150px" type="text" disabled name="em_code_disabled" id="em_code_disabled" value="${emp.em_code}" />
    		</td>
    		<td width="65px">
    			<div align="right"><font color="red">*</font>姓名:</div>
    		</td>
    		<td>
    			<input class="main_Input" type="text"  style="width:150px" id="em_name" name="em_name" value="${emp.em_name}" maxlength=5/>
    		</td>
  		</tr>
  		<tr>
   			<td width="80px" style="padding-top:3px;">
   				<div align="right">性别:</div>
   			</td>
    		<td width="155px">
    			<span class="ui-combo-wrap" id="span_Sex"></span>
				<input type="hidden" name="em_sex" id="em_sex" value="${emp.em_sex}"/>
    		</td>
    		<td width="65px">
    			<div align="right">出生日期:</div>
    		</td>
    		<td>
    			<input readonly type="text" class="main_Input Wdate" name="em_birthday" id="em_birthday" onclick="WdatePicker();"  value="${emp.em_birthday}" style="width:150px">
    		</td>
  		</tr>
  		<tr>
   			<td width="80px" style="padding-top:3px;">
   				<div align="right">婚姻状况:</div>
   			</td>
    		<td width="155px">
    			<span class="ui-combo-wrap" id="span_Marry"></span>
				<input type="hidden" name="em_marry" id="em_marry" value="${emp.em_marry}"/>
    		</td>
    		<td width="65px">
    			<div align="right">证件号码:</div>
    		</td>
    		<td>
    			<input class="main_Input" type="text"  id="em_cardid" name="em_cardid" value="${emp.em_cardid}" maxlength="18" style="width:150px" onblur="javascript:handle.isIdCardNo(this);" />
    		</td>
  		</tr>
  		<tr>
   			<td width="80px" style="padding-top:3px;">
   				<div align="right">家庭住址:</div>
   			</td>
    		<td width="155px">
    			<input class="main_Input" type="text" name="em_addr" id="em_addr" value="${emp.em_addr}" style="width:150px;"/>
    		</td>
    		<td width="65px">
    			<div align="right">员工类型:</div>
    		</td>
    		<td>
    			<span class="ui-combo-wrap" id="span_Type"></span>
				<input type="hidden" name="em_type" id="em_type" value="${emp.em_type}"/>
    		</td>
  		</tr>
  		<!-- <tr id="Em_Pass_Tr">
    		<td>
    			<div align="right">导购密码:</div>
   			</td>
    		<td>
    			<input class="main_Input" type="password" name="em_pass" id="em_pass" value="123456" maxlength=15 title="不能超过十五个字符" style="width:150px"/>
    		</td>
    		<td colspan="2">
    			&nbsp;&nbsp;&nbsp;&nbsp;密码未填写，默认：<b>123456</b>
    		</td>
  		</tr> -->
  		<tr>
    		<td>
    			<div align="right">手机:</div>
    		</td>
    		<td>
    			<input class="main_Input" type="text" name="em_mobile" id="em_mobile" maxlength ="11" value="${emp.em_mobile}" onblur="javascript:handle.checkMobile(this);" style="width:150px"/>
    		</td>
    		<td>
    			<div align="right"> 提成率:
    		</td>
    		<td>
    			<input class="main_Input" type="text"  name="em_royaltyrate" id='em_royaltyrate' value="${emp.em_royaltyrate}" title="请填写0-1之间的数据" onkeyup="javascript:handle.doOnlyDouble(this);" onkeydown="javascript:handle.doOnlyDouble(this);" style="width:150px"/>
    		</td>
  		</tr>
  		<tr>
    		<td align="right">
    			<font color="red">*</font>所属部门:
    		</td>
	 		<td >
		 		<span class="ui-combo-wrap" id="span_em_dm_Code"></span>
				<input type="hidden" name="em_dm_code" id="em_dm_code" value="${emp.em_dm_code}"/>
    		</td>
    		<td>
    			<div align="right"><font color="red">*</font>所属门店:</div>
    		</td>
    		<td>
    			<span class="ui-combo-wrap" id="span_em_shop_Code">
				</span>
				<input type="hidden" name="em_shop_code" id="em_shop_code" value="${emp.em_shop_code}"/>
    		</td>
  		</tr>
  		<tr>
    		<td>
    			<div align="right">状态:</div>
    		</td>
    		<td>
    			<span class="ui-combo-wrap" id="span_State"></span>
				<input type="hidden" name="em_state" id="em_state" value="${emp.em_state}"/>
    		</td>
    		<td>
    			<div align="right">进店时间:</div>
    		</td>
    		<td>
    			<input readonly type="text" class="main_Input Wdate" name="em_indate" id="em_indate" onclick="WdatePicker();"  value="${emp.em_indate}" style="width:150px">
    		</td>
  		</tr>
  		<tr>
    		<td>
    			<div align="right">工作时间:</div>
    		</td>
    		<td>
    			<input readonly type="text" class="main_Input Wdate" name="em_workdate" id="em_workdate" onclick="WdatePicker();"  value="${emp.em_workdate}" style="width:150px">
    		</td>
    		<td>
    			<div align="right">离店时间:</div>
    		</td>
    		<td>
    			<input readonly type="text" class="main_Input Wdate"  readonly="readonly" name="em_outdate" id="em_outdate" onclick="WdatePicker();" value="${emp.em_outdate}" style="width:150px">
    		</td>
  		</tr>
</table>
</div>
<input type="hidden" id="em_id" name="em_id" value="${emp.em_id}"/>
<input type="hidden" id="em_code" name="em_code" value="${emp.em_code}"/>
<input type="hidden" id="sex" value=""/>
<input type="hidden" id="marray" value=""/>
<input type="hidden" id="em_type_name" name="em_type_name" value=""/>
<input type="hidden" id="em_dm_name" name="em_dm_name" value=""/>
<input type="hidden" id="em_shop_name" name="em_shop_name" value=""/>
<input type="hidden" id="em_state_name" name="em_state_name" value=""/>
</form>
<div class="footdiv">
	<input class="ui-btn ui-btn-sp mrb" id="btn-save" name="btn-save" type="button" value="确定"/>
	<input class="ui-btn mrb" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
<script src="<%=basePath%>data/base/emp/emp_update.js"></script>
</body>
</html>