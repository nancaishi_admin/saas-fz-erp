<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/css/base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
	table tr td b {color:red;}
	.list td{line-height:32px; }
	.main_Input{width: 130px; height: 16px;}
	.width100{width:100px;}
	.mutab {
	    height: 32px;
	    margin: 10px 0 0 11px;
	}
	ul {
	    list-style: none;
	}
	.mutab li {
	    cursor: pointer;
	    display: inline;
	    float: left;
	    margin-right: 5px;
	}
	.select {
	    background: #fff;
	    border-style: solid;
	    border-width: 2px 1px 0;
	    border-color: #08c #ddd #ddd;
	    height: 30px;
	    text-align: center;
	    line-height: 30px;
	    width: 120px;
	    color: #08c;
	    font-weight: 700;
	    cursor: hand;
	}
	.menu {
	    background: #f1f1f1;
	    border-style: solid;
	    border-width: 1px 1px 0;
	    border-color: #ddd;
	    height: 30px;
	    text-align: center;
	    line-height: 30px;
	    width: 120px;
	    cursor: hand;
	}
	.content {
	    border: #ddd 1px solid;
	    height: 460px;
	    width: 98%;
	    background-color: #FFFFFF;
	    margin: 0 0 0 10px;
	}
	.error{
		color:red;
	}
	.opselect{
		border:#ddd solid 1px;
		height:25px;
		line-height:25px;
		padding:0 0 0 5px;
		width:80px;
	}
	.btn_copy{
		background:#f60;
		border:none;
		color:#fff;
		font-weight:700;
		height:30px;
		margin-left:10px;
		overflow:hidden;
		padding:0;
		width:80px;
	}
	.btn_adds{
		background:#08b;
		border:none;
		color:#fff;
		font-weight:700;
		height:26px;
		margin-left:10px;
		overflow:hidden;
		padding:0;
		width:55px;
	}
	.btn_icon{
	    background: #ddd no-repeat scroll;
	    border: 0 none;
	    color: #333;
	    cursor: pointer;
	    height: 27px;
	    margin: 3px;
	    width: 47px;
	}
	.btn_icon:hover {
	    background: #c2c2c2 no-repeat scroll;
	    color: #3366cc;
	}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.extend.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.messages_zh.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/lundarDate.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/change.js\"></sc"+"ript>");
</script>
<script>
var api = frameElement.api, W = api.opener;
var _callback = api.data.callback;
function doClose(){
	api.close();
}
function selContent(showId){
	$("#menu1").removeClass().addClass("menu");
	$("#menu2").removeClass().addClass("menu");
	/* $("#menu3").removeClass().addClass("menu"); */
	$("#menu"+showId).removeClass().addClass("select");
	$("#tagContent1").hide();
	$("#tagContent2").hide();
	/* $("#tagContent3").hide(); */
	$("#tagContent"+showId).show();
}
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<div class="mainwra" style="min-width:900px;overflow:hidden;">
	<div>
	<table width="100%" cellspacing="0" cellpadding="0">
	    <tr>
	      <td class="top-b border-b">
	        <input id="btn-save" type="button" class="t-btn btn-red" value="保存"/>
	        <input id="btn_close" class="t-btn" type="button" value="返回"/>
	      </td>
	    </tr>
    </table>
	<div class="mutab">
		<ul>
			<li id="menu1" class="select" onclick="selContent('1')">基本属性</li>
			<li id="menu2" class="menu" onclick="selContent('2')" >辅助属性</li>
			<!-- <li id="menu3" class="menu" onclick="selContent('3')" >条形码</li> -->
		</ul>
		<input type="hidden" id="pd_state" name="pd_state" value="0"/>
		<span style="color:red;margin-left: 20px;height:30px;font: 12px Microsoft Yahei;" id="errorTip"></span>
	</div>
	<div id="tagContent" style="height:400px;width:1100px;border:0px;margin:0px;">
		<div class="content" id="tagContent1" style="width:1100px;display:block;height:400px;">
			<table style="border:0px;margin-top:0px;margin-left:10px;padding:0px;float:left">
				<tr class="list first">
					<td align="right" width="80px"><b>*</b>商品货号：</td>
					<td width="130px">
						<input class="main_Input required" type="text" name="pd_no_dis" id="pd_no_dis" value="${product.pd_no }" disabled="disabled" onkeyup="value=value.replace(/[^\w\.\-\/]/ig,'')" maxlength="15"/>
						<input type="hidden" id="pd_id" name="pd_id" value="${product.pd_id }"/>
						<input type="hidden" id="pd_code" name="pd_code" value="${product.pd_code }"/>
						<input type="hidden" id="pd_no" name="pd_no" value="${product.pd_no }"/>
					</td>
					<td align="right" width="80px">厂家款号：</td>
					<td width="130px">
						<input class="main_Input required" type="text" name="pd_number" id="pd_number" value="${product.pd_number }" maxlength="15"/>
					</td>
					<td align="right" width="80px">配送折扣：</td>
					<td width="130px">
						<input class="main_Input" type="text" id="sort_discount" value="" name="sort_discount" onpropertychange="javascript:handle.changeSorthPrice()"/>
					</td>
					<td align="right" width="80px">配送价：</td>
					<td width="130px">
						<input class="main_Input" type="text"  name="pd_sort_price" id="pd_sort_price" onkeyup="if(isNaN(value))execCommand('undo')" onafterpaste="if(isNaN(value))execCommand('undo')"/>
						<input type="hidden" name="pd_sort_price_hid" id="pd_sort_price_hid" value="${product.pd_sort_price }"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right" width="80px"><b>*</b>商品品牌：</td>
					<td>
						<input class="main_Input" type="text" name="pd_bd_name" id="pd_bd_name" value="${product.pd_bd_name }" style="width:105px;" readonly="readonly"/>
						<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryBrand();"/>
						<input class="main_Input" type="hidden" name="pd_bd_code" id="pd_bd_code" value="${product.pd_bd_code }"/>
					</td>
					<td align="right"><b>*</b>商品名称：</td>
					<td>
						<input class="main_Input required" type="text" name="pd_name" id="pd_name" value="${product.pd_name }" maxlength="15"/>
					</td>
					<td align="right">导购提成额：</td>
					<td>
						<input class="main_Input" type="text" id="pd_commission" value="${product.pd_commission }" name="pd_commission" />
					</td>
					<td align="right">导购提成率：</td>
					<td>
						<input class="main_Input" type="text" id="pd_commission_rate" value="${product.pd_commission_rate }" name="pd_commission_rate" />
					</td>
				</tr>
				<tr class="list">
					<td align="right"><b>*</b>商品分类：</td>
					<td>
						<input class="main_Input" type="text" name="pd_tp_name" id="pd_tp_name" value="${product.pd_tp_name }" style="width:105px;" readonly="readonly"/>
						<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryType();"/>
						<input class="main_Input" type="hidden" name="pd_tp_code" id="pd_tp_code" value="${product.pd_tp_code }"/>
						<input class="main_Input" type="hidden" name="pd_tp_upcode" id="pd_tp_upcode" value="${product.pd_tp_upcode }"/>
					</td>
					<td align="right"><b>*</b>商品款式：</td>
					<td>
						<span class="ui-combo-wrap" id="span_style">
						</span>
						<input type="hidden" name="pd_style" id="pd_style" value="${product.pd_style }"/>
					</td>
					<td align="right">批发折扣：</td>
					<td>
						<input class="main_Input" type="text" name="BatchDiscount" id="BatchDiscount" value="" onkeyup="if(isNaN(value))execCommand('undo');handle.changeBatchPrice();" onafterpaste="if(isNaN(value))execCommand('undo')" onpropertychange="javascript:handle.changeBatchPrice();"/>
					</td>
					<td align="right">批发价：</td>
					<td>
						<input class="main_Input" type="text" name="pd_batch_price" id="pd_batch_price" onkeyup="if(isNaN(value))execCommand('undo')" onafterpaste="if(isNaN(value))execCommand('undo')"/>
						<input type="hidden" name="pd_batch_price_hid" id="pd_batch_price_hid" value="${product.pd_batch_price }"/>
					</td> 
				</tr>
				<tr class="list">
					<td align="right">商品单位：</td>
					<td>
						<span class="ui-combo-wrap" id="span_unit">
						</span>
						<input type="hidden" name="pd_unit" id="pd_unit" value="${product.pd_unit }"/>
					</td>
					<td align="right">供货单位：</td>
					<td>
						<input class="main_Input" type="text" name="pd_sp_name" id="pd_sp_name" value="${product.pd_sp_name }" style="width:105px;" readonly="readonly"/>
						<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQuerySupply();"/>
						<input class="main_Input" type="hidden" name="pd_sp_code" id="pd_sp_code" value="${product.pd_sp_code }"/>
					</td>
					<td align="right">积分兑换：</td>
            		<td>
            			<input class="main_Input" type="text" id="pd_score" name="pd_score" value="${product.pd_score }" onkeyup="if(isNaN(value))execCommand('undo')" onafterpaste="if(isNaN(value))execCommand('undo')"/>
            		</td>
            		<td align="right"><b>*</b>尺码组：</td>
      				<td>
      					<input class="main_Input" type="text" name="pd_szg_name" id="pd_szg_name" value="${product.pd_szg_name }" style="width:105px;" readonly="readonly"/>
						<input type="button" value="" class="btn_select" onclick="javascript:handle.sizeOptionChange('${product.pd_szg_code }','${product.pd_code}');" />
						<input class="main_Input" type="hidden" name="pd_szg_code" id="pd_szg_code" value="${product.pd_szg_code }"/>
      				</td>
				</tr>
				<tr class="list">
					<td align="right">上市日期：</td>
					<td>
						<input readonly type="text" class="main_Input Wdate" name="pd_date" id="pd_date" onclick="WdatePicker()"  value="${product.pd_date }" style="width:130px;"/>
					</td>
					<td align="right">退换货日期：</td>
					<td>
						<input readonly type="text" class="main_Input Wdate" name="pd_returndate" id="pd_returndate" onclick="WdatePicker()"  value="${product.pd_returndate }" style="width:130px;"/>
					</td>
					<td align="right">是否文胸：</td>
					<td>
						<span class="ui-combo-wrap" id="span_bra" style="border-color: red;">
						</span>
						<input type="hidden" name="pd_bra" id="pd_bra" value="${product.pd_bra }"/>
					</td>
					<td align="right">文胸杯型：</td>
					<td>
						<input class="main_Input" type="text" name="pdb_br_name" id="pdb_br_name" value="${product.pdb_br_name }" style="width:105px;border-color: red;" readonly="readonly"/>
						<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryBra();" style="border-color: red;"/>
						<input class="main_Input" type="hidden" name="pdb_br_codes" id="pdb_br_codes" value="${product.pdb_br_codes }"/>
						<input class="main_Input" type="hidden" name="pdb_br_codes_bak" id="pdb_br_codes_bak" value="${product.pdb_br_codes }"/>
					</td>
    			</tr>
    			<tr class="list">
    				<td align="right">进货周期：</td>
      				<td>
      					<input class="main_Input" type="text" name="pd_buy_cycle" id="pd_buy_cycle" value="${product.pd_buy_cycle }" title="格式：10" />
      				</td>
      				<td align="right"><b>*</b>零售周期：</td>
      				<td>
      					<input class="main_Input" type="text" name="pd_sell_cycle" id="pd_sell_cycle" value="${product.pd_sell_cycle }" title="格式：90" />
      				</td>
      				<td	colspan="4" rowspan="6">
      					<table style=" height:180px;margin-left:20px;float:left;margin-top:0px"  border="0px">
      						<tr style="line-height:40px;">
				                  <td>颜色查询：<input class="main_Input" type="text" id="colorSearch" name="colorSearch" title="可根据颜色名称或拼音简码查询颜色" style="width:50px;" onkeyup="javascript:handle.showColor();"/></td>
				                  <td><input type="button" onclick="javascript:Utils.addColor(this);" class="btn_icon" id="" value="添加"/></td>
				                  <td><b>*</b>已选颜色列表：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
				              </tr>
				              <tr height="160px;">
				              	<td align="left" style="width:140px">
				                  <select size="10" name="left_item" id="color_list" style="width:140px;height:160px;" multiple="multiple" ondblclick="change(form1.left_item, form1.right_item)">
				          			
				                  </select>
				              	</td>
				              	<td align="left">
				              		<div style="padding:10px;margin-top: 30px;">
										<input id="btn-right" name="btn-right" type="button" class="btn_icon" value=" &gt; " onClick="change(form1.left_item, form1.right_item)"/><p></p>
										<input id="btn-rightAll" name="btn-rightAll" type="button" class="btn_icon" value=" &gt;&gt; " onClick="changeAll(form1.left_item, form1.right_item)"/><p></p>
										<input id="btn-left" name="btn-left" type="button" class="btn_icon" value=" &lt; " onClick="javascript:handle.colorOptionChange(form1.right_item,form1.left_item,'${product.pd_code}')"/><p></p>
										<input name="btn-leftAll" id="btn-leftAll" type="button" class="btn_icon" value=" &lt;&lt; " onClick="javascript:handle.colorOptionChangeAll(form1.right_item,form1.left_item,'${product.pd_code}')"/><p></p>
									</div>
				              	</td>
				              	<td align="left">
				                	<select size="10" id="color_select" name="right_item" style="width:140px;height:160px;" ondblclick="javascript:handle.colorOptionChange(form1.right_item,form1.left_item,'${product.pd_code}')" multiple="multiple">
				                		<c:forEach items="${product_colors }" var="product_color" varStatus="status">
				                			<option value="${product_color.cr_code }">(${product_color.cr_code })${product_color.cr_name }</option>
				                		</c:forEach>
				                	</select>
				                	<input type="hidden" id="pdc_cr_codes" name="pdc_cr_codes" value="${product.pdc_cr_codes }"/>
				                	<input type="hidden" id="pdc_cr_codes_bak" name="pdc_cr_codes_bak" value="${product.pdc_cr_codes }"/>
				              	</td>
				            </tr>
      					</table>
      				</td>
			    </tr>
    			<tr class="list">
    				<td align="right">商品季节：</td>
					<td>
						<span class="ui-combo-wrap" id="span_season">
						</span>
						<input type="hidden" name="pd_season" id="pd_season" value="${product.pd_season }"/>
					</td>
					<td align="right">商品年份：</td>
					<td>
						<span class="ui-combo-wrap" id="span_year">
						</span>
						<input type="hidden" name="pd_year" id="pd_year" value="${product.pd_year }"/>
					</td>
    			</tr>
    			<tr class="list">
    				<td align="right">商品面料：</td>
					<td>
						<span class="ui-combo-wrap" id="span_fabric">
						</span>
						<input type="hidden" name="pd_fabric" id="pd_fabric" value="${product.pd_fabric }"/>
					</td>
					<td align="right">商品里料：</td>
      				<td >
      					<input class="main_Input" type="text" name="pd_in_fabric" id="pd_in_fabric" value="${product.pd_in_fabric }"/>
      				</td>
    			</tr>
   				<tr class="list">
   					<td align="right">进价折扣：</td>
      				<td>
      					<input class="main_Input" type="text" id="discount" value="" name="discount" onpropertychange="javascript:handle.changeInPrice()"/>
      				</td>
      				<td align="right">标牌价：</td>
			       	<td>
			       		<input class="main_Input" type="text" name="pd_sign_price" id="pd_sign_price" onkeyup="if(isNaN(value))execCommand('undo');handle.changePrice();" onafterpaste="if(isNaN(value))execCommand('undo')"/>
			       		<input type="hidden" name="pd_sign_price_hid" id="pd_sign_price_hid" value="${product.pd_sign_price }" />
			       	</td>
   				</tr>
   				<tr class="list">
					<td align="right">会员价：</td>
      				<td>
      					<input class="main_Input" type="text" name="pd_vip_price" id="pd_vip_price" onkeyup="if(isNaN(value))execCommand('undo')" onafterpaste="if(isNaN(value))execCommand('undo')"/>
      					<input type="hidden" name="pd_vip_price_hid" id="pd_vip_price_hid" value="${product.pd_vip_price }" />
      				</td>
      				<td align="right">零售价：</td>
			      	<td>
			      		<input class="main_Input" type="text" name="pd_sell_price" id="pd_sell_price" onkeyup="if(isNaN(value))execCommand('undo');handle.changeRetailPrice();" onafterpaste="if(isNaN(value))execCommand('undo')" />
			      		<input type="hidden" name="pd_sell_price_hid" id="pd_sell_price_hid" value="${product.pd_sell_price }" />
			      	</td>
   				</tr>
   				<tr class="list">
   					<td align="right">成本价：</td>
      				<td >
      					<input class="main_Input" type="text" name="pd_cost_price" id="pd_cost_price" onkeyup="if(isNaN(value))execCommand('undo')" onafterpaste="if(isNaN(value))execCommand('undo')"/>
      					<input type="hidden" name="pd_cost_price_hid" id="pd_cost_price_hid" value="${product.pd_cost_price }" />
      				</td>
      				<td align="right">进价：</td>
      				<td>
      					<input class="main_Input" type="text" name="pd_buy_price" id="pd_buy_price" onkeyup="if(isNaN(value))execCommand('undo');$('#pd_cost_price').val(this.value)" onafterpaste="if(isNaN(value))execCommand('undo')"/>
      					<input type="hidden" name="pd_buy_price_hid" id="pd_buy_price_hid" value="${product.pd_buy_price }" />
      				</td>
   				</tr>
			</table>
		</div>
		<div class="content" id="tagContent2" style="border-top:none;width:1100px; height:400px; display:none">
			<table class="pad_t20" border="0" style="margin-left:55px">
				<tr class="list first">
					<td align="right" width="90px">规格：</td>
					<td width="130px">
						<input class="main_Input required" type="text" name="pd_size" id="pd_size" value="${product.pd_size }"/>
					</td>
					<td align="right" width="90px">安全标准：</td>
					<td width="130px">
						<input class="main_Input required" type="text" name="pd_safe" id="pd_safe" value="${product.pd_safe }"/>
					</td>
					<td align="right" width="90px">产地：</td>
					<td width="130px">
						<input class="main_Input required" type="text" name="pd_place" id="pd_place" value="${product.pd_place }" />
					</td>
				</tr>
				<tr class="list">
					<td align="right">成份：</td>
					<td>
						<input class="main_Input" type="text" name="pd_fill" id="pd_fill" value="${product.pd_fill }"/>
					</td>
					<td align="right">执行标准：</td>
					<td>
						<input class="main_Input" type="text" name="pd_execute" id="pd_execute" value="${product.pd_execute }"/>
					</td>
      				<td align="right">等级：</td>
      				<td >
      					<input class="main_Input" type="text" name="pd_grade" id="pd_grade" value="${product.pd_grade }"/>
      				</td>
    			</tr>
    			<tr class="list">
					<td align="right">型号：</td>
					<td>
						<input class="main_Input" type="text" name="pd_model" id="pd_model" value="${product.pd_model }"/>
					</td>
					<td align="right">相色：</td>
					<td>
						<input class="main_Input" type="text" name="pd_color" id="pd_color" value="${product.pd_color }"/>
					</td>
      				<td align="right">价格特性：</td>
					<td>
						<span class="ui-combo-wrap" id="span_price">
						</span>
						<input type="hidden" name="pd_price_name" id="pd_price_name"  value="${product.pd_price_name }"/>
					</td>
    			</tr>
				<tr class="list">
					<td align="right">业务提成金额：</td>
					<td>
						<input class="main_Input" type="text" id="pd_salesman_comm" value="${product.pd_salesman_comm }" name="pd_salesman_comm" />
					</td>
					<td align="right">业务提成率：</td>
					<td>
						<input class="main_Input" type="text" id="pd_salesman_commrate" value="${product.pd_salesman_commrate }" name="pd_salesman_commrate" />
					</td>
					<td align="right">手动打折：</td>
      				<td>
      					<span class="ui-combo-wrap" id="span_sale">
						</span>
						<input type="hidden" name="pd_sale" id="pd_sale" value="${product.pd_sale }"/>
      				</td>
    			</tr>
    			<tr class="list">
    				<td align="right">分店变价：</td>
      				<td >
      					<span class="ui-combo-wrap" id="span_change">
						</span>
						<input type="hidden" name="pd_change" id="pd_change" value="${product.pd_change }"/>
      				</td>
      				<td align="right">是否赠品：</td>
      				<td>
      					<span class="ui-combo-wrap" id="span_gift">
						</span>
						<input type="hidden" name="pd_gift" id="pd_gift" value="${product.pd_gift }"/>
      				</td>
      				<td align="right">批发价1：</td>
      				<td >
      					<input class="main_Input" type="text"  name="pd_batch_price1" id="pd_batch_price1" onkeyup="if(isNaN(value))execCommand('undo')" onafterpaste="if(isNaN(value))execCommand('undo')"/>
      					<input type="hidden"  name="pd_batch_price1_hid" id="pd_batch_price1_hid" value="${product.pd_batch_price1 }"/>
      				</td>
    			</tr>
    			<tr class="list">
    				<td align="right">会员折扣：</td>
      				<td >
      					<span class="ui-combo-wrap" id="span_vip_sale">
						</span>
						<input type="hidden" name="pd_vip_sale" id="pd_vip_sale" value="${product.pd_vip_sale }"/>
      				</td>
      				<td align="right">是否礼品：</td>
      				<td >
      					<span class="ui-combo-wrap" id="span_present">
						</span>
						<input type="hidden" name="pd_present" id="pd_present" value="${product.pd_present }"/>
      				</td>
      				<td align="right">批发价2：</td>
      				<td >
      					<input class="main_Input" type="text"  name="pd_batch_price2" id="pd_batch_price2" value="${product.pd_batch_price2 }" onkeyup="if(isNaN(value))execCommand('undo')" onafterpaste="if(isNaN(value))execCommand('undo')"/>
      					<input type="hidden"  name="pd_batch_price2_hid" id="pd_batch_price2_hid" value="${product.pd_batch_price2 }"/>
      				</td>
    			</tr>
    			<tr class="list">
					<td align="right">允许采购：</td>
      				<td >
      					<span class="ui-combo-wrap" id="span_buy">
						</span>
						<input type="hidden" name="pd_buy" id="pd_buy" value="${product.pd_buy }"/>
      				</td>
      				<td align="right">是否积分：</td>
      				<td>
      					<span class="ui-combo-wrap" id="span_point">
						</span>
						<input type="hidden" name="pd_point" id="pd_point" value="${product.pd_point }"/>
      				</td>
      				<td align="right">批发价3：</td>
      				<td >
      					<input class="main_Input" type="text"  name="pd_batch_price3" id="pd_batch_price3" value="${product.pd_batch_price3 }" onkeyup="if(isNaN(value))execCommand('undo')" onafterpaste="if(isNaN(value))execCommand('undo')"/>
      					<input type="hidden"  name="pd_batch_price3_hid" id="pd_batch_price3_hid" value="${product.pd_batch_price3 }"/>
      				</td>
    			</tr>
    			<tr class="list">
					<td align="right">国标码：</td>
      				<td >
      					<input class="main_Input" type="text" name="pd_gbcode" id="pd_gbcode" value="${product.pd_gbcode }" maxlength="20"/>
      				</td>
      				<td align="right">洗涤说明：</td>
      				<td colspan="3">
      					<input class="main_Input" type="text" name="pd_wash_explain" id="pd_wash_explain" value="${product.pd_wash_explain }" maxlength="50" style="width: 388px;"/>
      				</td>
    			</tr>
			</table>
		</div>
		<div class="content" id="tagContent3" style="display:none;overflow-y:auto;">
			
		</div>
	</div>
	</div>
</div>
</form>
<script>
	$().ready(function() {
		$("#form1").validate({
			ignore: "",
			onfocusout : function(element) {
				$(element).valid();
			},
			errorPlacement: function(error, element) { //错误信息位置设置方法
				element.focus();
				$("#errorTip").empty();
				error.appendTo($("#errorTip"));
			},
			rules : {
				pd_no : "required",
				pd_name : "required",
				pd_bd_code : "required",
				pd_bd_name : "required",
				pd_tp_code : "required",
				pd_tp_name : "required",
				pd_szg_code : "required",
				pd_szg_name : "required",
				pd_style : "required",
				pd_buy_cycle : {
					isNumber : true
				},
				pd_sell_cycle : {
					required : true,
					isNumber : true
				},
				discount : {
					isDouble : true
				},
				pd_sign_price : {
					isNumber : true
				},
				pd_sell_price : {
					isNumber : true
				},
				pd_vip_price : {
					isNumber : true
				},
				pd_buy_price : {
					isNumber : true
				},
				pd_cost_price : {
					isNumber : true
				},
				BatchDiscount : {
					isDouble : true
				},
				pd_batch_price : {
					isNumber : true
				},
				pd_score : {
					isNumber : true
				},
				pd_batch_price1 : {
					isNumber : true
				},
				pd_batch_price2 : {
					isNumber : true
				},
				pd_batch_price3 : {
					isNumber : true
				},
				pd_commission : {
					isNumber : true
				},
				pd_commission_rate : {
					isDouble : true
				},
				pd_salesman_comm : {
					isNumber : true
				},
				pd_salesman_commrate : {
					isDouble : true
				},
				pd_sort_price : {
					isNumber : true
				}
			},
			messages : {
				pd_no : "请输入商品货号",
				pd_name : "请输入商品名称",
				pd_bd_code : "请选择商品品牌",
				pd_bd_name : "请选择商品品牌",
				pd_tp_code : "请选择商品类别",
				pd_tp_name : "请选择商品类别",
				pd_szg_code : "请选择尺码组",
				pd_szg_name : "请选择尺码组",
				pd_style : "请选择商品款式",
				pd_buy_cycle : {
					isNumber : "进货周期只能输入数字"
				},
				pd_sell_cycle : {
					required : "请输入零售周期",
					isNumber : "零售周期只能输入数字"
				},
				discount : {
					isDouble : "进价折扣只能输入0-1之间数字"
				},
				pd_sign_price : {
					isNumber : "标牌价只能输入数字"
				},
				pd_sell_price : {
					isNumber : "零售价只能输入数字"
				},
				pd_vip_price : {
					isNumber : "会员价只能输入数字"
				},
				pd_buy_price : {
					isNumber : "进价只能输入数字"
				},
				pd_cost_price : {
					isNumber : "成本价只能输入数字"
				},
				BatchDiscount : {
					isDouble : "批发折扣只能输入0-1之间数字"
				},
				pd_batch_price : {
					isNumber : "批发价只能输入数字"
				},
				pd_batch_price1 : {
					isNumber : "批发价1只能输入数字"
				},
				pd_batch_price2 : {
					isNumber : "批发价2只能输入数字"
				},
				pd_batch_price3 : {
					isNumber : "批发价3只能输入数字"
				},
				pd_score : {
					isNumber : "兑换积分只能输入数字"
				},
				pd_commission : {
					isNumber : "导购员提成金额只能输入数字"
				},
				pd_commission_rate : {
					isDouble : "导购员提成率只能输入0-1之间数字"
				},
				pd_salesman_comm : {
					isNumber : "业务员提成金额只能输入数字"
				},
				pd_salesman_commrate : {
					isDouble : "业务员提成率只能输入0-1之间数字"
				},
				pd_sort_price : {
					isNumber : "配送价只能输入数字"
				}
			}
		});
	});
</script>
<script src="<%=basePath%>data/base/product/product_update.js"></script>
</body>
</html>