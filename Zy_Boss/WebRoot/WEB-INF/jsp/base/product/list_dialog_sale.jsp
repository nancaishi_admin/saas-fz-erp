<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery-1.11.1.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/comm-search.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
</script> 
</head>
<body id="main_content">
<input type="hidden" name="flushFlag" id="flushFlag" value=""/>
<div class="mainwra">
	<div class="mod-search cf">
	    <div class="fl">
	    	<div id="filter-menu" class="ui-btn-menu fl">
	    		 <span style="float:left" class="ui-btn menu-btn">
		     		 <strong>查询条件</strong>
		     		 	<input type="text" id="SearchContent" name="SearchContent" class="main_Input" value="" title="支持货号/名称/拼音简码模糊查询"/>
	          			<input type="text" style="display:none"/>
					 <b></b>
			  	 </span>
		 		<div class="con" style="width: 320px;">
					<ul class="ul-inline">
						<li>
							<table class="searchbar">
								<tr>     
									<td align="right">商品货号：</td>
									<td>
			                            <input type="text" id="pd_no" name="pd_no" class="main_Input" value=""/>
							  	 	</td>
								 </tr>
								  <tr>
									  <td align="right">商品年份：</td>
									  <td>
									  	 <span class="ui-combo-wrap" id="span_year" style="width: 206px;height: 30px;"></span>
          								 <input type="hidden"  name="pd_year"  id="pd_year"/>
							  		   </td>
								  </tr>
								  <tr>
									  <td align="right">商品季节：</td>
									  <td>
									  	<span class="ui-combo-wrap" id="span_pd_season_Code">
										</span>
										<input type="hidden" name="pd_season" id="pd_season"/>
							  		</td>
								  </tr>
								  <tr>
										<td align="right">商品类别：</td>
										<td>
									      	<input class="main_Input" type="text" name="pd_tp_name" id="pd_tp_name" value="" style="width:170px;" readonly="readonly"/>
											<input type="button" value="" class="btn_select" onclick="javascript:handle.doQueryType();"/>
											<input class="main_Input" type="hidden" name="pd_tp_code" id="pd_tp_code" value=""/>
											<input class="main_Input" type="hidden" name="pd_tp_upcode" id="pd_tp_upcode" value=""/>
										</td>
									</tr>
									<tr>
										<td align="right">商品品牌：</td>
										<td>
											<input class="main_Input" type="text" name="pd_bd_name" id="pd_bd_name" value="" style="width:170px;" readonly="readonly"/>
											<input type="button" value="" class="btn_select" onclick="javascript:handle.doQueryBrand();"/>
											<input class="main_Input" type="hidden" name="pd_bd_code" id="pd_bd_code" value=""/>
										</td>
									</tr>
							</table>
						</li>
						<li style="float:right;">
							<a class="ui-btn ui-btn-sp mrb" id="btn_ok">确认</a>
		       				<a class="ui-btn mrb" id="btn_reset">重置</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	    <div class="fl-m">
	    	<a class="ui-btn mrb" id="btn-search" name="btn-search">查询</a>
	    	<a href="javascript:void(0);" class="ui-btn ui-btn-sp mrb" id="btn-add" >新增</a>
		</div>
	    <div class="fr">
	    	
	    </div>
	 </div>
	<div class="grid-wrap">
	    <table id="grid">
	    </table>
    	<div id="page"></div>
	</div>
</div>
<script src="<%=basePath%>data/base/product/product_list_dialog_sale.js"></script>
</body>
</html>