<%@page import="zy.util.StringUtil"%>
<%@page import="zy.util.CommonUtil"%>
<%@page import="zy.entity.sys.set.T_Sys_Set"%>
<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/css/iconfont/iconfont.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery-1.11.1.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/comm-search.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.size.js\"></sc"+"ript>");
</script> 
<style>
.opselect{border:#ddd solid 1px;height:25px;line-height:25px;padding:0 0 0 5px;}
</style>
</head>
<body>
<%
	T_Sys_Set set = (T_Sys_Set)request.getSession().getAttribute(CommonUtil.KEY_SYSSET);
%>
<div class="wrapper" >
	<div class="mod-search cf">
		<div class="fl">
			<div id="filter-menu" class="ui-btn-menu fl">
	    		 <span style="float:left" class="ui-btn menu-btn">
		     		 <strong>商品货号</strong>
		     		 <input type="text" id="pd_no" name="pd_no" class="main_Input" value="" title=""/>
	          		<input type="text" style="display:none"/>
					 <b></b>
			  	 </span>
			 	<div class="con" style="width: 320px;">
			 		<ul class="ul-inline">
						<li>
							<table class="searchbar">
								 <tr>
									  <td align="right">商品名称：</td>
									  <td>
									  	<input type="text" id="pd_name" name="pd_name" class="main_Input" value=""/>
							  		</td>
								  </tr>
								  <tr>
										<td align="right">商品类别：</td>
										<td>
									      	<input class="main_Input" type="text" name="pd_tp_name" id="pd_tp_name" value="" style="width:170px;" readonly="readonly"/>
											<input type="button" value="" class="btn_select" onclick="javascript:handle.doQueryType();"/>
											<input class="main_Input" type="hidden" name="pd_tp_code" id="pd_tp_code" value=""/>
											<input class="main_Input" type="hidden" name="pd_tp_upcode" id="pd_tp_upcode" value=""/>
										</td>
									</tr>
									<tr>
										<td align="right">商品品牌：</td>
										<td>
											<input class="main_Input" type="text" name="pd_bd_name" id="pd_bd_name" value="" style="width:170px;" readonly="readonly"/>
											<input type="button" value="" class="btn_select" onclick="javascript:handle.doQueryBrand();"/>
											<input class="main_Input" type="hidden" name="pd_bd_code" id="pd_bd_code" value=""/>
										</td>
									</tr>
									<tr>
									  <td align="right">条&nbsp;&nbsp;形&nbsp;&nbsp;码：</td>
									  <td>
									  	<input type="text" id="bc_barcode" name="bc_barcode" class="main_Input" value=""/>
							  		</td>
								  </tr>
							</table>
						</li>
						<li style="float:right;">
							<a class="ui-btn ui-btn-sp mrb" id="btn_ok">确认</a>
		       				<a class="ui-btn mrb" id="btn_reset">重置</a>
						</li>
					</ul>
				</div>
		</div>
	</div>
	<div class="fl-m"><a class="ui-btn fl mrb" id="btn-search" name="btn-search">查询</a></div>
	<font style="color: red;font-size: 12px;">条形码生成规则：<%=StringUtil.trimString(set.getSt_subcode_rule()) %></font>
	    <div class="fr">
	    	打印数量批量:<input type="text" id="changeNum" name="changeNum" class="main_Input" style="width:50px" value="" onkeyup="javascript:onKeyUpChangeNum(this);"/>
	    	<a href="javascript:void(0);" class="ui-btn ui-btn-sp mrb" id="btn-print">打印</a>
	    	<a href="javascript:void(0);" class="ui-btn ui-btn-sp mrb" id="btn-print-preview">打印预览</a>
	    	<a href="javascript:void(0);" class="ui-btn ui-btn-sp mrb" id="btn-printSite">打印设置</a>
	    	<a href="javascript:void(0);" class="ui-btn ui-btn-sp mrb" id="btn-bulkCopyBarcode">批量复制</a>
	    	<a href="javascript:void(0);" class="ui-btn ui-btn-sp mrb" id="btn-bulkSaveBarcode">批量保存</a>
	    	<a href="javascript:void(0);" class="ui-btn ui-btn-sp mrb" id="btn-bulkDelBarcode">批量删除</a>
	    	<a href="javascript:void(0);" class="ui-btn ui-btn-sp mrb" id="btn-DelAllBarcode">清空</a>
	    	<a href="javascript:void(0);" class="ui-btn ui-btn-sp mrb" id="btn-import" >导入</a>
	    	<!-- <a href="javascript:void(0);" class="ui-btn ui-btn-sp mrb" id="btn-export">导出</a> -->
	    </div>
	 </div>
	<div class="grid-wrap" style="padding:0px;">
	    <table id="grid">
	    </table>
	   	<div id="page"></div>
	</div>
	</div>
</body>
<script src="<%=basePath%>data/base/product/product_barcode_list.js?v=${time}"></script>
<%-- <script src="<%=printPath%>/CLodopfuncs.js"></script>
<script language="javascript" src="<%=basePath%>resources/util/barcode_print.js"></script> --%>
<script src="<%=basePath%>resources/util/forbidBackSpace.js"></script>
<object id="LODOP_OB" classid="clsid:2105C259-1E0C-4534-8141-A753534CB4CA" width=0 height=0> 
	<embed id="LODOP_EM" type="application/x-print-lodop" width=0 height=0 pluginspage="install_lodop32.exe"></embed>
</object>
<script language="javascript" src="<%=basePath%>resources/util/barcode_print.js?v=${time}"></script>
<script language="javascript" src="<%=basePath%>resources/util/LodopFuncs.js"></script>
</html>