<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/css/base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
.b1,.b2,.b3,.b4,.b1b,.b2b,.b3b,.b4b,.b{display:block;overflow:hidden;}
 .b1,.b2,.b3,.b1b,.b2b,.b3b{height:1px;}
 .b2,.b3,.b4,.b2b,.b3b,.b4b,.b{border-left:1px solid #999;border-right:1px solid #999;}
 .b1,.b1b{margin:0 5px;background:#999;}
 .b2,.b2b{margin:0 3px;border-width:2px;}
 .b3,.b3b{margin:0 2px;}
 .b4,.b4b{height:2px;margin:0 1px;}
 .d1{background:#F7F8F9;}
 .ui-droplist-wrap .droplist{
	height:auto;
}
.ui-droplist-wrap .list-item{line-height:21px;}
	.list td{line-height:32px; }
	.main_Input{width: 130px; height: 16px;}
	.width100{width:100px;}
	.mutab {
	    height: 32px;
	    margin: 10px 0 0 11px;
	}
	ul {
	    list-style: none;
	}
	.mutab li {
	    cursor: pointer;
	    display: inline;
	    float: left;
	    margin-right: 5px;
	}
	.select {
	    background: #fff;
	    border-style: solid;
	    border-width: 2px 1px 0;
	    border-color: #08c #ddd #ddd;
	    height: 30px;
	    text-align: center;
	    line-height: 30px;
	    width: 120px;
	    color: #08c;
	    font-weight: 700;
	    cursor: hand;
	}
	.menu {
	    background: #f1f1f1;
	    border-style: solid;
	    border-width: 1px 1px 0;
	    border-color: #ddd;
	    height: 30px;
	    text-align: center;
	    line-height: 30px;
	    width: 120px;
	    cursor: hand;
	}
	.content {
	    border: #ddd 1px solid;
	    height: 460px;
	    width: 98%;
	    background-color: #FFFFFF;
	    margin: 0 0 0 10px;
	}
	.error{
		color:red;
	}
	.opselect{
		border:#ddd solid 1px;
		height:25px;
		line-height:25px;
		padding:0 0 0 5px;
		width:80px;
	}
	.btn_copy{
		background:#f60;
		border:none;
		color:#fff;
		font-weight:700;
		height:30px;
		margin-left:10px;
		overflow:hidden;
		padding:0;
		width:80px;
	}
	.btn_adds{
		background:#08b;
		border:none;
		color:#fff;
		font-weight:700;
		height:26px;
		margin-left:10px;
		overflow:hidden;
		padding:0;
		width:55px;
	}
	.btn_icon{
	    background: #ddd no-repeat scroll;
	    border: 0 none;
	    color: #333;
	    cursor: pointer;
	    height: 27px;
	    margin: 3px;
	    width: 47px;
	}
	.btn_icon:hover {
	    background: #c2c2c2 no-repeat scroll;
	    color: #3366cc;
	}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/hanzi2pinyin.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery-1.11.1.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/validator.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
</script> 
</head>
<body>
<form name="form1" method="post" action="" id="form1">
<div class="mainwra" style="min-width:740px;overflow:hidden;left: 0px;top: 0px;">
	<div>
		<table width="100%" cellspacing="0" cellpadding="0">
		    <tr>
		      <td class="top-b border-b">
		        <input id="btn-save" type="button" class="t-btn btn-red" value="保存"/>
		        <input id="btn_close" class="t-btn" type="button" value="返回"/>
		        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>方案名称：</b><input class="main_Input" style="width: 250px;" type="text" maxlength="20" name="bs_plan_name" id="bs_plan_name" value="${barcode_set.bs_plan_name }"/>
		        <input type="hidden" id="bs_id" name="bs_id" value="${barcode_set.bs_id }"/>
		        <input type="hidden" id="bs_bartype" name="bs_bartype" value="128Auto"/>
		      </td>
		    </tr>
	    </table>
	    <div class="mutab">
			<ul>
				<li id="menu1" class="select" onclick="selContent('1','1')">条码样式</li>
				<li id="menu2" class="menu" onclick="selContent('2','2')" >条码字段</li>
			</ul>
			&nbsp;&nbsp;<font style="color: red;">条码类型：128Auto(数字、大写英文或下划线)</font>
		</div>
		<div id="tagContent" style="height:250px;width:710px;border:0px;margin:0px;">
			<div class="content" id="tagContent1" style="width:710px;display:block;height:250px;">
				<table width="100%">
					<tr>
						<td align="right">条码纸张宽度：</td>
						<td align="left">
							<input class="main_Input" style="width: 80px;" type="text" onpropertychange="javascript:changePreView();" name="bs_paper_width" id="bs_paper_width" value="${barcode_set.bs_paper_width }"/>(毫米)
						</td>
						<td align="right">条码纸张高度：</td>
						<td align="left">
							<input class="main_Input" style="width: 80px;" type="text" onpropertychange="javascript:changePreView();" name="bs_paper_height" id="bs_paper_height" value="${barcode_set.bs_paper_height }"/>(毫米)
						</td>
					</tr>
					<tr>
						<td align="right">整页上边距：</td>
						<td align="left">
							<input class="main_Input" style="width: 80px;" type="text" onpropertychange="javascript:changePreView();" name="bs_top_backgauge" id="bs_top_backgauge" value="${barcode_set.bs_top_backgauge }"/>(毫米)
						</td>
						<td align="right">整页左边距：</td>
						<td align="left">
							<input class="main_Input" style="width: 80px;" type="text" onpropertychange="javascript:changePreView();" name="bs_left_backgauge" id="bs_left_backgauge" value="${barcode_set.bs_left_backgauge }"/>(毫米)
						</td>
					</tr>
					<tr>
						<td align="right">条形码宽度：</td>
						<td align="left">
							<input class="main_Input" style="width: 80px;" onpropertychange="javascript:changePreView();" type="text" name="bs_width" id="bs_width" value="${barcode_set.bs_width }"/>(毫米)
						</td>
						<td  align="right">条形码高度：</td>
						<td align="left">
							<input class="main_Input" style="width: 80px;" onpropertychange="javascript:changePreView();" type="text" name="bs_height" id="bs_height" value="${barcode_set.bs_height }"/>(毫米)
						</td>
					</tr>
					<tr>
						<td align="right">条形码纸张列间距：</td>
						<td align="left">
							<input class="main_Input" style="width: 80px;" type="hidden" onpropertychange="javascript:changePreView();" name="bs_hight_spacing" id="bs_hight_spacing" value="${barcode_set.bs_hight_spacing }"/>
							<input class="main_Input" style="width: 80px;" type="text" onpropertychange="javascript:changePreView();" name="bs_width_spacing" id="bs_width_spacing" value="${barcode_set.bs_width_spacing }"/>(毫米)
						</td>
						<td align="right">打&nbsp;印&nbsp;列&nbsp;数：</td>
						<td align="left">
							<span class="ui-combo-wrap" id="span_line">
							</span>
							<input type="hidden" name="bs_line" id="bs_line" value="${barcode_set.bs_line }"/>
						</td>
					</tr>
					<tr>
						<td align="right">商品信息间距：</td>
						<td align="left">
							<input class="main_Input" style="width: 80px;" type="text" onpropertychange="javascript:changePreView();" name="bs_p_space" id="bs_p_space" value="${barcode_set.bs_p_space }"/>(毫米)
						</td>
						<td align="right">信息和条码距离：</td>
						<td align="left">
							<input class="main_Input" style="width: 80px;" type="text" onpropertychange="javascript:changePreView();" name="bs_ps_space" id="bs_ps_space" value="${barcode_set.bs_ps_space }"/>(毫米)
						</td>
					</tr>
					<tr>
						<td align="right">字体：</td>
						<td align="left">
							<span class="ui-combo-wrap" id="span_font_name">
							</span>
							<input type="hidden" name="bs_font_name" id="bs_font_name" value="${barcode_set.bs_font_name }"/>
						</td>
						<td  align="right">字体大小：</td>
						<td align="left">
							<span class="ui-combo-wrap" id="span_font_size">
							</span>
							<input type="hidden" name="bs_font_size" id="bs_font_size" value="${barcode_set.bs_font_size }"/>
						</td>
					</tr>
					<tr>
						<td align="right">是否加粗：</td>
						<td align="left">
							<span class="ui-combo-wrap" id="span_bold">
							</span>
							<input type="hidden" name="bs_bold" id="bs_bold" value="${barcode_set.bs_bold }"/>
						</td>
						<td align="right">英文字段名：</td>
						<td align="left">
							<span class="ui-combo-wrap" id="span_language">
							</span>
							<input type="hidden" name="bs_language" id="bs_language" value="${barcode_set.bs_language }"/>
						</td>
					</tr>
				</table>
			</div>
			<div class="content" id="tagContent2" style="width:710px; height:250px; display:none;overflow-y:auto;overflow-x:auto;">
				<table width="100%">
					<tr>
						<td align="right" style="width: 70px;"><b>货号：</b></td>
						<td align="left" style="width: 140px;">
							是否显示
							<span class="ui-combo-wrap" id="span_pdno_ifshow">
							</span>
							<input type="hidden" name="bs_pdno_ifshow" id="bs_pdno_ifshow" value="${barcode_set.bs_pdno_ifshow }"/>
						</td>
						<td align="left"  style="width: 140px;">
							行号
							<span class="ui-combo-wrap" id="span_pdno_row">
							</span>
							<input type="hidden" name="bs_pdno_row" id="bs_pdno_row" value="${barcode_set.bs_pdno_row }"/>
						</td>
						<td align="right" style="width: 70px;"><b>品名：</b></td>
						<td align="left" style="width: 140px;">
							是否显示
							<span class="ui-combo-wrap" id="span_pdname_ifshow">
							</span>
							<input type="hidden" name="bs_pdname_ifshow" id="bs_pdname_ifshow" value="${barcode_set.bs_pdname_ifshow }"/>
						</td>
						<td align="left"  style="width: 140px;">
							行号
							<span class="ui-combo-wrap" id="span_pdname_row">
							</span>
							<input type="hidden" name="bs_pdname_row" id="bs_pdname_row" value="${barcode_set.bs_pdname_row }"/>
						</td>
						<td></td>
					</tr>
					<tr>
						<td align="right"><b>颜色：</b></td>
						<td align="left">
							是否显示
							<span class="ui-combo-wrap" id="span_cr_ifshow">
							</span>
							<input type="hidden" name="bs_cr_ifshow" id="bs_cr_ifshow" value="${barcode_set.bs_cr_ifshow }"/>
						</td>
						<td align="left">
							行号
							<span class="ui-combo-wrap" id="span_cr_row">
							</span>
							<input type="hidden" name="bs_cr_row" id="bs_cr_row" value="${barcode_set.bs_cr_row }"/>
						</td>
						<td align="right"><b>杯型：</b></td>
						<td align="left">
							是否显示
							<span class="ui-combo-wrap" id="span_bs_ifshow">
							</span>
							<input type="hidden" name="bs_bs_ifshow" id="bs_bs_ifshow" value="${barcode_set.bs_bs_ifshow }"/>
						</td>
						<td align="left">
							行号
							<span class="ui-combo-wrap" id="span_bs_row">
							</span>
							<input type="hidden" name="bs_bs_row" id="bs_bs_row" value="${barcode_set.bs_bs_row }"/>
						</td>
						<td></td>
					</tr>
					<tr>
						<td align="right"><b>尺码：</b></td>
						<td align="left">
							是否显示
							<span class="ui-combo-wrap" id="span_sz_ifshow">
							</span>
							<input type="hidden" name="bs_sz_ifshow" id="bs_sz_ifshow" value="${barcode_set.bs_sz_ifshow }"/>
						</td>
						<td align="left">
							行号
							<span class="ui-combo-wrap" id="span_sz_row">
							</span>
							<input type="hidden" name="bs_sz_row" id="bs_sz_row" value="${barcode_set.bs_sz_row }"/>
						</td>
						<td align="right"><b>品牌：</b></td>
						<td align="left">
							是否显示
							<span class="ui-combo-wrap" id="span_bd_ifshow">
							</span>
							<input type="hidden" name="bs_bd_ifshow" id="bs_bd_ifshow" value="${barcode_set.bs_bd_ifshow }"/>
						</td>
						<td align="left">
							行号
							<span class="ui-combo-wrap" id="span_bd_row">
							</span>
							<input type="hidden" name="bs_bd_row" id="bs_bd_row" value="${barcode_set.bs_bd_row }"/>
						</td>
						<td></td>
					</tr>
					<tr>
						<td align="right"><b>零售价：</b></td>
						<td align="left">
							是否显示
							<span class="ui-combo-wrap" id="span_price_ifshow">
							</span>
							<input type="hidden" name="bs_price_ifshow" id="bs_price_ifshow" value="${barcode_set.bs_price_ifshow }"/>
						</td>
						<td align="left">
							行号
							<span class="ui-combo-wrap" id="span_price_row">
							</span>
							<input type="hidden" name="bs_price_row" id="bs_price_row" value="${barcode_set.bs_price_row }"/>
						</td>
						<td align="right"><b>标牌价：</b></td>
						<td align="left">
							是否显示
							<span class="ui-combo-wrap" id="span_signprice_ifshow">
							</span>
							<input type="hidden" name="bs_signprice_ifshow" id="bs_signprice_ifshow" value="${barcode_set.bs_signprice_ifshow }"/>
						</td>
						<td align="left">
							行号
							<span class="ui-combo-wrap" id="span_signprice_row">
							</span>
							<input type="hidden" name="bs_signprice_row" id="bs_signprice_row" value="${barcode_set.bs_signprice_row }"/>
						</td>
						<td></td>
					</tr>
					<tr>
						<td align="right"><b>会员价：</b></td>
						<td align="left">
							是否显示
							<span class="ui-combo-wrap" id="span_vipprice_ifshow">
							</span>
							<input type="hidden" name="bs_vipprice_ifshow" id="bs_vipprice_ifshow" value="${barcode_set.bs_vipprice_ifshow }"/>
						</td>
						<td align="left">
							行号
							<span class="ui-combo-wrap" id="span_vipprice_row">
							</span>
							<input type="hidden" name="bs_vipprice_row" id="bs_vipprice_row" value="${barcode_set.bs_vipprice_row }"/>
						</td>
						<td align="right"><b>成份：</b></td>
						<td align="left">
							是否显示
							<span class="ui-combo-wrap" id="span_filling_ifshow">
							</span>
							<input type="hidden" name="bs_filling_ifshow" id="bs_filling_ifshow" value="${barcode_set.bs_filling_ifshow }"/>
						</td>
						<td align="left">
							行号
							<span class="ui-combo-wrap" id="span_filling_row">
							</span>
							<input type="hidden" name="bs_filling_row" id="bs_filling_row" value="${barcode_set.bs_filling_row }"/>
						</td>
						<td></td>
					</tr>
					<tr>
						<td align="right" ><b>类别：</b></td>
						<td align="left">
							是否显示
							<span class="ui-combo-wrap" id="span_type_ifshow">
							</span>
							<input type="hidden" name="bs_type_ifshow" id="bs_type_ifshow" value="${barcode_set.bs_type_ifshow }"/>
						</td>
						<td align="left">
							行号
							<span class="ui-combo-wrap" id="span_type_row">
							</span>
							<input type="hidden" name="bs_type_row" id="bs_type_row" value="${barcode_set.bs_type_row }"/>
						</td>
						<td align="right" ><b>等级：</b></td>
						<td align="left">
							是否显示
							<span class="ui-combo-wrap" id="span_class_ifshow">
							</span>
							<input type="hidden" name="bs_class_ifshow" id="bs_class_ifshow" value="${barcode_set.bs_class_ifshow }"/>
						</td>
						<td align="left">
							行号
							<span class="ui-combo-wrap" id="span_class_row">
							</span>
							<input type="hidden" name="bs_class_row" id="bs_class_row" value="${barcode_set.bs_class_row }"/>
						</td>
						<td></td>
					</tr>
					<tr>
						<td align="right"><b>安全标准：</b></td>
						<td align="left" >
							是否显示
							<span class="ui-combo-wrap" id="span_safestandard_ifshow">
							</span>
							<input type="hidden" name="bs_safestandard_ifshow" id="bs_safestandard_ifshow" value="${barcode_set.bs_safestandard_ifshow }"/>
						</td>
						<td align="left">
							行号
							<span class="ui-combo-wrap" id="span_safestandard_row">
							</span>
							<input type="hidden" name="bs_safestandard_row" id="bs_safestandard_row" value="${barcode_set.bs_safestandard_row }"/>
						</td>
						<td align="right" ><b>执行标准：</b></td>
						<td align="left">
							是否显示
							<span class="ui-combo-wrap" id="span_executionstandard_ifshow">
							</span>
							<input type="hidden" name="bs_executionstandard_ifshow" id="bs_executionstandard_ifshow" value="${barcode_set.bs_executionstandard_ifshow }"/>
						</td>
						<td align="left">
							行号
							<span class="ui-combo-wrap" id="span_executionstandard_row">
							</span>
							<input type="hidden" name="bs_executionstandard_row" id="bs_executionstandard_row" value="${barcode_set.bs_executionstandard_row }"/>
						</td>
						<td></td>
					</tr>
					<tr>
						<td align="right"><b>面料：</b></td>
						<td align="left" >
							是否显示
							<span class="ui-combo-wrap" id="span_fabric_ifshow">
							</span>
							<input type="hidden" name="bs_fabric_ifshow" id="bs_fabric_ifshow" value="${barcode_set.bs_fabric_ifshow }"/>
						</td>
						<td align="left">
							行号
							<span class="ui-combo-wrap" id="span_fabric_row">
							</span>
							<input type="hidden" name="bs_fabric_row" id="bs_fabric_row" value="${barcode_set.bs_fabric_row }"/>
						</td>
						<td align="right" ><b>里料：</b></td>
						<td align="left">
							是否显示
							<span class="ui-combo-wrap" id="span_infabric_ifshow">
							</span>
							<input type="hidden" name="bs_infabric_ifshow" id="bs_infabric_ifshow" value="${barcode_set.bs_infabric_ifshow }"/>
						</td>
						<td align="left">
							行号
							<span class="ui-combo-wrap" id="span_infabric_row">
							</span>
							<input type="hidden" name="bs_infabric_row" id="bs_infabric_row" value="${barcode_set.bs_infabric_row }"/>
						</td>
						<td></td>
					</tr>
					<tr>
						<td align="right"><b>产地：</b></td>
						<td align="left" >
							是否显示
							<span class="ui-combo-wrap" id="span_place_ifshow">
							</span>
							<input type="hidden" name="bs_place_ifshow" id="bs_place_ifshow" value="${barcode_set.bs_place_ifshow }"/>
						</td>
						<td align="left">
							行号
							<span class="ui-combo-wrap" id="span_place_row">
							</span>
							<input type="hidden" name="bs_place_row" id="bs_place_row" value="${barcode_set.bs_place_row }"/>
						</td>
						<td align="right" ><b>洗涤说明：</b></td>
						<td align="left">
							是否显示
							<span class="ui-combo-wrap" id="span_washexplain_ifshow">
							</span>
							<input type="hidden" name="bs_washexplain_ifshow" id="bs_washexplain_ifshow" value="${barcode_set.bs_washexplain_ifshow }"/>
						</td>
						<td align="left">
							行号
							<span class="ui-combo-wrap" id="span_washexplain_row">
							</span>
							<input type="hidden" name="bs_washexplain_row" id="bs_washexplain_row" value="${barcode_set.bs_washexplain_row }"/>
						</td>
						<td></td>
					</tr>
				</table>
			</div>
		</div>
		<div style="width:100%;background-color: #C0C0C0;height: 20px;">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b style="font-size: 10pt;">条码预览：</b>
		</div>
		<div align="center" style="width: 100%;height: 100%;">

			<div id="preview" style="border: 1px solid gray;" align="left">
				<div id="paperDiv" style="">
					<b class="b1"></b><b class="b2 d1"></b><b class="b3 d1"></b><b class="b4 d1"></b>
					<div class="b d1" style="width: 100%;height: 100%;">
						<div id="divFont1" style="width: 100%;height: 3mm;margin-top: -1mm;"><font id="font1"></font></div>
						<div id="divFont2" style="width: 100%;height: 3mm;"><font id="font2"></font></div>
						<div id="divFont3" style="width: 100%;height: 3mm;"><font id="font3"></font></div>
						<div id="divFont4" style="width: 100%;height: 3mm;"><font id="font4"></font></div>
						<div id="divFont5" style="width: 100%;height: 3mm;"><font id="font5"></font></div>
						<div id="divFont6" style="width: 100%;height: 3mm;"><font id="font6"></font></div>
						<div id="divFont7" style="width: 100%;height: 3mm;"><font id="font7"></font></div>
						<div id="divFont8" style="width: 100%;height: 3mm;"><font id="font8"></font></div>
						<div id="divFont9" style="width: 100%;height: 3mm;"><font id="font9"></font></div>
						<div id="divFont10" style="width: 100%;height: 3mm;"><font id="font10"></font></div>
						<div id="subCodeShow1_Up" style="width: 100%;height: 3mm;"><font id="subCodeShowFont1_Up">100010010101</font></div>
						<div id="barcode">
							<div style="width: 5%;height:100%;background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 4%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 1%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 4%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 3%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 1%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 4%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 4%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 5%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 4%;height:100%;background-color: #FFF;float: left;"></div>
						</div>
						<div id="subCodeShow1" style="width: 100%;height: 3mm;"><font id="subCodeShowFont1">100010010101</font></div>
						<div id="priceShow1_Down" style="width: 100%;height: 3mm;"><font id="priceShowFont1_Down"> ￥89</font></div>
					</div>
					<b class="b4b d1"></b><b class="b3b d1"></b><b class="b2b d1"></b><b class="b1b"></b>
				</div>
			</div>
			<div id="preview2" style="border: 1px solid gray;" align="left">
				
				<div id="paperDiv1" style="float:left">
					<b class="b1"></b><b class="b2 d1"></b><b class="b3 d1"></b><b class="b4 d1"></b>
					<div class="b d1" style="width: 100%;height: 100%;">
						<div id="divFont21" style="width: 100%;height: 3mm;margin-top: -1mm;"><font id="font21"></font></div>
						<div id="divFont22" style="width: 100%;height: 3mm;"><font id="font22"></font></div>
						<div id="divFont23" style="width: 100%;height: 3mm;"><font id="font23"></font></div>
						<div id="divFont24" style="width: 100%;height: 3mm;"><font id="font24"></font></div>
						<div id="divFont25" style="width: 100%;height: 3mm;"><font id="font25"></font></div>
						<div id="divFont26" style="width: 100%;height: 3mm;"><font id="font26"></font></div>
						<div id="divFont27" style="width: 100%;height: 3mm;"><font id="font27"></font></div>
						<div id="divFont28" style="width: 100%;height: 3mm;"><font id="font28"></font></div>
						<div id="divFont29" style="width: 100%;height: 3mm;"><font id="font29"></font></div>
						<div id="divFont30" style="width: 100%;height: 3mm;"><font id="font30"></font></div>
						<div id="subCodeShow2_Up" style="width: 100%;height: 3mm;"><font id="subCodeShowFont2_Up">100010010101</font></div>
						<div id="barcode2">
							<div style="width: 5%;height:100%;background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 4%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 1%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 4%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 3%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 1%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 4%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 4%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 5%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 4%;height:100%;background-color: #FFF;float: left;"></div>
						</div>
						<div id="subCodeShow2" style="width: 100%;height: 3mm;"><font id="subCodeShowFont2">100010010101</font></div>
						<div id="priceShow2_Down" style="width: 100%;height: 3mm;"><font id="priceShowFont2_Down"> ￥89</font></div>
					</div>
					<b class="b4b d1"></b><b class="b3b d1"></b><b class="b2b d1"></b><b class="b1b"></b>
				</div>
		
				<div id="paperDiv2" style="float:left">
					<b class="b1"></b><b class="b2 d1"></b><b class="b3 d1"></b><b class="b4 d1"></b>
					<div class="b d1" style="width: 100%;height: 100%;">
						<div id="divFont31" style="width: 100%;height: 3mm;margin-top: -1mm;"><font id="font31"></font></div>
						<div id="divFont32" style="width: 100%;height: 3mm;"><font id="font32"></font></div>
						<div id="divFont33" style="width: 100%;height: 3mm;"><font id="font33"></font></div>
						<div id="divFont34" style="width: 100%;height: 3mm;"><font id="font34"></font></div>
						<div id="divFont35" style="width: 100%;height: 3mm;"><font id="font35"></font></div>
						<div id="divFont36" style="width: 100%;height: 3mm;"><font id="font36"></font></div>
						<div id="divFont37" style="width: 100%;height: 3mm;"><font id="font37"></font></div>
						<div id="divFont38" style="width: 100%;height: 3mm;"><font id="font38"></font></div>
						<div id="divFont39" style="width: 100%;height: 3mm;"><font id="font39"></font></div>
						<div id="divFont40" style="width: 100%;height: 3mm;"><font id="font40"></font></div>
						<div id="subCodeShow3_Up" style="width: 100%;height: 3mm;"><font id="subCodeShowFont3_Up">100010010101</font></div>
						<div id="barcode3">
							<div style="width: 5%;height:100%;background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 4%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 1%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 4%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 3%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 1%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 4%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 4%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 5%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 4%;height:100%;background-color: #FFF;float: left;"></div>
						</div>
						<div id="subCodeShow3" style="width: 100%;height: 3mm;"><font id="subCodeShowFont3">100010010101</font></div>
						<div id="priceShow3_Down" style="width: 100%;height: 3mm;"><font id="priceShowFont3_Down"> ￥89</font></div>
					</div>
					<b class="b4b d1"></b><b class="b3b d1"></b><b class="b2b d1"></b><b class="b1b"></b>
				</div>
			</div>
				<div id="preview3" style="border: 1px solid gray;" align="left">
				
				<div id="paperDiv3" style="float:left">
					<b class="b1"></b><b class="b2 d1"></b><b class="b3 d1"></b><b class="b4 d1"></b>
					<div class="b d1" style="width: 100%;height: 100%;">
						<div id="divFont41" style="width: 100%;height: 3mm;margin-top: -1mm;"><font id="font41"></font></div>
						<div id="divFont42" style="width: 100%;height: 3mm;"><font id="font42"></font></div>
						<div id="divFont43" style="width: 100%;height: 3mm;"><font id="font43"></font></div>
						<div id="divFont44" style="width: 100%;height: 3mm;"><font id="font44"></font></div>
						<div id="divFont45" style="width: 100%;height: 3mm;"><font id="font45"></font></div>
						<div id="divFont46" style="width: 100%;height: 3mm;"><font id="font46"></font></div>
						<div id="divFont47" style="width: 100%;height: 3mm;"><font id="font47"></font></div>
						<div id="divFont48" style="width: 100%;height: 3mm;"><font id="font48"></font></div>
						<div id="divFont49" style="width: 100%;height: 3mm;"><font id="font49"></font></div>
						<div id="divFont50" style="width: 100%;height: 3mm;"><font id="font50"></font></div>
						<div id="subCodeShow4_Up" style="width: 100%;height: 3mm;"><font id="subCodeShowFont4_Up">100010010101</font></div>
						<div id="barcode4">
							<div style="width: 5%;height:100%;background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 4%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 1%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 4%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 3%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 1%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 4%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 4%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 5%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 4%;height:100%;background-color: #FFF;float: left;"></div>
						</div>
						<div id="subCodeShow4" style="width: 100%;height: 3mm;"><font id="subCodeShowFont4">100010010101</font></div>
						<div id="priceShow4_Down" style="width: 100%;height: 3mm;"><font id="priceShowFont4_Down"> ￥89</font></div>
					</div>
					<b class="b4b d1"></b><b class="b3b d1"></b><b class="b2b d1"></b><b class="b1b"></b>
				</div>
				
				<div id="paperDiv4" style="float:left">
					<b class="b1"></b><b class="b2 d1"></b><b class="b3 d1"></b><b class="b4 d1"></b>
					<div class="b d1" style="width: 100%;height: 100%;">
						<div id="divFont51" style="width: 100%;height: 3mm;margin-top: -1mm;"><font id="font51"></font></div>
						<div id="divFont52" style="width: 100%;height: 3mm;"><font id="font52"></font></div>
						<div id="divFont53" style="width: 100%;height: 3mm;"><font id="font53"></font></div>
						<div id="divFont54" style="width: 100%;height: 3mm;"><font id="font54"></font></div>
						<div id="divFont55" style="width: 100%;height: 3mm;"><font id="font55"></font></div>
						<div id="divFont56" style="width: 100%;height: 3mm;"><font id="font56"></font></div>
						<div id="divFont57" style="width: 100%;height: 3mm;"><font id="font57"></font></div>
						<div id="divFont58" style="width: 100%;height: 3mm;"><font id="font58"></font></div>
						<div id="divFont59" style="width: 100%;height: 3mm;"><font id="font59"></font></div>
						<div id="divFont60" style="width: 100%;height: 3mm;"><font id="font60"></font></div>
						<div id="subCodeShow5_Up" style="width: 100%;height: 3mm;"><font id="subCodeShowFont5_Up">100010010101</font></div>
						<div id="barcode5">
							<div style="width: 5%;height:100%;background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 4%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 1%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 4%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 3%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 1%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 4%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 4%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 5%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 4%;height:100%;background-color: #FFF;float: left;"></div>
						</div>
						<div id="subCodeShow5" style="width: 100%;height: 3mm;"><font id="subCodeShowFont5">100010010101</font></div>
						<div id="priceShow5_Down" style="width: 100%;height: 3mm;"><font id="priceShowFont5_Down"> ￥89</font></div>
					</div>
					<b class="b4b d1"></b><b class="b3b d1"></b><b class="b2b d1"></b><b class="b1b"></b>
				</div>
				
				<div id="paperDiv5" style="float:left">
					<b class="b1"></b><b class="b2 d1"></b><b class="b3 d1"></b><b class="b4 d1"></b>
					<div class="b d1" style="width: 100%;height: 100%;">
						<div id="divFont61" style="width: 100%;height: 3mm;margin-top: -1mm;"><font id="font61"></font></div>
						<div id="divFont62" style="width: 100%;height: 3mm;"><font id="font62"></font></div>
						<div id="divFont63" style="width: 100%;height: 3mm;"><font id="font63"></font></div>
						<div id="divFont64" style="width: 100%;height: 3mm;"><font id="font64"></font></div>
						<div id="divFont65" style="width: 100%;height: 3mm;"><font id="font65"></font></div>
						<div id="divFont66" style="width: 100%;height: 3mm;"><font id="font66"></font></div>
						<div id="divFont67" style="width: 100%;height: 3mm;"><font id="font67"></font></div>
						<div id="divFont68" style="width: 100%;height: 3mm;"><font id="font68"></font></div>
						<div id="divFont69" style="width: 100%;height: 3mm;"><font id="font69"></font></div>
						<div id="divFont70" style="width: 100%;height: 3mm;"><font id="font70"></font></div>
						<div id="subCodeShow6_Up" style="width: 100%;height: 3mm;"><font id="subCodeShowFont6_Up">100010010101</font></div>
						<div id="barcode6">
							<div style="width: 5%;height:100%;background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 4%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 1%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 4%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 3%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 1%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 4%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 1%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 4%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 5%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 2%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 3%;height:100%;background-color: #FFF;float: left;"></div>
							<div style="width: 2%;height:100%; background-color: black;float: left;"></div>
							<div style="width: 4%;height:100%;background-color: #FFF;float: left;"></div>
						</div>
						<div id="subCodeShow6" style="width: 100%;height: 3mm;"><font id="subCodeShowFont6">100010010101</font></div>
						<div id="priceShow6_Down" style="width: 100%;height: 3mm;"><font id="priceShowFont6_Down"> ￥89</font></div>
					</div>
					<b class="b4b d1"></b><b class="b3b d1"></b><b class="b2b d1"></b><b class="b1b"></b>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
<script>
	var api = frameElement.api, W = api.opener;
    var ptToMm = 0.35;//每增加1字号对应的毫米
    /* var $_chk_BSU_Show_FieName = null;
    var $_chk_BSU_SubCode_ShowUp = null;
    var $_chk_BSU_Price_ShowDown = null; */
    
	function getFocus(id){
    	document.getElementById(id).value="";
		document.getElementById(id).focus();
    } 
    
    function onNext(){
    	document.getElementById("subCodeDiv1").style.display = "none";
    	document.getElementById("subCodeDiv2").style.display = "block";
    }
    
    function onBack(){
    	document.getElementById("subCodeDiv1").style.display = "block";
    	document.getElementById("subCodeDiv2").style.display = "none";
    }
    function selContent(showId,menuNo){
		for(var i=1;i<3;i++){
			var menu=document.getElementById("menu"+i);
			menu.className="menu";
		}
		menu=document.getElementById("menu"+menuNo).className="select";
		for(var i=1;i<3;i++){
			var content  = document.getElementById("tagContent"+i);
			if (content!=null){
				content.style.display="none";
			}
		}
		var contentFocus=document.getElementById("tagContent"+showId);
		contentFocus.style.display="block";
	}
</script>
<script src="<%=basePath%>data/base/product/product_barcode_addPrintSet.js"></script>
</body>
</html>