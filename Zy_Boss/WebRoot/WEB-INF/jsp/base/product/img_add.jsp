<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/css/base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery-1.11.1.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.form.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/AutoResizeImg.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/UpImgPreview.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
</script> 
<style type="text/css">
.btns{border:0;height:40px;width:100px;background:#f30;color:#fff;border-radius:3px;}
.uppicdiv{position:fixed;left:10px;top:10px;bottom:10px;width:380px;}
.uppictop{float:left;height:35px;width:100%;}
.uppic{float:left;height:290px;width:378px;border:#ddd solid 1px;}
.uppicview{float:left;height:255px;width:378px;}
.uppicview img{float:left;margin:7px 29px;}
.uppicsel{float:left;height:34px;width:378px;position:relative;border-top:#ddd solid 1px;background:#f4f4f4;line-height:34px;color:#f60;font-size:14px;font-weight:700;text-align:center}
.fileup{height: 34px;
    width: 378px;
    filter: alpha(opacity=0);
    -moz-opacity: 0;
    -khtml-opacity: 0;
    opacity: 0;
    position: absolute;
    left: 0;
    top: 0;}
.uppicbt{float:left;height:40px;width:100%;margin-top:10px;text-align:right;}
.piclist{position:fixed;top:10px;right:5px;bottom:10px;width:430px;}
</style>
</head>
<body>
<form name="form1" method="post" action="" id="form1">
<input type="hidden" name="pd_code" id="pd_code" value="${pd_code}"/>
<input type="hidden" name="serverPath" id="serverPath" value="<%=serverPath%>"/>
<div class="uppicdiv">
	<div class="uppictop">
		颜色:
		<span class="ui-combo-wrap" id="span_color"></span>
		<input type="hidden" id="cr_code" name="cr_code" value=""/>
		<label>【大小不要超过200K,不能超过四张图片。】</label>
	</div>
	<div class="uppic">
		<div class="uppicview">
			<img id="imgview" style="display:inherit;" width="320" height="240" />
		</div>
		<div class="uppicsel">选择图片<input class="fileup" type="file" name="uploadify" id="uploadify" /></div>
	</div>
	<div class="uppicbt"><input id="photo_td1" type="button" class="btns" onclick="javascript:handle.saveimg();" value="上传"/></div>
</div>
<div class="piclist">
	<div class="imgslist">
		<ul id="imgList">
			
		</ul>
	</div>
</div>
</form>
</body>
<script src="<%=basePath%>data/base/product/product_img_add.js"></script>
</html>
