<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/css/base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
	.list td{line-height:35px; }
	.main_Input{width: 130px; height: 16px;}
	.width100{width:100px;}
	.mutab {
	    height: 32px;
	    margin: 10px 0 0 11px;
	}
	ul {
	    list-style: none;
	}
	.mutab li {
	    cursor: pointer;
	    display: inline;
	    float: left;
	    margin-right: 5px;
	}
	.select {
	    background: #fff;
	    border-style: solid;
	    border-width: 2px 1px 0;
	    border-color: #08c #ddd #ddd;
	    height: 30px;
	    text-align: center;
	    line-height: 30px;
	    width: 120px;
	    color: #08c;
	    font-weight: 700;
	    cursor: hand;
	}
	.menu {
	    background: #f1f1f1;
	    border-style: solid;
	    border-width: 1px 1px 0;
	    border-color: #ddd;
	    height: 30px;
	    text-align: center;
	    line-height: 30px;
	    width: 120px;
	    cursor: hand;
	}
	.content {
	    border: #ddd 1px solid;
	    height: 460px;
	    width: 98%;
	    background-color: #FFFFFF;
	    margin: 0 0 0 10px;
	}
	.error{
		color:red;
	}
	.opselect{
		border:#ddd solid 1px;
		height:25px;
		line-height:25px;
		padding:0 0 0 5px;
		width:80px;
	}
	.btn_copy{
		background:#f60;
		border:none;
		color:#fff;
		font-weight:700;
		height:30px;
		margin-left:10px;
		overflow:hidden;
		padding:0;
		width:80px;
	}
	.btn_adds{
		background:#08b;
		border:none;
		color:#fff;
		font-weight:700;
		height:26px;
		margin-left:10px;
		overflow:hidden;
		padding:0;
		width:55px;
	}
	.btn_icon{
	    background: #ddd no-repeat scroll;
	    border: 0 none;
	    color: #333;
	    cursor: pointer;
	    height: 27px;
	    margin: 3px;
	    width: 47px;
	}
	.btn_icon:hover {
	    background: #c2c2c2 no-repeat scroll;
	    color: #3366cc;
	}
	.list td{
		padding-left:5px;
	}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.extend.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.messages_zh.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/lundarDate.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/change.js\"></sc"+"ript>");
</script>
<script>
var api = frameElement.api, W = api.opener;
var _callback = api.data.callback;
function doClose(){
	api.close();
}
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<div style="min-width:790px;overflow:hidden;">
<div>
	<table width="100%" cellspacing="0" cellpadding="0">
	    <tr>
	      <td class="top-b border-b">
	        <input id="btn-save" type="button" class="t-btn btn-red" value="保存"/>
	        <input id="btn_close" class="t-btn" type="button" value="返回"/>
	        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>模板名称：</b><input class="main_Input" style="width: 250px;" type="text" maxlength="20" name="pda_name" id="pda_name" value="${product_Assist.pda_name }"/>
	      	<span style="color:red;margin-left: 20px;height:30px;font: 12px Microsoft Yahei;" id="errorTip"></span>
	      	<input type="hidden" id="pda_id" name="pda_id" value="${product_Assist.pda_id }"/>
	      </td>
	    </tr>
    </table>
	<div id="tagContent" style="height:340px;width:750px;border:0px;margin:0px;">
			<table class="pad_t20" border="0">
				<tr class="list first">
					<td align="right" width="90px">规格：</td>
					<td width="140px">
						<input class="main_Input required" type="text" name="pda_size" id="pda_size" value="${product_Assist.pda_size }"/>
					</td>
					<td align="right" width="90px">安全标准：</td>
					<td width="140px">
						<input class="main_Input required" type="text" name="pda_safe" id="pda_safe" value="${product_Assist.pda_safe }"/>
					</td>
					<td align="right" width="90px">产地：</td>
					<td>
						<input class="main_Input required" type="text" name="pda_place" id="pda_place" value="${product_Assist.pda_place }" />
					</td>
				</tr>
				<tr class="list">
					<td align="right">成份：</td>
					<td>
						<input class="main_Input" type="text" name="pda_fill" id="pda_fill" value="${product_Assist.pda_fill }"/>
					</td>
					<td align="right">执行标准：</td>
					<td>
						<input class="main_Input" type="text" name="pda_execute" id="pda_execute" value="${product_Assist.pda_execute }"/>
					</td>
					<td align="right">等级：</td>
      				<td >
      					<input class="main_Input" type="text" name="pda_grade" id="pda_grade" value="${product_Assist.pda_grade }"/>
      				</td>
    			</tr>
    			<tr class="list">
					<td align="right">型号：</td>
					<td>
						<input class="main_Input" type="text" name="pda_model" id="pda_model" value="${product_Assist.pda_model }"/>
					</td>
					<td align="right">相色：</td>
					<td>
						<input class="main_Input" type="text" name="pda_color" id="pda_color" value="${product_Assist.pda_color }"/>
					</td>
      				<td align="right">价格特性：</td>
					<td>
						<span class="ui-combo-wrap" id="span_price">
						</span>
						<input type="hidden" name="pda_price_name" id="pda_price_name"/>
						<input type="hidden" name="pda_price_name_bak" id="pda_price_name_bak" value="${product_Assist.pda_price_name }"/>
					</td>
    			</tr>
    			<tr class="list">
					<td align="right">业务提成金额：</td>
					<td>
						<input class="main_Input" type="text" id="pda_salesman_comm" value="${product_Assist.pda_salesman_comm }" name="pda_salesman_comm" />
					</td>
					<td align="right">业务提成率：</td>
					<td>
						<input class="main_Input" type="text" id="pda_salesman_commrate" value="${product_Assist.pda_salesman_commrate }" name="pda_salesman_commrate" />
					</td>
					<td align="right">手动打折：</td>
      				<td>
      					<span class="ui-combo-wrap" id="span_sale">
						</span>
						<input type="hidden" name="pda_sale" id="pda_sale"/>
						<input type="hidden" name="pda_sale_bak" id="pda_sale_bak" value="${product_Assist.pda_sale }"/>
      				</td>
    			</tr>
				<tr class="list">
					<td align="right">分店变价：</td>
      				<td >
      					<span class="ui-combo-wrap" id="span_change">
						</span>
						<input type="hidden" name="pda_change" id="pda_change"/>
						<input type="hidden" name="pda_change_bak" id="pda_change_bak" value="${product_Assist.pda_change }"/>
      				</td>
      				<td align="right">是否赠品：</td>
      				<td>
      					<span class="ui-combo-wrap" id="span_gift">
						</span>
						<input type="hidden" name="pda_gift" id="pda_gift"/>
						<input type="hidden" name="pda_gift_bak" id="pda_gift_bak" value="${product_Assist.pda_gift }"/>
      				</td>
      				<td align="right">批发价1：</td>
      				<td >
      					<input class="main_Input" type="text"  name="pda_batch_price1" id="pda_batch_price1" value="${product_Assist.pda_batch_price1 }" onkeyup="if(isNaN(value))execCommand('undo')" onafterpaste="if(isNaN(value))execCommand('undo')"/>
      				</td>
    			</tr>
    			<tr class="list">
					<td align="right">会员折扣：</td>
      				<td >
      					<span class="ui-combo-wrap" id="span_vip_sale">
						</span>
						<input type="hidden" name="pda_vip_sale" id="pda_vip_sale"/>
						<input type="hidden" name="pda_vip_sale_bak" id="pda_vip_sale_bak" value="${product_Assist.pda_vip_sale }"/>
      				</td>
      				<td align="right">是否礼品：</td>
      				<td >
      					<span class="ui-combo-wrap" id="span_present">
						</span>
						<input type="hidden" name="pda_present" id="pda_present"/>
						<input type="hidden" name="pda_present_bak" id="pda_present_bak" value="${product_Assist.pda_present }"/>
      				</td>
      				<td align="right">批发价2：</td>
      				<td >
      					<input class="main_Input" type="text"  name="pda_batch_price2" id="pda_batch_price2" value="${product_Assist.pda_batch_price2 }" onkeyup="if(isNaN(value))execCommand('undo')" onafterpaste="if(isNaN(value))execCommand('undo')"/>
      				</td>
    			</tr>
    			<tr class="list">
					<td align="right">允许采购：</td>
      				<td >
      					<span class="ui-combo-wrap" id="span_buy">
						</span>
						<input type="hidden" name="pda_buy" id="pda_buy"/>
						<input type="hidden" name="pda_buy_bak" id="pda_buy_bak" value="${product_Assist.pda_buy }"/>
      				</td>
      				<td align="right">是否积分：</td>
      				<td>
      					<span class="ui-combo-wrap" id="span_point">
						</span>
						<input type="hidden" name="pda_point" id="pda_point"/>
						<input type="hidden" name="pda_point_bak" id="pda_point_bak" value="${product_Assist.pda_point }"/>
      				</td>
      				<td align="right">批发价3：</td>
      				<td >
      					<input class="main_Input" type="text"  name="pda_batch_price3" id="pda_batch_price3" value="${product_Assist.pda_batch_price3 }" onkeyup="if(isNaN(value))execCommand('undo')" onafterpaste="if(isNaN(value))execCommand('undo')"/>
      				</td>
    			</tr>
    			<tr class="list">
					<td align="right">国标码：</td>
      				<td >
      					<input class="main_Input" type="text" name="pda_gbcode" id="pda_gbcode" value="${product_Assist.pda_gbcode }" maxlength="20"/>
      				</td>
      				<td align="right">洗涤说明：</td>
      				<td colspan="3">
      					<input class="main_Input" type="text" name="pda_wash_explain" id="pda_wash_explain" value="${product_Assist.pda_wash_explain }" maxlength="50" style="width: 388px;"/>
      				</td>
    			</tr>
			</table>
	</div>
	</div>
</div>
</form>
<script>
	$().ready(function() {
		$("#form1").validate({
			ignore: "",
			onfocusout : function(element) {
				$(element).valid();
			},
			errorPlacement: function(error, element) { //错误信息位置设置方法
				element.focus();
				$("#errorTip").empty();
				error.appendTo($("#errorTip"));
			},
			rules : {
				pda_batch_price1 : {
					isNumber : true
				},
				pda_batch_price2 : {
					isNumber : true
				},
				pda_batch_price3 : {
					isNumber : true
				},
				pda_salesman_comm : {
					isNumber : true
				},
				pda_salesman_commrate : {
					isDouble : true
				}
			},
			messages : {
				pda_batch_price1 : {
					isNumber : "批发价1只能输入数字"
				},
				pda_batch_price2 : {
					isNumber : "批发价2只能输入数字"
				},
				pda_batch_price3 : {
					isNumber : "批发价3只能输入数字"
				},
				pda_salesman_comm : {
					isNumber : "业务员提成金额只能输入数字"
				},
				pda_salesman_commrate : {
					isDouble : "业务员提成率只能输入0-1之间数字"
				}
			}
		});
	});
</script>
<script src="<%=basePath%>data/base/product/product_add_product_assist.js"></script>
</body>
</html>