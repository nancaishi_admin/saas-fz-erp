<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/hanzi2pinyin.js\"></sc"+"ript>");
</script> 
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" id="tp_id" name="tp_id" value="${type.tp_id}"/>
<div class="shop_box">
	<table class="pad_t20" width="100%" height="90%">
		<tr>
			<td align="right" width="28%">分类名称：</td>
			<td align="left">
				<input class="main_Input"  type="text" name="tp_name" id="tp_name" maxlength="12" value="${type.tp_name}" onblur="javascript:autoChange('tp_name','tp_spell');"
					onclick="javascript:if(event.keyCode==13){autoChange('tp_name','tp_spell');}"/>
			</td>
		</tr>
		<tr>
			<td align="right" width="28%">分类简码：</td>
			<td align="left">
				<input class="main_Input"  type="text" name="tp_spell" id="tp_spell" maxlength="10" value="${type.tp_spell}"/>
			</td>
		</tr>
		<tr>
			<td align="right" width="28%">导购员提成率：</td>
			<td align="left">
				<input class="main_Input"  type="text" name="tp_rate" id="tp_rate" value="${type.tp_rate}" onblur="javascript:checkDiscount();"/>
			</td>
		</tr>
		<tr>
			<td align="right" width="28%">业务员提成率：</td>
			<td align="left">
				<input class="main_Input"  type="text" name="tp_salerate" id="tp_salerate" value="${type.tp_salerate}" onblur="javascript:checkDiscount();"/>
			</td>
		</tr>
	</table>
</div>
</form>
<div class="footdiv">
	<input class="ui-btn ui-btn-sp mrb" id="btn-save" name="btn-save" type="button" value="确定"/>
	<input class="ui-btn mrb" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
<script src="<%=basePath%>data/base/type/type_update.js"></script>
</body>
</html>