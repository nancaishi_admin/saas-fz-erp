<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/css/default.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
</script> 
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" id="sc_id" value="${complaint.sc_id }"/>
<table class="pad_t20" width="430" align="center">	
	<tr style="height:30px; ">
		<td align="right">顾客姓名：</td>
		<td align="left">
			<span>${complaint.sc_username }</span>
		</td>
	</tr>
	<tr style="height:30px; ">
		<td align="right">联系电话：</td>
		<td align="left">
			<span>${complaint.sc_mobile }</span>
		</td>
	</tr>
	<tr style="height:30px; ">
		<td align="right">投诉内容：</td>
		<td align="left" colspan="3">
			<span>${complaint.sc_content }</span>
		</td>
	</tr>
	<tr style="height:30px; ">
		<td align="right">投诉类型：</td>
		<td align="left" colspan="3" id="tdType">
			<c:choose>
			<c:when test="${complaint.sc_type == 1 }">
				产品
			</c:when>
			<c:when test="${complaint.sc_type == 2 }">
				服务
			</c:when>
			<c:when test="${complaint.sc_type == 3 }">
				其他
			</c:when>
			</c:choose>
		</td>
	</tr>
	<c:if test="${complaint.sc_type == 2 }">
	<tr style="height:30px; ">
		<td align="right">被投诉人名称：</td>
		<td align="left">
				${complaint.sc_selman }
		</td>
	</tr>
	</c:if>
	<tr style="height:30px; ">
		<td align="right"><font color="red">*</font>处理意见：</td>
		<td align="left" colspan="3">	
			<textarea id="sc_processinfo" style="font-size: 13px;font-family:Microsoft YaHei;width:250px;height:100px;resize:none;" onkeyup="javascript:handle.countPoint(this,'total');"></textarea>
           	<p style="color: red;">最多输入150个字，还可以输入<input disabled maxLength="4" id="total" name="total" size="3" value="150"/>个字</p>
		</td>
	</tr>
</table>
</form>
<div class="footdiv">
	<input class="btn_confirm" type="button" id="btn-save" name="btn-save" value="确定"/>
	<input class="btn_close" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
<script src="<%=basePath%>data/shop/complaint/complaint_update.js"></script>
</body>
</html>