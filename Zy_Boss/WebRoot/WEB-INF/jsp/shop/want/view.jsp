<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.extend.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.messages_zh.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc" + "ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.size.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/validator.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" id="wt_id" name="wt_id" value="${want.wt_id }"/>
<input type="hidden" id="wt_type" name="wt_type" value="${want.wt_type }"/>
<input type="hidden" id="wt_number" name="wt_number" value="${want.wt_number }"/>
<input type="hidden" id="wt_ar_state" name="wt_ar_state" value="${want.wt_ar_state }"/>
<div class="border">
	<table width="100%" cellspacing="0" cellpadding="0">
	    <tr>
	      <td class="top-b border-b">
	      	<input id="btn-approve" class="t-btn btn-green" type="button" value="审核" style="display:none;"/>
	        <input id="btn-reverse" class="t-btn btn-red" type="button" value="反审核" style="display:none;"/>
	        <input id="btn-receive" class="t-btn btn-red" type="button" value="接收" style="display:none;"/>
	        <input id="btn-reject" class="t-btn btn-bblue" type="button" value="拒收" style="display:none;"/>
	        <input id="btn-reject-confirm" class="t-btn btn-red" type="button" value="确认" style="display:none;"/>
	        
	        
	        <input id="btn-print" class="t-btn btn-cblue" type="button" value="打印预览" onclick="javascript:handle.doPrint();"/>
	        <input id="btn_close" class="t-btn" type="button" value="返回"/>
	        
	        <span style="float:right; padding-right:10px;">
	        	<input type="hidden" id="CurrentMode" value="0"/>
				<a id="sumBtn" name="mode-btn" class="t-btn btn-bl"  onclick="showMode.display(2);"><i></i>汇总</a>
			    <a id="listBtn" name="mode-btn" class="t-btn btn-blc on"  onclick="showMode.display(0);"><i></i>列表</a>
			    <a id="sizeBtn" name="mode-btn" class="t-btn btn-bc" onclick="showMode.display(1);"><i></i>尺码</a>
            </span>
	      </td>
	    </tr>
    </table>
</div>
<div class="grid-wrap">
    <div id="list-grid" style="display:'';width: 100%;">
        <table id="grid"></table>
        <div id="page"></div>
    </div>
    <div id="size-grid" style="display:none; width: 100%;">
        <table id="sizeGrid"></table>
        <div id="sizePage"></div>
    </div>
    <div id="sum-grid" style="display:none;width: 100%;">
       <table id="sumGrid"></table>
       <div id="sumPage"></div>
   </div>
</div>
<div >
	<table width="100%">
		<tr class="list first">
			<td align="right" width="60px">要货门店：</td>
			<td width="165px">
				<input class="main_Input w146" type="text" readonly="readonly" name="shop_name" id="shop_name" value="${want.shop_name }"/>
				<input type="hidden" name="wt_shop_code" id="wt_shop_code" value="${want.wt_shop_code }" />
			</td>
			<td align="right" width="60px">发货仓库：</td>
			<td width="165px">
				<input class="main_Input w146" type="text" readonly="readonly" name="outdepot_name" id="outdepot_name" value="${want.outdepot_name }"/>
				<input type="hidden" name="wt_outdp_code" id="wt_outdp_code" value="${want.wt_outdp_code }" />
			</td>
			<td align="right" width="60px">收货仓库：</td>
			<td width="165px">
				<input class="main_Input w146" type="text" readonly="readonly" name="indepot_name" id="indepot_name" value="${want.indepot_name }" style="width:${oper eq 'receive'?'122':'146' }px"/>
				<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryInDepot();" style="display:${oper eq 'receive'?'':'none' };"/>
				<input type="hidden" name="wt_indp_code" id="wt_indp_code" value="${want.wt_indp_code }" />
			</td>
			<td align="right" width="60px">单据性质：</td>
			<td>
				<input class="main_Input w146" type="text" name="wt_property" id="wt_property" readonly="readonly" value="${want.wt_property }" />
			</td>
		</tr>
		<tr class="list">
			<td align="right">申请数量：</td>
			<td>
				<input class="main_Input w146" type="text" name="wt_applyamount" id="wt_applyamount" value="${want.wt_applyamount }" readonly="readonly"/>
			</td>
			<td align="right">申请金额：</td>
			<td>
				<input class="main_Input w146" type="text" id="wt_applymoney" value="${want.wt_applymoney }" readonly="readonly"/>
			</td>
			<td align="right">申请日期：</td>
			<td>
				<input readonly type="text" class="main_Input w146"  name="wt_date" id="wt_date" value="${want.wt_date }" />
			</td>
			<td align="right">经办人员：</td>
			<td>
				<input class="main_Input w146" type="text" readonly="readonly" name="wt_manager" id="wt_manager" value="${want.wt_property }"/>
			</td>
		</tr>
		<tr class="list last">
			<td align="right">实发数量：</td>
			<td>
				<input class="main_Input w146" type="text" readonly="readonly" name="wt_sendamount" id="wt_sendamount" value="${want.wt_sendamount }"/>
			</td>
			<td align="right">实发金额：</td>
			<td>
				<input class="main_Input w146" type="text" readonly="readonly" name="wt_sendmoney" id="wt_sendmoney" value="${want.wt_sendmoney }"/>
			</td>
			
			<td align="right">手工单号：</td>
			<td>
				<input class="main_Input w146" type="text" name="wt_handnumber" id="wt_handnumber" readonly="readonly" value="${want.outdepot_name }" />
			</td>
			<td align="right">备注：</td>
			<td>
				<input class="main_Input w146" type="text" name="wt_remark" id="wt_remark" value=""/>
			</td>
		</tr>
	</table>
</div>
</form>
<script src="<%=basePath%>data/shop/want/want_view.js"></script>
</body>
</html>