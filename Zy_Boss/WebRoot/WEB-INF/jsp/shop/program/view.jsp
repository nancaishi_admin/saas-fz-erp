<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/css/base.css" rel="stylesheet"  type="text/css" />
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/css/style.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery-1.11.1.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.form.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.form.js\"></sc"+"ript>"); 
</script> 
<script src="<%=basePath%>resources/util/AutoResizeImg.js"></script>
<script src="<%=basePath%>resources/util/UpImgPreview.js"></script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<div class="mainwra" style="overflow:auto;">
  <div class="border" style="border-bottom:#ddd solid 1px;padding-bottom:10px;">
      <table width="100%" cellpadding="0" cellspacing="0">
          <tr>
              <td colspan="6" class="top-b border-b" >
              <a class="t-btn" id="btn_close">返回</a>
              </td>
          </tr>
          <tr>
	        <td width="520" valign="top">
	           <table id="showCondition" width="520" cellpadding="0" cellspacing="0">
	                <tr class="list first">
	                    <td width="80" style="text-align: right;">店铺：</td>
	                    <td style="text-align: left;">
	                    	<input type="text" id="shop_name" name="shop_name" readonly="readonly" class="main_Input atfer_select w146" style="width:158px" value="${program.sp_shop_name}"/>
	                    </td>
	                </tr>
	                <tr class="list ">
	                    <td style="text-align: right;">方案标题：</td>
	                    <td style="text-align: left;">
	                    	<input class="main_Input" style="width:182px;font-size: 10pt;font-family: 黑体,宋体(GB);" id="sp_title" name="sp_title" value="${program.sp_title}" readonly="readonly"/>
	                    </td>
	                </tr>
	                <tr class="list ">
	                    <td style="text-align: right;">方案说明：</td>
	                    <td style="text-align: left;">
	                    <textarea rows="6" cols="53" id="sp_info"  name="sp_info" readonly="readonly" style="width:196px;font-size: 10pt;font-family: 黑体,宋体(GB);">
	                    ${program.sp_info}
	                    </textarea>
	                    </td>
	                </tr>
	                
	           </table>
	        </td>
	        <td>
	           <div style="width:350px;height:300px;float:left;border:1px solid #CFCBCA;margin-top:20px;">
	              <table id="imageDiv" width="350" align="center">
	                <tr>
	                  <td height="260">
	                  	<div style="width:auto;height:auto;margin:10px 0 0 10px;float:left;border:1px #cfcbca solid;">
	                      <img id="image" src='<%=serverPath%>${program.sp_imgpath}' onload="AutoResizeImage(320,240,this);" /> 
	                  	</div>
	                  </td>
	                </tr>
	              </table>
	            </div>              
	
	        </td>
	      </tr>
	      <tr>
	        <td colspan="2">&nbsp;</td>
	      </tr>
    </table>
  </div>
</div>
</form>
</body>
<script>
var api = frameElement.api, W = api.opener;
var THISPAGE = {
	init:function (){
		this.initEvent();
	},
	initEvent:function(){
		$("#btn_close").click(function(){
			W.isRefresh = false;
			api.close();
		});
	}
};
THISPAGE.init();
</script>
</html>