<%@ page contentType="text/html; charset=utf-8" language="java"%>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet"
	type="text/css" />
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet"
	type="text/css" />
<link href="<%=basePath%>resources/grid/css/ui.base.css"
	rel="stylesheet" type="text/css" />
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css"
	rel="stylesheet" type="text/css" />

<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery-1.11.1.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc" + "ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc"+"ript>");
</script>
<script>
	//设置显示哪些价格
	function setShowPrice(){
		if($("#SellPrice").attr("checked")){$("td[show='SellPrice']").show();$("#SellPrice").val(1)}else{$("td[show='SellPrice']").hide();$("#SellPrice").val(0)}
		if($("#VipPrice").attr("checked")){$("td[show='VipPrice']").show();$("#VipPrice").val(1)}else{$("td[show='VipPrice']").hide();$("#VipPrice").val(0)}
		if($("#SortPrice").attr("checked")){$("td[show='SortPrice']").show();$("#SortPrice").val(1)}else{$("td[show='SortPrice']").hide();$("#SortPrice").val(0)}
		if($("#CostPrice").attr("checked")){$("td[show='CostPrice']").show();$("#CostPrice").val(1)}else{$("td[show='CostPrice']").hide();$("#CostPrice").val(0)}
	}
	//判断是否显示价格
	$(function(){
		if($("#SellPrice").attr("checked")){
			$("td[show='SellPrice']").show();
			$("#SellPrice").val(1)
		}else{
			$("td[show='SellPrice']").hide();
			$("#SellPrice").val(0)
		}
		if($("#VipPrice").attr("checked")){$("td[show='VipPrice']").show();$("#VipPrice").val(1)}else{$("td[show='VipPrice']").hide();$("#VipPrice").val(0)}
		if($("#SortPrice").attr("checked")){$("td[show='SortPrice']").show();$("#SortPrice").val(1)}else{$("td[show='SortPrice']").hide();$("#SortPrice").val(0)}
		if($("#CostPrice").attr("checked")){$("td[show='CostPrice']").show();$("#CostPrice").val(1)}else{$("td[show='CostPrice']").hide();$("#CostPrice").val(0)}
	});
	
</script>


</head>
<body>
	<form name="form1" method="post" action="" id="form1">
		<input type="hidden" id="flushFlag" name="flushFlag" value="" /> 
		<input type="hidden" id="sp_is_sellprice" value="${price.sp_is_sellprice }"/>
		<input type="hidden" id="sp_is_vipprice" value="${price.sp_is_vipprice }"/>
		<input type="hidden" id="sp_is_sortprice" value="${price.sp_is_sortprice }"/>
		<input type="hidden" id="sp_is_costprice" value="${price.sp_is_costprice }"/>
		<input type="hidden" id="sp_number" name="sp_number" value="${price.sp_number }"/>
		<input type="hidden" id="CurrentMode" value="0"/>
		<!-- 标记店铺类型 -->
		<div class="mainwra">
			<div class="border">
				<table width="100%" cellspacing="0" cellpadding="0">
				    <tr>
				      <td colspan="2" class="top-b border-b">
				        <input id="btn-approve" class="t-btn btn-green" type="button" value="审核" style="display:none;"/>
				        <input id="btn-print" class="t-btn btn-cblue" type="button" value="打印预览" onclick="javascript:handle.doPrint();"/>
				        <input id="btn_close" class="t-btn" type="button" value="返回"/>
				        <span id="errorTip" class="errorTip"></span>
				      </td>
				    </tr>
				    <tr class="list first">
				    	<td width="600">分店列表：
				            <input type="text" style="width:500px;" value="${price.sp_sp_name }" readonly="readonly" class="main_Input" />
				            <input type="hidden" id="sp_shop_code" name="sp_shop_code" value="${price.sp_shop_code }" />
				        </td>
				        <td >单据编号：
				            <input type="text" value="${price.sp_number }" readonly="readonly" class="main_Input" />
				        </td>
				      </tr>
				      <tr class="list last">
				          <td >
				            摘要信息：
				            <input style="width:500px;" type="text"  id="sp_remark" value="${price.sp_remark }" readonly="readonly" class="main_Input"/>
				         </td>
				         <td>
				            经办人员：
				            <input type="text" id="sp_manager" readonly="readonly" class="main_Input" value="${price.sp_manager }"/>
				        </td>
					</tr>
			    </table>
			</div>

			<div class="grid-wrap">
				<div id="listMode" style="display:'';">
					<table id="grid"></table>
					<div id="page"></div>
				</div>
			</div>
		</div>
	</form>
	<script src="<%=basePath%>data/shop/price/price_view.js"></script>
</body>
</html>