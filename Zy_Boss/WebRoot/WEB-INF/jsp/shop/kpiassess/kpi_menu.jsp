<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <meta http-equiv=Content-Type content="text/html; charset=utf-8"/>
    <link type="text/css" rel="stylesheet" href="<%=basePath%>resources/css/iconfont/iconfont.css"/>
    <link type="text/css" rel="stylesheet" href="<%=basePath%>resources/css/info.css"/>
	<script type="text/javascript">
		document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
		document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=false&skin=mac\"></sc"+"ript>");
	</script>
    <script>
		function addTags(obj,name){
			window.parent.addTags(obj,name);
		}
	</script>	
</head>
<body id="main_content">
<div class="main-menu">
	<div class="basebock">
    	<div class="icon-menu">
        	<div class="icons">
				<ul>
					<li>
						<a class="last_menu" dataType="iframe" dataLink="sys/kpi/to_list" id="kpi_0" jerichotabindex="kpi_0" 
								onclick="javascript:addTags(this,'KPI指标管理');">
							<i class="iconfont bg_green">&#xe644;</i>
							<span class="last_menu">KPI指标管理</span>
						</a>
					</li>
					<li>
						<a class="last_menu" dataType="iframe" dataLink="base/empgroup/to_list" id="kpi_1" jerichotabindex="kpi_1" 
								onclick="javascript:addTags(this,'员工组管理');">
							<i class="iconfont bg_green">&#xe636;</i>
							<span class="last_menu">员工组管理</span>
						</a>
					</li>
					<li>
						<a class="last_menu" dataType="iframe" dataLink="shop/kpiassess/to_list" id="kpi_2" jerichotabindex="kpi_2" 
								onclick="javascript:addTags(this,'考核制定');">
							<i class="iconfont bg_green">&#xe632;</i>
							<span class="last_menu">考核制定</span>
						</a>
					</li>
					<li>
						<a class="last_menu" dataType="iframe" dataLink="shop/kpipk/to_list" id="kpi_3" jerichotabindex="kpi_3" 
								onclick="javascript:addTags(this,'PK制定');">
							<i class="iconfont bg_green">&#xe929;</i>
							<span class="last_menu">PK制定</span>
						</a>
					</li>
				</ul>
            </div>
        </div>
        <div class="list-menu">
        </div>  
    </div>
</div>
</body>
</html>

