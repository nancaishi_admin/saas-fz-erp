<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc" + "ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.size.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/validator.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
</script>

</head>
<body>
<form action="" id="form1" name="form1" method="post" >
	<div class="border">
		<table width="100%" cellpadding="0" cellspacing="0">
	  		<tr>
	  			<td colspan="8" class="top-b border-b">
	  				<input type="button" class="t-btn btn-red" id="btn-save" onclick="javascript:handle.save();" value="保存"/>
	         		<a class="t-btn" id="btn_close" >返回</a>
	        	</td>
		  	</tr>
  			<tr class="list first last">
	  			<td align="right" width="80">考核范围：</td>
			    <td width="170">
					<span class="ui-combo-wrap" id="span_ka_type">
					</span>
					<input type="hidden" id="ka_type" name="ka_type" value=""/>
			    </td>
	  			<td align="right" width="80">开始时间：</td>
				<td width="160">
					<input type="text" class="main_Input w146 Wdate" readonly="readonly" id="begindate" name="begindate" 
						onclick="WdatePicker({maxDate:'#F{$dp.$D(\'enddate\',{d:0})}'})"  value="" onchange="javascript:Utils.changeDate();"/>
				</td>
			    <td align="right" width="80">结束时间：</td>
				<td width="170">
					<input type="text" class="main_Input w146 Wdate" readonly="readonly" id="enddate" name="enddate" 
						onclick="WdatePicker({minDate:'#F{$dp.$D(\'begindate\',{d:0})}'})" value="" onchange="javascript:Utils.changeDate();"/>
				</td>
			    <td align="right" width="80">备注：</td>
			    <td>
			    	<input type="text" class="main_Input" id="ka_remark" name="ka_remark"/>
			    </td>
	    	</tr>
		</table>
		<!-- <table cellpadding="0" cellspacing="0"  class="top-b border-t" width="100%">
			<tr>
				<td>
				
				</td>
			</tr>
		</table> -->
	</div>
	<div class="grid-wrap" style="float:left;margin-top: 10px;">
	    <table id="grid">
	    </table>
    	<div id="page"></div>
	</div>
	
	<div id="divDetail" class="grid-wrap" style="float:left;margin-left:15px;margin-top: 10px;">
	    <table id="detailGrid">
	    </table>
    	<div id="pageDetail"></div>
	</div>
	
</form>
<script src="<%=basePath%>data/shop/kpiassess/kpiassess_add.js"></script>
</body>
</html>