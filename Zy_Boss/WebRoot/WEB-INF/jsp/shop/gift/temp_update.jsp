<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.extend.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.messages_zh.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc" + "ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/validator.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.imagePreview.js\"></sc"+"ript>");
</script>
<script>
	var api = frameElement.api, W = api.opener;
	//控制控件只能输入数字,含.(小数点) 
	 function onlyPositiveNumber(){ 
			if(!(((window.event.keyCode >= 48) && (window.event.keyCode <= 57)) 
					|| (window.event.keyCode == 13) || (window.event.keyCode == 46) 
					)){ 
				window.event.keyCode = 0 ; 
			} 
	 }
	function valNumber(obj){
		var num = obj.value; 
		if(num.length > 0){
			if(isNaN(num)){
				obj.value=parseFloat($("#txtUnitPrice").text()).toFixed(2);
				window.parent.$.dialog.tips('请输入数字!',2,'32X32/hits.png');
				return false;
			}
		}
	}
	
	//图片放大
	$(function(){
		$("#ProductPhoto").preview();	  
	});
</script>
<style type="text/css">
h3{font-size:16px;}
.pricebt{background:#f60;padding:6px 5px;color:#fff;cursor: pointer;margin-left:5px;}
.price-pop{background:#fed;position:absolute;z-index:999;line-height:30px;top:100px;right:100px;border:1px solid #bbb;width:160px;background-color:#fff;}
.pricebt,.price-pop{-moz-border-radius: 3px;      /* Gecko browsers */
    	-webkit-border-radius: 3px;   /* Webkit browsers */
    	border-radius:3px;}
.priceslist{width:160px;}
.priceslist li{border-bottom:#aaa dotted 1px;color:#666;cursor:pointer;display:inline;float:left;width:160px;text-align:right;}
.priceslist li:hover{background:#eee;}
.priceslist li span{color:#f60;float:right;width:80px;text-align:left;}
.subtitles,.pricebt,.priceslist{font-family:"微软雅黑","Yahei";}
.poptxts{background:url(<%=basePath%>resources/grid/images/poptxt.png);width:92px;height:58px;position:absolute;padding:0 5px;line-height:25px;}
</style>
</head>
<body>
<body id="main_content" style="overflow-x:hidden">
<input type="hidden" id='fromJsp' name="fromJsp" value="${fromJsp }" /><!-- 标记来自哪个页面 -->
<input type="hidden" id='needVaildateStock' name="needVaildateStock" value="${needVaildateStock }" />
<input type="hidden" id='currentPdCode' name="currentPdCode" value="${pd_code }" />
<!-- <input type="hidden" id='priceModifyed' name="priceModifyed" value="" /> -->
<input type="hidden" id='ftpURL' name="ftpURL" value="<%=serverPath %>" />
<div class="mainwra">
    <div style="float:left;width:640px;height:96px;overflow:hidden;">
    	<table width="100%" id="ProductInfo" style="display:none;">
    		<tr>
    			<td rowspan="3" width="130">
    				<img id="ProductPhoto" src="<%=basePath%>resources/grid/images/nophoto.png" style="cursor:pointer;border:#ddd solid 1px;" width="120" height="80" />
    			</td>
    			<td rowspan="2" width="400">
    				<h2><span id="txtNo" ></span>(<span id="txtName" ></span>)</h2>
    				<p>
    					<span id="txtYear" style="font-weight:bold"></span> 年份&nbsp;
    					<span id="txtSSName" style="font-weight:bold"></span>&nbsp;
    					<span id="txtTpName" style="font-weight:bold"></span>&nbsp;
    					<span id="txtBdName" style="font-weight:bold"></span>
    				</p>
    				<p>零售价：
    					<input type="text" id="txtUnitPriceShow" name="txtUnitPriceShow" class="ui-input ui-input-ph w120" style="width: 50px;" 
    							disabled="disabled" onkeypress="javascript:onlyPositiveNumber()" onblur="return valNumber(this);"/>
              			<span id="txtUnitPrice" style="display:none"></span>
              			<span id="distributionPriceSpan" style="display:none;">&nbsp;&nbsp;配送价：</span>
    					<input type="hidden" id="txtSortPriceShow" name="txtSortPriceShow" class="ui-input ui-input-ph w120" style="width: 50px;" onkeypress="javascript:onlyPositiveNumber()" onblur="return valNumber(this);"/>
              			<span id="txtSortPrice" style="display:none"></span>
    				</p>
    			</td>
    		</tr>
    	</table>
    </div>
    
    <div style="margin-top:5px;width:100%;">
      <div id="noDetail" class="grid-wrap" style="float:left;">
          <table id="sizeDetail">
          </table>
          <div id="sizePage"></div>
      </div>
    </div>
    
	<div style="float:left;margin-top:5px;width:100%;">
		<span style="float:left;color:red;width:180px;line-height:30px;" id="spanDescribe">注：录入数量
		</span>
	    <span style="float:left;width:200px;line-height:30px;">快捷保存：按<strong>Ctrl+Enter</strong>键或按<strong>+</strong>键</span>
        <span> 
        
        <c:if test="${needVaildateStock == 1 }">
       	<span id="chkVaildateStock" style="float:left;width:100px;display:none;">
			<label class="chk over" style="margin-top: 6px;">
				<input name="box" type="checkbox" checked="checked" />验证库存量
			</label>
		</span>
		</c:if>
       	<c:if test="${needVaildateStock != 1 }">
       	<span id="chkVaildateStock" style="float:left;width:100px;">
			<label class="chk over" style="margin-top: 6px;">
				<input name="box" type="checkbox" />验证库存量
			</label>
		</span>
        </c:if>
        <span id="chkRealStock" style="float:left;width:80px;">
			<label class="chk over" style="margin-top: 6px;" title="显示实际库存">
				<input name="box" type="checkbox" />实际库存
			</label>
		</span>
		<span id="chkUseableStock" style="float:left;width:80px;">
			<label class="chk over" style="margin-top: 6px;">
				<input name="box" type="checkbox" />可用库存
			</label>
		</span>
          <a class="ui-btn mrb" style="display:none;float:right;" id="btnExit">关闭</a>
          <input title="按Ctrl+Enter键或按+键" class="t-btn btn-red" style="display:none;width:30px;float:right;" id="btnSave" value="保存"/>
		  <input class="t-btn btn-red" style="display:none;width:90px;float:right;" id="btnSaveAndNext" value="保存并下一条"/>
        </span>
	</div>
</div>
<script src="<%=basePath%>data/shop/gift/gift_temp_update.js"></script>
</body>
</html>
