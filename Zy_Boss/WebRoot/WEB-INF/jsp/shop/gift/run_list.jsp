<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery-1.11.1.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/comm-search.js\"></sc"+"ript>");
</script>
<title></title>
</head>
<body id="main_content">
<form name="form1" method="post" action="" id="form1">
<input type="hidden"  name="gir_pd_code"   id="gir_pd_code"  value="${gi_pd_code }"/>
<input type="hidden" name="gir_shop_code" id="gir_shop_code" value="${gi_shop_code }"/>
<div class="mainwra">
	<div class="mod-search cf">
		<div class="fl">
			<div id="filter-menu" class="ui-btn-menu fl">
				<span style="float:left" class="ui-btn menu-btn">
					 <strong>查询条件</strong>
					 	<input type="text" class="main_Input Wdate-select" readonly="readonly" id="begindate" name="begindate" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'enddate\',{d:0})}'})"  value="${param.begindate}"/>
						-
						<input class="main_Input Wdate-select" readonly="readonly" id="enddate" name="enddate" onclick="WdatePicker({minDate:'#F{$dp.$D(\'begindate\',{d:0})}'})"   value="${param.enddate}"/>
					<b></b>
				</span>
	 			<div class="con" style="width:430px" >
					<ul class="ul-inline">
						<li>
							<table class="searchbar">
								<tr>     
								  	 <td id="rqxz">日期选择：
				        				<label class="radio" id="date_today" style="width:30px">
			        						<input name="redio1" type="radio" id="theDate" onclick="javascript:dateRedioClick('theDate');" value="1"  checked="checked"/>今天
			        					</label>
			        					<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="yesterday"  onclick="javascript:dateRedioClick('yesterday');" value="2" />昨天
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theWeek"  onclick="javascript:dateRedioClick('theWeek');" value="3" />本周
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theMonth"  onclick="javascript:dateRedioClick('theMonth');" value="4" />本月
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:40px">
								  	 		<input name="redio1" type="radio" id="theSeason"  onclick="javascript:dateRedioClick('theSeason');" value="5" />本季度
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theYear"  onclick="javascript:dateRedioClick('theYear');" value="6" />本年
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="lastYear"  onclick="javascript:dateRedioClick('lastYear');" value="7" />去年
				        				</label>
				        				<input type="hidden" id="searDate"  value="${param.searDate}"/>
							  	 	</td>
								 </tr>
								  <tr>
									 <td>&nbsp;&nbsp;&nbsp;流水号：
		    							<input type="text" style="width:200px;" name="gir_number" id="gir_number" class="main_Input" value=""/>
									</td>
								</tr>
							</table>
						</li>
						<li style="float:right;">
							<a class="ui-btn ui-btn-sp mrb" id="btn_ok">确认</a>
							<a class="ui-btn mrb" id="btn_reset">重置</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="fl-m">
			<a class="ui-btn fl mrb" id="search">查询</a>
			<a href="javascript:void(0);" class="ui-btn ui-btn-sp mrb" id="btn-back" >返回</a>
		</div>
	 </div>  
	<div class="grid-wrap">
	    <table id="grid">
	    </table>
    	<div id="page"></div>
	</div>
</div>

</form>
<script src="<%=basePath%>data/shop/gift/gift_run_list.js"></script>
<script>
function LG_StateClick(value){
	$("#LG_State").val(value);
}

//重置
		function reset(){
			document.getElementById("LG_SI_Name").value="";
			document.getElementById("LG_SI_Code").value="";
			document.getElementById("LG_PI_NO").value="";
			document.getElementById("LG_State").value= '0';
			document.getElementById("begindate").value=DateToFullDateTimeString(new Date());
			document.getElementById("enddate").value=DateToFullDateTimeString(new Date());
			document.getElementById("searDate").value="1";
			document.form1.redio1[0].checked=true;
			document.form1.redio2[0].checked=true;
	}
</script>
</body>
</html>
