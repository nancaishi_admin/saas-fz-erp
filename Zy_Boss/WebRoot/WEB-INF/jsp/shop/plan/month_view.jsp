<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc" + "ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.size.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/validator.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" name="mp_number" id="mp_number" value="${plan.mp_number }" />
<div class="border">
	<table width="100%" cellspacing="0" cellpadding="0">
	    <tr>
	      <td class="top-b border-b">
	      	<input id="btn-approve" class="t-btn btn-red" type="button" value="审核" style="display:none;"/>
	      	<a class="t-btn" id="btn_close" >返回</a>
	      </td>
	    </tr>
    </table>
		<table width="100%">
			<tr class="list first">
				<td align="right" width="80px">计划年份：</td>
				<td width="165px">
					<input class="main_Input w146" type="text" name="mp_year" id="mp_year" value="${plan.mp_year }" readonly="readonly"/>
				</td>
				<td align="right" width="80px">计划月份：</td>
				<td width="165px">
					<input class="main_Input w146" type="text" name="mp_month" id="mp_month" value="${plan.mp_month }" readonly="readonly"/>
				</td>
				<td align="right" width="80px">店铺名称：</td>
				<td>
					<input class="main_Input w146" type="text" name="shop_name" id="shop_name" value="${plan.shop_name }" readonly="readonly"/>
				</td>
			</tr>
			<tr class="list last">
				<td align="right">计划金额：</td>
				<td>
					<input class="main_Input w146" type="text" name="mp_sell_money" id="mp_sell_money" value="${plan.mp_sell_money }" readonly="readonly"/>
				</td>
				<td align="right">备注：</td>
				<td>
					<input class="main_Input w146" type="text" name="mp_remark" id="mp_remark" value="${plan.mp_remark }" readonly="readonly"/>
				</td>
			</tr>
		</table>
		
		<div class="grid-wrap">
	        <table id="grid">
	        </table>
	        <div id="page"></div>
	    </div> 
	
</form>
<script src="<%=basePath%>data/shop/plan/month_plan_view.js"></script>
</body>
</html>