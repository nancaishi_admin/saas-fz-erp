package zy.dao.sys.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.sys.MainDAO;
import zy.entity.sys.menu.T_Sys_Menu;
import zy.entity.sys.set.T_Sys_Set;
import zy.entity.sys.user.T_Sys_User;
import zy.util.CommonUtil;
import zy.vo.sys.MainVO;
@Repository
public class MainDAOImpl extends BaseDaoImpl implements MainDAO{

	@Override
	public void login(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer();
		sql.append(" select us_id,us_code,us_name,us_ro_code,us_shop_code,u.companyid");
		sql.append(" ,s.sp_name as shop_name,s.sp_upcode as shop_upcode,us_end,us_limit,us_pay,us_lock");
		sql.append(" ,co_code,co_name as companyname,co_shops,co_users,co_ver,sp_init,co_type");
		sql.append(" ,sp_shop_type as shoptype");
		sql.append(" ,(select sp_shop_type FROM t_base_shop s1 WHERE s1.sp_code=s.sp_upcode AND s1.companyid=s.companyid LIMIT 1) AS shop_uptype");
		sql.append(" from t_sys_user u");
		sql.append(" join t_sys_company");
		sql.append(" on co_id=companyid");
		sql.append(" join t_base_shop s");
		sql.append(" on sp_code=u.us_shop_code");
		sql.append(" and s.companyid=u.companyid");
		sql.append(" where 1=1");
		sql.append(" and us_state=0");
		sql.append(" and us_pass=:us_pass");
		sql.append(" and us_account=:us_account");
		sql.append(" and co_code=:co_code");
		sql.append(" and sp_end>SYSDATE()");
		T_Sys_User user = namedParameterJdbcTemplate.queryForObject(sql.toString(), 
				param, 
				new BeanPropertyRowMapper<>(T_Sys_User.class));
		sql.setLength(0);
		sql.append(" SELECT mn_id,mn_code,mn_name,rm_limit as mn_limit,mn_upcode,mn_url,mn_icon,mn_style,mn_child,mn_app");
		sql.append(" FROM t_sys_rolemenu r");
		sql.append(" JOIN t_sys_menu m");
		sql.append(" ON mn_code=rm_mn_code");
		sql.append(" WHERE rm_ro_code=:rolecode");
		sql.append(" AND mn_state=0");
		sql.append(" AND rm_state=0");
		sql.append(" AND r.companyid=:companyid");
		sql.append(" ORDER BY mn_order");
		param.put("rolecode", user.getUs_ro_code());
		param.put(CommonUtil.COMPANYID,user.getCompanyid());
		List<T_Sys_Menu> menuList = namedParameterJdbcTemplate.query(sql.toString(), 
				param, 
				new BeanPropertyRowMapper<>(T_Sys_Menu.class));
		param.put(CommonUtil.KEY_USER, user);
		param.put(CommonUtil.KEY_MENU, menuList);
		param.put(CommonUtil.KEY_MENULIMIT, MainVO.buildMenuLimit(menuList));
		
		sql.setLength(0);
		sql.append(" update t_sys_user");
		sql.append(" set us_last=sysdate()");
		sql.append(" where us_id=:us_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(user));
		
		sql.setLength(0);
		sql.append("SELECT st_id,companyid,st_buy_calc_costprice,st_buy_os,st_batch_os,st_batch_showrebates,st_ar_print,st_blank,st_subcode_rule,st_subcode_isrule,");
		sql.append(" st_allocate_unitprice,st_check_showstock,st_check_showdiffer,st_force_checkstock,st_useable,st_use_upmoney ");
		sql.append(" FROM t_sys_set");
		sql.append(" WHERE companyid = :companyid");
		sql.append(" LIMIT 1");
		try{
			T_Sys_Set set = namedParameterJdbcTemplate.queryForObject(sql.toString(), param,
					new BeanPropertyRowMapper<>(T_Sys_Set.class));
			param.put(CommonUtil.KEY_SYSSET, set);
		}catch(Exception e){
		}
		
	}

}
