package zy.controller.batch;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.approve.T_Approve_Record;
import zy.entity.batch.settle.T_Batch_Settle;
import zy.entity.batch.settle.T_Batch_SettleList;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.batch.settle.BatchSettleService;
import zy.util.CommonUtil;
import zy.util.StringUtil;
import zy.vo.batch.BatchSettleVO;

@Controller
@RequestMapping("batch/settle")
public class BatchSettleController extends BaseController{
	@Resource
	private BatchSettleService batchSettleService;
	
	@RequestMapping(value = "to_list", method = RequestMethod.GET)
	public String to_list() {
		return "batch/settle/list";
	}
	@RequestMapping(value = "to_list_detail", method = RequestMethod.GET)
	public String to_list_detail() {
		return "batch/settle/list_detail";
	}
	@RequestMapping(value = "to_add", method = RequestMethod.GET)
	public String to_add() {
		return "batch/settle/add";
	}
	@RequestMapping(value = "to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer st_id,Model model,HttpSession session) {
		T_Sys_User user = getUser(session);
		T_Batch_Settle settle = batchSettleService.load(st_id);
		if(null != settle){
			batchSettleService.initUpdate(settle.getSt_number(), user.getUs_id(), user.getCompanyid());
			model.addAttribute("settle",settle);
			model.addAttribute("client",batchSettleService.loadClient(settle.getSt_client_code(), settle.getCompanyid()));
		}
		return "batch/settle/update";
	}
	@RequestMapping(value = "to_view", method = RequestMethod.GET)
	public String to_view(@RequestParam Integer st_id,Model model) {
		T_Batch_Settle settle = batchSettleService.load(st_id);
		if(null != settle){
			model.addAttribute("settle",settle);
			model.addAttribute("client",batchSettleService.loadClient(settle.getSt_client_code(), settle.getCompanyid()));
		}
		return "batch/settle/view";
	}
	@RequestMapping(value = "to_view/{number}", method = RequestMethod.GET)
	public String to_view(@PathVariable String number,Model model,HttpSession session) {
		T_Batch_Settle settle = batchSettleService.load(number, getCompanyid(session));
		if(null != settle){
			model.addAttribute("settle",settle);
			model.addAttribute("client",batchSettleService.loadClient(settle.getSt_client_code(), settle.getCompanyid()));
		}
		return "batch/settle/view";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = false) Integer st_ar_state,
			@RequestParam(required = false) String st_client_code,
			@RequestParam(required = false) String st_manager,
			@RequestParam(required = false) String st_number,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        if(CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype())){//总公司、分公司
        	param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		}else{//自营、加盟、合伙
			param.put(CommonUtil.SHOP_CODE, user.getShop_upcode());
		}
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("begindate", pageForm.getBegindate());
        param.put("enddate", pageForm.getEnddate());
        param.put("st_ar_state", st_ar_state);
        param.put("st_client_code", st_client_code);
        param.put("st_manager", StringUtil.decodeString(st_manager));
        param.put("st_number", StringUtil.decodeString(st_number));
		return ajaxSuccess(batchSettleService.page(param));
	}
	
	@RequestMapping(value = "detail_list/{st_number}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object detail_list(@PathVariable String st_number,HttpSession session) {
		return ajaxSuccess(batchSettleService.detail_list(st_number,getCompanyid(session)));
	}
	
	@RequestMapping(value = "temp_list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_list(HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("stl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(batchSettleService.temp_list(params));
	}
	
	@RequestMapping(value = "temp_save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_save(String ci_code,HttpSession session) {
		batchSettleService.temp_save(ci_code, getUser(session));
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateDiscountMoney", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateDiscountMoney(T_Batch_SettleList temp) {
		batchSettleService.temp_updateDiscountMoney(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updatePrepay", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updatePrepay(T_Batch_SettleList temp) {
		batchSettleService.temp_updatePrepay(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateRealMoney", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateRealMoney(T_Batch_SettleList temp) {
		batchSettleService.temp_updateRealMoney(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateRemark", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateRemark(T_Batch_SettleList temp) {
		batchSettleService.temp_updateRemark(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateJoin", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateJoin(String ids,String stl_join) {
		batchSettleService.temp_updateJoin(ids, stl_join);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_entire", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_entire(Double discount_money,Double prepay,Double received,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("discount_money", discount_money);
		params.put("prepay", prepay);
		params.put("received", received);
		params.put("user", getUser(session));
		return ajaxSuccess(batchSettleService.temp_entire(params));
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Batch_Settle settle,HttpSession session) {
		batchSettleService.save(settle, getUser(session));
		return ajaxSuccess(settle);
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Batch_Settle settle,HttpSession session) {
		batchSettleService.update(settle, getUser(session));
		return ajaxSuccess(settle);
	}
	
	@RequestMapping(value = "approve", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object approve(T_Approve_Record record,String number,HttpSession session) {
		record.setAr_describe(StringUtil.decodeString(record.getAr_describe()));
		return ajaxSuccess(batchSettleService.approve(number, record, getUser(session)));
	}
	
	@RequestMapping(value = "reverse", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object reverse(String number,HttpSession session) {
		return ajaxSuccess(batchSettleService.reverse(number, getUser(session)));
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(String number,HttpSession session) {
		batchSettleService.del(number, getCompanyid(session));
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "print", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object print(String number,Integer sp_id,HttpSession session) {
		Map<String, Object> resultMap = batchSettleService.loadPrintData(number, sp_id, getUser(session));
		resultMap.put("user", getUser(session));
		return ajaxSuccess(BatchSettleVO.buildPrintJson(resultMap));
	}
	
}
