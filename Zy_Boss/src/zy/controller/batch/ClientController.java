package zy.controller.batch;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.batch.client.T_Batch_Client;
import zy.entity.batch.client.T_Batch_Client_Shop;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.batch.client.ClientService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("batch/client")
public class ClientController extends BaseController{
	@Resource
	private ClientService clientService;
	
	@RequestMapping(value = "to_list", method = RequestMethod.GET)
	public String to_list() {
		return "batch/client/list";
	}
	@RequestMapping(value = "to_client_shop_list", method = RequestMethod.GET)
	public String to_client_shop_list(@RequestParam Integer ci_id,@RequestParam String ci_code,Model model) {
		model.addAttribute("ci_id", ci_id);
		model.addAttribute("ci_code", ci_code);
		return "batch/client/client_shop_list";
	}
	@RequestMapping(value = "to_add_shop_list", method = RequestMethod.GET)
	public String to_add_shop_list(@RequestParam String ci_code,Model model) {
		model.addAttribute("ci_code", ci_code);
		return "batch/client/add_client_shop_list";
	}
	
	@RequestMapping(value = "to_money", method = RequestMethod.GET)
	public String to_money() {
		return "batch/client/money";
	}
	@RequestMapping(value = "to_money_details", method = RequestMethod.GET)
	public String to_money_details() {
		return "batch/client/money_details";
	}
	@RequestMapping(value = "to_back_analysis", method = RequestMethod.GET)
	public String to_back_analysis() {
		return "batch/client/back_analysis";
	}
	@RequestMapping(value = "to_add", method = RequestMethod.GET)
	public String to_add() {
		return "batch/client/add";
	}
	@RequestMapping(value = "to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer ci_id,Model model) {
		T_Batch_Client client = clientService.load(ci_id);
		if(null != client){
			model.addAttribute("client",client);
		}
		return "batch/client/update";
	}
	
	@RequestMapping(value = "to_update_shop_list", method = RequestMethod.GET)
	public String to_update_shop_list(@RequestParam Integer cis_id,@RequestParam String ci_code,Model model) {
		T_Batch_Client_Shop client_Shop = clientService.load_shop(cis_id);
		if(null != client_Shop){
			model.addAttribute("client_Shop",client_Shop);
		}
		return "batch/client/update_client_shop_list";
	}
	
	@RequestMapping("to_list_dialog")
	public String to_list_dialog() {
		return "batch/client/list_dialog";
	}
	
	@RequestMapping("to_list_shop_dialog")
	public String to_list_shop_dialog(@RequestParam String ci_code,Model model) {
		model.addAttribute("ci_code", ci_code);
		return "batch/client/list_shop_dialog";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put(CommonUtil.PAGESIZE, pageForm.getRows());
        params.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        params.put("searchContent", pageForm.getSearchContent());
        if(CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype())){//总公司、分公司
        	params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		}else{//自营、加盟、合伙
			params.put(CommonUtil.SHOP_CODE, user.getShop_upcode());
		}
		return ajaxSuccess(clientService.page(params));
	}
	
	@RequestMapping(value = "shop_list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object shop_list(PageForm pageForm,
				@RequestParam String ci_code,
				@RequestParam String searchContent,
				@RequestParam Integer cis_stutas,
				HttpSession session) {
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        params.put("ci_code", ci_code);
        params.put("cis_stutas", cis_stutas);
        params.put("searchContent", StringUtil.decodeString(searchContent));
		return ajaxSuccess(clientService.shop_list(params));
	}
	
	@RequestMapping(value = "del_client_shop", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del_client_shop(@RequestParam Integer cis_id,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
	    param.put(CommonUtil.COMPANYID, user.getCompanyid());
	    param.put("cis_id",cis_id);
	    clientService.del_client_shop(param);//删除表数据
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "page4dialog", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page4dialog(PageForm pageForm,HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put(CommonUtil.PAGESIZE, pageForm.getRows());
        params.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        params.put("searchContent", pageForm.getSearchContent());
        if(CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype())){//总公司、分公司
        	params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		}else{//自营、加盟、合伙
			params.put(CommonUtil.SHOP_CODE, user.getShop_upcode());
		}
		return ajaxSuccess(clientService.page4dialog(params));
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Batch_Client client,HttpSession session) {
		clientService.save(client, getUser(session));
		return ajaxSuccess(client);
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Batch_Client client,HttpSession session) {
		clientService.update(client, getUser(session));
		return ajaxSuccess(client);
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(@RequestParam Integer ci_id,@RequestParam String ci_code,HttpSession session) {
		clientService.del(ci_id, ci_code, getCompanyid(session));
		return ajaxSuccess();
	}

	@RequestMapping(value = "pageMoneyDetails", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object pageMoneyDetails(PageForm pageForm,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String type,
			@RequestParam(required = false) String pay_state,
			@RequestParam(required = false) String client_code,
			@RequestParam(required = false) String manager,
			@RequestParam(required = false) String number,
			HttpSession session) {
        Map<String,Object> params = new HashMap<String, Object>();
        T_Sys_User user = getUser(session);
        if(CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype())){//总公司、分公司
        	params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		}else{//自营、加盟、合伙
			params.put(CommonUtil.SHOP_CODE, user.getShop_upcode());
		}
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put(CommonUtil.PAGESIZE, pageForm.getRows());
        params.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        params.put("begindate", begindate);
        params.put("enddate", enddate);
        params.put("type", type);
        params.put("pay_state", pay_state);
        params.put("client_code", client_code);
        params.put("manager", StringUtil.decodeString(manager));
        params.put("number", StringUtil.decodeString(number));
		return ajaxSuccess(clientService.pageMoneyDetails(params));
	}

	@RequestMapping(value = "pageBackAnalysis", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object pageBackAnalysis(PageForm pageForm,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String ci_code,
			HttpSession session) {
        Map<String,Object> params = new HashMap<String, Object>();
        T_Sys_User user = getUser(session);
        if(CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype())){//总公司、分公司
        	params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		}else{//自营、加盟、合伙
			params.put(CommonUtil.SHOP_CODE, user.getShop_upcode());
		}
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        params.put("begindate", begindate);
        params.put("enddate", enddate);
        params.put("ci_code", ci_code);
		return ajaxSuccess(clientService.listBackAnalysis(params));
	}
	
	@RequestMapping(value = "save_client_shop", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save_client_shop(T_Batch_Client_Shop client_Shop,HttpSession session) {
		clientService.save_client_shop(client_Shop, getUser(session));
		return ajaxSuccess(client_Shop);
	}
	
	@RequestMapping(value = "update_client_shop", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update_client_shop(T_Batch_Client_Shop client_Shop,HttpSession session) {
		clientService.update_client_shop(client_Shop, getUser(session));
		return ajaxSuccess(client_Shop);
	}
}
