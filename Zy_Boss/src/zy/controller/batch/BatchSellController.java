package zy.controller.batch;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.approve.T_Approve_Record;
import zy.entity.batch.sell.T_Batch_Sell;
import zy.entity.batch.sell.T_Batch_SellList;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.batch.sell.BatchSellService;
import zy.service.report.excel.ReportInterfaceFactory;
import zy.service.report.excel.reporter.BatchSellReporter;
import zy.util.CommonUtil;
import zy.util.StringUtil;
import zy.vo.batch.BatchSellVO;

@Controller
@RequestMapping("batch/sell")
public class BatchSellController extends BaseController{
	@Resource
	private BatchSellService batchSellService;
	@Resource
	private ReportInterfaceFactory reportInterfaceFactory;

	@RequestMapping(value = "to_list/{se_type}", method = RequestMethod.GET)
	public String to_list(@PathVariable Integer se_type,Model model) {
		model.addAttribute("se_type", se_type);
		return "batch/sell/list";
	}
	@RequestMapping(value = "to_list_warn/{se_type}", method = RequestMethod.GET)
	public String to_list_warn(@PathVariable Integer se_type,Model model) {
		model.addAttribute("se_type", se_type);
		return "batch/sell/list_warn";
	}
	@RequestMapping(value = "to_list_order_dialog/{se_type}", method = RequestMethod.GET)
	public String to_list_order_dialog(@PathVariable Integer se_type,Model model) {
		model.addAttribute("se_type", se_type);
		return "batch/sell/list_order_dialog";
	}
	@RequestMapping(value = "to_add/{se_type}", method = RequestMethod.GET)
	public String to_add(@PathVariable Integer se_type,Model model) {
		model.addAttribute("se_type", se_type);
		return "batch/sell/add";
	}
	@RequestMapping(value = "/to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer se_id,Model model,HttpSession session) {
		T_Sys_User user = getUser(session);
		T_Batch_Sell sell = batchSellService.load(se_id);
		if(null != sell){
			batchSellService.initUpdate(sell.getSe_number(), sell.getSe_type(), user.getUs_id(), user.getCompanyid());
			model.addAttribute("sell",sell);
		}
		return "batch/sell/update";
	}
	@RequestMapping(value = "to_view", method = RequestMethod.GET)
	public String to_view(@RequestParam Integer se_id,Model model) {
		T_Batch_Sell sell = batchSellService.load(se_id);
		if(null != sell){
			model.addAttribute("sell",sell);
		}
		return "batch/sell/view";
	}
	@RequestMapping(value = "to_view/{number}", method = RequestMethod.GET)
	public String to_view(@PathVariable String number,Model model,HttpSession session) {
		T_Batch_Sell sell = batchSellService.load(number,getCompanyid(session));
		if(null != sell){
			model.addAttribute("sell",sell);
		}
		return "batch/sell/view";
	}
	@RequestMapping(value = "to_select_product/{se_type}", method = RequestMethod.GET)
	public String to_select_product(@PathVariable Integer se_type,Model model) {
		model.addAttribute("se_type", se_type);
		return "batch/sell/select_product";
	}
	@RequestMapping(value = "to_temp_update/{se_type}", method = RequestMethod.GET)
	public String to_temp_update(@PathVariable Integer se_type,Model model) {
		model.addAttribute("se_type", se_type);
		return "batch/sell/temp_update";
	}
	
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = true) Integer se_type,
			@RequestParam(required = true) Integer se_isdraft,
			@RequestParam(required = false) Integer se_ar_state,
			@RequestParam(required = false) String se_client_code,
			@RequestParam(required = false) String se_depot_code,
			@RequestParam(required = false) String se_manager,
			@RequestParam(required = false) String se_number,
			HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        if(CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype())){//总公司、分公司
        	param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		}else{//自营、加盟、合伙
			param.put(CommonUtil.SHOP_CODE, user.getShop_upcode());
		}
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("begindate", pageForm.getBegindate());
        param.put("enddate", pageForm.getEnddate());
        param.put("se_type", se_type);
        param.put("se_isdraft", se_isdraft);
        param.put("se_ar_state", se_ar_state);
        param.put("se_client_code", se_client_code);
        param.put("se_depot_code", se_depot_code);
        param.put("se_manager", StringUtil.decodeString(se_manager));
        param.put("se_number", StringUtil.decodeString(se_number));
		return ajaxSuccess(batchSellService.page(param));
	}
	
	@RequestMapping(value = "report", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public String report(PageForm pageForm,
			@RequestParam(required = true) Integer se_type,
			@RequestParam(required = true) Integer se_isdraft,
			@RequestParam(required = false) Integer se_ar_state,
			@RequestParam(required = false) String se_client_code,
			@RequestParam(required = false) String se_depot_code,
			@RequestParam(required = false) String se_manager,
			@RequestParam(required = false) String se_number,
			HttpSession session,
			HttpServletRequest request,HttpServletResponse response) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        if(CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype())){//总公司、分公司
        	param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		}else{//自营、加盟、合伙
			param.put(CommonUtil.SHOP_CODE, user.getShop_upcode());
		}
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("begindate", pageForm.getBegindate());
        param.put("enddate", pageForm.getEnddate());
        param.put("se_type", se_type);
        param.put("se_isdraft", se_isdraft);
        param.put("se_ar_state", se_ar_state);
        param.put("se_client_code", se_client_code);
        param.put("se_depot_code", se_depot_code);
        param.put("se_manager", StringUtil.decodeString(se_manager));
        param.put("se_number", StringUtil.decodeString(se_number));
        
        File file = reportInterfaceFactory.newInstance(BatchSellReporter.class, param).getFile();
        return download(file, request, response);
	}
	
	@RequestMapping(value = "detail_list/{se_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_list(@PathVariable String se_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("sel_number", se_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(batchSellService.detail_list(params));
	}
	
	@RequestMapping(value = "detail_sum/{se_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_sum(@PathVariable String se_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("sel_number", se_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(batchSellService.detail_sum(params));
	}
	
	@RequestMapping(value = "detail_size_title/{se_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_size_title(@PathVariable String se_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("sel_number", se_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(batchSellService.detail_size_title(params));
	}
	
	@RequestMapping(value = "detail_size/{se_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_size(@PathVariable String se_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("sel_number", se_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(batchSellService.detail_size(params));
	}
	
	@RequestMapping(value = "temp_list/{se_type}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object temp_list(@PathVariable Integer se_type,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("sel_type", se_type);
		params.put("sel_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(batchSellService.temp_list(params));
	}
	
	@RequestMapping(value = "temp_sum/{se_type}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object temp_sum(@PathVariable Integer se_type,HttpSession session) {
		T_Sys_User user = getUser(session);
		return ajaxSuccess(batchSellService.temp_sum(se_type,user.getUs_id(), user.getCompanyid()));
	}
	
	@RequestMapping(value = "temp_size_title/{se_type}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object temp_size_title(@PathVariable Integer se_type,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("sel_type", se_type);
		params.put("sel_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(batchSellService.temp_size_title(params));
	}
	
	@RequestMapping(value = "temp_size/{se_type}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object temp_size(@PathVariable Integer se_type,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("sel_type", se_type);
		params.put("sel_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(batchSellService.temp_size(params));
	}
	
	@RequestMapping(value = "temp_save_bybarcode/{se_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_save_bybarcode(@PathVariable Integer se_type,HttpSession session,
			@RequestParam(required = true) String barcode,
			@RequestParam(required = true) Integer amount,
			@RequestParam(required = false) String ci_code,
			@RequestParam(required = false) String priceType,
			@RequestParam(required = false) String batch_price,
			@RequestParam(required = false) Double ci_rate) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("se_type", se_type);
		params.put("barcode", barcode);
		params.put("amount", amount);
		params.put("ci_code", ci_code);
		params.put("priceType", priceType);
		params.put("ci_rate", ci_rate);
		params.put("batch_price", batch_price);
		params.put("user", user);
		Map<String, Object> resultMap = batchSellService.temp_save_bybarcode(params);
		return ajaxSuccess(resultMap);
	}
	
	@RequestMapping(value = "page_product", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object page_product(PageForm pageForm,
			@RequestParam(required = true) String se_type,
			@RequestParam(required = false) String alreadyExist,
			@RequestParam(required = false) String exactQuery,
			HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
        param.put("searchContent", pageForm.getSearchContent());
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put("us_id", user.getUs_id());
        param.put("se_type", se_type);
        param.put("alreadyExist", alreadyExist);
        param.put("exactQuery", exactQuery);
		return ajaxSuccess(batchSellService.page_product(param));
	}
	
	@RequestMapping(value = "temp_loadproduct/{se_type}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object temp_loadproduct(@PathVariable Integer se_type,HttpSession session,
			@RequestParam(required = true) String pd_code,
			@RequestParam(required = false) String gift,
			@RequestParam(required = false) String exist,
			@RequestParam(required = false) String dp_code,
			@RequestParam(required = false) String priceType,
			@RequestParam(required = false) String batch_price,
			@RequestParam(required = false) Double ci_rate) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put(CommonUtil.COMPANYID, user.getCompanyid());
		params.put("se_type", se_type);
		params.put("pd_code", pd_code);
		params.put("sel_pi_type", gift);
		params.put("exist", exist);
		params.put("priceType", priceType);
		params.put("ci_rate", ci_rate);
		params.put("batch_price", batch_price);
		params.put("dp_code", dp_code);
		params.put("us_id", user.getUs_id());
		params.put(CommonUtil.KEY_SYSSET, getSysSet(session));
		return ajaxSuccess(batchSellService.temp_loadproduct(params));
	}
	
	@RequestMapping(value = "temp_save/{se_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_save(@PathVariable Integer se_type,@RequestBody Map<String, Object> data,HttpSession session) {
		T_Sys_User user = getUser(session);
		List<T_Batch_SellList> temps = BatchSellVO.convertMap2Model(data, se_type, user);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("se_type", se_type);
		params.put("temps", temps);
		params.put("user", user);
		params.put("pd_code", data.get("pd_code"));
		params.put("unitPrice", data.get("unitPrice"));
		batchSellService.temp_save(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_del(@RequestParam Integer sel_id) {
		batchSellService.temp_del(sel_id);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateAmount", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateAmount(T_Batch_SellList temp) {
		batchSellService.temp_updateAmount(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updatePrice/{se_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updatePrice(@PathVariable Integer se_type,T_Batch_SellList temp,HttpSession session) {
		T_Sys_User user = getUser(session);
		temp.setSel_us_id(user.getUs_id());
		temp.setCompanyid(user.getCompanyid());
		temp.setSel_type(se_type);
		batchSellService.temp_updatePrice(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateRebatePrice/{se_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateRebatePrice(@PathVariable Integer se_type,T_Batch_SellList temp,HttpSession session) {
		T_Sys_User user = getUser(session);
		temp.setSel_us_id(user.getUs_id());
		temp.setCompanyid(user.getCompanyid());
		temp.setSel_type(se_type);
		batchSellService.temp_updateRebatePrice(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateRemarkById", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateRemarkById(T_Batch_SellList temp) {
		temp.setSel_remark(StringUtil.decodeString(temp.getSel_remark()));
		batchSellService.temp_updateRemarkById(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateRemarkByPdCode/{se_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateRemarkByPdCode(@PathVariable Integer se_type,T_Batch_SellList temp,HttpSession session) {
		T_Sys_User user = getUser(session);
		temp.setSel_remark(StringUtil.decodeString(temp.getSel_remark()));
		temp.setSel_us_id(user.getUs_id());
		temp.setCompanyid(user.getCompanyid());
		temp.setSel_type(se_type);
		batchSellService.temp_updateRemarkByPdCode(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_delByPiCode/{se_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_delByPiCode(@PathVariable Integer se_type,
			@RequestParam String pd_code,@RequestParam Integer sel_pi_type,
			@RequestParam(required = false) String cr_code,
			@RequestParam(required = false) String br_code,
			HttpSession session) {
		T_Sys_User user = getUser(session);
		T_Batch_SellList temp = new T_Batch_SellList();
		temp.setSel_pd_code(pd_code);
		temp.setSel_cr_code(cr_code);
		temp.setSel_br_code(br_code);
		temp.setSel_pi_type(sel_pi_type);
		temp.setSel_type(se_type);
		temp.setSel_us_id(user.getUs_id());
		temp.setCompanyid(user.getCompanyid());
		batchSellService.temp_delByPiCode(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_clear/{se_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_clear(@PathVariable Integer se_type, HttpSession session) {
		T_Sys_User user = getUser(session);
		batchSellService.temp_clear(se_type, user.getUs_id(), user.getCompanyid());
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_import/{se_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_import(@PathVariable Integer se_type,@RequestBody Map<String, Object> data,HttpSession session) {
		T_Sys_User user = getUser(session);
		List<String[]> datas = BatchSellVO.convertMap2Model_import(data, user);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("se_type", se_type);
		params.put("datas", datas);
		params.put("user", user);
		params.put("ci_code", data.get("ci_code"));
		params.put("priceType", data.get("priceType"));
		params.put("ci_rate", data.get("ci_rate"));
		params.put("batch_price", data.get("batch_price"));
		batchSellService.temp_import(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_import_order/{se_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_import_order(@PathVariable Integer se_type,@RequestParam String ids,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("se_type", se_type);
		params.put("ids", ids);
		params.put("user", user);
		batchSellService.temp_import_order(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Batch_Sell sell,HttpSession session) {
		batchSellService.save(sell, getUser(session));
		return ajaxSuccess(sell);
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Batch_Sell sell,HttpSession session) {
		batchSellService.update(sell, getUser(session));
		return ajaxSuccess(sell);
	}
	
	@RequestMapping(value = "approve", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object approve(T_Approve_Record record,String number,HttpSession session) {
		record.setAr_describe(StringUtil.decodeString(record.getAr_describe()));
		T_Batch_Sell sell = batchSellService.approve(number, record, getUser(session));
		return ajaxSuccess(sell);
	}
	
	@RequestMapping(value = "reverse", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object reverse(String number,HttpSession session) {
		return ajaxSuccess(batchSellService.reverse(number, getUser(session)));
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(String number,HttpSession session) {
		batchSellService.del(number, getCompanyid(session));
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "print", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object print(String number,Integer sp_id,Integer displayMode,HttpSession session) {
		Map<String, Object> resultMap = batchSellService.loadPrintData(number, sp_id, displayMode, getUser(session));
		resultMap.put("user", getUser(session));
		Map<String, Object> temp = BatchSellVO.buildPrintJson(resultMap, displayMode);
		System.out.println("--------"+temp);
		return ajaxSuccess(temp);
	}
	
}
