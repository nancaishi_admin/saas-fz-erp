package zy.controller.batch;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.approve.T_Approve_Record;
import zy.entity.batch.prepay.T_Batch_Prepay;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.batch.prepay.BatchPrepayService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("batch/prepay")
public class BatchPrepayController extends BaseController{
	@Resource
	private BatchPrepayService batchPrepayService;
	
	@RequestMapping(value = "to_list", method = RequestMethod.GET)
	public String to_list() {
		return "batch/prepay/list";
	}
	@RequestMapping(value = "to_add", method = RequestMethod.GET)
	public String to_add() {
		return "batch/prepay/add";
	}
	@RequestMapping(value = "to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer pp_id,Model model) {
		T_Batch_Prepay prepay = batchPrepayService.load(pp_id);
		if(null != prepay){
			model.addAttribute("prepay",prepay);
			model.addAttribute("client",batchPrepayService.loadClient(prepay.getPp_client_code(), prepay.getCompanyid()));
			model.addAttribute("bank",batchPrepayService.loadBank(prepay.getPp_ba_code(), prepay.getCompanyid()));
		}
		return "batch/prepay/update";
	}
	@RequestMapping(value = "to_view", method = RequestMethod.GET)
	public String to_view(@RequestParam Integer pp_id,Model model) {
		T_Batch_Prepay prepay = batchPrepayService.load(pp_id);
		if(null != prepay){
			model.addAttribute("prepay",prepay);
		}
		return "batch/prepay/view";
	}
	@RequestMapping(value = "to_view/{number}", method = RequestMethod.GET)
	public String to_view(@PathVariable String number,Model model,HttpSession session) {
		T_Batch_Prepay prepay = batchPrepayService.load(number, getCompanyid(session));
		if(null != prepay){
			model.addAttribute("prepay",prepay);
		}
		return "batch/prepay/view";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = false) Integer pp_ar_state,
			@RequestParam(required = false) String pp_client_code,
			@RequestParam(required = false) String pp_ba_code,
			@RequestParam(required = false) String pp_manager,
			@RequestParam(required = false) String pp_number,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        if(CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype())){//总公司、分公司
        	param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		}else{//自营、加盟、合伙
			param.put(CommonUtil.SHOP_CODE, user.getShop_upcode());
		}
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("begindate", pageForm.getBegindate());
        param.put("enddate", pageForm.getEnddate());
        param.put("pp_ar_state", pp_ar_state);
        param.put("pp_client_code", pp_client_code);
        param.put("pp_ba_code", pp_ba_code);
        param.put("pp_manager", StringUtil.decodeString(pp_manager));
        param.put("pp_number", StringUtil.decodeString(pp_number));
		return ajaxSuccess(batchPrepayService.page(param));
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Batch_Prepay prepay,HttpSession session) {
		batchPrepayService.save(prepay, getUser(session));
		return ajaxSuccess(prepay);
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Batch_Prepay prepay,HttpSession session) {
		batchPrepayService.update(prepay, getUser(session));
		return ajaxSuccess(prepay);
	}
	
	@RequestMapping(value = "approve", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object approve(T_Approve_Record record,String number,HttpSession session) {
		record.setAr_describe(StringUtil.decodeString(record.getAr_describe()));
		return ajaxSuccess(batchPrepayService.approve(number, record, getUser(session)));
	}
	
	@RequestMapping(value = "reverse", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object reverse(String number,HttpSession session) {
		return ajaxSuccess(batchPrepayService.reverse(number, getUser(session)));
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(String number,HttpSession session) {
		batchPrepayService.del(number, getCompanyid(session));
		return ajaxSuccess();
	}
	
}
