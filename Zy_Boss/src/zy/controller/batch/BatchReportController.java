package zy.controller.batch;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sys.user.T_Sys_User;
import zy.form.ProductForm;
import zy.service.batch.report.BatchReportService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("batch/report")
public class BatchReportController extends BaseController{
	@Resource
	private BatchReportService batchReportService;
	
	@RequestMapping(value = "to_report_sell", method = RequestMethod.GET)
	public String to_report_sell() {
		return "batch/report/report_sell";
	}
	@RequestMapping(value = "to_report_sell_detail", method = RequestMethod.GET)
	public String to_report_sell_detail() {
		return "batch/report/report_sell_detail";
	}
	@RequestMapping(value = "to_report_order_detail", method = RequestMethod.GET)
	public String to_report_order_detail() {
		return "batch/report/report_order_detail";
	}
	@RequestMapping(value = "to_summary_sell", method = RequestMethod.GET)
	public String to_summary_sell() {
		return "batch/report/summary_sell";
	}
	/**
	 * 类别分级批发报表
	 */
	@RequestMapping(value = "to_type_level_sell/{tp_upcode}", method = RequestMethod.GET)
	public String to_type_level_sell(@PathVariable String tp_upcode,Model model) {
		model.addAttribute("tp_upcode", tp_upcode);
		return "batch/report/type_level_sell";
	}
	@RequestMapping(value = "to_report_sell_csb/{type}", method = RequestMethod.GET)
	public String to_report_sell_csb(@PathVariable String type,Model model) {
		model.addAttribute("type", type);
		return "batch/report/report_sell_csb";
	}
	@RequestMapping(value = "to_report_sell_cs", method = RequestMethod.GET)
	public String to_report_sell_cs() {
		return "batch/report/report_sell_cs";
	}
	
	@RequestMapping(value = "pageSellReport", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object pageSellReport(ProductForm productForm,
			@RequestParam(required = true) String type,
			@RequestParam(required = false) String se_type,
			@RequestParam(required = false) String se_client_code,
			@RequestParam(required = false) String se_depot_code,
			@RequestParam(required = false) String se_manager,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put(CommonUtil.PAGESIZE, productForm.getRows());
        params.put(CommonUtil.PAGEINDEX, productForm.getPage());
        params.put(CommonUtil.SIDX, productForm.getSidx());
        params.put(CommonUtil.SORD, productForm.getSord());
        params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        params.put("type", type);
        params.put("se_type", se_type);
        params.put("se_client_code", se_client_code);
        params.put("se_depot_code", se_depot_code);
        params.put("se_manager", StringUtil.decodeString(se_manager));
        params.put("begindate", productForm.getBegindate());
        params.put("enddate", productForm.getEnddate());
        params.put("bd_code", productForm.getBd_code());
        params.put("tp_code", productForm.getTp_code());
        params.put("pd_season", productForm.getPd_season());
        params.put("pd_year", productForm.getPd_year());
        params.put("pd_code", productForm.getPd_code());
		return ajaxSuccess(batchReportService.pageSellReport(params));
	}
	
	@RequestMapping(value = "pageSellDetailReport", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object pageSellDetailReport(ProductForm productForm,
			@RequestParam(required = false) String se_type,
			@RequestParam(required = false) String se_client_code,
			@RequestParam(required = false) String se_depot_code,
			@RequestParam(required = false) String se_manager,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put(CommonUtil.PAGESIZE, productForm.getRows());
        params.put(CommonUtil.PAGEINDEX, productForm.getPage());
        params.put(CommonUtil.SIDX, productForm.getSidx());
        params.put(CommonUtil.SORD, productForm.getSord());
        params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        params.put("se_type", se_type);
        params.put("se_client_code", se_client_code);
        params.put("se_depot_code", se_depot_code);
        params.put("se_manager", StringUtil.decodeString(se_manager));
        params.put("begindate", productForm.getBegindate());
        params.put("enddate", productForm.getEnddate());
        params.put("bd_code", productForm.getBd_code());
        params.put("tp_code", productForm.getTp_code());
        params.put("pd_season", productForm.getPd_season());
        params.put("pd_year", productForm.getPd_year());
        params.put("pd_code", productForm.getPd_code());
		return ajaxSuccess(batchReportService.pageSellDetailReport(params));
	}
	
	@RequestMapping(value = "pageOrderDetailReport", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object pageOrderDetailReport(ProductForm productForm,
			@RequestParam(required = false) String od_type,
			@RequestParam(required = false) String od_client_code,
			@RequestParam(required = false) String od_depot_code,
			@RequestParam(required = false) String od_manager,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put(CommonUtil.PAGESIZE, productForm.getRows());
        params.put(CommonUtil.PAGEINDEX, productForm.getPage());
        params.put(CommonUtil.SIDX, productForm.getSidx());
        params.put(CommonUtil.SORD, productForm.getSord());
        params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        params.put("od_type", od_type);
        params.put("od_client_code", od_client_code);
        params.put("od_depot_code", od_depot_code);
        params.put("od_manager", StringUtil.decodeString(od_manager));
        params.put("begindate", productForm.getBegindate());
        params.put("enddate", productForm.getEnddate());
        params.put("bd_code", productForm.getBd_code());
        params.put("tp_code", productForm.getTp_code());
        params.put("pd_season", productForm.getPd_season());
        params.put("pd_year", productForm.getPd_year());
        params.put("pd_code", productForm.getPd_code());
		return ajaxSuccess(batchReportService.pageOrderDetailReport(params));
	}
	
	@RequestMapping(value = "sell_detail_size_title", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object sell_detail_size_title(ProductForm productForm,
			@RequestParam(required = false) String se_type,
			@RequestParam(required = false) String se_client_code,
			@RequestParam(required = false) String se_depot_code,
			@RequestParam(required = false) String se_manager,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        params.put("se_type", se_type);
        params.put("se_client_code", se_client_code);
        params.put("se_depot_code", se_depot_code);
        params.put("se_manager", StringUtil.decodeString(se_manager));
        params.put("begindate", productForm.getBegindate());
        params.put("enddate", productForm.getEnddate());
        params.put("bd_code", productForm.getBd_code());
        params.put("tp_code", productForm.getTp_code());
        params.put("pd_season", productForm.getPd_season());
        params.put("pd_year", productForm.getPd_year());
        params.put("pd_code", productForm.getPd_code());
		return ajaxSuccess(batchReportService.sell_detail_size_title(params));
	}
	
	@RequestMapping(value = "pageSellDetailSizeReport", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object pageSellDetailSizeReport(ProductForm productForm,
			@RequestParam(required = false) String se_type,
			@RequestParam(required = false) String se_client_code,
			@RequestParam(required = false) String se_depot_code,
			@RequestParam(required = false) String se_manager,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put(CommonUtil.PAGESIZE, productForm.getRows());
        params.put(CommonUtil.PAGEINDEX, productForm.getPage());
        params.put(CommonUtil.SIDX, productForm.getSidx());
        params.put(CommonUtil.SORD, productForm.getSord());
        params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        params.put("se_type", se_type);
        params.put("se_client_code", se_client_code);
        params.put("se_depot_code", se_depot_code);
        params.put("se_manager", StringUtil.decodeString(se_manager));
        params.put("begindate", productForm.getBegindate());
        params.put("enddate", productForm.getEnddate());
        params.put("bd_code", productForm.getBd_code());
        params.put("tp_code", productForm.getTp_code());
        params.put("pd_season", productForm.getPd_season());
        params.put("pd_year", productForm.getPd_year());
        params.put("pd_code", productForm.getPd_code());
		return ajaxSuccess(batchReportService.pageSellDetailSizeReport(params));
	}
	
	@RequestMapping(value = "pageSellSummary", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object pageSellSummary(ProductForm productForm,
			@RequestParam(required = true) String type,
			@RequestParam(required = false) String year,
			@RequestParam(required = false) String month,
			@RequestParam(required = false) String se_type,
			@RequestParam(required = false) String se_client_code,
			@RequestParam(required = false) String se_depot_code,
			@RequestParam(required = false) String se_manager,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put(CommonUtil.PAGESIZE, productForm.getRows());
        params.put(CommonUtil.PAGEINDEX, productForm.getPage());
        params.put(CommonUtil.SIDX, productForm.getSidx());
        params.put(CommonUtil.SORD, productForm.getSord());
        params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        params.put("type", type);
        params.put("year", year);
        params.put("month", month);
        params.put("se_type", se_type);
        params.put("se_client_code", se_client_code);
        params.put("se_depot_code", se_depot_code);
        params.put("se_manager", StringUtil.decodeString(se_manager));
        params.put("begindate", productForm.getBegindate());
        params.put("enddate", productForm.getEnddate());
        params.put("bd_code", productForm.getBd_code());
        params.put("tp_code", productForm.getTp_code());
        params.put("pd_season", productForm.getPd_season());
        params.put("pd_year", productForm.getPd_year());
        params.put("pd_code", productForm.getPd_code());
		return ajaxSuccess(batchReportService.pageSellSummary(params));
	}
	
	@RequestMapping(value = "type_level_sell", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object type_level_sell(ProductForm productForm,
			@RequestParam(required = true) String tp_upcode,
			@RequestParam(required = true) String year,
			@RequestParam(required = false) String se_client_code,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put("bd_code", productForm.getBd_code());
        param.put("pd_season", productForm.getPd_season());
        param.put("tp_upcode", tp_upcode);
        param.put("year", year);
        param.put("se_client_code", se_client_code);
		return ajaxSuccess(batchReportService.type_level_sell(param));
	}
	
	@RequestMapping(value = "listSellReport_csb", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listSellReport_csb(ProductForm productForm,
			@RequestParam(required = true) String type,
			@RequestParam(required = false) String se_client_code,
			@RequestParam(required = false) String se_depot_code,
			@RequestParam(required = false) String se_manager,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put(CommonUtil.PAGESIZE, productForm.getRows());
        params.put(CommonUtil.PAGEINDEX, productForm.getPage());
        params.put(CommonUtil.SIDX, productForm.getSidx());
        params.put(CommonUtil.SORD, productForm.getSord());
        params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        params.put("type", type);
        params.put("se_client_code", se_client_code);
        params.put("se_depot_code", se_depot_code);
        params.put("se_manager", StringUtil.decodeString(se_manager));
        params.put("begindate", productForm.getBegindate());
        params.put("enddate", productForm.getEnddate());
        params.put("bd_code", productForm.getBd_code());
        params.put("tp_code", productForm.getTp_code());
        params.put("pd_season", productForm.getPd_season());
        params.put("pd_year", productForm.getPd_year());
        params.put("pd_code", productForm.getPd_code());
        params.put("cr_code", productForm.getCr_code());
        params.put("sz_code", productForm.getSz_code());
        params.put("br_code", productForm.getBr_code());
		return ajaxSuccess(batchReportService.listSellReport_csb(params));
	}
	
}
