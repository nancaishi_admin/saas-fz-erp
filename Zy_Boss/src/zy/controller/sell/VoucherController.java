package zy.controller.sell;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sell.voucher.T_Sell_Voucher;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.sell.voucher.VoucherService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("sell/voucher")
public class VoucherController extends BaseController{
	
	@Resource
	private VoucherService voucherService;
	
	@RequestMapping(value = "/to_list", method = RequestMethod.GET)
	public String to_list() {
		return "sell/voucher/list";
	}
	@RequestMapping(value = "/to_add", method = RequestMethod.GET)
	public String to_add() {
		return "sell/voucher/add";
	}
	@RequestMapping(value = "/to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer vc_id,Model model) {
		T_Sell_Voucher voucher = voucherService.queryByID(vc_id);
		if(null != voucher){
			model.addAttribute("voucher",voucher);
		}
		return "sell/voucher/update";
	}
	@RequestMapping(value = "/to_detail", method = RequestMethod.GET)
	public String to_detail() {
		return "sell/voucher/detail";
	}
	@RequestMapping(value = "/to_details", method = RequestMethod.GET)
	public String to_details() {
		return "sell/voucher/details";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String vc_shop_code,
			@RequestParam(required = false) String vc_cardcode,
			@RequestParam(required = false) String vc_name,
			@RequestParam(required = false) String vc_mobile,
			@RequestParam(required = false) String minmoney,
			@RequestParam(required = false) String maxmoney,
			HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("vc_shop_code", vc_shop_code);
        param.put("minmoney", minmoney);
        param.put("maxmoney", maxmoney);
        param.put("vc_cardcode", StringUtil.decodeString(vc_cardcode));
        param.put("vc_name", StringUtil.decodeString(vc_name));
        param.put("vc_mobile", StringUtil.decodeString(vc_mobile));
		return ajaxSuccess(voucherService.page(param));
	}
	
	@RequestMapping(value = "pageDetail", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object pageDetail(PageForm pageForm,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String vcl_shop_code,
			@RequestParam(required = false) String vcl_cardcode,
			HttpServletRequest request,HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("vcl_shop_code", vcl_shop_code);
        param.put("vcl_cardcode", StringUtil.decodeString(vcl_cardcode));
		return ajaxSuccess(voucherService.pageDetail(param));
	}
	
	@RequestMapping(value = "listDetail", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listDetail(@RequestParam(required = true) String vc_code,HttpSession session) {
		return ajaxSuccess(voucherService.listDetail(vc_code, getCompanyid(session)));
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Sell_Voucher voucher,String start_no,Integer add_count,String mark,Integer prefix,String vcl_ba_code,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		voucher.setCompanyid(getCompanyid(session));
		voucher.setVc_state(0);
		voucher.setVc_used_money(0d);
		voucher.setVc_cashrate(voucher.getVc_realcash()/voucher.getVc_money());
		params.put("voucher", voucher);
		params.put("start_no", start_no);
		params.put("add_count", add_count);
		params.put("mark", mark);
		params.put("prefix", prefix);
		params.put("vcl_ba_code", vcl_ba_code);
		voucherService.save(params);
		return ajaxSuccess(voucher);
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Sell_Voucher voucher) {
		voucherService.update(voucher);
		return ajaxSuccess(voucher);
	}
	
	@RequestMapping(value = "updateState", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object updateState(T_Sell_Voucher voucher) {
		voucherService.updateState(voucher);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(@RequestParam Integer vc_id) {
		voucherService.del(vc_id);
		return ajaxSuccess();
	}
}
