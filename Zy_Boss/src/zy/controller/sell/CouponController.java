package zy.controller.sell;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.approve.T_Approve_Record;
import zy.entity.sell.coupon.T_Sell_Coupon;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.sell.coupon.CouponService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("sell/coupon")
public class CouponController extends BaseController{
	
	@Resource
	private CouponService couponService;
	
	@RequestMapping(value = "/to_list", method = RequestMethod.GET)
	public String to_list(Model model,HttpSession session) {
		T_Sys_User user = getUser(session);
		model.addAttribute("log_user", user.getUs_id());
		return "sell/coupon/list";
	}
	
	@RequestMapping(value = "/to_add", method = RequestMethod.GET)
	public String to_add(Model model) {
		String begindate = DateUtil.getYearMonthDate();
		String enddate = DateUtil.getDateAddMonths(1);
		model.addAttribute("begindate", begindate);
		model.addAttribute("enddate", enddate);
		return "sell/coupon/add";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) Integer sc_state,
			@RequestParam(required = false) Integer sc_type,
			@RequestParam(required = false) String sc_shop_code,
			@RequestParam(required = false) String sc_name,
			@RequestParam(required = false) String sc_number,
			HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("sc_state", sc_state);
        param.put("sc_type", sc_type);
        param.put("sc_shop_code", sc_shop_code);
        param.put("sc_name", StringUtil.decodeString(sc_name));
        param.put("sc_number", StringUtil.decodeString(sc_number));
		return ajaxSuccess(couponService.page(param));
	}
	
	@RequestMapping(value = "listDetail", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listDetail(HttpSession session,
			@RequestParam(required = true) String sc_number,
			@RequestParam(required = true) Integer sc_type) {
		return ajaxSuccess(couponService.listDetail(sc_number,sc_type,getCompanyid(session)));
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Sell_Coupon coupon,HttpSession session) {
		coupon.setCompanyid(getCompanyid(session));
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("user", user);
		params.put("coupon", coupon);
		if(coupon.getSc_return_number() != null 
				&& !"".equals(coupon.getSc_return_number())){//修改
			couponService.update(params);
		}else {//保存
			couponService.save(params);
		}
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "/to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer sc_id,Model model) {
		T_Sell_Coupon coupon = couponService.load(sc_id);
		if(null != coupon){
			model.addAttribute("coupon",coupon);
		}
		return "sell/coupon/update";
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(String sc_number,HttpSession session) {
		couponService.del(sc_number, getCompanyid(session));
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "/to_view", method = RequestMethod.GET)
	public String to_view(@RequestParam Integer sc_id,Model model) {
		T_Sell_Coupon coupon = couponService.load(sc_id);
		if(null != coupon){
			model.addAttribute("coupon",coupon);
		}
		return "sell/coupon/view";
	}
	
	@RequestMapping(value = "approve", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object approve(T_Approve_Record record,String number,HttpSession session) {
		record.setAr_describe(StringUtil.decodeString(record.getAr_describe()));
		T_Sell_Coupon coupon = couponService.approve(number, record, getUser(session));
		return ajaxSuccess(coupon);
	}
	
	@RequestMapping(value = "stop", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object stop(T_Approve_Record record,String number,HttpSession session) {
		T_Sell_Coupon coupon = couponService.stop(number, record, getUser(session));
		return ajaxSuccess(coupon);
	}
}
