package zy.controller.sell;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.entity.sell.cashier.T_Sell_Cashier_Set;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.sell.cashier.CashierService;
import zy.util.CommonUtil;
import zy.util.MD5;
import zy.vo.sell.SellVO;
@Controller
@RequestMapping("sell/cashier")
public class CashierController extends BaseController{
	@Resource
	private CashierService cashierService;
	@RequestMapping("to_all")
	public String to_all() {
		return "sell/cashier/all";
	}
	@RequestMapping("to_tree")
	public String to_tree() {
		return "sell/cashier/tree";
	}
	@RequestMapping(value = "/to_list", method = RequestMethod.GET)
	public String to_list() {
		return "sell/cashier/list";
	}
	@RequestMapping(value = "/to_add", method = RequestMethod.GET)
	public String to_add() {
		return "sell/cashier/add";
	}
	
	@RequestMapping(value = "/to_limit", method = RequestMethod.GET)
	public String to_limit() {
		return "sell/cashier/limit";
	}
	
	@RequestMapping(value = "/to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer ca_id,Model model) {
		model.addAttribute("model", cashierService.queryByID(ca_id));
		return "sell/cashier/update";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,String sp_code,HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put("sp_code", sp_code);
        param.put("name", pageForm.getSearchContent());
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
		return ajaxSuccess(cashierService.page(param));
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Sell_Cashier cashier,HttpSession session) {
		cashier.setCompanyid(getCompanyid(session));
		cashier.setCa_state(0);
		cashier.setCa_pass(MD5.encryptMd5("123456"));
		cashierService.save(cashier);
		return ajaxSuccess(cashier);
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Sell_Cashier cashier,HttpSession session) {
		cashier.setCompanyid(getCompanyid(session));
		cashierService.update(cashier);
		return ajaxSuccess(cashier);
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(@RequestParam Integer ca_id) {
		cashierService.del(ca_id);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "limitByCode", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object limitByCode(String em_code,HttpSession session) {
		Map<String,Object> param = new HashMap<String, Object>();
        param.put("em_code", em_code);
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
		return ajaxSuccess(cashierService.limitByCode(param));
	}
	
	@RequestMapping(value = "updateLimit", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object updateLimit(HttpServletRequest request,HttpSession session) {
		String data = request.getParameter("data");
		String em_code = request.getParameter("em_code");
		
		Map<String,Object> param = new HashMap<String, Object>();
		List<T_Sell_Cashier_Set> list = SellVO.buildMap(data,em_code,getCompanyid(session));  
		param.put(CommonUtil.COMPANYID, getCompanyid(session));
		param.put("setlist", list);
		param.put("em_code", em_code);
		cashierService.updateLimit(param);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "reset/{ca_id}", method = { RequestMethod.GET,RequestMethod.POST })
	@ResponseBody
	public Object reset(@PathVariable(value = "ca_id") Integer ca_id) {
		cashierService.reset(ca_id);
		return ajaxSuccess("重置成功");
	}
}
