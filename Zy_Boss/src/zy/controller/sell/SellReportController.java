package zy.controller.sell;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.PageData;
import zy.entity.base.depot.T_Base_Depot;
import zy.entity.base.shop.T_Base_Shop;
import zy.entity.sell.cash.T_Sell_Shop;
import zy.entity.sell.cash.T_Sell_ShopList;
import zy.entity.sell.dayend.T_Sell_DayEnd;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.form.ProductForm;
import zy.service.base.depot.DepotService;
import zy.service.base.shop.ShopService;
import zy.service.sell.dayend.DayEndService;
import zy.service.sell.report.SellReportService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("sell/report")
public class SellReportController extends BaseController{
	
	@Resource
	private SellReportService sellReportService;
	@Resource
	private DayEndService dayEndService;
	
	@Resource
	private ShopService shopService;
	
	@Resource
	private DepotService depotService;
	
	@RequestMapping(value = "to_report_shop", method = RequestMethod.GET)
	public String to_report_shop() {
		return "sell/report/report_shop";
	}
	@RequestMapping(value = "to_report_shoplist", method = RequestMethod.GET)
	public String to_report_shoplist() {
		return "sell/report/report_shoplist";
	}
	
	@RequestMapping(value = "to_dayend", method = RequestMethod.GET)
	public String to_dayend() {
		return "sell/report/report_dayend";
	}
	//货号折扣表
	@RequestMapping(value = "to_no_rate", method = RequestMethod.GET)
	public String to_no_rate() {
		return "sell/report/report_no_rate";
	}
	//导购排行
	@RequestMapping(value = "to_shop_rank", method = RequestMethod.GET)
	public String to_shop_rank() {
		return "sell/report/report_shop_rank";
	}
	//品牌类别排行
	@RequestMapping(value = "to_rank", method = RequestMethod.GET)
	public String to_rank() {
		return "sell/report/report_rank";
	}
	//导购排行
	@RequestMapping(value = "to_emp_rank", method = RequestMethod.GET)
	public String to_emp_rank() {
		return "sell/report/report_emp_rank";
	}
	//导购排行双击到该员工品牌类别排行 
	@RequestMapping(value = "to_emp_type", method = RequestMethod.GET)
	public String to_emp_type() {
		return "sell/report/report_emp_type";
	}
	
	//导购排行
	@RequestMapping(value = "to_emp_service", method = RequestMethod.GET)
	public String to_emp_service() {
		return "sell/report/report_emp_service";
	}
	//供货商排行
	@RequestMapping(value = "to_buy_rank", method = RequestMethod.GET)
	public String to_buy_rank() {
		return "sell/report/report_buy_rank";
	}
	//货号排行
	@RequestMapping(value = "to_no_rank", method = RequestMethod.GET)
	public String to_no_rank() {
		return "sell/report/report_no_rank";
	}
	//零售明细
	@RequestMapping(value = "to_list_rank", method = RequestMethod.GET)
	public String to_list_rank() {
		return "sell/report/report_list_rank";
	}
	@RequestMapping(value = "to_style_rank", method = RequestMethod.GET)
	public String to_style_rank() {
		return "sell/report/report_style_rank";
	}
	//零售月报表
	@RequestMapping(value = "to_month", method = RequestMethod.GET)
	public String to_month() {
		return "sell/report/report_month";
	}
	//零售月报表
	@RequestMapping(value = "to_year", method = RequestMethod.GET)
	public String to_year() {
		return "sell/report/report_year";
	}
	@RequestMapping(value = "to_shop_sell", method = RequestMethod.GET)
	public String to_shop_sell() {
		return "sell/report/report_shop_sell";
	}
	//店铺调拨查询
	@RequestMapping(value = "to_sell_allocate", method = RequestMethod.GET)
	public String to_sell_allocate() {
		return "sell/report/report_sell_allocate";
	}
	
	@RequestMapping(value = "toQueryStockDetail", method = { RequestMethod.GET, RequestMethod.POST })
	public String toQueryStockDetail(String pd_code,Model model,HttpSession session) {
        Map<String,Object> params = new HashMap<String, Object>();
        bulidParam(params, getUser(session));
        params.put("pd_code", pd_code);
        String sp_name = sellReportService.getSupply(params);//获取供货单位名称
        model.addAttribute("sp_name", sp_name);
        return "sell/report/query_stock_detail";
	}
	
	@RequestMapping(value = "to_query_stock_detail_size", method = { RequestMethod.GET, RequestMethod.POST })
	public String to_query_stock_detail_size() {
        return "sell/report/query_stock_detail_size";
	}
	
	
	@RequestMapping(value = "pageShop", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object pageShop(PageForm pageForm,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String sh_shop_code,
			@RequestParam(required = false) String sh_em_code,
			@RequestParam(required = false) String sh_number,
			@RequestParam(required = false) String sh_state,
			@RequestParam(required = false) String jmd,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put(CommonUtil.PAGESIZE, pageForm.getRows());
        params.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        params.put("begindate", begindate);
        params.put("enddate", enddate);
        params.put("sh_shop_code", sh_shop_code);
        params.put("sh_em_code", sh_em_code);
        params.put("sh_state", sh_state);
        params.put("sh_number", StringUtil.trimString(sh_number));
        params.put("jmd", jmd);
		return ajaxSuccess(sellReportService.pageShop(params));
	}
	
	/**
	 * 零售流水报表
	 * @return
	 */
	@RequestMapping(value = "to_retail_report", method = RequestMethod.GET)
	public String to_retail_report() {
		return "sell/report/report_sell";
	}
	
	@RequestMapping(value = "listPage", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listPage(PageForm pageForm,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String pd_code,
			@RequestParam(required = false) String sh_shop_code,
			@RequestParam(required = false) String pd_year,
			@RequestParam(required = false) String pd_season,
			@RequestParam(required = false) String pd_tp_code,
			@RequestParam(required = false) String pd_bd_code,
			@RequestParam(required = false) String supply_code,
			@RequestParam(required = false) String vip_code,
			@RequestParam(required = false) String sh_em_code,
			@RequestParam(required = false) String shl_state,
			@RequestParam(required = false) String minmoney,
			@RequestParam(required = false) String maxmoney,
			@RequestParam(required = false) String jmd,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put(CommonUtil.PAGESIZE, pageForm.getRows());
        params.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        params.put("begindate", begindate);
        params.put("enddate", enddate);
        params.put("pd_code", pd_code);
        params.put("sh_shop_code", sh_shop_code);
        params.put("pd_year", pd_year);
        params.put("pd_season", pd_season);
        params.put("pd_tp_code", pd_tp_code);
        params.put("pd_bd_code", pd_bd_code);
        
        params.put("supply_code", supply_code);
        params.put("vip_code", vip_code);
        params.put("sh_em_code", sh_em_code);
        params.put("shl_state", shl_state);
        params.put("minmoney", minmoney);
        params.put("maxmoney", maxmoney);
        params.put("jmd", jmd);
		return ajaxSuccess(sellReportService.retailReport(params));
	}
	
	@RequestMapping(value = "listShopList/{number}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listShopList(@PathVariable String number,
			HttpSession session) {
		return ajaxSuccess(sellReportService.listShopList(number, getCompanyid(session)));
	}
	
	@RequestMapping(value = "dayend/list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object dayEnd_list(PageForm pageForm,HttpSession session,
			String begindate,String enddate,String de_state,
			String de_shop_code,String de_em_code,String jmd) {
        Map<String,Object> params = new HashMap<String, Object>();
        bulidParam(params, getUser(session));
        params.put(CommonUtil.PAGESIZE, pageForm.getRows());
        params.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        params.put("begindate", begindate);
        params.put("enddate", enddate);
        params.put("de_shop_code", de_shop_code);
        params.put("de_em_code", de_em_code);
        params.put("de_state", de_state);
        params.put("jmd", jmd);
        PageData<T_Sell_DayEnd> pageData = dayEndService.list(params);
		return ajaxSuccess(pageData);
	}
	@RequestMapping(value = "noRate", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object noRate(PageForm pageForm,String shl_shop_code,
			String begindate,String enddate,
			String shl_main,String pd_no,
			String bd_code,String tp_code,
			String pd_year,String pd_season,
			String minrate,String maxrate,
			String shl_state,
			HttpSession session) {
        Map<String,Object> params = new HashMap<String, Object>();
        bulidParam(params, getUser(session));
        params.put(CommonUtil.PAGESIZE, pageForm.getRows());
        params.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        params.put("begindate", begindate);
        params.put("enddate", enddate);
        params.put("shl_shop_code", shl_shop_code);
        params.put("shl_main", shl_main);
        params.put("pd_no", pd_no);
        params.put("tp_code", tp_code);
        params.put("bd_code", bd_code);
        params.put("shl_state", shl_state);
        params.put("pd_year", pd_year);
        params.put("pd_season", pd_season);
        params.put("minrate", minrate);
        params.put("maxrate", maxrate);
        PageData<T_Sell_ShopList> pageData = sellReportService.noRate(params);
		return ajaxSuccess(pageData);
	}
	@RequestMapping(value = "rank", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object rank(PageForm pageForm,String shl_shop_code,
			String begindate,String enddate,String em_code,
			String type,String bd_code,String tp_code,
			HttpSession session) {
        Map<String,Object> params = new HashMap<String, Object>();
        bulidParam(params, getUser(session));
        params.put(CommonUtil.PAGESIZE, pageForm.getRows());
        params.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        params.put("begindate", begindate);
        params.put("enddate", enddate);
        params.put("shl_shop_code", shl_shop_code);
        params.put("type", type);
        params.put("bd_code", bd_code);
        params.put("tp_code", tp_code);
        params.put("em_code", em_code);
        PageData<T_Sell_ShopList> pageData = sellReportService.rank(params);
		return ajaxSuccess(pageData);
	}
	@RequestMapping(value = "empRank", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object empRank(PageForm pageForm,String shl_shop_code,
			String begindate,String enddate,
			String type,String em_code,
			HttpSession session) {
        Map<String,Object> params = new HashMap<String, Object>();
        bulidParam(params, getUser(session));
        params.put(CommonUtil.PAGESIZE, pageForm.getRows());
        params.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        params.put("begindate", begindate);
        params.put("enddate", enddate);
        params.put("shl_shop_code", shl_shop_code);
        params.put("type", type);
        params.put("em_code", em_code);
        PageData<T_Sell_ShopList> pageData = sellReportService.empRank(params);
		return ajaxSuccess(pageData);
	}
	@RequestMapping(value = "noRank", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object noRank(PageForm pageForm,String shl_shop_code,
			String begindate,String enddate,String em_code,
			String bd_code,String tp_code,String pd_code,
			String type,HttpSession session) {
        Map<String,Object> params = new HashMap<String, Object>();
        bulidParam(params, getUser(session));
        params.put(CommonUtil.PAGESIZE, pageForm.getRows());
        params.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        params.put("begindate", begindate);
        params.put("enddate", enddate);
        params.put("em_code", em_code);
        params.put("bd_code", bd_code);
        params.put("tp_code", tp_code);
        params.put("shl_shop_code", shl_shop_code);
        params.put("pd_code", pd_code);
        PageData<T_Sell_ShopList> pageData = sellReportService.noRank(params);
		return ajaxSuccess(pageData);
	}
	
	@RequestMapping(value = "listRank", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listRank(PageForm pageForm,String shl_shop_code,
			String begindate,String enddate,String em_code,
			String bd_code,String tp_code,String pd_code,
			String type,HttpSession session) {
        Map<String,Object> params = new HashMap<String, Object>();
        bulidParam(params, getUser(session));
        params.put(CommonUtil.PAGESIZE, pageForm.getRows());
        params.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        params.put("begindate", begindate);
        params.put("enddate", enddate);
        params.put("em_code", em_code);
        params.put("bd_code", bd_code);
        params.put("tp_code", tp_code);
        params.put("type", type);
        params.put("shl_shop_code", shl_shop_code);
        params.put("pd_code", pd_code);
        PageData<T_Sell_ShopList> pageData = sellReportService.listRank(params);
		return ajaxSuccess(pageData);
	}
	@RequestMapping(value = "shopRank", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object shopRank(PageForm pageForm,String sh_shop_code,
			String begindate,String enddate,String jmd,
			HttpSession session) {
        Map<String,Object> params = new HashMap<String, Object>();
        bulidParam(params, getUser(session));
        params.put(CommonUtil.PAGESIZE, pageForm.getRows());
        params.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        params.put("begindate", begindate);
        params.put("enddate", enddate);
        params.put("sh_shop_code", sh_shop_code);
        params.put("jmd", jmd);
        PageData<T_Sell_Shop> pageData = sellReportService.shopRank(params);
		return ajaxSuccess(pageData);
	}
	@RequestMapping(value = "buyRank", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object buyRank(PageForm pageForm,String shl_shop_code,
			String begindate,String enddate,String sp_code,
			String tp_code,String bd_code,
			HttpSession session) {
        Map<String,Object> params = new HashMap<String, Object>();
        bulidParam(params, getUser(session));
        params.put(CommonUtil.PAGESIZE, pageForm.getRows());
        params.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        params.put("begindate", begindate);
        params.put("enddate", enddate);
        params.put("bd_code", bd_code);
        params.put("tp_code", tp_code);
        params.put("sp_code", sp_code);
        params.put("shl_shop_code", shl_shop_code);
        PageData<T_Sell_ShopList> pageData = sellReportService.buyRank(params);
		return ajaxSuccess(pageData);
	}
	
	@RequestMapping(value = "styleRank", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object styleRank(PageForm pageForm,String shl_shop_code,
			String begindate,String enddate,
			String sz_code,String br_code,
			String tp_code,String bd_code,
			HttpSession session) {
        Map<String,Object> params = new HashMap<String, Object>();
        bulidParam(params, getUser(session));
        params.put(CommonUtil.PAGESIZE, pageForm.getRows());
        params.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        params.put("begindate", begindate);
        params.put("enddate", enddate);
        params.put("bd_code", bd_code);
        params.put("tp_code", tp_code);
        params.put("br_code", br_code);
        params.put("sz_code", sz_code);
        params.put("shl_shop_code", shl_shop_code);
        PageData<T_Sell_ShopList> pageData = sellReportService.styleRank(params);
		return ajaxSuccess(pageData);
	}
	@RequestMapping(value = "month", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object month(PageForm pageForm,String sh_shop_code,
			String date,HttpSession session) {
        Map<String,Object> params = new HashMap<String, Object>();
        bulidParam(params, getUser(session));
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        params.put("sh_shop_code", sh_shop_code);
        params.put("date", date);
        PageData<T_Sell_Shop> pageData = sellReportService.month(params);
		return ajaxSuccess(pageData);
	}

	@RequestMapping(value = "year", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object year(PageForm pageForm,String sh_shop_code,
			String date,HttpSession session) {
        Map<String,Object> params = new HashMap<String, Object>();
        bulidParam(params, getUser(session));
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        params.put("sh_shop_code", sh_shop_code);
        params.put("date", date);
        PageData<T_Sell_Shop> pageData = sellReportService.year(params);
		return ajaxSuccess(pageData);
	}
	@RequestMapping(value = "shopSell", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object shopSell(PageForm pageForm,String shl_shop_code,
			String begindate,String enddate,
			String type,String bd_code,String tp_code,
			HttpSession session) {
        Map<String,Object> params = new HashMap<String, Object>();
        bulidParam(params, getUser(session));
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        params.put("begindate", begindate);
        params.put("enddate", enddate);
        params.put("sp_code", shl_shop_code);
        params.put("type", type);
        params.put("bd_code", bd_code);
        params.put("tp_code", tp_code);
        PageData<Map<String,Object>> pageData = sellReportService.shopSell(params);
		return ajaxSuccess(pageData);
	}
	
	@RequestMapping(value = "sell_allocate", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object sell_allocate(PageForm pageForm,
			@RequestParam(required = false) String in_shop_code,
			@RequestParam(required = false) String out_shop_code,
			@RequestParam(required = false) String ac_state,
			@RequestParam(required = false) String pd_no,
			@RequestParam(required = false) String ac_number,
			HttpServletRequest request,HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        bulidParam(param, getUser(session));
        param.put("begindate", pageForm.getBegindate());
        param.put("enddate", pageForm.getEnddate());
        param.put("in_shop_code", in_shop_code);
        param.put("out_shop_code", out_shop_code);
        param.put("ac_state", ac_state);
        param.put("pd_no", pd_no);
        param.put("ac_number", ac_number);
		return ajaxSuccess(sellReportService.sell_allocate(param));
	}
	
	@RequestMapping(value = "getAllStoreDepot", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object getAllStoreDepot(String shl_shop_code,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        if(StringUtil.isNotEmpty(shl_shop_code)){
        	param.put("shopCodes", Arrays.asList(shl_shop_code.split(",")));
        }
        List<T_Base_Shop> shops = shopService.sub_list(param);
        List<T_Base_Depot> depots = null;
        if(CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype())){//总公司、分公司
        	depots = depotService.listByShop(user.getUs_shop_code(), user.getCompanyid());
        }else {
        	depots = new ArrayList<T_Base_Depot>();
		}
        Map<String, Object> resultMap = new HashMap<String,Object>();
        resultMap.put("shops", shops);
        resultMap.put("depots", depots);
        return ajaxSuccess(resultMap);
	}
	
	@RequestMapping(value = "query_stock_detail", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object query_stock_detail(ProductForm productForm,
			@RequestParam(required = false) String shl_shop_code,
			@RequestParam(required = false) String em_code,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SIDX, productForm.getSidx());
        param.put(CommonUtil.SORD, productForm.getSord());
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put("pd_code", productForm.getPd_code());
        param.put("begindate", productForm.getBegindate());
        param.put("enddate", productForm.getEnddate());
        param.put("em_code", em_code);
        
        List<String> shopCodes = new ArrayList<String>();
        if(StringUtil.isNotEmpty(shl_shop_code)){
        	shopCodes.addAll(Arrays.asList(shl_shop_code.split(",")));
        }else if(CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype())){//总公司、分公司
        	List<T_Base_Shop> shops = shopService.sub_list(param);
			for (T_Base_Shop shop : shops) {
				shopCodes.add(shop.getSp_code());
			}
        }else {
			shopCodes.add(user.getUs_shop_code());
		}
        param.put("shopCodes", shopCodes);
		return ajaxSuccess(sellReportService.query_stock_detail(param));
	}
	
	@RequestMapping(value = "loadProductSize/{pd_code}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object loadProductSize(@PathVariable String pd_code,HttpSession session) {
		return ajaxSuccess(sellReportService.loadProductSize(pd_code, getCompanyid(session)));
	}
	
	@RequestMapping(value = "query_stock_detail_size", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object query_stock_detail_size(ProductForm productForm,
			@RequestParam(required = false) String shl_shop_code,
			@RequestParam(required = false) String em_code,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SIDX, productForm.getSidx());
        param.put(CommonUtil.SORD, productForm.getSord());
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put("pd_code", productForm.getPd_code());
        param.put("begindate", productForm.getBegindate());
        param.put("enddate", productForm.getEnddate());
        param.put("shl_shop_code", shl_shop_code);
        param.put("em_code", em_code);
        
        List<String> shopCodes = new ArrayList<String>();
        if(StringUtil.isNotEmpty(shl_shop_code)){
        	shopCodes.addAll(Arrays.asList(shl_shop_code.split(",")));
        }else if(CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype())){//总公司、分公司
        	List<T_Base_Shop> shops = shopService.sub_list(param);
			for (T_Base_Shop shop : shops) {
				shopCodes.add(shop.getSp_code());
			}
			shopCodes.add(user.getUs_shop_code());
        }else {
			shopCodes.add(user.getUs_shop_code());
		}
        param.put("shopCodes", shopCodes);
		return ajaxSuccess(sellReportService.query_stock_detail_size(param));
	}
	
}
