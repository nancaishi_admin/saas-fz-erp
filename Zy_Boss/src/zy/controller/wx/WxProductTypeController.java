package zy.controller.wx;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSON;

import zy.controller.BaseController;
import zy.entity.sys.user.T_Sys_User;
import zy.entity.wx.product.T_Wx_ProductType;
import zy.form.PageForm;
import zy.service.wx.product.WxProductTypeService;
import zy.util.CommonUtil;
import zy.util.StringUtil;
import zy.vo.wx.WxProductTypeVO;

@Controller
@RequestMapping("wx/producttype")
public class WxProductTypeController extends BaseController{
	@Resource
	private WxProductTypeService wxProductTypeService;
	
	@RequestMapping(value = "to_all")
	public String to_all() {
		return "wx/producttype/all";
	}
	
	@RequestMapping(value = "to_tree")
	public String to_tree() {
		return "wx/producttype/tree";
	}
	
	@RequestMapping(value = "to_list", method = RequestMethod.GET)
	public String to_list() {
		return "wx/producttype/list";
	}
	
	@RequestMapping(value = "to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer pt_id,Model model) {
		T_Wx_ProductType productType = wxProductTypeService.load(pt_id);
		if(null != productType){
			model.addAttribute("productType",productType);
		}
		return "wx/producttype/update";
	}
	
	@RequestMapping("to_add")
	public String to_add() {
		return "wx/producttype/add";
	}
	
	@RequestMapping("to_list_dialog")
	public String to_list_dialog() {
		return "wx/producttype/list_dialog";
	}
	@RequestMapping("to_tree_dialog")
	public String to_tree_dialog() {
		return "wx/producttype/tree_dialog";
	}
	
	@RequestMapping(value = "tree", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object tree(HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        if (CommonUtil.ONE.equals(user.getShoptype())
				|| CommonUtil.TWO.equals(user.getShoptype())
				|| CommonUtil.FOUR.equals(user.getShoptype())) {// 总公司、分公司、加盟
        	params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		}else{//自营、合伙
			params.put(CommonUtil.SHOP_CODE, user.getShop_upcode());
		}
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        List<T_Wx_ProductType> types = wxProductTypeService.list(params);
		return ajaxSuccess(WxProductTypeVO.buildTree(types));
	}
	
	
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object list(PageForm pageForm,
			@RequestParam(required = false) String pt_upcode,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put("searchContent", pageForm.getSearchContent());
        params.put("pt_upcode", pt_upcode);
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        if (CommonUtil.ONE.equals(user.getShoptype())
				|| CommonUtil.TWO.equals(user.getShoptype())
				|| CommonUtil.FOUR.equals(user.getShoptype())) {// 总公司、分公司、加盟
        	params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		}else{//自营、合伙
			params.put(CommonUtil.SHOP_CODE, user.getShop_upcode());
		}
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
		return ajaxSuccess(wxProductTypeService.list(params));
	}
	
	@RequestMapping(value = "save", method = RequestMethod.POST, produces = "text/html; charset=utf-8")
	@ResponseBody
	public String save(T_Wx_ProductType productType,MultipartFile img_type, HttpSession session)throws IOException, Exception {
		T_Sys_User user = getUser(session);
		productType.setCompanyid(user.getCompanyid());
		if (CommonUtil.ONE.equals(user.getShoptype())
				|| CommonUtil.TWO.equals(user.getShoptype())
				|| CommonUtil.FOUR.equals(user.getShoptype())) {// 总公司、分公司、加盟
			productType.setPt_shop_code(user.getUs_shop_code());
		} else {// 自营、合伙
			productType.setPt_shop_code(user.getShop_upcode());
		}
		if(img_type != null && StringUtil.isNotEmpty(img_type.getOriginalFilename())){
			String uploadFileName = img_type.getOriginalFilename();
			String suffix = uploadFileName.substring(uploadFileName.lastIndexOf("."));
			String saveFileName = CommonUtil.WX_PRODUCTTYPE_PATH + "/" + new Date().getTime() + "" + new Random().nextInt(9999) + suffix;
			File savefile = new File(CommonUtil.CHECK_BASE, saveFileName);
			if (!savefile.getParentFile().exists()) {
				savefile.getParentFile().mkdirs();
			}
			img_type.transferTo(savefile);
			productType.setPt_img_path(saveFileName);
		}
		wxProductTypeService.save(productType);
		return JSON.toJSONString(ajaxSuccess(productType));
	}
	
	@RequestMapping(value = "update", method = RequestMethod.POST, produces = "text/html; charset=utf-8")
	@ResponseBody
	public String update(T_Wx_ProductType productType,MultipartFile img_type, HttpSession session)throws IOException, Exception {
		T_Sys_User user = getUser(session);
		productType.setCompanyid(user.getCompanyid());
		if(img_type != null && StringUtil.isNotEmpty(img_type.getOriginalFilename())){
			String uploadFileName = img_type.getOriginalFilename();
			String suffix = uploadFileName.substring(uploadFileName.lastIndexOf("."));
			String saveFileName = CommonUtil.WX_PRODUCTTYPE_PATH + "/" + new Date().getTime() + "" + new Random().nextInt(9999) + suffix;
			File savefile = new File(CommonUtil.CHECK_BASE, saveFileName);
			if (!savefile.getParentFile().exists()) {
				savefile.getParentFile().mkdirs();
			}
			img_type.transferTo(savefile);
			productType.setPt_img_path(saveFileName);
		}
		wxProductTypeService.update(productType);
		return JSON.toJSONString(ajaxSuccess(productType));
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(@RequestParam Integer pt_id) {
		wxProductTypeService.del(pt_id);
		return ajaxSuccess();
	}
	
}
