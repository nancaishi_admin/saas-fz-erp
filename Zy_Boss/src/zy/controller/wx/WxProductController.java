package zy.controller.wx;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSON;

import zy.controller.BaseController;
import zy.entity.base.shop.T_Base_Shop;
import zy.entity.sys.user.T_Sys_User;
import zy.entity.wx.product.T_Wx_Product;
import zy.entity.wx.product.T_Wx_ProductShop;
import zy.form.PageForm;
import zy.service.base.shop.ShopService;
import zy.service.wx.product.WxProductService;
import zy.util.CommonUtil;
import zy.util.StringUtil;
import zy.vo.wx.WxProductVO;

@Controller
@RequestMapping("wx/product")
public class WxProductController extends BaseController{
	@Resource
	private WxProductService wxProductService;
	
	@Resource
	private ShopService shopService;
	
	
	@RequestMapping(value = "to_list", method = RequestMethod.GET)
	public String to_list() {
		return "wx/product/list";
	}
	@RequestMapping(value = "to_add", method = RequestMethod.GET)
	public String to_add() {
		return "wx/product/add";
	}
	@RequestMapping(value = "to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer wp_id,Model model) {
		T_Wx_Product product = wxProductService.load(wp_id);
		if(null != product){
			model.addAttribute("product",product);
		}
		return "wx/product/update";
	}
	
	@RequestMapping("to_select_product")
	public String to_select_product() {
		return "wx/product/select_product";
	}
	
	@RequestMapping("to_shop_update_tree")
	public String to_shop_update_tree() {
		return "wx/product/shop_update_tree";
	}
	
	@RequestMapping(value = "to_qrcode", method = RequestMethod.GET)
	public String to_qrcode() {
		return "wx/product/qrcode";
	}
	
	@RequestMapping(value = "page_product", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page_product(PageForm pageForm,
			@RequestParam(required = false) String exactQuery,
			HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
        param.put("searchContent", pageForm.getSearchContent());
        param.put("exactQuery", exactQuery);
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
		if (CommonUtil.ONE.equals(user.getShoptype())
				|| CommonUtil.TWO.equals(user.getShoptype())
				|| CommonUtil.FOUR.equals(user.getShoptype())) {// 总公司、分公司、加盟
        	param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		}else{//自营、合伙
			param.put(CommonUtil.SHOP_CODE, user.getShop_upcode());
		}
		return ajaxSuccess(wxProductService.page_product(param));
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.PAGESIZE, pageForm.getRows());
        params.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        if (CommonUtil.ONE.equals(user.getShoptype())
				|| CommonUtil.TWO.equals(user.getShoptype())
				|| CommonUtil.FOUR.equals(user.getShoptype())) {// 总公司、分公司、加盟
        	params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		}else{//自营、合伙
			params.put(CommonUtil.SHOP_CODE, user.getShop_upcode());
		}
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
		return ajaxSuccess(wxProductService.page(params));
	}
	
	@RequestMapping(value = "save", method = RequestMethod.POST)
	@ResponseBody
	public Object save(@RequestParam(required = true) String products,HttpSession session) {
		List<T_Wx_Product> wxProducts = JSON.parseArray(StringUtil.decodeString(products), T_Wx_Product.class);
		wxProductService.save(wxProducts, getUser(session));
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "update", method = RequestMethod.POST)
	@ResponseBody
	public Object update(@RequestParam(required = true) String product,HttpSession session) {
		T_Wx_Product wxProduct = JSON.parseObject(StringUtil.decodeString(product), T_Wx_Product.class);
		wxProductService.update(wxProduct, getUser(session));
		return ajaxSuccess();
	}
	
	@RequestMapping(value="updateState",method=RequestMethod.POST)
	@ResponseBody
	public Object updateState(@RequestParam(required = true) Integer wp_id,
			@RequestParam(required = true) Integer wp_state){
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("wp_id", wp_id);
		params.put("wp_state", wp_state);
		wxProductService.updateState(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value="updateShop",method=RequestMethod.POST)
	@ResponseBody
	public Object updateShop(@RequestParam(required = true) String wp_number,
			@RequestParam(required = true) String shop_codes,HttpSession session){
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("wp_number", wp_number);
		params.put("shop_codes", shop_codes);
		params.put(CommonUtil.COMPANYID, getCompanyid(session));
		wxProductService.updateShop(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(@RequestParam(required = true) Integer wp_id,
			@RequestParam(required = true) String wp_number,HttpSession session) {
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("wp_id", wp_id);
		params.put("wp_number", wp_number);
		params.put(CommonUtil.COMPANYID, getCompanyid(session));
		wxProductService.del(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "loadProductShop", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object loadProductShop(String wp_number,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        List<T_Base_Shop> shops = shopService.up_sub_list(param);
        List<T_Wx_ProductShop> productShops = wxProductService.loadProductShop(wp_number, user.getCompanyid());
		return ajaxSuccess(WxProductVO.buildShopTree(shops, productShops));
	}
	
	@RequestMapping(value = "uploadimage", method = RequestMethod.POST,produces = "text/html; charset=utf-8")
	@ResponseBody
	public String uploadimage(MultipartFile upfile,HttpSession session)throws IOException {
		String uploadFileName = upfile.getOriginalFilename();
		String suffix = uploadFileName.substring(uploadFileName.lastIndexOf("."));
		String saveFileName = CommonUtil.WXPRODUCT_IMG_PATH + "/" + new Date().getTime() + "" + new Random().nextInt(9999) + suffix;
		File savefile = new File(CommonUtil.CHECK_BASE, saveFileName);
		if (!savefile.getParentFile().exists()) {
			savefile.getParentFile().mkdirs();
		}
		upfile.transferTo(savefile);
		Map<String, Object> result = new HashMap<String,Object>();
		result.put("state", "SUCCESS");  
		result.put("url", saveFileName);  
		return JSON.toJSONString(result);
	}
	
}
