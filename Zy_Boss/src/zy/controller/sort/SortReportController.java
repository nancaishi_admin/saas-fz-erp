package zy.controller.sort;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import zy.controller.BaseController;
import zy.service.sort.report.SortReportService;

@Controller
@RequestMapping("sort/report")
public class SortReportController extends BaseController{
	@Resource
	private SortReportService sortReportService;
}
