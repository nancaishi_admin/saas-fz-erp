package zy.controller.report;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.form.ProductForm;
import zy.service.report.ReportService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("report")
public class ReportController extends BaseController{
	@Resource
	private ReportService reportService;
	
	@RequestMapping(value = "to_sell_max", method = RequestMethod.GET)
	public String to_sell_max() {
		return "report/sell_max";
	}
	
	@RequestMapping(value = "to_sell_min", method = RequestMethod.GET)
	public String to_sell_min() {
		return "report/sell_min";
	}
	
	@RequestMapping(value = "to_sell_compare", method = RequestMethod.GET)
	public String to_sell_compare() {
		return "report/sell_compare";
	}
	@RequestMapping(value = "to_sell_stock", method = RequestMethod.GET)
	public String to_sell_stock() {
		return "report/sell_stock";
	}
	@RequestMapping(value = "to_brand_run", method = RequestMethod.GET)
	public String to_brand_run() {
		return "report/brand_run";
	}
	
	@RequestMapping(value = "to_no_run", method = RequestMethod.GET)
	public String to_no_run() {
		return "report/product_run";
	}
	
	@RequestMapping(value = "to_kpi_analysis", method = RequestMethod.GET)
	public String to_kpi_analysis() {
		return "report/kpi_analysis";
	}
	
	@RequestMapping(value = "to_kpi_analysis_month", method = RequestMethod.GET)
	public String to_kpi_analysis_month() {
		return "report/kpi_analysis_month";
	}
	
	@RequestMapping(value = "to_kpi_analysis_day", method = RequestMethod.GET)
	public String to_kpi_analysis_day() {
		return "report/kpi_analysis_day";
	}
	
	@RequestMapping(value = "sell_max", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object sell_max(PageForm pageForm,
			String begindate,String enddate,
			String bd_code,String tp_code,
			String shl_shop_code,String pd_year,
			String pd_season,String amount,
			String type,String pd_style,
			String chkss_day,Integer ss_day,
			String pd_no,
			HttpSession session) {
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.PAGESIZE, pageForm.getRows());
        params.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        bulidParam(params, getUser(session));
        params.put("begindate", begindate);
        params.put("enddate", enddate);
        params.put("bd_code", bd_code);
        params.put("tp_code", tp_code);
        params.put("shl_shop_code", shl_shop_code);
        params.put("pd_year", pd_year);
        params.put("pd_season", StringUtil.decodeString(pd_season));
        params.put("pd_style", StringUtil.decodeString(pd_style));
        params.put("amount", amount);
        params.put("type", type);
        params.put("chkss_day", chkss_day);
        params.put("ss_day", ss_day);
        params.put("pd_no", pd_no);
		return ajaxSuccess(reportService.sell_max(params));
	}
	
	@RequestMapping(value = "sell_max_list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object sell_max_list(PageForm pageForm,
			String begindate,String enddate,
			String bd_code,String tp_code,
			String shl_shop_code,String pd_year,
			String pd_season,String amount,
			String type,String pd_style,
			String chkss_day,Integer ss_day,
			String pd_no,
			HttpSession session) {
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.PAGESIZE, pageForm.getRows());
        params.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        bulidParam(params, getUser(session));
        params.put("begindate", begindate);
        params.put("enddate", enddate);
        params.put("bd_code", bd_code);
        params.put("tp_code", tp_code);
        params.put("shl_shop_code", shl_shop_code);
        params.put("pd_year", pd_year);
        params.put("pd_season", StringUtil.decodeString(pd_season));
        params.put("pd_style", StringUtil.decodeString(pd_style));
        params.put("amount", amount);
        params.put("type", type);
        params.put("chkss_day", chkss_day);
        params.put("ss_day", ss_day);
        params.put("pd_no", pd_no);
		return ajaxSuccess(reportService.sell_max_list(params));
	}
	
	@RequestMapping(value = "sell_compare", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object sell_compare(String begindate,String enddate,
			String shl_shop_code,String type,
			String bd_code,String tp_code,
			HttpSession session){
		Map<String,Object> params = new HashMap<String, Object>();
        bulidParam(params, getUser(session));
        params.put("begindate", begindate);
        params.put("enddate", enddate);
        params.put("shl_shop_code", shl_shop_code);
        params.put("type", type);
        params.put("bd_code", bd_code);
        params.put("tp_code", tp_code);
		return ajaxSuccess(reportService.sell_compare(params));
	}
	
	@RequestMapping(value = "sellStock", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object sellStock(ProductForm productForm,
			String shl_shop_code,
			HttpSession session){
		Map<String,Object> params = new HashMap<String, Object>();
        bulidParam(params, getUser(session));
        params.put(CommonUtil.PAGESIZE, productForm.getRows());
        params.put(CommonUtil.PAGEINDEX, productForm.getPage());
        params.put(CommonUtil.SIDX, productForm.getSidx());
        params.put(CommonUtil.SORD, productForm.getSord());
        params.put("begindate", productForm.getBegindate());
        params.put("enddate", productForm.getEnddate());
        params.put("shl_shop_code", shl_shop_code);
        params.put("bd_code", productForm.getBd_code());
        params.put("tp_code", productForm.getTp_code());
        params.put("pd_code", productForm.getPd_code());
		return ajaxSuccess(reportService.sellStock(params));
	}

	@RequestMapping(value = "kpi_analysis", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object kpi_analysis(
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String shopCode,
			HttpSession session){
		T_Sys_User user = getUser(session);
		Map<String,Object> params = new HashMap<String, Object>();
		params.put(CommonUtil.COMPANYID, getCompanyid(session));
		params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
		params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		params.put("shopCode", shopCode);
        params.put("begindate", begindate);
        params.put("enddate", enddate+" 23:59:59");
		return ajaxSuccess(reportService.kpi_analysis(params));
	}
	
	@RequestMapping(value = "kpi_analysis_month", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object kpi_analysis_month(
			@RequestParam(required = true) Integer year,
			@RequestParam(required = false) String shopCode,
			HttpSession session){
		T_Sys_User user = getUser(session);
		Map<String,Object> params = new HashMap<String, Object>();
		params.put(CommonUtil.COMPANYID, getCompanyid(session));
		params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
		params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		params.put("shopCode", shopCode);
        params.put("year", year);
        params.put("begindate", year+"-01-01");
        params.put("enddate", year+"-12-31 23:59:59");
		return ajaxSuccess(reportService.kpi_analysis_month(params));
	}
	
	@RequestMapping(value = "kpi_analysis_day", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object kpi_analysis_day(
			@RequestParam(required = true) Integer year,
			@RequestParam(required = true) Integer month,
			@RequestParam(required = false) String shopCode,
			HttpSession session){
		T_Sys_User user = getUser(session);
		Map<String,Object> params = new HashMap<String, Object>();
		params.put(CommonUtil.COMPANYID, getCompanyid(session));
		params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
		params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		params.put("shopCode", shopCode);
        params.put("year", year);
        params.put("month", month);
        params.put("begindate", year+"-"+String.format("%02d",month)+"-01");
        params.put("enddate", DateUtil.monthEndDay(params.get("begindate").toString())+" 23:59:59");
		return ajaxSuccess(reportService.kpi_analysis_day(params));
	}
	
	@RequestMapping(value = "brand_run", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object brand_run(
			String begindate,String enddate,
			String depot_code,String pd_year,String type,
			String pd_season,String bd_code,String tp_code,
			HttpSession session){
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("begindate", begindate);
		bulidParam(params, getUser(session));
        params.put("enddate", enddate);
        params.put("depot_code", depot_code);
        params.put("pd_year", pd_year);
        params.put("pd_season", pd_season);
        params.put("bd_code", bd_code);
        params.put("tp_code", tp_code);
        params.put("type", type);
		return ajaxSuccess(reportService.brand_run(params));
	}
	@RequestMapping(value = "product_run", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object product_run(PageForm pageForm,
			String begindate,String enddate,
			String depot_code,String pd_year,
			String pd_season,String bd_code,String tp_code,
			HttpSession session){
		Map<String,Object> params = new HashMap<String, Object>();
		params.put(CommonUtil.PAGESIZE, pageForm.getRows());
        params.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
		bulidParam(params, getUser(session));
		params.put("begindate", begindate);
        params.put("enddate", enddate);
        params.put("depot_code", depot_code);
        params.put("pd_year", pd_year);
        params.put("pd_season", pd_season);
        params.put("bd_code", bd_code);
        params.put("tp_code", tp_code);
		return ajaxSuccess(reportService.product_run(params));
	}
	
}
