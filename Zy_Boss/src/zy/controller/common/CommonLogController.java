package zy.controller.common;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import zy.controller.BaseController;
import zy.service.common.log.CommonLogService;

@Controller
@RequestMapping(value="common/log")
public class CommonLogController extends BaseController{
	@Resource
	private CommonLogService commonLogService;
}
