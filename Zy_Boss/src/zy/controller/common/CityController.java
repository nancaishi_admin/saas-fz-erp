package zy.controller.common;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.common.city.Common_City;
import zy.service.common.city.CityService;
@Controller
@RequestMapping(value="/common/city")
public class CityController extends BaseController{
	@Resource
	private CityService cityService; 
	
	@RequestMapping(value = "queryProvince", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object queryProvince(HttpServletRequest request,HttpSession session){
		List<Common_City> list = cityService.queryProvince();
		return ajaxSuccess(list, "查询成功, 共" + list.size() + "条数据");
	}
	@RequestMapping(value = "queryCity", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object queryCity(Integer cy_id,HttpServletRequest request,HttpSession session){
		List<Common_City> list = cityService.queryCity(cy_id);
		return ajaxSuccess(list, "查询成功, 共" + list.size() + "条数据");
	}
	@RequestMapping(value = "queryTown", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object queryTown(Integer cy_id,HttpServletRequest request,HttpSession session){
		List<Common_City> list = cityService.queryTown(cy_id);
		return ajaxSuccess(list, "查询成功, 共" + list.size() + "条数据");
	}
}
