package zy.controller.common;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.service.common.dict.CommonDictService;

@Controller
@RequestMapping(value="common/dict")
public class CommonDictController extends BaseController{

	@Resource
	private CommonDictService commonDictService;
	
	@RequestMapping("to_list_dialog")
	public String to_list_dialog() {
		return "common/dict/list_dialog";
	}
	
	@RequestMapping(value = "list/{dt_type}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object list(@PathVariable String dt_type) {
		return ajaxSuccess(commonDictService.list(dt_type));
	}
}
