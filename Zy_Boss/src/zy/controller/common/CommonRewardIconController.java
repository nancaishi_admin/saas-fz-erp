package zy.controller.common;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.service.common.rewardicon.CommonRewardIconService;

@Controller
@RequestMapping(value="common/rewardicon")
public class CommonRewardIconController extends BaseController{

	@Resource
	private CommonRewardIconService commonRewardIconService;
	
	@RequestMapping("to_list_dialog")
	public String to_list_dialog() {
		return "common/rewardicon/list_dialog";
	}
	
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object list() {
		return ajaxSuccess(commonRewardIconService.list());
	}
	
}
