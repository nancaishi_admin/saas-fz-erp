package zy.controller.base;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.base.stream.T_Base_Stream;
import zy.entity.sys.user.T_Sys_User;
import zy.service.base.stream.StreamService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("base/stream")
public class StreamController extends BaseController{
	
	@Resource
	private StreamService streamService;
	
	@RequestMapping(value = "/to_list", method = RequestMethod.GET)
	public String to_list() {
		return "base/stream/list";
	}
	
	@RequestMapping("to_add")
	public String to_add() {
		return "base/stream/add";
	}
	
	@RequestMapping(value = "/to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer se_id,Model model) {
		T_Base_Stream stream = streamService.queryByID(se_id);
		if(null != stream){
			model.addAttribute("model",stream);
		}
		return "base/stream/update";
	}
	
	@RequestMapping("to_list_dialog")
	public String to_list_dialog() {
		return "base/stream/list_dialog";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(HttpServletRequest request,HttpSession session) {
		String name = request.getParameter("searchContent");
		String page = request.getParameter("page"); // 页码
        String rows = request.getParameter("rows"); // 每页条数
        String sidx = request.getParameter("sidx");
        String sord = request.getParameter("sord");
        int pageIndex = 1;
        int pageSize = super.pagesize;
        if (org.apache.commons.lang.StringUtils.isNotEmpty(page)) {
        	pageIndex = Integer.parseInt(page);
        	pageIndex = pageIndex == 0 ? 1 : pageIndex;
        }
        if (org.apache.commons.lang.StringUtils.isNotEmpty(rows)) {
        	pageSize = Integer.parseInt(rows);
        }
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.PAGESIZE, pageSize);
        param.put(CommonUtil.PAGEINDEX, pageIndex);
        param.put(CommonUtil.SIDX, sidx);
        param.put(CommonUtil.SORD, sord);
        param.put("name", name);
        if(CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype())){//总公司、分公司
        	param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		}else{//自营、加盟、合伙
			param.put(CommonUtil.SHOP_CODE, user.getShop_upcode());
		}
		return ajaxSuccess(streamService.page(param));
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Base_Stream stream, HttpSession session) {
		T_Sys_User user = getUser(session);
		stream.setCompanyid(user.getCompanyid());
		stream.setSe_spell(StringUtil.getSpell(stream.getSe_name()));
		if (CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype())) {// 总公司、分公司
			stream.setSe_sp_code(user.getUs_shop_code());
		} else {// 自营、加盟、合伙
			stream.setSe_sp_code(user.getShop_upcode());
		}
		streamService.save(stream);
		return ajaxSuccess(stream);
	}

	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Base_Stream stream,HttpSession session) {
		T_Sys_User user = getUser(session);
		stream.setCompanyid(user.getCompanyid());
		stream.setSe_spell(StringUtil.getSpell(stream.getSe_name()));
		if (CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype())) {// 总公司、分公司
			stream.setSe_sp_code(user.getUs_shop_code());
		} else {// 自营、加盟、合伙
			stream.setSe_sp_code(user.getShop_upcode());
		}
		streamService.update(stream);
		return ajaxSuccess(stream);
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object delp(@RequestParam Integer se_id) {
		streamService.del(se_id);
		return ajaxSuccess();
	}
}
