package zy.controller.base;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.base.affiliate.T_Base_Affiliate;
import zy.service.base.affiliate.AffiliateService;
import zy.util.CommonUtil;
import zy.util.StringUtil;
import zy.vo.base.AffiliateVO;

@Controller
@RequestMapping("base/affiliate")
public class AffiliateController extends BaseController{
	
	@Resource
	private AffiliateService affiliateService;

	@RequestMapping("to_all")
	public String to_all() {
		return "base/affiliate/all";
	}
	@RequestMapping("to_tree")
	public String to_tree() {
		return "base/affiliate/tree";
	}
	@RequestMapping(value = "/to_list", method = RequestMethod.GET)
	public String to_list() {
		return "base/affiliate/list";
	}
	@RequestMapping(value = "/to_add", method = RequestMethod.GET)
	public String to_add() {
		return "base/affiliate/add";
	}
	@RequestMapping(value = "/to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer af_id,Model model) {
		T_Base_Affiliate affiliate = affiliateService.queryByID(af_id);
		if(null != affiliate){
			model.addAttribute("affiliate",affiliate);
		}
		return "base/affiliate/update";
	}
	
	@RequestMapping(value = "tree", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object tree(HttpSession session) {
		Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
		List<T_Base_Affiliate> list = affiliateService.list(param);
		return ajaxSuccess(AffiliateVO.buildTree(list));
	}
	
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object list(String searchContent, String af_upcode,HttpSession session) {
		Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put("searchContent", StringUtil.trimString(searchContent));
        param.put("af_upcode", af_upcode);
		return ajaxSuccess(affiliateService.list(param));
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Base_Affiliate affiliate,HttpSession session) {
		affiliate.setCompanyid(getCompanyid(session));
		Integer id = affiliateService.queryByName(affiliate);
		if (null != id && id > 0) {
			return result(EXISTED);
		} else {
			affiliateService.save(affiliate);
			return ajaxSuccess(affiliate);
		}
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Base_Affiliate affiliate,HttpSession session) {
		affiliate.setCompanyid(getCompanyid(session));
		Integer id = affiliateService.queryByName(affiliate);
		if (null != id && id > 0) {
			return result(EXISTED);
		} else {
			affiliateService.update(affiliate);
			return ajaxSuccess(affiliate);
		}
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(@RequestParam Integer af_id) {
		affiliateService.del(af_id);
		return ajaxSuccess();
	}
}
