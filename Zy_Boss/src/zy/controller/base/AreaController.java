package zy.controller.base;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.base.area.T_Base_Area;
import zy.service.base.area.AreaService;
import zy.util.CommonUtil;
import zy.util.StringUtil;
import zy.vo.base.AreaVO;

@Controller
@RequestMapping("base/area")
public class AreaController extends BaseController{
	
	@Resource
	private AreaService areaService;
	
	@RequestMapping("to_all")
	public String to_all() {
		return "base/area/all";
	}
	@RequestMapping("to_tree")
	public String to_tree() {
		return "base/area/tree";
	}
	@RequestMapping(value = "/to_list", method = RequestMethod.GET)
	public String to_list() {
		return "base/area/list";
	}
	@RequestMapping(value = "/to_add", method = RequestMethod.GET)
	public String to_add() {
		return "base/area/add";
	}
	@RequestMapping(value = "/to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer ar_id,Model model) {
		T_Base_Area area = areaService.queryByID(ar_id);
		if(null != area){
			model.addAttribute("area",area);
		}
		return "base/area/update";
	}
	@RequestMapping("to_tree_dialog")
	public String to_tree_dialog() {
		return "base/area/tree_dialog";
	}
	
	@RequestMapping(value = "tree", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object tree(HttpSession session) {
		Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
		List<T_Base_Area> list = areaService.list(param);
		return ajaxSuccess(AreaVO.buildTree(list));
	}
	
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object list(String searchContent, String ar_upcode,HttpSession session) {
		Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put("searchContent", StringUtil.trimString(searchContent));
        param.put("ar_upcode", ar_upcode);
		return ajaxSuccess(areaService.list(param));
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Base_Area area,HttpSession session) {
		 area.setCompanyid(getCompanyid(session));
		 area.setAr_spell(StringUtil.getSpell(area.getAr_name()));
		 Integer id = areaService.queryByName(area);
		 if(null != id && id > 0){
			return result(EXISTED);
		 }else{
			 areaService.save(area);
			 return ajaxSuccess(area);
		 }
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Base_Area area,HttpSession session) {
		area.setCompanyid(getCompanyid(session));
		area.setAr_spell(StringUtil.getSpell(area.getAr_name()));
		Integer id = areaService.queryByName(area);
		if (null != id && id > 0) {
			return result(EXISTED);
		} else {
			areaService.update(area);
			return ajaxSuccess(area);
		}
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(@RequestParam Integer ar_id) {
		areaService.del(ar_id);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "listBySupply", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public void listBySupply(HttpServletRequest request,HttpSession session,HttpServletResponse response) {
		try {
			PrintWriter out=null;
			Map<String,Object> param = new HashMap<String, Object>();
			param.put(CommonUtil.COMPANYID, getCompanyid(session));
			String ar_list="{\"data\":{\"items\":[";
			List<T_Base_Area> list = areaService.list(param);
			for (int i=0;i<list.size();i++){
				ar_list+="{\"sp_ar_code\":\""+list.get(i).getAr_code()+"\",\"sp_ar_name\":\""+list.get(i).getAr_name()+"\"},";
			}
			ar_list=ar_list.substring(0,ar_list.length()-1);
			ar_list+="]}}";
			out = response.getWriter();
			out.print(ar_list);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
