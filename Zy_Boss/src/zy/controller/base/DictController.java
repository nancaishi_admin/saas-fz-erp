package zy.controller.base;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.base.dict.T_Base_Dict;
import zy.entity.base.dict.T_Base_DictList;
import zy.entity.sys.user.T_Sys_User;
import zy.service.base.dict.DictService;
import zy.util.CommonUtil;
@Controller
@RequestMapping("base/dict")
public class DictController extends BaseController{
	@Resource
	private DictService dictService;
	
	@RequestMapping(value = "/to_uplist", method = RequestMethod.GET)
	public String to_uplist() {
		return "base/dict/list";
	}
	@RequestMapping(value = "/to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer dtl_id,Model model) {
		T_Base_DictList dict = dictService.queryByID(dtl_id);
		if(null != dict){
			model.addAttribute("model",dict);
		}
		return "base/dict/update";
	}
	
	@RequestMapping("to_add")
	public String to_add() {
		return "base/dict/add";
	}

	@RequestMapping(value = "uplist", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object uplist(HttpServletRequest request,HttpSession session) {
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
		List<T_Base_Dict> list = dictService.uplist(param);
		return ajaxSuccess(list, "查询成功, 共" + list.size() + "条数据");
	}
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object list(HttpServletRequest request,HttpSession session) {
		Object up_code = request.getParameter("dtl_upcode");
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put("up_code", up_code);
		List<T_Base_DictList> list = dictService.list(param);
		return ajaxSuccess(list, "查询成功, 共" + list.size() + "条数据");
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Base_DictList dictList,HttpServletRequest request,HttpSession session) {
		 T_Sys_User user = getUser(session);
		 dictList.setCompanyid(user.getCompanyid());
		 Integer id = dictService.queryByName(dictList);
		 if(null != id && id > 0){
			return result(EXISTED);
		 }else{
			 dictService.save(dictList);
			 return ajaxSuccess(dictList);
		 }
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Base_DictList dictList,HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
		dictList.setCompanyid(user.getCompanyid());
		 Integer id = dictService.queryByName(dictList);
		 if(null != id && id > 0){
			return result(EXISTED);
		 }else{
			 dictService.update(dictList);
			 return ajaxSuccess(dictList);
		 }
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(@RequestParam Integer dtl_id,HttpServletRequest request,HttpSession session) {
		 dictService.del(dtl_id);
		 return ajaxSuccess();
	}
	
	/**
	 * 商品资料 基础字典获取下拉框
	 * @param request
	 * @param session
	 * @param response
	 */
	@RequestMapping(value = "listByProduct", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public void listByProduct(HttpServletRequest request,HttpSession session,HttpServletResponse response) {
		try {
			PrintWriter out=null;
	        T_Sys_User user = getUser(session);
	        Map<String,Object> param = new HashMap<String, Object>();
	        param.put(CommonUtil.COMPANYID, user.getCompanyid());
	        List<T_Base_DictList> list = dictService.list(param);
	        List<T_Base_DictList> seasonList = new ArrayList<T_Base_DictList>();//季节
	        List<T_Base_DictList> unitnameList = new ArrayList<T_Base_DictList>();//单位
	        List<T_Base_DictList> styleList = new ArrayList<T_Base_DictList>();//款式
	        List<T_Base_DictList> stuffList = new ArrayList<T_Base_DictList>();//面料
	        List<T_Base_DictList> priceList = new ArrayList<T_Base_DictList>();//价格特性
	        if(list!=null && list.size()>0){
	        	for(int i=0;i<list.size();i++){
		        	if(CommonUtil.SEASON.equals(list.get(i).getDtl_upcode())){//保存季节
		        		seasonList.add(list.get(i));
		        		continue;
		        	}
		        	if(CommonUtil.UNITNAME.equals(list.get(i).getDtl_upcode())){//保存单位
		        		unitnameList.add(list.get(i));
		        		continue;
		        	}
		        	if(CommonUtil.STYLE.equals(list.get(i).getDtl_upcode())){//保存款式
		        		styleList.add(list.get(i));
		        		continue;
		        	}
		        	if(CommonUtil.STUFF.equals(list.get(i).getDtl_upcode())){//保存面料
		        		stuffList.add(list.get(i));
		        		continue;
		        	}
		        	if(CommonUtil.PRICE.equals(list.get(i).getDtl_upcode())){//价格特性
		        		priceList.add(list.get(i));
		        		continue;
		        	}
		        }
	        }
	        
	        StringBuffer dictList = new StringBuffer();
	        dictList.append("{\"data\":{");
	        //季节
	        dictList.append("\"seasonitems\":[");
	        for (int i=0;i<seasonList.size();i++){
	        	dictList.append("{\"dtl_code\":\"").append(seasonList.get(i).getDtl_code())
	        				.append("\",\"dtl_name\":\"").append(seasonList.get(i).getDtl_name()).append("\"}");
	        	if(i<seasonList.size()-1){
	        		dictList.append(",");
	        	}
			}
	        dictList.append("],");
	        dictList.append("\"seasonitemsquery\":[");
	        dictList.append("{\"dtl_code\":\"\",\"dtl_name\":\"全部\"},");
	        for (int i=0;i<seasonList.size();i++){
	        	dictList.append("{\"dtl_code\":\"").append(seasonList.get(i).getDtl_code())
	        				.append("\",\"dtl_name\":\"").append(seasonList.get(i).getDtl_name()).append("\"}");
	        	if(i<seasonList.size()-1){
	        		dictList.append(",");
	        	}
			}
	        dictList.append("],");
	        //单位
	        dictList.append("\"unitnameitems\":[");
	        for (int i=0;i<unitnameList.size();i++){
	        	dictList.append("{\"dtl_code\":\"").append(unitnameList.get(i).getDtl_code())
	        				.append("\",\"dtl_name\":\"").append(unitnameList.get(i).getDtl_name()).append("\"}");
	        	if(i<unitnameList.size()-1){
	        		dictList.append(",");
	        	}
			}
	        dictList.append("],");
	        //面料
	        dictList.append("\"stuffitems\":[");
	        for (int i=0;i<stuffList.size();i++){
	        	dictList.append("{\"dtl_code\":\"").append(stuffList.get(i).getDtl_code())
	        				.append("\",\"dtl_name\":\"").append(stuffList.get(i).getDtl_name()).append("\"}");
	        	if(i<stuffList.size()-1){
	        		dictList.append(",");
	        	}
			}
	        dictList.append("],");
	        //款式
	        dictList.append("\"styleitems\":[");
	        for (int i=0;i<styleList.size();i++){
	        	dictList.append("{\"dtl_code\":\"").append(styleList.get(i).getDtl_code())
	        				.append("\",\"dtl_name\":\"").append(styleList.get(i).getDtl_name()).append("\"}");
	        	if(i<styleList.size()-1){
	        		dictList.append(",");
	        	}
			}
	        dictList.append("],");
	        dictList.append("\"styleitemsquery\":[");
	        dictList.append("{\"dtl_code\":\"\",\"dtl_name\":\"全部\"},");
	        for (int i=0;i<styleList.size();i++){
	        	dictList.append("{\"dtl_code\":\"").append(styleList.get(i).getDtl_code())
	        				.append("\",\"dtl_name\":\"").append(styleList.get(i).getDtl_name()).append("\"}");
	        	if(i<styleList.size()-1){
	        		dictList.append(",");
	        	}
			}
	        dictList.append("],");
	        
	        //价格特性
	        dictList.append("\"priceitems\":[");
	        for (int i=0;i<priceList.size();i++){
	        	dictList.append("{\"dtl_code\":\"").append(priceList.get(i).getDtl_code())
	        				.append("\",\"dtl_name\":\"").append(priceList.get(i).getDtl_name()).append("\"}");
	        	if(i<priceList.size()-1){
	        		dictList.append(",");
	        	}
			}
	        dictList.append("]");
	        
	        dictList.append("}}");
	        
			out = response.getWriter();
			out.print(dictList.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
