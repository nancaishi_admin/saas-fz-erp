package zy.controller.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.base.emp.T_Base_EmpGroup;
import zy.entity.base.emp.T_Base_EmpGroupList;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.base.emp.EmpGroupService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("base/empgroup")
public class EmpGroupController extends BaseController{
	@Resource
	private EmpGroupService empGroupService;
	
	@RequestMapping(value = "to_list", method = RequestMethod.GET)
	public String to_list() {
		return "base/empgroup/list";
	}
	@RequestMapping("to_add")
	public String to_add() {
		return "base/empgroup/add";
	}
	@RequestMapping(value = "to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer eg_id,Model model) {
		T_Base_EmpGroup empGroup = empGroupService.load(eg_id);
		if(null != empGroup){
			model.addAttribute("empGroup",empGroup);
		}
		return "base/empgroup/update";
	}
	@RequestMapping("to_list_dialog")
	public String to_list_dialog() {
		return "base/empgroup/list_dialog";
	}
	
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object list(PageForm pageForm,HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        params.put("searchContent", pageForm.getSearchContent());
		return ajaxSuccess(empGroupService.list(params));
	}
	
	@RequestMapping(value = "listDetails", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listDetails(PageForm pageForm,
			@RequestParam(required = true) String eg_code,
			HttpSession session) {
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        params.put("eg_code", eg_code);
		return ajaxSuccess(empGroupService.listDetails(params));
	}
	
	@RequestMapping(value = "listEmps", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listEmps(PageForm pageForm,
			@RequestParam(required = true) String eg_code,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put(CommonUtil.SHOP_TYPE, user.getShoptype());//店铺类型
        params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());//当前登录用户店铺编号
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        params.put("eg_code", eg_code);
		return ajaxSuccess(empGroupService.listEmps(params));
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Base_EmpGroup empGroup,String codes,HttpSession session) {
		empGroup.setEg_name(StringUtil.decodeString(empGroup.getEg_name()));
		empGroup.setEg_remark(StringUtil.decodeString(empGroup.getEg_remark()));
		List<T_Base_EmpGroupList> groupLists = new ArrayList<T_Base_EmpGroupList>();
		if(StringUtil.isNotEmpty(codes)){
			String[] codeArr = codes.split(",");
			T_Base_EmpGroupList item = null;
			for(String code:codeArr){
				item = new T_Base_EmpGroupList();
				item.setEgl_em_code(code);
				groupLists.add(item);
			}
		}
		empGroupService.save(empGroup, groupLists, getUser(session));
		return ajaxSuccess(empGroup);
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Base_EmpGroup empGroup,String codes,HttpSession session) {
		empGroup.setEg_name(StringUtil.decodeString(empGroup.getEg_name()));
		empGroup.setEg_remark(StringUtil.decodeString(empGroup.getEg_remark()));
		List<T_Base_EmpGroupList> groupLists = new ArrayList<T_Base_EmpGroupList>();
		if(StringUtil.isNotEmpty(codes)){
			String[] codeArr = codes.split(",");
			T_Base_EmpGroupList item = null;
			for(String code:codeArr){
				item = new T_Base_EmpGroupList();
				item.setEgl_em_code(code);
				groupLists.add(item);
			}
		}
		empGroupService.update(empGroup, groupLists, getUser(session));
		return ajaxSuccess(empGroup);
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(@RequestParam Integer eg_id) {
		empGroupService.del(eg_id);
		return ajaxSuccess();
	}
	
	
}
