package zy.controller.base;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.PageData;
import zy.entity.base.size.T_Base_Size;
import zy.entity.base.size.T_Base_SizeList;
import zy.entity.base.size.T_Base_Size_Group;
import zy.entity.sys.user.T_Sys_User;
import zy.service.base.size.SizeService;
import zy.util.CommonUtil;
import zy.vo.base.BaseVO;
import zy.vo.base.SizeVO;

import com.alibaba.fastjson.JSONObject;
@Controller
@RequestMapping("base/size")
public class SizeController extends BaseController{
	@Resource
	private SizeService sizeService;
	
	@RequestMapping(value = "/to_list", method = RequestMethod.GET)
	public String to_list() {
		return "base/size/list";
	}
	@RequestMapping("to_list_dialog")
	public String to_list_dialog() {
		return "base/size/list_dialog";
	}
	@RequestMapping(value = "/to_list_group", method = RequestMethod.GET)
	public String to_list_group() {
		return "base/size/list_group";
	}
	@RequestMapping(value = "/to_update_group", method = RequestMethod.GET)
	public String to_update_group(@RequestParam Integer szg_id,Model model,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>(2);
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        T_Base_Size_Group size = sizeService.queryGroupByID(szg_id);
		List<T_Base_Size> list = sizeService.list(param);
		for (Iterator<T_Base_Size> iter = list.iterator(); iter.hasNext();) {  
			T_Base_Size i = iter.next();  
            for(T_Base_SizeList sizeList:size.getSizelist()){
				if(i.getSz_code().equals(sizeList.getSzl_sz_code())){
					iter.remove();
				}
			}
        }  
		if(null != size){
			model.addAttribute("size_group",size);
			model.addAttribute("size_list", list);
		}
		return "base/size/update_group";
	}
	
	@RequestMapping(value = "/to_update", method = RequestMethod.GET)
	public String to_updatesub(@RequestParam Integer sz_id,Model model) {
		T_Base_Size size = sizeService.queryByID(sz_id);
		if(null != size){
			model.addAttribute("model",size);
		}
		return "base/size/update";
	}
	
	@RequestMapping("to_add_group")
	public String to_add_group(HttpSession session) {
		return "base/size/add_group";
	}
	
	@RequestMapping("to_add")
	public String to_add() {
		return "base/size/add";
	}
	
	/**
	 * 查询尺码组弹出框页面，支持单选和多选
	 */
	@RequestMapping("to_list_group_dialog")
	public String to_list_group_dialog() {
		return "base/size/list_group_dialog";
	}
	
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object list(HttpServletRequest request,HttpSession session){
		String name = request.getParameter("sz_name");
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>(2);
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put("name", name);
		List<T_Base_Size> sizeList = sizeService.list(param);
		return ajaxSuccess(sizeList, "查询成功, 共" + sizeList.size() + "条数据");
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(HttpServletRequest request,HttpSession session) {
		String name = request.getParameter("searchContent");
		String page = request.getParameter("page"); // 页码
        String rows = request.getParameter("rows"); // 每页条数
        String sidx = request.getParameter("sidx");
        String sord = request.getParameter("sord");
        int pageIndex = 1;
        int pageSize = super.pagesize;
        if (org.apache.commons.lang.StringUtils.isNotEmpty(page)) {
        	pageIndex = Integer.parseInt(page);
        	pageIndex = pageIndex == 0 ? 1 : pageIndex;
        }
        if (org.apache.commons.lang.StringUtils.isNotEmpty(rows)) {
        	pageSize = Integer.parseInt(rows);
        }
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.PAGESIZE, pageSize);
        param.put(CommonUtil.PAGEINDEX, pageIndex);
        param.put(CommonUtil.SIDX, sidx);
        param.put(CommonUtil.SORD, sord);
        param.put("name", name);
		PageData<T_Base_Size> pageData = sizeService.page(param);
		return ajaxSuccess(pageData, "查询成功, 共" + pageData.getList().size() + "条数据");
	}
	
	/**
	 * 根据主表ID查询子表的list
	 * */
	@RequestMapping(value = "listGroup", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listGroup(HttpServletRequest request,HttpSession session) {
		String name = request.getParameter("searchContent");
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put("name", name);
		List<T_Base_Size_Group> list = sizeService.listGroup(param);
		return ajaxSuccess(list, "查询成功, 共" + list.size() + "条数据");
	}
	
	/**
	 * 根据主表ID查询子表的list
	 * */
	@RequestMapping(value = "listByID", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listByID(@RequestParam String szl_szg_code,HttpServletRequest request,HttpSession session) {
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put("szl_szg_code", szl_szg_code);
		List<T_Base_SizeList> list = sizeService.listByID(param);
		return ajaxSuccess(list, "查询成功, 共" + list.size() + "条数据");
	}
	
	@RequestMapping(value = "saveGroup", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object saveGroup(HttpServletRequest request,HttpSession session) {
		 T_Sys_User user = getUser(session);
		 String jsondata = request.getParameter("jsondata");
		 Map<String,Object> map = BaseVO.buildSizeGroup(jsondata,user);
		 sizeService.saveGroup(map);
		 return ajaxSuccess();
	}
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Base_Size size,HttpServletRequest request,HttpSession session) {
		 T_Sys_User user = getUser(session);
		 size.setCompanyid(user.getCompanyid());
		 Integer id = sizeService.queryByName(size);
		 if(null != id && id > 0){
			return result(EXISTED);
		 }else{
			 sizeService.save(size);
			 return ajaxSuccess(size);
		 }
	}
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Base_Size model,HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
		model.setCompanyid(user.getCompanyid());
		sizeService.update(model);
		return ajaxSuccess(model);
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(@RequestParam Integer sz_id) {
		 sizeService.del(sz_id);
		 return ajaxSuccess();
	}
	
	@RequestMapping(value = "updateGroup", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object updateGroup(HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
		 String jsondata = request.getParameter("jsondata");
		 Map<String,Object> map = BaseVO.buildSizeGroup(jsondata,user);
		sizeService.updateGroup(map);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "delGroup", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object delGroup(@RequestParam Integer szg_id,HttpServletRequest request,HttpSession session) {
		 sizeService.delGroup(szg_id);
		 return ajaxSuccess();
	}
	
	/**
	 * 商品资料新增、修改查询尺码组信息
	 */
	@RequestMapping(value = "listGroupByProduct", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public void listGroupByProduct(HttpServletRequest request,HttpSession session,HttpServletResponse response) {
		PrintWriter out=null;
		try {
	        T_Sys_User user = getUser(session);
	        Map<String,Object> param = new HashMap<String, Object>();
	        param.put(CommonUtil.COMPANYID, user.getCompanyid());
			String sp_list="{\"data\":{\"items\":[";
			List<T_Base_Size_Group> list = sizeService.listGroup(param);
			for (int i=0;i<list.size();i++){
				sp_list+="{\"szg_code\":\""+list.get(i).getSzg_code()+"\",\"szg_name\":\""+list.get(i).getSzg_name()+"\"},";
			}
			sp_list=sp_list.substring(0,sp_list.length()-1);
			sp_list+="]}}";
			out = response.getWriter();
			out.print(sp_list);
			out.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			if(out != null)out.close();
		}
	}
	@RequestMapping(value = "sizeListByProduct", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public void sizeListByProduct(@RequestParam String szg_code,
			HttpServletRequest request,HttpSession session,HttpServletResponse response) {
		JSONObject jsonObject = new JSONObject();
		PrintWriter out=null;
		try {
	        T_Sys_User user = getUser(session);
	        Map<String,Object> param = new HashMap<String, Object>();
	        param.put(CommonUtil.COMPANYID, user.getCompanyid());
	        param.put("szl_szg_code", szg_code);
	        List<T_Base_SizeList> list = sizeService.listByID(param);
	        out = response.getWriter();
	        if(list != null&&list.size()>0){
				jsonObject.put("size_html",SizeVO.toSizeHtmlByList(list));
			}else{
				jsonObject.put("size_html","");
			}
			out.print(jsonObject);
			out.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			if(out != null)out.close();
		}
	}
}
