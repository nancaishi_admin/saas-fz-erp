package zy.controller.base;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.base.department.T_Base_Department;
import zy.entity.sys.user.T_Sys_User;
import zy.service.base.department.DepartmentService;
import zy.util.CommonUtil;
import zy.util.StringUtil;
@Controller
@RequestMapping("base/department")
public class DepartmentController extends BaseController{
	@Resource
	private DepartmentService departmentService;
	
	@RequestMapping(value = "/to_list", method = RequestMethod.GET)
	public String to_list() {
		return "base/department/list";
	}
	@RequestMapping(value = "/to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer dm_id,Model model) {
		T_Base_Department dept = departmentService.queryByID(dm_id);
		if(null != dept){
			model.addAttribute("model",dept);
		}
		return "base/department/update";
	}
	
	@RequestMapping("to_add")
	public String to_add() {
		return "base/department/add";
	}
	
	/**
	 * 查询弹出框页面，支持单选和多选
	 */
	@RequestMapping("to_list_dialog")
	public String to_list_dialog() {
		return "base/department/list_dialog";
	}
	
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object list(HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		List<T_Base_Department> list = departmentService.list(param);
		return ajaxSuccess(list, "查询成功, 共" + list.size() + "条数据");
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Base_Department model,HttpServletRequest request,HttpSession session) {
		 T_Sys_User user = getUser(session);
		 model.setDm_shop_code(user.getUs_shop_code());
		 model.setCompanyid(user.getCompanyid());
		 model.setDm_spell(StringUtil.getSpell(model.getDm_name()));
		 Integer id = departmentService.queryByName(model);
		 if(null != id && id > 0){
			return result(EXISTED);
		 }else{
			 departmentService.save(model);
			 return ajaxSuccess(model);
		 }
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Base_Department model,HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
		model.setDm_shop_code(user.getUs_shop_code());
		 model.setCompanyid(user.getCompanyid());
		 model.setDm_spell(StringUtil.getSpell(model.getDm_name()));
		 Integer id = departmentService.queryByName(model);
		 if(null != id && id > 0){
			return result(EXISTED);
		 }else{
			 departmentService.update(model);
			 return ajaxSuccess(model);
		 }
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(@RequestParam Integer dm_id,HttpServletRequest request,HttpSession session) {
		 departmentService.del(dm_id);
		 return ajaxSuccess();
	}
	
	@RequestMapping(value = "listByEmp", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public void listByEmp(HttpServletRequest request,HttpSession session,HttpServletResponse response) {
		try {
			PrintWriter out=null;
			T_Sys_User user = getUser(session);
			Map<String,Object> param = new HashMap<String, Object>();
			param.put(CommonUtil.COMPANYID, user.getCompanyid());
	        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
	        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
	        param.put(CommonUtil.SHOP_UPCODE, user.getShop_upcode());
			String dm_list="{\"data\":{\"items\":[";
			List<T_Base_Department> list = departmentService.list(param);
			for (int i=0;i<list.size();i++){
				dm_list+="{\"em_dm_code\":\""+list.get(i).getDm_code()+"\",\"em_dm_name\":\""+list.get(i).getDm_name()+"\"},";
			}
			dm_list=dm_list.substring(0,dm_list.length()-1);
			dm_list+="]}}";
			out = response.getWriter();
			out.print(dm_list);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
