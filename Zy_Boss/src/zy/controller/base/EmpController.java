package zy.controller.base;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.PageData;
import zy.entity.base.department.T_Base_Department;
import zy.entity.base.emp.T_Base_Emp;
import zy.entity.sys.user.T_Sys_User;
import zy.service.base.department.DepartmentService;
import zy.service.base.emp.EmpService;
import zy.util.CommonUtil;
import zy.util.MD5;
import zy.util.StringUtil;
import zy.vo.base.EmpVO;

@Controller
@RequestMapping("base/emp")
public class EmpController extends BaseController{
	@Resource
	private DepartmentService departmentService;
	@Resource
	private EmpService empService;
	
	@RequestMapping("to_all")
	public String to_all() {
		return "base/emp/all";
	}
	@RequestMapping("to_tree")
	public String to_tree() {
		return "base/emp/tree";
	}
	@RequestMapping(value = "/to_list", method = RequestMethod.GET)
	public String to_list() {
		return "base/emp/list";
	}
	@RequestMapping("to_add")
	public String to_add() {
		return "base/emp/add";
	}
	@RequestMapping(value = "/to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer em_id,Model model) {
		T_Base_Emp emp = empService.queryByID(em_id);
		if(null != emp){
			model.addAttribute("emp",emp);
		}
		return "base/emp/update";
	}
	/**
	 * 查询员工弹出框页面，支持单选和多选
	 */
	@RequestMapping("to_list_dialog")
	public String to_list_sub_dialog() {
		return "base/emp/list_dialog";
	}
	
	@RequestMapping(value = "tree", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object tree(HttpSession session) {
		Map<String,Object> param = new HashMap<String, Object>();
		T_Sys_User user = getUser(session);
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put(CommonUtil.SHOP_UPCODE, user.getShop_upcode());
		List<T_Base_Department> list = departmentService.list(param);
		return ajaxSuccess(EmpVO.buildTree(list));
	}
	
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object list(HttpServletRequest request,HttpSession session) {
		String page = request.getParameter("page"); // 页码
        String rows = request.getParameter("rows"); // 每页条数
        String sidx = request.getParameter("sidx");
        String sord = request.getParameter("sord");
        String searchContent = request.getParameter("searchContent");
        String dm_code = request.getParameter("dm_code");
        String em_type = request.getParameter("em_type");
        int pageIndex = 1;
        int pageSize = super.pagesize;
        if (org.apache.commons.lang.StringUtils.isNotEmpty(page)) {
        	pageIndex = Integer.parseInt(page);
        	pageIndex = pageIndex == 0 ? 1 : pageIndex;
        }
        if (org.apache.commons.lang.StringUtils.isNotEmpty(rows)) {
        	pageSize = Integer.parseInt(rows);
        }
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());//店铺类型
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());//当前登录用户店铺编号
        param.put(CommonUtil.PAGESIZE, pageSize);
        param.put(CommonUtil.PAGEINDEX, pageIndex);
        param.put(CommonUtil.SIDX, sidx);
        param.put(CommonUtil.SORD, sord);
        param.put("searchContent", StringUtil.decodeString(searchContent));
        param.put("dm_code", dm_code);
        param.put("em_type", em_type);
		PageData<T_Base_Emp> pageData = empService.page(param);
		return ajaxSuccess(pageData, "查询成功, 共" + pageData.getList().size() + "条数据");
	}
	
	@RequestMapping(value = "listCombo", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listCombo(HttpServletRequest request,HttpSession session) {
        String sp_code = request.getParameter("sp_code");
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());//店铺类型
        param.put(CommonUtil.SHOP_CODE, sp_code);//当前登录用户店铺编号
		List<T_Base_Emp> list = empService.listCombo(param);
		return ajaxSuccess(list, "查询成功, 共" + list.size() + "条数据");
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Base_Emp emp,HttpSession session) {
		T_Sys_User user = getUser(session);
		emp.setCompanyid(user.getCompanyid());
		Integer co_ver = user.getCo_ver();//获取当前登录用户店铺类型
		if(CommonUtil.THREE.equals(co_ver) || CommonUtil.FIVE.equals(co_ver)){//如果是自营店或合伙店，把当前用户父级店铺放到登录店铺
			emp.setEm_login_shop(user.getShop_upcode());
		}else{
			emp.setEm_login_shop(user.getUs_shop_code());
		}
		emp.setEm_pass(MD5.encryptMd5(emp.getEm_pass()));
		empService.save(emp);
		return ajaxSuccess(emp);
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Base_Emp emp,HttpSession session) {
		emp.setCompanyid(getCompanyid(session));
		empService.update(emp);
		return ajaxSuccess(emp);
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(@RequestParam Integer em_id) {
		empService.del(em_id);
		return ajaxSuccess();
	}
}
