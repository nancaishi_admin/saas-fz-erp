package zy.controller.base;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.PageData;
import zy.entity.base.bra.T_Base_Bra;
import zy.entity.sys.user.T_Sys_User;
import zy.service.base.bra.BraService;
import zy.util.CommonUtil;
import zy.vo.base.BraVO;

import com.alibaba.fastjson.JSONObject;
@Controller
@RequestMapping("base/bra")
public class BraController extends BaseController{
	@Resource
	private BraService braService;
	
	@RequestMapping(value = "/to_list", method = RequestMethod.GET)
	public String to_list() {
		return "base/bra/list";
	}
	@RequestMapping(value = "/to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer br_id,Model model) {
		T_Base_Bra size = braService.queryByID(br_id);
		if(null != size){
			model.addAttribute("model",size);
		}
		return "base/bra/update";
	}
	
	@RequestMapping("to_add")
	public String to_add() {
		return "base/bra/add";
	}
	
	/**
	 * 查询杯型弹出框页面，支持单选和多选
	 */
	@RequestMapping("to_list_dialog")
	public String to_list_dialog() {
		return "base/bra/list_dialog";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(HttpServletRequest request,HttpSession session) {
		String name = request.getParameter("searchContent");
		String page = request.getParameter("page"); // 页码
        String rows = request.getParameter("rows"); // 每页条数
        String sidx = request.getParameter("sidx");
        String sord = request.getParameter("sord");
        int pageIndex = 1;
        int pageSize = super.pagesize;
        if (org.apache.commons.lang.StringUtils.isNotEmpty(page)) {
        	pageIndex = Integer.parseInt(page);
        	pageIndex = pageIndex == 0 ? 1 : pageIndex;
        }
        if (org.apache.commons.lang.StringUtils.isNotEmpty(rows)) {
        	pageSize = Integer.parseInt(rows);
        }
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put("name", name);
        param.put(CommonUtil.PAGESIZE, pageSize);
        param.put(CommonUtil.PAGEINDEX, pageIndex);
        param.put(CommonUtil.SIDX, sidx);
        param.put(CommonUtil.SORD, sord);
		PageData<T_Base_Bra> pageData = braService.page(param);
		return ajaxSuccess(pageData, "查询成功, 共" + pageData.getList().size() + "条数据");
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Base_Bra bra,HttpServletRequest request,HttpSession session) {
		 T_Sys_User user = getUser(session);
		 bra.setCompanyid(user.getCompanyid());
		 Integer id = braService.queryByName(bra);
		 if(null != id && id > 0){
			return result(EXISTED);
		 }else{
			 braService.save(bra);
			 return ajaxSuccess(bra);
		 }
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Base_Bra bra,HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
		 bra.setCompanyid(user.getCompanyid());
		 Integer id = braService.queryByName(bra);
		 if(null != id && id > 0){
			return result(EXISTED);
		 }else{
			 braService.update(bra);
			 return ajaxSuccess(bra);
		 }
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(@RequestParam Integer br_id,HttpServletRequest request,HttpSession session) {
		 braService.del(br_id);
		 return ajaxSuccess();
	}
	
	/**
	 * 商品资料新增、修改查询杯型信息
	 */
	@RequestMapping(value = "braListByProduct", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public void braListByProduct(HttpServletRequest request,HttpSession session,HttpServletResponse response) {
		JSONObject jsonObject = new JSONObject();
		PrintWriter out=null;
		try {
	        T_Sys_User user = getUser(session);
	        Map<String,Object> param = new HashMap<String, Object>();
	        param.put(CommonUtil.COMPANYID, user.getCompanyid());
	        List<T_Base_Bra> list = braService.braList(param);
	        out = response.getWriter();
	        if(list != null&&list.size()>0){
				jsonObject.put("bra_html",BraVO.toBraHtmlByList(list));
			}else{
				jsonObject.put("bra_html","");
			}
			out.print(jsonObject);
			out.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			if(out != null)out.close();
		}
	}
}
