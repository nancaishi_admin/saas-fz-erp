package zy.controller.base;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.PageData;
import zy.entity.base.shop.T_Base_Shop;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.base.shop.ShopService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;
import zy.vo.base.ShopVO;

@Controller
@RequestMapping("base/shop")
public class ShopController extends BaseController{
	
	@Resource
	private ShopService shopService;

	@RequestMapping("to_all")
	public String to_all() {
		return "base/shop/all";
	}
	@RequestMapping("to_tree")
	public String to_tree() {
		return "base/shop/tree";
	}
	@RequestMapping(value = "/to_list", method = RequestMethod.GET)
	public String to_list() {
		return "base/shop/list";
	}
	@RequestMapping(value = "to_money", method = RequestMethod.GET)
	public String to_money() {
		return "base/shop/money";
	}
	@RequestMapping(value = "to_money_details", method = RequestMethod.GET)
	public String to_money_details() {
		return "base/shop/money_details";
	}
	@RequestMapping(value = "/to_zone", method = RequestMethod.GET)
	public String to_listZone() {
		return "base/shop/list_zone";
	}
	
	@RequestMapping(value = "/to_add_zone", method = RequestMethod.GET)
	public String to_add_zone() {
		return "base/shop/add_zone";
	}
	
	@RequestMapping(value = "/to_add", method = RequestMethod.GET)
	public String to_add() {
		return "base/shop/add";
	}
	
	@RequestMapping(value = "/to_update", method = RequestMethod.GET)
	public String to_update(Integer sp_id,Model model) {
		T_Base_Shop shop = shopService.queryByID(sp_id);
		model.addAttribute("model", shop);
		return "base/shop/update";
	}
	
	@RequestMapping(value = "/to_update_zone", method = RequestMethod.GET)
	public String to_update_zone(Integer sp_id,Model model) {
		T_Base_Shop shop = shopService.queryByID(sp_id);
		model.addAttribute("model", shop);
		return "base/shop/update_zone";
	}
	
	/**
	 * 查询下级店铺弹出框页面，支持单选和多选
	 */
	@RequestMapping("to_list_sub_dialog")
	public String to_list_sub_dialog() {
		return "base/shop/list_sub_dialog";
	}
	
	/**
	 * 列表单选
	 */
	@RequestMapping("to_sin_list_dialog")
	public String to_sin_list_dialog() {
		return "base/shop/sin_list_dialog";
	}
	/**
	 * 单选树
	 */
	@RequestMapping("to_sin_select_tree")
	public String to_sin_select_tree() {
		return "base/shop/sin_select_tree";
	}
	
	/**
	 * 商品调价单店铺查询，支持单选和多选
	 */
	@RequestMapping("to_list_price_dialog")
	public String to_list_price_dialog(Model model, HttpSession session) {
		T_Sys_User user = getUser(session);
		model.addAttribute("shop_type",user.getShoptype());
		return "base/shop/list_price_dialog";
	}
	
	/**
	 * 店铺查询弹出框，按照功能模块
	 */
	@RequestMapping("list_dialog_bymodule")
	public String list_dialog_bymodule() {
		return "base/shop/list_dialog_bymodule";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object page(PageForm pageForm,HttpServletRequest request, HttpSession session) {
		Object ty_id = request.getParameter("ty_id");
       
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put("name", StringUtil.decodeString(pageForm.getSearchContent()));
        param.put("ty_id", ty_id);
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
		PageData<T_Base_Shop> pageData = shopService.page(param);
		return ajaxSuccess(pageData, "查询成功, 共" + pageData.getList().size() + "条数据");
	}
	@RequestMapping(value = "pageZone", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object pageZone(PageForm pageForm,HttpServletRequest request, HttpSession session) {
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.SHOP_TYPE, CommonUtil.TWO);
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put("name", StringUtil.getSpell(StringUtil.decodeString(pageForm.getSearchContent())));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
		PageData<T_Base_Shop> pageData = shopService.pageZone(param);
		return ajaxSuccess(pageData, "查询成功, 共" + pageData.getList().size() + "条数据");
	}
	
	@RequestMapping(value = "all_list", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object all_list(String shop_type,String searchContent, HttpSession session) {
		Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put("shop_type", shop_type);
        param.put("searchContent", StringUtil.decodeString(searchContent));
		return ajaxSuccess(shopService.all_list(param));
	}
	
	@RequestMapping(value = "tree", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object tree(String shop_type,HttpSession session) {
		Map<String,Object> param = new HashMap<String, Object>();
		param.put("shop_type", shop_type);
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        List<T_Base_Shop> list = shopService.all_list(param);
		return ajaxSuccess(ShopVO.buildTreeAll(list));
	}
	
	/**
	 * 总公司和分公司查询下级店铺(自营店、合伙店)，否则查询自己店铺
	 */
	@RequestMapping(value = "sub_list", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object sub_list(String searchContent, HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put("searchContent", StringUtil.decodeString(searchContent));
		return ajaxSuccess(shopService.sub_list(param));
	}
	/**
	 * 总公司和分公司查询下级加盟店铺
	 */
	@RequestMapping(value = "sub_list_jmd", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object sub_list_jmd(String searchContent, HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put("searchContent", StringUtil.decodeString(searchContent));
		return ajaxSuccess(shopService.sub_list_jmd(param));
	}
	/**
	 * 自己选择的店铺
	 */
	@RequestMapping(value = "selectList", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object selectList(String searchContent, 
			String sp_code,
			HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put("searchContent", StringUtil.decodeString(searchContent));
        param.put("sp_code", sp_code);
		return ajaxSuccess(shopService.selectList(param));
	}
	
	/**
	 * 商品调价单店铺查询 总公司和分公司查询下级店铺(自营店、合伙店)，否则查询自己店铺
	 */
	@RequestMapping(value = "price_list", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object price_list(String searchContent,String sp_shop_type, HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put("searchContent", StringUtil.decodeString(searchContent));
        param.put("sp_shop_type", sp_shop_type);
		return ajaxSuccess(shopService.price_list(param));
	}
	
	@RequestMapping(value = "want_list", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object want_list(String searchContent, HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put("searchContent", StringUtil.decodeString(searchContent));
		return ajaxSuccess(shopService.want_list(param));
	}
	
	@RequestMapping(value = "allot_list", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object allot_list(String searchContent, HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put("searchContent", StringUtil.decodeString(searchContent));
		return ajaxSuccess(shopService.allot_list(param));
	}
	
	@RequestMapping(value = "money_list", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object money_list(String searchContent, HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put("searchContent", StringUtil.decodeString(searchContent));
		return ajaxSuccess(shopService.money_list(param));
	}
	
	/**
	 * 查上级和下级的店铺
	 * */
	@RequestMapping(value = "up_sub_list", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object up_sub_list(String searchContent, HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put("searchContent", StringUtil.decodeString(searchContent));
		return ajaxSuccess(shopService.up_sub_list(param));
	}
	
	@RequestMapping(value = "sub_tree", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object sub_tree(String searchContent,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put("searchContent", StringUtil.decodeString(searchContent));
		return ajaxSuccess(ShopVO.buildTreeAll(shopService.sub_tree(param)));
	}
	
	/**
	 * 员工新增、修改查询店铺信息
	 * @param request
	 * @param session
	 * @param response
	 */
	@RequestMapping(value = "listByEmp", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public void listByEmp(HttpServletRequest request,HttpSession session,HttpServletResponse response) {
		try {
			PrintWriter out=null;
			T_Sys_User user = getUser(session);
			Map<String,Object> param = new HashMap<String, Object>();
			param.put(CommonUtil.COMPANYID, user.getCompanyid());
	        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
	        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
			String sp_list="{\"data\":{\"items\":[";
			List<T_Base_Shop> list = shopService.listByEmp(param);
			for (int i=0;i<list.size();i++){
				sp_list+="{\"em_shop_code\":\""+list.get(i).getSp_code()+"\",\"em_shop_name\":\""+list.get(i).getSp_name()+"\"},";
			}
			sp_list=sp_list.substring(0,sp_list.length()-1);
			sp_list+="]}}";
			out = response.getWriter();
			out.print(sp_list);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Base_Shop model,HttpServletRequest request,HttpSession session) {
		 T_Sys_User user = getUser(session);
		 model.setCompanyid(user.getCompanyid());
		 model.setSp_upcode(user.getUs_shop_code());//上级店铺编号
		 model.setSp_spell(StringUtil.getSpell(model.getSp_name()));
		 model.setSp_init(0);//初始化默认不启用
		 model.setSp_state(0);//状态默认正常
		 model.setSp_lock(0);//加密狗默认不启用
		 model.setSp_end(DateUtil.getDateAddDays(15));//有效期
		 shopService.save(model);
		 return ajaxSuccess();
	}
	@RequestMapping(value = "saveZone", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object saveZone(T_Base_Shop model,HttpServletRequest request,HttpSession session) {
		 T_Sys_User user = getUser(session);
		 model.setCompanyid(user.getCompanyid());
		 model.setSp_upcode(user.getUs_shop_code());//上级店铺编号
		 model.setSp_spell(StringUtil.getSpell(model.getSp_name()));
		 model.setSp_init(0);//初始化默认不启用
		 model.setSp_state(0);//状态默认正常
		 model.setSp_lock(0);//加密狗默认不启用
		 model.setSp_end(DateUtil.getDateAddDays(15));//有效期
		 shopService.saveZone(model);
		 return ajaxSuccess();
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Base_Shop model,HttpServletRequest request,HttpSession session) {
		model.setSp_spell(StringUtil.getSpell(model.getSp_name()));
		T_Sys_User user = getUser(session);
		if (user.getSp_init() == 1) {// 启用账套，则期初欠款不能修改
			model.setSp_init_debt(null);
		}
		shopService.update(model);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "updateState", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object updateState(T_Base_Shop model,HttpServletRequest request,HttpSession session) {
		shopService.updateState(model);
		return ajaxSuccess();
	}

	@RequestMapping(value = "pageMoneyDetails", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object pageMoneyDetails(PageForm pageForm,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String type,
			@RequestParam(required = false) String pay_state,
			@RequestParam(required = false) String shopcode,
			@RequestParam(required = false) String manager,
			@RequestParam(required = false) String number,
			HttpSession session) {
        Map<String,Object> params = new HashMap<String, Object>();
        T_Sys_User user = getUser(session);
        if(CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype())){//总公司、分公司
        	params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		}else{//自营、加盟、合伙
			params.put(CommonUtil.SHOP_CODE, user.getShop_upcode());
		}
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put(CommonUtil.PAGESIZE, pageForm.getRows());
        params.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        params.put("begindate", begindate);
        params.put("enddate", enddate);
        params.put("type", type);
        params.put("pay_state", pay_state);
        params.put("shopcode", shopcode);
        params.put("manager", StringUtil.decodeString(manager));
        params.put("number", StringUtil.decodeString(number));
		return ajaxSuccess(shopService.pageMoneyDetails(params));
	}
	
}
