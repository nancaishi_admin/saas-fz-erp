package zy.controller.vip;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jxl.Workbook;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import zy.controller.BaseController;
import zy.entity.sell.cash.T_Sell_Shop;
import zy.entity.sys.improtinfo.T_Import_Info;
import zy.entity.sys.user.T_Sys_User;
import zy.entity.vip.member.T_Vip_Member;
import zy.entity.vip.member.T_Vip_Member_Info;
import zy.entity.vip.set.T_Vip_BirthdaySms;
import zy.entity.vip.set.T_Vip_Setup;
import zy.entity.vip.visit.T_Vip_Visit;
import zy.form.PageForm;
import zy.service.vip.member.MemberService;
import zy.service.vip.set.VipSetService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.ExcelImportUtil;
import zy.util.NumberUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("vip/member")
public class MemberController extends BaseController{
	
	@Resource
	private MemberService memberService;
	
	@Resource
	private VipSetService vipSetService;
	
	@RequestMapping(value = "/to_list", method = RequestMethod.GET)
	public String to_list(HttpSession session,Model model) {
		T_Sys_User user = getUser(session);
		Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, user.getCompanyid());
        params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        params.put(CommonUtil.SHOP_UPCODE, user.getShop_upcode());
        params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
		 //会员系统参数设置
        int lossDay = 180;//会员流失天数
		int consumeDay = 90;//活跃客户消费天数
		int loyalVipTimes = 3;//忠诚客户消费次数
		double richVipMoney = 1000;//高价客户消费金额
        T_Vip_Setup setup = vipSetService.loadSetUp(params);
        if(setup != null){
        	lossDay = setup.getVs_loss_day();
        	consumeDay = setup.getVs_consume_day();
        	loyalVipTimes= setup.getVs_loyalvip_times();
        	richVipMoney = setup.getVs_richvip_money();
        }
        model.addAttribute("lossDay", lossDay);
        model.addAttribute("consumeDay", consumeDay);
        model.addAttribute("loyalVipTimes", loyalVipTimes);
        model.addAttribute("richVipMoney", richVipMoney);
		return "vip/member/list";
	}
	
	@RequestMapping(value = "/to_dialog", method = RequestMethod.GET)
	public String to_dialog() {
		return "vip/member/list_dialog";
	}
	@RequestMapping(value = "/to_report", method = RequestMethod.GET)
	public String to_report(Integer vm_id,Model model) {
		T_Vip_Member member = memberService.queryByID(vm_id);
		if(null != member){
			T_Vip_Member_Info memberinfo = memberService.queryInfoByCode(member.getVm_code(), member.getCompanyid());
			model.addAttribute("member",member);
			model.addAttribute("memberinfo",memberinfo);
		}
		return "vip/member/vip_report";
	}
	@RequestMapping(value = "/to_add", method = RequestMethod.GET)
	public String to_add() {
		return "vip/member/add";
	}
	@RequestMapping(value = "/to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer vm_id,Model model) {
		T_Vip_Member member = memberService.queryByID(vm_id);
		if(null != member){
			T_Vip_Member_Info memberinfo = memberService.queryInfoByCode(member.getVm_code(), member.getCompanyid());
			model.addAttribute("member",member);
			model.addAttribute("memberinfo",memberinfo);
		}
		return "vip/member/update";
	}
	
	@RequestMapping(value = "/to_assign_list", method = RequestMethod.GET)
	public String to_assign_list() {
		return "vip/member/assign_list";
	}
	
	@RequestMapping(value = "/to_set_member_tag", method = RequestMethod.GET)
	public String to_set_member_tag(String vm_code,String vm_name,String vm_cardcode,Model model) {
		model.addAttribute("vm_code", vm_code);
		model.addAttribute("vm_name", StringUtil.decodeString(vm_name));
		model.addAttribute("vm_cardcode", vm_cardcode);
		return "vip/member/set_member_tag";
	}
	
	@RequestMapping(value = "/to_add_vip_tag", method = RequestMethod.GET)
	public String to_add_vip_tag(String vm_code,Model model) {
		model.addAttribute("vm_code", vm_code);
		return "vip/member/add_vip_tag";
	}
	
	
	/**
	 * 分配办卡人员，查询员工弹出框页面
	 */
	@RequestMapping("to_assign_emp")
	public String to_list_sub_dialog() {
		return "vip/member/assign_emp";
	}
	
	@RequestMapping("to_import_member")
	public String to_import_member() {
		return "vip/member/import_member";
	}
	
	@RequestMapping(value = "/to_birthday_list", method = RequestMethod.GET)
	public String to_birthday_list(HttpSession session,Model model) {
		T_Sys_User user = getUser(session);
		Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, user.getCompanyid());
        params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        params.put(CommonUtil.SHOP_UPCODE, user.getShop_upcode());
        params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        
		int consumeDay = 90;//会员默认消费周期
		int agowarnDay = 0;//短信提前提醒天数
		T_Vip_Setup setup = vipSetService.loadSetUp(params);
		if(setup != null){
        	consumeDay = setup.getVs_consume_day();
        }
		T_Vip_BirthdaySms birthdaySms = vipSetService.loadBirthdaySms(params);
		if(birthdaySms != null){
			agowarnDay = birthdaySms.getVb_agowarn_day();
		}
		model.addAttribute("consumeDay", consumeDay);
		model.addAttribute("agowarnDay", agowarnDay);
		return "vip/member/birthday_list";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String vm_mt_code,
			@RequestParam(required = false) String vm_shop_code,
			@RequestParam(required = false) String vm_manager_code,
			@RequestParam(required = false) String vm_cardcode,
			@RequestParam(required = false) String vm_name,
			@RequestParam(required = false) String vm_mobile,
			@RequestParam(required = false) String vm_tag_name,
			@RequestParam(required = false) String begin_total_money,
			@RequestParam(required = false) String end_total_money,
			@RequestParam(required = false) String begin_last_points,
			@RequestParam(required = false) String end_last_points,
			@RequestParam(required = false) String begin_notbuy_day,
			@RequestParam(required = false) String end_notbuy_day,
			@RequestParam(required = false) String select_member_type,
			HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        bulidParam(param, user);
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("vm_mt_code", vm_mt_code);
        param.put("vm_shop_code", vm_shop_code);
        param.put("vm_manager_code", vm_manager_code);
        param.put("vm_cardcode", StringUtil.decodeString(vm_cardcode));
        param.put("vm_name", StringUtil.decodeString(vm_name));
        param.put("vm_mobile", StringUtil.decodeString(vm_mobile));
        param.put("vm_tag_name", StringUtil.decodeString(vm_tag_name));
        param.put("begin_total_money", begin_total_money);
        param.put("end_total_money", end_total_money);
        param.put("begin_last_points", begin_last_points);
        param.put("end_last_points", end_last_points);
        param.put("begin_notbuy_day", begin_notbuy_day);
        param.put("end_notbuy_day", end_notbuy_day);
        param.put("select_member_type", select_member_type);
        
        String lossDay = request.getParameter("lossDay");
        String consumeDay = request.getParameter("consumeDay");
        String loyalVipTimes = request.getParameter("loyalVipTimes");
        String richVipMoney = request.getParameter("richVipMoney");
        param.put("lossDay", lossDay);
        param.put("consumeDay", consumeDay);
        param.put("loyalVipTimes", loyalVipTimes);
        param.put("richVipMoney", richVipMoney);
        
		return ajaxSuccess(memberService.page(param));
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Vip_Member member,T_Vip_Member_Info memberInfo,HttpSession session) {
		member.setCompanyid(getCompanyid(session));
		memberInfo.setCompanyid(getCompanyid(session));
		memberService.save(member, memberInfo, getUser(session));
		return ajaxSuccess(member);
	}
	
	@RequestMapping(value = "save_import_member", method = RequestMethod.POST,produces = "text/html; charset=utf-8")
	@ResponseBody
	public Object save_import_member(MultipartFile excel,HttpServletRequest request,HttpSession session)throws IOException {
		if (excel == null || excel.getSize() <= 0) {
			return ajaxFail("请选择需要导入的文件！"); 
		}
		Map<String,Object> param = new HashMap<String, Object>();
		Workbook sourceWorkBook = null;
		Workbook wb_error = null;
		WritableWorkbook wb = null;
		
		try {
			T_Sys_User user = getUser(session);
			param.put(CommonUtil.COMPANYID, user.getCompanyid());
			param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
			param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
			param.put("us_id", user.getUs_id());
			//根据商家编号、用户编号及当前时间生成文件名
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
			String fileName = "member_" + user.getCompanyid() + (user.getUs_id() + format.format(new Date())) + ".xls";
			
			// 复制客户端文件到服务器临时文件夹进行处理,处理完以后删除
			String temp_uploaddir = CommonUtil.CHECK_BASE + CommonUtil.EXCEL_PATH;
			String toFilePath = temp_uploaddir + File.separator + fileName;
			File savefile = new File(toFilePath);
			if (!savefile.getParentFile().exists()) {
				savefile.getParentFile().mkdirs();
			}
			excel.transferTo(savefile);
			
			// Excel 模板文件路径，把模板文件复制，然后作为错误文件进行数据追加
			String relPath = session.getServletContext().getRealPath("resources");
			String excel_member_template = relPath + File.separator + "excel" + File.separator + CommonUtil.MEMBERLEADING;
			
			// 错误文件名称
			String errFileName = "fail" + fileName;
			String errorFilePath = temp_uploaddir + File.separator + errFileName;
			File excel_template = new File(excel_member_template);// 原有Excel 模板文件
			File errorFile = new File(errorFilePath);// 生成的错误文件路径
			// 把模板文件复制给错误文件
			FileUtils.copyFile(excel_template, errorFile);
			
			sourceWorkBook = Workbook.getWorkbook(savefile);// 要导入的Excel
			wb_error = Workbook.getWorkbook(errorFile);
			wb = Workbook.createWorkbook(errorFile, wb_error);
			Map<String, Object> result = memberService.parseSavingCardInfoExcel(sourceWorkBook, wb, param);
			wb.write();
			
			ExcelImportUtil.deleteFile(savefile);
			int success = (Integer)(result.get("success"));// 导入成功条数
			int fail = (Integer)(result.get("fail"));// 导入失败条数
			param.put("memberList", (List<T_Vip_Member>)result.get("memberList"));//导入成功的会员信息
			param.put("memberInfoMap", (Map<String, T_Vip_Member_Info>)result.get("memberInfoMap"));//导入成功会员其他信息
			
			T_Import_Info t_Import_Info = new T_Import_Info();
			t_Import_Info.setIi_success(success);
			t_Import_Info.setIi_fail(fail);
			t_Import_Info.setIi_total(success+fail);
			t_Import_Info.setIi_filename(excel.getOriginalFilename());
			t_Import_Info.setIi_error_filename(errFileName);
			t_Import_Info.setIi_us_id(user.getUs_id());
			t_Import_Info.setIi_sysdate(DateUtil.getCurrentTime());
			t_Import_Info.setIi_type(CommonUtil.IMPROT_MEMBERTYPE);
			t_Import_Info.setCompanyid(user.getCompanyid());
			param.put("t_Import_Info", t_Import_Info);
			param.put("ii_type", CommonUtil.IMPROT_MEMBERTYPE);
			
			memberService.saveImport(param);
		}  catch (Exception e){
			e.printStackTrace();
			return ajaxFail("导入失败，请联系管理员！");
		} finally {
			if (sourceWorkBook != null)
				sourceWorkBook.close();
			if (wb_error != null)
				wb_error.close();
			try {
				if (wb != null)
					wb.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Vip_Member member,T_Vip_Member_Info memberInfo,HttpSession session) {
		member.setCompanyid(getCompanyid(session));
		memberInfo.setCompanyid(getCompanyid(session));
		memberService.update(member, memberInfo, getUser(session));
		return ajaxSuccess(member);
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(@RequestParam Integer vm_id,@RequestParam String vm_code,HttpSession session) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(CommonUtil.COMPANYID, getCompanyid(session));
		params.put("vm_id", vm_id);
		params.put("vm_code", vm_code);
		memberService.del(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "assign_page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object assign_page(PageForm pageForm,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String vm_mt_code,
			@RequestParam(required = false) String vm_shop_code,
			@RequestParam(required = false) String vm_manager_code,
			@RequestParam(required = false) String vm_cardcode,
			@RequestParam(required = false) String vm_name,
			@RequestParam(required = false) String vm_mobile,
			@RequestParam(required = false) String vm_empty_manager,
			HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("vm_mt_code", vm_mt_code);
        param.put("vm_shop_code", vm_shop_code);
        param.put("vm_manager_code", vm_manager_code);
        param.put("vm_cardcode", StringUtil.decodeString(vm_cardcode));
        param.put("vm_name", StringUtil.decodeString(vm_name));
        param.put("vm_mobile", StringUtil.decodeString(vm_mobile));
        param.put("vm_empty_manager", StringUtil.decodeString(vm_empty_manager));
		return ajaxSuccess(memberService.assign_page(param));
	}
	
	@RequestMapping(value = "update_assign_emp", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update_assign_emp(@RequestParam String rowIds,@RequestParam String codes,HttpSession session) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(CommonUtil.COMPANYID, getCompanyid(session));
		params.put("rowIds", rowIds);
		params.put("codes", StringUtil.decodeString(codes));
		memberService.update_assign_emp(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "/vip_visit_analysis", method = RequestMethod.GET)
	public String vip_visit_analysis(HttpSession session,Model model,
			@RequestParam(required = false) Integer vi_type,
			@RequestParam(required = false) Integer vm_id,
			@RequestParam(required = false) String vm_cardcode) {
		T_Sys_User user = getUser(session);
		Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, user.getCompanyid());
        T_Vip_Member member = memberService.queryByID(vm_id);
        model.addAttribute("vi_type", vi_type);
        model.addAttribute("member", member);
        String nowDate = DateUtil.getYearMonthDate();
        model.addAttribute("nowDate", nowDate);
		return "vip/member/visit_analysis";
	}
	
	@RequestMapping(value = "save_visit", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save_visit(T_Vip_Visit visit,HttpSession session) {
		T_Sys_User user = getUser(session);
		visit.setCompanyid(user.getCompanyid());
		visit.setVi_us_id(user.getUs_id());
		visit.setVi_sysdate(DateUtil.getCurrentTime());
		visit.setVi_vm_name(StringUtil.decodeString(visit.getVi_vm_name()));
		visit.setVi_manager(StringUtil.decodeString(visit.getVi_manager()));
		visit.setVi_content(StringUtil.decodeString(visit.getVi_content()));
		visit.setVi_remark(StringUtil.decodeString(visit.getVi_remark()));
		memberService.save_visit(visit);
		return ajaxSuccess();
	}
	
	
	@RequestMapping(value = "sellList", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object sellList(PageForm pageForm,String vm_code,
			String begindate,String enddate,
			HttpServletRequest request,HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        bulidParam(param, getUser(session));
        param.put("begindate", begindate);
    	param.put("enddate", enddate);
        param.put("vm_code", vm_code);
		return ajaxSuccess(memberService.sellList(param));
	}
	@RequestMapping(value = "shopList", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object shopList(String vm_code,
			String begindate,String enddate,
			HttpServletRequest request,HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        bulidParam(param, getUser(session));
        param.put("begindate", begindate);
    	param.put("enddate", enddate);
        param.put("vm_code", vm_code);
		return ajaxSuccess(memberService.shopList(param));
	}
	@RequestMapping(value = "tryList", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object tryList(String vm_code,
			String begindate,String enddate,
			HttpServletRequest request,HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        bulidParam(param, getUser(session));
        param.put("begindate", begindate);
    	param.put("enddate", enddate);
        param.put("vm_code", vm_code);
		return ajaxSuccess(memberService.tryList(param));
	}
	@RequestMapping(value = "ecouponList", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object ecouponList(String vm_code,String ecu_state,
			HttpServletRequest request,HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        bulidParam(param, getUser(session));
    	param.put("ecu_state",ecu_state);
        param.put("vm_code", vm_code);
		return ajaxSuccess(memberService.ecouponList(param));
	}
	@RequestMapping(value = "backList", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object backList(String vm_code,
			String begindate,String enddate,
			HttpServletRequest request,HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        bulidParam(param, getUser(session));
        param.put("begindate", begindate);
    	param.put("enddate", enddate);
        param.put("vm_code", vm_code);
		return ajaxSuccess(memberService.backList(param));
	}
	
	@RequestMapping(value = "timeList", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object timeList(String vm_code,
			String begindate,String enddate,
			HttpServletRequest request,HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        bulidParam(param, getUser(session));
    	param.put("begindate", begindate);
    	param.put("enddate", enddate);
        param.put("vm_code", vm_code);
		return ajaxSuccess(memberService.timeList(param));
	}
	
	@RequestMapping(value = "pushList", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object pushList(String vm_code,
			HttpServletRequest request,HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        bulidParam(param, getUser(session));
        param.put("vm_code", vm_code);
		return ajaxSuccess(memberService.pushList(param));
	}
	
	@RequestMapping(value = "query_vip_tag", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object query_vip_tag(String vm_code,HttpServletRequest request,HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        bulidParam(param, getUser(session));
        param.put("vm_code", vm_code);
		return ajaxSuccess(memberService.query_vip_tag(param));
	}
	
	@RequestMapping(value = "del_vip_tag", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del_vip_tag(String vt_code,String del_type,String vm_code,HttpSession session) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(CommonUtil.COMPANYID, getCompanyid(session));
		params.put("vt_code", vt_code);
		params.put("del_type", del_type);
		params.put("vm_code", vm_code);
		memberService.del_vip_tag(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "set_member_tag", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object set_member_tag(String vt_code,String vm_code,String vt_name,HttpSession session) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(CommonUtil.COMPANYID, getCompanyid(session));
		params.put("vt_code", vt_code);
		params.put("vt_name", vt_name);
		params.put("vm_code", vm_code);
		memberService.set_member_tag(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "save_vip_tag", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save_vip_tag(String vt_name,String vm_code,HttpSession session) {
		Map<String, Object> params = new HashMap<String, Object>();
		T_Sys_User user = getUser(session);
		bulidParam(params, user);
		params.put("us_code", user.getUs_code());
		params.put("vt_name", StringUtil.decodeString(vt_name));
		params.put("vm_code", vm_code);
		memberService.save_vip_tag(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "birthday_list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object birthday_list(PageForm pageForm,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String vm_shop_code,
			@RequestParam(required = false) String vm_cardcode,
			@RequestParam(required = false) String vm_name,
			@RequestParam(required = false) String vm_mobile,
			@RequestParam(required = false) String begin_notbuy_day,
			@RequestParam(required = false) String end_notbuy_day,
			@RequestParam(required = false) String consumeDay,
			@RequestParam(required = false) String agowarnDay,
			@RequestParam(required = false) String isHadBirthday,
			@RequestParam(required = false) String CurrentMode,
			@RequestParam(required = false) String vipUpdatePower,
			HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        bulidParam(param, user);
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("vm_shop_code", vm_shop_code);
        param.put("vm_cardcode", vm_cardcode);
        param.put("vm_name", StringUtil.decodeString(vm_name));
        param.put("vm_mobile", vm_mobile);
        param.put("begin_notbuy_day", begin_notbuy_day);
        param.put("end_notbuy_day", end_notbuy_day);
        param.put("consumeDay", consumeDay);
        param.put("agowarnDay", agowarnDay);
        param.put("isHadBirthday", isHadBirthday);
        param.put("CurrentMode", CurrentMode);
        param.put("vipUpdatePower", vipUpdatePower);
		return ajaxSuccess(memberService.birthday_list(param));
	}
}
