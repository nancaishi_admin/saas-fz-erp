package zy.controller.vip;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sys.user.T_Sys_User;
import zy.entity.vip.set.T_Vip_ReturnSetUp;
import zy.form.PageForm;
import zy.service.vip.set.VipSetService;
import zy.service.vip.visit.VisitService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("vip/visit")
public class VisitPlanController extends BaseController{
	
	@Resource
	private VipSetService vipSetService;
	@Resource
	private VisitService visitService;
	
	@RequestMapping(value = "/to_list", method = RequestMethod.GET)
	public String to_list(HttpSession session,Model model) {
		T_Sys_User user = getUser(session);
		Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, user.getCompanyid());
        params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        params.put(CommonUtil.SHOP_UPCODE, user.getShop_upcode());
        params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        List<T_Vip_ReturnSetUp> returnSetUps = vipSetService.loadReturnSetUp(params);
        int rts_day = 0;
        if(returnSetUps != null){
        	rts_day = returnSetUps.get(0).getRts_day();
        }else {
        	returnSetUps = new ArrayList<T_Vip_ReturnSetUp>();
		}
        model.addAttribute("rts_day", rts_day);
        model.addAttribute("returnSetUps", returnSetUps);
		return "vip/visit/list";
	}
	
	@RequestMapping(value = "/to_detail", method = RequestMethod.GET)
	public String to_detail() {
		return "vip/visit/detail";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = false) String card_manager_code,
			@RequestParam(required = false) Double begin_buy_money,
			@RequestParam(required = false) Double end_buy_money,
			@RequestParam(required = false) String card_code,
			@RequestParam(required = false) Integer rts_day,
			@RequestParam(required = false) String visit,
			@RequestParam(required = false) String card_shop_code,
			HttpServletRequest request,HttpSession session) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -rts_day);
		String sell_date = DateUtil.format(cal, "yyyy-MM-dd");
		
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put("card_manager_code", card_manager_code);
        param.put("begin_buy_money", begin_buy_money);
        param.put("end_buy_money", end_buy_money);
        param.put("card_code", StringUtil.decodeString(card_code));
        param.put("sell_date", sell_date);
        param.put("visit", visit);
        param.put("card_shop_code", card_shop_code);
		return ajaxSuccess(visitService.page(param));
	}
	
	@RequestMapping(value = "detail_page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object detail_page(PageForm pageForm,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String vm_cardcode,
			@RequestParam(required = false) String vm_mobile,
			@RequestParam(required = false) String vi_shop_code,
			@RequestParam(required = false) String vi_manager_code,
			@RequestParam(required = false) String vi_way,
			@RequestParam(required = false) String vi_is_arrive,
			HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        if( StringUtil.isEmpty(begindate) ){
			begindate = DateUtil.format(DateUtil.getMonthStart(new Date()), "yyyy-MM-dd");
		}
		if( StringUtil.isEmpty(enddate) ){
			enddate = DateUtil.format(DateUtil.getMonthEnd(new Date()), "yyyy-MM-dd");
		}
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("vm_cardcode", StringUtil.decodeString(vm_cardcode));
        param.put("vm_mobile", StringUtil.decodeString(vm_mobile));
        param.put("vi_shop_code", vi_shop_code);
        param.put("vi_manager_code", vi_manager_code);
        param.put("vi_way", vi_way);
        param.put("vi_is_arrive", vi_is_arrive);
		return ajaxSuccess(visitService.detail_page(param));
	}
}
