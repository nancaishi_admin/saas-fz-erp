package zy.controller.vip;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sys.user.T_Sys_User;
import zy.entity.vip.set.T_Vip_Setup;
import zy.form.PageForm;
import zy.service.vip.clear.VipPointsClearService;
import zy.service.vip.set.VipSetService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;


@Controller
@RequestMapping("vip/clear")
public class VipPointsClearController extends BaseController{
	
	@Resource
	private VipSetService vipSetService;
	
	@Resource
	private VipPointsClearService vipPointsClearService;
	
	@RequestMapping(value = "/to_list", method = RequestMethod.GET)
	public String to_list(HttpSession session,Model model) {
		T_Sys_User user = getUser(session);
		Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, user.getCompanyid());
        params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        params.put(CommonUtil.SHOP_UPCODE, user.getShop_upcode());
        params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        
		int consumeDay = 90;//会员默认消费周期
		T_Vip_Setup setup = vipSetService.loadSetUp(params);
		if(setup != null){
        	consumeDay = setup.getVs_consume_day();
        }
		model.addAttribute("consumeDay", consumeDay);
		return "vip/clear/list";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String vm_shop_code,
			@RequestParam(required = false) String vm_mt_code,
			@RequestParam(required = false) String gd_name,
			@RequestParam(required = false) String vm_manager_code,
			@RequestParam(required = false) String vm_cardcode,
			@RequestParam(required = false) String begin_last_points,
			@RequestParam(required = false) String end_last_points,
			@RequestParam(required = false) String consumeDay,
			HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("vm_mt_code", vm_mt_code);
        param.put("vm_shop_code", vm_shop_code);
        param.put("gd_name", StringUtil.decodeString(gd_name));
        param.put("vm_manager_code", vm_manager_code);
        param.put("vm_cardcode", StringUtil.decodeString(vm_cardcode));
        param.put("begin_last_points", StringUtil.decodeString(begin_last_points));
        param.put("end_last_points", StringUtil.decodeString(end_last_points));
        if(StringUtil.isEmpty(consumeDay)){
			consumeDay = "90";
		}
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -Integer.parseInt(consumeDay));
		String runOffDate = DateUtil.getYearMonthDate(cal.getTime());
        param.put("runOffDate", runOffDate);
		return ajaxSuccess(vipPointsClearService.page(param));
	}
	
	@RequestMapping(value = "clear_points", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object clear_points(HttpServletRequest request,HttpSession session,
			@RequestParam(required = false) String ids) {
		T_Sys_User user = getUser(session);
		Map<String,Object> params = new HashMap<String, Object>();
	    params.put(CommonUtil.COMPANYID, user.getCompanyid());
	    params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
	    params.put(CommonUtil.SHOP_NAME, user.getShop_name());
	    params.put("us_name", user.getUs_name());
	    params.put("us_id", user.getUs_id());
	    params.put("ids", ids);
	    vipPointsClearService.clear_points(params);
		return ajaxSuccess();
	}
}
