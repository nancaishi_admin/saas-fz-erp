package zy.controller.vip;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sys.user.T_Sys_User;
import zy.entity.vip.membertype.T_Vip_MemberType;
import zy.service.vip.membertype.MemberTypeService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("vip/membertype")
public class MemberTypeController extends BaseController{
	
	@Resource
	private MemberTypeService memberTypeService;

	@RequestMapping(value = "/to_list", method = RequestMethod.GET)
	public String to_list() {
		return "vip/membertype/list";
	}
	@RequestMapping(value = "/to_add", method = RequestMethod.GET)
	public String to_add() {
		return "vip/membertype/add";
	}
	@RequestMapping(value = "/to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer mt_id,Model model) {
		T_Vip_MemberType membertype = memberTypeService.queryByID(mt_id);
		if(null != membertype){
			model.addAttribute("membertype",membertype);
		}
		return "vip/membertype/update";
	}
	/**
	 * 列表弹出框选择
	 */
	@RequestMapping("to_list_dialog")
	public String to_list_dialog() {
		return "vip/membertype/list_dialog";
	}
	
	/**
	 * 促销方案列表弹出框选择
	 */
	@RequestMapping("to_list_dialog_sale")
	public String to_list_dialog_sale() {
		return "vip/membertype/list_dialog_sale";
	}
	
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object list(String searchContent, HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put("searchContent", StringUtil.decodeString(searchContent));
		return ajaxSuccess(memberTypeService.list(param));
	}
	
	@RequestMapping(value = "list_sale", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody void list_sale(String searchContent, HttpSession session, HttpServletResponse response) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put("searchContent", StringUtil.decodeString(searchContent));
        List<T_Vip_MemberType> memberTypes = memberTypeService.list(param);
        StringBuffer jsonData= new StringBuffer();
		jsonData.append("{\"rows\":[");
		if (memberTypes != null && memberTypes.size() > 0){
			jsonData.append("{\"mt_code\":\"ALL\",\"mt_name\":\"不做限制\",\"mt_discount\":\"\"},");
			jsonData.append("{\"mt_code\":\"Y\",\"mt_name\":\"所有会员\",\"mt_discount\":\"\"},");
			jsonData.append("{\"mt_code\":\"N\",\"mt_name\":\"非会员\",\"mt_discount\":\"\"},");
			for(T_Vip_MemberType type:memberTypes){
					jsonData.append("{");
					jsonData.append("\"mt_code\":\""+type.getMt_code()+"\",");
					jsonData.append("\"mt_name\":\""+type.getMt_name()+"\",");
					jsonData.append("\"mt_discount\":\""+type.getMt_discount()+"\"");
					jsonData.append("},");	
			}
			jsonData.deleteCharAt(jsonData.length()-1);		
		}
		jsonData.append("]}");
		
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.print(jsonData.toString());
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			if(out != null)out.close();
		}
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Vip_MemberType memberType,HttpSession session) {
		T_Sys_User user = getUser(session);
		memberType.setCompanyid(user.getCompanyid());
		if(CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype()) || CommonUtil.FOUR.equals(user.getShoptype())){//总公司、分公司、加盟店
			memberType.setMt_shop_upcode(user.getUs_shop_code());
		}else {//自营店、合伙店
			memberType.setMt_shop_upcode(user.getShop_upcode());
		}
		Integer id = memberTypeService.queryByName(memberType);
		if (null != id && id > 0) {
			return result(EXISTED);
		} else {
			memberTypeService.save(memberType);
			return ajaxSuccess(memberType);
		}
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Vip_MemberType memberType,HttpSession session) {
		memberType.setCompanyid(getCompanyid(session));
		Integer id = memberTypeService.queryByName(memberType);
		if (null != id && id > 0) {
			return result(EXISTED);
		} else {
			memberTypeService.update(memberType);
			return ajaxSuccess(memberType);
		}
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(@RequestParam Integer mt_id) {
		memberTypeService.del(mt_id);
		return ajaxSuccess();
	}
}
