package zy.controller.vip;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sys.user.T_Sys_User;
import zy.entity.vip.set.T_Vip_AgeGroupSetUp;
import zy.entity.vip.set.T_Vip_BirthdaySms;
import zy.entity.vip.set.T_Vip_Grade;
import zy.entity.vip.set.T_Vip_ReturnSetUp;
import zy.entity.vip.set.T_Vip_Setup;
import zy.service.vip.set.VipSetService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("vip/set")
public class VipSetController extends BaseController{
	
	@Resource
	private VipSetService vipSetService;
	
	@RequestMapping(value = "to_update", method = RequestMethod.GET)
	public String to_update(HttpSession session,Model model) {
		T_Sys_User user = getUser(session);
		Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, user.getCompanyid());
        params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        params.put(CommonUtil.SHOP_UPCODE, user.getShop_upcode());
        params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        //会员短信设置
        T_Vip_BirthdaySms birthdaySms = vipSetService.loadBirthdaySms(params);
        if(birthdaySms != null){
        	birthdaySms.setShop_name(user.getShop_name());
        }else {
        	birthdaySms.setVb_state(0);
        	birthdaySms.setVb_agowarn_day(0);
		}
        model.addAttribute("birthdaySms", birthdaySms);
        //消费回访设置
        List<T_Vip_ReturnSetUp> returnSetUps = vipSetService.loadReturnSetUp(params);
        if(null != returnSetUps && returnSetUps.size()>0){
        	String allDaysArray="";
        	for(int i=0;i<returnSetUps.size();i++){
        		allDaysArray+=","+ returnSetUps.get(i).getRts_day();
        	}
        	allDaysArray=allDaysArray.replaceFirst(",", "");
        	model.addAttribute("allDaysArray", allDaysArray);//回访日期，拼接成字符串
        }
        model.addAttribute("returnSetUps", returnSetUps);
        //年龄段设置
        List<T_Vip_AgeGroupSetUp> ageGroupSetUp = vipSetService.loadAgeGroupSetUp(params);
        model.addAttribute("ageGroupSetUp", ageGroupSetUp);
        //会员等级设置
        List<T_Vip_Grade> grades = vipSetService.loadGrade(params);
        model.addAttribute("grades", grades);
        //会员系统参数设置
        int lossDay = 180;//会员流失天数
		int consumeDay = 90;//会员默认消费周期
		int loyalVipTimes = 3;//忠诚客户消费次数
		double richVipMoney = 1000;//高价客户消费金额
        T_Vip_Setup setup = vipSetService.loadSetUp(params);
        if(setup != null){
        	lossDay = setup.getVs_loss_day();
        	consumeDay = setup.getVs_consume_day();
        	loyalVipTimes= setup.getVs_loyalvip_times();
        	richVipMoney = setup.getVs_richvip_money();
        }
        model.addAttribute("lossDay", lossDay);
        model.addAttribute("consumeDay", consumeDay);
        model.addAttribute("loyalVipTimes", loyalVipTimes);
        model.addAttribute("richVipMoney", richVipMoney);
		return "vip/set/update";
	}
	
	@RequestMapping(value = "to_add_returnset", method = RequestMethod.GET)
	public String to_add_returnset(HttpServletRequest request) {
		return "vip/set/add_returnset";
	}
	
	@RequestMapping(value = "to_add_agegroupset", method = RequestMethod.GET)
	public String to_add_agegroupset(HttpServletRequest request) {
		return "vip/set/add_agegroupset";
	}
	
	@RequestMapping(value = "to_update_agegroupset", method = RequestMethod.GET)
	public String to_update_agegroupset(HttpSession session,Model model,@RequestParam Integer ags_id) {
		T_Vip_AgeGroupSetUp ageGroupSetUp = vipSetService.loadAgeGroupSetUp(ags_id);
		if(ageGroupSetUp == null){
			ageGroupSetUp = new T_Vip_AgeGroupSetUp();
		}
		model.addAttribute("ageGroupSetUp", ageGroupSetUp);
		return "vip/set/update_agegroupset";
	}
	/**
	 * 会员等级列表弹出框选择
	 */
	@RequestMapping("to_list_dialog")
	public String to_list_dialog() {
		return "vip/set/list_dialog";
	}
	
	@RequestMapping(value = "getShopSmsSet", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object getShopSmsSet(HttpSession session,@RequestParam String shop_code) {
		T_Sys_User user = getUser(session);
		Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, user.getCompanyid());
        params.put("shop_code", shop_code);
        T_Vip_BirthdaySms birthdaySms = vipSetService.loadBirthdaySms(params);
        if(birthdaySms == null){
        	birthdaySms = new T_Vip_BirthdaySms();
        	birthdaySms.setVb_state(2);
        	birthdaySms.setVb_agowarn_day(0);
        	birthdaySms.setVb_sms_content("");
        }
		return ajaxSuccess(birthdaySms);
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> params = new HashMap<String, Object>();
		params.put(CommonUtil.COMPANYID, user.getCompanyid());
		params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
	    params.put(CommonUtil.SHOP_UPCODE, user.getShop_upcode());
	    params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
		//会员短信
		T_Vip_BirthdaySms birthdaySms = new T_Vip_BirthdaySms();
		birthdaySms.setCompanyid(user.getCompanyid());
		birthdaySms.setVb_agowarn_day(Integer.parseInt(request.getParameter("vb_agowarn_day")));
		birthdaySms.setVb_shop_code(request.getParameter("vb_shop_code"));
		birthdaySms.setVb_sms_content(StringUtil.decodeString(request.getParameter("vb_sms_content")));
		birthdaySms.setVb_state(Integer.parseInt(request.getParameter("vb_state")));
		params.put("birthdaySms", birthdaySms);
		
		//保存回访设置
		String rts_seconds = request.getParameter("rts_seconds");
		if(rts_seconds!=null && !"".equals(rts_seconds)){
			List<T_Vip_ReturnSetUp> returnSetUps = new ArrayList<T_Vip_ReturnSetUp>();
			String shop_code = user.getUs_shop_code();
			if(CommonUtil.THREE.equals(user.getShoptype()) || CommonUtil.FIVE.equals(user.getShoptype())){//自营店、合伙店
				shop_code = user.getShop_upcode();
			}
			String[] rts_second = rts_seconds.split(",");
			T_Vip_ReturnSetUp returnSetUp = null;
			for(int i=0;i<rts_second.length;i++){
				returnSetUp = new T_Vip_ReturnSetUp();
				returnSetUp.setRts_day(Integer.parseInt(request.getParameter("rts_day"+rts_second[i])));//获取回访次数对应的天数
				returnSetUp.setRts_name(StringUtil.decodeString(request.getParameter("rts_name"+rts_second[i])));//获取回访次数对应的名称
				returnSetUp.setRts_second(Integer.parseInt(rts_second[i]));
				returnSetUp.setRts_shop_code(shop_code);
				returnSetUp.setCompanyid(user.getCompanyid());
				returnSetUps.add(returnSetUp);
			}
			params.put("returnSetUps", returnSetUps);
		}
		
		//会员等级设置
		List<T_Vip_Grade> grades = new ArrayList<T_Vip_Grade>();
		T_Vip_Grade grade = null;
		for(int i=1;i<5;i++){
			grade = new T_Vip_Grade();
			grade.setGd_lower(Double.parseDouble(request.getParameter("gd_lower"+i)));
			grade.setGd_upper(Double.parseDouble(request.getParameter("gd_upper"+i)));
			grade.setGd_color(StringUtil.decodeString(request.getParameter("gd_color"+i)));
			grade.setGd_name(StringUtil.decodeString(request.getParameter("gd_name"+i)));
			grade.setGd_remark(StringUtil.decodeString(request.getParameter("gd_remark"+i)));
			grade.setGd_code(StringUtil.decodeString(request.getParameter("gd_code"+i)));
			grade.setCompanyid(user.getCompanyid());
			grades.add(grade);
		}
		params.put("grades", grades);
		
		//会员系统参数设置
		T_Vip_Setup setup = new T_Vip_Setup();
		setup.setCompanyid(user.getCompanyid());
		setup.setVs_loss_day(Integer.parseInt(request.getParameter("vs_loss_day")));
		setup.setVs_consume_day(Integer.parseInt(request.getParameter("vs_consume_day")));
		setup.setVs_loyalvip_times(Integer.parseInt(request.getParameter("vs_loyalvip_times")));
		setup.setVs_richvip_money(Double.parseDouble(request.getParameter("vs_richvip_money")));
		params.put("setup", setup);
		
		vipSetService.update(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "delReturnSet", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object delReturnSet(HttpSession session,@RequestParam Integer rts_id) {
		T_Sys_User user = getUser(session);
		Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, user.getCompanyid());
        params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        params.put(CommonUtil.SHOP_UPCODE, user.getShop_upcode());
        params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        params.put("rts_id", rts_id);
		vipSetService.delReturnSet(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "delAgeGroupSet", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object delAgeGroupSet(HttpSession session,@RequestParam Integer ags_id) {
		T_Sys_User user = getUser(session);
		Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, user.getCompanyid());
        params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        params.put(CommonUtil.SHOP_UPCODE, user.getShop_upcode());
        params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        params.put("ags_id", ags_id);
		vipSetService.delAgeGroupSet(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "saveReturnSet", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object saveReturnSet(HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> params = new HashMap<String, Object>();
		params.put(CommonUtil.COMPANYID, user.getCompanyid());
		params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		params.put(CommonUtil.SHOP_UPCODE, user.getShop_upcode());
		params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
		String rts_name = StringUtil.decodeString(request.getParameter("rts_name"));
		String rts_day = request.getParameter("rts_day");
		String rts_second = request.getParameter("rts_second");
		params.put("rts_name", rts_name);
		params.put("rts_day", rts_day);
		params.put("rts_second", rts_second);
		vipSetService.saveReturnSet(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "saveAgeGroupSet", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object saveAgeGroupSet(HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> params = new HashMap<String, Object>();
		params.put(CommonUtil.COMPANYID, user.getCompanyid());
		params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		params.put(CommonUtil.SHOP_UPCODE, user.getShop_upcode());
		params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
		String ags_beg_age = request.getParameter("ags_beg_age");
		String ags_end_age = request.getParameter("ags_end_age");
		params.put("ags_beg_age", ags_beg_age);
		params.put("ags_end_age", ags_end_age);
		vipSetService.saveAgeGroupSet(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "updateAgeGroupSet", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object updateAgeGroupSet(HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> params = new HashMap<String, Object>();
		params.put(CommonUtil.COMPANYID, user.getCompanyid());
		String ags_id = request.getParameter("ags_id");
		String ags_beg_age = request.getParameter("ags_beg_age");
		String ags_end_age = request.getParameter("ags_end_age");
		params.put("ags_id", ags_id);
		params.put("ags_beg_age", ags_beg_age);
		params.put("ags_end_age", ags_end_age);
		vipSetService.updateAgeGroupSet(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "grade_list", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object grade_list(String searchContent, HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put("searchContent", StringUtil.decodeString(searchContent));
		return ajaxSuccess(vipSetService.loadGrade(param));
	}
}
