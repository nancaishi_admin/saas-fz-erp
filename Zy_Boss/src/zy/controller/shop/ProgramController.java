package zy.controller.shop;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import zy.controller.BaseController;
import zy.entity.PageData;
import zy.entity.shop.program.T_Shop_Program;
import zy.entity.sys.user.T_Sys_User;
import zy.service.shop.program.ProgramService;
import zy.util.CommonUtil;

@Controller
@RequestMapping("shop/program")
public class ProgramController extends BaseController{
	
	@Resource
	private ProgramService programService;
	
	@RequestMapping(value = "/to_list", method = RequestMethod.GET)
	public String to_list() {
		return "shop/program/list";
	}
	
	@RequestMapping("to_add")
	public String to_add() {
		return "shop/program/add";
	}
	
	@RequestMapping(value = "/to_view", method = RequestMethod.GET)
	public String to_view(@RequestParam String sp_id,Model model,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
		param.put(CommonUtil.COMPANYID, user.getCompanyid());
		param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
	    param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		param.put("sp_id", sp_id);
		T_Shop_Program program = programService.load(param);
		if(null != program){
			model.addAttribute("program",program);
		}
		return "shop/program/view";
	}
	
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object list(HttpServletRequest request,HttpSession session) {
		String page = request.getParameter("page"); // 页码
        String rows = request.getParameter("rows"); // 每页条数
        String sidx = request.getParameter("sidx");
        String sord = request.getParameter("sord");
        String begindate = request.getParameter("begindate");
        String enddate = request.getParameter("enddate");
        String sp_state = request.getParameter("sp_state");
        String sp_shop_code = request.getParameter("sp_shop_code");
        
        int pageIndex = 1;
        int pageSize = super.pagesize;
        if (org.apache.commons.lang.StringUtils.isNotEmpty(page)) {
        	pageIndex = Integer.parseInt(page);
        	pageIndex = pageIndex == 0 ? 1 : pageIndex;
        }
        if (org.apache.commons.lang.StringUtils.isNotEmpty(rows)) {
        	pageSize = Integer.parseInt(rows);
        }
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("sp_state", sp_state);
        param.put("sp_shop_code", sp_shop_code);
        param.put(CommonUtil.PAGESIZE, pageSize);
        param.put(CommonUtil.PAGEINDEX, pageIndex);
        param.put(CommonUtil.SIDX, sidx);
        param.put(CommonUtil.SORD, sord);
		PageData<T_Shop_Program> pageData = programService.page(param);
		return ajaxSuccess(pageData, "查询成功, 共" + pageData.getList().size() + "条数据");
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET,RequestMethod.POST })
	@ResponseBody
	public Object save(T_Shop_Program program,MultipartFile uploadPro, 
			String serverPath, HttpSession session) throws Exception{
		T_Sys_User user = getUser(session);
		if (uploadPro == null || uploadPro.getSize() <= 0) {
			return ajaxFailMessage("请上传照片");
		}
		//获取图片大小
		long picSize = (uploadPro.getSize()/1024) + 1;
		//图片不能大于200K
		if(picSize > 200){
			return ajaxFailMessage("上传的图片不能大于200K!");
		}
		
		String realpath = CommonUtil.CHECK_BASE;
		String uploadFileName = uploadPro.getOriginalFilename();
		String suffix = uploadFileName.substring(uploadFileName
				.lastIndexOf("."));
		String img_name = new Date().getTime() + "" + new Random().nextInt(9999) + suffix;
		String img_path = CommonUtil.PROGRAM_IMG_PATH + "/" + img_name;
//		String all_path = serverPath + img_path;
		
		program.setSp_imgpath(img_path);
		programService.save(program,user);
		
		File savefile = new File(realpath, img_path);
		if (!savefile.getParentFile().exists()) {
			savefile.getParentFile().mkdirs();
		}
		uploadPro.transferTo(savefile);
		return ajaxSuccess("保存成功");
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(@RequestParam Integer sps_id,@RequestParam Integer sp_id,@RequestParam String sp_imgpath,
			HttpServletRequest request,HttpSession session) {
		if (sps_id == null || 0 == sps_id) {
			throw new IllegalArgumentException("id不能为空！");
		}
		if (sp_id == null || 0 == sp_id) {
			throw new IllegalArgumentException("id不能为空！");
		}
		if (sp_imgpath == null || "".equals(sp_imgpath)) {
			throw new IllegalArgumentException("图片路径不能为空！");
		}
		programService.del(sp_id,sps_id);
		String realpath = CommonUtil.CHECK_BASE;
		File _temp = new File(realpath + sp_imgpath);
		if (_temp.exists()) {
			_temp.delete();
		}
		return ajaxSuccess();
	}
	
}
