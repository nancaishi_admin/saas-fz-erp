package zy.controller.shop;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.PageData;
import zy.entity.sell.deposit.T_Sell_DepositList;
import zy.entity.sys.user.T_Sys_User;
import zy.service.shop.deposit.DepositService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("shop/deposit")
public class DepositController extends BaseController{
	
	@Resource
	private DepositService depositService;
	
	@RequestMapping(value = "/to_list", method = RequestMethod.GET)
	public String to_list() {
		return "shop/deposit/list";
	}
	
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object list(HttpServletRequest request,HttpSession session) {
		String page = request.getParameter("page"); // 页码
        String rows = request.getParameter("rows"); // 每页条数
        String sidx = request.getParameter("sidx");
        String sord = request.getParameter("sord");
        String begindate = request.getParameter("begindate");
        String enddate = request.getParameter("enddate");
        String sdl_state = request.getParameter("sdl_state");
        String sdl_shop_code = request.getParameter("sdl_shop_code");
        String pd_no = request.getParameter("pd_no");
        String sdl_number = request.getParameter("sdl_number");
        
        int pageIndex = 1;
        int pageSize = super.pagesize;
        if (org.apache.commons.lang.StringUtils.isNotEmpty(page)) {
        	pageIndex = Integer.parseInt(page);
        	pageIndex = pageIndex == 0 ? 1 : pageIndex;
        }
        if (org.apache.commons.lang.StringUtils.isNotEmpty(rows)) {
        	pageSize = Integer.parseInt(rows);
        }
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("sdl_state", sdl_state);
        param.put("sdl_shop_code", sdl_shop_code);
        param.put("pd_no", StringUtil.decodeString(pd_no));
        param.put("sdl_number", StringUtil.decodeString(sdl_number));
        param.put(CommonUtil.PAGESIZE, pageSize);
        param.put(CommonUtil.PAGEINDEX, pageIndex);
        param.put(CommonUtil.SIDX, sidx);
        param.put(CommonUtil.SORD, sord);
		PageData<T_Sell_DepositList> pageData = depositService.page(param);
		return ajaxSuccess(pageData, "查询成功, 共" + pageData.getList().size() + "条数据");
	}
}
