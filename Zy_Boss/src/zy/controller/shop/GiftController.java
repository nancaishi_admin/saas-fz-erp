package zy.controller.shop;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.PageData;
import zy.entity.buy.order.T_Buy_Order;
import zy.entity.shop.gift.T_Shop_Gift;
import zy.entity.shop.gift.T_Shop_GiftList;
import zy.entity.shop.gift.T_Shop_Gift_Run;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.shop.gift.GiftService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;
import zy.vo.shop.GiftVO;

@Controller
@RequestMapping("shop/gift")
public class GiftController extends BaseController{
	@Resource
	private GiftService giftService;
	
	@RequestMapping(value = "/to_list", method = RequestMethod.GET)
	public String to_list() {
		return "shop/gift/list";
	}

	@RequestMapping(value = "/to_report_list", method = RequestMethod.GET)
	public String to_report_list() {
		return "shop/gift/report_list";
	}
	@RequestMapping(value = "to_select_product", method = RequestMethod.GET)
	public String to_select_product(Model model) {
		return "shop/gift/select_product";
	}
	
	@RequestMapping(value = "to_temp_update", method = RequestMethod.GET)
	public String to_temp_update(HttpServletRequest request,Model model) {
		String pd_code = request.getParameter("pd_code");
		String needVaildateStock = request.getParameter("needVaildateStock");
		String gil_shop_code = request.getParameter("gil_shop_code");
		String fromJsp = request.getParameter("fromJsp");
		model.addAttribute("pd_code", pd_code);
		model.addAttribute("needVaildateStock", needVaildateStock);
		model.addAttribute("gil_shop_code", gil_shop_code);
		model.addAttribute("fromJsp", fromJsp);
		return "shop/gift/temp_update";
	}
	
	@RequestMapping(value = "/to_add", method = { RequestMethod.GET })
	public String to_add(Model model, HttpSession session) {
		String begindate = DateUtil.getYearMonthDate();
		String enddate = DateUtil.getDateAddMonths(1);
		model.addAttribute("begindate", begindate);// 获取服务器日期//获取服务器日期
		model.addAttribute("enddate", enddate);// 获取一个月后日期

		T_Sys_User user = getUser(session);
		Map<String, Object> param = new HashMap<String, Object>();
		param.put(CommonUtil.COMPANYID, user.getCompanyid());
		param.put("us_code", user.getUs_code());
		// 删除临时表
		giftService.deleteTemp(param);
		return "shop/gift/add";
	}
	
	@RequestMapping(value = "/to_run_list", method = RequestMethod.GET)
	public String to_run_list(HttpServletRequest request,Model model) {
		String gi_pd_code = request.getParameter("gi_pd_code");
		String gi_shop_code = request.getParameter("gi_shop_code");
		model.addAttribute("gi_pd_code", gi_pd_code);
		model.addAttribute("gi_shop_code", gi_shop_code);
		return "shop/gift/run_list";
	}
	
	@RequestMapping(value = "/to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer gi_id,Model model,HttpSession session) {
		T_Sys_User user = getUser(session);
		T_Shop_Gift gift = giftService.load(gi_id);
		if(null == gift){
			gift = new T_Shop_Gift();
		}else {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put(CommonUtil.COMPANYID, user.getCompanyid());
			params.put("us_code", user.getUs_code());
			params.put("gi_pd_code", gift.getGi_pd_code());
			params.put("gi_shop_code", gift.getGi_shop_code());
			giftService.initGiftUpdate(params);
		}
		model.addAttribute("gift",gift);
		return "shop/gift/update";
	}
	
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object list(HttpServletRequest request,HttpSession session) {
		String page = request.getParameter("page"); // 页码
        String rows = request.getParameter("rows"); // 每页条数
        String sidx = request.getParameter("sidx");
        String sord = request.getParameter("sord");
        
        String begindate = request.getParameter("begindate");
        String enddate = request.getParameter("enddate");
        String pd_no = request.getParameter("pd_no");
        String gi_state = request.getParameter("gi_state");
        String gi_shop_code = request.getParameter("gi_shop_code");
        
        int pageIndex = 1;
        int pageSize = super.pagesize;
        if (org.apache.commons.lang.StringUtils.isNotEmpty(page)) {
        	pageIndex = Integer.parseInt(page);
        	pageIndex = pageIndex == 0 ? 1 : pageIndex;
        }
        if (org.apache.commons.lang.StringUtils.isNotEmpty(rows)) {
        	pageSize = Integer.parseInt(rows);
        }
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put(CommonUtil.PAGESIZE, pageSize);
        param.put(CommonUtil.PAGEINDEX, pageIndex);
        param.put(CommonUtil.SIDX, sidx);
        param.put(CommonUtil.SORD, sord);
        param.put("pd_no", pd_no);
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("gi_state", gi_state);
        param.put("gi_shop_code", gi_shop_code);
		PageData<T_Shop_Gift> pageData = giftService.page(param);
		return ajaxSuccess(pageData, "查询成功, 共" + pageData.getList().size() + "条数据");
	}
	
	@RequestMapping(value = "page_product", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object page_product(PageForm pageForm,
			@RequestParam(required = false) String gil_shop_code,
			@RequestParam(required = false) String exactQuery,
			HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
        param.put("searchContent", pageForm.getSearchContent());
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put("us_code", user.getUs_code());
        param.put("gil_shop_code", gil_shop_code);
        
        //根据店铺编号查询仓库
        List<String> dpcodeList =giftService.queryDpCodeByShop(param);
        StringBuffer dp_code = new StringBuffer();
		if (dpcodeList != null && dpcodeList.size() > 0) {
			for(int i=0;i<dpcodeList.size();i++){
				dp_code.append(dpcodeList.get(i)).append(",");
			}
			dp_code.deleteCharAt(dp_code.length()-1);
		}
		param.put("dp_code", dp_code);
        param.put("exactQuery", exactQuery);
		return ajaxSuccess(giftService.page_product(param));
	}
	
	@RequestMapping(value = "temp_list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_list(HttpServletRequest request,HttpSession session) {
		String gil_shop_code = request.getParameter("gil_shop_code");
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
		param.put("gil_user_code", user.getUs_code());
		param.put(CommonUtil.COMPANYID, user.getCompanyid());
		if(gil_shop_code == null || "".equals(gil_shop_code)){
        	gil_shop_code = user.getUs_shop_code();
		}
        param.put("gil_shop_code", gil_shop_code);
        //根据店铺编号查询仓库
        List<String> dpcodeList =giftService.queryDpCodeByShop(param);
        StringBuffer dp_code = new StringBuffer();
		if (dpcodeList != null && dpcodeList.size() > 0) {
			for(int i=0;i<dpcodeList.size();i++){
				dp_code.append(dpcodeList.get(i)).append(",");
			}
			dp_code.deleteCharAt(dp_code.length()-1);
		}
		param.put("dp_code", dp_code.toString());
		return ajaxSuccess(giftService.temp_list(param));
	}
	
	@RequestMapping(value = "temp_sum", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object temp_sum(HttpSession session) {
		T_Sys_User user = getUser(session);
		return ajaxSuccess(giftService.temp_sum(user.getUs_code(), user.getCompanyid()));
	}
	
	@RequestMapping(value = "temp_size", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object temp_size(HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("gil_user_code", user.getUs_code());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(giftService.temp_size(params));
	}
	
	@RequestMapping(value = "temp_size_title", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object temp_size_title(HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("gil_user_code", user.getUs_code());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(giftService.temp_size_title(params));
	}
	
	@RequestMapping(value = "temp_loadproduct", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public void temp_loadproduct(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
		PrintWriter out=null;
		try {
			out=response.getWriter();
	        String gil_shop_code = request.getParameter("gil_shop_code");
	        String pd_code = request.getParameter("pd_code");
	        String fromJsp = StringUtil.trimString(request.getParameter("fromJsp"));
	        String sizeGroupCode = "";
	        T_Sys_User user = getUser(session);
	        Map<String,Object> param = new HashMap<String, Object>();
	        param.put(CommonUtil.COMPANYID, user.getCompanyid());
	        param.put("gil_shop_code", gil_shop_code);
	        //根据店铺编号查询仓库
	        List<String> dpcodeList =giftService.queryDpCodeByShop(param);
	        StringBuffer dp_code = new StringBuffer();
			if (dpcodeList != null && dpcodeList.size() > 0) {
				for(int i=0;i<dpcodeList.size();i++){
					dp_code.append(dpcodeList.get(i)).append(",");
				}
				dp_code.deleteCharAt(dp_code.length()-1);
			}
			param.put("fromJsp", fromJsp);
			param.put("user", user);
			param.put("pd_code", pd_code);
			param.put("sizeGroupCode", sizeGroupCode);
			param.put("dp_code", dp_code.toString());
			String json = giftService.temp_loadproduct(param);
			out.print(json);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if (out!=null){
				out.flush();
				out.close();
				out=null;
			}
		}
	}
	
	@RequestMapping(value = "temp_save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_save(HttpSession session,HttpServletRequest request) {
		T_Sys_User user = getUser(session);
		String datas=StringUtil.decodeString(request.getParameter("data"));//json
		String pd_code = StringUtil.trimString(request.getParameter("pd_code"));
		String fromJsp = StringUtil.trimString(request.getParameter("fromJsp"));
		List<T_Shop_GiftList> temps = null;
		if(!StringUtil.trimString(datas).equals("")){
			if("modify".equals(fromJsp)){
				temps = GiftVO.convertJsonToGiftListTempForModify(datas, user);
			}else{
				temps = GiftVO.convertJsonToGiftListTemp(datas, user);
			}
		}
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("temps", temps);
		params.put("user", user);
		params.put("pd_code", pd_code);
		
		if("modify".equals(fromJsp)){
			giftService.temp_save_modify(params);
		}else{
			giftService.temp_save(params);
		}
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_del(@RequestParam Integer gil_id) {
		giftService.temp_del(gil_id);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_delByPiCode", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_delByPiCode(@RequestParam String pd_code,
			@RequestParam(required = false) String cr_code,
			@RequestParam(required = false) String br_code,
			HttpSession session) {
		T_Sys_User user = getUser(session);
		T_Shop_GiftList temp = new T_Shop_GiftList();
		temp.setGil_pd_code(pd_code);
		temp.setGil_cr_code(cr_code);
		temp.setGil_bs_code(br_code);
		temp.setGil_user_code(user.getUs_code());
		temp.setCompanyid(user.getCompanyid());
		giftService.temp_delByPiCode(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateAmountById", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateAmountById(@RequestParam Integer gil_id,
			@RequestParam(required = false) Integer amount,
			HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("gil_id", gil_id);
		params.put("amount", amount);
		giftService.temp_updateAmountById(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updatePointByPdcode", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updatePointByPdcode(@RequestParam String pd_code,
			@RequestParam(required = false) Integer point,
			HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("pd_code", pd_code);
		params.put("point", point);
		params.put("user", user);
		params.put("us_code", user.getUs_code());
		params.put("companyid", user.getCompanyid());
		giftService.temp_updatePointByPdcode(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Shop_Gift gift,HttpSession session) {
		giftService.save(gift, getUser(session));
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "list_run", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object list_run(HttpServletRequest request,HttpSession session) {
		String page = request.getParameter("page"); // 页码
        String rows = request.getParameter("rows"); // 每页条数
        String sidx = request.getParameter("sidx");
        String sord = request.getParameter("sord");
        
        String begindate = request.getParameter("begindate");
        String enddate = request.getParameter("enddate");
        String gir_pd_code = request.getParameter("gir_pd_code");
        String gir_shop_code = request.getParameter("gir_shop_code");
        String gir_number = request.getParameter("gir_number");
        
        int pageIndex = 1;
        int pageSize = super.pagesize;
        if (org.apache.commons.lang.StringUtils.isNotEmpty(page)) {
        	pageIndex = Integer.parseInt(page);
        	pageIndex = pageIndex == 0 ? 1 : pageIndex;
        }
        if (org.apache.commons.lang.StringUtils.isNotEmpty(rows)) {
        	pageSize = Integer.parseInt(rows);
        }
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.PAGESIZE, pageSize);
        param.put(CommonUtil.PAGEINDEX, pageIndex);
        param.put(CommonUtil.SIDX, sidx);
        param.put(CommonUtil.SORD, sord);
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put("gir_pd_code", gir_pd_code);
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("gir_shop_code", gir_shop_code);
        param.put("gir_number", gir_number);
		PageData<T_Shop_Gift_Run> pageData = giftService.list_run(param);
		return ajaxSuccess(pageData, "查询成功, 共" + pageData.getList().size() + "条数据");
	}
	
	@RequestMapping(value = "show_gift", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object show_gift(@RequestParam String pd_code,@RequestParam String shop_code,
			@RequestParam Integer state,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put(CommonUtil.COMPANYID, user.getCompanyid());
		params.put("pd_code", pd_code);
		params.put("shop_code", shop_code);
		params.put("state", state);
		giftService.show_gift(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(@RequestParam String pd_code,@RequestParam String shop_code,
			@RequestParam Integer gi_id,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put(CommonUtil.COMPANYID, user.getCompanyid());
		params.put("pd_code", pd_code);
		params.put("shop_code", shop_code);
		params.put("gi_id", gi_id);
		giftService.del(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Shop_Gift gift,HttpSession session) {
		giftService.update(gift, getUser(session));
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "updateGiftListTempAddAmount", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object updateGiftListTempAddAmount(@RequestParam Integer amount,
			@RequestParam Integer gil_id,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put(CommonUtil.COMPANYID, user.getCompanyid());
		params.put("gil_id", gil_id);
		params.put("amount", amount);
		giftService.updateGiftListTempAddAmount(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "getGiftTempSum", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object getGiftTempSum(HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("gil_user_code", user.getUs_code());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(giftService.getGiftTempSum(params));
	}
	
	
	@RequestMapping(value = "report_list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object report_list(PageForm pageForm,String begindate,
			String enddate,String gs_shop_code,
			HttpServletRequest request,HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        bulidParam(param, getUser(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("gs_shop_code", gs_shop_code);
		return ajaxSuccess(giftService.report_list(param));
	}
}
