package zy.controller.money;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.approve.T_Approve_Record;
import zy.entity.money.income.T_Money_Income;
import zy.entity.money.income.T_Money_IncomeList;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.money.income.IncomeService;
import zy.util.CommonUtil;
import zy.util.StringUtil;
import zy.vo.money.IncomeVO;

import com.alibaba.fastjson.JSON;

@Controller
@RequestMapping("money/income")
public class IncomeController extends BaseController{
	
	@Resource
	private IncomeService incomeService;
	
	@RequestMapping(value = "to_list", method = RequestMethod.GET)
	public String to_list() {
		return "money/income/list";
	}
	@RequestMapping(value = "to_add", method = RequestMethod.GET)
	public String to_add() {
		return "money/income/add";
	}
	@RequestMapping(value = "to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer ic_id,Model model,HttpSession session) {
		T_Sys_User user = getUser(session);
		T_Money_Income income = incomeService.load(ic_id);
		if(null != income){
			incomeService.initUpdate(income.getIc_number(), user.getUs_id(), user.getCompanyid());
			model.addAttribute("income",income);
		}
		return "money/income/update";
	}
	@RequestMapping(value = "to_view", method = RequestMethod.GET)
	public String to_view(@RequestParam Integer ic_id,Model model) {
		T_Money_Income income = incomeService.load(ic_id);
		if(null != income){
			model.addAttribute("income",income);
		}
		return "money/income/view";
	}
	@RequestMapping(value = "to_report", method = RequestMethod.GET)
	public String to_report() {
		return "money/income/report";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = false) Integer ic_ar_state,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String ic_ba_code,
			@RequestParam(required = false) String ic_shop_code,
			@RequestParam(required = false) String ic_manager,
			@RequestParam(required = false) String ic_number,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("ic_ar_state", ic_ar_state);
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("ic_ba_code", ic_ba_code);
        param.put("ic_shop_code", ic_shop_code);
        param.put("ic_manager", StringUtil.decodeString(ic_manager));
        param.put("ic_number", StringUtil.decodeString(ic_number));
		return ajaxSuccess(incomeService.page(param));
	}
	
	@RequestMapping(value = "detail_list/{ic_number}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object detail_list(@PathVariable String ic_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("icl_number", ic_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(incomeService.detail_list(params));
	}
	
	@RequestMapping(value = "temp_list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_list(HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("icl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(incomeService.temp_list(params));
	}
	
	@RequestMapping(value = "temp_save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_save(@RequestParam String temps,HttpSession session) {
		T_Sys_User user = getUser(session);
		List<T_Money_IncomeList> incomeLists = JSON.parseArray(temps, T_Money_IncomeList.class);
		incomeService.temp_save(incomeLists, user);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateMoney", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateMoney(T_Money_IncomeList temp) {
		incomeService.temp_updateMoney(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateRemark", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateRemark(T_Money_IncomeList temp) {
		incomeService.temp_updateRemark(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_del(@RequestParam Integer icl_id) {
		incomeService.temp_del(icl_id);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_clear", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_clear(HttpSession session) {
		T_Sys_User user = getUser(session);
		incomeService.temp_clear(user.getUs_id(), user.getCompanyid());
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Money_Income income,HttpSession session) {
		incomeService.save(income, getUser(session));
		return ajaxSuccess(income);
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Money_Income income,HttpSession session) {
		incomeService.update(income, getUser(session));
		return ajaxSuccess(income);
	}
	
	@RequestMapping(value = "approve", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object approve(T_Approve_Record record,String number,HttpSession session) {
		record.setAr_describe(StringUtil.decodeString(record.getAr_describe()));
		return ajaxSuccess(incomeService.approve(number, record, getUser(session)));
	}
	
	@RequestMapping(value = "reverse", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object reverse(String number,HttpSession session) {
		return ajaxSuccess(incomeService.reverse(number, getUser(session)));
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(String number,HttpSession session) {
		incomeService.del(number, getCompanyid(session));
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "print", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object print(String number,Integer sp_id,HttpSession session) {
		Map<String, Object> resultMap = incomeService.loadPrintData(number, sp_id, getUser(session));
		resultMap.put("user", getUser(session));
		return ajaxSuccess(IncomeVO.buildPrintJson(resultMap));
	}
	
	@RequestMapping(value = "listReport", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listReport(PageForm pageForm,
			@RequestParam(required = true) String type,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String ic_shop_code,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put("type", type);
        param.put("ic_shop_code", ic_shop_code);
        param.put("begindate", begindate);
        param.put("enddate", enddate);
		return ajaxSuccess(incomeService.listReport(param));
	}
	
	@RequestMapping(value = "listReportDetail", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listReportDetail(PageForm pageForm,
			@RequestParam(required = false) String month,
			@RequestParam(required = false) String icl_mp_code,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String ic_shop_code,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put("month", month);
        param.put("icl_mp_code", icl_mp_code);
        param.put("ic_shop_code", ic_shop_code);
        param.put("begindate", begindate);
        param.put("enddate", enddate);
		return ajaxSuccess(incomeService.listReportDetail(param));
	}
	
}
