package zy.controller.sys;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sys.user.T_Sys_User;
import zy.service.home.HomeService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("home")
public class HomeController extends BaseController{
	@Resource
	private HomeService homeService;
	
	@RequestMapping(value = "to_daily_report", method = RequestMethod.GET)
	public String to_daily_report() {
		return "index/daily_report";
	}
	
	@RequestMapping(value = "loadHomeInfo", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object loadHomeInfo(HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
		param.put(CommonUtil.COMPANYID, getCompanyid(session));
		param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
		param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		return ajaxSuccess(homeService.loadHomeInfo(param));
	}
	
	@RequestMapping(value = "statSellByDay", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object statSellByDay(HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
		param.put(CommonUtil.COMPANYID, getCompanyid(session));
		param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
		param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		param.put("begindate", DateUtil.monthStartDay(DateUtil.getCurrentTime()));
		param.put("enddate", DateUtil.monthEndDay(DateUtil.getCurrentTime()));
		Map<String, Object> sellDayMap = homeService.statSellByDay(param);
		List<Map<String, Object>> resultList = new ArrayList<Map<String,Object>>();
		int dayCount = DateUtil.getCurrentMonthDays();
		Map<String, Object> item = null;
		for (int i = 1; i <= dayCount; i++) {
			item = new HashMap<String,Object>();
			item.put("day", String.valueOf(i));
			if(sellDayMap.containsKey(String.valueOf(i))){
				item.put("sellMoney", String.format("%.2f", Double.parseDouble(sellDayMap.get(String.valueOf(i)).toString())));
			}else {
				item.put("sellMoney", "0");
			}
			resultList.add(item);
		}
		return ajaxSuccess(resultList);
	}
	
	@RequestMapping(value = "statDayKpi", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object statDayKpi(@RequestParam(required = false) String shopCode,
			@RequestParam(required = false) String date,
			HttpSession session) {
		T_Sys_User user = getUser(session);
		if(StringUtil.isEmpty(date)){
			date = DateUtil.getYearMonthDate();
		}
		Map<String,Object> param = new HashMap<String, Object>();
		param.put(CommonUtil.COMPANYID, getCompanyid(session));
		param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
		param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		param.put("shopCode", shopCode);
		param.put("date", date);
		param.put("weekStart", DateUtil.weekFirstDay(date));
		param.put("weekEnd", DateUtil.weekLastDay(date));
		return ajaxSuccess(homeService.statDayKpi(param));
	}
	
	@RequestMapping(value = "statByHour", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object statByHour(@RequestParam(required = false) String shopCode,
			@RequestParam(required = false) String date,
			HttpSession session) {
		T_Sys_User user = getUser(session);
		if(StringUtil.isEmpty(date)){
			date = DateUtil.getYearMonthDate();
		}
		Map<String,Object> param = new HashMap<String, Object>();
		param.put(CommonUtil.COMPANYID, getCompanyid(session));
		param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
		param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		param.put("shopCode", shopCode);
		param.put("date", date);
		return ajaxSuccess(homeService.statByHour(param));
	}
	
	@RequestMapping(value = "loadWeather", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object loadWeather(@RequestParam(required = false) String shopCode,
			@RequestParam(required = false) String date,HttpSession session) {
		T_Sys_User user = getUser(session);
		if(StringUtil.isEmpty(date)){
			date = DateUtil.getYearMonthDate();
		}
		Map<String,Object> param = new HashMap<String, Object>();
		param.put(CommonUtil.COMPANYID, getCompanyid(session));
		param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
		param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		param.put("shopCode", shopCode);
		param.put("date", date);
		return ajaxSuccess(homeService.loadWeather(param));
	}
	
}
