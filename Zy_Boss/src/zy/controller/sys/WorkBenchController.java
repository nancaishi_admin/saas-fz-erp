package zy.controller.sys;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sys.user.T_Sys_User;
import zy.entity.sys.workbench.T_Sys_WorkBench;
import zy.service.sys.workbench.WorkBenchService;
import zy.util.CommonUtil;

@Controller
@RequestMapping(value="sys/workbench")
public class WorkBenchController extends BaseController{
	@Resource
	private WorkBenchService workBenchService;
	
	
	@RequestMapping(value = "to_workbench_my", method = RequestMethod.GET)
	public String to_workbench_my() {
		return "sys/workbench/workbench_my";
	}
	@RequestMapping(value = "to_workbench_set", method = RequestMethod.GET)
	public String to_workbench_set() {
		return "sys/workbench/workbench_set";
	}
	@RequestMapping(value = "to_add", method = RequestMethod.GET)
	public String to_add() {
		return "sys/workbench/add";
	}
	@RequestMapping(value = "to_add_todo", method = RequestMethod.GET)
	public String to_add_todo() {
		return "sys/workbench/add_todo";
	}
	
	@RequestMapping(value = "listBySelf", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listBySelf(@RequestParam(required = false) String wb_type,HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put("wb_us_id", user.getUs_id());
        params.put("wb_type", wb_type);
		return ajaxSuccess(workBenchService.list(params));
	}
	
	@RequestMapping(value = "listByUser", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listByUser(HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put("wb_us_id", user.getUs_id());
        List<T_Sys_WorkBench> workBenchs = workBenchService.listByUser(params);
        List<T_Sys_WorkBench> dailyWork = new ArrayList<T_Sys_WorkBench>();
        List<T_Sys_WorkBench> dailySheet = new ArrayList<T_Sys_WorkBench>();
		for (T_Sys_WorkBench item : workBenchs) {
			if(item.getWb_type().equals(0)){
				dailyWork.add(item);
			}else if(item.getWb_type().equals(1)){
				dailySheet.add(item);
			}
		}
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("dailyWork", dailyWork);
		resultMap.put("dailySheet", dailySheet);
		resultMap.put("toDo", workBenchService.listToDoByUser(params));
		return ajaxSuccess(resultMap);
	}
	
	@RequestMapping(value = "listUpMenu", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listUpMenu(HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put("mn_version", user.getCo_ver());
        params.put("shop_type", user.getShoptype());
        params.put("ro_code", user.getUs_ro_code());
		return ajaxSuccess(workBenchService.listUpMenu(params));
	}
	
	@RequestMapping(value = "listSubMenu", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listSubMenu(@RequestParam(required = false) String mn_upcode,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put("us_id", user.getUs_id());
        params.put("mn_version", user.getCo_ver());
        params.put("mn_upcode", mn_upcode);
        params.put("shop_type", user.getShoptype());
        params.put("ro_code", user.getUs_ro_code());
		return ajaxSuccess(workBenchService.listSubMenu(params));
	}
	
	@RequestMapping(value = "listToDo", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listToDo(HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put("us_id", user.getUs_id());
        params.put("td_version", user.getCo_ver());
        params.put("shop_type", user.getShoptype());
		return ajaxSuccess(workBenchService.listToDo(params));
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(Integer wb_type,String ids,HttpSession session) {
		workBenchService.save(getUser(session), wb_type, ids);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(String ids,HttpSession session) {
		String[] idArr = ids.split(",");
		List<Integer> idsList = new ArrayList<Integer>();
		for (String id : idArr) {
			idsList.add(Integer.parseInt(id));
		}
		workBenchService.delete(idsList);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "statToDoWarn", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object statToDoWarn(@RequestParam(required = true) String todoids,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
		param.put(CommonUtil.COMPANYID, getCompanyid(session));
		param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
		param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		param.put(CommonUtil.SHOP_UPCODE, user.getShop_upcode());
		param.put("us_id", user.getUs_id());
		param.put("todoids", todoids);
		return ajaxSuccess(workBenchService.statToDoWarn(param));
	}
	
	
}
