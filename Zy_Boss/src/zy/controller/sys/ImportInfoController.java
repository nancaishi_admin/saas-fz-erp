package zy.controller.sys;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import zy.controller.BaseController;
import zy.entity.sys.improtinfo.T_Import_Info;
import zy.entity.sys.user.T_Sys_User;
import zy.service.sys.importinfo.ImportInfoService;

@Controller
@RequestMapping("sys/importinfo")
public class ImportInfoController extends BaseController{
	@Resource
	private ImportInfoService importInfoService;
	
	@RequestMapping(value = "/to_import_result", method = RequestMethod.GET)
	public String to_import_result(HttpSession session,Model model,HttpServletRequest request) {
		String ii_type = request.getParameter("ii_type");
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
		param.put("companyid", user.getCompanyid());
		param.put("us_id", user.getUs_id());
		param.put("ii_type", ii_type);
		T_Import_Info t_Import_Info = importInfoService.queryImportInfo(param);
		if(null != t_Import_Info){
			model.addAttribute("t_Import_Info",t_Import_Info);
		}
		return "sys/importinfo/import_result";
	}
}
