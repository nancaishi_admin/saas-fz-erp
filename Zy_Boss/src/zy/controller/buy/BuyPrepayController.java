package zy.controller.buy;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.approve.T_Approve_Record;
import zy.entity.buy.prepay.T_Buy_Prepay;
import zy.form.PageForm;
import zy.service.buy.prepay.BuyPrepayService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("buy/prepay")
public class BuyPrepayController extends BaseController{
	@Resource
	private BuyPrepayService buyPrepayService;
	
	@RequestMapping(value = "to_list", method = RequestMethod.GET)
	public String to_list() {
		return "buy/prepay/list";
	}
	@RequestMapping(value = "to_add", method = RequestMethod.GET)
	public String to_add() {
		return "buy/prepay/add";
	}
	@RequestMapping(value = "to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer pp_id,Model model) {
		T_Buy_Prepay prepay = buyPrepayService.load(pp_id);
		if(null != prepay){
			model.addAttribute("prepay",prepay);
			model.addAttribute("supply",buyPrepayService.loadSupply(prepay.getPp_supply_code(), prepay.getCompanyid()));
			model.addAttribute("bank",buyPrepayService.loadBank(prepay.getPp_ba_code(), prepay.getCompanyid()));
		}
		return "buy/prepay/update";
	}
	@RequestMapping(value = "to_view", method = RequestMethod.GET)
	public String to_view(@RequestParam Integer pp_id,Model model) {
		T_Buy_Prepay prepay = buyPrepayService.load(pp_id);
		if(null != prepay){
			model.addAttribute("prepay",prepay);
		}
		return "buy/prepay/view";
	}
	@RequestMapping(value = "to_view/{number}", method = RequestMethod.GET)
	public String to_view(@PathVariable String number,Model model,HttpSession session) {
		T_Buy_Prepay prepay = buyPrepayService.load(number, getCompanyid(session));
		if(null != prepay){
			model.addAttribute("prepay",prepay);
		}
		return "buy/prepay/view";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = false) Integer pp_ar_state,
			@RequestParam(required = false) String pp_supply_code,
			@RequestParam(required = false) String pp_ba_code,
			@RequestParam(required = false) String pp_manager,
			@RequestParam(required = false) String pp_number,
			HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("begindate", pageForm.getBegindate());
        param.put("enddate", pageForm.getEnddate());
        param.put("pp_ar_state", pp_ar_state);
        param.put("pp_supply_code", pp_supply_code);
        param.put("pp_ba_code", pp_ba_code);
        param.put("pp_manager", StringUtil.decodeString(pp_manager));
        param.put("pp_number", StringUtil.decodeString(pp_number));
		return ajaxSuccess(buyPrepayService.page(param));
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Buy_Prepay prepay,HttpSession session) {
		buyPrepayService.save(prepay, getUser(session));
		return ajaxSuccess(prepay);
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Buy_Prepay prepay,HttpSession session) {
		buyPrepayService.update(prepay, getUser(session));
		return ajaxSuccess(prepay);
	}
	
	@RequestMapping(value = "approve", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object approve(T_Approve_Record record,String number,HttpSession session) {
		record.setAr_describe(StringUtil.decodeString(record.getAr_describe()));
		return ajaxSuccess(buyPrepayService.approve(number, record, getUser(session)));
	}
	
	@RequestMapping(value = "reverse", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object reverse(String number,HttpSession session) {
		return ajaxSuccess(buyPrepayService.reverse(number, getUser(session)));
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(String number,HttpSession session) {
		buyPrepayService.del(number, getCompanyid(session));
		return ajaxSuccess();
	}
	
	
}
