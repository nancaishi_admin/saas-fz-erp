package zy.controller.buy;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.form.PageForm;
import zy.service.buy.dealings.BuyDealingsService;
import zy.util.CommonUtil;

@Controller
@RequestMapping("buy/dealings")
public class BuyDealingsController extends BaseController{
	@Resource
	private BuyDealingsService buyDealingsService;
	
	@RequestMapping(value = "to_list", method = RequestMethod.GET)
	public String to_list() {
		return "buy/dealings/list";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = true) String sp_code,
			HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("begindate", pageForm.getBegindate());
        param.put("enddate", pageForm.getEnddate());
        param.put("dl_supply_code", sp_code);
		return ajaxSuccess(buyDealingsService.page(param));
	}
	
}
