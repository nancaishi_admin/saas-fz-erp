package zy.controller.buy;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.approve.T_Approve_Record;
import zy.entity.buy.enter.T_Buy_Enter;
import zy.entity.buy.enter.T_Buy_EnterList;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.buy.enter.EnterService;
import zy.service.report.excel.ReportInterfaceFactory;
import zy.service.report.excel.reporter.BuyEnterReporter;
import zy.util.CommonUtil;
import zy.util.StringUtil;
import zy.vo.buy.BuyVO;

@Controller
@RequestMapping("buy/enter")
public class EnterController extends BaseController{
	@Resource
	private EnterService enterService;
	@Resource
	private ReportInterfaceFactory reportInterfaceFactory;
	
	@RequestMapping(value = "to_list/{et_type}", method = RequestMethod.GET)
	public String to_list(@PathVariable Integer et_type,Model model) {
		model.addAttribute("et_type", et_type);
		return "buy/enter/list";
	}
	@RequestMapping(value = "to_list_warn/{et_type}", method = RequestMethod.GET)
	public String to_list_warn(@PathVariable Integer et_type,Model model) {
		model.addAttribute("et_type", et_type);
		return "buy/enter/list_warn";
	}
	@RequestMapping(value = "to_list_draft_dialog/{et_type}", method = RequestMethod.GET)
	public String to_list_draft_dialog(@PathVariable Integer et_type,Model model) {
		model.addAttribute("et_type", et_type);
		return "buy/enter/list_draft_dialog";
	}
	@RequestMapping(value = "to_list_order_dialog/{et_type}", method = RequestMethod.GET)
	public String to_list_order_dialog(@PathVariable Integer et_type,Model model) {
		model.addAttribute("et_type", et_type);
		return "buy/enter/list_order_dialog";
	}
	@RequestMapping(value = "to_add/{et_type}", method = RequestMethod.GET)
	public String to_add(@PathVariable Integer et_type,Model model) {
		model.addAttribute("et_type", et_type);
		return "buy/enter/add";
	}
	@RequestMapping(value = "/to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer et_id,Model model,HttpSession session) {
		T_Sys_User user = getUser(session);
		T_Buy_Enter enter = enterService.load(et_id);
		if(null != enter){
			enterService.initUpdate(enter.getEt_number(), enter.getEt_type(), user.getUs_id(), user.getCompanyid());
			model.addAttribute("enter",enter);
		}
		return "buy/enter/update";
	}
	@RequestMapping(value = "/to_view", method = RequestMethod.GET)
	public String to_view(@RequestParam Integer et_id,Model model) {
		T_Buy_Enter enter = enterService.load(et_id);
		if(null != enter){
			model.addAttribute("enter",enter);
		}
		return "buy/enter/view";
	}
	@RequestMapping(value = "to_view/{number}", method = RequestMethod.GET)
	public String to_view(@PathVariable String number,Model model,HttpSession session) {
		T_Buy_Enter enter = enterService.load(number,getCompanyid(session));
		if(null != enter){
			model.addAttribute("enter",enter);
		}
		return "buy/enter/view";
	}
	@RequestMapping(value = "to_print_barcode/{number}", method = RequestMethod.GET)
	public String to_print_barcode(@PathVariable String number,Model model) {
		model.addAttribute("number",number);
		return "buy/enter/print_barcode";
	}
	@RequestMapping(value = "to_select_product/{et_type}", method = RequestMethod.GET)
	public String to_select_product(@PathVariable Integer et_type,Model model) {
		model.addAttribute("et_type", et_type);
		return "buy/enter/select_product";
	}
	@RequestMapping(value = "to_temp_update/{et_type}", method = RequestMethod.GET)
	public String to_temp_update(@PathVariable Integer et_type,Model model) {
		model.addAttribute("et_type", et_type);
		return "buy/enter/temp_update";
	}
	@RequestMapping(value = "to_import/{et_type}", method = RequestMethod.GET)
	public String to_import(@PathVariable Integer et_type,Model model) {
		model.addAttribute("et_type", et_type);
		return "buy/enter/import";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = true) Integer et_type,
			@RequestParam(required = true) Integer et_isdraft,
			@RequestParam(required = false) Integer et_ar_state,
			@RequestParam(required = false) String et_supply_code,
			@RequestParam(required = false) String et_depot_code,
			@RequestParam(required = false) String et_manager,
			@RequestParam(required = false) String et_number,
			HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("begindate", pageForm.getBegindate());
        param.put("enddate", pageForm.getEnddate());
        param.put("et_type", et_type);
        param.put("et_isdraft", et_isdraft);
        param.put("et_ar_state", et_ar_state);
        param.put("et_supply_code", et_supply_code);
        param.put("et_depot_code", et_depot_code);
        param.put("et_manager", StringUtil.decodeString(et_manager));
        param.put("et_number", StringUtil.decodeString(et_number));
		return ajaxSuccess(enterService.page(param));
	}
	
	@RequestMapping(value = "report", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public String report(PageForm pageForm,
			@RequestParam(required = true) Integer et_type,
			@RequestParam(required = true) Integer et_isdraft,
			@RequestParam(required = false) Integer et_ar_state,
			@RequestParam(required = false) String et_supply_code,
			@RequestParam(required = false) String et_depot_code,
			@RequestParam(required = false) String et_manager,
			@RequestParam(required = false) String et_number,
			HttpSession session,
			HttpServletRequest request,HttpServletResponse response) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("begindate", pageForm.getBegindate());
        param.put("enddate", pageForm.getEnddate());
        param.put("et_type", et_type);
        param.put("et_isdraft", et_isdraft);
        param.put("et_ar_state", et_ar_state);
        param.put("et_supply_code", et_supply_code);
        param.put("et_depot_code", et_depot_code);
        param.put("et_manager", StringUtil.decodeString(et_manager));
        param.put("et_number", StringUtil.decodeString(et_number));
        
        File file = reportInterfaceFactory.newInstance(BuyEnterReporter.class, param).getFile();
        return download(file, request, response);
	}
	

	@RequestMapping(value = "detail_list/{et_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_list(@PathVariable String et_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("etl_number", et_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(enterService.detail_list(params));
	}
	
	@RequestMapping(value = "print_barcode/{et_number}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object print_barcode(@PathVariable String et_number,HttpServletRequest request,HttpSession session) {
        String sidx = request.getParameter("sidx");
        String sord = request.getParameter("sord");
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.SIDX, sidx);
        param.put(CommonUtil.SORD, sord);
        param.put("etl_number", et_number);
		return ajaxSuccess(enterService.print_barcode(param));
	}
	
	@RequestMapping(value = "detail_sum/{et_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_sum(@PathVariable String et_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("etl_number", et_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(enterService.detail_sum(params));
	}
	
	@RequestMapping(value = "detail_size_title/{et_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_size_title(@PathVariable String et_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("etl_number", et_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(enterService.detail_size_title(params));
	}
	
	@RequestMapping(value = "detail_size/{et_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_size(@PathVariable String et_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("etl_number", et_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(enterService.detail_size(params));
	}
	
	@RequestMapping(value = "temp_list/{et_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_list(@PathVariable Integer et_type,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("etl_type", et_type);
		params.put("etl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(enterService.temp_list(params));
	}
	
	@RequestMapping(value = "temp_sum/{et_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_sum(@PathVariable Integer et_type,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("etl_type", et_type);
		params.put("etl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(enterService.temp_sum(params));
	}
	
	@RequestMapping(value = "temp_size_title/{et_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_size_title(@PathVariable Integer et_type,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("etl_type", et_type);
		params.put("etl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(enterService.temp_size_title(params));
	}
	
	@RequestMapping(value = "temp_size/{et_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_size(@PathVariable Integer et_type,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("etl_type", et_type);
		params.put("etl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(enterService.temp_size(params));
	}
	
	@RequestMapping(value = "temp_save_bybarcode/{et_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_save_bybarcode(@PathVariable Integer et_type,HttpSession session,
			@RequestParam(required = true) String barcode,
			@RequestParam(required = true) Integer amount,
			@RequestParam(required = true) Integer priceType,
			@RequestParam(required = true) Double sp_rate) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("et_type", et_type);
		params.put("barcode", barcode);
		params.put("amount", amount);
		params.put("priceType", priceType);
		params.put("sp_rate", sp_rate);
		params.put("user", user);
		Map<String, Object> resultMap = enterService.temp_save_bybarcode(params);
		return ajaxSuccess(resultMap);
	}
	
	@RequestMapping(value = "page_product", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page_product(PageForm pageForm,@RequestParam(required = true) String et_type,
			@RequestParam(required = false) String alreadyExist,
			@RequestParam(required = false) String exactQuery,
			HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
        param.put("searchContent", pageForm.getSearchContent());
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put("us_id", user.getUs_id());
        param.put("et_type", et_type);
        param.put("alreadyExist", alreadyExist);
        param.put("exactQuery", exactQuery);
		return ajaxSuccess(enterService.page_product(param));
	}
	
	@RequestMapping(value = "temp_loadproduct/{et_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_loadproduct(@PathVariable Integer et_type,HttpSession session,
			@RequestParam(required = true) String pd_code,
			@RequestParam(required = false) String gift,
			@RequestParam(required = false) String exist,
			@RequestParam(required = false) String dp_code,
			@RequestParam(required = false) String priceType,
			@RequestParam(required = false) Double sp_rate) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put(CommonUtil.COMPANYID, user.getCompanyid());
		params.put("et_type", et_type);
		params.put("pd_code", pd_code);
		params.put("etl_pi_type", gift);
		params.put("exist", exist);
		params.put("priceType", priceType);
		params.put("sp_rate", sp_rate);
		params.put("dp_code", dp_code);
		params.put("us_id", user.getUs_id());
		params.put(CommonUtil.KEY_SYSSET, getSysSet(session));
		return ajaxSuccess(enterService.temp_loadproduct(params));
	}
	
	@RequestMapping(value = "temp_save/{et_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_save(@PathVariable Integer et_type,@RequestBody Map<String, Object> data,HttpSession session) {
		T_Sys_User user = getUser(session);
		List<T_Buy_EnterList> temps = BuyVO.convertMap2Model_Enter(data, et_type, user);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("et_type", et_type);
		params.put("temps", temps);
		params.put("user", user);
		params.put("pd_code", data.get("pd_code"));
		params.put("unitPrice", data.get("unitPrice"));
		enterService.temp_save(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_del(@RequestParam Integer etl_id) {
		enterService.temp_del(etl_id);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateAmount", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateAmount(T_Buy_EnterList temp) {
		enterService.temp_updateAmount(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updatePrice/{et_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updatePrice(@PathVariable Integer et_type,T_Buy_EnterList temp,HttpSession session) {
		T_Sys_User user = getUser(session);
		temp.setEtl_us_id(user.getUs_id());
		temp.setCompanyid(user.getCompanyid());
		temp.setEtl_type(et_type);
		enterService.temp_updatePrice(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateRemarkById", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateRemarkById(T_Buy_EnterList temp) {
		temp.setEtl_remark(StringUtil.decodeString(temp.getEtl_remark()));
		enterService.temp_updateRemarkById(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateRemarkByPdCode/{et_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateRemarkByPdCode(@PathVariable Integer et_type,T_Buy_EnterList temp,HttpSession session) {
		T_Sys_User user = getUser(session);
		temp.setEtl_remark(StringUtil.decodeString(temp.getEtl_remark()));
		temp.setEtl_us_id(user.getUs_id());
		temp.setCompanyid(user.getCompanyid());
		temp.setEtl_type(et_type);
		enterService.temp_updateRemarkByPdCode(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_delByPiCode/{et_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_delByPiCode(@PathVariable Integer et_type,
			@RequestParam String pd_code,@RequestParam Integer etl_pi_type,
			@RequestParam(required = false) String cr_code,
			@RequestParam(required = false) String br_code,
			HttpSession session) {
		T_Sys_User user = getUser(session);
		T_Buy_EnterList temp = new T_Buy_EnterList();
		temp.setEtl_pd_code(pd_code);
		temp.setEtl_cr_code(cr_code);
		temp.setEtl_br_code(br_code);
		temp.setEtl_pi_type(etl_pi_type);
		temp.setEtl_type(et_type);
		temp.setEtl_us_id(user.getUs_id());
		temp.setCompanyid(user.getCompanyid());
		enterService.temp_delByPiCode(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_clear/{et_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_clear(@PathVariable Integer et_type, HttpSession session) {
		T_Sys_User user = getUser(session);
		enterService.temp_clear(et_type, user.getUs_id(), user.getCompanyid());
		return ajaxSuccess();
	}

	@RequestMapping(value = "temp_import/{et_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_import(@PathVariable Integer et_type,@RequestBody Map<String, Object> data,HttpSession session) {
		T_Sys_User user = getUser(session);
		List<String[]> datas = BuyVO.convertMap2Model_import(data, user);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("et_type", et_type);
		params.put("datas", datas);
		params.put("user", user);
		params.put("priceType", data.get("priceType"));
		params.put("sp_rate", data.get("sp_rate"));
		enterService.temp_import(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_import_draft/{et_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_import_draft(@PathVariable Integer et_type,@RequestParam String et_number,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("et_type", et_type);
		params.put("et_number", et_number);
		params.put("user", user);
		enterService.temp_import_draft(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_import_order/{et_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_import_order(@PathVariable Integer et_type,@RequestParam String ids,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("et_type", et_type);
		params.put("ids", ids);
		params.put("user", user);
		enterService.temp_import_order(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Buy_Enter enter,HttpSession session) {
		enterService.save(enter, getUser(session));
		return ajaxSuccess(enter);
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Buy_Enter enter,HttpSession session) {
		enterService.update(enter, getUser(session));
		return ajaxSuccess(enter);
	}
	
	@RequestMapping(value = "approve", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object approve(T_Approve_Record record,String number,HttpSession session) {
		record.setAr_describe(StringUtil.decodeString(record.getAr_describe()));
		T_Buy_Enter enter = enterService.approve(number, record, getUser(session),getSysSet(session));
		return ajaxSuccess(enter);
	}
	
	@RequestMapping(value = "reverse", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object reverse(String number,HttpSession session) {
		return ajaxSuccess(enterService.reverse(number, getUser(session),getSysSet(session)));
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(String number,HttpSession session) {
		enterService.del(number, getCompanyid(session));
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "print", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object print(String number,Integer sp_id,Integer displayMode,HttpSession session) {
		Map<String, Object> resultMap = enterService.loadPrintData(number, sp_id, displayMode, getUser(session));
		resultMap.put("user", getUser(session));
		Map<String, Object> temp = BuyVO.buildPrintJson_Enter(resultMap, displayMode);
		return ajaxSuccess(temp);
	}
	
}
