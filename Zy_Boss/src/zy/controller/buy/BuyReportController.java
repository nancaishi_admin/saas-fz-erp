package zy.controller.buy;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sys.user.T_Sys_User;
import zy.form.ProductForm;
import zy.service.buy.report.BuyReportService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("buy/report")
public class BuyReportController extends BaseController{
	@Resource
	private BuyReportService buyReportService;
	
	@RequestMapping(value = "to_report_enter/{type}", method = RequestMethod.GET)
	public String to_report_enter(@PathVariable String type,Model model) {
		model.addAttribute("type", type);
		return "buy/report/report_enter";
	}
	@RequestMapping(value = "to_report_enter_product", method = RequestMethod.GET)
	public String to_report_enter_product() {
		return "buy/report/report_enter_product";
	}
	@RequestMapping(value = "to_report_enter_csb/{type}", method = RequestMethod.GET)
	public String to_report_enter_csb(@PathVariable String type,Model model) {
		model.addAttribute("type", type);
		return "buy/report/report_enter_csb";
	}
	@RequestMapping(value = "to_report_enter_cs", method = RequestMethod.GET)
	public String to_report_enter_cs() {
		return "buy/report/report_enter_cs";
	}
	@RequestMapping(value = "to_report_enter_detail", method = RequestMethod.GET)
	public String to_report_enter_detail() {
		return "buy/report/report_enter_detail";
	}
	@RequestMapping(value = "to_enter_details", method = RequestMethod.GET)
	public String to_enter_details() {
		return "buy/report/enter_details";
	}
	@RequestMapping(value = "to_rank_enter", method = RequestMethod.GET)
	public String to_rank_enter() {
		return "buy/report/rank_enter";
	}
	@RequestMapping(value = "to_summary_enter", method = RequestMethod.GET)
	public String to_summary_enter() {
		return "buy/report/summary_enter";
	}
	/**
	 * 类别分级采购报表
	 */
	@RequestMapping(value = "to_type_level_enter/{tp_upcode}", method = RequestMethod.GET)
	public String to_type_level_enter(@PathVariable String tp_upcode,Model model) {
		model.addAttribute("tp_upcode", tp_upcode);
		return "buy/report/type_level_enter";
	}
	
	
	@RequestMapping(value = "pageEnterReport", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object pageEnterReport(ProductForm productForm,
			@RequestParam(required = true) String type,
			@RequestParam(required = false) String et_supply_code,
			@RequestParam(required = false) String et_depot_code,
			@RequestParam(required = false) String et_manager,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put(CommonUtil.PAGESIZE, productForm.getRows());
        params.put(CommonUtil.PAGEINDEX, productForm.getPage());
        params.put(CommonUtil.SIDX, productForm.getSidx());
        params.put(CommonUtil.SORD, productForm.getSord());
        params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        params.put("type", type);
        params.put("et_supply_code", et_supply_code);
        params.put("et_depot_code", et_depot_code);
        params.put("et_manager", StringUtil.decodeString(et_manager));
        params.put("begindate", productForm.getBegindate());
        params.put("enddate", productForm.getEnddate());
        params.put("bd_code", productForm.getBd_code());
        params.put("tp_code", productForm.getTp_code());
        params.put("pd_season", productForm.getPd_season());
        params.put("pd_year", productForm.getPd_year());
        params.put("pd_code", productForm.getPd_code());
		return ajaxSuccess(buyReportService.pageEnterReport(params));
	}
	
	@RequestMapping(value = "listEnterReport_csb", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listEnterReport_csb(ProductForm productForm,
			@RequestParam(required = true) String type,
			@RequestParam(required = false) String et_supply_code,
			@RequestParam(required = false) String et_depot_code,
			@RequestParam(required = false) String et_manager,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put(CommonUtil.PAGESIZE, productForm.getRows());
        params.put(CommonUtil.PAGEINDEX, productForm.getPage());
        params.put(CommonUtil.SIDX, productForm.getSidx());
        params.put(CommonUtil.SORD, productForm.getSord());
        params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        params.put("type", type);
        params.put("et_supply_code", et_supply_code);
        params.put("et_depot_code", et_depot_code);
        params.put("et_manager", StringUtil.decodeString(et_manager));
        params.put("begindate", productForm.getBegindate());
        params.put("enddate", productForm.getEnddate());
        params.put("bd_code", productForm.getBd_code());
        params.put("tp_code", productForm.getTp_code());
        params.put("pd_season", productForm.getPd_season());
        params.put("pd_year", productForm.getPd_year());
        params.put("pd_code", productForm.getPd_code());
        params.put("cr_code", productForm.getCr_code());
        params.put("sz_code", productForm.getSz_code());
        params.put("br_code", productForm.getBr_code());
		return ajaxSuccess(buyReportService.listEnterReport_csb(params));
	}
	
	@RequestMapping(value = "pageEnterDetailReport", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object pageEnterDetailReport(ProductForm productForm,
			@RequestParam(required = false) String et_supply_code,
			@RequestParam(required = false) String et_depot_code,
			@RequestParam(required = false) String et_manager,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put(CommonUtil.PAGESIZE, productForm.getRows());
        params.put(CommonUtil.PAGEINDEX, productForm.getPage());
        params.put(CommonUtil.SIDX, productForm.getSidx());
        params.put(CommonUtil.SORD, productForm.getSord());
        params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        params.put("et_supply_code", et_supply_code);
        params.put("et_depot_code", et_depot_code);
        params.put("et_manager", StringUtil.decodeString(et_manager));
        params.put("begindate", productForm.getBegindate());
        params.put("enddate", productForm.getEnddate());
        params.put("bd_code", productForm.getBd_code());
        params.put("tp_code", productForm.getTp_code());
        params.put("pd_season", productForm.getPd_season());
        params.put("pd_year", productForm.getPd_year());
        params.put("pd_code", productForm.getPd_code());
		return ajaxSuccess(buyReportService.pageEnterDetailReport(params));
	}
	
	@RequestMapping(value = "loadEnterDetailSizeReport", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object loadEnterDetailSizeReport(ProductForm productForm,
			@RequestParam(required = false) String et_supply_code,
			@RequestParam(required = false) String et_depot_code,
			@RequestParam(required = false) String et_manager,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put(CommonUtil.PAGESIZE, productForm.getRows());
        params.put(CommonUtil.PAGEINDEX, productForm.getPage());
        params.put(CommonUtil.SIDX, productForm.getSidx());
        params.put(CommonUtil.SORD, productForm.getSord());
        params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        params.put("pd_code", productForm.getPd_code());
        params.put("begindate", productForm.getBegindate());
        params.put("enddate", productForm.getEnddate());
        params.put("et_supply_code", et_supply_code);
        params.put("et_depot_code", et_depot_code);
        params.put("et_manager", StringUtil.decodeString(et_manager));
		return ajaxSuccess(buyReportService.loadEnterDetailSizeReport(params));
	}
	
	@RequestMapping(value = "pageEnterDetails", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object pageEnterDetails(ProductForm productForm,
			@RequestParam(required = false) String et_supply_code,
			@RequestParam(required = false) String et_depot_code,
			@RequestParam(required = false) String et_manager,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put(CommonUtil.PAGESIZE, productForm.getRows());
        params.put(CommonUtil.PAGEINDEX, productForm.getPage());
        params.put(CommonUtil.SIDX, productForm.getSidx());
        params.put(CommonUtil.SORD, productForm.getSord());
        params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        params.put("et_supply_code", et_supply_code);
        params.put("et_depot_code", et_depot_code);
        params.put("et_manager", StringUtil.decodeString(et_manager));
        params.put("begindate", productForm.getBegindate());
        params.put("enddate", productForm.getEnddate());
        params.put("bd_code", productForm.getBd_code());
        params.put("tp_code", productForm.getTp_code());
        params.put("pd_season", StringUtil.decodeString(productForm.getPd_season()));
        params.put("pd_year", productForm.getPd_year());
        params.put("pd_code", productForm.getPd_code());
		return ajaxSuccess(buyReportService.pageEnterDetails(params));
	}
	
	@RequestMapping(value = "pageEnterRank", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object pageEnterRank(ProductForm productForm,
			@RequestParam(required = true) String type,
			@RequestParam(required = false) String et_supply_code,
			@RequestParam(required = false) String et_depot_code,
			@RequestParam(required = false) String et_manager,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put(CommonUtil.PAGESIZE, productForm.getRows());
        params.put(CommonUtil.PAGEINDEX, productForm.getPage());
        params.put(CommonUtil.SIDX, productForm.getSidx());
        params.put(CommonUtil.SORD, productForm.getSord());
        params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        params.put("type", type);
        params.put("et_supply_code", et_supply_code);
        params.put("et_depot_code", et_depot_code);
        params.put("et_manager", StringUtil.decodeString(et_manager));
        params.put("begindate", productForm.getBegindate());
        params.put("enddate", productForm.getEnddate());
        params.put("bd_code", productForm.getBd_code());
        params.put("tp_code", productForm.getTp_code());
        params.put("pd_season", productForm.getPd_season());
        params.put("pd_year", productForm.getPd_year());
        params.put("pd_code", productForm.getPd_code());
		return ajaxSuccess(buyReportService.pageEnterRank(params));
	}
	
	@RequestMapping(value = "type_level_enter", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object type_level_enter(ProductForm productForm,
			@RequestParam(required = true) String tp_upcode,
			@RequestParam(required = true) String year,
			@RequestParam(required = false) String et_supply_code,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put("bd_code", productForm.getBd_code());
        param.put("pd_season", productForm.getPd_season());
        param.put("tp_upcode", tp_upcode);
        param.put("year", year);
        param.put("et_supply_code", et_supply_code);
		return ajaxSuccess(buyReportService.type_level_enter(param));
	}
	
}
