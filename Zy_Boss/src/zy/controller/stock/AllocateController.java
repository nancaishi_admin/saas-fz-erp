package zy.controller.stock;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.approve.T_Approve_Record;
import zy.entity.stock.allocate.T_Stock_Allocate;
import zy.entity.stock.allocate.T_Stock_AllocateList;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.stock.allocate.AllocateService;
import zy.util.CommonUtil;
import zy.util.StringUtil;
import zy.vo.stock.AllocateVO;

@Controller
@RequestMapping("stock/allocate")
public class AllocateController extends BaseController{
	
	@Resource
	private AllocateService allocateService;

	@RequestMapping(value = "to_list", method = RequestMethod.GET)
	public String to_list() {
		return "stock/allocate/list";
	}
	@RequestMapping(value = "to_list_draft_dialog", method = RequestMethod.GET)
	public String to_list_draft_dialog() {
		return "stock/allocate/list_draft_dialog";
	}
	@RequestMapping(value = "to_add", method = RequestMethod.GET)
	public String to_add() {
		return "stock/allocate/add";
	}
	@RequestMapping(value = "/to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer ac_id,Model model,HttpSession session) {
		T_Sys_User user = getUser(session);
		T_Stock_Allocate allocate = allocateService.load(ac_id);
		if(null != allocate){
			allocateService.initUpdate(allocate.getAc_number(), user.getUs_id(), user.getCompanyid());
			model.addAttribute("allocate",allocate);
		}
		return "stock/allocate/update";
	}
	@RequestMapping(value = "/to_view", method = RequestMethod.GET)
	public String to_view(@RequestParam Integer ac_id,Model model) {
		T_Stock_Allocate allocate = allocateService.load(ac_id);
		if(null != allocate){
			model.addAttribute("allocate",allocate);
		}
		return "stock/allocate/view";
	}
	@RequestMapping(value = "to_select_product", method = RequestMethod.GET)
	public String to_select_product() {
		return "stock/allocate/select_product";
	}
	@RequestMapping(value = "to_temp_update", method = RequestMethod.GET)
	public String to_temp_update() {
		return "stock/allocate/temp_update";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = true) Integer ac_isdraft,
			@RequestParam(required = false) Integer ac_ar_state,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String ac_outdp_code,
			@RequestParam(required = false) String ac_indp_code,
			@RequestParam(required = false) String ac_manager,
			@RequestParam(required = false) String ac_number,
			HttpServletRequest request,HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("ac_isdraft", ac_isdraft);
        param.put("ac_ar_state", ac_ar_state);
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("ac_outdp_code", ac_outdp_code);
        param.put("ac_indp_code", ac_indp_code);
        param.put("ac_manager", StringUtil.decodeString(ac_manager));
        param.put("ac_number", StringUtil.decodeString(ac_number));
		return ajaxSuccess(allocateService.page(param));
	}
	
	@RequestMapping(value = "detail_list/{ac_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_list(@PathVariable String ac_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("acl_number", ac_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(allocateService.detail_list(params));
	}
	
	@RequestMapping(value = "detail_sum/{ac_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_sum(@PathVariable String ac_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("acl_number", ac_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(allocateService.detail_sum(params));
	}
	
	@RequestMapping(value = "detail_size_title/{ac_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_size_title(@PathVariable String ac_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("acl_number", ac_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(allocateService.detail_size_title(params));
	}
	
	@RequestMapping(value = "detail_size/{ac_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_size(@PathVariable String ac_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("acl_number", ac_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(allocateService.detail_size(params));
	}
	
	@RequestMapping(value = "temp_list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_list(HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("acl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(allocateService.temp_list(params));
	}
	
	@RequestMapping(value = "temp_sum", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_sum(HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("acl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(allocateService.temp_sum(params));
	}
	
	@RequestMapping(value = "temp_size_title", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_size_title(HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("acl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(allocateService.temp_size_title(params));
	}
	
	@RequestMapping(value = "temp_size", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_size(HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("acl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(allocateService.temp_size(params));
	}
	
	@RequestMapping(value = "temp_save_bybarcode", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_save_bybarcode(HttpSession session,
			@RequestParam(required = true) String barcode,
			@RequestParam(required = true) Integer amount) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("barcode", barcode);
		params.put("amount", amount);
		params.put("user", user);
		Map<String, Object> resultMap = allocateService.temp_save_bybarcode(params);
		return ajaxSuccess(resultMap);
	}
	
	@RequestMapping(value = "page_product", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page_product(PageForm pageForm,@RequestParam(required = false) String alreadyExist,
			@RequestParam(required = false) String exactQuery,
			HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
        param.put("searchContent", pageForm.getSearchContent());
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put("us_id", user.getUs_id());
        param.put("alreadyExist", alreadyExist);
        param.put("exactQuery", exactQuery);
		return ajaxSuccess(allocateService.page_product(param));
	}
	
	@RequestMapping(value = "temp_loadproduct", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_loadproduct(HttpSession session,
			@RequestParam(required = true) String pd_code,
			@RequestParam(required = false) String exist,
			@RequestParam(required = false) String dp_code) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put(CommonUtil.COMPANYID, user.getCompanyid());
		params.put("pd_code", pd_code);
		params.put("exist", exist);
		params.put("dp_code", dp_code);
		params.put("us_id", user.getUs_id());
		params.put(CommonUtil.KEY_SYSSET, getSysSet(session));
		return ajaxSuccess(allocateService.temp_loadproduct(params));
	}
	
	@RequestMapping(value = "temp_save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_save(@RequestBody Map<String, Object> data,HttpSession session) {
		T_Sys_User user = getUser(session);
		List<T_Stock_AllocateList> temps = AllocateVO.convertMap2Model(data, user);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("temps", temps);
		params.put("user", user);
		params.put("pd_code", data.get("pd_code"));
		params.put("unitPrice", data.get("unitPrice"));
		allocateService.temp_save(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_del(@RequestParam Integer acl_id) {
		allocateService.temp_del(acl_id);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateAmount", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateAmount(T_Stock_AllocateList temp) {
		allocateService.temp_updateAmount(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updatePrice", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updatePrice(T_Stock_AllocateList temp,HttpSession session) {
		T_Sys_User user = getUser(session);
		temp.setAcl_us_id(user.getUs_id());
		temp.setCompanyid(user.getCompanyid());
		allocateService.temp_updatePrice(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateRemarkById", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateRemarkById(T_Stock_AllocateList temp) {
		temp.setAcl_remark(StringUtil.decodeString(temp.getAcl_remark()));
		allocateService.temp_updateRemarkById(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateRemarkByPdCode", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateRemarkByPdCode(T_Stock_AllocateList temp,HttpSession session) {
		T_Sys_User user = getUser(session);
		temp.setAcl_remark(StringUtil.decodeString(temp.getAcl_remark()));
		temp.setAcl_us_id(user.getUs_id());
		temp.setCompanyid(user.getCompanyid());
		allocateService.temp_updateRemarkByPdCode(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_delByPiCode", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_delByPiCode(@RequestParam String pd_code,
			@RequestParam(required = false) String cr_code,
			@RequestParam(required = false) String br_code,
			HttpSession session) {
		T_Sys_User user = getUser(session);
		T_Stock_AllocateList temp = new T_Stock_AllocateList();
		temp.setAcl_pd_code(pd_code);
		temp.setAcl_cr_code(cr_code);
		temp.setAcl_br_code(br_code);
		temp.setAcl_us_id(user.getUs_id());
		temp.setCompanyid(user.getCompanyid());
		allocateService.temp_delByPiCode(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_clear", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_clear(HttpSession session) {
		T_Sys_User user = getUser(session);
		allocateService.temp_clear(user.getUs_id(), user.getCompanyid());
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_import", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_import(@RequestBody Map<String, Object> data,HttpSession session) {
		T_Sys_User user = getUser(session);
		List<String[]> datas = AllocateVO.convertMap2Model_import(data);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("datas", datas);
		params.put("user", user);
		allocateService.temp_import(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_import_draft", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_import_draft(@RequestParam String ac_number,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("ac_number", ac_number);
		params.put("user", user);
		allocateService.temp_import_draft(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Stock_Allocate allocate,HttpSession session) {
		allocateService.save(allocate, getUser(session));
		return ajaxSuccess(allocate);
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Stock_Allocate allocate,HttpSession session) {
		allocateService.update(allocate, getUser(session));
		return ajaxSuccess(allocate);
	}

	@RequestMapping(value = "approve", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object approve(T_Approve_Record record,String number,HttpSession session) {
		record.setAr_describe(StringUtil.decodeString(record.getAr_describe()));
		T_Stock_Allocate allocate = allocateService.approve(number, record, getUser(session));
		return ajaxSuccess(allocate);
	}
	
	@RequestMapping(value = "reverse", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object reverse(String number,HttpSession session) {
		return ajaxSuccess(allocateService.reverse(number, getUser(session)));
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(String number,HttpSession session) {
		allocateService.del(number, getCompanyid(session));
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "print", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object print(String number,Integer sp_id,Integer displayMode,HttpSession session) {
		Map<String, Object> resultMap = allocateService.loadPrintData(number, sp_id, displayMode, getUser(session));
		resultMap.put("user", getUser(session));
		Map<String, Object> temp = AllocateVO.buildPrintJson(resultMap, displayMode);
		return ajaxSuccess(temp);
	}
	
}
