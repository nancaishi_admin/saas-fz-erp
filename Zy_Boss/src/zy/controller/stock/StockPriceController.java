package zy.controller.stock;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;

import zy.controller.BaseController;
import zy.entity.approve.T_Approve_Record;
import zy.entity.stock.price.T_Stock_Price;
import zy.entity.stock.price.T_Stock_PriceList;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.stock.price.StockPriceService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("stock/price")
public class StockPriceController extends BaseController{
	@Resource
	private StockPriceService stockPriceService;
	
	@RequestMapping(value = "to_list", method = RequestMethod.GET)
	public String to_list() {
		return "stock/price/list";
	}
	@RequestMapping(value = "to_list_draft_dialog", method = RequestMethod.GET)
	public String to_list_draft_dialog() {
		return "stock/price/list_draft_dialog";
	}
	@RequestMapping(value = "to_add", method = RequestMethod.GET)
	public String to_add() {
		return "stock/price/add";
	}
	@RequestMapping(value = "/to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer pc_id,Model model,HttpSession session) {
		T_Sys_User user = getUser(session);
		T_Stock_Price price = stockPriceService.load(pc_id);
		if(null != price){
			stockPriceService.initUpdate(price.getPc_number(), user.getUs_id(), user.getCompanyid());
			model.addAttribute("price",price);
		}
		return "stock/price/update";
	}
	@RequestMapping(value = "/to_view", method = RequestMethod.GET)
	public String to_view(@RequestParam Integer pc_id,Model model) {
		T_Stock_Price price = stockPriceService.load(pc_id);
		if(null != price){
			model.addAttribute("price",price);
		}
		return "stock/price/view";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = true) Integer pc_isdraft,
			@RequestParam(required = false) Integer pc_ar_state,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String pc_manager,
			@RequestParam(required = false) String pc_number,
			HttpServletRequest request,HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("pc_isdraft", pc_isdraft);
        param.put("pc_ar_state", pc_ar_state);
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("pc_manager", StringUtil.decodeString(pc_manager));
        param.put("pc_number", StringUtil.decodeString(pc_number));
		return ajaxSuccess(stockPriceService.page(param));
	}
	
	@RequestMapping(value = "detail_list/{pc_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_list(@PathVariable String pc_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("pcl_number", pc_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(stockPriceService.detail_list(params));
	}
	
	@RequestMapping(value = "temp_list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_list(HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("pcl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(stockPriceService.temp_list(params));
	}
	
	@RequestMapping(value = "temp_save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_save(@RequestParam(required = true) String products,HttpSession session) {
		T_Sys_User user = getUser(session);
		List<T_Stock_PriceList> temps = JSON.parseArray(products, T_Stock_PriceList.class);
		stockPriceService.temp_save(temps, user);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_update(T_Stock_PriceList temp) {
		stockPriceService.temp_update(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_del(@RequestParam Integer pcl_id) {
		stockPriceService.temp_del(pcl_id);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_clear", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_clear(HttpSession session) {
		T_Sys_User user = getUser(session);
		stockPriceService.temp_clear(user.getUs_id(), user.getCompanyid());
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_import_draft", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_import_draft(@RequestParam String pc_number,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("pc_number", pc_number);
		params.put("user", user);
		stockPriceService.temp_import_draft(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Stock_Price price,HttpSession session) {
		stockPriceService.save(price, getUser(session));
		return ajaxSuccess(price);
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Stock_Price price,HttpSession session) {
		stockPriceService.update(price, getUser(session));
		return ajaxSuccess(price);
	}
	
	@RequestMapping(value = "approve", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object approve(T_Approve_Record record,String number,HttpSession session) {
		record.setAr_describe(StringUtil.decodeString(record.getAr_describe()));
		T_Stock_Price price = stockPriceService.approve(number, record, getUser(session));
		return ajaxSuccess(price);
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(String number,HttpSession session) {
		stockPriceService.del(number, getCompanyid(session));
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "print", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object print(String number,HttpSession session) {
		return ajaxSuccess(stockPriceService.print(number, getUser(session)));
	}
	
}
