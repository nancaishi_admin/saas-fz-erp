package zy.controller.stock;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.approve.T_Approve_Record;
import zy.entity.stock.overflow.T_Stock_Overflow;
import zy.entity.stock.overflow.T_Stock_OverflowList;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.stock.overflow.OverflowService;
import zy.util.CommonUtil;
import zy.util.StringUtil;
import zy.vo.stock.OverflowVO;

@Controller
@RequestMapping("stock/overflow")
public class OverflowController extends BaseController{
	
	@Resource
	private OverflowService overflowService;
	
	@RequestMapping(value = "to_list", method = RequestMethod.GET)
	public String to_list() {
		return "stock/overflow/list";
	}
	@RequestMapping(value = "to_list_draft_dialog", method = RequestMethod.GET)
	public String to_list_draft_dialog() {
		return "stock/overflow/list_draft_dialog";
	}
	@RequestMapping(value = "to_add", method = RequestMethod.GET)
	public String to_add() {
		return "stock/overflow/add";
	}
	@RequestMapping(value = "to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer of_id,Model model,HttpSession session) {
		T_Sys_User user = getUser(session);
		T_Stock_Overflow overflow = overflowService.load(of_id);
		if(null != overflow){
			overflowService.initUpdate(overflow.getOf_number(), user.getUs_id(), user.getCompanyid());
			model.addAttribute("overflow",overflow);
		}
		return "stock/overflow/update";
	}
	@RequestMapping(value = "to_view", method = RequestMethod.GET)
	public String to_view(@RequestParam Integer of_id,Model model) {
		T_Stock_Overflow overflow = overflowService.load(of_id);
		if(null != overflow){
			model.addAttribute("overflow",overflow);
		}
		return "stock/overflow/view";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = true) Integer of_isdraft,
			@RequestParam(required = false) Integer of_ar_state,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String of_dp_code,
			@RequestParam(required = false) String of_manager,
			@RequestParam(required = false) String of_number,
			HttpServletRequest request,HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("of_isdraft", of_isdraft);
        param.put("of_ar_state", of_ar_state);
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("of_dp_code", of_dp_code);
        param.put("of_manager", StringUtil.decodeString(of_manager));
        param.put("of_number", StringUtil.decodeString(of_number));
		return ajaxSuccess(overflowService.page(param));
	}
	
	@RequestMapping(value = "detail_list/{of_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_list(@PathVariable String of_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("ofl_number", of_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(overflowService.detail_list(params));
	}
	
	@RequestMapping(value = "detail_sum/{of_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_sum(@PathVariable String of_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("ofl_number", of_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(overflowService.detail_sum(params));
	}
	
	@RequestMapping(value = "detail_size_title/{of_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_size_title(@PathVariable String of_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("ofl_number", of_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(overflowService.detail_size_title(params));
	}
	
	@RequestMapping(value = "detail_size/{of_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_size(@PathVariable String of_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("ofl_number", of_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(overflowService.detail_size(params));
	}
	
	@RequestMapping(value = "temp_list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_list(HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("ofl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(overflowService.temp_list(params));
	}
	
	@RequestMapping(value = "temp_sum", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_sum(HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("ofl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(overflowService.temp_sum(params));
	}
	
	@RequestMapping(value = "temp_size_title", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_size_title(HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("ofl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(overflowService.temp_size_title(params));
	}
	
	@RequestMapping(value = "temp_size", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_size(HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("ofl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(overflowService.temp_size(params));
	}
	
	@RequestMapping(value = "temp_save_bybarcode", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_save_bybarcode(HttpSession session,
			@RequestParam(required = true) String barcode,
			@RequestParam(required = true) Integer amount) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("barcode", barcode);
		params.put("amount", amount);
		params.put("user", user);
		Map<String, Object> resultMap = overflowService.temp_save_bybarcode(params);
		return ajaxSuccess(resultMap);
	}
	
	@RequestMapping(value = "page_product", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page_product(PageForm pageForm,@RequestParam(required = false) String alreadyExist,
			@RequestParam(required = false) String exactQuery,
			HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
        param.put("searchContent", pageForm.getSearchContent());
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put("us_id", user.getUs_id());
        param.put("alreadyExist", alreadyExist);
        param.put("exactQuery", exactQuery);
		return ajaxSuccess(overflowService.page_product(param));
	}
	
	@RequestMapping(value = "temp_loadproduct", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_loadproduct(HttpSession session,
			@RequestParam(required = true) String pd_code,
			@RequestParam(required = false) String exist,
			@RequestParam(required = false) String dp_code) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put(CommonUtil.COMPANYID, user.getCompanyid());
		params.put("pd_code", pd_code);
		params.put("exist", exist);
		params.put("dp_code", dp_code);
		params.put("us_id", user.getUs_id());
		params.put(CommonUtil.KEY_SYSSET, getSysSet(session));
		return ajaxSuccess(overflowService.temp_loadproduct(params));
	}
	
	@RequestMapping(value = "temp_save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_save(@RequestBody Map<String, Object> data,HttpSession session) {
		T_Sys_User user = getUser(session);
		List<T_Stock_OverflowList> temps = OverflowVO.convertMap2Model(data, user);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("temps", temps);
		params.put("user", user);
		overflowService.temp_save(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_del(@RequestParam Integer ofl_id) {
		overflowService.temp_del(ofl_id);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateAmount", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateAmount(T_Stock_OverflowList temp) {
		overflowService.temp_updateAmount(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateRemarkById", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateRemarkById(T_Stock_OverflowList temp) {
		temp.setOfl_remark(StringUtil.decodeString(temp.getOfl_remark()));
		overflowService.temp_updateRemarkById(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateRemarkByPdCode", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateRemarkByPdCode(T_Stock_OverflowList temp,HttpSession session) {
		T_Sys_User user = getUser(session);
		temp.setOfl_remark(StringUtil.decodeString(temp.getOfl_remark()));
		temp.setOfl_us_id(user.getUs_id());
		temp.setCompanyid(user.getCompanyid());
		overflowService.temp_updateRemarkByPdCode(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_delByPiCode", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_delByPiCode(@RequestParam String pd_code,
			@RequestParam(required = false) String cr_code,
			@RequestParam(required = false) String br_code,
			HttpSession session) {
		T_Sys_User user = getUser(session);
		T_Stock_OverflowList temp = new T_Stock_OverflowList();
		temp.setOfl_pd_code(pd_code);
		temp.setOfl_cr_code(cr_code);
		temp.setOfl_br_code(br_code);
		temp.setOfl_us_id(user.getUs_id());
		temp.setCompanyid(user.getCompanyid());
		overflowService.temp_delByPiCode(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_clear", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_clear(HttpSession session) {
		T_Sys_User user = getUser(session);
		overflowService.temp_clear(user.getUs_id(), user.getCompanyid());
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_import", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_import(@RequestBody Map<String, Object> data,HttpSession session) {
		T_Sys_User user = getUser(session);
		List<String[]> datas = OverflowVO.convertMap2Model_import(data);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("datas", datas);
		params.put("user", user);
		overflowService.temp_import(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_import_draft", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_import_draft(@RequestParam String of_number,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("of_number", of_number);
		params.put("user", user);
		overflowService.temp_import_draft(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Stock_Overflow overflow,HttpSession session) {
		overflowService.save(overflow, getUser(session));
		return ajaxSuccess(overflow);
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Stock_Overflow overflow,HttpSession session) {
		overflowService.update(overflow, getUser(session));
		return ajaxSuccess(overflow);
	}
	
	@RequestMapping(value = "approve", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object approve(T_Approve_Record record,String number,HttpSession session) {
		record.setAr_describe(StringUtil.decodeString(record.getAr_describe()));
		T_Stock_Overflow overflow = overflowService.approve(number, record, getUser(session));
		return ajaxSuccess(overflow);
	}
	
	@RequestMapping(value = "reverse", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object reverse(String number,HttpSession session) {
		return ajaxSuccess(overflowService.reverse(number, getUser(session)));
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(String number,HttpSession session) {
		overflowService.del(number, getCompanyid(session));
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "print", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object print(String number,Integer sp_id,Integer displayMode,HttpSession session) {
		Map<String, Object> resultMap = overflowService.loadPrintData(number, sp_id, displayMode, getUser(session));
		resultMap.put("user", getUser(session));
		Map<String, Object> temp = OverflowVO.buildPrintJson(resultMap, displayMode);
		return ajaxSuccess(temp);
	}
	
}
