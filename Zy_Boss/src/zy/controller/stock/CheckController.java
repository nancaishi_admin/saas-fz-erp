package zy.controller.stock;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.approve.T_Approve_Record;
import zy.entity.stock.check.T_Stock_Batch;
import zy.entity.stock.check.T_Stock_Check;
import zy.entity.stock.check.T_Stock_CheckList;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.stock.check.CheckService;
import zy.util.CommonUtil;
import zy.util.StringUtil;
import zy.vo.stock.CheckVO;

@Controller
@RequestMapping("stock/check")
public class CheckController extends BaseController{
	@Resource
	private CheckService checkService;
	
	@RequestMapping(value = "to_batch_list", method = RequestMethod.GET)
	public String to_list() {
		return "stock/check/batch_list";
	}
	@RequestMapping(value = "to_batch_add", method = RequestMethod.GET)
	public String to_batch_add() {
		return "stock/check/batch_add";
	}
	@RequestMapping(value = "/to_batch_view", method = RequestMethod.GET)
	public String to_batch_view(@RequestParam Integer ba_id,Model model) {
		T_Stock_Batch batch = checkService.batch_load(ba_id);
		if(null != batch){
			model.addAttribute("batch",batch);
		}
		return "stock/check/batch_view";
	}
	
	@RequestMapping(value = "to_check_add", method = RequestMethod.GET)
	public String to_check_add(@RequestParam Integer ba_id,Model model) {
		T_Stock_Batch batch = checkService.batch_load(ba_id);
		if(null != batch){
			model.addAttribute("batch",batch);
		}
		return "stock/check/check_add";
	}
	@RequestMapping(value = "to_check_addrepair", method = RequestMethod.GET)
	public String to_check_addrepair(@RequestParam Integer ba_id,Model model) {
		T_Stock_Batch batch = checkService.batch_load(ba_id);
		if(null != batch){
			model.addAttribute("batch",batch);
		}
		return "stock/check/check_addrepair";
	}
	@RequestMapping(value = "to_select_product", method = RequestMethod.GET)
	public String to_select_product() {
		return "stock/check/select_product";
	}
	@RequestMapping(value = "to_check_temp_update", method = RequestMethod.GET)
	public String to_temp_update() {
		return "stock/check/check_temp_update";
	}
	@RequestMapping(value = "to_check_view", method = RequestMethod.GET)
	public String to_check_view(@RequestParam Integer ba_id,@RequestParam String ck_number,Model model,HttpSession session) {
		T_Stock_Batch batch = checkService.batch_load(ba_id);
		if(null != batch){
			model.addAttribute("batch",batch);
			model.addAttribute("check",checkService.check_load(ck_number, getCompanyid(session)));
		}
		return "stock/check/check_view";
	}
	@RequestMapping(value = "to_check_view_all", method = RequestMethod.GET)
	public String to_check_view_all(@RequestParam Integer ba_id,Model model) {
		T_Stock_Batch batch = checkService.batch_load(ba_id);
		if(null != batch){
			model.addAttribute("batch",batch);
		}
		return "stock/check/check_view_all";
	}
	@RequestMapping(value = "to_check_exec", method = RequestMethod.GET)
	public String to_check_exec(@RequestParam Integer ba_id,Model model,HttpSession session) {
		T_Stock_Batch batch = checkService.batch_load(ba_id);
		if(null != batch){
			model.addAttribute("batch",batch);
		}
		return "stock/check/check_exec";
	}
	@RequestMapping(value = "to_check_missing", method = RequestMethod.GET)
	public String to_check_missing(T_Stock_Batch batch,Model model) {
		model.addAttribute("batch",batch);
		return "stock/check/check_missing";
	}
	
	@RequestMapping(value = "to_check_diff", method = RequestMethod.GET)
	public String to_check_diff() {
		return "stock/check/check_diff";
	}
	
	@RequestMapping(value = "batch_page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object batch_page(PageForm pageForm,
			@RequestParam(required = false) Integer ba_ar_state,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String ba_dp_code,
			@RequestParam(required = false) String ba_manager,
			@RequestParam(required = false) String ba_number,
			HttpServletRequest request,HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("ba_ar_state", ba_ar_state);
        param.put("begindate", begindate);
        param.put("enddate", StringUtil.isEmpty(enddate)?"":enddate+" 23:59:59");
        param.put("ba_dp_code", ba_dp_code);
        param.put("ba_manager", StringUtil.decodeString(ba_manager));
        param.put("ba_number", StringUtil.decodeString(ba_number));
        param.put("us_id", getUser(session).getUs_id());
		return ajaxSuccess(checkService.batch_page(param));
	}
	
	@RequestMapping(value = "batch_save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object batch_save(T_Stock_Batch batch,HttpSession session) {
		checkService.batch_save(batch, getUser(session));
		return ajaxSuccess(batch);
	}
	
	@RequestMapping(value = "batch_approve", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object batch_approve(T_Approve_Record record,String number,HttpSession session) {
		record.setAr_describe(StringUtil.decodeString(record.getAr_describe()));
		T_Stock_Batch batch = checkService.batch_approve(number, record, getUser(session));
		return ajaxSuccess(batch);
	}
	
	@RequestMapping(value = "batch_delete", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object batch_delete(String number,HttpSession session) {
		checkService.batch_delete(number, getCompanyid(session));
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "check_list4exec/{ba_number}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object check_list4exec(@PathVariable String ba_number,HttpSession session) {
		return ajaxSuccess(checkService.check_list4exec(ba_number,getCompanyid(session)));
	}
	
	@RequestMapping(value = "check_detail_list_all/{ba_number}/{ba_isexec}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object check_detail_list_all(@PathVariable String ba_number,@PathVariable Integer ba_isexec,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("ckl_ba_number", ba_number);
		params.put("ba_isexec", ba_isexec);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(checkService.check_detail_list_all(params));
	}
	
	@RequestMapping(value = "check_detail_sum_all/{ba_number}/{ba_isexec}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object check_detail_sum_all(@PathVariable String ba_number,@PathVariable Integer ba_isexec,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("ckl_ba_number", ba_number);
		params.put("ba_isexec", ba_isexec);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(checkService.check_detail_sum_all(params));
	}
	
	@RequestMapping(value = "check_detail_size_title_all/{ba_number}/{ba_isexec}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object check_detail_size_title_all(@PathVariable String ba_number,@PathVariable Integer ba_isexec,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("ckl_ba_number", ba_number);
		params.put("ba_isexec", ba_isexec);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(checkService.check_detail_size_title_all(params));
	}
	
	@RequestMapping(value = "check_detail_size_all/{ba_number}/{ba_isexec}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object check_detail_size_all(@PathVariable String ba_number,@PathVariable Integer ba_isexec,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("ckl_ba_number", ba_number);
		params.put("ba_isexec", ba_isexec);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(checkService.check_detail_size_all(params));
	}
	
	@RequestMapping(value = "check_detail_list/{ck_number}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object check_detail_list(@PathVariable String ck_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("ckl_number", ck_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(checkService.check_detail_list(params));
	}
	
	@RequestMapping(value = "check_detail_sum/{ck_number}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object check_detail_sum(@PathVariable String ck_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("ckl_number", ck_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(checkService.check_detail_sum(params));
	}
	
	@RequestMapping(value = "check_detail_size_title/{ck_number}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object check_detail_size_title(@PathVariable String ck_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("ckl_number", ck_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(checkService.check_detail_size_title(params));
	}
	
	@RequestMapping(value = "check_detail_size/{ck_number}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object check_detail_size(@PathVariable String ck_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("ckl_number", ck_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(checkService.check_detail_size(params));
	}
	
	@RequestMapping(value = "check_temp_list/{ba_number}/{ckl_isrepair}/{dp_code}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object check_temp_list(@PathVariable String ba_number,@PathVariable Integer ckl_isrepair,@PathVariable String dp_code,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("ba_number", ba_number);
		params.put("ckl_isrepair", ckl_isrepair);
		params.put("dp_code", dp_code);
		params.put("ckl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(checkService.check_temp_list(params));
	}
	
	@RequestMapping(value = "check_temp_sum/{ba_number}/{ckl_isrepair}/{dp_code}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object check_temp_sum(@PathVariable String ba_number,@PathVariable Integer ckl_isrepair,@PathVariable String dp_code,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("ba_number", ba_number);
		params.put("ckl_isrepair", ckl_isrepair);
		params.put("dp_code", dp_code);
		params.put("ckl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(checkService.check_temp_sum(params));
	}
	
	@RequestMapping(value = "check_temp_size_title/{ba_number}/{ckl_isrepair}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object check_temp_size_title(@PathVariable String ba_number,@PathVariable Integer ckl_isrepair,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("ba_number", ba_number);
		params.put("ckl_isrepair", ckl_isrepair);
		params.put("ckl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(checkService.check_temp_size_title(params));
	}
	
	@RequestMapping(value = "check_temp_size/{ba_number}/{ckl_isrepair}/{dp_code}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object check_temp_size(@PathVariable String ba_number,@PathVariable Integer ckl_isrepair,@PathVariable String dp_code,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("ba_number", ba_number);
		params.put("ckl_isrepair", ckl_isrepair);
		params.put("dp_code", dp_code);
		params.put("ckl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(checkService.check_temp_size(params));
	}
	
	@RequestMapping(value = "check_temp_save_bybarcode/{ba_number}/{ckl_isrepair}/{dp_code}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object check_temp_save_bybarcode(@PathVariable String ba_number,@PathVariable Integer ckl_isrepair,@PathVariable String dp_code,
			@RequestParam(required = true) String barcode,@RequestParam(required = true) Integer amount,
			@RequestParam(required = false) Integer ba_scope,
			@RequestParam(required = false) String ba_scope_code,
			HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("ba_number", ba_number);
		params.put("ckl_isrepair", ckl_isrepair);
		params.put("dp_code", dp_code);
		params.put("barcode", barcode);
		params.put("amount", amount);
		params.put("ba_scope", ba_scope);
		params.put("ba_scope_code", ba_scope_code);
		params.put("user", user);
		Map<String, Object> resultMap = checkService.check_temp_save_bybarcode(params);
		return ajaxSuccess(resultMap);
	}
	
	@RequestMapping(value = "page_product/{ba_number}/{ckl_isrepair}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page_product(@PathVariable String ba_number,@PathVariable Integer ckl_isrepair,
			PageForm pageForm,
			@RequestParam(required = false) String alreadyExist,
			@RequestParam(required = false) String exactQuery,
			@RequestParam(required = false) Integer ba_scope,
			@RequestParam(required = false) String ba_scope_code,
			HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
		param.put("ba_number", ba_number);
		param.put("ckl_isrepair", ckl_isrepair);
        param.put("searchContent", pageForm.getSearchContent());
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put("us_id", user.getUs_id());
        param.put("alreadyExist", alreadyExist);
        param.put("exactQuery", exactQuery);
        param.put("ba_scope", ba_scope);
        param.put("ba_scope_code", ba_scope_code);
		return ajaxSuccess(checkService.page_product(param));
	}
	
	@RequestMapping(value = "check_temp_loadproduct/{ba_number}/{ckl_isrepair}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object check_temp_loadproduct(@PathVariable String ba_number,@PathVariable Integer ckl_isrepair,
			@RequestParam(required = true) String pd_code,
			@RequestParam(required = false) String exist,
			@RequestParam(required = false) String dp_code,
			HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put(CommonUtil.COMPANYID, user.getCompanyid());
		params.put("ba_number", ba_number);
		params.put("ckl_isrepair", ckl_isrepair);
		params.put("pd_code", pd_code);
		params.put("exist", exist);
		params.put("dp_code", dp_code);
		params.put("us_id", user.getUs_id());
		params.put(CommonUtil.KEY_SYSSET, getSysSet(session));
		return ajaxSuccess(checkService.temp_loadproduct(params));
	}
	
	@RequestMapping(value = "check_temp_save/{ba_number}/{ckl_isrepair}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object check_temp_save(@PathVariable String ba_number,@PathVariable Integer ckl_isrepair,
			@RequestBody Map<String, Object> data,HttpSession session) {
		T_Sys_User user = getUser(session);
		List<T_Stock_CheckList> temps = CheckVO.convertMap2Model(data, user,ba_number,ckl_isrepair);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("temps", temps);
		params.put("user", user);
		checkService.check_temp_save(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "check_temp_del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object check_temp_del(@RequestParam Integer ckl_id) {
		checkService.check_temp_del(ckl_id);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "check_temp_updateAmount", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object check_temp_updateAmount(T_Stock_CheckList temp) {
		checkService.check_temp_updateAmount(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "check_temp_updateRemarkById", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object check_temp_updateRemarkById(T_Stock_CheckList temp) {
		temp.setCkl_remark(StringUtil.decodeString(temp.getCkl_remark()));
		checkService.check_temp_updateRemarkById(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "check_temp_updateRemarkByPdCode", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object check_temp_updateRemarkByPdCode(T_Stock_CheckList temp,HttpSession session) {
		T_Sys_User user = getUser(session);
		temp.setCkl_remark(StringUtil.decodeString(temp.getCkl_remark()));
		temp.setCkl_us_id(user.getUs_id());
		temp.setCompanyid(user.getCompanyid());
		checkService.check_temp_updateRemarkByPdCode(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "check_temp_delByPiCode", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object check_temp_delByPiCode(@RequestParam String pd_code,@RequestParam String ckl_ba_number,@RequestParam Integer ckl_isrepair,
			@RequestParam(required = false) String cr_code,
			@RequestParam(required = false) String br_code,
			HttpSession session) {
		T_Sys_User user = getUser(session);
		T_Stock_CheckList temp = new T_Stock_CheckList();
		temp.setCkl_pd_code(pd_code);
		temp.setCkl_cr_code(cr_code);
		temp.setCkl_br_code(br_code);
		temp.setCkl_us_id(user.getUs_id());
		temp.setCompanyid(user.getCompanyid());
		temp.setCkl_ba_number(ckl_ba_number);
		temp.setCkl_isrepair(ckl_isrepair);
		checkService.check_temp_delByPiCode(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "check_temp_clear", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object check_temp_clear(@RequestParam String ckl_ba_number,@RequestParam Integer ckl_isrepair,HttpSession session) {
		T_Sys_User user = getUser(session);
		checkService.check_temp_clear(ckl_ba_number, ckl_isrepair, user.getUs_id(), user.getCompanyid());
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "check_temp_clear_all", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object check_temp_clear_all(@RequestParam String ckl_ba_number,HttpSession session) {
		T_Sys_User user = getUser(session);
		checkService.check_temp_clear_all(ckl_ba_number, user.getUs_id(), user.getCompanyid());
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "check_temp_import/{ba_number}/{ckl_isrepair}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object check_temp_import(@PathVariable String ba_number,@PathVariable Integer ckl_isrepair,
			@RequestBody Map<String, Object> data,HttpSession session) {
		T_Sys_User user = getUser(session);
		List<String[]> datas = CheckVO.convertMap2Model_import(data);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("datas", datas);
		params.put("user", user);
		params.put("ba_number", ba_number);
		params.put("ckl_isrepair", ckl_isrepair);
		checkService.check_temp_import(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "check_temp_repair/{ba_number}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object check_temp_repair(@PathVariable String ba_number,@RequestParam String repair_type,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("user", user);
		params.put("ba_number", ba_number);
		params.put("repair_type", repair_type);
		checkService.check_temp_repair(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "check_save/{ba_number}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object check_save(@PathVariable String ba_number,T_Stock_Check check,HttpSession session) {
		check.setCk_ba_number(ba_number);
		checkService.check_save(check, getUser(session));
		return ajaxSuccess(check);
	}
	
	@RequestMapping(value = "check_recheck/{ck_number}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object check_recheck(@PathVariable String ck_number,HttpSession session) {
		checkService.check_recheck(ck_number, getUser(session));
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "check_missing/{ba_number}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object check_missing(@PathVariable String ba_number,
			@RequestParam(required = true) Integer ba_scope,
			@RequestParam(required = true) String ba_scope_code,
			@RequestParam(required = true) String dp_code,
			HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("ba_number", ba_number);
		params.put("ba_scope", ba_scope);
		params.put("ba_scope_code", ba_scope_code);
		params.put("dp_code", dp_code);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(checkService.check_missing(params));
	}
	
	@RequestMapping(value = "check_exec/{ba_number}/{ba_isexec}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object check_exec(@PathVariable String ba_number,@PathVariable Integer ba_isexec,HttpSession session) {
		checkService.check_exec(ba_number, ba_isexec, getUser(session));
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "check_diff_list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object check_diff_list(PageForm pageForm,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String ba_dp_code,
			@RequestParam(required = false) String ba_number,
			@RequestParam(required = false) String pd_no,
			@RequestParam(required = false) String pd_name,
			@RequestParam(required = false) String pd_tp_code,
			@RequestParam(required = false) String pd_bd_code,
			@RequestParam(required = true) Integer loss,
			@RequestParam(required = true) Integer overflow,
			@RequestParam(required = true) Integer flat,
			HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("ba_dp_code", ba_dp_code);
        param.put("ba_number", StringUtil.decodeString(ba_number));
        param.put("pd_no", StringUtil.decodeString(pd_no));
        param.put("pd_name", StringUtil.decodeString(pd_name));
        param.put("pd_tp_code", pd_tp_code);
        param.put("pd_bd_code", pd_bd_code);
        param.put("loss", loss);
        param.put("overflow", overflow);
        param.put("flat", flat);
		return ajaxSuccess(checkService.check_diff_list(param));
	}
	
	@RequestMapping(value = "check_diff_sum", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object check_diff_sum(PageForm pageForm,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String ba_dp_code,
			@RequestParam(required = false) String ba_number,
			@RequestParam(required = false) String pd_no,
			@RequestParam(required = false) String pd_name,
			@RequestParam(required = false) String pd_tp_code,
			@RequestParam(required = false) String pd_bd_code,
			@RequestParam(required = true) Integer loss,
			@RequestParam(required = true) Integer overflow,
			@RequestParam(required = true) Integer flat,
			HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("ba_dp_code", ba_dp_code);
        param.put("ba_number", StringUtil.decodeString(ba_number));
        param.put("pd_no", StringUtil.decodeString(pd_no));
        param.put("pd_name", StringUtil.decodeString(pd_name));
        param.put("pd_tp_code", pd_tp_code);
        param.put("pd_bd_code", pd_bd_code);
        param.put("loss", loss);
        param.put("overflow", overflow);
        param.put("flat", flat);
		return ajaxSuccess(checkService.check_diff_sum(param));
	}
	
}
