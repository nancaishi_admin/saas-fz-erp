package zy.util;

public class UseableStockUtil {
	public final static String BUY_ORDER_0 = "BUY_ORDER_0";//采购订单
	public final static String BUY_ENTER_0 = "BUY_ENTER_0";//采购入库单
	public final static String BUY_ORDER_1 = "BUY_ORDER_1";//采购退货申请单
	public final static String BUY_ENTER_1 = "BUY_ENTER_1";//采购退货出库单
	public final static String BATCH_ORDER_0 = "BATCH_ORDER_0";//批发订单
	public final static String BATCH_SELL_0 = "BATCH_SELL_0";//批发出库单
	public final static String BATCH_ORDER_1 = "BATCH_ORDER_1";//批发退货申请单
	public final static String BATCH_SELL_1 = "BATCH_SELL_1";//批发退货入库单
	public final static String SHOP_WANT_0 = "SHOP_WANT_0";//前台补货单
	public final static String SHOP_WANT_1 = "SHOP_WANT_1";//前台退货单
	public final static String SORT_ALLOT_0 = "SORT_ALLOT_0";//店铺配货单
	public final static String SORT_ALLOT_1 = "SORT_ALLOT_1";//店铺退货单
	public final static String STOCK_ALLOCATE = "STOCK_ALLOCATE";//仓库调拔单
}
