package zy.util;

public class FormatterUtil {
	/**
	 * 盘点机分隔符
	 */
	public static String formatPandianSplitChar(Integer st_blank){
		if(st_blank == null){//未配置，则默认三个空格
			return "   ";
		}
		if(st_blank.equals(0)){
			return ",";
		}else if(st_blank.equals(1)){
			return " ";
		}else if(st_blank.equals(2)){
			return "  ";
		}else if(st_blank.equals(3)){
			return "   ";
		}else if(st_blank.equals(4)){
			return ";";
		}else if(st_blank.equals(5)){
			return "\t";
		}
		return "   ";
	}
}
