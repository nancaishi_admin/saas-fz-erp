package zy.util;

import java.text.DecimalFormat;


public class PriceLimitUtil {
	public static String checkPriceLimit(double price,String limitFlag,String us_price_limit){
		if(us_price_limit.indexOf(limitFlag) > -1){
			return String.valueOf(new DecimalFormat("#0.00").format(price));
		}else{
			return "***";
		}
	}
	public static boolean hasPriceLimit(String limitFlag,String us_price_limit){
		if(us_price_limit.indexOf(limitFlag) > -1){
			return true;
		}else{
			return false;
		}
	}
	
	public static String checkPriceLimit(double price, String limitFlag1, String limitFlag2, String us_price_limit) {
		if (us_price_limit.indexOf(limitFlag1) > -1 && us_price_limit.indexOf(limitFlag2) > -1) {
			return String.valueOf(new DecimalFormat("#0.00").format(price));
		} else {
			return "***";
		}
	}
	
	public static boolean hasPriceLimit(String limitFlag1,String limitFlag2,String us_price_limit){
		if (us_price_limit.indexOf(limitFlag1) > -1 && us_price_limit.indexOf(limitFlag2) > -1) {
			return true;
		}else{
			return false;
		}
	}

}
