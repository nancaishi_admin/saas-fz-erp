package zy.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class NumberUtil {
	final static String ONE = "1"; 
	final static String TWO = "2"; 
	final static String THREE = "3";
	
	public final static double buildMoney(String decimal,String type,double money){
		BigDecimal number = new BigDecimal(money);
		money = number.setScale(getDecimal(decimal), getType(type)).doubleValue();  
		return money;
	}
	private static int getDecimal(String decimal){
		int _decimal = 0;
		if(ONE.toString().equals(decimal)){//整数
			_decimal = 0;
		} else if(TWO.toString().equals(decimal)){//一位小数
			_decimal = 1;
		}else if(THREE.toString().equals(decimal)){//两位小数
			_decimal = 2;
		}
		return _decimal;
	}
	private static RoundingMode getType(String type){
		RoundingMode _type = RoundingMode.HALF_UP;
		if(ONE.equals(type)){//四舍五入
			_type = RoundingMode.HALF_UP;
		} else if(TWO.equals(type)){//只舍不入
			_type = RoundingMode.FLOOR;
		} else if(THREE.equals(type)){//只入不舍
			_type = RoundingMode.CEILING;
		}
		return _type;
	}
	
	public static double toDouble(Object money){
		if(StringUtil.isNotEmpty(money) && !"null".equals(money.toString())){
			return Double.parseDouble(money.toString());
		}else{
			return 0d;
		}
	}
	
	public static Integer toInteger(Object money){
		if(StringUtil.isNotEmpty(money) && !"null".equals(money.toString())){
			String _money = StringUtil.trimString(money);
			if(_money.indexOf(".")>0){
				_money = _money.substring(0, _money.indexOf("."));
			}
			return Integer.parseInt(_money);
		}else{
			return 0;
		}
	}
	public static void main(String[] args) {
		List<String> list = new ArrayList<String>();
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");
		for(String str:list){
			if(str.equals("c")){
				list.remove(str);
			}
		}
		for(int i=0;i<list.size();i++){
			if(list.get(i).equals("a")){
				list.remove(i);
			}
		}
		System.out.println(list.size());
	}
}
