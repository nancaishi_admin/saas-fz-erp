package zy.util;

public class URLUtil {
	public static final String TAOBAO_CITYURL = "http://ip.taobao.com/service/getIpInfo.php?ip=";
	
	public static final String APPKEY = "27378";//13267
	public static final String SIGN = "a26fbbba487bb9138e0841984a952a95";//cf16fd8ac91cb1df9f868fd8d2d03dff
	public static final String FORMAT = "json";
	
	public static final String URL_CITYJSON = "http://api.k780.com:88/?app=weather.city&format=json";
	public static final String URL_WEATHER = "http://api.k780.com:88/?app=weather.today&appkey="+APPKEY+"&sign="+SIGN+"&format="+FORMAT+"&weaid=";
	public static final String URL_TOMORROW = "http://api.k780.com:88/?app=weather.future&&appkey="+APPKEY+"&sign="+SIGN+"&format="+FORMAT+"&weaid=";

	
	
	public static final String RESULT = "result";
	public static final String CITYNAME = "citynm";
	public static final String WEAID = "weaid";
	public static final String TEMPLOW = "temp_low";
	public static final String TEMPHIGH = "temp_high"; 
	public static final String WEATID = "weatid";
	public static final String WEATHER = "weather";
	public static final String WIND = "wind";
}
