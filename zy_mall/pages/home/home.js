var common = require("../../common/common.js");
var server = require("../../common/server.js");
//获取应用实例
const app = getApp()
// common.initTemplateInfo();
Page({
  data: {
    my_shop: '我的商家',
    logo: '../../resources/images/logo.png',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo')
  },
  onLoad: function () {
    console.log(app);
    wx.navigateTo({
      url: '/pages/login/login'
    })
  },
  getUserInfo: function (e) {
    console.log(e)
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  },
  toProduct: function (e) {
    console.log("name:");
    wx.switchTab({
      url: '../shop/shop'
    }) 
  }
})
