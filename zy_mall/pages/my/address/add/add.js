
//var Bmob = require('../../utils/bmob.js');
//var WxNotificationCenter = require('../../utils/WxNotificationCenter.js');

var common = require("../../../../common/common.js");
var server = require("../../../../common/server.js");
//获取应用实例
const app = getApp()

var that;

Page({
  data:{
    wd_id:0
  },
	onLoad: function (options) {
        that = this;
        
        // 注册通知
        //WxNotificationCenter.addNotification("poiSelectedNotification",that.getAddress,that);
        // 属于编辑状态
        if (options.objectId) {
            that.loadAddress(options.objectId);
            that.setData({
                isEdit: true
            });
            wx.setNavigationBarTitle({
                title: '编辑地址'
            })
        } else {
            wx.setNavigationBarTitle({
                title: '添加地址'
            })
        }
        
	},
	selectAddress: function () {
        console.log('tapped')
        // 跳转选择poi
		wx.navigateTo({
			url: '../search/search'
		});
	},
    getAddress: function (area) {
        // 选择poi地址回调
        that.setData({
            area: area
        });
    },
    add: function (e) {
        var form = e.detail.value;
        form.wd_user_code = '10005';
        // console.log(form);
        // 表单验证
        if (form.wd_name == '') {
            wx.showModal({
                title: '请填写收件人姓名',
                showCancel: false
            });
            return;
        }

        if (!(/^1[34578]\d{9}$/.test(form.wd_mobile))){ 
            wx.showModal({
                title: '请填写正确手机号码',
                showCancel: false
            });
            return;
        }

        if (form.wd_addr == '') {
            wx.showModal({
                title: '请填写详细地址',
                showCancel: false
            });
            return;
        }

        console.info(form);

        // 是否处在编辑状态
        if (that.data.isEdit) {

          form.wd_id = that.data.wd_id
          server.postJSON(common.gb.CurrentURL + '/api/wx/my/updateAddressById',
            form, function (result) {
              console.info(result);
            });
        }else{
          server.postJSON(common.gb.CurrentURL + '/api/wx/my/saveAddress',
            form, function (result) {
              console.info(result);
            });
        }

        wx.navigateTo({
          url: '../list/list'
        });
        return;

        form.gender = parseInt(form.gender);
        form.user = Bmob.User.current();
        var address = new Bmob.Object('Address');
        // 是否处在编辑状态
        if (that.data.isEdit) {
            address = that.data.address;
        }
        address.save(form).then(function (res) {
            // console.log(res)
            wx.showModal({
                title: '保存成功',
                showCancel: false,
                success: function () {
                    wx.navigateBack();
                }
            });
        }, function (res) {
            // console.log(res)
            wx.showModal({
                title: '保存失败',
                showCancel: false
            });
        });
    },
    loadAddress: function (objectId) {
      console.info(objectId);
      that.setData({
        wd_id: objectId
      });
      console.info(that.data.wd_id);
        /*var query = new Bmob.Query('Address');
        query.get(objectId).then(function (addressObject) {
            that.setData({
                address: addressObject
            });
        });*/

      server.postJSON(common.gb.CurrentURL + '/api/wx/my/getAddressInfoById',
        { companyid: 1, wd_id: parseInt(objectId) }, function (result) {
          //console.info(result.data.data);
          //var res = result.data.data;
          that.setData({
            address: result.data.data
            //visual: results.length ? 'hidden' : 'show'
          });
        });
      /*
      var addressObject1 = {wd_name:'hello'};
      that.setData({
        address: addressObject1
      });
      */

    },
    delete: function () {
        // 确认删除对话框
        wx.showModal({
            title: '确认删除',
            success: function (res) {
                if (res.confirm) {

                  server.postJSON(common.gb.CurrentURL + '/api/wx/my/deleteAddressById/' + that.data.wd_id,
                    {}, function (result) {
                      var res = result.data;
                      var data = res.data;
                      var message = res.message;

                      if (res.stat == 200) {

                        wx.showToast({
                          title: message,
                          icon: 'success',
                          duration: 2000
                        })
                        setTimeout(function () {
                          wx.redirectTo({
                            url: '/pages/index/index'
                          })
                        }, 2000);
                      }
                    });

/*
                    var address = that.data.address;
                    address.destroy().then(function (result) {
                        wx.showModal({
                            title: '删除成功',
                            showCancel: false,
                            success: function () {
                                wx.navigateBack();
                            }
                        });
                    });
*/

                }
            }
        });
        
    }
})