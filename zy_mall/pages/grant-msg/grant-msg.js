// page/grant-msg/grant-msg.js
var app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    app.canILogin();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  // /**
  //  * 用户点击右上角分享
  //  */
  // onShareAppMessage: function () {
  
  // },

  auth: function() {
    wx.openSetting({
      success: function (data) {
        if (data) {
          if (data.authSetting["scope.userInfo"] == true) {

            if (app.canILogin()) {
              app.getUserInfo({
                that: that,
                succFun: function () {
                  // var userInfo = app.globalData["userInfo"];
                  // that.setData({
                  //   userInfo: userInfo
                  // });
                  wx.redirectTo({
                    url: '/pages/loading/loading',
                  })
                }
              });
            }
            // app.getUserInfo({
            //   success: function() {
            //     wx.redirectTo({
            //       url: '/pages/loading/loading',
            //     })
            //   }
            // });
          }
        }
      },
      fail: function () {
        console.info("设置失败返回数据");
      }
    });
  },
  getUserInfo: function (e) {
    var that = this;

    if (e.detail.errMsg == 'getUserInfo:fail auth deny') {
      wx.hideLoading();
      wx.showToast({
        title: '请允许微信授权',
        icon: 'none'
      })
    } else {
      app.setUserInfo({
        that: that,
        res: {
          userInfo: e.detail.userInfo,
          signature: e.detail.signature,
          encryptedData: e.detail.encryptedData,
          iv: e.detail.iv
        },
        succFun: function () {
          wx.redirectTo({
            url: '/pages/loading/loading',
          })
        }
      });
    }
  }
})