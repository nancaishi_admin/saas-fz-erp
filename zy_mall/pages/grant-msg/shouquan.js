// pages/shouquan/shouquan.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    infodata: {
      signature: '',
      encryptedData: '',
      iv: ''
    },
    returnUrl: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    if (options.returnUrl != undefined) {
      that.setData({
        returnUrl: options.returnUrl
      })
    }
  },

  getuserinfo: function (e) {
    var that = this;
    console.log(JSON.stringify(e));
    wx.showLoading();
    if (e.detail.errMsg == 'getUserInfo:fail auth deny') {
      wx.hideLoading();
      wx.showModal({
        title: '提示',
        content: '请确认授权',
        success:function(){
          that.gotoshouquan({e:e});
        }
      })
    }
    else {
      wx.hideLoading();
      app.setUserInfo({
        that: that,
        res: {
          userInfo: e.detail.userInfo,
          signature: e.detail.signature,
          encryptedData: e.detail.encryptedData,
          iv: e.detail.iv
        },
        succFun: function () {
          wx.switchTab({
            url: '/pages/my/my',
          })
        }
      });
      
    }
  },

  gotoshouquan: function (e) {
    var that = this
    if (wx.openSetting) {
      wx.openSetting({
        success: function (res) {
          //尝试再次登录  
          wx.showLoading();
          that.getuserinfo(e);
        }
      })
    } else {
      wx.showModal({
        title: '授权提示',
        content: '小程序需要您的微信授权才能使用哦~ 错过授权页面的处理方法：删除小程序->重新搜索进入->点击授权按钮'
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})