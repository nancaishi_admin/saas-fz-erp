var common = require("../../../common/common.js");
var server = require("../../../common/server.js");
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    count: 0,
    photos: [],
    star: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  showphoto: function () {
    var that = this;
    if (that.data.photos.length >= 4) {
      wx.showToast({
        title: '最多可以秀4张图',
        icon: 'none'
      })
      return false;
    }

    that.data.count = that.data.count + 1;

    wx.chooseImage({
      count: 1, // 默认9  
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有  
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有  
      success: function (res) {
        var filePath = res.tempFilePaths[0];
        console.warn("filepath:" + filePath)
        wx.uploadFile({
          url: common.gb.CurrentURL + '/api/wx/product/saveCommentImg',
          filePath: filePath,
          name: 'file',
          success: function (res) {
            console.warn("res:" + JSON.stringify(res));
            var data = JSON.parse(res.data);
            if (data.ErrorCode == "0") {
              var fileName = data.Data;
              var dir = 'wxcomment_img/' + fileName;
              var commentPic = common.gb.CurrentURL + '/' + dir;

              that.data.photos.push(commentPic);
              that.setData({
                photos: that.data.photos
              })
              console.warn("commentPic:" + commentPic);

            }
          }
        });
      }
    })

    that.setData({
      count: that.data.count
    })
  },
  getcomment: function (e) {
    var that = this;
    that.setData({
      comment: e.detail.value
    });
  },
  saveComment: function () {
    var that = this;
    if (!that.data.comment) {
      wx.showToast({
        title: '请填写您的评价',
        icon: 'none'
      })
      return false;
    }

    console.log("comment:" + that.data.comment);
    var escapecomment = escape(that.data.comment);
    console.log("escape:" + escapecomment);

    var unescapecomment = unescape(escapecomment);
    console.log("unescape:" + unescapecomment);
    var memberid = wx.getStorageSync('rd_session') || '0';
    var storeid = wx.getStorageSync('storeId') || '0';
    server.getJSON(common.gb.CurrentURL + '/Interface/HmapInfo/GetDataInfoWithPageFlagForCY', {
      action: 'saveComment',
      content: escapecomment,
      photos: that.data.photos || '',
      star: that.data.star,
      memberid: memberid,
      storeid: storeid,
      hmapid: common.gb.ID,
      orgid: common.gb.OrgID,
      delphotos: that.data.delphotos || ''
    }, function (json) {
      //console.warn("modifyMember:" + JSON.stringify(json));
      var data = json.data;
      if (data) {
        if (data.status == 'success') {
          wx.showToast({
            title: '发表完成',
            success: function () {
              wx.navigateBack({
              })
            }
          })
        }
      }
    });
  },
  dotstar: function (e) {
    var that = this;
    var curindex = e.currentTarget.dataset.index
    that.setData({
      star: curindex
    })

  },
  //删除当前添加的图片
  delImage: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    var photos = that.data.photos;
    var delphotos = [];
    if (photos) {
      delphotos.push(photos[index]);
      photos.splice(index, 1);
    }

    that.setData({
      photos: photos,
      delphotos: delphotos
    })
  }

  // dotstar:function(e){
  //   var that= this;
  //   var curindex = e.currentTarget.dataset.index
  //   that.data.star = that.data.star + 1;
  //   that.setData({
  //     star: that.data.star
  //   })

  // },
  // cancletstar:function(e){
  //   var that = this;
  //   var curindex = e.currentTarget.dataset.index
  //   that.data.star = that.data.star - 1;
  //   that.setData({
  //     star: that.data.star
  //   })
  // }

})