var common = require("../../../common/common.js");
var server = require("../../../common/server.js");
// 获取全局应用程序实例对象
var app = getApp();


// userinfo.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    items: [
      { value: '1', name: '先生', checked: true },
      { value: '2', name: '女士', checked: false },
    ],
    member: {
      name: '',
      born: '',
      sex: '1',
      wxnumber: '',
      mobile: '',
      address: '',
      address2:''
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    var user = app.globalData.user;
    var company = app.globalData.currentCompany;//店铺信息

    that.setData({
      user: user,
      company: company
    })

    var action = options.action ||'';
    var params = options.params ||'';

    var memberid = app.rd_session || wx.getStorageSync('rd_session');
    var today = new Date(); // 获取今天时间
    var todayscope = new Date(); // 获取今天时间

    today.setFullYear(today.getFullYear()-50);
    var startdate = today.getFullYear() + '-' + (parseInt(today.getMonth()) + 1) + '-' + today.getDate();
    var edate = new Date(); 
    var enddate = edate.getFullYear() + '-' + (parseInt(edate.getMonth()) + 1) + '-' + edate.getDate();
    var born = today.getFullYear() + '-' + (parseInt(today.getMonth()) + 1) + '-' + today.getDate();
    that.setData({
      born: born,
      startdate: startdate,
      enddate: enddate,
      action: action,    
      params: params
    })

    // common.getMember({
    //   app: app,
    //   that: that,
    //   memberid: memberid
    // });


  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
  onShareAppMessage: function () {

  },
   */
  /**
   * 监听名称输入框输入
   */
  nameInput: function (e) {
    var that = this;
    that.setData({
      name: e.detail.value
    })
  
  },
  
  /**
   * 监听微信号输入框输入
   */
  wxnumberInput: function (e) {
    var that = this;
    that.setData({
      wxnumber: e.detail.value
    })
    
  },
 
  /**
   * 监听地址输入框输入
   */
  addressInput: function (e) {
    var that = this;
    that.setData({
      address: e.detail.value
    })
   
  },
  
  /**
   * 保存会员档案
   */
  savemember: function () {
    var that = this; 
    // var memberid = app.rd_session || wx.getStorageSync('rd_session');
    // that.setData({
    //   memberid: memberid
    // })
    // if (!that.data.member.name){
    //   wx.showToast({
    //     title: '请输入姓名',
    //     icon:'none'
    //   })
    //   return false;
    // }
    // if (!that.data.member.mobile) {
    //   wx.showToast({
    //     title: '请输入手机号码',
    //     icon: 'none'
    //   })
    //   return false;
    // }
    // if (!that.data.member.address) {
    //   wx.showToast({
    //     title: '请输入地址',
    //     icon: 'none'
    //   })
    //   return false;
    // }

    that.setData({
      controller: '/api/wx/user/saveUserInfo',
      params: {
        mame: that.data.name||'',
        sex: that.data.sex || '',
        born: that.data.born || '',
        wxnumber: that.data.wxnumber || '',
        address: that.data.address || '',
        wu_code:that.data.user.wu_code
      }
    })

    common.doMethod({
      that: that,
      succFun: function (data) {
        if (data) {
          if (data.stat == 200) {
            if (data.data) {
              wx.showToast({
                title: '保存成功',
                icon: 'none',
                duration: 3000,
                success: function () {
                  wx.navigateBack({
                  })
                }
              })
            }
          }
          else {
            wx.showToast({
              title: data.message,
              icon: 'none'
            });
          }
        }
      }
    })

    


  },
    //  点击日期组件确定事件  
  bindDateChange: function (e) {
    var that = this;
    that.data.member.born = e.detail.value
    this.setData({
      born: e.detail.value
    })
  }, 

  radioChange: function (e) {
    console.log('radio发生change事件，携带value值为：', e.detail.value)
    var that = this;
    var items = this.data.items;

    var sex = '';
    for (var i = 0, len = items.length; i < len; ++i) {
      items[i].checked = false;
    }
    for (var i = 0, len = items.length; i < len; ++i) {
      items[i].checked = items[i].value == e.detail.value
      if (items[i].checked) {
        sex = items[i].value;
        that.setData({
          sex:sex
        })
        // that.data.member.sex = sex;
        break;

      }
    }

    that.setData({
      items: items,
      //card: that.data.card
    })
  },
})