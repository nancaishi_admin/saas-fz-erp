var common = require("../../../common/common.js");
var server = require("../../../common/server.js");
//获取应用实例
const app = getApp()
var pageIndex=1;
var pageSize=10;
Page({
	data: {
    statusType: ["全部", "待付款", "待收货", "已收货", "已完成","售后"],
    currentTpye:0,
		tabClass: ["", "", "", "", ""],
		orderList: [],
		offlinepay_qrcode: false
	},
  onLoad: function (options) {
    // 生命周期函数--监听页面加载
    this.data.currentTpye = options.type * 1
    this.setData({
      currentTpye: this.data.currentTpye
    });
    var that = this;
    // that.checkofflinepay();
  },
  initPageData: function () {
    wx.showToast({
      title: '加载中...',
      icon:'loading'
    })
    var that = this

    that.setData({
      controller: '/api/wx/order/getOrderList',
      params: {
        companyid: app.globalData.currentCompany.companyid,
        wu_code: app.globalData.user.wu_code,
        statustap: that.data.currentTpye,
        pageSize: pageSize,
        pageIndex: pageIndex
      }
    })

    common.pagedDetails({
      that: that,
      comFun:function(){
        wx.hideToast();
        wx.hideNavigationBarLoading();
      },
      succFun: function(data) {
        wx.hideLoading();        
        that.setData({
          details: data
        })
      }
    })

  },
  onReady: function () {
    // 生命周期函数--监听页面初次渲染完成

  },
  onShow: function () {
    // 获取订单列表
    var that = this
    that.initPageData();
  },
  onHide: function () {
    // 生命周期函数--监听页面隐藏

  },
  onUnload: function () {
    // 生命周期函数--监听页面卸载

  },
  //以下为自定义点击事件
  onPullDownRefresh() {
    var that = this;
    //下拉  
    console.log("下拉");
    pageIndex = 1;

    that.setData({
      pagination: false
    });
    that.initPageData();
  },
  //以下为自定义点击事件
  onReachBottom: function () {
    var that = this;
    //上拉  
    console.log("上拉");
    pageIndex++;

    that.setData({
      pagination: true
    });
    that.initPageData();
  },
	statusTap: function (e) {
		var curType = e.currentTarget.dataset.index;
		this.data.currentTpye = curType
		this.setData({
      pageIndex:1,
			currentTpye: curType
		});
    this.onPullDownRefresh();
	},
	orderDetail: function (e) {
    var ordernumber = e.currentTarget.dataset.ordernumber;
		wx.navigateTo({
      url: "/pages/order/order-details/index?ordernumber=" + ordernumber
		})
	},
	cancelOrderTap: function (e) {
		var that = this;
		//保存formid 开始
		var formid = e.detail.formId;
		var formid_json = {
			"xcxid": wx.getExtConfigSync().ID,
			"type": "submit",
			"openid": app.globalData.login.openId,
			"formid": formid
		}
		app.appSaveFormId(formid_json, function (res) { })
		//保存formid 结束
		var orderId = e.currentTarget.dataset.id;
		var orderAmt = e.currentTarget.dataset.amt;
		var productName = e.currentTarget.dataset.name;
		wx.showModal({
			title: '确定要取消该订单吗？',
			content: '',
			success: function (res) {
				if (res.confirm) {
					wx.showLoading();
					app.appRequest('/Order/OrderCancel',
						{
							orderNumber: orderId
						},
						function (res) {
							wx.hideLoading();
							var resData = res.data
							if (resData.ErrorCode == 0) {
								//订单取消发送消息模板 开始
								var msgtempdata = {
									"openid": app.globalData.login.openId,
									"xcxid": wx.getExtConfigSync().ID,
									"sendtype": "OrderInfo",
									"OrderType": "CA",
									"OrderNo": orderId,
									"OrderAmt": orderAmt,
									"ProductName": productName
								}
								app.sendMsgTemp(msgtempdata);
								//订单取消发送消息模板 结束
								that.onShow();
							} else {
								wx.showToast({
									title: resData.ErrorMsg,
								})
							}
						}
					)
				}
			}
		})
	},
	toReciveTap: function (e) {
		var that = this;
		var formid = e.detail.formId;
		var formid_json = {
			"xcxid": wx.getExtConfigSync().ID,
			"type": "submit",
			"openid": app.globalData.login.openId,
			"formid": formid
		}
		app.appSaveFormId(formid_json, function (res) { })
		var orderId = e.currentTarget.dataset.id;
		var productName = e.currentTarget.dataset.name;
		wx.showModal({
			title: '确定要收货吗？',
			content: '',
			success: function (res) {
				if (res.confirm) {
					wx.showLoading();
					app.appRequest('/Order/OrderSignReceive',
						{
							orderNumber: orderId
						},
						function (res) {
							wx.hideLoading();
							var resData = res.data
							if (resData.ErrorCode == 0) {
								that.onShow();
								//发送模板消息 开始
								var msgtempdata = {
									"openid": app.globalData.login.openId,
									"xcxid": wx.getExtConfigSync().ID,
									"sendtype": "OrderInfo",
									"OrderType": "SN",
									"OrderNo": orderId,
									"ProductName": productName
								}
								app.sendMsgTemp(msgtempdata);
								//发送模板消息 结束
							}
							else {
								wx.showToast({
									title: resData.ErrorMsg,
								})
							}
						}
					)
				}
			}
		})
	},
	toPayTap: function (e) {
		var that = this
		var order = e.currentTarget.dataset.id;
		app.appPay(order, function () {
			that.onShow();
		})
	},
	toOfflinePayTap: function (e) {
		wx.navigateTo({
			url: '/pages/qrcode/index?sono=' + e.currentTarget.dataset.id + "&soamt=" + e.currentTarget.dataset.amt,
		})
	},
	// checkofflinepay: function () {
	// 	var that = this;
	// 	app.appRequest("/org/GetOrgProperty", {
	// 		'orgId': app.globalData.orgId
	// 	}, function (res) {
	// 		if (JSON.parse(JSON.parse(res.data.Data)).offlinepay_qrcode != undefined &&
	// 			JSON.parse(JSON.parse(res.data.Data)).offlinepay_qrcode.length > 0) {
	// 			that.setData({
	// 				offlinepay_qrcode: true
	// 			})
	// 		}
	// 		console.log(that.data.offlinepay_qrcode)
	// 	})
	// },
	
})