var common = require("../../common/common.js");
var server = require("../../common/server.js");
//获取应用实例
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    oneTypes: [],
    twoTypes: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      this.loadTypes();
  },
  loadTypes: function () {
    var that = this;
    var currentCompany = app.globalData.currentCompany;
    server.postJSON(common.gb.CurrentURL + '/api/wx/producttype/list_level',
      {
        "companyid": currentCompany.companyid,
        "shop_type": currentCompany.sp_shop_type,
        "shop_code": currentCompany.sp_code
      }, function (result) {
        var res = result.data;
        if (res.stat == 200) {
          var data = res.data;
          that.setData({
            oneTypes: data
          })
        }
      });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  doTab:function(e){
    console.log("1");
    
  }
})