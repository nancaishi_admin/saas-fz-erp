var common = require("../../common/common.js");
var server = require("../../common/server.js");
//获取应用实例
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    mobile: '',
    pwd: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  mobileInput: function (e) {
    this.setData({
      mobile: e.detail.value
    })
  },
  pwdInput: function (e) {
    this.setData({
      pwd: e.detail.value
    })
  },

  toIndex:function(){
    var that = this;
    var mobile = 15151136382;//that.data.mobile;
    var pwd = 123456;//that.data.pwd;
    if(!mobile){
      wx.showToast({
        title: '请输入手机号码',
        icon: 'none'
      });
      return false;
    }
    if(!pwd){
      wx.showToast({
        title: '请输入密码',
        icon:'none'
      });
      return false;
    }

    that.setData({
      controller: '/login',
      params: {
        "mobile": mobile,
        "pwd": pwd,
        // "nickname":nickname,
        // "avatarurl":avatarurl,
        "openid": app.globalData.openid
      }
    })

    common.doMethod({
      that: that,
      succFun: function (data) {
        if (data) {
          if (data.stat == 200){
            if (data.data){
              wx.showToast({
                title: '登录成功跳转中',
                icon: 'none',
                duration: 3000,
                success: function () {
                  app.globalData.user = data.data.user||'';
                  app.globalData.user.address = data.data.address||'';
                  console.log(JSON.stringify(app.globalData.user));
                  wx.redirectTo({
                    url: '/pages/index/index'
                  })
                }
              })
            }
            else {
              wx.showToast({
                title: '手机号码或密码错误！',
                icon: 'none'
              });
            }            
          }
          else{
            wx.showToast({
              title: data.message,
              icon:'none'
            });
          }
        }
      }
    })


    // // server.postJSON(common.gb.CurrentURL + '/login/'+mobile+'/'+pwd+'/'+nickname+'/'+avatarurl,
    // //   {}, function (result) {
    // //   var res = result.data;
    // //   if(res.stat == 200){
    // //     var data = res.data;
    // //     if(data){
    // //       wx.showToast({
    // //         title: '登录成功跳转中',
    // //         icon: 'none',
    // //         duration: 3000,
    // //         success:function(){
    // //           app.globalData.user = data;
    // //           wx.redirectTo({
    // //             url: '/pages/index/index'
    // //           })
    // //         }
    // //       })
    // //     }else{
    // //       wx.showToast({
    // //         title: '手机号码或密码错误！',
    // //       });
    // //     }         
    // //   }
    // // });   
  }
})