var common = require("../../common/common.js");
var server = require("../../common/server.js");
var pageIndex = 1;
var pageSize = 10;
// 获取全局应用程序实例对象
var app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    hidden: true,
    pagination: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    pageIndex = 1;
    that.getCouponList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    var that = this;
    if (that.data.pageurl == 'order') {
      wx.navigateBack({});
    }
  },
  //以下为自定义点击事件
  onPullDownRefresh() {

    //下拉  
    //console.log("下拉");
    wx.showNavigationBarLoading(); //在标题栏中显示加载
    pageIndex = 1;
    var that = this;
    that.setData({
      couponlist :[],
      pagination : false
    });
    that.getCouponList();
  },
  //以下为自定义点击事件
  onReachBottom: function () {

    var that = this;
    //上拉  
    //console.log("上拉")
    var that = this;
    that.setData({
      pagination: true
    });
    pageIndex++;
    that.getCouponList();
  },

  // /**
  //  * 用户点击右上角分享
  //  */
  // onShareAppMessage: function () {

  // },
  
  drawVourcher: function (e) {
    var that = this;
    var ecl_code = e.currentTarget.dataset.voucherid;

    that.setData({
      controller: '/api/wx/coupon/drawCoupon',
      params: {
        companyid: app.globalData.currentCompany.companyid,
        shop_code: app.globalData.currentCompany.sp_code,
        wu_mobile: app.globalData.user.wu_mobile,
        wu_nickname: app.globalData.user.wu_nickname,
        ecl_code: ecl_code
        
      }
    })

     common.doMethod({
      that: that,
      succFun: function (data) {
        // var data = data.data;
        wx.showToast({
          title: '领取成功',
          icon: 'none',
          duration: 3000,
          success: function () {
            that.onPullDownRefresh();
          }
        })
      }
    })
  },
  getCouponList:function(e){
    var that = this;
    that.setData({
      controller: '/api/wx/coupon/getCouponCenterList',
      params: {
        companyid: app.globalData.currentCompany.companyid,
        shop_code: app.globalData.currentCompany.sp_code,
        wu_mobile: app.globalData.user.wu_mobile,
        pageIndex: pageIndex,
        pageSize:pageSize
      }
    })

    common.pagedDetails({
      that: that,
      succFun: function (data) {
        var data = data;
        // var data=[{
        //   usetype:0,//使用条件：无限额
        //   amount:10,//优惠券面值
        //   limitamount:0,//满多少
        //   endtime:'2018-05-30',//有效期
        //   status:0,//状态 0：正常 待领
        // },
        // {
        //   usetype: 1,//
        //   amount: 10,
        //   limitamount: 100,
        //   endtime: '2018-05-30',
        //   status: 1,// 状态 1: 已领
        // }
        // ];
        that.setData({
          couponlist: data
        })
      },
      comFun:function(){
        wx.hideNavigationBarLoading();
        wx.hideLoading();
      }
    })
  }
})
